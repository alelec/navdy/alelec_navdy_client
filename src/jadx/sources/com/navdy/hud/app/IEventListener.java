package com.navdy.hud.app;

public interface IEventListener extends android.os.IInterface {

    public static abstract class Stub extends android.os.Binder implements com.navdy.hud.app.IEventListener {
        private static final java.lang.String DESCRIPTOR = "com.navdy.hud.app.IEventListener";
        static final int TRANSACTION_onEvent = 1;

        private static class Proxy implements com.navdy.hud.app.IEventListener {
            private android.os.IBinder mRemote;

            Proxy(android.os.IBinder remote) {
                this.mRemote = remote;
            }

            public android.os.IBinder asBinder() {
                return this.mRemote;
            }

            public java.lang.String getInterfaceDescriptor() {
                return com.navdy.hud.app.IEventListener.Stub.DESCRIPTOR;
            }

            public void onEvent(byte[] bytes) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.IEventListener.Stub.DESCRIPTOR);
                    _data.writeByteArray(bytes);
                    this.mRemote.transact(1, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static com.navdy.hud.app.IEventListener asInterface(android.os.IBinder obj) {
            if (obj == null) {
                return null;
            }
            android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof com.navdy.hud.app.IEventListener)) {
                return new com.navdy.hud.app.IEventListener.Stub.Proxy(obj);
            }
            return (com.navdy.hud.app.IEventListener) iin;
        }

        public android.os.IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    onEvent(data.createByteArray());
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onEvent(byte[] bArr) throws android.os.RemoteException;
}
