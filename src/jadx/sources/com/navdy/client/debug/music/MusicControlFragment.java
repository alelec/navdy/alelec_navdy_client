package com.navdy.client.debug.music;

public class MusicControlFragment extends android.app.Fragment {

    class Anon1 implements android.view.View.OnClickListener {
        final /* synthetic */ com.navdy.service.library.events.audio.MusicEvent.Action val$action;

        Anon1(com.navdy.service.library.events.audio.MusicEvent.Action action) {
            this.val$action = action;
        }

        public void onClick(android.view.View v) {
            com.navdy.client.debug.music.MusicControlFragment.this.performAction(this.val$action);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.audio.MusicEvent val$event;

        Anon2(com.navdy.service.library.events.audio.MusicEvent musicEvent) {
            this.val$event = musicEvent;
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance().onMusicEvent(this.val$event);
        }
    }

    @android.support.annotation.Nullable
    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_music_control, container, false);
        setUpButton((android.widget.Button) rootView.findViewById(com.navdy.client.R.id.prev), com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PREVIOUS);
        setUpButton((android.widget.Button) rootView.findViewById(com.navdy.client.R.id.play), com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PLAY);
        setUpButton((android.widget.Button) rootView.findViewById(com.navdy.client.R.id.pause), com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PAUSE);
        setUpButton((android.widget.Button) rootView.findViewById(com.navdy.client.R.id.next), com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_NEXT);
        return rootView;
    }

    private void setUpButton(@android.support.annotation.Nullable android.widget.Button pauseButton, com.navdy.service.library.events.audio.MusicEvent.Action action) {
        if (pauseButton != null) {
            pauseButton.setOnClickListener(new com.navdy.client.debug.music.MusicControlFragment.Anon1(action));
        }
    }

    private void performAction(com.navdy.service.library.events.audio.MusicEvent.Action action) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.debug.music.MusicControlFragment.Anon2(new com.navdy.service.library.events.audio.MusicEvent.Builder().action(action).dataSource(com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_NONE).build()), 1);
    }
}
