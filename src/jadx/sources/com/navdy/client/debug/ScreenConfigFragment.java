package com.navdy.client.debug;

public class ScreenConfigFragment extends android.app.Fragment {
    private static int MAX_SCALE = 10;
    private static final int REPEAT_INTERVAL_MS = 250;
    private static final java.lang.String TAG = "ScreenConfigFragment";
    private android.graphics.Rect margins;
    private android.view.View pressedButton;
    private java.lang.Runnable repeatAction = new com.navdy.client.debug.ScreenConfigFragment.Anon1();
    private android.os.Handler repeatHandler;
    private int scale = 1;
    private float scaleX;
    private float scaleY;
    private com.navdy.service.library.events.settings.ScreenConfiguration screenConfiguration;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.debug.ScreenConfigFragment.this.adjustMargins(com.navdy.client.debug.ScreenConfigFragment.this.pressedButton, com.navdy.client.debug.ScreenConfigFragment.this.scale);
            if (com.navdy.client.debug.ScreenConfigFragment.this.scale < com.navdy.client.debug.ScreenConfigFragment.MAX_SCALE) {
                com.navdy.client.debug.ScreenConfigFragment.this.scale = com.navdy.client.debug.ScreenConfigFragment.this.scale + 1;
            }
            com.navdy.client.debug.ScreenConfigFragment.this.repeatHandler.postDelayed(this, 250);
        }
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.repeatHandler = new android.os.Handler();
    }

    public void onPause() {
        com.navdy.client.app.framework.util.BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_screen_config, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        return rootView;
    }

    public void onResume() {
        super.onResume();
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
        if (this.screenConfiguration == null) {
            sendEvent(new com.navdy.service.library.events.settings.ReadSettingsRequest(null));
        }
    }

    private void adjustMargins(int leftOffset, int topOffset, int rightOffset, int bottomOffset, int scale2) {
        if (this.margins != null) {
            this.margins.set(java.lang.Math.max(this.margins.left + (leftOffset * scale2), 0), this.margins.top + (topOffset * scale2), java.lang.Math.max(this.margins.right + (rightOffset * scale2), 0), java.lang.Math.max(this.margins.bottom + (bottomOffset * scale2), 0));
            updateSettings();
        }
    }

    private void adjustMargins(android.view.View button, int scale2) {
        if (button != null) {
            switch (button.getId()) {
                case com.navdy.client.R.id.slide_down /*2131755651*/:
                    adjustMargins(0, 1, 0, -1, scale2);
                    return;
                default:
                    adjustMargins(0, -1, 0, 1, scale2);
                    return;
            }
        }
    }

    @butterknife.OnTouch({2131755651, 2131755652})
    boolean onTouchButton(android.view.View v, android.view.MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case 0:
                this.pressedButton = v;
                this.scale = 1;
                this.repeatHandler.post(this.repeatAction);
                break;
            case 1:
            case 3:
                this.repeatHandler.removeCallbacks(this.repeatAction);
                break;
            default:
                android.util.Log.d(TAG, "onTouchButton: unknown action: " + action);
                break;
        }
        return false;
    }

    @butterknife.OnClick({2131755653})
    public void onResetScale() {
        this.margins = new android.graphics.Rect(0, 0, 0, 0);
        this.scaleY = 1.0f;
        this.scaleX = 1.0f;
        updateSettings();
    }

    private void sendEvent(com.squareup.wire.Message event) {
        com.navdy.client.app.framework.DeviceConnection.postEvent(event);
    }

    private void updateSettings() {
        if (this.margins != null) {
            sendEvent(new com.navdy.service.library.events.settings.UpdateSettings(new com.navdy.service.library.events.settings.ScreenConfiguration(java.lang.Integer.valueOf(this.margins.left), java.lang.Integer.valueOf(this.margins.top), java.lang.Integer.valueOf(this.margins.right), java.lang.Integer.valueOf(this.margins.bottom), java.lang.Float.valueOf(this.scaleX), java.lang.Float.valueOf(this.scaleY)), null));
        }
    }

    @com.squareup.otto.Subscribe
    public void onDeviceConnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent event) {
        sendEvent(new com.navdy.service.library.events.settings.ReadSettingsRequest(null));
    }

    @com.squareup.otto.Subscribe
    public void onDeviceDisconnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent event) {
        this.screenConfiguration = null;
    }

    @com.squareup.otto.Subscribe
    public void onReadSettingsResponse(com.navdy.service.library.events.settings.ReadSettingsResponse response) {
        this.screenConfiguration = response.screenConfiguration;
        this.margins = new android.graphics.Rect(this.screenConfiguration.marginLeft.intValue(), this.screenConfiguration.marginTop.intValue(), this.screenConfiguration.marginRight.intValue(), this.screenConfiguration.marginBottom.intValue());
        this.scaleX = this.screenConfiguration.scaleX.floatValue();
        this.scaleY = this.screenConfiguration.scaleY.floatValue();
    }
}
