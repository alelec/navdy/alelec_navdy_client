package com.navdy.client.debug;

public class DevicePickerFragment$$ViewInjector {

    /* compiled from: DevicePickerFragment$$ViewInjector */
    static class Anon1 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.DevicePickerFragment val$target;

        Anon1(com.navdy.client.debug.DevicePickerFragment devicePickerFragment) {
            this.val$target = devicePickerFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onAddClicked();
        }
    }

    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.DevicePickerFragment target, java.lang.Object source) {
        target.mTextViewCurrentDevice = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.device_picker_textview_current_device, "field 'mTextViewCurrentDevice'");
        android.view.View view = finder.findRequiredView(source, com.navdy.client.R.id.device_picker_button_add_new_device, "field 'mButtonAddDevice' and method 'onAddClicked'");
        target.mButtonAddDevice = (android.widget.Button) view;
        view.setOnClickListener(new com.navdy.client.debug.DevicePickerFragment$$ViewInjector.Anon1(target));
        target.mEditTextNewDevice = (android.widget.EditText) finder.findRequiredView(source, com.navdy.client.R.id.device_picker_edittext_add_new_device, "field 'mEditTextNewDevice'");
    }

    public static void reset(com.navdy.client.debug.DevicePickerFragment target) {
        target.mTextViewCurrentDevice = null;
        target.mButtonAddDevice = null;
        target.mEditTextNewDevice = null;
    }
}
