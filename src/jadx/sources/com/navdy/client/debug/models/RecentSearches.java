package com.navdy.client.debug.models;

public class RecentSearches {
    private static final int MAX_SIZE = 10;
    private java.util.ArrayList<com.navdy.service.library.events.places.PlacesSearchResult> results = new java.util.ArrayList<>();

    public java.util.List<com.navdy.service.library.events.places.PlacesSearchResult> getResults() {
        return this.results;
    }

    public void add(com.navdy.service.library.events.places.PlacesSearchResult result) {
        int position = findMatch(result);
        if (position != -1) {
            this.results.remove(position);
        }
        if (this.results.size() >= 10) {
            this.results.remove(9);
        }
        this.results.add(0, result);
    }

    public int findMatch(com.navdy.service.library.events.places.PlacesSearchResult result) {
        java.lang.String label = result.label;
        for (int position = 0; position < this.results.size(); position++) {
            if (((com.navdy.service.library.events.places.PlacesSearchResult) this.results.get(position)).label.equals(label)) {
                return position;
            }
        }
        return -1;
    }
}
