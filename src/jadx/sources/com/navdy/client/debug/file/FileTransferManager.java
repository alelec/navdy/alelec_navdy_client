package com.navdy.client.debug.file;

public abstract class FileTransferManager {
    protected static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.file.FileTransferManager.class);
    protected final android.content.Context mContext;
    protected final com.navdy.service.library.file.TransferDataSource mDataSource;
    protected final java.lang.String mDestinationFileName;
    protected final java.lang.String mDestinationFolder;
    protected final com.navdy.client.debug.file.FileTransferManager.FileTransferListener mFileTransferListener;
    protected final com.navdy.service.library.events.file.FileType mFileType;

    public interface FileTransferListener {
        void onError(com.navdy.service.library.events.file.FileTransferError fileTransferError, java.lang.String str);

        void onFileTransferResponse(com.navdy.service.library.events.file.FileTransferResponse fileTransferResponse);

        void onFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus fileTransferStatus);
    }

    public abstract void cancelFileUpload();

    public abstract boolean pullFile();

    public abstract boolean sendFile();

    public abstract boolean sendFile(long j);

    public FileTransferManager(android.content.Context context, com.navdy.service.library.events.file.FileType fileType, java.lang.String destinationFolder, com.navdy.client.debug.file.FileTransferManager.FileTransferListener listener) {
        this.mContext = context;
        this.mFileType = fileType;
        this.mFileTransferListener = listener;
        this.mDataSource = null;
        this.mDestinationFileName = null;
        this.mDestinationFolder = destinationFolder;
    }

    public FileTransferManager(android.content.Context context, com.navdy.service.library.file.TransferDataSource source, com.navdy.service.library.events.file.FileType fileType, java.lang.String destinationFileName, com.navdy.client.debug.file.FileTransferManager.FileTransferListener fileTransferListener) {
        this.mContext = context;
        this.mFileTransferListener = fileTransferListener;
        java.io.File file = source.getFile();
        if (file != null && !file.exists()) {
            postOnError(null, "File does not exist");
        }
        this.mDataSource = source;
        this.mFileType = fileType;
        this.mDestinationFileName = destinationFileName;
        this.mDestinationFolder = null;
    }

    protected void postOnFileTransferResponse(com.navdy.service.library.events.file.FileTransferResponse response) {
        try {
            this.mFileTransferListener.onFileTransferResponse(response);
        } catch (Throwable t) {
            sLogger.e("Bad listener : onFileTransferResponse ", t);
        }
    }

    protected void postOnFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus status) {
        try {
            this.mFileTransferListener.onFileTransferStatus(status);
        } catch (Throwable t) {
            sLogger.e("Bad listener : onFileTransferStatus ", t);
        }
    }

    protected void postOnError(com.navdy.service.library.events.file.FileTransferError errorCode, java.lang.String error) {
        try {
            this.mFileTransferListener.onError(errorCode, error);
        } catch (Throwable t) {
            sLogger.e("Bad listener : onError ", t);
        }
    }
}
