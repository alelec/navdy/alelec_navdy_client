package com.navdy.client.debug;

public class HudSettingsFragment$$ViewInjector {

    /* compiled from: HudSettingsFragment$$ViewInjector */
    static class Anon1 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.HudSettingsFragment val$target;

        Anon1(com.navdy.client.debug.HudSettingsFragment hudSettingsFragment) {
            this.val$target = hudSettingsFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onClick((android.widget.CheckBox) p0);
        }
    }

    /* compiled from: HudSettingsFragment$$ViewInjector */
    static class Anon2 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.HudSettingsFragment val$target;

        Anon2(com.navdy.client.debug.HudSettingsFragment hudSettingsFragment) {
            this.val$target = hudSettingsFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onClick((android.widget.CheckBox) p0);
        }
    }

    /* compiled from: HudSettingsFragment$$ViewInjector */
    static class Anon3 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.HudSettingsFragment val$target;

        Anon3(com.navdy.client.debug.HudSettingsFragment hudSettingsFragment) {
            this.val$target = hudSettingsFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onClick((android.widget.CheckBox) p0);
        }
    }

    /* compiled from: HudSettingsFragment$$ViewInjector */
    static class Anon4 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.HudSettingsFragment val$target;

        Anon4(com.navdy.client.debug.HudSettingsFragment hudSettingsFragment) {
            this.val$target = hudSettingsFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onClick((android.widget.CheckBox) p0);
        }
    }

    /* compiled from: HudSettingsFragment$$ViewInjector */
    static class Anon5 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.HudSettingsFragment val$target;

        Anon5(com.navdy.client.debug.HudSettingsFragment hudSettingsFragment) {
            this.val$target = hudSettingsFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onClick(p0);
        }
    }

    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.HudSettingsFragment target, java.lang.Object source) {
        android.view.View view = finder.findRequiredView(source, com.navdy.client.R.id.autoBrightness, "field 'autoBrightness' and method 'onClick'");
        target.autoBrightness = (android.widget.CheckBox) view;
        view.setOnClickListener(new com.navdy.client.debug.HudSettingsFragment$$ViewInjector.Anon1(target));
        target.brightness = (android.widget.SeekBar) finder.findRequiredView(source, com.navdy.client.R.id.brightness, "field 'brightness'");
        target.brightnessTextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.brightnessTextView, "field 'brightnessTextView'");
        target.ledBrightness = (android.widget.SeekBar) finder.findRequiredView(source, com.navdy.client.R.id.ledBrightness, "field 'ledBrightness'");
        target.ledBrightnessTextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.ledBrightnessTextView, "field 'ledBrightnessTextView'");
        android.view.View view2 = finder.findRequiredView(source, com.navdy.client.R.id.enable_gesture, "field 'enableGesture' and method 'onClick'");
        target.enableGesture = (android.widget.CheckBox) view2;
        view2.setOnClickListener(new com.navdy.client.debug.HudSettingsFragment$$ViewInjector.Anon2(target));
        android.view.View view3 = finder.findRequiredView(source, com.navdy.client.R.id.enable_preview, "field 'enablePreview' and method 'onClick'");
        target.enablePreview = (android.widget.CheckBox) view3;
        view3.setOnClickListener(new com.navdy.client.debug.HudSettingsFragment$$ViewInjector.Anon3(target));
        target.mapTilt = (android.widget.SeekBar) finder.findRequiredView(source, com.navdy.client.R.id.mapTilt, "field 'mapTilt'");
        target.mapZoomLevel = (android.widget.SeekBar) finder.findRequiredView(source, com.navdy.client.R.id.mapZoomLevel, "field 'mapZoomLevel'");
        target.zoomLevelTextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.zoomLevelTextView, "field 'zoomLevelTextView'");
        target.tiltLevelTextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.tiltLevelTextView, "field 'tiltLevelTextView'");
        android.view.View view4 = finder.findRequiredView(source, com.navdy.client.R.id.enable_video_loop, "field 'enableVideoLoop' and method 'onClick'");
        target.enableVideoLoop = (android.widget.CheckBox) view4;
        view4.setOnClickListener(new com.navdy.client.debug.HudSettingsFragment$$ViewInjector.Anon4(target));
        target.voiceSearchLabel = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.hud_voice_search_label, "field 'voiceSearchLabel'");
        target.enableSpecialVoiceSearch = (android.widget.CheckBox) finder.findRequiredView(source, com.navdy.client.R.id.use_special_hud_voice_commands, "field 'enableSpecialVoiceSearch'");
        finder.findRequiredView(source, com.navdy.client.R.id.resetMapZoom, "method 'onClick'").setOnClickListener(new com.navdy.client.debug.HudSettingsFragment$$ViewInjector.Anon5(target));
    }

    public static void reset(com.navdy.client.debug.HudSettingsFragment target) {
        target.autoBrightness = null;
        target.brightness = null;
        target.brightnessTextView = null;
        target.ledBrightness = null;
        target.ledBrightnessTextView = null;
        target.enableGesture = null;
        target.enablePreview = null;
        target.mapTilt = null;
        target.mapZoomLevel = null;
        target.zoomLevelTextView = null;
        target.tiltLevelTextView = null;
        target.enableVideoLoop = null;
        target.voiceSearchLabel = null;
        target.enableSpecialVoiceSearch = null;
    }
}
