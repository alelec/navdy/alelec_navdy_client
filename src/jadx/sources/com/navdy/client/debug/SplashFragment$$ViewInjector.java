package com.navdy.client.debug;

public class SplashFragment$$ViewInjector {

    /* compiled from: SplashFragment$$ViewInjector */
    static class Anon1 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.SplashFragment val$target;

        Anon1(com.navdy.client.debug.SplashFragment splashFragment) {
            this.val$target = splashFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onConnectClick();
        }
    }

    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.SplashFragment target, java.lang.Object source) {
        finder.findRequiredView(source, com.navdy.client.R.id.splash_connect, "method 'onConnectClick'").setOnClickListener(new com.navdy.client.debug.SplashFragment$$ViewInjector.Anon1(target));
    }

    public static void reset(com.navdy.client.debug.SplashFragment target) {
    }
}
