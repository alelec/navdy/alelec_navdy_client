package com.navdy.client.debug.devicepicker;

public class DeviceAdapter extends android.widget.ArrayAdapter<com.navdy.client.debug.devicepicker.Device> {
    static int[] button_ids;
    static com.navdy.client.debug.devicepicker.PreferredConnectionType[] types = com.navdy.client.debug.devicepicker.PreferredConnectionType.values();

    private static class ViewHolder {
        private android.view.View.OnClickListener clickListener;
        private android.widget.Button[] connectionTypes;
        private com.navdy.client.debug.devicepicker.Device device;
        private android.widget.TextView key;

        class Anon1 implements android.view.View.OnClickListener {
            Anon1() {
            }

            public void onClick(android.view.View v) {
                com.navdy.client.debug.devicepicker.DeviceAdapter.ViewHolder.this.device.setPreferredType((com.navdy.client.debug.devicepicker.PreferredConnectionType) v.getTag());
            }
        }

        private ViewHolder(android.view.View view) {
            this.connectionTypes = new android.widget.Button[3];
            this.clickListener = new com.navdy.client.debug.devicepicker.DeviceAdapter.ViewHolder.Anon1();
            this.key = (android.widget.TextView) view.findViewById(com.navdy.client.R.id.key);
            if (com.navdy.client.debug.devicepicker.DeviceAdapter.button_ids == null) {
                com.navdy.client.debug.devicepicker.DeviceAdapter.lookupIds();
            }
            for (int i = 0; i < com.navdy.client.debug.devicepicker.DeviceAdapter.button_ids.length; i++) {
                android.widget.Button button = (android.widget.Button) view.findViewById(com.navdy.client.debug.devicepicker.DeviceAdapter.button_ids[i]);
                button.setOnClickListener(this.clickListener);
                button.setTag(com.navdy.client.debug.devicepicker.DeviceAdapter.types[i]);
                this.connectionTypes[i] = button;
            }
        }

        private void setDevice(com.navdy.client.debug.devicepicker.Device device2) {
            boolean z;
            this.device = device2;
            this.key.setText(device2.toString());
            com.navdy.service.library.device.connection.ConnectionInfo[] connectionTypes2 = device2.getConnectionTypes();
            for (int i = 0; i < connectionTypes2.length; i++) {
                android.widget.Button button = this.connectionTypes[i];
                button.setVisibility(connectionTypes2[i] != null ? 0 : 8);
                android.widget.Checkable checkable = (android.widget.Checkable) button;
                if (i == device2.getPreferredType()) {
                    z = true;
                } else {
                    z = false;
                }
                checkable.setChecked(z);
            }
        }
    }

    static void lookupIds() {
        com.navdy.client.debug.devicepicker.PreferredConnectionType[] preferredConnectionTypeArr;
        java.lang.Class clazz = com.navdy.client.R.id.class;
        button_ids = new int[types.length];
        for (com.navdy.client.debug.devicepicker.PreferredConnectionType type : types) {
            try {
                button_ids[type.getPriority()] = clazz.getField("connection_type_" + type.name().toLowerCase(java.util.Locale.US)).getInt(null);
            } catch (java.lang.NoSuchFieldException e) {
                e.printStackTrace();
            } catch (java.lang.IllegalAccessException e2) {
                e2.printStackTrace();
            }
        }
    }

    public DeviceAdapter(android.content.Context context, java.util.List<com.navdy.client.debug.devicepicker.Device> objects) {
        super(context, com.navdy.client.R.layout.list_item_device, objects);
    }

    public android.view.View getView(int pos, android.view.View convertView, android.view.ViewGroup parent) {
        com.navdy.client.debug.devicepicker.DeviceAdapter.ViewHolder holder;
        if (convertView == null) {
            convertView = android.view.LayoutInflater.from(getContext()).inflate(com.navdy.client.R.layout.list_item_device, null);
            holder = new com.navdy.client.debug.devicepicker.DeviceAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (com.navdy.client.debug.devicepicker.DeviceAdapter.ViewHolder) convertView.getTag();
        }
        holder.setDevice((com.navdy.client.debug.devicepicker.Device) getItem(pos));
        return convertView;
    }
}
