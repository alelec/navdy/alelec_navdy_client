package com.navdy.client.debug;

class PlaceDebugAutocomplete {
    public java.lang.CharSequence description;
    public java.lang.CharSequence placeId;

    PlaceDebugAutocomplete(java.lang.CharSequence placeId2, java.lang.CharSequence description2) {
        this.placeId = placeId2;
        this.description = description2;
    }

    public java.lang.String toString() {
        return this.description.toString();
    }
}
