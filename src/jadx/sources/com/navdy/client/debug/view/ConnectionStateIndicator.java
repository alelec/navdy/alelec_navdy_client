package com.navdy.client.debug.view;

public class ConnectionStateIndicator extends android.widget.ImageView {
    private static final int[] CONNECTED = {com.navdy.client.R.attr.connected};
    private static final int[] CONNECTING = {com.navdy.client.R.attr.connecting};
    private static final int[] DISCONNECTED = {com.navdy.client.R.attr.disconnected};
    private static final int[] USING_BLUETOOTH = {com.navdy.client.R.attr.using_bluetooth};
    private com.navdy.service.library.device.connection.Connection.Status status;
    private boolean usingBluetooth;

    public ConnectionStateIndicator(android.content.Context context) {
        this(context, null);
    }

    public ConnectionStateIndicator(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ConnectionStateIndicator(android.content.Context context, android.util.AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.usingBluetooth = false;
        this.status = com.navdy.service.library.device.connection.Connection.Status.DISCONNECTED;
    }

    public int[] onCreateDrawableState(int extraSpace) {
        int[] drawableState = super.onCreateDrawableState(extraSpace + 2);
        if (this.usingBluetooth) {
            mergeDrawableStates(drawableState, USING_BLUETOOTH);
        }
        if (this.status != null) {
            switch (this.status) {
                case DISCONNECTED:
                    mergeDrawableStates(drawableState, DISCONNECTED);
                    break;
                case CONNECTING:
                    mergeDrawableStates(drawableState, CONNECTING);
                    break;
                case CONNECTED:
                    mergeDrawableStates(drawableState, CONNECTED);
                    break;
            }
        } else {
            mergeDrawableStates(drawableState, DISCONNECTED);
        }
        return drawableState;
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        android.graphics.drawable.Drawable drawable = getDrawable();
        if (drawable != null && (drawable.getCurrent() instanceof android.graphics.drawable.AnimationDrawable)) {
            ((android.graphics.drawable.AnimationDrawable) drawable.getCurrent()).start();
        }
    }

    public void setConnectionType(com.navdy.service.library.device.connection.ConnectionType type) {
        boolean bluetooth = type == com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF;
        if (this.usingBluetooth != bluetooth) {
            this.usingBluetooth = bluetooth;
            refreshDrawableState();
        }
    }

    public void setConnectionStatus(com.navdy.service.library.device.connection.Connection.Status status2) {
        if (this.status != status2) {
            this.status = status2;
            refreshDrawableState();
        }
    }
}
