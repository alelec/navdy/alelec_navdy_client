package com.navdy.client.debug;

public class RemoteAddressPickerFragment extends android.app.ListFragment {
    private static final double MI_TO_METERS = 1690.34d;
    public static final java.lang.String PREFS_FILE_USER_HISTORY = "UserHistory";
    public static final java.lang.String PREFS_KEY_RECENT_SEARCHES = "RecentSearches";
    private static final int SEARCH_RADIUS_METERS = 169034;
    protected static int fragmentTitle = com.navdy.client.R.string.title_address_search;
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.RemoteAddressPickerFragment.class);
    private android.app.Activity activity;
    @butterknife.InjectView(2131755628)
    android.widget.Button mButtonSearch;
    protected java.util.ArrayList<java.lang.String> mDestinationLabels;
    @butterknife.InjectView(2131755627)
    org.droidparts.widget.ClearableEditText mEditTextDestinationQuery;
    protected com.navdy.client.debug.navigation.NavigationManager mNavigationManager;
    protected com.google.gson.Gson mObjectMapper;
    protected com.navdy.client.debug.models.RecentSearches mRecentSearches;
    protected java.util.ArrayList<com.navdy.service.library.events.places.PlacesSearchResult> mSearchResults;
    protected android.content.SharedPreferences mSharedPrefs;

    class Anon1 implements android.widget.TextView.OnEditorActionListener {
        Anon1() {
        }

        public boolean onEditorAction(android.widget.TextView v, int actionId, android.view.KeyEvent event) {
            if (actionId != 3) {
                return false;
            }
            com.navdy.client.debug.RemoteAddressPickerFragment.this.onSearchClicked();
            return true;
        }
    }

    class Anon2 implements org.droidparts.widget.ClearableEditText.Listener {
        Anon2() {
        }

        public void didClearText() {
            com.navdy.client.debug.RemoteAddressPickerFragment.this.showKeyboard();
        }
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        android.content.Context context = getActivity().getApplicationContext();
        this.mObjectMapper = new com.google.gson.Gson();
        this.mSharedPrefs = context.getSharedPreferences(PREFS_FILE_USER_HISTORY, 0);
        this.mRecentSearches = loadRecentSearches();
        this.mDestinationLabels = new java.util.ArrayList<>();
        setListAdapter(new android.widget.ArrayAdapter(getActivity(), 17367043, 16908308, this.mDestinationLabels));
        addDestinations(this.mRecentSearches.getResults());
        this.mNavigationManager = new com.navdy.client.debug.navigation.HUDNavigationManager();
    }

    public void onAttach(android.app.Activity activity2) {
        super.onAttach(activity2);
        this.activity = activity2;
    }

    public void onDetach() {
        super.onDetach();
        this.activity = null;
    }

    private boolean isAttached() {
        return this.activity != null && !this.activity.isFinishing();
    }

    protected void addDestinations(java.util.List<com.navdy.service.library.events.places.PlacesSearchResult> searchResults) {
        this.mDestinationLabels.clear();
        for (com.navdy.service.library.events.places.PlacesSearchResult result : searchResults) {
            this.mDestinationLabels.add(result.label);
        }
        ((android.widget.ArrayAdapter) getListAdapter()).notifyDataSetChanged();
    }

    protected com.navdy.client.debug.models.RecentSearches loadRecentSearches() {
        try {
            java.lang.String json = this.mSharedPrefs.getString(PREFS_KEY_RECENT_SEARCHES, "");
            if (json.length() > 0) {
                return (com.navdy.client.debug.models.RecentSearches) this.mObjectMapper.fromJson(json, com.navdy.client.debug.models.RecentSearches.class);
            }
        } catch (com.google.gson.JsonParseException e) {
            sLogger.e("Unable to read recent searches", e);
        }
        return new com.navdy.client.debug.models.RecentSearches();
    }

    protected void saveRecentSearches() {
        this.mSharedPrefs.edit().putString(PREFS_KEY_RECENT_SEARCHES, this.mObjectMapper.toJson((java.lang.Object) this.mRecentSearches)).apply();
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_nav_address_picker, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        this.mEditTextDestinationQuery.setOnEditorActionListener(new com.navdy.client.debug.RemoteAddressPickerFragment.Anon1());
        this.mEditTextDestinationQuery.setListener(new com.navdy.client.debug.RemoteAddressPickerFragment.Anon2());
        return rootView;
    }

    public void onResume() {
        super.onResume();
        refreshUI();
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    public void onPause() {
        com.navdy.client.app.framework.util.BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    public void onDestroy() {
        saveRecentSearches();
        super.onDestroy();
    }

    public void refreshUI() {
        if (this.mDestinationLabels.size() == 0) {
            showKeyboard();
        }
    }

    public void onListItemClick(android.widget.ListView l, android.view.View v, int position, long id) {
        com.navdy.service.library.events.places.PlacesSearchResult destination;
        super.onListItemClick(l, v, position, id);
        sLogger.d("locationClick: " + ((java.lang.String) getListView().getItemAtPosition(position)));
        if (this.mSearchResults != null) {
            destination = (com.navdy.service.library.events.places.PlacesSearchResult) this.mSearchResults.get(position);
        } else {
            destination = (com.navdy.service.library.events.places.PlacesSearchResult) this.mRecentSearches.getResults().get(position);
        }
        com.navdy.service.library.events.location.Coordinate navigationPosition = destination.navigationPosition;
        if (navigationPosition != null) {
            showError("NavPosition: " + navigationPosition.latitude + org.droidparts.contract.SQL.DDL.SEPARATOR + navigationPosition.longitude);
        } else if (destination.destinationLocation != null) {
            navigationPosition = destination.destinationLocation;
            showError("DestPosition: " + navigationPosition.latitude + org.droidparts.contract.SQL.DDL.SEPARATOR + navigationPosition.longitude);
        } else {
            showError("Unable to find coordinate.");
            return;
        }
        this.mRecentSearches.add(destination);
        hideKeyboard();
        processSelectedLocation(navigationPosition, (java.lang.String) this.mDestinationLabels.get(position), null);
    }

    protected void processSelectedLocation(com.navdy.service.library.events.location.Coordinate location, java.lang.String locationLabel, java.lang.String streetAddress) {
        if (this.mNavigationManager != null) {
            this.mNavigationManager.startRouteRequest(location, locationLabel, streetAddress, null);
        }
    }

    @butterknife.OnClick({2131755628})
    public void onSearchClicked() {
        boolean postSuccess;
        java.lang.String queryText = this.mEditTextDestinationQuery.getText().toString();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(queryText)) {
            addDestinations(new java.util.ArrayList(0));
            if (!com.navdy.client.app.framework.DeviceConnection.isConnected()) {
                showError("Device not connected.");
                postSuccess = false;
            } else {
                postSuccess = com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.places.PlacesSearchRequest.Builder().searchQuery(queryText).searchArea(com.navdy.client.app.framework.search.NavdySearch.SEARCH_AREA_VALUE).maxResults(java.lang.Integer.valueOf(30)).build());
            }
            if (!postSuccess) {
                showError("Unable to post search");
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onPlacesSearchResponse(com.navdy.service.library.events.places.PlacesSearchResponse response) {
        if (response.status != com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
            java.lang.String errorString = "Error: " + response.status + " " + response.statusDetail;
            sLogger.e(errorString);
            showError(errorString);
            return;
        }
        java.util.List<com.navdy.service.library.events.places.PlacesSearchResult> results = response.results;
        if (results == null || results.size() == 0) {
            showError("No results");
            sLogger.d("no results");
            this.mEditTextDestinationQuery.requestFocus();
            return;
        }
        sLogger.d("returned results: " + results.size());
        hideKeyboard();
        addDestinations(results);
        this.mSearchResults = new java.util.ArrayList<>(results);
    }

    protected void hideKeyboard() {
        android.view.inputmethod.InputMethodManager imm = (android.view.inputmethod.InputMethodManager) getActivity().getSystemService("input_method");
        if (imm != null) {
            imm.hideSoftInputFromWindow(this.mEditTextDestinationQuery.getWindowToken(), 0);
        }
    }

    protected void showKeyboard() {
        ((android.view.inputmethod.InputMethodManager) getActivity().getSystemService("input_method")).showSoftInput(this.mEditTextDestinationQuery, 1);
    }

    protected void showError(java.lang.String error) {
        android.widget.Toast.makeText(getActivity(), error, 0).show();
    }

    @com.squareup.otto.Subscribe
    public void onNavigationRouteResponse(com.navdy.service.library.events.navigation.NavigationRouteResponse event) {
        if (!isAttached()) {
            sLogger.v("isAttached: false");
            return;
        }
        sLogger.e(event.status + com.navdy.client.app.framework.util.CrashlyticsAppender.SEPARATOR + event.statusDetail);
        if (event.status != com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
            sLogger.e("Unable to get routes.");
            return;
        }
        java.util.List<com.navdy.service.library.events.navigation.NavigationRouteResult> results = event.results;
        if (results == null || results.size() == 0) {
            sLogger.e("No results.");
            return;
        }
        com.navdy.client.debug.RemoteNavControlFragment navControlFragment = com.navdy.client.debug.RemoteNavControlFragment.newInstance((com.navdy.service.library.events.navigation.NavigationRouteResult) results.get(0), event.label);
        android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(com.navdy.client.R.id.container, navControlFragment);
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null);
        ft.commit();
    }

    private android.app.ActionBar getActionBar() {
        return getActivity().getActionBar();
    }
}
