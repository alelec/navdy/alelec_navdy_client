package com.navdy.client.debug;

public class SubmitTicketFragment$$ViewInjector {

    /* compiled from: SubmitTicketFragment$$ViewInjector */
    static class Anon1 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.SubmitTicketFragment val$target;

        Anon1(com.navdy.client.debug.SubmitTicketFragment submitTicketFragment) {
            this.val$target = submitTicketFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onSubmit(p0);
        }
    }

    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.SubmitTicketFragment target, java.lang.Object source) {
        target.mTitle = (android.widget.EditText) finder.findRequiredView(source, com.navdy.client.R.id.txt_title, "field 'mTitle'");
        target.mDescription = (android.widget.EditText) finder.findRequiredView(source, com.navdy.client.R.id.txt_description, "field 'mDescription'");
        target.spinner = (android.widget.Spinner) finder.findRequiredView(source, com.navdy.client.R.id.project_spinner, "field 'spinner'");
        finder.findRequiredView(source, com.navdy.client.R.id.btn_submit, "method 'onSubmit'").setOnClickListener(new com.navdy.client.debug.SubmitTicketFragment$$ViewInjector.Anon1(target));
    }

    public static void reset(com.navdy.client.debug.SubmitTicketFragment target) {
        target.mTitle = null;
        target.mDescription = null;
        target.spinner = null;
    }
}
