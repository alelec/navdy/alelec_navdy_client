package com.navdy.client.debug;

public class MainDebugActivity extends com.navdy.client.debug.common.BaseDebugActivity {
    public static final java.lang.String EXTRA_OPEN_DIAL_SIM = "openDialSimulator";
    public static final java.lang.String MAIN_TAG = "main";
    public static final java.lang.String SPLASH_TAG = "splash";
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.AppInstance.class);
    protected com.navdy.client.app.framework.AppInstance mAppInstance;

    public static class MainFragment extends android.app.ListFragment {
        private static final android.content.IntentFilter DATETIME_CHANGE_INTENT_FILTER = new android.content.IntentFilter("android.intent.action.DATE_CHANGED");
        private com.navdy.client.debug.ConnectionStatusUpdater mConnectionStatus;
        private java.util.List<com.navdy.client.debug.MainDebugActivity.MainFragment.ListItem> mListItems;

        class Anon1 implements android.content.DialogInterface.OnClickListener {
            Anon1() {
            }

            public void onClick(android.content.DialogInterface dialog, int id) {
                dialog.cancel();
            }
        }

        class Anon2 implements android.content.DialogInterface.OnClickListener {
            final /* synthetic */ android.widget.EditText val$userInput;

            Anon2(android.widget.EditText editText) {
                this.val$userInput = editText;
            }

            public void onClick(android.content.DialogInterface dialog, int id) {
                com.navdy.client.app.framework.servicehandler.RecordingServiceManager.getInstance().sendStartDriveRecordingEvent(this.val$userInput.getText().toString());
            }
        }

        enum ListItem {
            SEARCH(com.navdy.client.R.string.main_search, com.navdy.client.debug.GoogleAddressPickerFragment.class),
            RECENTS(com.navdy.client.R.string.main_recents),
            SETTINGS(com.navdy.client.R.string.main_settings, com.navdy.client.debug.SettingsFragment.class),
            GESTURE(com.navdy.client.R.string.main_gesture, com.navdy.client.debug.gesture.GestureControlFragment.class),
            DEBUG(com.navdy.client.R.string.main_debug, com.navdy.client.debug.DebugActionsFragment.class),
            TICKET(com.navdy.client.R.string.main_submit_ticket, com.navdy.client.debug.SubmitTicketFragment.class),
            NAVDY_DIAL(com.navdy.client.R.string.main_navdy_dial, com.navdy.client.debug.NavdyDialFragment.class),
            RECORD_ROUTE(com.navdy.client.R.string.main_record_drive),
            STOP_RECORD_ROUTE(com.navdy.client.R.string.main_stop_record_drive),
            UPLOAD_TRIP_DATA(com.navdy.client.R.string.upload_trip_data),
            MUSIC(com.navdy.client.R.string.main_music, com.navdy.client.debug.music.MusicControlFragment.class);
            
            private final int mResId;
            private java.lang.String mString;
            private java.lang.Class<? extends android.app.Fragment> targetFragment;

            private ListItem(int item) {
                this(r2, r3, item, null);
            }

            private ListItem(int mResId2, java.lang.Class<? extends android.app.Fragment> targetFragment2) {
                this.mResId = mResId2;
                this.targetFragment = targetFragment2;
            }

            public java.lang.Class<? extends android.app.Fragment> getTargetFragment() {
                return this.targetFragment;
            }

            void bindResource(android.content.res.Resources r) {
                this.mString = r.getString(this.mResId);
            }

            public java.lang.String toString() {
                return this.mString;
            }
        }

        static {
            DATETIME_CHANGE_INTENT_FILTER.addAction("android.intent.action.TIME_SET");
            DATETIME_CHANGE_INTENT_FILTER.addAction("android.intent.action.TIMEZONE_CHANGED");
        }

        public void onCreate(android.os.Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            this.mListItems = new java.util.ArrayList(java.util.Arrays.asList(com.navdy.client.debug.MainDebugActivity.MainFragment.ListItem.values()));
            for (com.navdy.client.debug.MainDebugActivity.MainFragment.ListItem item : this.mListItems) {
                item.bindResource(getResources());
            }
            setListAdapter(new android.widget.ArrayAdapter(getActivity(), com.navdy.client.R.layout.list_item_main, 16908308, com.navdy.client.debug.MainDebugActivity.MainFragment.ListItem.values()));
        }

        public android.view.View onCreateView(@android.support.annotation.NonNull android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
            android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_main, container, false);
            butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
            this.mConnectionStatus = new com.navdy.client.debug.ConnectionStatusUpdater(rootView);
            return rootView;
        }

        public void onPause() {
            this.mConnectionStatus.onPause();
            super.onPause();
        }

        public void onResume() {
            super.onResume();
            if (getActivity().getActionBar() != null) {
                getActivity().getActionBar().setTitle(com.navdy.client.R.string.title_activity_main);
            }
            this.mConnectionStatus.onResume();
            if (com.navdy.client.ota.OTAUpdateService.isLaunchedByOtaUpdateService(getActivity().getIntent())) {
                com.navdy.client.debug.util.FragmentHelper.pushFullScreenFragment(getFragmentManager(), com.navdy.client.debug.SettingsFragment.class);
            }
        }

        public void onListItemClick(android.widget.ListView l, android.view.View v, int position, long id) {
            super.onListItemClick(l, v, position, id);
            l.setItemChecked(position, false);
            com.navdy.client.debug.MainDebugActivity.MainFragment.ListItem clickedItem = (com.navdy.client.debug.MainDebugActivity.MainFragment.ListItem) this.mListItems.get(position);
            switch (clickedItem) {
                case SEARCH:
                    pushNavigationSearchFragment();
                    return;
                case RECORD_ROUTE:
                    recordRoute();
                    return;
                case STOP_RECORD_ROUTE:
                    stopRecordingRoute();
                    return;
                case UPLOAD_TRIP_DATA:
                    com.navdy.client.app.framework.suggestion.DestinationSuggestionService.uploadTripDatabase(getActivity(), false);
                    android.widget.Toast.makeText(getActivity(), "Thanks :) You are awesome !!", 1).show();
                    return;
                default:
                    java.lang.Class<? extends android.app.Fragment> nextFragment = clickedItem.getTargetFragment();
                    if (nextFragment != null) {
                        com.navdy.client.debug.util.FragmentHelper.pushFullScreenFragment(getFragmentManager(), nextFragment);
                        return;
                    }
                    return;
            }
        }

        private void recordRoute() {
            android.view.View promptsView = android.view.LayoutInflater.from(getActivity()).inflate(com.navdy.client.R.layout.record_route_name_prompt, null);
            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
            alertDialogBuilder.setView(promptsView);
            alertDialogBuilder.setPositiveButton("Go", new com.navdy.client.debug.MainDebugActivity.MainFragment.Anon2((android.widget.EditText) promptsView.findViewById(com.navdy.client.R.id.drive_name_input))).setNegativeButton("Cancel", new com.navdy.client.debug.MainDebugActivity.MainFragment.Anon1());
            alertDialogBuilder.create().show();
        }

        private void stopRecordingRoute() {
            com.navdy.client.app.framework.servicehandler.RecordingServiceManager.getInstance().sendStopDriveRecordingEvent();
        }

        private void pushNavigationSearchFragment() {
            com.navdy.client.debug.util.FragmentHelper.pushFullScreenFragment(getFragmentManager(), com.navdy.client.debug.GoogleAddressPickerFragment.class);
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        boolean hasConnection;
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        android.preference.PreferenceManager.setDefaultValues(this, com.navdy.client.R.xml.preferences, false);
        setContentView(com.navdy.client.R.layout.activity_main);
        this.mAppInstance = com.navdy.client.app.framework.AppInstance.getInstance();
        if (this.mAppInstance.getRemoteDevice() != null) {
            hasConnection = true;
        } else {
            hasConnection = false;
        }
        boolean launchedFromOtaNotification = com.navdy.client.ota.OTAUpdateService.isLaunchedByOtaUpdateService(getIntent());
        if (hasConnection || launchedFromOtaNotification) {
            new com.navdy.client.debug.MainDebugActivity.MainFragment();
        } else {
            new com.navdy.client.debug.SplashFragment();
        }
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().add(com.navdy.client.R.id.container, new com.navdy.client.debug.MainDebugActivity.MainFragment(), MAIN_TAG).commit();
            if (!com.navdy.client.app.NavdyApplication.isDeveloperBuild()) {
                checkForUpdates();
            }
        }
        if (getIntent().getBooleanExtra(EXTRA_OPEN_DIAL_SIM, false)) {
            com.navdy.client.debug.util.FragmentHelper.pushFullScreenFragment(getFragmentManager(), com.navdy.client.debug.gesture.GestureControlFragment.class);
        }
    }

    public void onResume() {
        super.onResume();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.Settings.DEBUG);
    }

    public void checkForUpdates(net.hockeyapp.android.UpdateManagerListener listener) {
        net.hockeyapp.android.UpdateManager.register((android.app.Activity) this, com.navdy.client.app.framework.util.CredentialsUtils.getCredentials(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.metadata_hockey_app_credentials)), listener, true);
    }

    public void checkForUpdates() {
        checkForUpdates(null);
    }
}
