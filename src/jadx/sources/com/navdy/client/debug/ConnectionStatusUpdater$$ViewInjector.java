package com.navdy.client.debug;

public class ConnectionStatusUpdater$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.ConnectionStatusUpdater target, java.lang.Object source) {
        target.mTextViewCurrentDevice = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.current_device, "field 'mTextViewCurrentDevice'");
        target.mConnectionIndicator = (com.navdy.client.debug.view.ConnectionStateIndicator) finder.findRequiredView(source, com.navdy.client.R.id.connectionStateIndicator, "field 'mConnectionIndicator'");
    }

    public static void reset(com.navdy.client.debug.ConnectionStatusUpdater target) {
        target.mTextViewCurrentDevice = null;
        target.mConnectionIndicator = null;
    }
}
