package com.navdy.client.debug;

public class FileTransferTestFragment$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.FileTransferTestFragment target, java.lang.Object source) {
        target.sizeSelection = (android.widget.RadioGroup) finder.findRequiredView(source, com.navdy.client.R.id.transfer_size_select, "field 'sizeSelection'");
        target.transferButton = (android.widget.Button) finder.findRequiredView(source, com.navdy.client.R.id.transfer_button, "field 'transferButton'");
        target.transferProgress = (android.widget.ProgressBar) finder.findRequiredView(source, com.navdy.client.R.id.transfer_progress, "field 'transferProgress'");
        target.progressContainer = (android.widget.LinearLayout) finder.findRequiredView(source, com.navdy.client.R.id.progress_container, "field 'progressContainer'");
        target.transferMessage = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.transfer_message, "field 'transferMessage'");
    }

    public static void reset(com.navdy.client.debug.FileTransferTestFragment target) {
        target.sizeSelection = null;
        target.transferButton = null;
        target.transferProgress = null;
        target.progressContainer = null;
        target.transferMessage = null;
    }
}
