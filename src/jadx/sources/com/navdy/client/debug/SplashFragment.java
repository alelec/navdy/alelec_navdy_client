package com.navdy.client.debug;

public class SplashFragment extends android.app.Fragment {
    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_splash, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        return rootView;
    }

    @butterknife.OnClick({2131755658})
    void onConnectClick() {
        com.navdy.client.debug.util.FragmentHelper.pushFullScreenFragment(getFragmentManager(), com.navdy.client.debug.DevicePickerFragment.class);
    }
}
