package com.navdy.client.debug;

public class DebugActionsFragment$$ViewInjector {

    /* compiled from: DebugActionsFragment$$ViewInjector */
    static class Anon1 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.DebugActionsFragment val$target;

        Anon1(com.navdy.client.debug.DebugActionsFragment debugActionsFragment) {
            this.val$target = debugActionsFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onShowScreenClicked(p0);
        }
    }

    /* compiled from: DebugActionsFragment$$ViewInjector */
    static class Anon2 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.DebugActionsFragment val$target;

        Anon2(com.navdy.client.debug.DebugActionsFragment debugActionsFragment) {
            this.val$target = debugActionsFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onShowScreenClicked(p0);
        }
    }

    /* compiled from: DebugActionsFragment$$ViewInjector */
    static class Anon3 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.DebugActionsFragment val$target;

        Anon3(com.navdy.client.debug.DebugActionsFragment debugActionsFragment) {
            this.val$target = debugActionsFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onShowScreenClicked(p0);
        }
    }

    /* compiled from: DebugActionsFragment$$ViewInjector */
    static class Anon4 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.DebugActionsFragment val$target;

        Anon4(com.navdy.client.debug.DebugActionsFragment debugActionsFragment) {
            this.val$target = debugActionsFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onShowScreenClicked(p0);
        }
    }

    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.DebugActionsFragment target, java.lang.Object source) {
        target.mTextViewCurrentSimSpeed = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.test_textview_current_sim_speed, "field 'mTextViewCurrentSimSpeed'");
        finder.findRequiredView(source, com.navdy.client.R.id.dash_screen, "method 'onShowScreenClicked'").setOnClickListener(new com.navdy.client.debug.DebugActionsFragment$$ViewInjector.Anon1(target));
        finder.findRequiredView(source, com.navdy.client.R.id.menu_screen, "method 'onShowScreenClicked'").setOnClickListener(new com.navdy.client.debug.DebugActionsFragment$$ViewInjector.Anon2(target));
        finder.findRequiredView(source, com.navdy.client.R.id.context_menu_screen, "method 'onShowScreenClicked'").setOnClickListener(new com.navdy.client.debug.DebugActionsFragment$$ViewInjector.Anon3(target));
        finder.findRequiredView(source, com.navdy.client.R.id.hybrid_map_screen, "method 'onShowScreenClicked'").setOnClickListener(new com.navdy.client.debug.DebugActionsFragment$$ViewInjector.Anon4(target));
    }

    public static void reset(com.navdy.client.debug.DebugActionsFragment target) {
        target.mTextViewCurrentSimSpeed = null;
    }
}
