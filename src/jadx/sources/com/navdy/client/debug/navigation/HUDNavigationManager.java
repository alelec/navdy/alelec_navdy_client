package com.navdy.client.debug.navigation;

public class HUDNavigationManager extends com.navdy.client.debug.navigation.NavigationManager {
    private static final boolean VERBOSE = false;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.navigation.HUDNavigationManager.class);

    public boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String label, java.lang.String streetAddress, java.lang.String destinationId, com.navdy.service.library.events.destination.Destination.FavoriteType destinationType, com.navdy.service.library.events.location.Coordinate display) {
        return startRouteRequest(coordinate, label, null, streetAddress, destinationId, destinationType, display);
    }

    public boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String label, java.util.List<com.navdy.service.library.events.location.Coordinate> waypoints, java.lang.String streetAddress, java.lang.String destinationId, com.navdy.service.library.events.destination.Destination.FavoriteType destinationType, com.navdy.service.library.events.location.Coordinate display) {
        return startRouteRequest(coordinate, label, waypoints, streetAddress, destinationId, destinationType, display, java.util.UUID.randomUUID().toString());
    }

    public boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String label, java.util.List<com.navdy.service.library.events.location.Coordinate> waypoints, java.lang.String streetAddress, java.lang.String destinationId, com.navdy.service.library.events.destination.Destination.FavoriteType destinationType, com.navdy.service.library.events.location.Coordinate display, java.lang.String requestId) {
        return startRouteRequest(coordinate, label, waypoints, streetAddress, destinationId, destinationType, display, requestId, false);
    }

    public boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String label, java.util.List<com.navdy.service.library.events.location.Coordinate> waypoints, java.lang.String streetAddress, java.lang.String destinationId, com.navdy.service.library.events.destination.Destination.FavoriteType destinationType, com.navdy.service.library.events.location.Coordinate display, java.lang.String requestId, boolean useStreetAddress) {
        return startRouteRequest(coordinate, label, waypoints, streetAddress, destinationId, destinationType, display, requestId, useStreetAddress, false, null);
    }

    public boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String label, java.util.List<com.navdy.service.library.events.location.Coordinate> waypoints, java.lang.String streetAddress, java.lang.String destinationId, com.navdy.service.library.events.destination.Destination.FavoriteType destinationType, com.navdy.service.library.events.location.Coordinate display, java.lang.String requestId, boolean useStreetAddress, boolean initiatedOnHud) {
        return startRouteRequest(coordinate, label, waypoints, streetAddress, destinationId, destinationType, display, requestId, useStreetAddress, false, null);
    }

    public boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String label, java.util.List<com.navdy.service.library.events.location.Coordinate> waypoints, java.lang.String streetAddress, java.lang.String destinationId, com.navdy.service.library.events.destination.Destination.FavoriteType destinationType, com.navdy.service.library.events.location.Coordinate display, java.lang.String requestId, boolean useStreetAddress, boolean initiatedOnHud, com.navdy.service.library.events.destination.Destination destination) {
        return com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder().destination(coordinate).destination_identifier(destinationId).destinationType(destinationType).label(label).waypoints(waypoints).streetAddress(streetAddress).geoCodeStreetAddress(java.lang.Boolean.valueOf(useStreetAddress)).cancelCurrent(java.lang.Boolean.valueOf(true)).destinationDisplay(display).requestId(requestId).requestDestination(destination).originDisplay(java.lang.Boolean.valueOf(initiatedOnHud)).build());
    }
}
