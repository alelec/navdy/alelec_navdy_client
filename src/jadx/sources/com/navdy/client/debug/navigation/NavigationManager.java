package com.navdy.client.debug.navigation;

public abstract class NavigationManager {
    public abstract boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String str, java.lang.String str2, java.lang.String str3, com.navdy.service.library.events.destination.Destination.FavoriteType favoriteType, com.navdy.service.library.events.location.Coordinate coordinate2);

    public abstract boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String str, java.util.List<com.navdy.service.library.events.location.Coordinate> list, java.lang.String str2, java.lang.String str3, com.navdy.service.library.events.destination.Destination.FavoriteType favoriteType, com.navdy.service.library.events.location.Coordinate coordinate2);

    public abstract boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String str, java.util.List<com.navdy.service.library.events.location.Coordinate> list, java.lang.String str2, java.lang.String str3, com.navdy.service.library.events.destination.Destination.FavoriteType favoriteType, com.navdy.service.library.events.location.Coordinate coordinate2, java.lang.String str4);

    public abstract boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String str, java.util.List<com.navdy.service.library.events.location.Coordinate> list, java.lang.String str2, java.lang.String str3, com.navdy.service.library.events.destination.Destination.FavoriteType favoriteType, com.navdy.service.library.events.location.Coordinate coordinate2, java.lang.String str4, boolean z);

    public abstract boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String str, java.util.List<com.navdy.service.library.events.location.Coordinate> list, java.lang.String str2, java.lang.String str3, com.navdy.service.library.events.destination.Destination.FavoriteType favoriteType, com.navdy.service.library.events.location.Coordinate coordinate2, java.lang.String str4, boolean z, boolean z2);

    public abstract boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String str, java.util.List<com.navdy.service.library.events.location.Coordinate> list, java.lang.String str2, java.lang.String str3, com.navdy.service.library.events.destination.Destination.FavoriteType favoriteType, com.navdy.service.library.events.location.Coordinate coordinate2, java.lang.String str4, boolean z, boolean z2, com.navdy.service.library.events.destination.Destination destination);

    public boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String label, java.lang.String streetAddress, com.navdy.service.library.events.location.Coordinate display) {
        return startRouteRequest(coordinate, label, streetAddress, (java.lang.String) null, com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE, display);
    }

    public boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String label, java.util.List<com.navdy.service.library.events.location.Coordinate> waypoints, java.lang.String streetAddress, com.navdy.service.library.events.location.Coordinate display) {
        return startRouteRequest(coordinate, label, waypoints, streetAddress, (java.lang.String) null, com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE, display);
    }

    public boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String label, java.util.List<com.navdy.service.library.events.location.Coordinate> waypoints, java.lang.String streetAddress, com.navdy.service.library.events.location.Coordinate display, java.lang.String requestId) {
        return startRouteRequest(coordinate, label, waypoints, streetAddress, (java.lang.String) null, com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE, display, requestId);
    }

    public boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String label, java.util.List<com.navdy.service.library.events.location.Coordinate> waypoints, java.lang.String streetAddress, com.navdy.service.library.events.location.Coordinate display, java.lang.String requestId, boolean useStreetAddress) {
        return startRouteRequest(coordinate, label, waypoints, streetAddress, (java.lang.String) null, com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE, display, requestId, useStreetAddress);
    }

    public boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String label, java.util.List<com.navdy.service.library.events.location.Coordinate> waypoints, java.lang.String streetAddress, com.navdy.service.library.events.location.Coordinate display, java.lang.String requestId, boolean useStreetAddress, boolean initiatedOnHud) {
        return startRouteRequest(coordinate, label, waypoints, streetAddress, null, com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE, display, requestId, useStreetAddress, initiatedOnHud);
    }

    public boolean startRouteRequest(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String label, java.util.List<com.navdy.service.library.events.location.Coordinate> waypoints, java.lang.String streetAddress, com.navdy.service.library.events.location.Coordinate display, java.lang.String requestId, boolean useStreetAddress, boolean initiatedOnHud, com.navdy.service.library.events.destination.Destination destination) {
        return startRouteRequest(coordinate, label, waypoints, streetAddress, null, com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE, display, requestId, useStreetAddress, initiatedOnHud, destination);
    }
}
