package com.navdy.client.debug;

public class DevicePickerFragment extends android.app.ListFragment {
    protected com.navdy.client.app.framework.AppInstance mAppInstance;
    @butterknife.InjectView(2131755577)
    android.widget.Button mButtonAddDevice;
    protected com.navdy.service.library.device.connection.ConnectionInfo mCurrentDevice;
    protected com.navdy.service.library.device.RemoteDeviceRegistry.DeviceListUpdatedListener mDeviceListUpdateListener;
    protected java.util.ArrayList<com.navdy.client.debug.devicepicker.Device> mDevices;
    @butterknife.InjectView(2131755576)
    android.widget.EditText mEditTextNewDevice;
    protected com.navdy.service.library.device.RemoteDeviceRegistry mRemoteDeviceRegistry;
    @butterknife.InjectView(2131755575)
    android.widget.TextView mTextViewCurrentDevice;

    class Anon1 implements com.navdy.service.library.device.RemoteDeviceRegistry.DeviceListUpdatedListener {

        /* renamed from: com.navdy.client.debug.DevicePickerFragment$Anon1$Anon1 reason: collision with other inner class name */
        class C0072Anon1 implements java.lang.Runnable {
            final /* synthetic */ java.util.Set val$deviceList;

            C0072Anon1(java.util.Set set) {
                this.val$deviceList = set;
            }

            public void run() {
                com.navdy.client.debug.DevicePickerFragment.this.addConnectionInfo(this.val$deviceList);
                ((com.navdy.client.debug.devicepicker.DeviceAdapter) com.navdy.client.debug.DevicePickerFragment.this.getListAdapter()).notifyDataSetChanged();
            }
        }

        Anon1() {
        }

        public void onDeviceListChanged(java.util.Set<com.navdy.service.library.device.connection.ConnectionInfo> deviceList) {
            com.navdy.client.debug.DevicePickerFragment.this.getActivity().runOnUiThread(new com.navdy.client.debug.DevicePickerFragment.Anon1.C0072Anon1(deviceList));
        }
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mRemoteDeviceRegistry = com.navdy.service.library.device.RemoteDeviceRegistry.getInstance(getActivity().getApplicationContext());
        this.mAppInstance = com.navdy.client.app.framework.AppInstance.getInstance();
        this.mDevices = new java.util.ArrayList<>();
        addConnectionInfo(this.mRemoteDeviceRegistry.getKnownConnectionInfo());
        setListAdapter(new com.navdy.client.debug.devicepicker.DeviceAdapter(getActivity(), this.mDevices));
        this.mDeviceListUpdateListener = new com.navdy.client.debug.DevicePickerFragment.Anon1();
        this.mRemoteDeviceRegistry.addListener(this.mDeviceListUpdateListener);
    }

    protected void addConnectionInfo(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo, boolean updateIfFound) {
        com.navdy.client.debug.devicepicker.Device newDevice = new com.navdy.client.debug.devicepicker.Device(connectionInfo);
        int index = this.mDevices.indexOf(newDevice);
        if (index != -1) {
            ((com.navdy.client.debug.devicepicker.Device) this.mDevices.get(index)).add(connectionInfo, updateIfFound);
        } else {
            this.mDevices.add(newDevice);
        }
    }

    protected void addConnectionInfo(java.util.Set<com.navdy.service.library.device.connection.ConnectionInfo> connectionInfoSet) {
        for (com.navdy.service.library.device.connection.ConnectionInfo connectionInfo : connectionInfoSet) {
            addConnectionInfo(connectionInfo, true);
        }
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_device_picker, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        return rootView;
    }

    public void onResume() {
        super.onResume();
        refreshCurrentDevice();
        this.mRemoteDeviceRegistry.startScanning();
    }

    public void onPause() {
        super.onPause();
        this.mRemoteDeviceRegistry.stopScanning();
    }

    public void onDestroy() {
        super.onDestroy();
        this.mRemoteDeviceRegistry.removeListener(this.mDeviceListUpdateListener);
    }

    public void refreshCurrentDevice() {
        this.mCurrentDevice = com.navdy.service.library.device.RemoteDeviceRegistry.getInstance(getActivity()).getDefaultConnectionInfo();
        if (this.mCurrentDevice != null) {
            this.mTextViewCurrentDevice.setText("Current device: " + com.navdy.client.debug.devicepicker.Device.prettyName(this.mCurrentDevice));
            addConnectionInfo(this.mCurrentDevice, false);
        }
    }

    public void onListItemClick(android.widget.ListView l, android.view.View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        this.mRemoteDeviceRegistry.setDefaultConnectionInfo(((com.navdy.client.debug.devicepicker.Device) this.mDevices.get(position)).getPreferredConnectionInfo());
        this.mAppInstance.initializeDevice();
        hideKeyboard();
        android.app.FragmentManager fragmentManager = getFragmentManager();
        android.app.Fragment splash = fragmentManager.findFragmentByTag(com.navdy.client.debug.MainDebugActivity.SPLASH_TAG);
        fragmentManager.popBackStackImmediate();
        if (splash != null) {
            android.app.FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(com.navdy.client.R.id.container, new com.navdy.client.debug.MainDebugActivity.MainFragment(), com.navdy.client.debug.MainDebugActivity.MAIN_TAG);
            ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.commit();
        }
    }

    @butterknife.OnClick({2131755577})
    public void onAddClicked() {
        java.lang.String newDeviceAddress = this.mEditTextNewDevice.getText().toString();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(newDeviceAddress)) {
            this.mRemoteDeviceRegistry.addDiscoveredConnectionInfo(new com.navdy.service.library.device.connection.TCPConnectionInfo(com.navdy.service.library.device.NavdyDeviceId.UNKNOWN_ID, newDeviceAddress));
            hideKeyboard();
            this.mEditTextNewDevice.setText("");
        }
    }

    protected void hideKeyboard() {
        ((android.view.inputmethod.InputMethodManager) getActivity().getSystemService("input_method")).hideSoftInputFromWindow(this.mEditTextNewDevice.getWindowToken(), 0);
    }

    private android.app.ActionBar getActionBar() {
        return getActivity().getActionBar();
    }
}
