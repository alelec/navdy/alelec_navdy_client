package com.navdy.client.debug.navdebug;

public class ManeuverArrayAdapter extends android.widget.ArrayAdapter<com.here.android.mpa.routing.Maneuver> {
    private final android.app.Activity mActivity;
    private final java.util.List<com.here.android.mpa.routing.Maneuver> mManeuvers;

    static class ViewHolder {
        com.navdy.client.debug.navdebug.ManeuverDetailPanel mManeuverDetailPanel;

        public ViewHolder(com.navdy.client.debug.navdebug.ManeuverDetailPanel maneuverDetailPanel) {
            this.mManeuverDetailPanel = maneuverDetailPanel;
        }
    }

    public ManeuverArrayAdapter(android.app.Activity activity, java.util.List<com.here.android.mpa.routing.Maneuver> maneuvers) {
        super(activity, com.navdy.client.R.layout.maneuver_panel_children, maneuvers);
        this.mActivity = activity;
        this.mManeuvers = maneuvers;
    }

    public android.view.View getView(int position, android.view.View view, android.view.ViewGroup parent) {
        com.navdy.client.debug.navdebug.ManeuverArrayAdapter.ViewHolder holder;
        if (view != null) {
            holder = (com.navdy.client.debug.navdebug.ManeuverArrayAdapter.ViewHolder) view.getTag();
        } else {
            android.view.LayoutInflater layoutInflater = this.mActivity.getLayoutInflater();
            view = com.navdy.client.debug.navdebug.ManeuverDetailPanel.inflate(parent);
            holder = new com.navdy.client.debug.navdebug.ManeuverArrayAdapter.ViewHolder((com.navdy.client.debug.navdebug.ManeuverDetailPanel) view);
            view.setTag(holder);
        }
        holder.mManeuverDetailPanel.setManeuver((com.here.android.mpa.routing.Maneuver) this.mManeuvers.get(position));
        return view;
    }
}
