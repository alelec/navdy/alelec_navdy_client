package com.navdy.client.debug.navdebug;

public class NavigationDemoActivity extends com.here.android.mpa.common.MapActivity {
    private static final double[] DEFAULT_DESTINATION = {49.24831d, -122.980013d};
    private static final double[] DEFAULT_START_POINT = {49.260024d, -123.006984d};
    public static final java.lang.String EXTRA_COORDS_END = "dest_coords";
    public static final java.lang.String EXTRA_COORDS_START = "start_coords";
    public static final float FT_IN_METER = 3.28084f;
    public static final float METERS_IN_MI = 1609.34f;
    public static final float SMALL_UNITS_THRESHOLD = 0.1f;
    private static final java.lang.String VOICE_MARC_CODE = "eng";
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.navdebug.NavigationDemoActivity.class);
    private android.widget.TextView currentRoadTV;
    private android.widget.TextView currentSpeedTV;
    private android.widget.TextView distToNextManeuverTV;
    protected final java.text.SimpleDateFormat etaFormatter = new java.text.SimpleDateFormat("h:mm");
    private android.widget.TextView etaTV;
    @butterknife.InjectView(2131755211)
    protected android.widget.TextView laneInfo;
    private com.here.android.mpa.guidance.NavigationManager.LaneInformationListener laneInformationListener = new com.navdy.client.debug.navdebug.NavigationDemoActivity.Anon5();
    private java.lang.String mCurrentRoad = "";
    private java.lang.String mDistanceToPendingStreet = "";
    private com.here.android.mpa.common.GeoCoordinate mEndCoord;
    protected java.util.HashMap<com.here.android.mpa.routing.Maneuver.Turn, com.navdy.service.library.events.navigation.NavigationTurn> mEngineTurnToNavigationTurnMap;
    private java.lang.String mEtaText = "";
    private java.util.ArrayList<com.here.android.mpa.guidance.LaneInformation> mLaneInfoList;
    private android.view.Menu mMenu;
    private java.lang.String mPendingStreet = "";
    private com.navdy.service.library.events.navigation.NavigationTurn mPendingTurn = com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UNKNOWN;
    private java.lang.String mSpeedText = "";
    private com.here.android.mpa.common.GeoCoordinate mStartCoord;
    private java.util.ArrayList<com.here.android.mpa.routing.Maneuver> maneuverDetailList;
    @butterknife.InjectView(2131755212)
    protected android.widget.ListView maneuverDetailListView;
    private android.widget.RelativeLayout maneuverInfo;
    private com.here.android.mpa.mapping.Map map = null;
    private com.here.android.mpa.mapping.MapFragment mapFragment = null;
    private com.here.android.mpa.mapping.MapRoute mapRoute = null;
    private android.widget.RelativeLayout navigationInfo;
    private com.here.android.mpa.guidance.NavigationManager navigationManager;
    private com.here.android.mpa.guidance.NavigationManager.NavigationManagerEventListener navigationManagerListener = new com.navdy.client.debug.navdebug.NavigationDemoActivity.Anon4();
    private com.here.android.mpa.guidance.NavigationManager.NewInstructionEventListener newInstructionEventListener = new com.navdy.client.debug.navdebug.NavigationDemoActivity.Anon6();
    private android.widget.TextView nextManeuverTurnTV;
    private android.widget.TextView nextRoadTV;
    private com.here.android.mpa.guidance.NavigationManager.PositionListener positionListener = new com.navdy.client.debug.navdebug.NavigationDemoActivity.Anon7();
    private com.here.android.mpa.common.PositioningManager positioningManager;
    private com.here.android.mpa.routing.Route route = null;
    private com.here.android.mpa.routing.RouteManager.Listener routeManagerListener = new com.navdy.client.debug.navdebug.NavigationDemoActivity.Anon3();
    private android.widget.TextView speedLimitTV;
    private com.here.android.mpa.guidance.NavigationManager.SpeedWarningListener speedWarningListener = new com.navdy.client.debug.navdebug.NavigationDemoActivity.Anon8();
    @butterknife.InjectView(2131755213)
    protected android.widget.Button toggleButton;
    private com.here.android.mpa.guidance.VoiceCatalog voiceCatalog;
    private com.here.android.mpa.guidance.VoiceSkin voiceSkin = null;

    class Anon1 implements com.here.android.mpa.common.OnEngineInitListener {
        Anon1() {
        }

        public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
            if (error == com.here.android.mpa.common.OnEngineInitListener.Error.NONE) {
                com.navdy.client.debug.navdebug.NavigationDemoActivity.this.onMapEngineInitializationCompleted();
                com.navdy.client.debug.navdebug.NavigationDemoActivity.this.createRoute(com.navdy.client.debug.navdebug.NavigationDemoActivity.this.mStartCoord, com.navdy.client.debug.navdebug.NavigationDemoActivity.this.mEndCoord);
                return;
            }
            com.navdy.client.debug.navdebug.NavigationDemoActivity.sLogger.e("Cannot initialize map fragment:" + error.toString());
        }
    }

    class Anon2 implements com.here.android.mpa.guidance.VoiceCatalog.OnDownloadDoneListener {

        class Anon1 implements com.here.android.mpa.guidance.VoiceCatalog.OnDownloadDoneListener {
            final /* synthetic */ com.here.android.mpa.guidance.VoicePackage val$voicePackage;

            Anon1(com.here.android.mpa.guidance.VoicePackage voicePackage) {
                this.val$voicePackage = voicePackage;
            }

            public void onDownloadDone(com.here.android.mpa.guidance.VoiceCatalog.Error error) {
                if (error == com.here.android.mpa.guidance.VoiceCatalog.Error.NONE) {
                    com.navdy.client.debug.navdebug.NavigationDemoActivity.this.voiceSkin = com.navdy.client.debug.navdebug.NavigationDemoActivity.this.voiceCatalog.getLocalVoiceSkin(this.val$voicePackage.getId());
                } else {
                    com.navdy.client.debug.navdebug.NavigationDemoActivity.sLogger.e("Download voice package failed:" + error.toString());
                }
            }
        }

        Anon2() {
        }

        public void onDownloadDone(com.here.android.mpa.guidance.VoiceCatalog.Error error) {
            if (error == com.here.android.mpa.guidance.VoiceCatalog.Error.NONE) {
                for (com.here.android.mpa.guidance.VoicePackage voicePackage : com.navdy.client.debug.navdebug.NavigationDemoActivity.this.voiceCatalog.getCatalogList()) {
                    if (voicePackage.getMarcCode().equalsIgnoreCase(com.navdy.client.debug.navdebug.NavigationDemoActivity.VOICE_MARC_CODE) && voicePackage.isTts()) {
                        com.navdy.client.debug.navdebug.NavigationDemoActivity.this.voiceCatalog.downloadVoice(voicePackage.getId(), new com.navdy.client.debug.navdebug.NavigationDemoActivity.Anon2.Anon1(voicePackage));
                        return;
                    }
                }
                return;
            }
            com.navdy.client.debug.navdebug.NavigationDemoActivity.sLogger.e("Download voice catalog failed:" + error.toString());
        }
    }

    class Anon3 implements com.here.android.mpa.routing.RouteManager.Listener {
        Anon3() {
        }

        public void onCalculateRouteFinished(com.here.android.mpa.routing.RouteManager.Error error, java.util.List<com.here.android.mpa.routing.RouteResult> results) {
            if (error != com.here.android.mpa.routing.RouteManager.Error.NONE || ((com.here.android.mpa.routing.RouteResult) results.get(0)).getRoute() == null) {
                com.navdy.client.debug.navdebug.NavigationDemoActivity.sLogger.e("Route calculation failed:" + error.toString());
                return;
            }
            com.navdy.client.debug.navdebug.NavigationDemoActivity.this.route = ((com.here.android.mpa.routing.RouteResult) results.get(0)).getRoute();
            com.navdy.client.debug.navdebug.NavigationDemoActivity.this.mapRoute = new com.here.android.mpa.mapping.MapRoute(com.navdy.client.debug.navdebug.NavigationDemoActivity.this.route);
            com.navdy.client.debug.navdebug.NavigationDemoActivity.this.map.addMapObject(com.navdy.client.debug.navdebug.NavigationDemoActivity.this.mapRoute);
            com.navdy.client.debug.navdebug.NavigationDemoActivity.this.map.zoomTo(com.navdy.client.debug.navdebug.NavigationDemoActivity.this.route.getBoundingBox(), com.here.android.mpa.mapping.Map.Animation.NONE, -1.0f);
        }

        public void onProgress(int percentage) {
            com.navdy.client.debug.navdebug.NavigationDemoActivity.sLogger.i("... " + percentage + "percent done ...");
        }
    }

    class Anon4 extends com.here.android.mpa.guidance.NavigationManager.NavigationManagerEventListener {
        Anon4() {
        }

        public void onRunningStateChanged() {
            com.here.android.mpa.guidance.NavigationManager.NavigationState navigationState = com.navdy.client.debug.navdebug.NavigationDemoActivity.this.navigationManager.getRunningState();
            com.navdy.client.debug.navdebug.NavigationDemoActivity.sLogger.e("Running state: " + navigationState);
            switch (com.navdy.client.debug.navdebug.NavigationDemoActivity.Anon9.$SwitchMap$com$here$android$mpa$guidance$NavigationManager$NavigationState[navigationState.ordinal()]) {
                case 1:
                    com.navdy.client.debug.navdebug.NavigationDemoActivity.this.toggleButton.setText("||");
                    return;
                case 2:
                    com.navdy.client.debug.navdebug.NavigationDemoActivity.this.toggleButton.setText(">");
                    return;
                case 3:
                    com.navdy.client.debug.navdebug.NavigationDemoActivity.this.toggleButton.setText(" ");
                    return;
                default:
                    com.navdy.client.debug.navdebug.NavigationDemoActivity.sLogger.e("Unknown nav state: " + navigationState);
                    return;
            }
        }

        public void onRouteUpdated(com.here.android.mpa.routing.Route route) {
            super.onRouteUpdated(route);
            com.navdy.client.debug.navdebug.NavigationDemoActivity.this.map.removeMapObject(com.navdy.client.debug.navdebug.NavigationDemoActivity.this.mapRoute);
            com.navdy.client.debug.navdebug.NavigationDemoActivity.this.mapRoute = new com.here.android.mpa.mapping.MapRoute(route);
            com.navdy.client.debug.navdebug.NavigationDemoActivity.this.map.addMapObject(com.navdy.client.debug.navdebug.NavigationDemoActivity.this.mapRoute);
        }

        public void onEnded(com.here.android.mpa.guidance.NavigationManager.NavigationMode mode) {
            com.navdy.client.debug.navdebug.NavigationDemoActivity.this.resetUIOnNavigationEnds();
        }
    }

    class Anon5 extends com.here.android.mpa.guidance.NavigationManager.LaneInformationListener {
        Anon5() {
        }

        public void onLaneInformation(java.util.List<com.here.android.mpa.guidance.LaneInformation> list, com.here.android.mpa.common.RoadElement roadElement) {
            super.onLaneInformation(list, roadElement);
            com.navdy.client.debug.navdebug.NavigationDemoActivity.this.clearLaneInfo();
            com.navdy.client.debug.navdebug.NavigationDemoActivity.this.addLaneInfo(list);
        }
    }

    class Anon6 extends com.here.android.mpa.guidance.NavigationManager.NewInstructionEventListener {
        Anon6() {
        }

        public void onNewInstructionEvent() {
            super.onNewInstructionEvent();
            com.navdy.client.debug.navdebug.NavigationDemoActivity.this.updateManeuver();
        }
    }

    class Anon7 extends com.here.android.mpa.guidance.NavigationManager.PositionListener {
        Anon7() {
        }

        public void onPositionUpdated(com.here.android.mpa.common.GeoPosition geoPosition) {
            super.onPositionUpdated(geoPosition);
            com.navdy.client.debug.navdebug.NavigationDemoActivity.this.updatePositionInfo(geoPosition);
        }
    }

    class Anon8 extends com.here.android.mpa.guidance.NavigationManager.SpeedWarningListener {
        Anon8() {
        }

        public void onSpeedExceeded(java.lang.String roadName, float speedLimit) {
            super.onSpeedExceeded(roadName, speedLimit);
            com.navdy.client.debug.navdebug.NavigationDemoActivity.this.speedLimitTV.setText(java.lang.String.format(java.util.Locale.getDefault(), "%d m/s", new java.lang.Object[]{java.lang.Integer.valueOf((int) speedLimit)}));
        }

        public void onSpeedExceededEnd(java.lang.String roadName, float speedLimit) {
            super.onSpeedExceededEnd(roadName, speedLimit);
            com.navdy.client.debug.navdebug.NavigationDemoActivity.this.speedLimitTV.setText("");
        }
    }

    static /* synthetic */ class Anon9 {
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$guidance$NavigationManager$NavigationState = new int[com.here.android.mpa.guidance.NavigationManager.NavigationState.values().length];

        static {
            try {
                $SwitchMap$com$here$android$mpa$guidance$NavigationManager$NavigationState[com.here.android.mpa.guidance.NavigationManager.NavigationState.RUNNING.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$NavigationManager$NavigationState[com.here.android.mpa.guidance.NavigationManager.NavigationState.PAUSED.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$NavigationManager$NavigationState[com.here.android.mpa.guidance.NavigationManager.NavigationState.IDLE.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e3) {
            }
        }
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.navdy.client.R.layout.activity_navigation_demo);
        loadBundleArguments();
        this.mapFragment = (com.here.android.mpa.mapping.MapFragment) getFragmentManager().findFragmentById(com.navdy.client.R.id.map_fragment);
        if (this.mapFragment != null) {
            this.mapFragment.init(new com.navdy.client.debug.navdebug.NavigationDemoActivity.Anon1());
        }
        this.maneuverInfo = (android.widget.RelativeLayout) findViewById(com.navdy.client.R.id.maneuver_info);
        this.navigationInfo = (android.widget.RelativeLayout) findViewById(com.navdy.client.R.id.navigation_info);
        this.currentRoadTV = (android.widget.TextView) findViewById(com.navdy.client.R.id.current_road);
        this.currentSpeedTV = (android.widget.TextView) findViewById(com.navdy.client.R.id.curr_speed_nav);
        this.etaTV = (android.widget.TextView) findViewById(com.navdy.client.R.id.estimated_time_arrival);
        this.nextManeuverTurnTV = (android.widget.TextView) findViewById(com.navdy.client.R.id.next_maneuver_turn);
        this.nextRoadTV = (android.widget.TextView) findViewById(com.navdy.client.R.id.next_road_name);
        this.distToNextManeuverTV = (android.widget.TextView) findViewById(com.navdy.client.R.id.dist_to_next_maneuver);
        this.speedLimitTV = (android.widget.TextView) findViewById(com.navdy.client.R.id.curr_speed_limit);
        butterknife.ButterKnife.inject((android.app.Activity) this);
        this.maneuverDetailList = new java.util.ArrayList<>();
        this.maneuverDetailListView.setAdapter(new com.navdy.client.debug.navdebug.ManeuverArrayAdapter(this, this.maneuverDetailList));
        this.mLaneInfoList = new java.util.ArrayList<>();
    }

    protected void onDestroy() {
        super.onDestroy();
        sLogger.e("destroy!");
        this.navigationManager.removeNavigationManagerEventListener(this.navigationManagerListener);
    }

    public void loadBundleArguments() {
        double[] startLatLon = getIntent().getDoubleArrayExtra("start_coords");
        double[] endLatLon = getIntent().getDoubleArrayExtra("dest_coords");
        if (startLatLon == null || startLatLon.length != 2) {
            sLogger.e("Missing valid start coordinates. Falling back.");
            startLatLon = DEFAULT_START_POINT;
        }
        if (endLatLon == null || endLatLon.length != 2) {
            sLogger.e("Missing end coordinates. Falling back.");
            endLatLon = DEFAULT_DESTINATION;
        }
        this.mStartCoord = new com.here.android.mpa.common.GeoCoordinate(startLatLon[0], startLatLon[1]);
        this.mEndCoord = new com.here.android.mpa.common.GeoCoordinate(endLatLon[0], endLatLon[1]);
    }

    public static double[] latLonArrayFromGeoCoordinate(com.here.android.mpa.common.GeoCoordinate coordinate) {
        return new double[]{coordinate.getLatitude(), coordinate.getLongitude()};
    }

    public static android.content.Intent createIntentWithCoords(android.content.Context context, com.here.android.mpa.common.GeoCoordinate startCoord, com.here.android.mpa.common.GeoCoordinate endCoord) {
        if (startCoord == null || endCoord == null) {
            sLogger.e("createIntentWithCoords: bad coordinates");
            return null;
        }
        android.content.Intent i = new android.content.Intent(context, com.navdy.client.debug.navdebug.NavigationDemoActivity.class);
        i.putExtra("start_coords", latLonArrayFromGeoCoordinate(startCoord));
        i.putExtra("dest_coords", latLonArrayFromGeoCoordinate(endCoord));
        return i;
    }

    public void onResume() {
        super.onResume();
        if (this.navigationManager != null && this.navigationManager.getRunningState().equals(com.here.android.mpa.guidance.NavigationManager.NavigationState.RUNNING)) {
            this.maneuverInfo.setVisibility(View.VISIBLE);
            this.navigationInfo.setVisibility(View.VISIBLE);
        }
    }

    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(com.navdy.client.R.menu.activity_navigation_demo, menu);
        this.mMenu = menu;
        return true;
    }

    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case com.navdy.client.R.id.navigation_start /*2131756042*/:
                startNavigation(false);
                return true;
            case com.navdy.client.R.id.sim_start /*2131756043*/:
                startNavigation(true);
                return true;
            case com.navdy.client.R.id.navigation_stop /*2131756044*/:
                this.navigationManager.stop();
                resetUIOnNavigationEnds();
                return true;
            default:
                return false;
        }
    }

    public boolean onPrepareOptionsMenu(android.view.Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (this.navigationManager != null && this.navigationManager.getRunningState().equals(com.here.android.mpa.guidance.NavigationManager.NavigationState.RUNNING)) {
            updateMenuForNavigationActive(true);
        }
        return true;
    }

    private void onMapEngineInitializationCompleted() {
        if (this.mapFragment != null) {
            this.map = this.mapFragment.getMap();
        }
        this.positioningManager = com.here.android.mpa.common.PositioningManager.getInstance();
        if (!this.positioningManager.isActive()) {
            this.positioningManager.start(com.here.android.mpa.common.PositioningManager.LocationMethod.GPS_NETWORK);
        }
        if (this.map != null) {
            this.map.getPositionIndicator().setVisible(true);
        }
        this.navigationManager = com.here.android.mpa.guidance.NavigationManager.getInstance();
        this.navigationManager.addNavigationManagerEventListener(new java.lang.ref.WeakReference(this.navigationManagerListener));
        this.navigationManager.addPositionListener(new java.lang.ref.WeakReference(this.positionListener));
        this.navigationManager.addLaneInformationListener(new java.lang.ref.WeakReference(this.laneInformationListener));
        this.navigationManager.addNewInstructionEventListener(new java.lang.ref.WeakReference(this.newInstructionEventListener));
        this.navigationManager.addSpeedWarningListener(new java.lang.ref.WeakReference(this.speedWarningListener));
        this.voiceCatalog = com.here.android.mpa.guidance.VoiceCatalog.getInstance();
        downloadTargetVoiceSkin();
    }

    private void createRoute(com.here.android.mpa.common.GeoCoordinate start, com.here.android.mpa.common.GeoCoordinate end) {
        com.here.android.mpa.routing.RouteManager routeManager = new com.here.android.mpa.routing.RouteManager();
        com.here.android.mpa.routing.RoutePlan routePlan = new com.here.android.mpa.routing.RoutePlan();
        com.here.android.mpa.routing.RouteOptions routeOptions = new com.here.android.mpa.routing.RouteOptions();
        routeOptions.setTransportMode(com.here.android.mpa.routing.RouteOptions.TransportMode.CAR);
        routeOptions.setRouteType(com.here.android.mpa.routing.RouteOptions.Type.FASTEST);
        routePlan.setRouteOptions(routeOptions);
        routePlan.addWaypoint(start);
        routePlan.addWaypoint(end);
        routeManager.calculateRoute(routePlan, this.routeManagerListener);
    }

    private void startNavigation(boolean simulated) {
        com.here.android.mpa.guidance.NavigationManager.Error error;
        updateMenuForNavigationActive(true);
        this.navigationManager.setMap(this.map);
        if (simulated) {
            error = this.navigationManager.simulate(this.route, (long) com.navdy.client.debug.DebugActionsFragment.getSimulationSpeed());
        } else {
            error = this.navigationManager.startNavigation(this.route);
        }
        if (error == com.here.android.mpa.guidance.NavigationManager.Error.NONE) {
            this.navigationManager.setMapUpdateMode(com.here.android.mpa.guidance.NavigationManager.MapUpdateMode.ROADVIEW);
            if (this.voiceSkin != null) {
                this.navigationManager.setVoiceSkin(this.voiceSkin);
            }
            this.maneuverInfo.setVisibility(View.VISIBLE);
            this.navigationInfo.setVisibility(View.VISIBLE);
            return;
        }
        sLogger.e("Start simulation failed:" + error.toString());
    }

    private void updateMenuForNavigationActive(boolean active) {
        boolean z;
        boolean z2 = true;
        android.view.MenuItem findItem = this.mMenu.findItem(com.navdy.client.R.id.navigation_start);
        if (!active) {
            z = true;
        } else {
            z = false;
        }
        findItem.setEnabled(z);
        android.view.MenuItem findItem2 = this.mMenu.findItem(com.navdy.client.R.id.sim_start);
        if (active) {
            z2 = false;
        }
        findItem2.setEnabled(z2);
        this.mMenu.findItem(com.navdy.client.R.id.navigation_stop).setEnabled(active);
    }

    private void downloadTargetVoiceSkin() {
        java.util.Iterator it = this.voiceCatalog.getLocalVoiceSkins().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            com.here.android.mpa.guidance.VoiceSkin skin = (com.here.android.mpa.guidance.VoiceSkin) it.next();
            if (skin.getMarcCode().compareToIgnoreCase(VOICE_MARC_CODE) == 0 && skin.getOutputType() == com.here.android.mpa.guidance.VoiceSkin.OutputType.TTS) {
                this.voiceSkin = skin;
                break;
            }
        }
        if (this.voiceSkin == null) {
            this.voiceCatalog.downloadCatalog(new com.navdy.client.debug.navdebug.NavigationDemoActivity.Anon2());
        }
    }

    private void clearNavigationInfo() {
        this.nextRoadTV.setText("");
        this.nextManeuverTurnTV.setText("");
        this.distToNextManeuverTV.setText("");
        this.currentRoadTV.setText("");
        this.speedLimitTV.setText("");
        this.currentSpeedTV.setText("");
        this.etaTV.setText("");
        this.laneInfo.setText("");
    }

    private void resetUIOnNavigationEnds() {
        clearNavigationInfo();
        updateMenuForNavigationActive(false);
    }

    private void updatePositionInfo(com.here.android.mpa.common.GeoPosition loc) {
        this.mSpeedText = java.lang.String.format(java.util.Locale.getDefault(), "%d mph", new java.lang.Object[]{java.lang.Integer.valueOf((int) (loc.getSpeed() / 1609.3399658203125d))});
        long remainingTimeMs = this.navigationManager.getEta(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.DISABLED).getTime() - new java.util.Date().getTime();
        java.util.Calendar eta = java.util.Calendar.getInstance();
        eta.add(13, (int) (remainingTimeMs / 1000));
        this.mEtaText = this.etaFormatter.format(eta.getTime());
        this.mDistanceToPendingStreet = imperialUSDistanceStringFromMeters(this.navigationManager.getNextManeuverDistance());
        this.distToNextManeuverTV.setText(this.mDistanceToPendingStreet);
        this.etaTV.setText(this.mEtaText);
        this.currentSpeedTV.setText(this.mSpeedText);
        com.here.android.mpa.routing.Maneuver nextManeuver = this.navigationManager.getNextManeuver();
        if (nextManeuver != null) {
            this.mPendingStreet = nextManeuver.getNextRoadName();
            this.mCurrentRoad = nextManeuver.getRoadName();
            this.nextRoadTV.setText(this.mPendingStreet);
            this.currentRoadTV.setText(this.mCurrentRoad);
        }
        sendNavigationEvent();
    }

    @butterknife.OnClick({2131755213})
    protected void onToggleClicked() {
        if (this.navigationManager.getRunningState().equals(com.here.android.mpa.guidance.NavigationManager.NavigationState.PAUSED)) {
            this.navigationManager.resume();
        } else if (this.navigationManager.getRunningState().equals(com.here.android.mpa.guidance.NavigationManager.NavigationState.RUNNING)) {
            this.navigationManager.pause();
        }
    }

    private void updateManeuver() {
        com.here.android.mpa.routing.Maneuver nextManeuver = this.navigationManager.getNextManeuver();
        if (nextManeuver != null) {
            this.mPendingStreet = nextManeuver.getNextRoadName();
            com.here.android.mpa.routing.Maneuver.Turn engineNextTurn = nextManeuver.getTurn();
            if (this.mPendingTurn != null) {
                this.mPendingTurn = navigationTurnFromEngineTurn(engineNextTurn);
            }
            this.mCurrentRoad = nextManeuver.getRoadName();
            this.nextRoadTV.setText(this.mPendingStreet);
            this.nextManeuverTurnTV.setText(java.lang.String.valueOf(this.mPendingTurn));
            this.currentRoadTV.setText(this.mCurrentRoad);
            this.maneuverDetailList.clear();
            this.maneuverDetailList.add(nextManeuver);
            ((com.navdy.client.debug.navdebug.ManeuverArrayAdapter) this.maneuverDetailListView.getAdapter()).notifyDataSetChanged();
            sendNavigationEvent();
            return;
        }
        sLogger.e("No next maneuver.");
    }

    public void addLaneInfo(java.util.List<com.here.android.mpa.guidance.LaneInformation> laneInfoList) {
        if (laneInfoList == null) {
            sLogger.e("empty lane info");
            return;
        }
        this.mLaneInfoList.clear();
        for (com.here.android.mpa.guidance.LaneInformation laneInfo2 : laneInfoList) {
            if (!this.mLaneInfoList.contains(laneInfo2)) {
                this.mLaneInfoList.add(laneInfo2);
            }
        }
        refreshLaneInfoDisplay();
    }

    public void clearLaneInfo() {
        this.mLaneInfoList.clear();
        refreshLaneInfoDisplay();
    }

    public void refreshLaneInfoDisplay() {
        java.util.ArrayList<java.lang.String> laneTextList = new java.util.ArrayList<>();
        java.util.Iterator it = this.mLaneInfoList.iterator();
        while (it.hasNext()) {
            laneTextList.add("(" + android.text.TextUtils.join(org.droidparts.contract.SQL.DDL.SEPARATOR, ((com.here.android.mpa.guidance.LaneInformation) it.next()).getDirections()) + ")");
        }
        java.lang.String laneText = android.text.TextUtils.join("; ", laneTextList);
        sLogger.e("Lanes: " + laneText);
        this.laneInfo.setText(laneText);
    }

    private void sendNavigationEvent() {
        com.navdy.service.library.events.navigation.NavigationManeuverEvent navEvent = new com.navdy.service.library.events.navigation.NavigationManeuverEvent(this.mCurrentRoad, this.mPendingTurn, this.mDistanceToPendingStreet, this.mPendingStreet, this.mEtaText, this.mSpeedText, null, java.lang.Long.valueOf(0));
        sLogger.d("Maneuver: " + navEvent.toString());
        com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice != null) {
            remoteDevice.postEvent((com.squareup.wire.Message) navEvent);
        }
    }

    public java.lang.String imperialUSDistanceStringFromMeters(long meters) {
        float distanceValue;
        java.lang.String distanceUnits;
        java.lang.String distanceFormat;
        if (((float) meters) >= 160.934f) {
            distanceValue = ((float) meters) / 1609.34f;
            distanceUnits = "mi";
            distanceFormat = "%.1f %s";
        } else {
            distanceValue = ((float) meters) * 3.28084f;
            distanceUnits = "ft";
            distanceFormat = "%.0f %s";
        }
        return java.lang.String.format(distanceFormat, new java.lang.Object[]{java.lang.Float.valueOf(distanceValue), distanceUnits});
    }

    private com.navdy.service.library.events.navigation.NavigationTurn navigationTurnFromEngineTurn(com.here.android.mpa.routing.Maneuver.Turn turn) {
        if (turn == null) {
            sLogger.e("directionInfoFromTurn: passed null");
            return null;
        }
        if (this.mEngineTurnToNavigationTurnMap == null) {
            this.mEngineTurnToNavigationTurnMap = new java.util.HashMap<>();
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.UNDEFINED, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UNKNOWN);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.NO_TURN, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UNKNOWN);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.KEEP_MIDDLE, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.KEEP_RIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_RIGHT);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.LIGHT_RIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_RIGHT);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.QUITE_RIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_RIGHT);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.HEAVY_RIGHT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_RIGHT);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.KEEP_LEFT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_KEEP_LEFT);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.LIGHT_LEFT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_EASY_LEFT);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.QUITE_LEFT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_LEFT);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.HEAVY_LEFT, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_SHARP_LEFT);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.RETURN, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_UTURN_LEFT);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_1, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_2, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_3, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_4, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_5, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_6, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_7, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_8, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_9, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_10, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_11, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(com.here.android.mpa.routing.Maneuver.Turn.ROUNDABOUT_12, com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        }
        com.navdy.service.library.events.navigation.NavigationTurn navigationTurn = (com.navdy.service.library.events.navigation.NavigationTurn) this.mEngineTurnToNavigationTurnMap.get(turn);
        if (navigationTurn != null) {
            return navigationTurn;
        }
        sLogger.e("navigationTurnFromEngineTurn: unknown & defaulting to straight for: " + turn);
        return com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_STRAIGHT;
    }
}
