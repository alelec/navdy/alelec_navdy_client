package com.navdy.client.debug.videoplayer;

public class VideoPlayerActivity$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.videoplayer.VideoPlayerActivity target, java.lang.Object source) {
        target.videoView = (android.widget.VideoView) finder.findRequiredView(source, com.navdy.client.R.id.video_view, "field 'videoView'");
        target.downloadStatus = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.download_status, "field 'downloadStatus'");
    }

    public static void reset(com.navdy.client.debug.videoplayer.VideoPlayerActivity target) {
        target.videoView = null;
        target.downloadStatus = null;
    }
}
