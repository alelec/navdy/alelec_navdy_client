package com.navdy.client.debug.common;

public class BaseDebugFragment extends android.app.Fragment {
    protected com.navdy.client.debug.common.BaseDebugActivity baseActivity;
    /* access modifiers changed from: protected */
    public com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());

    public void onAttach(android.app.Activity activity) {
        this.baseActivity = (com.navdy.client.debug.common.BaseDebugActivity) activity;
        super.onAttach(activity);
    }

    public void onDetach() {
        this.baseActivity = null;
        super.onDetach();
    }

    public boolean isAlive() {
        return this.baseActivity != null && !this.baseActivity.isActivityDestroyed();
    }
}
