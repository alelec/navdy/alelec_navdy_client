package com.navdy.client;

public final class BuildConfig {
    public static final java.lang.String APPLICATION_ID = "com.navdy.client";
    public static final java.lang.String BUILD_TYPE = "debug";
    public static final boolean DEBUG = java.lang.Boolean.parseBoolean("true");
    public static final java.lang.String FLAVOR = "Modern";
    public static final int VERSION_CODE = 1;
    public static final java.lang.String VERSION_NAME = "1.4.1-1db36186e";
}
