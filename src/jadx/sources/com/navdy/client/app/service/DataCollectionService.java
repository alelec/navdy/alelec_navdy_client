package com.navdy.client.app.service;

public class DataCollectionService implements android.hardware.SensorEventListener, com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener {
    private static final int FAST_REFRESH_RATE = 83333;
    private static final java.lang.String FILE_EXTENSION = ".json.gz";
    private static final boolean LOG_ALL_SENSOR_EVENTS = false;
    private static final int LOG_FREQUENCY = 1000;
    private static final int MAX_FOLDER_SIZE = 52428800;
    private static final int MAX_S3_UPLOAD_RETRY = 3;
    private static final int MIN_FREE_SPACE = 5242880;
    private static final int REQUEST_CODE = 654974;
    private static final java.lang.String S3_BUCKET_NAME = "navdy-trip-data";
    private static final java.lang.String SENSOR_DATA_FOLDER = "SensorData";
    private static final int SLOW_REFRESH_RATE = 1000;
    private static final int SUPER_FAST_REFRESH_RATE = 166666;
    private static final long UPLOAD_DELAY = java.util.concurrent.TimeUnit.MINUTES.toMillis(30);
    private static int failCount = 0;
    private static volatile java.io.File folder;
    private static java.text.DecimalFormat formatter = new java.text.DecimalFormat("0.00000000000000", new java.text.DecimalFormatSymbols(java.util.Locale.US));
    private static com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.service.DataCollectionService.class);
    private static volatile java.io.File rootFolder;
    private static java.util.concurrent.atomic.AtomicBoolean sendInProgress = new java.util.concurrent.atomic.AtomicBoolean(false);
    private static com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver transferObserver;
    private static java.lang.Boolean useNanoTime = null;
    private android.hardware.Sensor accelerometer = null;
    private android.os.Handler bgHandler;
    public com.navdy.client.app.service.DataCollectionService.Data data = new com.navdy.client.app.service.DataCollectionService.Data();
    private final java.lang.Object dataLock = new java.lang.Object();
    private com.google.android.gms.common.api.GoogleApiClient googleApiClient;
    private android.hardware.Sensor gravity = null;
    private android.hardware.Sensor gyroscope = null;
    private boolean isCollecting = false;
    private boolean isConnected = false;
    private boolean isDriving = false;
    private long lastGravityTimeStamp = 0;
    private long lastGyroscopeTimeStamp = 0;
    private long lastLinearAccelerationTimeStamp = 0;
    private com.navdy.client.app.service.DataCollectionService.Data.Location lastLocation = null;
    private int lastMagFieldAccuracy = 0;
    private long lastMagneticFieldTimeStamp = 0;
    private long lastOrientationTimeStamp = 0;
    private long lastRotationVectorTimeStamp = 0;
    private android.hardware.Sensor linearAcceleration = null;
    private java.lang.Runnable locationUpdatesAtRegularInterval = new com.navdy.client.app.service.DataCollectionService.Anon6();
    private android.hardware.Sensor magneticField = null;
    private android.hardware.Sensor rotationVector = null;
    private android.hardware.SensorManager sensorManager = null;
    private com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility transferUtility = new com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility(com.navdy.client.ota.impl.OTAUpdateManagerImpl.createS3Client(), com.navdy.client.app.NavdyApplication.getAppContext());
    private android.os.Handler uploadScheduler;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            synchronized (com.navdy.client.app.service.DataCollectionService.this.dataLock) {
                com.navdy.client.app.service.DataCollectionService.this.isConnected = true;
            }
            com.navdy.client.app.service.DataCollectionService.this.updateHudUuid();
            com.navdy.client.app.service.DataCollectionService.this.startCollecting();
        }
    }

    class Anon10 implements java.lang.Runnable {
        Anon10() {
        }

        public void run() {
            synchronized (com.navdy.client.app.service.DataCollectionService.this.dataLock) {
                com.navdy.client.app.service.DataCollectionService.logger.w("We are about to run out of memory");
                if (com.navdy.client.app.service.DataCollectionService.this.isCollecting) {
                    com.navdy.client.app.service.DataCollectionService.this.writeDataToFileAndStartNewData(com.navdy.client.app.service.DataCollectionService.this.data.tripUuid);
                } else {
                    com.navdy.client.app.service.DataCollectionService.this.moveAnyTempFileToUploadFolderAndSendIfWiFi();
                    com.navdy.client.app.service.DataCollectionService.this.resetTheData();
                }
            }
        }
    }

    static class Anon11 implements com.amazonaws.mobileconnectors.s3.transferutility.TransferListener {
        final /* synthetic */ java.io.File val$file;
        final /* synthetic */ java.io.File[] val$files;

        Anon11(java.io.File file, java.io.File[] fileArr) {
            this.val$file = file;
            this.val$files = fileArr;
        }

        public void onStateChanged(int id, com.amazonaws.mobileconnectors.s3.transferutility.TransferState state) {
            com.navdy.client.app.service.DataCollectionService.logger.d("Uploading File " + this.val$file.getName() + ", onStatChanged : " + id + org.droidparts.contract.SQL.DDL.SEPARATOR + state.name());
            if (state == com.amazonaws.mobileconnectors.s3.transferutility.TransferState.FAILED) {
                com.navdy.client.app.service.DataCollectionService.access$Anon1808();
                com.navdy.client.app.service.DataCollectionService.logger.d("Uploading File " + this.val$file.getName() + " failed. This is attempt number " + com.navdy.client.app.service.DataCollectionService.failCount + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + 3);
                if (com.navdy.client.app.service.DataCollectionService.failCount < 3) {
                    com.navdy.client.app.service.DataCollectionService.sendToS3();
                    return;
                }
                com.navdy.client.app.service.DataCollectionService.failCount = 0;
                com.navdy.client.app.service.DataCollectionService.EraseThisFileAndSendNextFile(this.val$file);
            } else if (state == com.amazonaws.mobileconnectors.s3.transferutility.TransferState.COMPLETED) {
                com.navdy.client.app.service.DataCollectionService.failCount = 0;
                com.navdy.client.app.service.DataCollectionService.EraseThisFileAndSendNextFile(this.val$file);
            }
        }

        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            com.navdy.client.app.service.DataCollectionService.logger.d("Uploading File " + this.val$files[0].getName() + ", onProgressChanged : " + id + ", Size :" + bytesCurrent + ", of " + bytesTotal);
        }

        public void onError(int id, java.lang.Exception ex) {
            com.navdy.client.app.service.DataCollectionService.logger.e("Uploading File " + this.val$file.getName() + ", onError : " + id, ex);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.client.app.service.DataCollectionService.this.updateHudUuid();
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            synchronized (com.navdy.client.app.service.DataCollectionService.this.dataLock) {
                com.navdy.client.app.service.DataCollectionService.this.isConnected = false;
            }
            com.navdy.client.app.service.DataCollectionService.this.checkForStop();
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ int val$confidence;
        final /* synthetic */ boolean val$isDriving;

        Anon4(int i, boolean z) {
            this.val$confidence = i;
            this.val$isDriving = z;
        }

        public void run() {
            synchronized (com.navdy.client.app.service.DataCollectionService.this.dataLock) {
                com.navdy.client.app.service.DataCollectionService.Data.Activity activity = new com.navdy.client.app.service.DataCollectionService.Data.Activity(this.val$confidence);
                activity.isDriving = this.val$isDriving;
                com.navdy.client.app.service.DataCollectionService.this.isDriving = this.val$isDriving;
                com.navdy.client.app.service.DataCollectionService.this.data.activities.add(activity);
                if (com.navdy.client.app.service.DataCollectionService.this.data.activities.size() % 1000 == 0) {
                    com.navdy.client.app.service.DataCollectionService.logger.d("ACTIVITY_MONITOR   : nbRecords=" + com.navdy.client.app.service.DataCollectionService.this.data.activities.size() + " \tisDriving=" + this.val$isDriving + " \tconfidence=" + this.val$confidence);
                }
                if (com.navdy.client.app.service.DataCollectionService.this.data.activities.size() > 750000) {
                    com.navdy.client.app.service.DataCollectionService.this.writeDataToFileAndStartNewData(com.navdy.client.app.service.DataCollectionService.this.data.tripUuid);
                }
                com.navdy.client.app.service.DataCollectionService.this.checkForStop();
            }
        }
    }

    class Anon5 implements java.lang.Runnable {
        final /* synthetic */ android.hardware.SensorEvent val$event;

        Anon5(android.hardware.SensorEvent sensorEvent) {
            this.val$event = sensorEvent;
        }

        public void run() {
            com.navdy.client.app.service.DataCollectionService.this.handleSensorEvent(this.val$event);
        }
    }

    class Anon6 implements java.lang.Runnable {
        Anon6() {
        }

        public void run() {
            com.navdy.client.app.service.DataCollectionService.this.addLastLocationToData();
        }
    }

    class Anon7 implements java.lang.Runnable {
        Anon7() {
        }

        public void run() {
            synchronized (com.navdy.client.app.service.DataCollectionService.this.dataLock) {
                if (com.navdy.client.app.service.DataCollectionService.this.lastLocation != null) {
                    com.navdy.client.app.service.DataCollectionService.logger.d("LOCATION           : nbRecords=" + com.navdy.client.app.service.DataCollectionService.this.data.locations.size() + " \tdata=" + com.navdy.client.app.service.DataCollectionService.this.lastLocation);
                    com.navdy.client.app.service.DataCollectionService.this.data.locations.add(com.navdy.client.app.service.DataCollectionService.this.lastLocation);
                    com.navdy.client.app.service.DataCollectionService.this.lastLocation = null;
                }
                com.navdy.client.app.service.DataCollectionService.this.bgHandler.postDelayed(com.navdy.client.app.service.DataCollectionService.this.locationUpdatesAtRegularInterval, 1000);
            }
        }
    }

    class Anon8 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.TripUpdate val$tripUpdate;

        Anon8(com.navdy.service.library.events.TripUpdate tripUpdate) {
            this.val$tripUpdate = tripUpdate;
        }

        public void run() {
            com.navdy.client.app.service.DataCollectionService.this.handleTripUpdate(this.val$tripUpdate);
        }
    }

    class Anon9 implements java.lang.Runnable {
        Anon9() {
        }

        public void run() {
            com.navdy.client.app.service.DataCollectionService.this.moveAnyTempFileToUploadFolderAndSendIfWiFi();
        }
    }

    static class Data {
        static final int MAX_RECORDS = 750000;
        static final int UNKNOWN_ACCURACY = -1;
        java.util.LinkedList<com.navdy.client.app.service.DataCollectionService.Data.Activity> activities = new java.util.LinkedList<>();
        java.lang.String appVersion = com.navdy.client.app.ui.homescreen.HomescreenActivity.getAppVersionString();
        java.lang.String device = (android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL);
        long endTimestamp;
        java.util.LinkedList<com.navdy.client.app.service.DataCollectionService.Data.SensorData> gravity = new java.util.LinkedList<>();
        java.util.LinkedList<com.navdy.client.app.service.DataCollectionService.Data.SensorData> gyroscope = new java.util.LinkedList<>();
        boolean isPersistedToFile = false;
        boolean isSplitData = false;
        float[] lastAccelerometerData = null;
        java.util.LinkedList<com.navdy.client.app.service.DataCollectionService.Data.SensorData> linearAcceleration = new java.util.LinkedList<>();
        java.util.LinkedList<com.navdy.client.app.service.DataCollectionService.Data.Location> locations = new java.util.LinkedList<>();
        java.util.LinkedList<com.navdy.client.app.service.DataCollectionService.Data.SensorData> magneticField = new java.util.LinkedList<>();
        java.util.LinkedList<com.navdy.client.app.service.DataCollectionService.Data.SensorData> orientation = new java.util.LinkedList<>();
        java.lang.String osVersion = (android.os.Build.VERSION.SDK_INT + "(" + android.os.Build.VERSION.RELEASE + ")");
        java.lang.String platform = "Android";
        java.util.LinkedList<com.navdy.client.app.service.DataCollectionService.Data.SensorData> rotationVector = new java.util.LinkedList<>();
        long startTimestamp;
        int timeZoneOffset;
        long tripUuid;

        public static class Activity {
            int confidence = 0;
            boolean isDriving = true;
            long timestamp;

            public Activity(int confidence2) {
                this.confidence = confidence2;
                this.timestamp = java.lang.System.currentTimeMillis();
            }

            public java.lang.String toString() {
                return "Activity{confidence=" + this.confidence + ", isDriving=" + this.isDriving + ", timestamp=" + com.navdy.client.app.service.DataCollectionService.getTimestampString(this.timestamp) + '}';
            }

            void writeToJson(@android.support.annotation.NonNull android.util.JsonWriter writer) throws java.io.IOException {
                writer.beginObject();
                writer.name("timestamp").value(com.navdy.client.app.service.DataCollectionService.getTimestampString(this.timestamp));
                writer.name("confidence").value((long) this.confidence);
                writer.name("is_driving").value(this.isDriving);
                writer.endObject();
            }
        }

        public static class Location {
            java.lang.Float accuracy;
            java.lang.Double altitude;
            java.lang.Float heading;
            java.lang.Double latitude;
            java.lang.Double longitude;
            java.lang.Float speed;
            java.lang.Long timestamp;
            int tripDistance;

            public java.lang.String toString() {
                return "Location{  timestamp=" + com.navdy.client.app.service.DataCollectionService.getTimestampString(this.timestamp.longValue()) + ", \tlatitude=" + this.latitude + ", \tlongitude=" + this.longitude + ", \taltitude='" + this.altitude + '\'' + ", \tspeed=" + this.speed + ", \theading=" + this.heading + ", \taccuracy='" + this.accuracy + '\'' + ", \ttripDistance=" + this.tripDistance + '}';
            }

            void writeToJson(@android.support.annotation.NonNull android.util.JsonWriter writer) throws java.io.IOException {
                writer.beginObject();
                if (this.accuracy != null) {
                    writer.name("accuracy").value(this.accuracy);
                }
                if (this.altitude != null) {
                    writer.name("altitude").value(com.navdy.client.app.service.DataCollectionService.getDoubleString(this.altitude.doubleValue()));
                }
                if (this.heading != null) {
                    writer.name("heading").value(this.heading);
                }
                if (this.latitude != null) {
                    writer.name("latitude").value(this.latitude);
                }
                if (this.longitude != null) {
                    writer.name("longitude").value(this.longitude);
                }
                if (this.speed != null) {
                    writer.name(com.amazonaws.mobileconnectors.s3.transferutility.TransferTable.COLUMN_SPEED).value(this.speed);
                }
                writer.name("trip-distance").value((long) this.tripDistance);
                writer.name("timestamp").value(com.navdy.client.app.service.DataCollectionService.getTimestampString(this.timestamp.longValue()));
                writer.endObject();
            }
        }

        public static class OrientationData extends com.navdy.client.app.service.DataCollectionService.Data.SensorData {
            float dt = 0.0f;

            OrientationData(android.hardware.SensorEvent event) {
                super(event);
            }

            public java.lang.String toString() {
                return "SensorData{timestamp=" + com.navdy.client.app.service.DataCollectionService.getTimestampString(this.timestamp) + ", \tdt=" + this.dt + ", \tazimuth=" + this.values[0] + ", \tpitch=" + this.values[1] + ", \troll=" + this.values[2] + (this.accuracy != -1 ? ", \taccuracy=" + this.accuracy : "") + '}';
            }

            public void writeToJson(@android.support.annotation.NonNull android.util.JsonWriter writer) throws java.io.IOException {
                writer.beginObject();
                if (this.accuracy != -1) {
                    writer.name("accuracy").value((long) this.accuracy);
                }
                writer.name("timestamp").value(com.navdy.client.app.service.DataCollectionService.getTimestampString(this.timestamp));
                writer.name("dt").value((double) this.dt);
                writer.name("azimuth").value((double) this.values[0]);
                writer.name("pitch").value((double) this.values[1]);
                writer.name("roll").value((double) this.values[2]);
                writer.endObject();
            }
        }

        public static class SensorData {
            int accuracy = -1;
            boolean hasW = false;
            long timestamp;
            float[] values;

            SensorData(android.hardware.SensorEvent event) {
                this.timestamp = getEpochTimestamp(event);
                this.values = (float[]) event.values.clone();
            }

            private long getEpochTimestamp(android.hardware.SensorEvent event) {
                long theRightDiff;
                if (com.navdy.client.app.service.DataCollectionService.useNanoTime == null) {
                    long nanoTime = java.lang.System.nanoTime();
                    long realtimeNanos = android.os.SystemClock.elapsedRealtimeNanos();
                    long nanoTimeDiff = java.lang.Math.abs(event.timestamp - nanoTime);
                    long realtimeNanosDiff = java.lang.Math.abs(event.timestamp - realtimeNanos);
                    if (nanoTimeDiff > realtimeNanosDiff) {
                        theRightDiff = event.timestamp - realtimeNanos;
                        if (realtimeNanosDiff > 0 && nanoTimeDiff > 2 * realtimeNanosDiff) {
                            com.navdy.client.app.service.DataCollectionService.useNanoTime = java.lang.Boolean.valueOf(false);
                        }
                    } else {
                        theRightDiff = event.timestamp - nanoTime;
                        if (nanoTimeDiff > 0 && realtimeNanosDiff > 2 * nanoTimeDiff) {
                            com.navdy.client.app.service.DataCollectionService.useNanoTime = java.lang.Boolean.valueOf(true);
                        }
                    }
                } else {
                    theRightDiff = com.navdy.client.app.service.DataCollectionService.useNanoTime.booleanValue() ? event.timestamp - java.lang.System.nanoTime() : event.timestamp - android.os.SystemClock.elapsedRealtimeNanos();
                }
                return (theRightDiff / 1000000) + java.lang.System.currentTimeMillis();
            }

            public java.lang.String toString() {
                return "SensorData{timestamp=" + com.navdy.client.app.service.DataCollectionService.getTimestampString(this.timestamp) + ", \tvalues=" + java.util.Arrays.toString(this.values) + (this.accuracy != -1 ? ", \taccuracy=" + this.accuracy : "") + '}';
            }

            public void writeToJson(@android.support.annotation.NonNull android.util.JsonWriter writer) throws java.io.IOException {
                if (!java.lang.Float.isNaN(this.values[0]) && !java.lang.Float.isNaN(this.values[1]) && !java.lang.Float.isNaN(this.values[2])) {
                    writer.beginObject();
                    if (this.accuracy != -1) {
                        writer.name("accuracy").value((long) this.accuracy);
                    }
                    writer.name("timestamp").value(com.navdy.client.app.service.DataCollectionService.getTimestampString(this.timestamp));
                    writer.name("x").value((double) this.values[0]);
                    writer.name("y").value((double) this.values[1]);
                    writer.name("z").value((double) this.values[2]);
                    if (this.hasW && this.values.length > 3 && !java.lang.Float.isNaN(this.values[3])) {
                        writer.name("w").value((double) this.values[3]);
                    }
                    writer.endObject();
                }
            }
        }

        Data() {
        }

        int getLastTripDistance() {
            if (this.locations == null || this.locations.size() <= 0) {
                return 0;
            }
            return ((com.navdy.client.app.service.DataCollectionService.Data.Location) this.locations.getLast()).tripDistance;
        }

        public java.lang.String toString() {
            return "Data{appVersion='" + this.appVersion + '\'' + ", device='" + this.device + '\'' + ", platform='" + this.platform + '\'' + ", osVersion='" + this.osVersion + '\'' + ", tripUuid=" + this.tripUuid + ", timeZoneOffset=" + this.timeZoneOffset + ", startTimestamp=" + this.startTimestamp + ", endTimestamp=" + this.endTimestamp + ", locations.size=" + this.locations.size() + ", activities.size=" + this.activities.size() + ", orientation.size=" + this.orientation.size() + ", linearAcceleration.size=" + this.linearAcceleration.size() + ", gravity.size=" + this.gravity.size() + ", magneticField.size=" + this.magneticField.size() + ", gyroscope.size=" + this.gyroscope.size() + ", rotationVector.size=" + this.rotationVector.size() + ", isPersistedToFile=" + this.isPersistedToFile + ", isSplitData=" + this.isSplitData + '}';
        }
    }

    private static class DataJsonFormat {
        static final java.lang.String ACTIVITIES = "android_drivings";
        static final java.lang.String ANDROID_SENSORS = "android_sensors";
        static final java.lang.String APP_VERSION = "app_version";
        static final java.lang.String DEVICE = "device";
        static final java.lang.String ENDTIMESTAMP = "endTimestamp";
        static final java.lang.String GRAVITY = "gravities";
        static final java.lang.String GYROSCOPE = "gyroscopes";
        static final java.lang.String IS_SPLIT_DATA = "is_split_data";
        static final java.lang.String LINEAR_ACCELERATION = "linear_accelerations";
        static final java.lang.String LOCATIONS = "locations";
        static final java.lang.String MAGNETIC_FIELD = "magnetic_fields";
        static final java.lang.String ORIENTATION = "orientations";
        static final java.lang.String OS_VERSION = "os_version";
        static final java.lang.String PLATFORM = "platform";
        static final java.lang.String ROTATION_VECTOR = "rotation_vectors";
        static final java.lang.String STARTTIMESTAMP = "startTimestamp";
        static final java.lang.String TIME_ZONE_OFFSET = "time_zone_offset";
        static final java.lang.String TRIP_UUID = "trip_uuid";
        static final java.lang.String VEHICLE_MAKE = "vehicle-make";
        static final java.lang.String VEHICLE_MODEL = "vehicle-model";
        static final java.lang.String VEHICLE_YEAR = "vehicle-year";
        static final java.lang.String VERSION = "version";
        static final int VERSION_NUMBER = 1;

        static class Activity {
            static final java.lang.String CONFIDENCE = "confidence";
            static final java.lang.String IS_DRIVING = "is_driving";
            static final java.lang.String TIMESTAMP = "timestamp";

            Activity() {
            }
        }

        static class Location {
            static final java.lang.String ACCURACY = "accuracy";
            static final java.lang.String ALTITUDE = "altitude";
            static final java.lang.String HEADING = "heading";
            static final java.lang.String LATITUDE = "latitude";
            static final java.lang.String LONGITUDE = "longitude";
            static final java.lang.String SPEED = "speed";
            static final java.lang.String TIMESTAMP = "timestamp";
            static final java.lang.String TRIP_DISTANCE = "trip-distance";

            Location() {
            }
        }

        static class SensorData {
            static final java.lang.String ACCURACY = "accuracy";
            static final java.lang.String AZIMUTH = "azimuth";
            static final java.lang.String DT = "dt";
            static final java.lang.String PITCH = "pitch";
            static final java.lang.String ROLL = "roll";
            static final java.lang.String TIMESTAMP = "timestamp";
            static final java.lang.String W = "w";
            static final java.lang.String X = "x";
            static final java.lang.String Y = "y";
            static final java.lang.String Z = "z";

            SensorData() {
            }
        }

        private DataJsonFormat() {
        }
    }

    private static class InstanceHolder {
        static final com.navdy.client.app.service.DataCollectionService instance = new com.navdy.client.app.service.DataCollectionService();

        private InstanceHolder() {
        }
    }

    static /* synthetic */ int access$Anon1808() {
        int i = failCount;
        failCount = i + 1;
        return i;
    }

    public static com.navdy.client.app.service.DataCollectionService getInstance() {
        return com.navdy.client.app.service.DataCollectionService.InstanceHolder.instance;
    }

    static java.lang.String getTimestampString(long timestamp) {
        java.lang.String str1 = java.lang.String.valueOf(timestamp / 1000);
        return str1 + "." + java.lang.String.format(java.util.Locale.US, "%03d", new java.lang.Object[]{java.lang.Long.valueOf(timestamp % 1000)});
    }

    static java.lang.String getDoubleString(double d) {
        return formatter.format(d);
    }

    DataCollectionService() {
    }

    public void startService(android.content.Context context) {
        ensureFolderExists();
        moveAnyTempFileToUploadFolderAndSendIfWiFi();
        if (!shouldNotCollectData()) {
            com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
            android.os.HandlerThread bgHandlerThread = new android.os.HandlerThread("BgHandlerThread");
            bgHandlerThread.start();
            this.bgHandler = new android.os.Handler(bgHandlerThread.getLooper());
            android.os.HandlerThread uploadThread = new android.os.HandlerThread("DataCollectionHandler");
            uploadThread.start();
            this.uploadScheduler = new android.os.Handler(uploadThread.getLooper());
            this.sensorManager = (android.hardware.SensorManager) context.getSystemService("sensor");
            if (this.sensorManager == null) {
                logger.e("Unable to get sensor manager!!!");
                return;
            }
            this.accelerometer = this.sensorManager.getDefaultSensor(1);
            if (this.accelerometer == null) {
                logger.e("The phone doesn't seem to have an accelerometer :'(");
            }
            this.linearAcceleration = this.sensorManager.getDefaultSensor(10);
            if (this.linearAcceleration == null) {
                logger.e("The phone doesn't seem to have a linearAcceleration :'(");
            }
            this.gravity = this.sensorManager.getDefaultSensor(9);
            if (this.gravity == null) {
                logger.e("The phone doesn't seem to have a gravity :'(");
            }
            this.magneticField = this.sensorManager.getDefaultSensor(2);
            if (this.magneticField == null) {
                logger.e("The phone doesn't seem to have a magneticField :'(");
            }
            this.gyroscope = this.sensorManager.getDefaultSensor(4);
            if (this.gyroscope == null) {
                logger.e("The phone doesn't seem to have a gyroscope :'(");
            }
            this.rotationVector = this.sensorManager.getDefaultSensor(11);
            if (this.rotationVector == null) {
                logger.e("The phone doesn't seem to have a rotationVector :'(");
            }
            initGoogleApi(context);
        }
    }

    private synchronized void initGoogleApi(android.content.Context context) {
        this.googleApiClient = new com.google.android.gms.common.api.GoogleApiClient.Builder(context).addApi(com.google.android.gms.location.ActivityRecognition.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
        this.googleApiClient.connect();
    }

    @com.squareup.otto.Subscribe
    public void onDeviceConnected(com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent deviceConnectedEvent) {
        logger.v("onDeviceConnected");
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.service.DataCollectionService.Anon1(), 1);
    }

    @com.squareup.otto.Subscribe
    public void onDeviceInfoEvent(com.navdy.client.app.framework.DeviceConnection.DeviceInfoEvent deviceInfoEvent) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.service.DataCollectionService.Anon2(), 1);
    }

    @com.squareup.otto.Subscribe
    public void onDeviceDisconnected(com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent deviceDisconnectedEvent) {
        logger.v("onDeviceDisconnected");
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.service.DataCollectionService.Anon3(), 1);
    }

    @com.squareup.otto.Subscribe
    public void onNavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionStatusEvent event) {
    }

    private java.lang.String getHudUuidFromAppInstance() {
        java.lang.String str;
        com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice != null) {
            com.navdy.service.library.events.DeviceInfo deviceInfo = remoteDevice.getDeviceInfo();
            if (!(deviceInfo == null || deviceInfo.deviceUuid == null)) {
                synchronized (this.dataLock) {
                    str = deviceInfo.deviceUuid;
                }
                return str;
            }
        }
        return null;
    }

    private void updateHudUuid() {
        java.lang.String hudUuid = getHudUuidFromAppInstance();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(hudUuid)) {
            com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.LAST_HUD_UUID, hudUuid).apply();
        }
    }

    private java.lang.String getHudUuid() {
        java.lang.String hudUuid = getHudUuidFromAppInstance();
        return !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(hudUuid) ? hudUuid : com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.LAST_HUD_UUID, com.navdy.client.app.ui.settings.SettingsConstants.LAST_HUD_UUID_DEFAULT);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        logger.i("Starting sensor data collection");
        initDataIfNotAlready();
        r5.bgHandler.postDelayed(r5.locationUpdatesAtRegularInterval, 1000);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002b, code lost:
        if (r5.accelerometer == null) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002d, code lost:
        r5.sensorManager.registerListener(r5, r5.accelerometer, SUPER_FAST_REFRESH_RATE);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0039, code lost:
        if (r5.linearAcceleration == null) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        r5.sensorManager.registerListener(r5, r5.linearAcceleration, FAST_REFRESH_RATE);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0044, code lost:
        if (r5.gravity == null) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0046, code lost:
        r5.sensorManager.registerListener(r5, r5.gravity, FAST_REFRESH_RATE);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004f, code lost:
        if (r5.magneticField == null) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0051, code lost:
        r5.sensorManager.registerListener(r5, r5.magneticField, FAST_REFRESH_RATE);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005a, code lost:
        if (r5.gyroscope == null) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005c, code lost:
        r5.sensorManager.registerListener(r5, r5.gyroscope, FAST_REFRESH_RATE);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0065, code lost:
        if (r5.rotationVector == null) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0067, code lost:
        r5.sensorManager.registerListener(r5, r5.rotationVector, FAST_REFRESH_RATE);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006e, code lost:
        requestActivityRecognitionUpdates();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return;
     */
    public void startCollecting() {
        synchronized (this.dataLock) {
            if (!shouldNotCollectData() && !this.isCollecting) {
                this.isCollecting = true;
            }
        }
    }

    private static boolean shouldNotCollectData() {
        return false;
    }

    private void stopCollecting() {
        logger.i("Stopping sensor data collection");
        this.bgHandler.removeCallbacksAndMessages(null);
        this.sensorManager.unregisterListener(this);
        removeActivityRecognitionUpdates();
        writeDataToZipFileAndScheduleForUpload();
        synchronized (this.dataLock) {
            this.isCollecting = false;
        }
    }

    private android.app.PendingIntent getPendingIntent() {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        return android.app.PendingIntent.getService(context, REQUEST_CODE, new android.content.Intent(context, com.navdy.client.app.service.ActivityRecognizedCallbackService.class), 134217728);
    }

    private synchronized void requestActivityRecognitionUpdates() {
        logger.d("GoogleApiClient : requesting activity updates");
        if (this.googleApiClient == null || !this.googleApiClient.isConnected()) {
            logger.d("The GoogleApiClient is not connected yet. Waiting for it to be connected");
        } else {
            logger.d("GoogleApiClient is already connected, requesting the updates");
            android.app.PendingIntent pendingIntent = getPendingIntent();
            if (com.google.android.gms.location.ActivityRecognition.ActivityRecognitionApi != null) {
                com.google.android.gms.location.ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(this.googleApiClient, 1000, pendingIntent);
            }
        }
    }

    private synchronized void removeActivityRecognitionUpdates() {
        logger.d("GoogleApiClient : removing activity updates");
        if (this.googleApiClient != null && this.googleApiClient.isConnected()) {
            android.app.PendingIntent pendingIntent = getPendingIntent();
            if (com.google.android.gms.location.ActivityRecognition.ActivityRecognitionApi != null) {
                com.google.android.gms.location.ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(this.googleApiClient, pendingIntent);
            }
        }
    }

    public synchronized void onConnected(@android.support.annotation.Nullable android.os.Bundle bundle) {
        logger.d("GoogleApiClient : connected ");
        if (com.navdy.client.app.framework.AppInstance.getInstance().isDeviceConnected()) {
            logger.d("HUD is already connected");
            requestActivityRecognitionUpdates();
        }
    }

    public void onConnectionSuspended(int i) {
        logger.d("GoogleApiClient : onConnectionSuspended , Connection :" + i);
    }

    public void onConnectionFailed(@android.support.annotation.NonNull com.google.android.gms.common.ConnectionResult connectionResult) {
        logger.d("GoogleApiClient : onConnectionFailed , Connection result : " + connectionResult);
    }

    void handleDetectedActivities(boolean isDriving2, int confidence) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.service.DataCollectionService.Anon4(confidence, isDriving2), 1);
    }

    private void checkForStop() {
        synchronized (this.dataLock) {
            if (!this.isDriving && !this.isConnected && this.isCollecting) {
                stopCollecting();
            }
        }
    }

    public final void onAccuracyChanged(android.hardware.Sensor sensor, int accuracy) {
        if (sensor == this.magneticField) {
            this.lastMagFieldAccuracy = accuracy;
        }
    }

    public final void onSensorChanged(android.hardware.SensorEvent event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.service.DataCollectionService.Anon5(event), 1);
    }

    private void handleSensorEvent(android.hardware.SensorEvent event) {
        com.navdy.client.app.service.DataCollectionService.Data.SensorData sensorData = new com.navdy.client.app.service.DataCollectionService.Data.SensorData(event);
        synchronized (this.dataLock) {
            if (event.sensor == this.accelerometer) {
                this.data.lastAccelerometerData = event.values;
            } else if (event.sensor == this.linearAcceleration) {
                if (event.timestamp - this.lastLinearAccelerationTimeStamp >= 83333000) {
                    if (this.data.linearAcceleration.size() % 1000 == 0) {
                        logger.d("LINEAR_ACCELERATION: nbRecords=" + this.data.linearAcceleration.size() + " \tdata=" + sensorData);
                    }
                    this.lastLinearAccelerationTimeStamp = event.timestamp;
                    this.data.linearAcceleration.add(sensorData);
                }
            } else if (event.sensor == this.gravity) {
                if (event.timestamp - this.lastGravityTimeStamp >= 83333000) {
                    if (this.data.gravity.size() % 1000 == 0) {
                        logger.d("GRAVITY            : nbRecords=" + this.data.gravity.size() + " \tdata=" + sensorData);
                    }
                    this.lastGravityTimeStamp = event.timestamp;
                    this.data.gravity.add(sensorData);
                }
            } else if (event.sensor == this.magneticField) {
                if (event.timestamp - this.lastMagneticFieldTimeStamp >= 83333000) {
                    sensorData.accuracy = this.lastMagFieldAccuracy;
                    if (this.data.magneticField.size() % 1000 == 0) {
                        logger.d("MAGNETIC_FIELD     : nbRecords=" + this.data.magneticField.size() + " \tdata=" + sensorData);
                    }
                    this.lastMagneticFieldTimeStamp = event.timestamp;
                    this.data.magneticField.add(sensorData);
                    if (this.data.lastAccelerometerData != null && this.data.lastAccelerometerData.length > 2) {
                        float[] R = new float[9];
                        if (android.hardware.SensorManager.getRotationMatrix(R, new float[9], this.data.lastAccelerometerData, event.values)) {
                            com.navdy.client.app.service.DataCollectionService.Data.OrientationData orientationData = new com.navdy.client.app.service.DataCollectionService.Data.OrientationData(event);
                            android.hardware.SensorManager.getOrientation(R, orientationData.values);
                            if (this.lastOrientationTimeStamp > 0) {
                                orientationData.dt = ((float) (event.timestamp - this.lastOrientationTimeStamp)) / 1000000.0f;
                            }
                            this.lastOrientationTimeStamp = event.timestamp;
                            this.data.orientation.add(orientationData);
                            if (this.data.orientation.size() % 1000 == 0) {
                                logger.d("ORIENTATION        : nbRecords=" + this.data.orientation.size() + " \tdata=" + orientationData);
                            }
                        }
                    }
                }
            } else if (event.sensor == this.gyroscope) {
                if (event.timestamp - this.lastGyroscopeTimeStamp >= 83333000) {
                    if (this.data.gyroscope.size() % 1000 == 0) {
                        logger.d("GYROSCOPE          : nbRecords=" + this.data.gyroscope.size() + " \tdata=" + sensorData);
                    }
                    this.lastGyroscopeTimeStamp = event.timestamp;
                    this.data.gyroscope.add(sensorData);
                }
            } else if (event.sensor == this.rotationVector && event.timestamp - this.lastRotationVectorTimeStamp >= 83333000) {
                if (this.data.rotationVector.size() % 1000 == 0) {
                    logger.d("ROTATION_VECTOR    : nbRecords=" + this.data.rotationVector.size() + " \tdata=" + sensorData);
                }
                this.lastRotationVectorTimeStamp = event.timestamp;
                sensorData.hasW = true;
                this.data.rotationVector.add(sensorData);
            }
            if (this.data.orientation.size() > 750000 || this.data.linearAcceleration.size() > 750000 || this.data.gravity.size() > 750000 || this.data.magneticField.size() > 750000 || this.data.gyroscope.size() > 750000 || this.data.rotationVector.size() > 750000) {
                writeDataToFileAndStartNewData(this.data.tripUuid);
            }
        }
    }

    private void addLastLocationToData() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.service.DataCollectionService.Anon7(), 1);
    }

    @com.squareup.otto.Subscribe
    public void onTripUpdate(com.navdy.service.library.events.TripUpdate tripUpdate) {
        synchronized (this.dataLock) {
            if (this.isCollecting) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.service.DataCollectionService.Anon8(tripUpdate), 1);
            }
        }
    }

    private void handleTripUpdate(com.navdy.service.library.events.TripUpdate tripUpdate) {
        if (tripUpdate != null) {
            if (this.data == null) {
                resetTheData();
            }
            if (this.data.isPersistedToFile) {
                if (tripUpdate.meters_traveled_since_boot.intValue() >= this.data.getLastTripDistance()) {
                    removeAnyTemporaryFiles();
                    this.data.isPersistedToFile = false;
                } else {
                    moveAnyTempFileToUploadFolderAndSendIfWiFi();
                    resetTheData();
                }
            }
            this.lastLocation = new com.navdy.client.app.service.DataCollectionService.Data.Location();
            if (tripUpdate.last_raw_coordinate != null) {
                this.lastLocation.accuracy = tripUpdate.last_raw_coordinate.accuracy;
                this.lastLocation.altitude = tripUpdate.last_raw_coordinate.altitude;
                this.lastLocation.heading = tripUpdate.last_raw_coordinate.bearing;
                this.lastLocation.latitude = tripUpdate.last_raw_coordinate.latitude;
                this.lastLocation.longitude = tripUpdate.last_raw_coordinate.longitude;
                this.lastLocation.speed = tripUpdate.last_raw_coordinate.speed;
                this.lastLocation.timestamp = tripUpdate.last_raw_coordinate.timestamp;
            } else {
                this.lastLocation.timestamp = java.lang.Long.valueOf(java.lang.System.currentTimeMillis());
            }
            this.lastLocation.tripDistance = tripUpdate.meters_traveled_since_boot.intValue();
        }
    }

    private static void ensureFolderExists() {
        if (rootFolder == null || !rootFolder.exists() || folder == null || !folder.exists()) {
            java.io.File internalStorage = com.navdy.client.app.NavdyApplication.getAppContext().getFilesDir();
            java.lang.String appVersionCode = getAppVersionCode();
            java.lang.String rootFolderPath = internalStorage + java.io.File.separator + SENSOR_DATA_FOLDER;
            rootFolder = new java.io.File(rootFolderPath);
            folder = new java.io.File(rootFolderPath + java.io.File.separator + appVersionCode);
            if (!folder.exists() && !folder.mkdirs()) {
                logger.e("Unable to create the output folder for the sensor data: " + folder);
            }
        }
    }

    private void writeDataToFileAndStartNewData(long tripUuid) {
        synchronized (this.dataLock) {
            this.data.isSplitData = true;
            writeDataToZipFileAndScheduleForUpload();
            moveAnyTempFileToUploadFolderAndSendIfWiFi();
            resetTheData();
            this.data.isSplitData = true;
            if (shouldNotCollectData()) {
                stopCollecting();
            }
            logger.i("Starting new sensor data collection");
            this.data.tripUuid = tripUuid;
        }
    }

    private void resetTheData() {
        synchronized (this.dataLock) {
            this.data = new com.navdy.client.app.service.DataCollectionService.Data();
            this.lastLocation = null;
            this.lastMagFieldAccuracy = 0;
            this.lastLinearAccelerationTimeStamp = 0;
            this.lastGravityTimeStamp = 0;
            this.lastMagneticFieldTimeStamp = 0;
            this.lastGyroscopeTimeStamp = 0;
            this.lastRotationVectorTimeStamp = 0;
            this.lastOrientationTimeStamp = 0;
            initDataIfNotAlready();
        }
    }

    private void initDataIfNotAlready() {
        if (this.data == null) {
            this.data = new com.navdy.client.app.service.DataCollectionService.Data();
        }
        if (this.data.startTimestamp == 0) {
            this.data.startTimestamp = java.lang.System.currentTimeMillis();
        }
        if (this.data.timeZoneOffset == 0) {
            java.util.TimeZone tz = java.util.TimeZone.getDefault();
            this.data.timeZoneOffset = tz.getOffset(java.lang.System.currentTimeMillis()) / 1000;
        }
        if (this.data.tripUuid == 0) {
            this.data.tripUuid = java.lang.System.currentTimeMillis();
        }
    }

    private static boolean isAboveTheLimitOfFolderSize() {
        return fileSize(rootFolder) > 52428800 || availableSdCardSpace() < 5242880;
    }

    private static long fileSize(java.io.File fileOrFolder) {
        long result = 0;
        if (fileOrFolder == null || !fileOrFolder.exists()) {
            return 0;
        }
        if (!fileOrFolder.isDirectory()) {
            return 0 + fileOrFolder.length();
        }
        java.io.File[] fileList = fileOrFolder.listFiles();
        if (fileList == null) {
            return 0;
        }
        for (java.io.File file : fileList) {
            result += fileSize(file);
        }
        return result;
    }

    private static long availableSdCardSpace() {
        android.os.StatFs stat = new android.os.StatFs(rootFolder.getPath());
        return stat.getAvailableBlocksLong() * stat.getBlockSizeLong();
    }

    private void writeDataToZipFileAndScheduleForUpload() {
        java.lang.String path = writeDataToZipFile();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(path)) {
            java.io.File file = new java.io.File(path);
            if (!file.exists() || file.length() <= 0) {
                logger.e("Unable find output file: " + file + " doesn't exist or is 0 length");
                if (!file.delete()) {
                    logger.e("Unable to erase data file: " + file);
                    return;
                }
                return;
            }
            scheduleForUpload();
        }
    }

    private void scheduleForUpload() {
        this.uploadScheduler.postDelayed(new com.navdy.client.app.service.DataCollectionService.Anon9(), UPLOAD_DELAY);
    }

    public void handleLowMemory() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.service.DataCollectionService.Anon10(), 1);
    }

    private void moveAnyTempFileToUploadFolderAndSendIfWiFi() {
        moveAnyPendingFileToUploadFolder();
        sendDataToS3IfOnWiFi();
    }

    private void moveAnyPendingFileToUploadFolder() {
        if (this.uploadScheduler != null) {
            this.uploadScheduler.removeCallbacksAndMessages(null);
        }
        ensureFolderExists();
        boolean pendingFilesExist = false;
        java.io.File[] files = rootFolder.listFiles();
        if (files == null) {
            logger.e("Unable to list files in rootFolder: " + rootFolder);
            return;
        }
        for (java.io.File from : files) {
            if (!from.isDirectory()) {
                java.io.File to = new java.io.File(folder.getPath() + java.io.File.separator + from.getName());
                logger.d("Moving " + from + " to " + to);
                if (!from.renameTo(to)) {
                    logger.e("Unable to move " + from + " to upload folder: " + to);
                } else {
                    pendingFilesExist = true;
                }
            }
        }
        if (pendingFilesExist) {
            com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.PENDING_SENSOR_DATA_EXIST, true).apply();
        }
    }

    private void removeAnyTemporaryFiles() {
        if (this.uploadScheduler != null) {
            this.uploadScheduler.removeCallbacksAndMessages(null);
        }
        ensureFolderExists();
        if (rootFolder != null) {
            java.io.File[] files = rootFolder.listFiles();
            if (files == null) {
                logger.e("Unable to list files in rootFolder: " + rootFolder);
                return;
            }
            for (java.io.File file : files) {
                if (!file.isDirectory()) {
                    if (file.delete()) {
                        logger.d("Removed " + file);
                    } else {
                        logger.e("Unable to erase temporary file: " + file);
                    }
                }
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0349 A[SYNTHETIC, Splitter:B:26:0x0349] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x035e A[SYNTHETIC, Splitter:B:35:0x035e] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:37:0x0361=Splitter:B:37:0x0361, B:13:0x031a=Splitter:B:13:0x031a, B:28:0x034c=Splitter:B:28:0x034c} */
    @android.support.annotation.Nullable
    @android.support.annotation.CheckResult
    private java.lang.String writeDataToZipFile() {
        java.lang.String path = null;
        synchronized (this.dataLock) {
            this.data.endTimestamp = java.lang.System.currentTimeMillis();
            android.util.JsonWriter writer = null;
            try {
                ensureFolderExists();
                java.text.SimpleDateFormat photoDateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd'-'HH-mm-ss", java.util.Locale.US);
                java.util.Date currentTime = new java.util.Date();
                path = rootFolder.getPath() + java.io.File.separator + ((getHudUuid() + "-" + photoDateFormat.format(currentTime)) + FILE_EXTENSION);
                logger.i("Writing sensor data to file: " + path);
                android.util.JsonWriter jsonWriter = new android.util.JsonWriter(new java.io.OutputStreamWriter(new java.util.zip.GZIPOutputStream(new java.io.FileOutputStream(path)), "UTF-8"));
                try {
                    jsonWriter.beginObject();
                    jsonWriter.name("version").value(1);
                    jsonWriter.name("app_version").value(this.data.appVersion);
                    jsonWriter.name("device").value(this.data.device);
                    jsonWriter.name("os_version").value(this.data.osVersion);
                    jsonWriter.name(com.here.android.mpa.search.TransitDeparture.DEPARTURE_PLATFORM_KEY_NAME).value(this.data.platform);
                    jsonWriter.name("startTimestamp").value(getTimestampString(this.data.startTimestamp));
                    jsonWriter.name("endTimestamp").value(getTimestampString(this.data.endTimestamp));
                    jsonWriter.name("time_zone_offset").value((long) this.data.timeZoneOffset);
                    jsonWriter.name("trip_uuid").value(this.data.tripUuid);
                    android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
                    java.lang.String yearString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "");
                    java.lang.String makeString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "");
                    java.lang.String modelString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
                    jsonWriter.name(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR).value(yearString);
                    jsonWriter.name(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE).value(makeString);
                    jsonWriter.name(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL).value(modelString);
                    jsonWriter.name("is_split_data").value(this.data.isSplitData);
                    writeActivityData(jsonWriter);
                    writeLocationData(jsonWriter);
                    jsonWriter.name("android_sensors");
                    jsonWriter.beginObject();
                    writeThisSensorData(jsonWriter, "gravities", this.data.gravity);
                    writeThisSensorData(jsonWriter, "gyroscopes", this.data.gyroscope);
                    writeThisSensorData(jsonWriter, "linear_accelerations", this.data.linearAcceleration);
                    writeThisSensorData(jsonWriter, "magnetic_fields", this.data.magneticField);
                    writeThisSensorData(jsonWriter, "orientations", this.data.orientation);
                    writeThisSensorData(jsonWriter, "rotation_vectors", this.data.rotationVector);
                    jsonWriter.endObject();
                    jsonWriter.endObject();
                    this.data.isPersistedToFile = true;
                    if (jsonWriter != null) {
                        try {
                            jsonWriter.close();
                        } catch (java.io.IOException e) {
                            logger.e("Unable to close the file writer for sensor data", e);
                        }
                    }
                    android.util.JsonWriter jsonWriter2 = jsonWriter;
                    return path;
                } catch (java.io.IOException e2) {
                    e = e2;
                    writer = jsonWriter;
                    try {
                        logger.e("Unable to write sensor data to file. Erasing it.", e);
                        com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.client.app.NavdyApplication.getAppContext(), path);
                        if (writer != null) {
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (writer != null) {
                            try {
                                writer.close();
                            } catch (java.io.IOException e3) {
                                logger.e("Unable to close the file writer for sensor data", e3);
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    writer = jsonWriter;
                    if (writer != null) {
                    }
                    throw th;
                }
            } catch (java.io.IOException e4) {
                e = e4;
                logger.e("Unable to write sensor data to file. Erasing it.", e);
                com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.client.app.NavdyApplication.getAppContext(), path);
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (java.io.IOException e5) {
                        logger.e("Unable to close the file writer for sensor data", e5);
                    }
                }
                return null;
            }
        }
    }

    private void writeActivityData(android.util.JsonWriter writer) throws java.io.IOException {
        if (this.data.activities != null && this.data.activities.size() > 0) {
            writer.name("android_drivings");
            writer.beginArray();
            java.util.Iterator it = this.data.activities.iterator();
            while (it.hasNext()) {
                ((com.navdy.client.app.service.DataCollectionService.Data.Activity) it.next()).writeToJson(writer);
            }
            writer.endArray();
        }
    }

    private void writeLocationData(android.util.JsonWriter writer) throws java.io.IOException {
        if (this.data.locations != null && this.data.locations.size() > 0) {
            writer.name("locations");
            writer.beginArray();
            java.util.Iterator it = this.data.locations.iterator();
            while (it.hasNext()) {
                ((com.navdy.client.app.service.DataCollectionService.Data.Location) it.next()).writeToJson(writer);
            }
            writer.endArray();
        }
    }

    private void writeThisSensorData(@android.support.annotation.NonNull android.util.JsonWriter writer, @android.support.annotation.NonNull java.lang.String sensorName, @android.support.annotation.Nullable java.util.LinkedList<com.navdy.client.app.service.DataCollectionService.Data.SensorData> sensorData) throws java.io.IOException {
        if (sensorData != null && sensorData.size() > 0) {
            writer.name(sensorName);
            writer.beginArray();
            java.util.Iterator it = sensorData.iterator();
            while (it.hasNext()) {
                ((com.navdy.client.app.service.DataCollectionService.Data.SensorData) it.next()).writeToJson(writer);
            }
            writer.endArray();
        }
    }

    public static void sendDataToS3IfOnWiFi() {
        if (com.navdy.service.library.util.SystemUtils.isConnectedToWifi(com.navdy.client.app.NavdyApplication.getAppContext())) {
            sendToS3IfNotAlreadySending();
        } else if (android.os.Build.VERSION.SDK_INT >= 24) {
            com.navdy.client.app.framework.service.UnmeteredNetworkConnectivityJobService.scheduleNetworkServiceForNougatOrAbove();
        }
    }

    public static void sendToS3IfNotAlreadySending() {
        if (sendInProgress.getAndSet(true)) {
            logger.d("Sending is already in progress");
        } else {
            sendToS3();
        }
    }

    private static void sendToS3() {
        int i = 0;
        logger.i("Sending sensor data to S3");
        if (rootFolder == null || !rootFolder.exists()) {
            logger.e("sendToS3: Unable to find data rootFolder: " + rootFolder);
            sendInProgress.set(false);
            return;
        }
        java.io.File[] subFolders = rootFolder.listFiles();
        if (subFolders == null || subFolders.length <= 0) {
            finishSendingToS3(rootFolder);
            return;
        }
        boolean calledSend = false;
        int length = subFolders.length;
        while (true) {
            if (i >= length) {
                break;
            }
            java.io.File subFolder = subFolders[i];
            if (subFolder.isDirectory()) {
                sendToS3(subFolder, subFolder.getName());
                calledSend = true;
                break;
            }
            i++;
        }
        if (!calledSend) {
            finishSendingToS3(rootFolder);
        }
    }

    private static void finishSendingToS3(java.io.File folder2) {
        logger.v("sendToS3: Empty data folder: " + folder2);
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.PENDING_SENSOR_DATA_EXIST, false).apply();
        sendInProgress.set(false);
    }

    private static void sendToS3(java.io.File folder2, java.lang.String appVersionCode) {
        java.io.File[] files = folder2.listFiles();
        if (files == null || files.length <= 0) {
            EraseThisFileAndSendNextFile(folder2);
            return;
        }
        java.io.File file = files[0];
        if (file == null) {
            return;
        }
        if (file.length() <= 0 || !file.exists()) {
            EraseThisFileAndSendNextFile(file);
            return;
        }
        java.lang.String key = "insurance-logs/android/" + appVersionCode + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + new java.text.SimpleDateFormat("yyyy-MM", java.util.Locale.US).format(new java.util.Date()) + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + file.getName();
        logger.d("Length of the file being uploaded: " + file.length() + " bytes");
        transferObserver = getInstance().transferUtility.upload(S3_BUCKET_NAME, key, file);
        transferObserver.setTransferListener(new com.navdy.client.app.service.DataCollectionService.Anon11(file, files));
    }

    private static java.lang.String getAppVersionCode() {
        android.content.pm.PackageInfo pInfo = null;
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (pInfo != null && pInfo.versionCode > 0) {
            return java.lang.String.valueOf(pInfo.versionCode);
        }
        if (pInfo == null || pInfo.versionName == null) {
            return context.getString(com.navdy.client.R.string.unknown);
        }
        return pInfo.versionName;
    }

    public void cancelAnyOngoingUpload() {
        if (this.transferUtility != null && transferObserver != null) {
            this.transferUtility.cancel(transferObserver.getId());
        }
    }

    private static void EraseThisFileAndSendNextFile(@android.support.annotation.Nullable java.io.File file) {
        if (file == null || file.delete()) {
            sendToS3();
        } else {
            logger.e("Error while trying to delete file: " + file);
        }
    }
}
