package com.navdy.client.app.ui.homescreen;

public class MicPermissionDialogActivity extends com.navdy.client.app.ui.base.BaseActivity {

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ android.view.View val$v;

        Anon1(android.view.View view) {
            this.val$v = view;
        }

        public void run() {
            com.navdy.client.app.ui.homescreen.MicPermissionDialogActivity.this.onCloseClick(this.val$v);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            android.content.Intent intent = new android.content.Intent(com.navdy.client.app.ui.homescreen.MicPermissionDialogActivity.this.getApplicationContext(), com.navdy.client.app.ui.homescreen.PermissionFailedActivity.class);
            intent.putExtra(com.navdy.client.app.ui.homescreen.PermissionFailedActivity.EXTRA_TITLE_RES, com.navdy.client.R.string.fle_app_setup_microphone_title_fail);
            intent.putExtra(com.navdy.client.app.ui.homescreen.PermissionFailedActivity.EXTRA_DESCRIPTION_RES, com.navdy.client.R.string.fle_app_setup_microphone_desc_fail);
            intent.putExtra(com.navdy.client.app.ui.homescreen.PermissionFailedActivity.EXTRA_BUTTON_RES, com.navdy.client.R.string.fle_app_setup_microphone_button_fail);
            com.navdy.client.app.ui.homescreen.MicPermissionDialogActivity.this.startActivity(intent);
            com.navdy.client.app.ui.homescreen.MicPermissionDialogActivity.this.finish();
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.dialog_mic_permission);
    }

    public void onButtonClick(android.view.View v) {
        requestMicrophonePermission(new com.navdy.client.app.ui.homescreen.MicPermissionDialogActivity.Anon1(v), new com.navdy.client.app.ui.homescreen.MicPermissionDialogActivity.Anon2());
        android.widget.RelativeLayout dialog = (android.widget.RelativeLayout) findViewById(com.navdy.client.R.id.dialog);
        if (dialog != null) {
            dialog.setAlpha(0.0f);
        }
    }

    public void onCloseClick(android.view.View view) {
        if (!isActivityDestroyed()) {
            finish();
        }
    }
}
