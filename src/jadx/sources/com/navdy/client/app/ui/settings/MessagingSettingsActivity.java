package com.navdy.client.app.ui.settings;

public class MessagingSettingsActivity extends com.navdy.client.app.ui.base.BaseEditActivity {
    private com.navdy.client.app.ui.settings.RepliesAdapter adapter;
    private android.support.v7.widget.RecyclerView repliesRecycler;
    private com.navdy.client.app.ui.settings.MessagingSettingsActivity.ReplyClickListener replyClickListener;

    class Anon1 implements com.navdy.client.app.ui.settings.MessagingSettingsActivity.ReplyClickListener {

        /* renamed from: com.navdy.client.app.ui.settings.MessagingSettingsActivity$Anon1$Anon1 reason: collision with other inner class name */
        class C0071Anon1 implements android.support.v7.widget.PopupMenu.OnMenuItemClickListener {
            private static final int DELETE_MENU_ITEM = 2131756069;
            private static final int EDIT_MENU_ITEM = 2131756068;
            final /* synthetic */ android.support.v7.widget.PopupMenu val$popup;
            final /* synthetic */ java.lang.String val$reply;

            C0071Anon1(android.support.v7.widget.PopupMenu popupMenu, java.lang.String str) {
                this.val$popup = popupMenu;
                this.val$reply = str;
            }

            public boolean onMenuItemClick(android.view.MenuItem item) {
                this.val$popup.dismiss();
                switch (item.getItemId()) {
                    case com.navdy.client.R.id.menu_delete /*2131756069*/:
                        com.navdy.client.app.ui.settings.MessagingSettingsActivity.this.adapter.delete(this.val$reply);
                        com.navdy.client.app.ui.settings.MessagingSettingsActivity.this.somethingChanged = true;
                        break;
                    default:
                        android.content.Intent intent = new android.content.Intent(com.navdy.client.app.ui.settings.MessagingSettingsActivity.this.getApplicationContext(), com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.class);
                        intent.putExtra(com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.EXTRA_OLD_MESSAGE, this.val$reply);
                        com.navdy.client.app.ui.settings.MessagingSettingsActivity.this.startActivityForResult(intent, com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.EDIT_CODE);
                        break;
                }
                return true;
            }
        }

        Anon1() {
        }

        public void onReplyClick(java.lang.String reply, android.view.View v) {
            android.support.v7.widget.PopupMenu popup = new android.support.v7.widget.PopupMenu(com.navdy.client.app.ui.settings.MessagingSettingsActivity.this, v);
            popup.setOnMenuItemClickListener(new com.navdy.client.app.ui.settings.MessagingSettingsActivity.Anon1.C0071Anon1(popup, reply));
            int menuRes = com.navdy.client.R.menu.menu_messaging_edit_delete;
            if (com.navdy.client.app.ui.settings.MessagingSettingsActivity.this.adapter.getItemCount() <= 3) {
                menuRes = com.navdy.client.R.menu.menu_messaging_edit;
            }
            popup.getMenuInflater().inflate(menuRes, popup.getMenu());
            popup.show();
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.client.app.ui.settings.MessagingSettingsActivity.this.somethingChanged = true;
        }
    }

    public interface ReplyClickListener {
        void onReplyClick(java.lang.String str, android.view.View view);
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.settings_messaging);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.menu_messaging).build();
        this.repliesRecycler = (android.support.v7.widget.RecyclerView) findViewById(com.navdy.client.R.id.replies_list);
        java.util.ArrayList<java.lang.String> replies = getRepliesFromSharedPrefs();
        this.replyClickListener = new com.navdy.client.app.ui.settings.MessagingSettingsActivity.Anon1();
        this.adapter = new com.navdy.client.app.ui.settings.RepliesAdapter(replies, this.replyClickListener, new com.navdy.client.app.ui.settings.MessagingSettingsActivity.Anon2(), this.imageCache);
        if (this.repliesRecycler != null) {
            this.repliesRecycler.setAdapter(this.adapter);
            new android.support.v7.widget.helper.ItemTouchHelper(this.adapter.getItemTouchHelperCallback()).attachToRecyclerView(this.repliesRecycler);
            android.support.v7.widget.LinearLayoutManager layoutManager = new android.support.v7.widget.LinearLayoutManager(this);
            layoutManager.setOrientation(1);
            this.repliesRecycler.setLayoutManager(layoutManager);
        }
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.Settings.MESSAGING);
    }

    protected void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 4242) {
            java.lang.String newMessage = data.getStringExtra(com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.EXTRA_NEW_MESSAGE);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(newMessage)) {
                this.adapter.replace(data.getStringExtra(com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.EXTRA_OLD_MESSAGE), newMessage);
                this.somethingChanged = true;
            } else {
                return;
            }
        } else if (resultCode == 4241) {
            java.lang.String newMessage2 = data.getStringExtra(com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.EXTRA_NEW_MESSAGE);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(newMessage2)) {
                this.adapter.add(newMessage2);
                this.somethingChanged = true;
                this.repliesRecycler.scrollToPosition(this.adapter.getItemCount() - 1);
            }
        }
        if (this.somethingChanged) {
            saveChanges();
        }
    }

    public static java.util.ArrayList<java.lang.String> getRepliesFromSharedPrefs() {
        java.util.ArrayList<java.lang.String> replies = new java.util.ArrayList<>();
        java.lang.String repliesString = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.MESSAGING_REPLIES, null);
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(repliesString)) {
            return convertStringToArray(repliesString);
        }
        replies.addAll(java.util.Arrays.asList(context.getResources().getStringArray(com.navdy.client.R.array.default_replies_list)));
        replies.add("\ud83d\udc4d");
        replies.add("\ud83d\udc4e");
        replies.add("\ud83d\ude00");
        replies.add("\ud83d\ude1e");
        replies.add("\u2764");
        return replies;
    }

    @android.support.annotation.Nullable
    public static java.util.ArrayList<java.lang.String> convertStringToArray(@android.support.annotation.Nullable java.lang.String repliesString) {
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(repliesString)) {
            return null;
        }
        java.util.ArrayList<java.lang.String> replies = new java.util.ArrayList<>();
        try {
            org.json.JSONArray jsonArray = new org.json.JSONObject(repliesString).getJSONArray("replies");
            for (int i = 0; i < jsonArray.length(); i++) {
                replies.add((java.lang.String) jsonArray.get(i));
            }
            return replies;
        } catch (org.json.JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean saveRepliesToSharedPrefsAndSendToHud(java.util.ArrayList<java.lang.String> replies) {
        android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        java.lang.String repliesString = convertArrayToString(replies);
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(repliesString)) {
            return false;
        }
        sharedPrefs.edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.MESSAGING_REPLIES, repliesString).apply();
        com.navdy.client.app.ui.settings.SettingsUtils.sendMessagingSettingsToTheHud(replies, java.lang.Long.valueOf(com.navdy.client.app.ui.settings.SettingsUtils.incrementSerialNumber("nav_serial_number")));
        return true;
    }

    @android.support.annotation.Nullable
    private java.lang.String convertArrayToString(java.util.ArrayList<java.lang.String> replies) {
        java.lang.String str = null;
        if (replies == null || replies.size() <= 0) {
            return str;
        }
        try {
            org.json.JSONArray array = new org.json.JSONArray(replies);
            org.json.JSONObject object = new org.json.JSONObject();
            object.put("replies", array);
            return object.toString();
        } catch (org.json.JSONException e) {
            e.printStackTrace();
            return str;
        }
    }

    public void onAddReplyClick(android.view.View view) {
        startActivityForResult(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.class), com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.ADD_CODE);
    }

    protected void saveChanges() {
        if (saveRepliesToSharedPrefsAndSendToHud(this.adapter.getList())) {
            showShortToast(com.navdy.client.R.string.settings_messaging_succeeded, new java.lang.Object[0]);
        } else {
            showShortToast(com.navdy.client.R.string.settings_messaging_failed, new java.lang.Object[0]);
        }
    }
}
