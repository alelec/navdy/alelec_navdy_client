package com.navdy.client.app.ui.firstlaunch;

public class CablePickerActivity extends com.navdy.client.app.ui.base.BaseActivity {
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_cable_picker);
        loadImage(com.navdy.client.R.id.illustration, com.navdy.client.R.drawable.image_install_cords);
        loadImage(com.navdy.client.R.id.obd2_illustration, com.navdy.client.R.drawable.image_obd_plug);
        loadImage(com.navdy.client.R.id.cla_illustration, com.navdy.client.R.drawable.image_12_v_plug);
    }

    protected void onResume() {
        super.onResume();
        hideSystemUI();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.CABLE_PICKER);
    }

    public void onObd2Click(android.view.View view) {
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.POWER_CABLE_SELECTION, "OBD").apply();
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.InstallActivity.class);
        intent.putExtra("extra_step", com.navdy.client.R.layout.fle_install_locate_obd);
        startActivity(intent);
    }

    public void onClaClick(android.view.View view) {
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.POWER_CABLE_SELECTION, com.navdy.client.app.ui.settings.SettingsConstants.POWER_CABLE_SELECTION_CLA).apply();
        startActivity(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.InstallClaActivity.class));
    }

    public void onBackClick(android.view.View view) {
        finish();
    }
}
