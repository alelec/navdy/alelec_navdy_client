package com.navdy.client.app.ui.homescreen;

class ContentLoadingViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
    private com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.homescreen.ContentLoadingViewHolder.class);
    private android.graphics.drawable.AnimationDrawable loopAnimation = null;

    ContentLoadingViewHolder(android.view.View itemView) {
        super(itemView);
        android.widget.ImageView image = (android.widget.ImageView) itemView.findViewById(com.navdy.client.R.id.illustration);
        if (image != null) {
            this.loopAnimation = (android.graphics.drawable.AnimationDrawable) image.getDrawable();
        }
    }

    void startAnimation() {
        this.logger.d("Starting animation - loopAnimation: " + this.loopAnimation + " isRunning: " + (this.loopAnimation != null ? java.lang.Boolean.valueOf(this.loopAnimation.isRunning()) : com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID));
        if (this.loopAnimation != null && !this.loopAnimation.isRunning()) {
            this.loopAnimation.start();
        }
    }

    void stopAnimation() {
        this.logger.d("Stopping animation - loopAnimation: " + this.loopAnimation + " isRunning: " + (this.loopAnimation != null ? java.lang.Boolean.valueOf(this.loopAnimation.isRunning()) : com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID));
        if (this.loopAnimation != null && this.loopAnimation.isRunning()) {
            this.loopAnimation.stop();
        }
    }
}
