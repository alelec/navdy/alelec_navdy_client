package com.navdy.client.app.ui.settings;

public class FeatureVideoViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
    protected android.widget.TextView duration;
    protected android.widget.ImageView image;
    protected android.view.View row;
    protected android.widget.TextView title;
    protected android.widget.RelativeLayout watchedLayout;

    public FeatureVideoViewHolder(android.view.View itemView) {
        super(itemView);
        this.row = itemView;
        this.duration = (android.widget.TextView) itemView.findViewById(com.navdy.client.R.id.duration);
        this.image = (android.widget.ImageView) itemView.findViewById(com.navdy.client.R.id.illustration);
        this.title = (android.widget.TextView) itemView.findViewById(com.navdy.client.R.id.title);
        this.watchedLayout = (android.widget.RelativeLayout) itemView.findViewById(com.navdy.client.R.id.watched_layout);
    }
}
