package com.navdy.client.app.ui.base;

public class BaseActivity extends android.support.v7.app.AppCompatActivity implements com.navdy.client.app.ui.SimpleDialogFragment.DialogProvider {
    private static final java.lang.String DIALOG = "DIALOG";
    private static final int REQUEST_CALENDAR = 7;
    private static final int REQUEST_CAMERA = 8;
    private static final int REQUEST_CONTACTS = 3;
    private static final int REQUEST_LOCATION = 1;
    private static final int REQUEST_MICROPHONE = 2;
    private static final int REQUEST_PHONE = 4;
    private static final int REQUEST_SMS = 5;
    private static final int REQUEST_STORAGE = 6;
    protected static final boolean VERBOSE = false;
    protected static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.base.BaseActivity.class);
    private static android.widget.Toast toast;
    private final java.util.Queue<java.lang.Runnable> calendarPermissionDenialHandlers = new java.util.LinkedList();
    private final java.util.Queue<java.lang.Runnable> cameraPermissionDenialHandlers = new java.util.LinkedList();
    private final java.util.Queue<java.lang.Runnable> contactPermissionDenialHandlers = new java.util.LinkedList();
    private boolean destroying;
    /* access modifiers changed from: protected */
    public final android.os.Handler handler = new android.os.Handler();
    public com.navdy.client.app.framework.util.ImageCache imageCache = new com.navdy.client.app.framework.util.ImageCache();
    java.util.concurrent.atomic.AtomicBoolean instanceStateIsSaved = new java.util.concurrent.atomic.AtomicBoolean(false);
    private final java.util.Queue<java.lang.Runnable> locationPermissionDenialHandlers = new java.util.LinkedList();
    private final java.lang.Object lock = new java.lang.Object();
    /* access modifiers changed from: protected */
    public final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());
    private android.view.ActionMode mActionMode;
    private volatile boolean mIsInForeground = false;
    private final java.util.Queue<java.lang.Runnable> microphonePermissionDenialHandlers = new java.util.LinkedList();
    private android.app.AlertDialog obdDialog = null;
    private final java.util.Queue<java.lang.Runnable> phonePermissionDenialHandlers = new java.util.LinkedList();
    private android.app.ProgressDialog progressDialog = null;
    private android.app.AlertDialog questionDialog = null;
    private android.app.AlertDialog requestRouteDialog = null;
    private final java.util.Queue<java.lang.Runnable> smsPermissionDenialHandlers = new java.util.LinkedList();
    private final java.util.Queue<java.lang.Runnable> storagePermissionDenialHandlers = new java.util.LinkedList();
    private final java.util.Queue<java.lang.Runnable> thingsThatRequireCalendarPermission = new java.util.LinkedList();
    private final java.util.Queue<java.lang.Runnable> thingsThatRequireCameraPermission = new java.util.LinkedList();
    private final java.util.Queue<java.lang.Runnable> thingsThatRequireContactPermission = new java.util.LinkedList();
    private final java.util.Queue<java.lang.Runnable> thingsThatRequireLocationPermission = new java.util.LinkedList();
    private final java.util.Queue<java.lang.Runnable> thingsThatRequireMicrophonePermission = new java.util.LinkedList();
    private final java.util.Queue<java.lang.Runnable> thingsThatRequirePhonePermission = new java.util.LinkedList();
    private final java.util.Queue<java.lang.Runnable> thingsThatRequireSmsPermission = new java.util.LinkedList();
    private final java.util.Queue<java.lang.Runnable> thingsThatRequireStoragePermission = new java.util.LinkedList();

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ android.app.AlertDialog.Builder val$builder;

        Anon1(android.app.AlertDialog.Builder builder) {
            this.val$builder = builder;
        }

        public void run() {
            com.navdy.client.app.ui.base.BaseActivity.this.createAndShowQuestionDialog(this.val$builder);
        }
    }

    class Anon10 implements java.lang.Runnable {
        Anon10() {
        }

        public void run() {
            com.navdy.client.app.ui.base.BaseActivity.this.questionDialog.dismiss();
        }
    }

    static class Anon11 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Object[] val$args;
        final /* synthetic */ int val$length;
        final /* synthetic */ int val$messageResId;

        Anon11(int i, java.lang.Object[] objArr, int i2) {
            this.val$messageResId = i;
            this.val$args = objArr;
            this.val$length = i2;
        }

        public void run() {
            if (com.navdy.client.app.ui.base.BaseActivity.toast != null) {
                com.navdy.client.app.ui.base.BaseActivity.toast.cancel();
            }
            android.content.Context appContext = com.navdy.client.app.NavdyApplication.getAppContext();
            com.navdy.client.app.ui.base.BaseActivity.toast = android.widget.Toast.makeText(appContext, appContext.getString(this.val$messageResId, this.val$args), this.val$length);
            com.navdy.client.app.ui.base.BaseActivity.toast.show();
        }
    }

    class Anon2 implements android.content.DialogInterface.OnClickListener {
        final /* synthetic */ java.lang.Runnable val$requestRouteRunnable;

        Anon2(java.lang.Runnable runnable) {
            this.val$requestRouteRunnable = runnable;
        }

        public void onClick(android.content.DialogInterface dialogInterface, int i) {
            this.val$requestRouteRunnable.run();
        }
    }

    class Anon3 implements android.content.DialogInterface.OnClickListener {
        Anon3() {
        }

        public void onClick(android.content.DialogInterface dialogInterface, int i) {
        }
    }

    class Anon4 implements android.content.DialogInterface.OnClickListener {
        final /* synthetic */ android.content.SharedPreferences val$sharedPrefs;

        Anon4(android.content.SharedPreferences sharedPreferences) {
            this.val$sharedPrefs = sharedPreferences;
        }

        public void onClick(android.content.DialogInterface dialogInterface, int i) {
            com.navdy.client.app.ui.base.BaseActivity.this.logger.d("user turned obd data off");
            this.val$sharedPrefs.edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.HUD_OBD_ON, com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_OFF.toString()).apply();
        }
    }

    class Anon5 implements android.content.DialogInterface.OnClickListener {
        final /* synthetic */ android.content.SharedPreferences val$sharedPrefs;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.client.app.ui.base.BaseActivity.this.logger.d("user overrode blacklist");
                com.navdy.client.app.ui.base.BaseActivity.Anon5.this.val$sharedPrefs.edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.HUD_OBD_ON, com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_ON.toString()).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_BLACKLIST_OVERRIDDEN, false).apply();
            }
        }

        Anon5(android.content.SharedPreferences sharedPreferences) {
            this.val$sharedPrefs = sharedPreferences;
        }

        public void onClick(android.content.DialogInterface dialogInterface, int i) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.base.BaseActivity.Anon5.Anon1(), 1);
        }
    }

    class Anon6 implements java.lang.Runnable {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.client.app.ui.base.BaseActivity.this.showExplicitObdSettingForBlacklistDialog();
            }
        }

        Anon6() {
        }

        public void run() {
            android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
            if (com.navdy.client.app.ui.settings.SettingsUtils.obdScanIsOn()) {
                boolean overrodeBlacklist = sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_BLACKLIST_OVERRIDDEN, false);
                if (com.navdy.client.app.ui.settings.SettingsUtils.carIsBlacklistedForObdData() && !overrodeBlacklist) {
                    com.navdy.client.app.ui.base.BaseActivity.this.runOnUiThread(new com.navdy.client.app.ui.base.BaseActivity.Anon6.Anon1());
                }
            }
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Runnable val$doThingsThatRequireLocationPermission;

        Anon7(java.lang.Runnable runnable) {
            this.val$doThingsThatRequireLocationPermission = runnable;
        }

        public void run() {
            com.navdy.client.app.framework.location.NavdyLocationManager.getInstance().initLocationServices();
            if (this.val$doThingsThatRequireLocationPermission != null) {
                this.val$doThingsThatRequireLocationPermission.run();
            }
        }
    }

    static class Anon8 implements java.lang.Runnable {
        final /* synthetic */ android.app.ProgressDialog val$finalProgressDialog;

        Anon8(android.app.ProgressDialog progressDialog) {
            this.val$finalProgressDialog = progressDialog;
        }

        public void run() {
            this.val$finalProgressDialog.show();
        }
    }

    static class Anon9 implements java.lang.Runnable {
        final /* synthetic */ android.app.ProgressDialog val$progressDialog;

        Anon9(android.app.ProgressDialog progressDialog) {
            this.val$progressDialog = progressDialog;
        }

        public void run() {
            this.val$progressDialog.dismiss();
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        this.logger.v("::onCreate");
        this.instanceStateIsSaved.set(false);
        super.onCreate(savedInstanceState);
    }

    protected void onStart() {
        this.logger.v("::onStart");
        super.onStart();
    }

    protected void onResume() {
        super.onResume();
        this.logger.v("::onResume");
        this.mIsInForeground = true;
        if (requiresBus()) {
            com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
        }
    }

    protected void onPause() {
        this.logger.v("::onPause");
        this.mIsInForeground = false;
        hideProgressDialog();
        endActionMode();
        if (requiresBus()) {
            com.navdy.client.app.framework.util.BusProvider.getInstance().unregister(this);
        }
        super.onPause();
    }

    protected void onStop() {
        this.logger.v("::onStop");
        super.onStop();
    }

    protected void onDestroy() {
        this.logger.v("::onDestroy");
        this.destroying = true;
        this.thingsThatRequireLocationPermission.clear();
        this.locationPermissionDenialHandlers.clear();
        this.thingsThatRequireContactPermission.clear();
        this.contactPermissionDenialHandlers.clear();
        this.thingsThatRequireSmsPermission.clear();
        this.smsPermissionDenialHandlers.clear();
        this.thingsThatRequirePhonePermission.clear();
        this.phonePermissionDenialHandlers.clear();
        this.thingsThatRequireStoragePermission.clear();
        this.storagePermissionDenialHandlers.clear();
        dismissProgressDialog();
        dismissQuestionDialog();
        this.imageCache.clearCache();
        super.onDestroy();
    }

    public void onBackPressed() {
        try {
            if (!isEnding(this)) {
                super.onBackPressed();
            }
        } catch (java.lang.IllegalStateException e) {
            this.logger.e("Unable to perform back pressed", e);
        }
    }

    public void onSaveInstanceState(android.os.Bundle outState, android.os.PersistableBundle outPersistentState) {
        this.instanceStateIsSaved.set(true);
        super.onSaveInstanceState(outState, outPersistentState);
    }

    public boolean isActivityDestroyed() {
        return this.destroying || isDestroyed() || isFinishing();
    }

    public android.app.Dialog createDialog(int id, android.os.Bundle arguments) {
        if (arguments == null) {
            return null;
        }
        if (arguments.get("title") == null && arguments.get("message") == null) {
            return null;
        }
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setCancelable(true);
        java.lang.String title = arguments.getString("title");
        if (title != null) {
            builder.setTitle(title);
        }
        java.lang.String message = arguments.getString("message");
        if (message != null) {
            builder.setMessage(message);
        }
        java.lang.String positiveButtonTitle = arguments.getString(com.navdy.client.app.ui.SimpleDialogFragment.EXTRA_POSITIVE_BUTTON_TITLE);
        if (positiveButtonTitle != null) {
            builder.setPositiveButton(positiveButtonTitle, null);
        } else {
            builder.setPositiveButton(getString(com.navdy.client.R.string.ok), null);
        }
        return builder.create();
    }

    public boolean isInForeground() {
        return this.mIsInForeground;
    }

    public void showSimpleDialog(int dialogId, @android.support.annotation.StringRes int titleId, @android.support.annotation.StringRes int messageId) {
        showSimpleDialog(dialogId, getString(titleId), getString(messageId));
    }

    public void showSimpleDialog(int dialogId, java.lang.String title, java.lang.String message) {
        android.app.FragmentManager manager = getFragmentManager();
        if (manager != null) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putString("title", title);
            bundle.putString("message", message);
            com.navdy.client.app.ui.SimpleDialogFragment dialogFragment = com.navdy.client.app.ui.SimpleDialogFragment.newInstance(dialogId, bundle);
            if (dialogFragment != null) {
                dialogFragment.show(manager, DIALOG);
            }
        }
    }

    public void showQuestionDialog(@android.support.annotation.StringRes int titleResId, @android.support.annotation.StringRes int messageResId, @android.support.annotation.StringRes int positiveResId, @android.support.annotation.StringRes int negativeResId, @android.support.annotation.NonNull android.content.DialogInterface.OnClickListener dialogClickListener, @android.support.annotation.Nullable android.content.DialogInterface.OnCancelListener onCancelListener) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(titleResId).setMessage(messageResId).setPositiveButton(positiveResId, dialogClickListener).setNegativeButton(negativeResId, dialogClickListener);
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        if (android.os.Looper.myLooper() == android.os.Looper.getMainLooper()) {
            createAndShowQuestionDialog(builder);
        } else {
            runOnUiThread(new com.navdy.client.app.ui.base.BaseActivity.Anon1(builder));
        }
    }

    private void createAndShowQuestionDialog(android.app.AlertDialog.Builder builder) {
        if (!isActivityDestroyed()) {
            try {
                this.questionDialog = builder.create();
                this.questionDialog.show();
            } catch (Throwable t) {
                this.logger.e("Error happened while trying to create question dialog: " + builder, t);
            }
        }
    }

    public void showRequestNewRouteDialog(java.lang.Runnable requestRouteRunnable) {
        if (isActivityDestroyed() || !com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance().isInOneOfTheActiveTripStates()) {
            requestRouteRunnable.run();
            return;
        }
        android.content.DialogInterface.OnClickListener dialogClickListener = new com.navdy.client.app.ui.base.BaseActivity.Anon2(requestRouteRunnable);
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(com.navdy.client.R.string.request_new_route_dialog_title).setMessage(com.navdy.client.R.string.request_new_route_dialog_message).setPositiveButton(com.navdy.client.R.string.yes_button, dialogClickListener).setNegativeButton(com.navdy.client.R.string.cancel_button, new com.navdy.client.app.ui.base.BaseActivity.Anon3());
        if (this.requestRouteDialog != null) {
            this.requestRouteDialog.hide();
        }
        this.requestRouteDialog = builder.create();
        this.requestRouteDialog.show();
    }

    public void showExplicitObdSettingForBlacklistDialog() {
        android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(com.navdy.client.R.string.settings_obd_warning_title).setMessage(com.navdy.client.R.string.settings_obd_warning_description).setPositiveButton(com.navdy.client.R.string.enable_button, new com.navdy.client.app.ui.base.BaseActivity.Anon5(sharedPrefs)).setNegativeButton(com.navdy.client.R.string.cancel, new com.navdy.client.app.ui.base.BaseActivity.Anon4(sharedPrefs));
        if (!isActivityDestroyed()) {
            if (this.obdDialog != null) {
                this.obdDialog.hide();
            }
            this.obdDialog = builder.create();
            this.obdDialog.show();
        }
    }

    public void showObdDialogIfCarHasBeenAddedToBlacklist() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.base.BaseActivity.Anon6(), 1);
    }

    private static java.lang.String[] getLocationPermissions() {
        java.lang.String[] permissions = {"android.permission.ACCESS_FINE_LOCATION"};
        if (android.os.Build.VERSION.SDK_INT <= 25) {
            return permissions;
        }
        return new java.lang.String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION"};
    }

    private static java.lang.String getMicrophonePermissions() {
        return "android.permission.RECORD_AUDIO";
    }

    @android.support.annotation.NonNull
    private static java.lang.String getContactsPermission() {
        return "android.permission.READ_CONTACTS";
    }

    @android.support.annotation.NonNull
    private static java.lang.String[] getSmsPermissions() {
        java.lang.String[] permissions = {"android.permission.RECEIVE_SMS"};
        if (android.os.Build.VERSION.SDK_INT <= 25) {
            return permissions;
        }
        return new java.lang.String[]{"android.permission.RECEIVE_SMS", "android.permission.SEND_SMS"};
    }

    @android.support.annotation.NonNull
    private static java.lang.String[] getPhonePermissions() {
        java.lang.String[] permissions = {"android.permission.CALL_PHONE"};
        if (android.os.Build.VERSION.SDK_INT <= 25) {
            return permissions;
        }
        return new java.lang.String[]{"android.permission.CALL_PHONE", "android.permission.PROCESS_OUTGOING_CALLS", "android.permission.READ_PHONE_STATE", "android.permission.ANSWER_PHONE_CALLS", "android.permission.READ_PHONE_NUMBERS"};
    }

    @android.support.annotation.NonNull
    private static java.lang.String[] getStoragePermissions() {
        java.lang.String[] permissions = {"android.permission.WRITE_EXTERNAL_STORAGE"};
        if (android.os.Build.VERSION.SDK_INT <= 25) {
            return permissions;
        }
        return new java.lang.String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
    }

    @android.support.annotation.NonNull
    private static java.lang.String getCalendarPermissions() {
        return "android.permission.READ_CALENDAR";
    }

    @android.support.annotation.NonNull
    private static java.lang.String getCameraPermissions() {
        return "android.permission.CAMERA";
    }

    public static boolean weHaveAllPermissions() {
        return weHaveNotificationPermission() && weHaveContactsPermission() && weHaveLocationPermission() && weHaveMicrophonePermission() && weHaveSmsPermission() && weHavePhonePermission() && weHaveStoragePermission() && weHaveCalendarPermission();
    }

    public static boolean weHaveNotificationPermission() {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        java.lang.String enabledNotificationListeners = android.provider.Settings.Secure.getString(context.getContentResolver(), "enabled_notification_listeners");
        return enabledNotificationListeners != null && enabledNotificationListeners.contains(context.getPackageName());
    }

    public static boolean weHaveLocationPermission() {
        return weHaveThisPermission(com.navdy.client.app.NavdyApplication.getAppContext(), getLocationPermissions());
    }

    public static boolean weHaveMicrophonePermission() {
        return weHaveThisPermission(com.navdy.client.app.NavdyApplication.getAppContext(), getMicrophonePermissions());
    }

    public static boolean weHaveContactsPermission() {
        return weHaveThisPermission(com.navdy.client.app.NavdyApplication.getAppContext(), getContactsPermission());
    }

    public static boolean weHaveSmsPermission() {
        return weHaveThisPermission(com.navdy.client.app.NavdyApplication.getAppContext(), getSmsPermissions());
    }

    public static boolean weHavePhonePermission() {
        return weHaveThisPermission(com.navdy.client.app.NavdyApplication.getAppContext(), getPhonePermissions());
    }

    public static boolean weHaveStoragePermission() {
        return weHaveThisPermission(com.navdy.client.app.NavdyApplication.getAppContext(), getStoragePermissions());
    }

    public static boolean weHaveCalendarPermission() {
        return weHaveThisPermission(com.navdy.client.app.NavdyApplication.getAppContext(), getCalendarPermissions());
    }

    public static boolean weHaveCameraPermission() {
        return weHaveThisPermission(com.navdy.client.app.NavdyApplication.getAppContext(), getCameraPermissions());
    }

    public boolean weHaveThisPermission(java.lang.String permission) {
        return weHaveThisPermission(getApplicationContext(), permission);
    }

    public static boolean weHaveThisPermission(android.content.Context context, @android.support.annotation.NonNull java.lang.String[] permissions) {
        boolean hasPermissions = true;
        for (java.lang.String permission : permissions) {
            if (!hasPermissions || !weHaveThisPermission(context, permission)) {
                hasPermissions = false;
            } else {
                hasPermissions = true;
            }
        }
        return hasPermissions;
    }

    public static boolean weHaveThisPermission(android.content.Context context, java.lang.String permission) {
        if (context == null) {
            context = com.navdy.client.app.NavdyApplication.getAppContext();
        }
        if (context != null && android.support.v4.content.ContextCompat.checkSelfPermission(context, permission) == 0) {
            return true;
        }
        return false;
    }

    private void requestThisPermission(java.lang.String permission, java.util.Queue<java.lang.Runnable> thingsThatRequireThisPermission, java.util.Queue<java.lang.Runnable> thisPermissionDenialHandlers, java.lang.Runnable doThingsThatRequireThisPermission, java.lang.Runnable handleThisPermissionDenial, int requestCode) {
        requestThisPermission(new java.lang.String[]{permission}, thingsThatRequireThisPermission, thisPermissionDenialHandlers, doThingsThatRequireThisPermission, handleThisPermissionDenial, requestCode);
    }

    private void requestThisPermission(java.lang.String[] permissions, java.util.Queue<java.lang.Runnable> thingsThatRequireThisPermission, java.util.Queue<java.lang.Runnable> thisPermissionDenialHandlers, java.lang.Runnable doThingsThatRequireThisPermission, java.lang.Runnable handleThisPermissionDenial, int requestCode) {
        boolean weDontHavePermission = false;
        for (java.lang.String permission : permissions) {
            weDontHavePermission = weDontHavePermission || android.support.v4.content.ContextCompat.checkSelfPermission(getApplicationContext(), permission) != 0;
        }
        if (weDontHavePermission) {
            boolean showTheToast = false;
            for (java.lang.String permission2 : permissions) {
                boolean thisIsTheFirstTimeWeAsk = isThisTheFirstTimeWeRequestedThisPermission(permission2);
                showTheToast = showTheToast || (!android.support.v4.app.ActivityCompat.shouldShowRequestPermissionRationale(this, permission2) && !thisIsTheFirstTimeWeAsk);
                if (thisIsTheFirstTimeWeAsk) {
                    com.navdy.client.app.ui.settings.SettingsUtils.getPermissionsSharedPreferences().edit().putBoolean(permission2, false).apply();
                }
            }
            if (showTheToast) {
                showLongToast(com.navdy.client.R.string.need_permissions, new java.lang.Object[0]);
            }
            synchronized (this.lock) {
                if (!(doThingsThatRequireThisPermission == null || thingsThatRequireThisPermission == null)) {
                    thingsThatRequireThisPermission.add(doThingsThatRequireThisPermission);
                }
                if (!(handleThisPermissionDenial == null || thisPermissionDenialHandlers == null)) {
                    thisPermissionDenialHandlers.add(handleThisPermissionDenial);
                }
            }
            android.support.v4.app.ActivityCompat.requestPermissions(this, permissions, requestCode);
        } else if (doThingsThatRequireThisPermission != null) {
            doThingsThatRequireThisPermission.run();
        }
    }

    public static boolean isThisTheFirstTimeWeRequestedThisPermission(java.lang.String permission) {
        return com.navdy.client.app.ui.settings.SettingsUtils.getPermissionsSharedPreferences().getBoolean(permission, true);
    }

    public static boolean isThisTheFirstTimeWeRequestedThisPermission(java.lang.String[] permissions) {
        android.content.SharedPreferences sp = com.navdy.client.app.ui.settings.SettingsUtils.getPermissionsSharedPreferences();
        boolean isFirstTime = true;
        for (java.lang.String permission : permissions) {
            if (!isFirstTime || !sp.getBoolean(permission, true)) {
                isFirstTime = false;
            } else {
                isFirstTime = true;
            }
        }
        return isFirstTime;
    }

    public static boolean alreadyAskedForLocationPermission() {
        return !isThisTheFirstTimeWeRequestedThisPermission(getLocationPermissions());
    }

    public static boolean alreadyAskedForMicrophonePermission() {
        return !isThisTheFirstTimeWeRequestedThisPermission(getMicrophonePermissions());
    }

    public static boolean alreadyAskedForContactsPermission() {
        return !isThisTheFirstTimeWeRequestedThisPermission(getContactsPermission());
    }

    public static boolean alreadyAskedForSmsPermission() {
        return !isThisTheFirstTimeWeRequestedThisPermission(getSmsPermissions());
    }

    public static boolean alreadyAskedForPhonePermission() {
        return !isThisTheFirstTimeWeRequestedThisPermission(getPhonePermissions());
    }

    public static boolean alreadyAskedForStoragePermission() {
        return !isThisTheFirstTimeWeRequestedThisPermission(getStoragePermissions());
    }

    public static boolean alreadyAskedForCalendarPermission() {
        return !isThisTheFirstTimeWeRequestedThisPermission(getCalendarPermissions());
    }

    public static boolean alreadyAskedForCameraPermission() {
        return !isThisTheFirstTimeWeRequestedThisPermission(getCameraPermissions());
    }

    public static void requestLocationPermission(java.lang.Runnable doThingsThatRequireLocationPermission, java.lang.Runnable handleLocationPermissionDenial, android.app.Activity activity) {
        if (activity != null && (activity instanceof com.navdy.client.app.ui.base.BaseActivity)) {
            ((com.navdy.client.app.ui.base.BaseActivity) activity).requestLocationPermission(doThingsThatRequireLocationPermission, handleLocationPermissionDenial);
        } else if (weHaveLocationPermission()) {
            doThingsThatRequireLocationPermission.run();
        } else {
            handleLocationPermissionDenial.run();
        }
    }

    public void requestLocationPermission(java.lang.Runnable doThingsThatRequireLocationPermission, java.lang.Runnable handleLocationPermissionDenial) {
        requestThisPermission(getLocationPermissions(), this.thingsThatRequireLocationPermission, this.locationPermissionDenialHandlers, (java.lang.Runnable) new com.navdy.client.app.ui.base.BaseActivity.Anon7(doThingsThatRequireLocationPermission), handleLocationPermissionDenial, 1);
    }

    public void requestMicrophonePermission(java.lang.Runnable doThingsThatRequireMicrophonePermission, java.lang.Runnable handleMicrophonePermissionDenial) {
        requestThisPermission(getMicrophonePermissions(), this.thingsThatRequireMicrophonePermission, this.microphonePermissionDenialHandlers, doThingsThatRequireMicrophonePermission, handleMicrophonePermissionDenial, 2);
    }

    public void requestContactsPermission(java.lang.Runnable doThingsThatRequireContactPermission, java.lang.Runnable handleContactPermissionDenial) {
        requestThisPermission(getContactsPermission(), this.thingsThatRequireContactPermission, this.contactPermissionDenialHandlers, doThingsThatRequireContactPermission, handleContactPermissionDenial, 3);
    }

    public void requestSmsPermission(java.lang.Runnable doThingsThatRequireSmsPermission, java.lang.Runnable handleSmsPermissionDenial) {
        requestThisPermission(getSmsPermissions(), this.thingsThatRequireSmsPermission, this.smsPermissionDenialHandlers, doThingsThatRequireSmsPermission, handleSmsPermissionDenial, 5);
    }

    public void requestPhonePermission(java.lang.Runnable doThingsThatRequirePhonePermission, java.lang.Runnable handlePhonePermissionDenial) {
        requestThisPermission(getPhonePermissions(), this.thingsThatRequirePhonePermission, this.phonePermissionDenialHandlers, doThingsThatRequirePhonePermission, handlePhonePermissionDenial, 4);
    }

    public void requestStoragePermission(java.lang.Runnable doThingsThatRequireStoragePermission, java.lang.Runnable handleStoragePermissionDenial) {
        requestThisPermission(getStoragePermissions(), this.thingsThatRequireStoragePermission, this.storagePermissionDenialHandlers, doThingsThatRequireStoragePermission, handleStoragePermissionDenial, 6);
    }

    public void requestCalendarPermission(java.lang.Runnable doThingsThatRequireCalendarPermission, java.lang.Runnable handleCalendarPermissionDenial) {
        requestThisPermission(getCalendarPermissions(), this.thingsThatRequireCalendarPermission, this.calendarPermissionDenialHandlers, doThingsThatRequireCalendarPermission, handleCalendarPermissionDenial, 7);
    }

    public void requestCameraPermission(java.lang.Runnable doThingsThatRequireCameraPermission, java.lang.Runnable handleCameraPermissionDenial) {
        requestThisPermission(getCameraPermissions(), this.thingsThatRequireCameraPermission, this.cameraPermissionDenialHandlers, doThingsThatRequireCameraPermission, handleCameraPermissionDenial, 8);
    }

    public void onRequestPermissionsResult(int requestCode, @android.support.annotation.NonNull java.lang.String[] permissions, @android.support.annotation.NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                synchronized (this.lock) {
                    if (grantResults.length <= 0 || grantResults[0] != 0) {
                        java.util.HashMap<java.lang.String, java.lang.String> attributes = new java.util.HashMap<>();
                        attributes.put("Type", "Location");
                        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.PERMISSION_REJECTED, attributes);
                        for (java.lang.Runnable r : this.locationPermissionDenialHandlers) {
                            r.run();
                        }
                    } else {
                        for (java.lang.Runnable r2 : this.thingsThatRequireLocationPermission) {
                            r2.run();
                        }
                    }
                    this.thingsThatRequireLocationPermission.clear();
                    this.locationPermissionDenialHandlers.clear();
                }
                return;
            case 2:
                synchronized (this.lock) {
                    if (grantResults.length <= 0 || grantResults[0] != 0) {
                        java.util.HashMap<java.lang.String, java.lang.String> attributes2 = new java.util.HashMap<>();
                        attributes2.put("Type", com.navdy.client.app.tracking.TrackerConstants.Attributes.MICROPHONE);
                        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.PERMISSION_REJECTED, attributes2);
                        for (java.lang.Runnable r3 : this.microphonePermissionDenialHandlers) {
                            r3.run();
                        }
                    } else {
                        for (java.lang.Runnable r4 : this.thingsThatRequireMicrophonePermission) {
                            r4.run();
                        }
                    }
                    this.thingsThatRequireMicrophonePermission.clear();
                    this.microphonePermissionDenialHandlers.clear();
                }
                return;
            case 3:
                synchronized (this.lock) {
                    if (grantResults.length <= 0 || grantResults[0] != 0) {
                        java.util.HashMap<java.lang.String, java.lang.String> attributes3 = new java.util.HashMap<>();
                        attributes3.put("Type", com.navdy.client.app.tracking.TrackerConstants.Attributes.CONTACTS);
                        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.PERMISSION_REJECTED, attributes3);
                        for (java.lang.Runnable r5 : this.contactPermissionDenialHandlers) {
                            r5.run();
                        }
                    } else {
                        for (java.lang.Runnable r6 : this.thingsThatRequireContactPermission) {
                            r6.run();
                        }
                    }
                    this.thingsThatRequireContactPermission.clear();
                    this.contactPermissionDenialHandlers.clear();
                }
                return;
            case 4:
                synchronized (this.lock) {
                    if (grantResults.length <= 0 || grantResults[0] != 0) {
                        java.util.HashMap<java.lang.String, java.lang.String> attributes4 = new java.util.HashMap<>();
                        attributes4.put("Type", com.navdy.client.app.tracking.TrackerConstants.Attributes.PHONE);
                        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.PERMISSION_REJECTED, attributes4);
                        for (java.lang.Runnable r7 : this.phonePermissionDenialHandlers) {
                            r7.run();
                        }
                    } else {
                        for (java.lang.Runnable r8 : this.thingsThatRequirePhonePermission) {
                            r8.run();
                        }
                    }
                    this.thingsThatRequirePhonePermission.clear();
                    this.phonePermissionDenialHandlers.clear();
                }
                return;
            case 5:
                synchronized (this.lock) {
                    if (grantResults.length <= 0 || grantResults[0] != 0) {
                        java.util.HashMap<java.lang.String, java.lang.String> attributes5 = new java.util.HashMap<>();
                        attributes5.put("Type", com.navdy.client.app.tracking.TrackerConstants.Attributes.SMS);
                        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.PERMISSION_REJECTED, attributes5);
                        for (java.lang.Runnable r9 : this.smsPermissionDenialHandlers) {
                            r9.run();
                        }
                    } else {
                        for (java.lang.Runnable r10 : this.thingsThatRequireSmsPermission) {
                            r10.run();
                        }
                    }
                    this.thingsThatRequireSmsPermission.clear();
                    this.smsPermissionDenialHandlers.clear();
                }
                return;
            case 6:
                synchronized (this.lock) {
                    if (grantResults.length <= 0 || grantResults[0] != 0) {
                        java.util.HashMap<java.lang.String, java.lang.String> attributes6 = new java.util.HashMap<>();
                        attributes6.put("Type", com.navdy.client.app.tracking.TrackerConstants.Attributes.STORAGE);
                        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.PERMISSION_REJECTED, attributes6);
                        for (java.lang.Runnable r11 : this.storagePermissionDenialHandlers) {
                            r11.run();
                        }
                    } else {
                        for (java.lang.Runnable r12 : this.thingsThatRequireStoragePermission) {
                            r12.run();
                        }
                    }
                    this.thingsThatRequireStoragePermission.clear();
                    this.storagePermissionDenialHandlers.clear();
                }
                return;
            case 7:
                synchronized (this.lock) {
                    if (grantResults.length <= 0 || grantResults[0] != 0) {
                        for (java.lang.Runnable r13 : this.calendarPermissionDenialHandlers) {
                            r13.run();
                        }
                    } else {
                        for (java.lang.Runnable r14 : this.thingsThatRequireCalendarPermission) {
                            r14.run();
                        }
                    }
                    this.thingsThatRequireCalendarPermission.clear();
                    this.calendarPermissionDenialHandlers.clear();
                }
                return;
            case 8:
                synchronized (this.lock) {
                    if (grantResults.length <= 0 || grantResults[0] != 0) {
                        for (java.lang.Runnable r15 : this.cameraPermissionDenialHandlers) {
                            r15.run();
                        }
                    } else {
                        for (java.lang.Runnable r16 : this.thingsThatRequireCameraPermission) {
                            r16.run();
                        }
                    }
                    this.thingsThatRequireCameraPermission.clear();
                    this.cameraPermissionDenialHandlers.clear();
                }
                return;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                return;
        }
    }

    public void requestNotificationPermission() {
        if (!weHaveNotificationPermission()) {
            startActivity(new android.content.Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        }
    }

    public void openBtConnectionDialog() {
        this.logger.v("Opening BT connection dialog");
        boolean isFirstLaunch = false;
        if (this instanceof com.navdy.client.app.ui.firstlaunch.AppSetupActivity) {
            this.logger.v("instance of FirstLaunchActivity");
            isFirstLaunch = true;
        }
        android.content.Intent i = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.settings.BluetoothPairActivity.class);
        i.putExtra(com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.EXTRA_KILL_ACTIVITY_ON_CANCEL, true);
        i.putExtra(com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.EXTRA_USER_WAS_IN_FIRST_LAUNCH_EXPERIENCE, isFirstLaunch);
        startActivity(i);
    }

    public void setStatusBarColor(int color) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            android.view.Window window = getWindow();
            window.clearFlags(67108864);
            window.addFlags(Integer.MIN_VALUE);
            window.setStatusBarColor(color);
        }
    }

    public void showProgressDialog() {
        this.progressDialog = showProgressDialog(this, this.progressDialog);
    }

    public static android.app.ProgressDialog showProgressDialog(android.app.Activity activity, android.app.ProgressDialog progressDialog2) {
        if (isEnding(activity)) {
            return progressDialog2;
        }
        if (progressDialog2 == null) {
            try {
                progressDialog2 = new android.app.ProgressDialog(activity);
            } catch (java.lang.Exception e) {
                sLogger.e("Unable to show progress dialog. " + progressDialog2, e);
            }
        }
        progressDialog2.setProgressStyle(0);
        progressDialog2.setMessage(activity.getString(com.navdy.client.R.string.loading_please_wait));
        progressDialog2.setIndeterminate(true);
        progressDialog2.setCanceledOnTouchOutside(false);
        if (android.os.Looper.myLooper() == android.os.Looper.getMainLooper()) {
            progressDialog2.show();
        } else {
            activity.runOnUiThread(new com.navdy.client.app.ui.base.BaseActivity.Anon8(progressDialog2));
        }
        return progressDialog2;
    }

    public void hideProgressDialog() {
        hideProgressDialog(this, this.progressDialog);
    }

    public static void hideProgressDialog(android.app.Activity activity, android.app.ProgressDialog progressDialog2) {
        if (progressDialog2 != null && progressDialog2.isShowing()) {
            dismissProgressDialog(activity, progressDialog2);
        }
    }

    public void dismissProgressDialog() {
        dismissProgressDialog(this, this.progressDialog);
    }

    public static void dismissProgressDialog(android.app.Activity activity, android.app.ProgressDialog progressDialog2) {
        if (progressDialog2 != null) {
            if (android.os.Looper.myLooper() == android.os.Looper.getMainLooper()) {
                progressDialog2.dismiss();
            } else {
                activity.runOnUiThread(new com.navdy.client.app.ui.base.BaseActivity.Anon9(progressDialog2));
            }
        }
    }

    public void dismissQuestionDialog() {
        if (this.questionDialog != null) {
            if (android.os.Looper.myLooper() == android.os.Looper.getMainLooper()) {
                this.questionDialog.dismiss();
            } else {
                runOnUiThread(new com.navdy.client.app.ui.base.BaseActivity.Anon10());
            }
        }
    }

    @android.support.annotation.NonNull
    public android.graphics.Point getScreenSize() {
        return getScreenSize(this);
    }

    @android.support.annotation.NonNull
    public static android.graphics.Point getScreenSize(android.support.v4.app.FragmentActivity activity) {
        android.view.Display display;
        if (activity != null) {
            display = activity.getWindowManager().getDefaultDisplay();
        } else {
            display = ((android.view.WindowManager) com.navdy.client.app.NavdyApplication.getAppContext().getSystemService("window")).getDefaultDisplay();
        }
        android.graphics.Point size = new android.graphics.Point();
        display.getSize(size);
        return size;
    }

    public static boolean isEnding(android.app.Activity activity) {
        return activity == null || activity.isFinishing() || activity.isDestroyed() || ((activity instanceof com.navdy.client.app.ui.base.BaseActivity) && ((com.navdy.client.app.ui.base.BaseActivity) activity).instanceStateIsSaved.get());
    }

    public boolean requiresBus() {
        return true;
    }

    public void onActionModeStarted(android.view.ActionMode mode) {
        super.onActionModeStarted(mode);
        this.mActionMode = mode;
    }

    private void endActionMode() {
        if (this.mActionMode != null) {
            this.mActionMode.finish();
        }
    }

    public void onActionModeFinished(android.view.ActionMode mode) {
        super.onActionModeFinished(mode);
        this.mActionMode = null;
    }

    /* access modifiers changed from: protected */
    public void openMarketAppFor(java.lang.String appPackageName) {
        try {
            startActivity(new android.content.Intent("android.intent.action.VIEW", android.net.Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException e) {
            startActivity(new android.content.Intent("android.intent.action.VIEW", android.net.Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public static void showLongToast(@android.support.annotation.StringRes int messageResId, java.lang.Object... args) {
        showToast(1, messageResId, args);
    }

    public static void showShortToast(@android.support.annotation.StringRes int messageResId, java.lang.Object... args) {
        showToast(0, messageResId, args);
    }

    private static void showToast(int length, @android.support.annotation.StringRes int messageResId, java.lang.Object... args) {
        com.navdy.client.app.framework.util.SystemUtils.runOnUiThread(new com.navdy.client.app.ui.base.BaseActivity.Anon11(messageResId, args, length));
    }

    public void hideSystemUI() {
        if (!isEnding(this)) {
            android.view.Window window = getWindow();
            if (window != null) {
                android.view.View decorView = window.getDecorView();
                if (decorView != null) {
                    decorView.setSystemUiVisibility(5380);
                }
            }
        }
    }

    public void showSystemUI() {
        if (!isEnding(this)) {
            android.view.Window window = getWindow();
            if (window != null) {
                android.view.View decorView = window.getDecorView();
                if (decorView != null) {
                    decorView.setSystemUiVisibility(256);
                }
            }
        }
    }

    public void hideSystemUiDependingOnOrientation() {
        if (!isEnding(this)) {
            android.content.res.Resources resources = getResources();
            if (resources != null) {
                android.content.res.Configuration configuration = resources.getConfiguration();
                if (configuration == null) {
                    return;
                }
                if (configuration.orientation == 2) {
                    hideSystemUI();
                } else {
                    showSystemUI();
                }
            }
        }
    }

    public void openBrowserFor(android.net.Uri uri) {
        try {
            startActivity(new android.content.Intent("android.intent.action.VIEW", uri));
        } catch (android.content.ActivityNotFoundException e) {
            android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.string.no_browser, 1).show();
        }
    }

    public void loadImage(@android.support.annotation.IdRes int imgId, @android.support.annotation.DrawableRes int drawableRes) {
        android.widget.ImageView imgView = (android.widget.ImageView) findViewById(imgId);
        if (imgView != null) {
            com.navdy.client.app.framework.util.ImageUtils.loadImage(imgView, drawableRes, this.imageCache);
        }
    }
}
