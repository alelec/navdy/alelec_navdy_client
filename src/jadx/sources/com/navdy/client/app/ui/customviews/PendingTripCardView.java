package com.navdy.client.app.ui.customviews;

public class PendingTripCardView extends com.navdy.client.app.ui.customviews.TripCardView {
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.customviews.PendingTripCardView.class);
    private android.content.Context callingActivity;
    private final android.content.Context context;
    private android.view.View.OnClickListener currentClickListener;

    class Anon1 implements android.view.View.OnClickListener {
        final /* synthetic */ com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo val$pendingRoute;

        Anon1(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo navdyRouteInfo) {
            this.val$pendingRoute = navdyRouteInfo;
        }

        public void onClick(android.view.View v) {
            com.navdy.client.app.ui.customviews.PendingTripCardView.logger.v("onClick, starting details activity");
            if (com.navdy.client.app.ui.customviews.PendingTripCardView.this.callingActivity != null) {
                com.navdy.client.app.ui.details.DetailsActivity.startDetailsActivityForResult(this.val$pendingRoute.getDestination(), com.navdy.client.app.ui.customviews.PendingTripCardView.this.callingActivity);
                com.navdy.client.app.ui.customviews.PendingTripCardView.this.callingActivity = null;
                return;
            }
            com.navdy.client.app.ui.details.DetailsActivity.startDetailsActivity(this.val$pendingRoute.getDestination(), com.navdy.client.app.ui.customviews.PendingTripCardView.this.context);
        }
    }

    class Anon2 implements android.view.View.OnClickListener {
        Anon2() {
        }

        public void onClick(android.view.View v) {
            com.navdy.client.app.ui.customviews.PendingTripCardView.logger.v("onClick, retrying route");
            com.navdy.client.app.ui.customviews.PendingTripCardView.this.navdyRouteHandler.retryRoute();
        }
    }

    public PendingTripCardView(android.content.Context context2) {
        this(context2, null, 0);
    }

    public PendingTripCardView(android.content.Context context2, android.util.AttributeSet attrs) {
        this(context2, attrs, 0);
    }

    public PendingTripCardView(android.content.Context context2, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context2, attrs, defStyleAttr);
        this.callingActivity = null;
        this.context = context2;
    }

    public void onPendingRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo pendingRoute) {
        super.onPendingRouteCalculated(error, pendingRoute);
        setUpClickListener(error, pendingRoute);
    }

    private void setUpClickListener(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo pendingRoute) {
        if (error == com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error.NONE) {
            this.currentClickListener = new com.navdy.client.app.ui.customviews.PendingTripCardView.Anon1(pendingRoute);
        } else {
            this.currentClickListener = new com.navdy.client.app.ui.customviews.PendingTripCardView.Anon2();
        }
    }

    public void handleOnClick() {
        if (this.currentClickListener != null) {
            this.currentClickListener.onClick(this);
        }
    }

    public void handleOnClick(android.content.Context context2) {
        this.callingActivity = context2;
        if (this.currentClickListener != null && this.callingActivity != null) {
            this.currentClickListener.onClick(this);
        }
    }

    protected java.lang.String appendPrefix(java.lang.String name) {
        return getResources().getString(com.navdy.client.R.string.pending_trip_prefix, new java.lang.Object[]{name});
    }
}
