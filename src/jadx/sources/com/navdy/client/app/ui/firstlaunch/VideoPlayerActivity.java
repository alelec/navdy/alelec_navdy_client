package com.navdy.client.app.ui.firstlaunch;

public class VideoPlayerActivity extends com.navdy.client.app.ui.base.BaseActivity {
    public static final java.lang.String EXTRA_VIDEO_URL = "video_url";
    private int currentPosition;
    private android.widget.VideoView myVideo;
    private android.widget.TextView subtitlesTextView;
    private java.lang.String videoUrl;

    class Anon1 extends android.widget.MediaController {
        Anon1(android.content.Context x0) {
            super(x0);
        }

        public boolean dispatchKeyEvent(android.view.KeyEvent event) {
            if (event.getKeyCode() == 4) {
                com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.this.finish();
            }
            return super.dispatchKeyEvent(event);
        }
    }

    class Anon2 implements android.media.MediaPlayer.OnCompletionListener {
        Anon2() {
        }

        public void onCompletion(android.media.MediaPlayer mediaPlayer) {
            com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.this.finish();
        }
    }

    class Anon3 implements android.media.MediaPlayer.OnPreparedListener {

        class Anon1 implements android.media.MediaPlayer.OnInfoListener {
            Anon1() {
            }

            public boolean onInfo(android.media.MediaPlayer mp, int what, int extra) {
                if (what == 701) {
                    com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.this.showProgressDialog();
                }
                if (what == 702) {
                    com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.this.hideProgressDialog();
                }
                return false;
            }
        }

        class Anon2 implements android.media.MediaPlayer.OnTimedTextListener {

            class Anon1 implements java.lang.Runnable {
                final /* synthetic */ android.media.TimedText val$text;

                Anon1(android.media.TimedText timedText) {
                    this.val$text = timedText;
                }

                public void run() {
                    java.lang.String subtitleLine = this.val$text.getText();
                    if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(subtitleLine)) {
                        if (subtitleLine.endsWith("\n")) {
                            subtitleLine = subtitleLine.substring(0, subtitleLine.length() - 1);
                        }
                        if (subtitleLine.endsWith("\n")) {
                            subtitleLine = subtitleLine.substring(0, subtitleLine.length() - 1);
                        }
                        com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.this.subtitlesTextView.setText(subtitleLine);
                        com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.this.subtitlesTextView.setVisibility(View.VISIBLE);
                        return;
                    }
                    com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.this.subtitlesTextView.setVisibility(View.GONE);
                }
            }

            Anon2() {
            }

            public void onTimedText(android.media.MediaPlayer mp, android.media.TimedText text) {
                if (text != null) {
                    com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.this.handler.post(new com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.Anon3.Anon2.Anon1(text));
                }
            }
        }

        Anon3() {
        }

        public void onPrepared(android.media.MediaPlayer player) {
            com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.this.hideProgressDialog();
            player.setOnInfoListener(new com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.Anon3.Anon1());
            try {
                java.util.Locale currentLocale = com.navdy.client.app.framework.i18n.I18nManager.getInstance().getCurrentLocale();
                if (!isEnglish(currentLocale)) {
                    int textTrackIndex = com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.this.findTrackIndexFor(player.getTrackInfo(), 3, currentLocale.getISO3Language());
                    if (textTrackIndex >= 0) {
                        player.selectTrack(textTrackIndex);
                    } else {
                        com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.this.logger.w("Cannot find text track for language: " + currentLocale.getISO3Language());
                    }
                }
                player.setOnTimedTextListener(new com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.Anon3.Anon2());
            } catch (java.lang.Exception e) {
                e.printStackTrace();
            }
        }

        private boolean isEnglish(java.util.Locale currentLocale) {
            return currentLocale == null || com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(currentLocale.getLanguage(), "en");
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_install_video);
        this.subtitlesTextView = (android.widget.TextView) findViewById(com.navdy.client.R.id.subtitles);
        showProgressDialog();
        android.widget.MediaController mediaController = new com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.Anon1(this);
        this.myVideo = (android.widget.VideoView) findViewById(com.navdy.client.R.id.install_video);
        this.videoUrl = getIntent().getStringExtra(EXTRA_VIDEO_URL);
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.videoUrl)) {
            this.logger.e("Invalid video URL! [" + this.videoUrl + "]");
        }
        if (this.myVideo != null) {
            this.myVideo.setMediaController(mediaController);
            this.myVideo.setVideoPath(this.videoUrl);
            mediaController.setAnchorView(this.myVideo);
            mediaController.setMediaPlayer(this.myVideo);
            this.myVideo.setOnCompletionListener(new com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.Anon2());
            this.myVideo.setOnPreparedListener(new com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.Anon3());
            this.myVideo.start();
        }
    }

    private int findTrackIndexFor(android.media.MediaPlayer.TrackInfo[] trackInfo, int mediaTrackType, java.lang.String language) {
        for (int i = 0; i < trackInfo.length; i++) {
            if (trackInfo[i].getTrackType() == mediaTrackType && com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(trackInfo[i].getLanguage(), language)) {
                return i;
            }
        }
        return -1;
    }

    protected void onRestart() {
        this.myVideo.start();
        this.myVideo.seekTo(this.currentPosition);
        if (!this.myVideo.isPlaying()) {
            this.myVideo.resume();
        }
        super.onRestart();
    }

    protected void onPause() {
        this.currentPosition = this.myVideo.getCurrentPosition();
        this.myVideo.pause();
        super.onPause();
    }

    protected void onResume() {
        super.onResume();
        com.navdy.client.app.tracking.Tracker.tagScreen("First_Launch_Install_Video: " + this.videoUrl);
    }

    protected void onDestroy() {
        if (!(this.myVideo == null || this.myVideo.getHolder() == null || this.myVideo.getHolder().getSurface() == null)) {
            this.myVideo.getHolder().getSurface().release();
        }
        super.onDestroy();
    }
}
