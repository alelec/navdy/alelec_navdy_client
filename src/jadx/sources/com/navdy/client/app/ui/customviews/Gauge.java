package com.navdy.client.app.ui.customviews;

public class Gauge extends android.view.View implements com.navdy.client.app.ui.customviews.SerialValueAnimator.SerialValueAnimatorAdapter {
    private static final int DEFAULT_SHADOW_THICKNESS_DP = 0;
    private static final float DEFAULT_SUB_TEXT_SIZE = 40.0f;
    private static final float DEFAULT_TEXT_SIZE = 90.0f;
    private static final int DEFAULT_THICKNESS_DP = 40;
    private static final int DEFAULT_TIC_LENGTH_DP = 4;
    private static final int SHADOW_START_ANGLE = 2;
    private static final int TIC_STYLE_CIRCLE = 2;
    private static final int TIC_STYLE_LINE = 1;
    private static final int TIC_STYLE_NONE = 0;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.customviews.Gauge.class);
    private boolean AntialiasSetting;
    private int forwardAnimationDuration;
    private int mBackgroundColor;
    private android.graphics.Paint mBackgroundPaint;
    private java.lang.String mCenterSubtext;
    private java.lang.String mCenterText;
    private int mEndColor;
    private int mMaxValue;
    private int mMinValue;
    private com.navdy.client.app.ui.customviews.SerialValueAnimator mSerialValueAnimator;
    private int mShadowColor;
    private int mShadowThickness;
    private com.navdy.client.app.framework.util.CustomDimension mShadowThicknessAttribute;
    private int mStartAngle;
    private int mStartColor;
    private float mSubTextSize;
    private com.navdy.client.app.framework.util.CustomDimension mSubTextSizeAttribute;
    private int mSweepAngle;
    private int mTextColor;
    private float mTextSize;
    private com.navdy.client.app.framework.util.CustomDimension mTextSizeAttribute;
    private int mThickness;
    private com.navdy.client.app.framework.util.CustomDimension mThicknessAttribute;
    private int mTicColor;
    private int mTicInterval;
    private int mTicLength;
    private int mTicPadding;
    private android.graphics.Paint mTicPaint;
    private int mTicStyle;
    private int mValue;
    private int mWarningColor;
    private int mWarningValue;
    private int reverseAnimationDuration;

    public Gauge(android.content.Context context) {
        this(context, null);
    }

    public Gauge(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Gauge(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.forwardAnimationDuration = 300;
        this.reverseAnimationDuration = 100;
        this.mTicColor = -1;
        this.AntialiasSetting = true;
        initFromAttributes(context, attrs);
        initDrawingTools();
        this.mSerialValueAnimator = new com.navdy.client.app.ui.customviews.SerialValueAnimator(this, this.forwardAnimationDuration, this.reverseAnimationDuration);
    }

    public void setAnimationSpeeds(int forwardAnimationDuration2, int reverseAnimationDuration2) {
        this.forwardAnimationDuration = forwardAnimationDuration2;
        this.reverseAnimationDuration = reverseAnimationDuration2;
        this.mSerialValueAnimator.setAnimationSpeeds(forwardAnimationDuration2, reverseAnimationDuration2);
    }

    /* JADX INFO: finally extract failed */
    private void initFromAttributes(android.content.Context context, android.util.AttributeSet attrs) {
        android.content.res.TypedArray a = context.getTheme().obtainStyledAttributes(attrs, com.navdy.client.R.styleable.Gauge, 0, 0);
        int defaultThickness = com.navdy.client.app.ui.UiUtils.convertDpToPx(DEFAULT_SUB_TEXT_SIZE);
        int defaultShadowThickness = com.navdy.client.app.ui.UiUtils.convertDpToPx(0.0f);
        int defaultTicLength = com.navdy.client.app.ui.UiUtils.convertDpToPx(4.0f);
        try {
            this.mMinValue = a.getInteger(4, 0);
            this.mValue = a.getInteger(6, 0);
            this.mMaxValue = a.getInteger(5, 120);
            this.mStartAngle = a.getInteger(0, 150);
            this.mSweepAngle = a.getInteger(1, 240);
            this.mTicPadding = a.getDimensionPixelOffset(19, 0);
            this.mThicknessAttribute = com.navdy.client.app.framework.util.CustomDimension.getDimension(this, a, 2, (float) defaultThickness);
            this.mShadowThicknessAttribute = com.navdy.client.app.framework.util.CustomDimension.getDimension(this, a, 3, (float) defaultShadowThickness);
            this.mTextSizeAttribute = com.navdy.client.app.framework.util.CustomDimension.getDimension(this, a, 13, DEFAULT_TEXT_SIZE);
            this.mSubTextSizeAttribute = com.navdy.client.app.framework.util.CustomDimension.getDimension(this, a, 14, DEFAULT_SUB_TEXT_SIZE);
            this.mTicStyle = a.getInteger(18, 0);
            this.mTicLength = a.getDimensionPixelSize(20, defaultTicLength);
            this.mTicInterval = a.getInteger(21, 10);
            this.mBackgroundColor = a.getColor(7, -7829368);
            this.mWarningColor = a.getColor(9, android.support.v4.internal.view.SupportMenu.CATEGORY_MASK);
            this.mTextColor = a.getColor(10, -1);
            this.mWarningValue = a.getInteger(8, 75);
            this.mStartColor = a.getColor(15, -16777216);
            this.mEndColor = a.getColor(16, -1);
            this.mShadowColor = a.getColor(17, -7829368);
            this.mCenterText = a.getString(11);
            this.mCenterSubtext = a.getString(12);
            a.recycle();
            if (this.mMaxValue < this.mMinValue) {
                this.mMaxValue = this.mMinValue + 1;
            }
            if (this.mSweepAngle <= 0) {
                this.mSweepAngle = 1;
            }
            if (this.mTicInterval <= 0) {
                this.mTicInterval = 1;
            }
        } catch (Throwable th) {
            a.recycle();
            throw th;
        }
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        sLogger.d("Width spec: " + android.view.View.MeasureSpec.toString(widthMeasureSpec));
        sLogger.d("Height spec: " + android.view.View.MeasureSpec.toString(heightMeasureSpec));
        int chosenDimension = java.lang.Math.min(chooseDimension(android.view.View.MeasureSpec.getMode(widthMeasureSpec), android.view.View.MeasureSpec.getSize(widthMeasureSpec)), chooseDimension(android.view.View.MeasureSpec.getMode(heightMeasureSpec), android.view.View.MeasureSpec.getSize(heightMeasureSpec)));
        setMeasuredDimension(chosenDimension, chosenDimension);
    }

    private int chooseDimension(int mode, int size) {
        return (mode == Integer.MIN_VALUE || mode == 1073741824) ? size : getPreferredSize();
    }

    private int getPreferredSize() {
        return 300;
    }

    public void setGaugeValue(int value) {
        if (value < this.mMinValue) {
            value = this.mMinValue;
        }
        if (value > this.mMaxValue) {
            value = this.mMaxValue;
        }
        this.mSerialValueAnimator.setValue((float) value);
    }

    public void setValue(float newValue) {
        this.mValue = (int) newValue;
        invalidate();
    }

    public void setMaxValue(int mMaxValue2) {
        this.mMaxValue = mMaxValue2;
    }

    public void setCenterText(java.lang.String text) {
        this.mCenterText = text;
        invalidate();
    }

    public void setCenterSubtext(java.lang.String text) {
        this.mCenterSubtext = text;
        invalidate();
    }

    private void evaluateDimensions() {
        int smallSide = java.lang.Math.min(getWidth(), getHeight());
        this.mThickness = (int) this.mThicknessAttribute.getSize(this, (float) smallSide, 0.0f);
        this.mShadowThickness = (int) this.mShadowThicknessAttribute.getSize(this, (float) smallSide, 0.0f);
        if (this.mThickness <= this.mShadowThickness) {
            sLogger.e("Shadow is set to be bigger than gauge's thickness - removing shadow");
            this.mShadowThickness = 0;
        }
        this.mTextSize = this.mTextSizeAttribute.getSize(this, (float) smallSide, 0.0f);
        this.mSubTextSize = this.mSubTextSizeAttribute.getSize(this, (float) smallSide, 0.0f);
    }

    private void initDrawingTools() {
        evaluateDimensions();
        this.mBackgroundPaint = new android.graphics.Paint();
        this.mBackgroundPaint.setStrokeWidth((float) this.mThickness);
        this.mBackgroundPaint.setAntiAlias(true);
        this.mBackgroundPaint.setStrokeCap(android.graphics.Paint.Cap.BUTT);
        this.mBackgroundPaint.setStyle(android.graphics.Paint.Style.STROKE);
        this.mTicPaint = new android.graphics.Paint();
        this.mTicPaint.setColor(this.mTicColor);
        this.mTicPaint.setStyle(this.mTicStyle == 1 ? android.graphics.Paint.Style.STROKE : android.graphics.Paint.Style.FILL);
        this.mTicPaint.setStrokeWidth(0.0f);
        this.mTicPaint.setAntiAlias(this.AntialiasSetting);
    }

    private float getRadius() {
        return getRadius(this.mThickness);
    }

    private float getRadius(int thickness) {
        return ((float) (java.lang.Math.min(getHeight(), getWidth()) - thickness)) / 2.0f;
    }

    private void drawBackground(android.graphics.Canvas canvas) {
        float width = (float) getWidth();
        float height = (float) getHeight();
        float radius = getRadius();
        android.graphics.Paint paint = this.mBackgroundPaint;
        android.graphics.RectF rect = new android.graphics.RectF();
        rect.set((width / 2.0f) - radius, (height / 2.0f) - radius, (width / 2.0f) + radius, (height / 2.0f) + radius);
        paint.setColor(this.mBackgroundColor);
        float backgroundSweep = (float) this.mSweepAngle;
        if (this.mWarningValue != 0) {
            float warningSweep = deltaToAngle(this.mMaxValue - this.mWarningValue);
            backgroundSweep -= warningSweep;
            paint.setColor(this.mWarningColor);
            canvas.drawArc(rect, valueToAngle(this.mWarningValue), warningSweep, false, paint);
        }
        paint.setColor(this.mBackgroundColor);
        canvas.drawArc(rect, (float) this.mStartAngle, backgroundSweep, false, paint);
    }

    private float valueToAngle(int value) {
        return ((float) this.mStartAngle) + ((((float) this.mSweepAngle) * ((float) (value - this.mMinValue))) / ((float) (this.mMaxValue - this.mMinValue)));
    }

    private float deltaToAngle(int deltaValue) {
        return (((float) this.mSweepAngle) * ((float) deltaValue)) / ((float) (this.mMaxValue - this.mMinValue));
    }

    private void drawTics(android.graphics.Canvas canvas) {
        if (this.mTicStyle != 0) {
            float centerX = (float) java.lang.Math.round(((float) getWidth()) / 2.0f);
            float centerY = (float) java.lang.Math.round(((float) getHeight()) / 2.0f);
            float radius = getRadius();
            canvas.save(1);
            canvas.rotate((float) this.mStartAngle, centerX, centerY);
            float tickSweep = deltaToAngle(this.mTicInterval);
            float maxAngle = (float) (this.mStartAngle + this.mSweepAngle);
            float alphaSweep = tickSweep / 2.0f;
            float valueAngle = valueToAngle(this.mValue);
            for (float tickAngle = (float) this.mStartAngle; tickAngle <= maxAngle; tickAngle += tickSweep) {
                int alpha = 128;
                if (valueAngle > tickAngle) {
                    alpha = 255;
                } else if (valueAngle > tickAngle - alphaSweep) {
                    alpha = 128 + java.lang.Math.round((((float) android.support.v4.media.TransportMediator.KEYCODE_MEDIA_PAUSE) * (valueAngle - (tickAngle - alphaSweep))) / alphaSweep);
                }
                this.mTicPaint.setAlpha(alpha);
                if (this.mTicStyle == 1) {
                    canvas.drawLine((((centerX + radius) - (((float) this.mThickness) / 2.0f)) - ((float) this.mTicLength)) - ((float) this.mTicPadding), centerY, ((centerX + radius) - (((float) this.mThickness) / 2.0f)) - ((float) this.mTicPadding), centerY, this.mTicPaint);
                } else if (this.mTicStyle == 2) {
                    canvas.drawCircle((((centerX + radius) - (((float) this.mThickness) / 2.0f)) - (((float) this.mTicLength) / 2.0f)) - ((float) this.mTicPadding), centerY, ((float) this.mTicLength) / 2.0f, this.mTicPaint);
                }
                canvas.rotate(tickSweep, centerX, centerY);
            }
            canvas.restore();
        }
    }

    private void drawText(android.graphics.Canvas canvas) {
        if (this.mCenterText != null || this.mCenterSubtext != null) {
            android.graphics.Paint paint = new android.graphics.Paint();
            paint.setColor(this.mTextColor);
            float mainTextSize = this.mTextSize;
            float subTextSize = this.mSubTextSize;
            paint.setTextSize(mainTextSize);
            paint.setTextAlign(android.graphics.Paint.Align.CENTER);
            float centerX = ((float) getWidth()) / 2.0f;
            float centerY = ((float) getHeight()) / 2.0f;
            if (this.mCenterText != null) {
                canvas.drawText(this.mCenterText, centerX, centerY, paint);
            }
            if (this.mCenterSubtext != null) {
                paint.setTextSize(subTextSize);
                canvas.drawText(this.mCenterSubtext, centerX, centerY + subTextSize + 10.0f, paint);
            }
        }
    }

    private void drawIndicator(android.graphics.Canvas canvas) {
        float width = (float) getWidth();
        float height = (float) getHeight();
        float radius = getRadius();
        android.graphics.Paint paint = new android.graphics.Paint();
        paint.setColor(this.mStartColor);
        paint.setStrokeWidth((float) this.mThickness);
        paint.setAntiAlias(true);
        paint.setStrokeCap(android.graphics.Paint.Cap.BUTT);
        paint.setStyle(android.graphics.Paint.Style.STROKE);
        float sweep = deltaToAngle(this.mValue - this.mMinValue);
        if (this.mStartColor != this.mEndColor) {
            paint.setShader(new android.graphics.SweepGradient(width / 2.0f, height / 2.0f, new int[]{this.mStartColor, this.mStartColor, this.mEndColor, this.mEndColor}, new float[]{0.0f, ((float) this.mStartAngle) / 360.0f, (((float) this.mStartAngle) + sweep) / 360.0f, 1.0f}));
        }
        float rectLeft = (width / 2.0f) - radius;
        float rectTop = (height / 2.0f) - radius;
        float rectRight = (width / 2.0f) + radius;
        float rectBottom = (height / 2.0f) + radius;
        float radiusDeltaForShadow = 0.0f;
        float angleDelta = 0.0f;
        if (this.mShadowThickness > 0) {
            android.graphics.Paint shadowPaint = new android.graphics.Paint(paint);
            shadowPaint.setColor(this.mShadowColor);
            canvas.drawArc(new android.graphics.RectF(rectLeft, rectTop, rectRight, rectBottom), (float) this.mStartAngle, sweep, false, shadowPaint);
            int newThickness = this.mThickness - this.mShadowThickness;
            paint.setStrokeWidth((float) newThickness);
            radiusDeltaForShadow = radius - getRadius(newThickness);
            angleDelta = 2.0f;
        }
        if (sweep > angleDelta) {
            android.graphics.RectF rect = new android.graphics.RectF();
            rect.set(rectLeft + radiusDeltaForShadow, rectTop + radiusDeltaForShadow, rectRight - radiusDeltaForShadow, rectBottom - radiusDeltaForShadow);
            canvas.drawArc(rect, ((float) this.mStartAngle) + angleDelta, sweep - angleDelta, false, paint);
        }
    }

    protected void onDraw(android.graphics.Canvas canvas) {
        drawBackground(canvas);
        drawTics(canvas);
        drawText(canvas);
        drawIndicator(canvas);
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        initDrawingTools();
    }

    public void setBackgroundColor(int color) {
        this.mBackgroundColor = color;
        invalidate();
    }

    public void clearAnimationQueue() {
        this.mSerialValueAnimator.release();
    }

    public float getValue() {
        return (float) this.mValue;
    }
}
