package com.navdy.client.app.ui;

public abstract class RecyclerViewCursorAdapter<VH extends android.support.v7.widget.RecyclerView.ViewHolder> extends android.support.v7.widget.RecyclerView.Adapter<VH> {
    private static final int FOOTER_VIEW = -2;
    private static final int HEADER_VIEW = -1;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.RecyclerViewCursorAdapter.class);
    protected android.content.Context context;
    protected android.database.Cursor cursor = null;
    private android.widget.CursorAdapter cursorAdapter;
    protected boolean dataIsValid = false;
    private android.database.DataSetObserver dataSetObserver;
    protected boolean hasFooter = false;
    protected boolean hasHeader = false;
    private boolean observerRegistered;

    class Anon1 extends android.database.DataSetObserver {
        Anon1() {
        }

        public void onChanged() {
            super.onChanged();
            com.navdy.client.app.ui.RecyclerViewCursorAdapter.this.dataIsValid = true;
            com.navdy.client.app.ui.RecyclerViewCursorAdapter.this.changeCursor(com.navdy.client.app.ui.RecyclerViewCursorAdapter.this.getNewCursor());
        }

        public void onInvalidated() {
            super.onInvalidated();
            com.navdy.client.app.ui.RecyclerViewCursorAdapter.this.dataIsValid = false;
            com.navdy.client.app.ui.RecyclerViewCursorAdapter.this.changeCursor(com.navdy.client.app.ui.RecyclerViewCursorAdapter.this.getNewCursor());
        }
    }

    class Anon2 extends android.widget.CursorAdapter {
        Anon2(android.content.Context x0, android.database.Cursor x1, boolean x2) {
            super(x0, x1, x2);
        }

        public android.view.View newView(android.content.Context context, android.database.Cursor cursor, android.view.ViewGroup parent) {
            return null;
        }

        public void bindView(android.view.View view, android.content.Context context, android.database.Cursor cursor) {
        }
    }

    public abstract long getItemId(int i);

    public abstract android.database.Cursor getNewCursor();

    public abstract void onBindViewHolder(VH vh, int i);

    public abstract VH onCreateFooterViewHolder(android.view.ViewGroup viewGroup);

    public abstract VH onCreateHeaderViewHolder(android.view.ViewGroup viewGroup);

    public abstract VH onCreateNormalViewHolder(android.view.ViewGroup viewGroup);

    public RecyclerViewCursorAdapter(android.content.Context context2, boolean hasHeader2, boolean hasFooter2) {
        this.context = context2;
        this.hasHeader = hasHeader2;
        this.hasFooter = hasFooter2;
        setHasStableIds(true);
        this.dataSetObserver = new com.navdy.client.app.ui.RecyclerViewCursorAdapter.Anon1();
    }

    public void notifyDbChanged() {
        this.dataSetObserver.onChanged();
    }

    public void onAttachedToRecyclerView(android.support.v7.widget.RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        changeCursor(getNewCursor());
    }

    public void onDetachedFromRecyclerView(android.support.v7.widget.RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        if (this.cursor != null) {
            if (this.observerRegistered) {
                this.cursor.unregisterDataSetObserver(this.dataSetObserver);
                this.observerRegistered = false;
                logger.v("observer unregistered detached");
            }
            com.navdy.service.library.util.IOUtils.closeStream(this.cursor);
            this.cursor = null;
        }
    }

    public int getItemCount() {
        int count = 0;
        if (this.cursor != null) {
            count = this.cursor.getCount();
            if (count > 0 && this.hasFooter) {
                count++;
            }
        }
        return this.hasHeader ? count + 1 : count;
    }

    public VH onCreateViewHolder(android.view.ViewGroup parent, int viewType) {
        if (viewType == -1) {
            return onCreateHeaderViewHolder(parent);
        }
        if (viewType == -2) {
            return onCreateFooterViewHolder(parent);
        }
        return onCreateNormalViewHolder(parent);
    }

    public int getItemViewType(int position) {
        if (this.hasHeader && position == 0) {
            return -1;
        }
        if (!this.hasFooter || position != this.cursor.getCount()) {
            return super.getItemViewType(position);
        }
        return -2;
    }

    public void changeCursor(android.database.Cursor newCursor) {
        com.navdy.service.library.util.IOUtils.closeStream(swapCursor(newCursor));
    }

    private android.database.Cursor swapCursor(android.database.Cursor newCursor) {
        boolean z = false;
        if (newCursor == this.cursor) {
            return null;
        }
        android.database.Cursor oldCursor = this.cursor;
        if (oldCursor != null && this.observerRegistered) {
            oldCursor.unregisterDataSetObserver(this.dataSetObserver);
            logger.v("observer unregistered old");
            this.observerRegistered = false;
        }
        this.cursor = newCursor;
        if (this.cursor != null) {
            z = true;
        }
        this.dataIsValid = z;
        if (this.cursor != null) {
            logger.v("observer registered");
            this.cursor.registerDataSetObserver(this.dataSetObserver);
            this.observerRegistered = true;
        }
        notifyDataSetChanged();
        if (this.cursorAdapter == null) {
            this.cursorAdapter = new com.navdy.client.app.ui.RecyclerViewCursorAdapter.Anon2(this.context, this.cursor, true);
            return oldCursor;
        }
        this.cursorAdapter.swapCursor(this.cursor);
        return oldCursor;
    }

    public void close() {
        logger.v("close");
        try {
            if (this.cursor != null) {
                if (this.observerRegistered) {
                    this.observerRegistered = false;
                    this.cursor.unregisterDataSetObserver(this.dataSetObserver);
                    logger.v("observer unregistered");
                }
                com.navdy.service.library.util.IOUtils.closeStream(this.cursor);
            }
        } catch (Throwable t) {
            logger.e(t);
        }
    }
}
