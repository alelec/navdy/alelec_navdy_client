package com.navdy.client.app.ui.customviews;

public class CustomSpinner extends android.widget.Spinner {
    boolean spinnerHasBeenOpened;
    com.navdy.client.app.ui.customviews.CustomSpinner.OnSpinnerStateChangedListener spinnerStateListener;

    public interface OnSpinnerStateChangedListener {
        void spinnerClosed();

        void spinnerOpened();
    }

    public CustomSpinner(android.content.Context context) {
        super(context);
    }

    public CustomSpinner(android.content.Context context, int mode) {
        super(context, mode);
    }

    public CustomSpinner(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean performClick() {
        this.spinnerHasBeenOpened = true;
        if (this.spinnerStateListener != null) {
            this.spinnerStateListener.spinnerOpened();
        }
        return super.performClick();
    }

    public void spinnerClosed() {
        this.spinnerHasBeenOpened = false;
        if (this.spinnerStateListener != null) {
            this.spinnerStateListener.spinnerClosed();
        }
    }

    public boolean hasBeenOpened() {
        return this.spinnerHasBeenOpened;
    }

    public void setListener(com.navdy.client.app.ui.customviews.CustomSpinner.OnSpinnerStateChangedListener spinnerStateListener2) {
        this.spinnerStateListener = spinnerStateListener2;
    }
}
