package com.navdy.client.app.ui.settings;

public class MessagingSettingsEditDialogActivity extends com.navdy.client.app.ui.base.BaseActivity {
    public static final int ADD_CODE = 4241;
    public static final int EDIT_CODE = 4242;
    public static final java.lang.String EXTRA_NEW_MESSAGE = "extra_new_message";
    public static final java.lang.String EXTRA_OLD_MESSAGE = "extra_old_message";
    private android.widget.Button done;
    private android.widget.EditText input;
    private java.lang.String oldMessage;

    class Anon1 implements android.text.TextWatcher {
        final android.view.animation.Animation animShake = android.view.animation.AnimationUtils.loadAnimation(com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.this, com.navdy.client.R.anim.shake);

        Anon1() {
        }

        public void beforeTextChanged(java.lang.CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(java.lang.CharSequence s, int start, int before, int count) {
        }

        public void afterTextChanged(android.text.Editable s) {
            boolean messageIsValid;
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(s)) {
                messageIsValid = true;
            } else {
                messageIsValid = false;
            }
            com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.this.done.setEnabled(messageIsValid);
            int maxChars = com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.this.getResources().getInteger(com.navdy.client.R.integer.reply_text_max_chars);
            if (s != null && s.length() >= maxChars) {
                ((android.os.Vibrator) com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.this.getSystemService("vibrator")).vibrate(100);
                com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.this.input.startAnimation(this.animShake);
                int selectionEnd = com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.this.input.getSelectionEnd();
                com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.this.input.setText(s.subSequence(0, s.length() - 1));
                android.widget.EditText access$Anon100 = com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.this.input;
                if (selectionEnd > com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.this.input.length()) {
                    selectionEnd = com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.this.input.length();
                }
                access$Anon100.setSelection(selectionEnd);
            }
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        getWindow().setFlags(262144, 262144);
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.dialog_settings_messaging_add_edit);
        this.input = (android.widget.EditText) findViewById(com.navdy.client.R.id.input);
        this.done = (android.widget.Button) findViewById(com.navdy.client.R.id.done_button);
        if (this.input != null && this.done != null) {
            this.input.addTextChangedListener(new com.navdy.client.app.ui.settings.MessagingSettingsEditDialogActivity.Anon1());
            android.content.Intent intent = getIntent();
            if (intent != null) {
                this.oldMessage = intent.getStringExtra(EXTRA_OLD_MESSAGE);
                if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.oldMessage)) {
                    android.widget.TextView title = (android.widget.TextView) findViewById(com.navdy.client.R.id.dialog_title);
                    if (title != null) {
                        title.setText(com.navdy.client.R.string.add_message);
                    }
                    this.done.setEnabled(false);
                    return;
                }
                this.input.setText(this.oldMessage);
                this.input.setSelection(this.input.length());
            }
        }
    }

    protected void onResume() {
        super.onResume();
        this.input.clearFocus();
        this.input.requestFocus();
        this.input.setSelection(this.input.length());
        getWindow().setSoftInputMode(4);
    }

    public boolean onTouchEvent(android.view.MotionEvent event) {
        return true;
    }

    public void onCancelClick(android.view.View view) {
        finish();
    }

    public void onDoneClick(android.view.View view) {
        java.lang.String message = java.lang.String.valueOf(this.input.getText());
        int code = ADD_CODE;
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.oldMessage)) {
            code = EDIT_CODE;
        }
        android.content.Intent intent = new android.content.Intent();
        intent.putExtra(EXTRA_NEW_MESSAGE, message);
        intent.putExtra(EXTRA_OLD_MESSAGE, this.oldMessage);
        setResult(code, intent);
        finish();
    }
}
