package com.navdy.client.app.ui;

public class SimpleDialogFragment extends android.app.DialogFragment {
    public static final java.lang.String EXTRA_ID = "_id";
    public static final java.lang.String EXTRA_MESSAGE = "message";
    public static final java.lang.String EXTRA_POSITIVE_BUTTON_TITLE = "positive_button_title";
    public static final java.lang.String EXTRA_TITLE = "title";

    public interface DialogProvider {
        android.app.Dialog createDialog(int i, android.os.Bundle bundle);
    }

    public static com.navdy.client.app.ui.SimpleDialogFragment newInstance(int id, android.os.Bundle args) {
        if (args == null) {
            args = new android.os.Bundle();
        }
        args.putInt("_id", id);
        com.navdy.client.app.ui.SimpleDialogFragment simpleDialogFragment = new com.navdy.client.app.ui.SimpleDialogFragment();
        simpleDialogFragment.setArguments(args);
        return simpleDialogFragment;
    }

    public android.app.Dialog onCreateDialog(android.os.Bundle savedInstanceState) {
        android.app.Activity activity = getActivity();
        android.os.Bundle arguments = getArguments();
        if (arguments != null) {
            int id = arguments.getInt("_id");
            if (activity != null && (activity instanceof com.navdy.client.app.ui.SimpleDialogFragment.DialogProvider)) {
                return ((com.navdy.client.app.ui.SimpleDialogFragment.DialogProvider) activity).createDialog(id, arguments);
            }
        }
        return super.onCreateDialog(savedInstanceState);
    }
}
