package com.navdy.client.app.ui.settings;

class CalendarAdapter extends android.support.v7.widget.RecyclerView.Adapter {
    private android.content.Context appContext = com.navdy.client.app.NavdyApplication.getAppContext();
    private final java.util.ArrayList<com.navdy.client.app.framework.models.Calendar> calendars = com.navdy.client.app.ui.homescreen.CalendarUtils.listCalendars(this.appContext);

    class Anon1 implements android.widget.CompoundButton.OnCheckedChangeListener {
        Anon1() {
        }

        public void onCheckedChanged(android.widget.CompoundButton buttonView, boolean isChecked) {
            com.navdy.client.app.ui.settings.CalendarAdapter.this.notifyDataSetChanged();
        }
    }

    CalendarAdapter() {
    }

    public com.navdy.client.app.framework.models.Calendar getItem(int position) {
        if (this.calendars != null) {
            return (com.navdy.client.app.framework.models.Calendar) this.calendars.get(position);
        }
        return null;
    }

    public long getItemId(int position) {
        com.navdy.client.app.framework.models.Calendar calendar = getItem(position);
        if (calendar != null) {
            return calendar.id;
        }
        return 0;
    }

    public int getItemViewType(int position) {
        com.navdy.client.app.framework.models.Calendar calendar = getItem(position);
        if (calendar != null) {
            return calendar.type.getValue();
        }
        return 0;
    }

    public int getItemCount() {
        if (this.calendars != null) {
            return this.calendars.size();
        }
        return 0;
    }

    public android.support.v7.widget.RecyclerView.ViewHolder onCreateViewHolder(android.view.ViewGroup parent, int viewType) {
        android.view.LayoutInflater vi = android.view.LayoutInflater.from(this.appContext);
        switch (com.navdy.client.app.framework.models.Calendar.CalendarListItemType.fromValue(viewType)) {
            case GLOBAL_SWITCH:
                return new com.navdy.client.app.ui.settings.CalendarGlobalSwitchViewHolder(vi.inflate(com.navdy.client.R.layout.settings_switch_row, parent, false), new com.navdy.client.app.ui.settings.CalendarAdapter.Anon1());
            case TITLE:
                return new com.navdy.client.app.ui.settings.CalendarTitleViewHolder(vi.inflate(com.navdy.client.R.layout.calendar_title_list_row, parent, false));
            case CALENDAR:
                return new com.navdy.client.app.ui.settings.CalendarViewHolder(vi.inflate(com.navdy.client.R.layout.calendar_list_row, parent, false));
            default:
                return null;
        }
    }

    public void onBindViewHolder(android.support.v7.widget.RecyclerView.ViewHolder holder, int position) {
        com.navdy.client.app.framework.models.Calendar calendar = getItem(position);
        if (calendar != null) {
            switch (calendar.type) {
                case TITLE:
                    if (holder instanceof com.navdy.client.app.ui.settings.CalendarTitleViewHolder) {
                        ((com.navdy.client.app.ui.settings.CalendarTitleViewHolder) holder).setName(calendar.displayName);
                        return;
                    }
                    return;
                case CALENDAR:
                    if (holder instanceof com.navdy.client.app.ui.settings.CalendarViewHolder) {
                        ((com.navdy.client.app.ui.settings.CalendarViewHolder) holder).setCalendar(calendar);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }
}
