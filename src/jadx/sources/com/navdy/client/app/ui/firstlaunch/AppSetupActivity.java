package com.navdy.client.app.ui.firstlaunch;

public class AppSetupActivity extends com.navdy.client.app.ui.settings.ProfileSettingsActivity {
    private android.support.v4.view.ViewPager bottomCard;
    private boolean hasNeverShownBtSuccess = true;
    private boolean hasOpenedBluetoothPairingDialog = false;
    private int hiddenButtonClickCount = 0;
    private android.widget.ImageView hud;
    private com.navdy.client.app.ui.firstlaunch.AppSetupPagerAdapter setupPagerAdapter;
    private boolean showingFail = false;

    class Anon1 implements java.lang.Runnable {

        /* renamed from: com.navdy.client.app.ui.firstlaunch.AppSetupActivity$Anon1$Anon1 reason: collision with other inner class name */
        class C0058Anon1 implements java.lang.Runnable {
            C0058Anon1() {
            }

            public void run() {
                com.navdy.client.app.ui.settings.SettingsUtils.incrementDriverProfileSerial();
                com.navdy.client.app.ui.settings.SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
            }
        }

        Anon1() {
        }

        public void run() {
            com.navdy.client.app.framework.models.UserAccountInfo userAccountInfo = com.navdy.client.app.framework.util.ContactsManager.lookUpUserProfileInfo();
            if (userAccountInfo != null) {
                boolean incrementAndSendToHud = false;
                if (com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.userPhoto == null && userAccountInfo.photo != null) {
                    com.navdy.client.app.tracking.Tracker.saveUserPhotoToInternalStorage(userAccountInfo.photo);
                    incrementAndSendToHud = true;
                }
                if (com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.customerPrefs != null) {
                    android.content.SharedPreferences.Editor editor = com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.customerPrefs.edit();
                    if (!com.navdy.client.app.framework.util.StringUtils.isValidName(com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.name) && com.navdy.client.app.framework.util.StringUtils.isValidName(userAccountInfo.fullName)) {
                        editor.putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.FULL_NAME, userAccountInfo.fullName);
                        incrementAndSendToHud = true;
                    }
                    if (!com.navdy.client.app.framework.util.StringUtils.isValidEmail(com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.email)) {
                        if (!com.navdy.client.app.framework.util.StringUtils.isValidEmail(userAccountInfo.email)) {
                            android.accounts.Account[] accounts = ((android.accounts.AccountManager) com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.getSystemService("account")).getAccounts();
                            int length = accounts.length;
                            int i = 0;
                            while (true) {
                                if (i >= length) {
                                    break;
                                }
                                android.accounts.Account account = accounts[i];
                                if (account.type.equalsIgnoreCase("com.google") && com.navdy.client.app.framework.util.StringUtils.isValidEmail(account.name)) {
                                    editor.putString("email", account.name);
                                    incrementAndSendToHud = true;
                                    break;
                                }
                                i++;
                            }
                        } else {
                            editor.putString("email", userAccountInfo.email);
                            incrementAndSendToHud = true;
                        }
                    }
                    editor.apply();
                }
                if (incrementAndSendToHud) {
                    com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.firstlaunch.AppSetupActivity.Anon1.C0058Anon1(), 1);
                }
            }
        }
    }

    class Anon2 implements android.support.v4.view.ViewPager.OnPageChangeListener {
        Anon2() {
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        public void onPageSelected(int position) {
            com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.showingFail = false;
            com.navdy.client.app.framework.util.SystemUtils.dismissKeyboard(com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this);
            com.navdy.client.app.ui.firstlaunch.AppSetupScreen currentScreen = com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.getCurrentScreen();
            if (currentScreen == null || currentScreen.screenType != com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.PROFILE) {
                com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.hideProfileStuff();
                com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.updateIllustration(position);
                if (com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.setupPagerAdapter != null) {
                    com.navdy.client.app.ui.firstlaunch.AppSetupBottomCardGenericFragment frag = (com.navdy.client.app.ui.firstlaunch.AppSetupBottomCardGenericFragment) com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.setupPagerAdapter.getItem(position);
                    if (frag != null) {
                        frag.showNormal();
                    }
                }
            } else {
                com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.showProfileStuff();
            }
            if (currentScreen != null && com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.hasNeverShownBtSuccess && currentScreen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.BLUETOOTH && com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.isDeviceConnected(com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.getApplicationContext())) {
                com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.hasNeverShownBtSuccess = false;
                com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.moveToNextScreen();
            }
        }

        public void onPageScrollStateChanged(int state) {
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.moveToNextScreen();
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.showFailureForCurrentScreen();
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.moveToNextScreen();
            com.navdy.client.app.framework.AppInstance appInstance = com.navdy.client.app.framework.AppInstance.getInstance();
            if (appInstance.mLocationTransmitter != null) {
                appInstance.mLocationTransmitter.start();
            }
        }
    }

    class Anon6 implements java.lang.Runnable {
        Anon6() {
        }

        public void run() {
            com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.moveToNextScreen();
        }
    }

    class Anon7 implements java.lang.Runnable {
        Anon7() {
        }

        public void run() {
            com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.moveToNextScreen();
            com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
            if (remoteDevice != null) {
                remoteDevice.postEvent((com.squareup.wire.Message) com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getMusicCapabilities());
            }
            com.navdy.client.app.framework.servicehandler.MusicServiceHandler musicServiceHandler = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance();
            if (musicServiceHandler != null && musicServiceHandler.shouldPerformFullPlaylistIndex()) {
                musicServiceHandler.indexPlaylists();
            }
        }
    }

    public static void goToAppSetup(android.app.Activity activity) {
        activity.startActivity(new android.content.Intent(activity, com.navdy.client.app.ui.firstlaunch.AppSetupActivity.class));
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_app_setup);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.app_setup).build();
        this.hud = (android.widget.ImageView) findViewById(com.navdy.client.R.id.hud);
        this.bottomCard = (android.support.v4.view.ViewPager) findViewById(com.navdy.client.R.id.bottom_card);
        if (this.hud == null || this.bottomCard == null) {
            this.logger.e("Ui element missing !");
            return;
        }
        this.userPhoto = com.navdy.client.app.tracking.Tracker.getUserProfilePhoto();
        if (this.customerPrefs != null) {
            this.name = this.customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.FULL_NAME, "");
            this.email = this.customerPrefs.getString("email", "");
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isValidName(this.name) || !com.navdy.client.app.framework.util.StringUtils.isValidEmail(this.email)) {
            requestContactsPermission(new com.navdy.client.app.ui.firstlaunch.AppSetupActivity.Anon1(), null);
        }
        setUpViewPager();
    }

    private void setUpViewPager() {
        this.bottomCard.setOffscreenPageLimit(1);
        this.setupPagerAdapter = new com.navdy.client.app.ui.firstlaunch.AppSetupPagerAdapter(getSupportFragmentManager(), this.imageCache);
        this.bottomCard.addOnPageChangeListener(new com.navdy.client.app.ui.firstlaunch.AppSetupActivity.Anon2());
        this.bottomCard.setAdapter(this.setupPagerAdapter);
    }

    protected void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        if (!com.navdy.client.app.tracking.Tracker.weHaveCarInfo()) {
            startActivity(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.class));
        }
        this.setupPagerAdapter.recalculateScreenCount();
        com.navdy.client.app.ui.firstlaunch.AppSetupScreen currentScreen = getCurrentScreen();
        if (currentScreen != null) {
            if (currentScreen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.BLUETOOTH) {
                if (isDeviceConnected(getApplicationContext())) {
                    moveToNextScreen();
                } else if (this.hasOpenedBluetoothPairingDialog) {
                    showFailureForCurrentScreen();
                }
            }
            if (currentScreen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.NOTIFICATIONS) {
                if (com.navdy.client.app.ui.base.BaseActivity.weHaveNotificationPermission()) {
                    moveToNextScreen();
                } else {
                    showFailureForCurrentScreen();
                }
            }
        }
        com.navdy.client.app.tracking.Tracker.tagScreen("First_Launch");
    }

    protected void onDestroy() {
        if (this.setupPagerAdapter != null) {
            this.setupPagerAdapter.clearCache();
        }
        super.onDestroy();
    }

    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, @android.support.annotation.NonNull java.lang.String[] permissions, @android.support.annotation.NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (this.setupPagerAdapter != null) {
            this.setupPagerAdapter.recalculateScreenCount();
        }
    }

    @com.squareup.otto.Subscribe
    public void appInstanceDeviceConnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent deviceConnectedEvent) {
        handleConnectionChange(true);
    }

    @com.squareup.otto.Subscribe
    public void appInstanceDeviceDisconnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent deviceDisconnectedEvent) {
        handleConnectionChange(false);
    }

    private void handleConnectionChange(boolean connected) {
        if (this.setupPagerAdapter != null) {
            this.setupPagerAdapter.recalculateScreenCount();
        }
        if (!connected) {
            this.bottomCard.setCurrentItem(1);
            return;
        }
        com.navdy.client.app.ui.firstlaunch.AppSetupScreen currentScreen = getCurrentScreen();
        if (currentScreen != null && currentScreen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.BLUETOOTH) {
            moveToNextScreen();
        }
    }

    public boolean isDeviceConnected(android.content.Context applicationContext) {
        return com.navdy.client.app.framework.AppInstance.getInstance().isDeviceConnected();
    }

    public void onPrivacyPolicyClick(android.view.View view) {
        android.content.Intent browserIntent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.WebViewActivity.class);
        browserIntent.putExtra("type", com.navdy.client.app.ui.WebViewActivity.PRIVACY);
        startActivity(browserIntent);
    }

    public void onContactSupportClick(android.view.View view) {
        startActivity(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.settings.ContactUsActivity.class));
    }

    public void onDescriptionClick(android.view.View view) {
        if (this.showingFail) {
            com.navdy.client.app.ui.firstlaunch.AppSetupScreen currentScreen = getCurrentScreen();
            if (currentScreen != null && currentScreen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.BLUETOOTH) {
                onHelpCenterClick(null);
            } else if (currentScreen == null || !(currentScreen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.PROFILE || currentScreen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.NOTIFICATIONS)) {
                com.navdy.client.app.framework.util.SystemUtils.goToSystemSettingsAppInfoForOurApp(this);
            }
        }
    }

    public void onHelpCenterClick(android.view.View view) {
        openBrowserFor(com.navdy.client.app.framework.util.ZendeskJWT.getZendeskUri());
    }

    public void onButtonClick(android.view.View v) {
        com.navdy.client.app.ui.firstlaunch.AppSetupScreen currentScreen = getCurrentScreen();
        if (currentScreen != null && !com.navdy.client.app.ui.base.BaseActivity.isEnding(this)) {
            java.lang.Runnable doThingsThatRequirePermission = new com.navdy.client.app.ui.firstlaunch.AppSetupActivity.Anon3();
            java.lang.Runnable handlePermissionDenial = new com.navdy.client.app.ui.firstlaunch.AppSetupActivity.Anon4();
            android.content.Context applicationContext = com.navdy.client.app.NavdyApplication.getAppContext();
            switch (currentScreen.screenType) {
                case BLUETOOTH:
                    if (isDeviceConnected(applicationContext)) {
                        moveToNextScreen();
                        return;
                    }
                    this.hasNeverShownBtSuccess = true;
                    this.hasOpenedBluetoothPairingDialog = true;
                    openBtConnectionDialog();
                    return;
                case NOTIFICATIONS:
                    if (!com.navdy.client.app.ui.base.BaseActivity.weHaveNotificationPermission()) {
                        startActivity(new android.content.Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case ACCESS_FINE_LOCATION:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestLocationPermission(new com.navdy.client.app.ui.firstlaunch.AppSetupActivity.Anon5(), handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case USE_MICROPHONE:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestMicrophonePermission(new com.navdy.client.app.ui.firstlaunch.AppSetupActivity.Anon6(), handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case READ_CONTACTS:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestContactsPermission(doThingsThatRequirePermission, handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case RECEIVE_SMS:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestSmsPermission(doThingsThatRequirePermission, handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case CALL_PHONE:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestPhonePermission(doThingsThatRequirePermission, handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case READ_CALENDAR:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestCalendarPermission(doThingsThatRequirePermission, handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case WRITE_EXTERNAL_STORAGE:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestStoragePermission(new com.navdy.client.app.ui.firstlaunch.AppSetupActivity.Anon7(), handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case END:
                    com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.FINISHED_APP_SETUP, true).apply();
                    moveToNextScreen();
                    return;
                default:
                    moveToNextScreen();
                    return;
            }
        }
    }

    public void onProfileInfoSet() {
        onButtonClick(null);
    }

    private void showProfileStuff() {
        changeProfileStuffVisibility(0);
    }

    private void hideProfileStuff() {
        changeProfileStuffVisibility(8);
    }

    private void changeProfileStuffVisibility(int visibility) {
        if (this.photo != null) {
            this.photo.setVisibility(visibility);
        }
        if (this.photoHint != null) {
            this.photoHint.setVisibility(visibility);
        }
        if (this.hud != null && visibility == 0) {
            this.hud.setImageResource(com.navdy.client.R.drawable.image_navdy_profile_empty);
        }
    }

    protected boolean validateAndSaveName(boolean success) {
        boolean success2 = super.validateAndSaveName(success);
        if (this.setupPagerAdapter != null) {
            this.setupPagerAdapter.recalculateScreenCount();
        }
        return success2;
    }

    protected boolean validateAndSaveEmail(boolean success) {
        boolean success2 = super.validateAndSaveEmail(success);
        if (this.setupPagerAdapter != null) {
            this.setupPagerAdapter.recalculateScreenCount();
        }
        return success2;
    }

    public void updateIllustration(int position) {
        this.setupPagerAdapter.updateIllustration(new java.lang.ref.WeakReference<>(this.hud), position, this.showingFail);
    }

    private void showFailureForCurrentScreen() {
        this.logger.d("showFailureForCurrentScreen");
        if (this.setupPagerAdapter != null && this.bottomCard != null) {
            int currentPosition = this.bottomCard.getCurrentItem();
            com.navdy.client.app.ui.firstlaunch.AppSetupBottomCardGenericFragment fragment = (com.navdy.client.app.ui.firstlaunch.AppSetupBottomCardGenericFragment) this.setupPagerAdapter.getItem(currentPosition);
            this.showingFail = true;
            fragment.showFail();
            updateIllustration(currentPosition);
        }
    }

    @android.support.annotation.Nullable
    public com.navdy.client.app.ui.firstlaunch.AppSetupScreen getCurrentScreen() {
        int currentIndex = this.bottomCard != null ? this.bottomCard.getCurrentItem() : 0;
        if (this.setupPagerAdapter != null) {
            return com.navdy.client.app.ui.firstlaunch.AppSetupPagerAdapter.getScreen(currentIndex);
        }
        return null;
    }

    private void moveToNextScreen() {
        this.setupPagerAdapter.recalculateScreenCount();
        int nextIndex = this.bottomCard.getCurrentItem() + 1;
        com.navdy.client.app.ui.firstlaunch.AppSetupScreen currentScreen = getCurrentScreen();
        if (currentScreen != null && currentScreen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.END) {
            goToHomeScreen();
        } else if (nextIndex < this.setupPagerAdapter.getCount()) {
            this.bottomCard.setCurrentItem(nextIndex);
        }
    }

    private void goToHomeScreen() {
        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Install.FIRST_LAUNCH_COMPLETED);
        android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
        java.lang.String carYear = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "");
        java.lang.String carMake = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "");
        java.lang.String carModel = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
        android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        java.lang.String box = sharedPreferences.getString(com.navdy.client.app.ui.settings.SettingsConstants.BOX, "Old_Box");
        int nbConfig = sharedPreferences.getInt(com.navdy.client.app.ui.settings.SettingsConstants.NB_CONFIG, 0);
        java.lang.String mount = com.navdy.client.app.ui.firstlaunch.InstallPagerAdapter.getCurrentMountType(sharedPreferences).getValue();
        java.util.HashMap<java.lang.String, java.lang.String> attributes = new java.util.HashMap<>(5);
        attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_YEAR, carYear);
        attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_MAKE, carMake);
        attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_MODEL, carModel);
        attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.SELECTED_MOUNT, mount);
        attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.SELECTED_BOX, box);
        attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.NB_CONFIGURATIONS, java.lang.String.valueOf(nbConfig));
        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Install.CONFIGURATION_AT_COMPLETION, attributes);
        com.navdy.client.app.framework.servicehandler.ContactServiceHandler.getInstance().forceSendFavoriteContactsToHud();
        android.content.Intent i = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.homescreen.HomescreenActivity.class);
        i.setFlags(268468224);
        startActivity(i);
        finish();
    }

    public void onBackPressed() {
        int nextIndex = this.bottomCard.getCurrentItem() - 1;
        if (nextIndex < 0) {
            super.onBackPressed();
        } else {
            this.bottomCard.setCurrentItem(nextIndex);
        }
    }

    public static boolean userHasFinishedAppSetup() {
        return com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.FINISHED_APP_SETUP, false);
    }

    public void onHiddenButtonClick(android.view.View view) {
        com.navdy.client.app.ui.firstlaunch.AppSetupScreen currentScreen = getCurrentScreen();
        if (currentScreen != null && currentScreen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.BLUETOOTH) {
            this.hiddenButtonClickCount++;
            if (this.hiddenButtonClickCount >= 10) {
                this.hiddenButtonClickCount = 0;
                this.setupPagerAdapter.pretendBtConnected();
                moveToNextScreen();
            }
        }
    }
}
