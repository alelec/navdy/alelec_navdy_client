package com.navdy.client.app.ui.firstlaunch;

public class MountPickerActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    public static final java.lang.String EXTRA_USE_CASE = "extra_use_case";
    public static final int MEDIUM_MOUNT_RECOMMENDED = 0;
    public static final int MEDIUM_TALL_IS_TOO_HIGH = 2;
    public static final int NONE_OF_THE_MOUNTS_WILL_WORK_4_U = 3;
    public static final int SHORT_IS_TOO_LOW = 1;
    java.lang.String box;
    com.navdy.client.app.framework.models.MountInfo mountInfo = new com.navdy.client.app.framework.models.MountInfo();
    private int usecase;

    class Anon1 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
        final /* synthetic */ android.widget.TextView val$desc;
        final /* synthetic */ android.widget.TextView val$mediumMountPill;
        final /* synthetic */ java.lang.String val$modelString;
        final /* synthetic */ android.widget.TextView val$shortMountPill;
        final /* synthetic */ android.widget.TextView val$title;

        Anon1(java.lang.String str, android.widget.TextView textView, android.widget.TextView textView2, android.widget.TextView textView3, android.widget.TextView textView4) {
            this.val$modelString = str;
            this.val$shortMountPill = textView;
            this.val$mediumMountPill = textView2;
            this.val$title = textView3;
            this.val$desc = textView4;
        }

        /* access modifiers changed from: protected */
        public java.lang.Void doInBackground(java.lang.Void... params) {
            com.navdy.client.app.ui.firstlaunch.MountPickerActivity.this.mountInfo = com.navdy.client.app.ui.settings.SettingsUtils.getMountInfo();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(java.lang.Void aVoid) {
            com.navdy.client.app.ui.firstlaunch.MountPickerActivity.this.handleMountInfo(com.navdy.client.app.ui.firstlaunch.MountPickerActivity.this.box, this.val$modelString, this.val$shortMountPill, this.val$mediumMountPill, this.val$title, this.val$desc);
        }
    }

    class Anon2 implements android.view.View.OnClickListener {
        Anon2() {
        }

        public void onClick(android.view.View v) {
            com.navdy.client.app.ui.firstlaunch.MountPickerActivity.this.startActivity(new android.content.Intent(com.navdy.client.app.ui.firstlaunch.MountPickerActivity.this.getApplicationContext(), com.navdy.client.app.ui.settings.ContactUsActivity.class));
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        int imageRes;
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_mount_picker);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title(com.navdy.client.app.ui.settings.SettingsUtils.getCarYearMakeModelString()).build();
        this.usecase = getIntent().getIntExtra(EXTRA_USE_CASE, 0);
        this.box = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.BOX, "Old_Box");
        java.lang.String modelString = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
        android.widget.TextView shortMountPill = (android.widget.TextView) findViewById(com.navdy.client.R.id.short_mount_pill);
        android.widget.TextView mediumMountPill = (android.widget.TextView) findViewById(com.navdy.client.R.id.medium_mount_pill);
        android.widget.TextView title = (android.widget.TextView) findViewById(com.navdy.client.R.id.wrong_mount_title);
        android.widget.TextView desc = (android.widget.TextView) findViewById(com.navdy.client.R.id.wrong_mount_description);
        if (title != null) {
            title.setText(getString(com.navdy.client.R.string.mount_picker_title, new java.lang.Object[]{modelString}));
        }
        new com.navdy.client.app.ui.firstlaunch.MountPickerActivity.Anon1(modelString, shortMountPill, mediumMountPill, title, desc).execute(new java.lang.Void[0]);
        android.widget.TextView shortMountDescription = (android.widget.TextView) findViewById(com.navdy.client.R.id.short_mount_description);
        if (shortMountDescription != null) {
            shortMountDescription.setText(com.navdy.client.R.string.lowers_the_display);
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.box, "Old_Box")) {
            imageRes = com.navdy.client.R.drawable.image_2016_short_mount_sm;
        } else {
            imageRes = com.navdy.client.R.drawable.image_2017_short_mount_sm;
        }
        loadImage(com.navdy.client.R.id.short_mount_illustration, imageRes);
        loadImage(com.navdy.client.R.id.medium_mount_illustration, com.navdy.client.R.drawable.image_medium_mount_sm);
    }

    private void handleMountInfo(java.lang.String box2, java.lang.String modelString, android.widget.TextView shortMountPill, android.widget.TextView mediumMountPill, android.widget.TextView title, android.widget.TextView desc) {
        if (this.mountInfo.noneOfTheMountsWillWork()) {
            this.usecase = 3;
        }
        switch (this.usecase) {
            case 1:
                loadImage(com.navdy.client.R.id.illustration, com.navdy.client.R.drawable.asset_install_medium_pov);
                if (shortMountPill != null) {
                    shortMountPill.setText(com.navdy.client.R.string.lens_is_too_low);
                    shortMountPill.setBackgroundResource(com.navdy.client.R.drawable.pill_red);
                    shortMountPill.setVisibility(View.VISIBLE);
                }
                if (mediumMountPill != null) {
                    mediumMountPill.setVisibility(View.GONE);
                    break;
                }
                break;
            case 2:
                if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(box2, "Old_Box")) {
                    loadImage(com.navdy.client.R.id.illustration, com.navdy.client.R.drawable.asset_install_2016_short_pov);
                } else {
                    loadImage(com.navdy.client.R.id.illustration, com.navdy.client.R.drawable.asset_install_2017_short_pov);
                }
                if (shortMountPill != null) {
                    shortMountPill.setVisibility(View.GONE);
                }
                if (mediumMountPill != null) {
                    mediumMountPill.setText(com.navdy.client.R.string.lens_is_too_high);
                    mediumMountPill.setBackgroundResource(com.navdy.client.R.drawable.pill_red);
                    mediumMountPill.setVisibility(View.VISIBLE);
                    break;
                }
                break;
            case 3:
                if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(box2, "Old_Box")) {
                    loadImage(com.navdy.client.R.id.illustration, com.navdy.client.R.drawable.img_installation_lens_2016_short_low);
                } else {
                    loadImage(com.navdy.client.R.id.illustration, com.navdy.client.R.drawable.img_installation_lens_2017_short_low);
                }
                if (shortMountPill != null) {
                    shortMountPill.setText(com.navdy.client.R.string.not_recommended);
                    shortMountPill.setBackgroundResource(com.navdy.client.R.drawable.pill_red);
                    shortMountPill.setVisibility(View.VISIBLE);
                }
                if (mediumMountPill != null) {
                    mediumMountPill.setText(com.navdy.client.R.string.not_recommended);
                    mediumMountPill.setBackgroundResource(com.navdy.client.R.drawable.pill_red);
                    mediumMountPill.setVisibility(View.VISIBLE);
                }
                if (title != null) {
                    title.setText(com.navdy.client.R.string.no_mount_will_work_title);
                }
                if (desc != null) {
                    desc.setText(com.navdy.client.app.framework.util.StringUtils.fromHtml(getString(com.navdy.client.R.string.no_mount_will_work_desc, new java.lang.Object[]{modelString})));
                    desc.setOnClickListener(new com.navdy.client.app.ui.firstlaunch.MountPickerActivity.Anon2());
                    break;
                }
                break;
            default:
                loadImage(com.navdy.client.R.id.illustration, com.navdy.client.R.drawable.asset_install_medium_pov);
                if (shortMountPill != null) {
                    shortMountPill.setVisibility(View.GONE);
                }
                if (mediumMountPill != null) {
                    mediumMountPill.setText(getString(com.navdy.client.R.string.recommended_for_your_car, new java.lang.Object[]{modelString}));
                    mediumMountPill.setBackgroundResource(com.navdy.client.R.drawable.pill_blue);
                    mediumMountPill.setVisibility(View.VISIBLE);
                    break;
                }
                break;
        }
        android.widget.ImageView backBtn = (android.widget.ImageView) findViewById(com.navdy.client.R.id.backBtn);
        android.widget.TextView selectToContinue = (android.widget.TextView) findViewById(com.navdy.client.R.id.select_mount_to_continue);
        if (this.usecase == 3) {
            if (selectToContinue != null) {
                selectToContinue.setVisibility(View.INVISIBLE);
            }
            if (backBtn != null) {
                backBtn.setImageResource(com.navdy.client.R.drawable.button_back_black);
                return;
            }
            return;
        }
        if (selectToContinue != null) {
            selectToContinue.setVisibility(View.VISIBLE);
        }
        if (backBtn != null) {
            backBtn.setImageResource(com.navdy.client.R.drawable.btn_back);
        }
    }

    protected void onResume() {
        super.onResume();
        hideSystemUI();
        com.navdy.client.app.tracking.Tracker.tagScreen(getTrackerScreen());
    }

    public void onShortMountClick(android.view.View view) {
        android.content.Intent intent;
        tagMountClickEvent(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.MOUNT_PICKER_MOUNT_TYPE_SHORT);
        if (!this.mountInfo.shortSupported) {
            intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.MountNotRecommendedActivity.class);
            intent.putExtra("extra_mount", com.navdy.client.app.framework.models.MountInfo.MountType.SHORT.getValue());
        } else {
            intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.InstallActivity.class);
            intent.putExtra("extra_step", com.navdy.client.R.layout.fle_install_short_mount);
        }
        startActivity(intent);
    }

    public void onMediumTallMountClick(android.view.View view) {
        android.content.Intent intent;
        tagMountClickEvent(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.MOUNT_PICKER_MOUNT_TYPE_MED_TALL);
        if (!this.mountInfo.mediumSupported && !this.mountInfo.tallSupported) {
            intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.MountNotRecommendedActivity.class);
            intent.putExtra("extra_mount", com.navdy.client.app.framework.models.MountInfo.MountType.TALL.getValue());
        } else if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.box, "New_Box")) {
            intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.YouWillNeedTheMountKitActivity.class);
        } else {
            intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.InstallActivity.class);
            intent.putExtra("extra_step", com.navdy.client.R.layout.fle_install_medium_or_tall_mount);
        }
        startActivity(intent);
    }

    public void onBackClick(android.view.View view) {
        finish();
    }

    @android.support.annotation.NonNull
    private java.lang.String getTrackerScreen() {
        switch (this.usecase) {
            case 1:
                return com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.SHORT_IS_TOO_LOW;
            case 2:
                return com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.MEDIUM_TALL_IS_TOO_HIGH;
            case 3:
                return com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.NO_MOUNT_SUPPORTED;
            default:
                return com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.MEDIUM_MOUNT_RECOMMENDED;
        }
    }

    private void tagMountClickEvent(java.lang.String mountPickerMountTypeShort) {
        java.util.HashMap<java.lang.String, java.lang.String> attributes = new java.util.HashMap<>(2);
        attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.MOUNT_PICKER_MOUNT_TYPE, mountPickerMountTypeShort);
        attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.MOUNT_PICKER_USE_CASE, getTrackerScreen());
        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Install.MOUNT_PICKER_SELECTION, attributes);
    }
}
