package com.navdy.client.app.ui.firstlaunch;

public class MountNotRecommendedActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    java.lang.String box = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.BOX, "Old_Box");
    android.content.SharedPreferences customerPrefs;

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_mount_not_recommended);
        this.customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title(com.navdy.client.app.ui.settings.SettingsUtils.getCarYearMakeModelString()).build();
    }

    protected void onResume() {
        super.onResume();
        hideSystemUI();
        android.widget.TextView title = (android.widget.TextView) findViewById(com.navdy.client.R.id.you_will_need_the_mount_kit_title);
        android.widget.TextView desc = (android.widget.TextView) findViewById(com.navdy.client.R.id.you_will_need_the_mount_kit_description);
        java.lang.String modelString = this.customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
        java.lang.String mount = getIntent().getStringExtra("extra_mount");
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(mount) || com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(mount, com.navdy.client.app.framework.models.MountInfo.MountType.SHORT.getValue())) {
            com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.SHORT_MOUNT_NOT_RECOMMENDED);
            if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.box, "Old_Box")) {
                loadImage(com.navdy.client.R.id.illustration, com.navdy.client.R.drawable.img_installation_lens_2016_short_low);
            } else {
                loadImage(com.navdy.client.R.id.illustration, com.navdy.client.R.drawable.img_installation_lens_2017_short_low);
            }
            if (title != null) {
                title.setText(getString(com.navdy.client.R.string.the_short_mount_is_not_recommended, new java.lang.Object[]{modelString}));
            }
            if (desc != null) {
                desc.setText(com.navdy.client.R.string.the_short_mount_is_not_recommended_description);
                return;
            }
            return;
        }
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.MEDIUM_TALL_MOUNT_NOT_RECOMMENDED);
        loadImage(com.navdy.client.R.id.illustration, com.navdy.client.R.drawable.img_installation_lens_medium_tall_low);
        if (title != null) {
            title.setText(getString(com.navdy.client.R.string.the_medium_and_tall_mounts_are_not_recommended, new java.lang.Object[]{modelString}));
        }
        if (desc != null) {
            desc.setText(com.navdy.client.R.string.the_medium_and_tall_mounts_are_not_recommended_description);
        }
    }

    public void onContinueWithShortClick(android.view.View view) {
        java.lang.String mount = getIntent().getStringExtra("extra_mount");
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.InstallActivity.class);
        intent.putExtra("extra_step", com.navdy.client.R.layout.fle_install_overview);
        intent.putExtra("extra_mount", mount);
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(mount) || com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(mount, com.navdy.client.app.framework.models.MountInfo.MountType.SHORT.getValue())) {
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Install.SHORT_MOUNT_NOT_RECOMMENDED_CONTINUE);
        } else {
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Install.MEDIUM_TALL_MOUNT_NOT_RECOMMENDED_CONTINUE);
            if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.box, "New_Box")) {
                intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.YouWillNeedTheMountKitActivity.class);
            }
        }
        startActivity(intent);
        finish();
    }

    public void onBackClick(android.view.View view) {
        onBackPressed();
    }
}
