package com.navdy.client.app.ui.customviews;

public class FluctuatorAnimatorView extends android.view.View {
    private android.animation.ValueAnimator alphaAnimator;
    private int animationDelay;
    private int animationDuration;
    private com.navdy.client.app.ui.customviews.DefaultAnimationListener animationListener;
    private android.animation.AnimatorSet animatorSet;
    float currentCircle;
    private float endRadius;
    private android.os.Handler handler;
    private android.view.animation.LinearInterpolator interpolator;
    private android.graphics.Paint paint;
    private int paintColor;
    private android.animation.ValueAnimator radiusAnimator;
    private float startRadius;
    public java.lang.Runnable startRunnable;
    private float strokeWidth;

    class Anon1 extends com.navdy.client.app.ui.customviews.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            com.navdy.client.app.ui.customviews.FluctuatorAnimatorView.this.handler.postDelayed(com.navdy.client.app.ui.customviews.FluctuatorAnimatorView.this.startRunnable, (long) com.navdy.client.app.ui.customviews.FluctuatorAnimatorView.this.animationDelay);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.client.app.ui.customviews.FluctuatorAnimatorView.this.animatorSet.start();
        }
    }

    class Anon3 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon3() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            com.navdy.client.app.ui.customviews.FluctuatorAnimatorView.this.currentCircle = ((java.lang.Float) animation.getAnimatedValue()).floatValue();
            com.navdy.client.app.ui.customviews.FluctuatorAnimatorView.this.invalidate();
        }
    }

    class Anon4 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon4() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            com.navdy.client.app.ui.customviews.FluctuatorAnimatorView.this.setAlpha(((java.lang.Float) animation.getAnimatedValue()).floatValue());
        }
    }

    public FluctuatorAnimatorView(android.content.Context context) {
        this(context, null);
    }

    public FluctuatorAnimatorView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FluctuatorAnimatorView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.interpolator = new android.view.animation.LinearInterpolator();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.animationListener = new com.navdy.client.app.ui.customviews.FluctuatorAnimatorView.Anon1();
        this.startRunnable = new com.navdy.client.app.ui.customviews.FluctuatorAnimatorView.Anon2();
        android.content.res.TypedArray a = context.obtainStyledAttributes(attrs, com.navdy.client.R.styleable.FluctuatorAnimatorView, defStyleAttr, 0);
        if (a != null) {
            this.paintColor = a.getColor(0, 0);
            this.startRadius = a.getDimension(1, 0.0f);
            this.endRadius = a.getDimension(2, 0.0f);
            this.strokeWidth = a.getDimension(3, 0.0f);
            this.animationDuration = a.getInteger(4, 0);
            this.animationDelay = a.getInteger(5, 0);
            a.recycle();
        }
        this.paint = new android.graphics.Paint();
        this.paint.setStrokeWidth(this.strokeWidth);
        this.paint.setStyle(android.graphics.Paint.Style.STROKE);
        this.paint.setAntiAlias(true);
        this.paint.setColor(this.paintColor);
        this.animatorSet = new android.animation.AnimatorSet();
        this.animatorSet.setDuration((long) this.animationDuration);
        this.radiusAnimator = android.animation.ValueAnimator.ofFloat(new float[]{this.startRadius, this.endRadius});
        this.alphaAnimator = android.animation.ValueAnimator.ofFloat(new float[]{1.0f, 0.0f});
        this.animatorSet.setInterpolator(this.interpolator);
        this.radiusAnimator.addUpdateListener(new com.navdy.client.app.ui.customviews.FluctuatorAnimatorView.Anon3());
        this.alphaAnimator.addUpdateListener(new com.navdy.client.app.ui.customviews.FluctuatorAnimatorView.Anon4());
        this.animatorSet.playTogether(new android.animation.Animator[]{this.radiusAnimator, this.alphaAnimator});
    }

    public void setColor(int paintColor2) {
        this.paint.setColor(paintColor2);
    }

    public void onDraw(android.graphics.Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, this.currentCircle, this.paint);
    }

    public void start() {
        stop();
        this.animatorSet.addListener(this.animationListener);
        this.animatorSet.start();
    }

    public void stop() {
        this.handler.removeCallbacks(this.startRunnable);
        if (this.animatorSet.isRunning()) {
            this.animatorSet.removeAllListeners();
            this.animatorSet.cancel();
        }
    }
}
