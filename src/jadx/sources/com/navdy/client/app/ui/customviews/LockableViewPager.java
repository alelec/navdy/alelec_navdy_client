package com.navdy.client.app.ui.customviews;

public class LockableViewPager extends android.support.v4.view.ViewPager {
    private boolean swipeable = false;

    public LockableViewPager(android.content.Context context) {
        super(context);
    }

    public LockableViewPager(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean getSwipeable() {
        return this.swipeable;
    }

    public void setSwipeable(boolean swipeable2) {
        this.swipeable = swipeable2;
    }

    public boolean onTouchEvent(android.view.MotionEvent event) {
        return this.swipeable && super.onTouchEvent(event);
    }

    public boolean onInterceptTouchEvent(android.view.MotionEvent event) {
        return this.swipeable && super.onInterceptTouchEvent(event);
    }

    public boolean canScrollHorizontally(int direction) {
        return this.swipeable && super.canScrollHorizontally(direction);
    }

    public boolean executeKeyEvent(android.view.KeyEvent event) {
        return this.swipeable && super.executeKeyEvent(event);
    }
}
