package com.navdy.client.app.ui.firstlaunch;

public class InstallLocateObdFragment extends com.navdy.client.app.ui.firstlaunch.InstallCardFragment {
    public InstallLocateObdFragment() {
        setLayoutId(com.navdy.client.R.layout.fle_install_locate_obd);
    }

    @android.support.annotation.Nullable
    public android.view.View onCreateView(android.view.LayoutInflater inflater, @android.support.annotation.Nullable android.view.ViewGroup container, @android.support.annotation.Nullable android.os.Bundle savedInstanceState) {
        com.navdy.client.app.framework.util.ImageCache imageCache = null;
        com.navdy.client.app.ui.firstlaunch.InstallActivity activity = (com.navdy.client.app.ui.firstlaunch.InstallActivity) getActivity();
        if (activity != null) {
            imageCache = activity.imageCache;
        }
        this.rootView = inflater.inflate(com.navdy.client.R.layout.fle_install_locate_obd, container, false);
        com.navdy.client.app.framework.util.ImageUtils.loadImage((android.widget.ImageView) this.rootView.findViewById(com.navdy.client.R.id.illustration), com.navdy.client.R.drawable.image_fle_installation_locating, imageCache);
        return this.rootView;
    }
}
