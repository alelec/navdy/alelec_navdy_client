package com.navdy.client.app.ui.settings;

public class AudioDialogActivity extends com.navdy.client.app.ui.base.BaseActivity {
    public static final java.lang.String ACTION_HFP_NOT_CONNECTED = "TEST_HFP_NOT_CONNECTED";
    public static final java.lang.String ACTION_TEST_AUDIO_STATUS = "TEST_AUDIO_STATUS";
    public static final int DELAY_MILLIS = 2000;
    public static final java.lang.String EXTRA_TITLE = "EXTRA_TITLE";
    private android.media.AudioManager audioManager;
    com.squareup.otto.Bus bus;
    private java.lang.Runnable finishActivityRunnable;
    private boolean firstEvent = true;
    private android.os.Handler handler;
    boolean isMuted = false;
    boolean isShowingAudioStatus = false;
    @butterknife.InjectView(2131755321)
    @butterknife.Optional
    android.widget.TextView outputDeviceName;
    @butterknife.InjectView(2131755320)
    @butterknife.Optional
    android.widget.ImageView statusImage;
    @butterknife.InjectView(2131755135)
    @butterknife.Optional
    android.widget.TextView statusTitle;
    @butterknife.InjectView(2131755322)
    @butterknife.Optional
    android.widget.ProgressBar volumeProgress;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.app.ui.settings.AudioDialogActivity.this.finish();
        }
    }

    class Anon2 implements android.view.View.OnClickListener {
        Anon2() {
        }

        public void onClick(android.view.View view) {
            com.navdy.client.app.ui.settings.AudioDialogActivity.this.finish();
        }
    }

    class Anon3 implements android.view.View.OnClickListener {
        Anon3() {
        }

        public void onClick(android.view.View view) {
            com.navdy.client.app.ui.settings.AudioDialogActivity.this.finish();
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        java.lang.String title;
        super.onCreate(savedInstanceState);
        this.handler = new android.os.Handler();
        this.audioManager = (android.media.AudioManager) getSystemService("audio");
        this.finishActivityRunnable = new com.navdy.client.app.ui.settings.AudioDialogActivity.Anon1();
        this.bus = com.navdy.client.app.framework.util.BusProvider.getInstance();
        android.content.Intent intent = getIntent();
        if (intent != null) {
            java.lang.String action = intent.getAction();
            if (ACTION_TEST_AUDIO_STATUS.equals(action)) {
                setContentView((int) com.navdy.client.R.layout.dialog_audio_status);
                android.view.View view = findViewById(16908290);
                ((android.view.ViewGroup) view.getParent()).setBackground(null);
                view.setBackground(null);
                butterknife.ButterKnife.inject((java.lang.Object) this, (android.app.Activity) this);
                android.content.Intent activityIntent = getIntent();
                if (activityIntent == null || !activityIntent.hasExtra(EXTRA_TITLE)) {
                    title = getString(com.navdy.client.R.string.settings_audio_status_playing_audio_test);
                } else {
                    title = activityIntent.getStringExtra(EXTRA_TITLE);
                }
                this.statusTitle.setText(title);
                this.volumeProgress.setProgress(0);
                this.isShowingAudioStatus = true;
            } else if (ACTION_HFP_NOT_CONNECTED.equals(action)) {
                setContentView((int) com.navdy.client.R.layout.settings_audio_hfp_not_connected);
                butterknife.ButterKnife.inject((java.lang.Object) this, (android.app.Activity) this);
            } else {
                setContentView((int) com.navdy.client.R.layout.dialog_audio_settings);
            }
        } else {
            setContentView((int) com.navdy.client.R.layout.dialog_audio_settings);
        }
    }

    protected void onResume() {
        super.onResume();
        if (this.isShowingAudioStatus) {
            this.bus.register(this);
        }
    }

    protected void onPause() {
        super.onPause();
        if (this.isShowingAudioStatus) {
            this.bus.unregister(this);
        }
    }

    public void onCloseClick(android.view.View view) {
        onBackPressed();
    }

    private void updateAudioStatus(com.navdy.client.app.framework.util.TTSAudioRouter.TTSAudioStatus audioStatus) {
        if (audioStatus.playingTTS || audioStatus.waitingForHfp) {
            this.handler.removeCallbacks(this.finishActivityRunnable);
            this.firstEvent = false;
            if (audioStatus.playingTTS && audioStatus.currentStreamVolume == 0) {
                this.isMuted = true;
                setContentView((int) com.navdy.client.R.layout.dialog_audio_muted);
                this.audioManager.adjustStreamVolume(audioStatus.streamType == 4 ? 3 : audioStatus.streamType, 0, 1);
                ((android.widget.ImageButton) findViewById(com.navdy.client.R.id.btn_close)).setOnClickListener(new com.navdy.client.app.ui.settings.AudioDialogActivity.Anon2());
                ((android.widget.Button) findViewById(com.navdy.client.R.id.btn_confirm_audio_turned_up)).setOnClickListener(new com.navdy.client.app.ui.settings.AudioDialogActivity.Anon3());
            }
            if (!this.isMuted) {
                if (audioStatus.throughBluetooth) {
                    this.statusImage.setImageResource(com.navdy.client.R.drawable.icon_bluetooth_output);
                    if (audioStatus.playingTTS && audioStatus.throughHFp) {
                        this.audioManager.adjustStreamVolume(6, 0, 1);
                    }
                } else {
                    this.statusImage.setImageResource(com.navdy.client.R.drawable.icon_audio_output);
                }
                this.outputDeviceName.setText(audioStatus.outputDeviceName);
            } else {
                return;
            }
        } else if (this.firstEvent) {
            this.firstEvent = false;
            this.handler.postDelayed(this.finishActivityRunnable, 2000);
        } else {
            this.handler.removeCallbacks(this.finishActivityRunnable);
            if (!this.isMuted) {
                finish();
            }
        }
        if (audioStatus.currentStreamMaxVolume != 0) {
            this.volumeProgress.setMax(audioStatus.currentStreamMaxVolume);
            this.volumeProgress.setProgress(audioStatus.currentStreamVolume);
        }
    }

    @butterknife.OnClick({2131755913, 2131755914})
    @butterknife.Optional
    public void onClick(android.view.View view) {
        switch (view.getId()) {
            case com.navdy.client.R.id.settings /*2131755913*/:
                android.content.Intent intentOpenBluetoothSettings = new android.content.Intent();
                intentOpenBluetoothSettings.setAction("android.settings.BLUETOOTH_SETTINGS");
                startActivity(intentOpenBluetoothSettings);
                return;
            case com.navdy.client.R.id.ok /*2131755914*/:
                finish();
                return;
            default:
                return;
        }
    }

    @com.squareup.otto.Subscribe
    public void onAudioStatus(com.navdy.client.app.framework.util.TTSAudioRouter.TTSAudioStatus ttsAudioStatus) {
        if (this.isShowingAudioStatus) {
            updateAudioStatus(ttsAudioStatus);
        }
    }

    public static void startAudioStatusActivity(android.content.Context context) {
        startAudioStatusActivity(context, null);
    }

    public static void startAudioStatusActivity(android.content.Context context, java.lang.String title) {
        android.content.Intent intent = new android.content.Intent(context, com.navdy.client.app.ui.settings.AudioDialogActivity.class);
        intent.setAction(ACTION_TEST_AUDIO_STATUS);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(title)) {
            intent.putExtra(EXTRA_TITLE, title);
        }
        context.startActivity(intent);
    }

    public static void showHFPNotConnected(android.content.Context context) {
        android.content.Intent intent = new android.content.Intent(context, com.navdy.client.app.ui.settings.AudioDialogActivity.class);
        intent.setAction(ACTION_HFP_NOT_CONNECTED);
        context.startActivity(intent);
    }

    public boolean requiresBus() {
        return false;
    }
}
