package com.navdy.client.app.ui;

public class WebViewActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    public static final java.lang.String ATTRIBUTION = "Attributions";
    public static final java.lang.String EXTRA_HTML = "extra_html";
    public static final java.lang.String EXTRA_PREVENT_LINKS = "prevent_links";
    public static final java.lang.String EXTRA_TYPE = "type";
    public static final java.lang.String PRIVACY = "Privacy";
    public static final java.lang.String TERMS = "Terms";
    public static final java.lang.String ZENDESK = "Zendesk";
    private java.lang.String type;

    class Anon1 extends android.webkit.WebViewClient {
        Anon1() {
        }

        public boolean shouldOverrideUrlLoading(android.webkit.WebView view, java.lang.String url) {
            return true;
        }

        public void onLoadResource(android.webkit.WebView view, java.lang.String url) {
        }
    }

    class Anon2 extends android.webkit.WebViewClient {
        Anon2() {
        }

        public void onPageFinished(android.webkit.WebView finishedWebView, java.lang.String url) {
            try {
                com.navdy.client.app.ui.WebViewActivity.this.injectMobileStyleForZendeskPage(finishedWebView);
            } catch (java.lang.Exception e) {
                com.navdy.client.app.ui.WebViewActivity.this.logger.e("IOException: " + e);
            }
            super.onPageFinished(finishedWebView, url);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00b2, code lost:
        if (r4.equals(PRIVACY) != false) goto L_0x003f;
     */
    protected void onCreate(android.os.Bundle savedInstanceState) {
        char c = 0;
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.web_view_activity);
        android.webkit.WebView webView = (android.webkit.WebView) findViewById(com.navdy.client.R.id.web_view);
        if (webView == null) {
            showLongToast(com.navdy.client.R.string.unable_to_load_webview, new java.lang.Object[0]);
            finish();
            return;
        }
        android.content.Intent intent = getIntent();
        this.type = intent.getStringExtra("type");
        boolean preventLinks = intent.getBooleanExtra(EXTRA_PREVENT_LINKS, false);
        java.lang.String str = this.type;
        switch (str.hashCode()) {
            case -852110348:
                if (str.equals(ATTRIBUTION)) {
                    c = 2;
                    break;
                }
            case 80697703:
                if (str.equals(TERMS)) {
                    c = 1;
                    break;
                }
            case 1350155112:
                break;
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
                new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.privacy_policy).build();
                break;
            case 1:
                new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.terms_of_service).build();
                break;
            case 2:
                new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.acknowledgments).build();
                break;
        }
        if (preventLinks || PRIVACY.equals(this.type) || TERMS.equals(this.type)) {
            webView.setWebViewClient(new com.navdy.client.app.ui.WebViewActivity.Anon1());
        } else if (ZENDESK.equals(this.type)) {
            webView.setWebViewClient(new com.navdy.client.app.ui.WebViewActivity.Anon2());
        }
        try {
            if (ATTRIBUTION.equals(this.type)) {
                java.lang.String attribution = com.navdy.service.library.util.IOUtils.convertInputStreamToString(getResources().openRawResource(com.navdy.client.R.raw.attribution), "UTF-8");
                webView.getSettings().setUseWideViewPort(true);
                webView.loadDataWithBaseURL(null, attribution + com.google.android.gms.common.GoogleApiAvailability.getInstance().getOpenSourceSoftwareLicenseInfo(this), "text/plain", null, null);
                return;
            }
            java.lang.String fileName = null;
            if (PRIVACY.equals(this.type)) {
                fileName = "file:///android_res/raw/privacypolicy.html";
            } else if (TERMS.equals(this.type)) {
                fileName = "file:///android_res/raw/terms.html";
            } else if (ZENDESK.equals(this.type)) {
                fileName = intent.getStringExtra(EXTRA_HTML);
            }
            webView.getSettings().setLayoutAlgorithm(android.webkit.WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            webView.loadUrl(fileName);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }

    protected void onResume() {
        super.onResume();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.Web.tag(this.type));
    }

    public void injectMobileStyleForZendeskPage(android.webkit.WebView webView) {
        try {
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl("javascript:(function() {var parent = document.getElementsByTagName('head').item(0);var style = document.createElement('style');style.type = 'text/css';style.appendChild(document.createTextNode(\".request-nav, .footer, .comments-title, .comment-list, .comment-load-more-comments, .comment-closed-notification, .comment-submit-request { display: none !important }\"));parent.appendChild(style);})()");
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }
}
