package com.navdy.client.app.ui.glances;

class GlancesAppAdapter extends android.widget.ArrayAdapter<android.content.pm.ApplicationInfo> {
    private final java.util.List<android.content.pm.ApplicationInfo> appsList;
    private boolean glancesAreEnabled = false;
    private final android.view.LayoutInflater layoutInflater;
    private final android.content.pm.PackageManager packageManager;
    private android.content.SharedPreferences sharedPrefs;

    class Anon1 implements android.view.View.OnClickListener {
        final /* synthetic */ android.widget.Switch val$appName;
        final /* synthetic */ android.content.pm.ApplicationInfo val$data;

        Anon1(android.content.pm.ApplicationInfo applicationInfo, android.widget.Switch switchR) {
            this.val$data = applicationInfo;
            this.val$appName = switchR;
        }

        public void onClick(android.view.View v) {
            com.navdy.client.app.ui.glances.GlanceUtils.saveGlancesConfigurationChanges(this.val$data.packageName, this.val$appName.isChecked());
            com.navdy.client.app.ui.glances.GlancesAppAdapter.this.setSwitchColor(this.val$appName, this.val$appName.isChecked());
        }
    }

    class Anon2 implements android.view.View.OnClickListener {
        final /* synthetic */ android.widget.Switch val$appName;

        Anon2(android.widget.Switch switchR) {
            this.val$appName = switchR;
        }

        public void onClick(android.view.View v) {
            if (this.val$appName != null && this.val$appName.isEnabled()) {
                this.val$appName.performClick();
            }
        }
    }

    GlancesAppAdapter(@android.support.annotation.NonNull android.content.Context context, @android.support.annotation.LayoutRes int textViewResourceId, java.util.List<android.content.pm.ApplicationInfo> appsList2) {
        super(context, textViewResourceId, appsList2);
        this.appsList = appsList2;
        this.sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        if (this.sharedPrefs != null) {
            this.glancesAreEnabled = this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES, false);
        }
        this.packageManager = context.getPackageManager();
        this.layoutInflater = (android.view.LayoutInflater) context.getSystemService("layout_inflater");
    }

    public int getCount() {
        if (this.appsList != null) {
            return this.appsList.size();
        }
        return 0;
    }

    public android.content.pm.ApplicationInfo getItem(int position) {
        if (this.appsList == null || position == 0) {
            return null;
        }
        return (android.content.pm.ApplicationInfo) this.appsList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    @android.support.annotation.NonNull
    public android.view.View getView(int position, android.view.View convertView, @android.support.annotation.NonNull android.view.ViewGroup parent) {
        if (convertView == null || java.lang.Boolean.TRUE.equals(convertView.getTag())) {
            convertView = this.layoutInflater.inflate(com.navdy.client.R.layout.app_list_row, parent, false);
        }
        convertView.setTag(java.lang.Boolean.valueOf(false));
        if (this.appsList != null) {
            android.content.pm.ApplicationInfo data = (android.content.pm.ApplicationInfo) this.appsList.get(position);
            if (data != null) {
                android.widget.Switch appName = (android.widget.Switch) convertView.findViewById(com.navdy.client.R.id.app_name);
                if (appName != null) {
                    appName.setText(data.loadLabel(this.packageManager));
                    appName.setEnabled(this.glancesAreEnabled);
                    appName.setOnClickListener(new com.navdy.client.app.ui.glances.GlancesAppAdapter.Anon1(data, appName));
                    boolean isChecked = this.sharedPrefs.getBoolean(data.packageName, false);
                    appName.setChecked(isChecked);
                    setSwitchColor(appName, isChecked);
                }
                android.widget.ImageView iconView = (android.widget.ImageView) convertView.findViewById(com.navdy.client.R.id.app_icon);
                if (iconView != null) {
                    iconView.setImageDrawable(data.loadIcon(this.packageManager));
                    iconView.setOnClickListener(new com.navdy.client.app.ui.glances.GlancesAppAdapter.Anon2(appName));
                }
            }
        }
        return convertView;
    }

    public void setSwitchColor(@android.support.annotation.NonNull android.widget.Switch appSwitch, boolean isChecked) {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        int trackColor = android.support.v4.content.ContextCompat.getColor(context, isChecked ? com.navdy.client.R.color.blue_high : com.navdy.client.R.color.grey);
        int thumbColor = android.support.v4.content.ContextCompat.getColor(context, isChecked ? com.navdy.client.R.color.blue : com.navdy.client.R.color.grey_3);
        android.graphics.drawable.Drawable trackDrawable = appSwitch.getTrackDrawable();
        android.graphics.drawable.Drawable thumbDrawable = appSwitch.getThumbDrawable();
        if (android.os.Build.VERSION.SDK_INT < 23) {
            if (trackDrawable != null) {
                trackDrawable.setColorFilter(trackColor, android.graphics.PorterDuff.Mode.MULTIPLY);
            }
            if (thumbDrawable != null) {
                thumbDrawable.setColorFilter(thumbColor, android.graphics.PorterDuff.Mode.MULTIPLY);
                return;
            }
            return;
        }
        if (trackDrawable != null) {
            trackDrawable.setTint(trackColor);
        }
        if (thumbDrawable != null) {
            thumbDrawable.setTint(thumbColor);
        }
    }
}
