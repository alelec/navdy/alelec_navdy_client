package com.navdy.client.app.ui.homescreen;

public class SuggestedDestinationsAdapter extends android.support.v7.widget.RecyclerView.Adapter<android.support.v7.widget.RecyclerView.ViewHolder> implements android.view.View.OnLongClickListener, com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener {
    private static final float DESELECTED_SUGGESTION_ELEVATION = 0.0f;
    private static final float SELECTED_SUGGESTION_ELEVATION = 8.0f;
    public static final int SUGGESTION_LIST_MAX_SIZE = 20;
    private static final boolean VERBOSE = false;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.class);
    private com.navdy.client.app.ui.customviews.ActiveTripCardView activeTripCardView;
    private final android.content.Context context;
    int googleMapHeight = -1;
    int hereMapHeight = -1;
    private com.navdy.client.app.framework.util.CustomItemClickListener listener;
    private com.navdy.client.app.framework.util.CustomSuggestionsLongItemClickListener longClickListener;
    private final com.navdy.client.app.framework.navigation.NavdyRouteHandler navdyRouteHandler;
    int offlineBannerHeight = -1;
    private com.navdy.client.app.ui.customviews.PendingTripCardView pendingTripCardView;
    private android.support.v7.widget.RecyclerView recyclerView;
    private android.view.View selectedItem;
    private int selectedItemPosition = -1;
    private com.navdy.client.app.framework.models.Suggestion.SuggestionType selectedSuggestionType;
    @android.support.annotation.NonNull
    private java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> suggestions = new java.util.ArrayList<>(20);

    class Anon1 implements android.view.View.OnClickListener {
        final /* synthetic */ android.support.v7.widget.RecyclerView.ViewHolder val$viewHolder;

        Anon1(android.support.v7.widget.RecyclerView.ViewHolder viewHolder) {
            this.val$viewHolder = viewHolder;
        }

        public void onClick(android.view.View v) {
            if (com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.selectedItemPosition < 0) {
                com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.listener.onClick(this.val$viewHolder.itemView, this.val$viewHolder.getAdapterPosition());
            }
        }
    }

    class Anon2 implements android.view.View.OnClickListener {
        final /* synthetic */ com.navdy.client.app.ui.homescreen.ActiveTripViewHolder val$activeTripViewHolder;

        Anon2(com.navdy.client.app.ui.homescreen.ActiveTripViewHolder activeTripViewHolder) {
            this.val$activeTripViewHolder = activeTripViewHolder;
        }

        public void onClick(android.view.View view) {
            com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.listener.onClick(view, this.val$activeTripViewHolder.getAdapterPosition());
        }
    }

    class Anon3 implements android.view.View.OnClickListener {
        final /* synthetic */ com.navdy.client.app.ui.homescreen.ActiveTripViewHolder val$activeTripViewHolder;
        final /* synthetic */ com.navdy.client.app.framework.models.Suggestion val$suggestion;

        Anon3(com.navdy.client.app.ui.homescreen.ActiveTripViewHolder activeTripViewHolder, com.navdy.client.app.framework.models.Suggestion suggestion) {
            this.val$activeTripViewHolder = activeTripViewHolder;
            this.val$suggestion = suggestion;
        }

        public void onClick(android.view.View view) {
            int adapterPosition = this.val$activeTripViewHolder.getAdapterPosition();
            if (com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.selectedItemPosition >= 0) {
                com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.triggerLongClick(true, this.val$activeTripViewHolder.itemView, adapterPosition, this.val$suggestion);
            } else if (com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.listener != null) {
                com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.listener.onClick(view, adapterPosition);
            }
        }
    }

    class Anon4 implements android.view.View.OnLongClickListener {
        final /* synthetic */ com.navdy.client.app.ui.homescreen.ActiveTripViewHolder val$activeTripViewHolder;
        final /* synthetic */ com.navdy.client.app.framework.models.Suggestion val$suggestion;

        Anon4(com.navdy.client.app.ui.homescreen.ActiveTripViewHolder activeTripViewHolder, com.navdy.client.app.framework.models.Suggestion suggestion) {
            this.val$activeTripViewHolder = activeTripViewHolder;
            this.val$suggestion = suggestion;
        }

        public boolean onLongClick(android.view.View view) {
            com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.longClickListener.onItemLongClick(view, this.val$activeTripViewHolder.getAdapterPosition(), this.val$suggestion);
            return true;
        }
    }

    class Anon5 implements android.view.View.OnClickListener {
        final /* synthetic */ com.navdy.client.app.ui.homescreen.PendingTripViewHolder val$pendingTripViewHolder;
        final /* synthetic */ com.navdy.client.app.framework.models.Suggestion val$suggestion;

        Anon5(com.navdy.client.app.ui.homescreen.PendingTripViewHolder pendingTripViewHolder, com.navdy.client.app.framework.models.Suggestion suggestion) {
            this.val$pendingTripViewHolder = pendingTripViewHolder;
            this.val$suggestion = suggestion;
        }

        public void onClick(android.view.View view) {
            int adapterPosition = this.val$pendingTripViewHolder.getAdapterPosition();
            if (com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.selectedItemPosition >= 0) {
                com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.triggerLongClick(true, this.val$pendingTripViewHolder.itemView, adapterPosition, this.val$suggestion);
            } else if (com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.listener != null) {
                com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.listener.onClick(view, adapterPosition);
            }
        }
    }

    class Anon6 implements android.view.View.OnLongClickListener {
        final /* synthetic */ com.navdy.client.app.ui.homescreen.PendingTripViewHolder val$pendingTripViewHolder;
        final /* synthetic */ com.navdy.client.app.framework.models.Suggestion val$suggestion;

        Anon6(com.navdy.client.app.ui.homescreen.PendingTripViewHolder pendingTripViewHolder, com.navdy.client.app.framework.models.Suggestion suggestion) {
            this.val$pendingTripViewHolder = pendingTripViewHolder;
            this.val$suggestion = suggestion;
        }

        public boolean onLongClick(android.view.View view) {
            com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.longClickListener.onItemLongClick(view, this.val$pendingTripViewHolder.getAdapterPosition(), this.val$suggestion);
            return true;
        }
    }

    class Anon7 implements android.view.View.OnClickListener {
        final /* synthetic */ android.view.View val$background;
        final /* synthetic */ boolean val$shouldSendToHud;
        final /* synthetic */ com.navdy.client.app.framework.models.Suggestion val$suggestion;
        final /* synthetic */ com.navdy.client.app.ui.homescreen.SuggestionsViewHolder val$viewHolder;

        Anon7(com.navdy.client.app.ui.homescreen.SuggestionsViewHolder suggestionsViewHolder, boolean z, android.view.View view, com.navdy.client.app.framework.models.Suggestion suggestion) {
            this.val$viewHolder = suggestionsViewHolder;
            this.val$shouldSendToHud = z;
            this.val$background = view;
            this.val$suggestion = suggestion;
        }

        public void onClick(android.view.View view) {
            int adapterPosition = this.val$viewHolder.getAdapterPosition();
            if (com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.selectedItemPosition >= 0) {
                com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.triggerLongClick(this.val$shouldSendToHud, this.val$background, adapterPosition, this.val$suggestion);
            } else if (com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.listener != null) {
                com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.listener.onClick(view, adapterPosition);
            }
        }
    }

    class Anon8 implements android.view.View.OnLongClickListener {
        final /* synthetic */ android.view.View val$background;
        final /* synthetic */ boolean val$shouldSendToHud;
        final /* synthetic */ com.navdy.client.app.framework.models.Suggestion val$suggestion;
        final /* synthetic */ com.navdy.client.app.ui.homescreen.SuggestionsViewHolder val$viewHolder;

        Anon8(boolean z, android.view.View view, com.navdy.client.app.ui.homescreen.SuggestionsViewHolder suggestionsViewHolder, com.navdy.client.app.framework.models.Suggestion suggestion) {
            this.val$shouldSendToHud = z;
            this.val$background = view;
            this.val$viewHolder = suggestionsViewHolder;
            this.val$suggestion = suggestion;
        }

        public boolean onLongClick(android.view.View view) {
            return com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.this.triggerLongClick(this.val$shouldSendToHud, this.val$background, this.val$viewHolder.getAdapterPosition(), this.val$suggestion);
        }
    }

    SuggestedDestinationsAdapter(android.content.Context context2, int googleMapHeight2, int hereMapHeight2, int offlineBannerHeight2) {
        this.context = context2;
        this.navdyRouteHandler = com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance();
        this.googleMapHeight = googleMapHeight2;
        this.hereMapHeight = hereMapHeight2;
        this.offlineBannerHeight = offlineBannerHeight2;
        rebuildSuggestions();
    }

    int getGoogleMapHeight() {
        return this.googleMapHeight - (com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet() ? 0 : this.offlineBannerHeight);
    }

    int getHereMapHeight() {
        return this.hereMapHeight - (com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet() ? 0 : this.offlineBannerHeight);
    }

    @android.support.annotation.UiThread
    void onSuggestionBuildComplete(java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> suggestions2) {
        this.suggestions = suggestions2;
        notifyDataSetChanged();
    }

    @android.support.annotation.MainThread
    void rebuildSuggestions() {
        stopRefreshingAllEtas();
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        this.selectedItem = null;
        this.selectedItemPosition = -1;
        this.selectedSuggestionType = null;
        this.suggestions = com.navdy.client.app.framework.util.SuggestionManager.getEmptySuggestionState();
        notifyDataSetChanged();
        com.navdy.client.app.framework.util.SuggestionManager.rebuildSuggestionListAndSendToHudAsync();
        if (this.pendingTripCardView != null) {
            this.pendingTripCardView.setNameAddressAndIcon(com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance().getCurrentDestination());
        }
    }

    void setClickListener(com.navdy.client.app.framework.util.CustomItemClickListener onClickListener) {
        this.listener = onClickListener;
    }

    void setSuggestionLongClickListener(com.navdy.client.app.framework.util.CustomSuggestionsLongItemClickListener onLongClickListener) {
        this.longClickListener = onLongClickListener;
    }

    @android.support.annotation.MainThread
    public com.navdy.client.app.framework.models.Suggestion getItem(int position) {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        int position2 = position - 1;
        if (position2 < 0 || position2 >= this.suggestions.size()) {
            return null;
        }
        return (com.navdy.client.app.framework.models.Suggestion) this.suggestions.get(position2);
    }

    void setSelectedSuggestion(android.view.View selectedCardRow, com.navdy.client.app.framework.models.Suggestion.SuggestionType suggestionType, int actionModePosition) {
        if (this.selectedItem != null) {
            setViewToNormalMode(this.selectedItem, this.selectedSuggestionType);
        }
        this.selectedItem = selectedCardRow;
        this.selectedItemPosition = actionModePosition;
        this.selectedSuggestionType = suggestionType;
        if (selectedCardRow != null) {
            setViewToSelectedMode(selectedCardRow, suggestionType);
        }
    }

    void endSelectionMode() {
        setViewToNormalMode(this.selectedItem, this.selectedSuggestionType);
        this.selectedItemPosition = -1;
        this.selectedItem = null;
        this.selectedSuggestionType = null;
    }

    void clickPendingTripCard(android.content.Context context2) {
        if (this.pendingTripCardView != null) {
            this.pendingTripCardView.handleOnClick(context2);
        }
    }

    void clickActiveTripCard() {
        if (this.activeTripCardView != null) {
            this.activeTripCardView.handleOnClick();
        }
    }

    public void onAttachedToRecyclerView(android.support.v7.widget.RecyclerView recyclerView2) {
        super.onAttachedToRecyclerView(recyclerView2);
        this.recyclerView = recyclerView2;
        this.navdyRouteHandler.addListener(this);
    }

    public void onDetachedFromRecyclerView(android.support.v7.widget.RecyclerView recyclerView2) {
        this.navdyRouteHandler.removeListener(this);
        clearAllAnimations();
        stopRefreshingAllEtas();
        this.recyclerView = null;
        super.onDetachedFromRecyclerView(recyclerView2);
    }

    public void onViewRecycled(android.support.v7.widget.RecyclerView.ViewHolder holder) {
        clearAnimationIfRunning(holder);
        super.onViewRecycled(holder);
    }

    public boolean onFailedToRecycleView(android.support.v7.widget.RecyclerView.ViewHolder holder) {
        clearAnimationIfRunning(holder);
        return super.onFailedToRecycleView(holder);
    }

    void clearAllAnimations() {
        logger.d("Clearing all animations in the suggestion adapter.");
        int count = this.recyclerView.getChildCount();
        for (int i = 0; i < count; i++) {
            try {
                clearAnimationIfRunning(this.recyclerView.findViewHolderForAdapterPosition(i));
            } catch (Throwable t) {
                logger.e("Unable to clear animations", t);
            }
        }
        android.support.v7.widget.RecyclerView.RecycledViewPool pool = this.recyclerView.getRecycledViewPool();
        if (pool != null) {
            while (true) {
                android.support.v7.widget.RecyclerView.ViewHolder holder = pool.getRecycledView(com.navdy.client.app.framework.models.Suggestion.SuggestionType.LOADING.ordinal());
                if (holder != null) {
                    clearAnimationIfRunning(holder);
                } else {
                    return;
                }
            }
        }
    }

    void stopRefreshingAllEtas() {
        logger.d("Removing all ETA refreshers");
        com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.handler.removeCallbacksAndMessages(null);
    }

    private void clearAnimationIfRunning(android.support.v7.widget.RecyclerView.ViewHolder holder) {
        if (holder != null && (holder instanceof com.navdy.client.app.ui.homescreen.ContentLoadingViewHolder)) {
            ((com.navdy.client.app.ui.homescreen.ContentLoadingViewHolder) holder).stopAnimation();
        }
    }

    public android.support.v7.widget.RecyclerView.ViewHolder onCreateViewHolder(android.view.ViewGroup parent, int viewType) {
        if (viewType == com.navdy.client.app.framework.models.Suggestion.SuggestionType.LOADING.ordinal()) {
            return new com.navdy.client.app.ui.homescreen.ContentLoadingViewHolder(android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.list_item_loading, parent, false));
        }
        if (viewType == com.navdy.client.app.framework.models.Suggestion.SuggestionType.GOOGLE_HEADER.ordinal()) {
            android.view.View v = android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.google_map_header, parent, false);
            v.setLayoutParams(new android.widget.RelativeLayout.LayoutParams(-1, getGoogleMapHeight()));
            return new com.navdy.client.app.ui.homescreen.SuggestionsViewHolder(v);
        } else if (viewType == com.navdy.client.app.framework.models.Suggestion.SuggestionType.HERE_HEADER.ordinal()) {
            android.view.View v2 = android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.here_map_header, parent, false);
            v2.setLayoutParams(new android.widget.RelativeLayout.LayoutParams(-1, getHereMapHeight()));
            return new com.navdy.client.app.ui.homescreen.SuggestionsViewHolder(v2);
        } else if (viewType == com.navdy.client.app.framework.models.Suggestion.SuggestionType.GOOGLE_FOOTER.ordinal() || viewType == com.navdy.client.app.framework.models.Suggestion.SuggestionType.HERE_FOOTER.ordinal()) {
            android.view.View v3 = android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.suggestion_footer, parent, false);
            int footerHeight = getGoogleMapHeight();
            if (viewType == com.navdy.client.app.framework.models.Suggestion.SuggestionType.HERE_FOOTER.ordinal()) {
                footerHeight = getHereMapHeight();
            }
            android.content.res.Resources resources = this.context.getResources();
            if (resources != null) {
                footerHeight += resources.getDimensionPixelSize(com.navdy.client.R.dimen.suggestion_list_top_height);
            }
            v3.setLayoutParams(new android.widget.RelativeLayout.LayoutParams(-1, footerHeight));
            return new com.navdy.client.app.ui.homescreen.SectionHeaderViewHolder(v3);
        } else if (viewType == com.navdy.client.app.framework.models.Suggestion.SuggestionType.ACTIVE_TRIP.ordinal()) {
            if (this.activeTripCardView == null) {
                this.activeTripCardView = (com.navdy.client.app.ui.customviews.ActiveTripCardView) android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.active_trip_card, parent, false);
            }
            android.widget.ImageView chevron = (android.widget.ImageView) this.activeTripCardView.findViewById(com.navdy.client.R.id.chevron);
            if (chevron != null) {
                chevron.setVisibility(View.VISIBLE);
            }
            return new com.navdy.client.app.ui.homescreen.ActiveTripViewHolder(this.activeTripCardView);
        } else if (viewType == com.navdy.client.app.framework.models.Suggestion.SuggestionType.PENDING_TRIP.ordinal()) {
            if (this.pendingTripCardView == null) {
                this.pendingTripCardView = (com.navdy.client.app.ui.customviews.PendingTripCardView) android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.pending_trip_card, parent, false);
            }
            logger.v("viewType for Pending Route found: " + viewType);
            return new com.navdy.client.app.ui.homescreen.PendingTripViewHolder(this.pendingTripCardView);
        } else if (viewType == com.navdy.client.app.framework.models.Suggestion.SuggestionType.SECTION_HEADER.ordinal()) {
            return new com.navdy.client.app.ui.homescreen.SectionHeaderViewHolder(android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.suggestion_section_header, parent, false));
        } else {
            android.view.View v4 = android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.list_item, parent, false);
            if (isRoutedSuggestion(viewType)) {
                return new com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder(v4);
            }
            return new com.navdy.client.app.ui.homescreen.SuggestionsViewHolder(v4);
        }
    }

    private boolean isRoutedSuggestion(int viewType) {
        return viewType == com.navdy.client.app.framework.models.Suggestion.SuggestionType.CALENDAR.ordinal() || viewType == com.navdy.client.app.framework.models.Suggestion.SuggestionType.RECOMMENDATION.ordinal();
    }

    @android.support.annotation.MainThread
    public void onBindViewHolder(android.support.v7.widget.RecyclerView.ViewHolder viewHolder, int unreliablePosition) {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        if (viewHolder == null || viewHolder.itemView == null) {
            logger.e("onBindViewHolder called on a null viewHolder !!!");
            return;
        }
        int suggestionPosition = unreliablePosition - 1;
        if (!isValidSuggestion(suggestionPosition) || isNotSuggestionType(viewHolder)) {
            if (viewHolder instanceof com.navdy.client.app.ui.homescreen.ContentLoadingViewHolder) {
                ((com.navdy.client.app.ui.homescreen.ContentLoadingViewHolder) viewHolder).startAnimation();
            }
            if (viewHolder.getItemViewType() == com.navdy.client.app.framework.models.Suggestion.SuggestionType.GOOGLE_HEADER.ordinal()) {
                viewHolder.itemView.setLayoutParams(new android.widget.RelativeLayout.LayoutParams(-1, getGoogleMapHeight()));
            }
            if (viewHolder.getItemViewType() == com.navdy.client.app.framework.models.Suggestion.SuggestionType.HERE_HEADER.ordinal()) {
                viewHolder.itemView.setLayoutParams(new android.widget.RelativeLayout.LayoutParams(-1, getHereMapHeight()));
            }
            if (this.listener != null) {
                viewHolder.itemView.setOnClickListener(new com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.Anon1(viewHolder));
                return;
            }
            return;
        }
        com.navdy.client.app.framework.models.Suggestion suggestion = (com.navdy.client.app.framework.models.Suggestion) this.suggestions.get(suggestionPosition);
        if (viewHolder instanceof com.navdy.client.app.ui.homescreen.ActiveTripViewHolder) {
            onBindActiveTrip((com.navdy.client.app.ui.homescreen.ActiveTripViewHolder) viewHolder, suggestion);
        } else if (viewHolder instanceof com.navdy.client.app.ui.homescreen.PendingTripViewHolder) {
            logger.v("pendingTripViewHolder found: " + suggestion.destination.name + " at adapter position: " + unreliablePosition);
            onBindPendingTrip((com.navdy.client.app.ui.homescreen.PendingTripViewHolder) viewHolder, suggestion);
        } else if (viewHolder instanceof com.navdy.client.app.ui.homescreen.SectionHeaderViewHolder) {
            onBindSectionHeader((com.navdy.client.app.ui.homescreen.SectionHeaderViewHolder) viewHolder, suggestion, suggestionPosition);
        } else if (viewHolder instanceof com.navdy.client.app.ui.homescreen.SuggestionsViewHolder) {
            com.navdy.client.app.ui.homescreen.SuggestionsViewHolder suggestionsViewHolder = (com.navdy.client.app.ui.homescreen.SuggestionsViewHolder) viewHolder;
            onBindSuggestion(suggestionsViewHolder, suggestion);
            if (viewHolder instanceof com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder) {
                ((com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder) viewHolder).onBindViewHolder(suggestion);
            } else {
                suggestionsViewHolder.subIllustrationText.setVisibility(View.GONE);
            }
        }
    }

    @android.support.annotation.MainThread
    public int getItemCount() {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        return this.suggestions.size() + 2;
    }

    @android.support.annotation.MainThread
    public int getItemViewType(int position) {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        if (position == 0) {
            if (firstItemIsActiveTrip() || firstItemIsPendingTrip()) {
                return com.navdy.client.app.framework.models.Suggestion.SuggestionType.HERE_HEADER.ordinal();
            }
            return com.navdy.client.app.framework.models.Suggestion.SuggestionType.GOOGLE_HEADER.ordinal();
        } else if (position != getItemCount() - 1) {
            int position2 = position - 1;
            if (isValidIndex(position2)) {
                return ((com.navdy.client.app.framework.models.Suggestion) this.suggestions.get(position2)).getType().ordinal();
            }
            return com.navdy.client.app.framework.models.Suggestion.SuggestionType.RECENT.ordinal();
        } else if (firstItemIsActiveTrip() || firstItemIsPendingTrip()) {
            return com.navdy.client.app.framework.models.Suggestion.SuggestionType.HERE_FOOTER.ordinal();
        } else {
            return com.navdy.client.app.framework.models.Suggestion.SuggestionType.GOOGLE_FOOTER.ordinal();
        }
    }

    public boolean onLongClick(android.view.View v) {
        return false;
    }

    public void onPendingRouteCalculating(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        addPendingTripCard(destination);
    }

    public void onPendingRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo pendingRoute) {
    }

    public void onRouteCalculating(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
    }

    public void onRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
    }

    public void onRouteStarted(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
        addActiveTripCard(route.getDestination());
    }

    public void onTripProgress(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo progress) {
    }

    public void onReroute() {
    }

    public void onRouteArrived(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
    }

    public void onStopRoute() {
        removeTripCard();
    }

    @android.support.annotation.MainThread
    private boolean isValidIndex(int position) {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        return position >= 0 && !this.suggestions.isEmpty() && position < this.suggestions.size() && this.suggestions.get(position) != null;
    }

    private boolean isNotSuggestionType(android.support.v7.widget.RecyclerView.ViewHolder viewHolder) {
        return !(viewHolder instanceof com.navdy.client.app.ui.homescreen.SuggestionsViewHolder) && !(viewHolder instanceof com.navdy.client.app.ui.homescreen.ActiveTripViewHolder) && !(viewHolder instanceof com.navdy.client.app.ui.homescreen.PendingTripViewHolder) && !(viewHolder instanceof com.navdy.client.app.ui.homescreen.SectionHeaderViewHolder);
    }

    @android.support.annotation.MainThread
    private boolean isValidSuggestion(int suggestionPosition) {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        return isValidIndex(suggestionPosition) && ((com.navdy.client.app.framework.models.Suggestion) this.suggestions.get(suggestionPosition)).destination != null;
    }

    private void onBindActiveTrip(com.navdy.client.app.ui.homescreen.ActiveTripViewHolder activeTripViewHolder, com.navdy.client.app.framework.models.Suggestion suggestion) {
        activeTripViewHolder.itemView.setOnClickListener(new com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.Anon2(activeTripViewHolder));
        if (activeTripViewHolder.getAdapterPosition() == this.selectedItemPosition) {
            setViewToSelectedMode(activeTripViewHolder.itemView, com.navdy.client.app.framework.models.Suggestion.SuggestionType.ACTIVE_TRIP);
        } else {
            setViewToNormalMode(activeTripViewHolder.itemView, com.navdy.client.app.framework.models.Suggestion.SuggestionType.ACTIVE_TRIP);
        }
        activeTripViewHolder.itemView.setOnClickListener(new com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.Anon3(activeTripViewHolder, suggestion));
        if (this.longClickListener != null) {
            activeTripViewHolder.itemView.setOnLongClickListener(new com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.Anon4(activeTripViewHolder, suggestion));
        }
    }

    private void onBindPendingTrip(com.navdy.client.app.ui.homescreen.PendingTripViewHolder pendingTripViewHolder, com.navdy.client.app.framework.models.Suggestion suggestion) {
        if (suggestion != null && suggestion.destination != null) {
            if (suggestion.isPendingTrip()) {
                if (pendingTripViewHolder.getAdapterPosition() == this.selectedItemPosition) {
                    setViewToSelectedMode(pendingTripViewHolder.itemView, com.navdy.client.app.framework.models.Suggestion.SuggestionType.PENDING_TRIP);
                } else {
                    setViewToNormalMode(pendingTripViewHolder.itemView, com.navdy.client.app.framework.models.Suggestion.SuggestionType.PENDING_TRIP);
                }
            }
            pendingTripViewHolder.itemView.setOnClickListener(new com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.Anon5(pendingTripViewHolder, suggestion));
            if (this.longClickListener != null) {
                pendingTripViewHolder.itemView.setOnLongClickListener(new com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.Anon6(pendingTripViewHolder, suggestion));
            }
        }
    }

    private void onBindSectionHeader(com.navdy.client.app.ui.homescreen.SectionHeaderViewHolder sectionHeaderViewHolder, com.navdy.client.app.framework.models.Suggestion suggestion, int suggestionPosition) {
        if (!(sectionHeaderViewHolder.title == null || suggestion == null || suggestion.destination == null)) {
            sectionHeaderViewHolder.title.setText(suggestion.destination.name);
        }
        if (suggestionPosition == 0) {
            sectionHeaderViewHolder.chevron.setVisibility(View.VISIBLE);
        } else {
            sectionHeaderViewHolder.chevron.setVisibility(View.GONE);
        }
    }

    private void onBindSuggestion(com.navdy.client.app.ui.homescreen.SuggestionsViewHolder suggestionViewHolder, com.navdy.client.app.framework.models.Suggestion suggestion) {
        onBindSuggestionIconIllustration(suggestion, suggestionViewHolder);
        if (suggestion.shouldShowRightChevron()) {
            suggestionViewHolder.rightChevron.setVisibility(View.VISIBLE);
        } else {
            suggestionViewHolder.rightChevron.setVisibility(View.GONE);
        }
        suggestionViewHolder.infoButton.setImageResource(com.navdy.client.R.drawable.ic_info_outline_grey_500);
        onBindSuggestionNameAndAddress(suggestion, suggestionViewHolder);
        boolean shouldSendToHud = suggestion.shouldSendToHud();
        suggestionViewHolder.separator.setVisibility(View.VISIBLE);
        onBindSuggestionClickListeners(suggestionViewHolder, suggestion, suggestionViewHolder, shouldSendToHud);
    }

    private void onBindSuggestionIconIllustration(com.navdy.client.app.framework.models.Suggestion suggestion, com.navdy.client.app.ui.homescreen.SuggestionsViewHolder suggestionViewHolder) {
        boolean isSelected;
        if (suggestionViewHolder.getAdapterPosition() == this.selectedItemPosition) {
            isSelected = true;
        } else {
            isSelected = false;
        }
        int badgeAsset = suggestion.getBadgeAsset(isSelected);
        if (badgeAsset > 0) {
            suggestionViewHolder.icon.setImageResource(badgeAsset);
        } else {
            suggestionViewHolder.icon.setImageBitmap(null);
        }
        setBackground(suggestionViewHolder.background, suggestion.getType(), false);
    }

    private void onBindSuggestionNameAndAddress(com.navdy.client.app.framework.models.Suggestion suggestion, com.navdy.client.app.ui.homescreen.SuggestionsViewHolder suggestionViewHolder) {
        if (!suggestion.hasInfoButton()) {
            suggestionViewHolder.infoButton.setVisibility(View.INVISIBLE);
        }
        suggestionViewHolder.icon.setVisibility(View.VISIBLE);
        if (suggestion.destination != null) {
            if (suggestion.isTip()) {
                suggestionViewHolder.firstLine.setText(suggestion.destination.name);
                suggestionViewHolder.secondLine.setText(suggestion.destination.getAddressForDisplay());
            } else if (!suggestion.isCalendar() || suggestion.event == null || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(suggestion.event.displayName)) {
                android.util.Pair<java.lang.String, java.lang.String> splitAddress = suggestion.destination.getTitleAndSubtitle();
                suggestionViewHolder.firstLine.setText((java.lang.CharSequence) splitAddress.first);
                suggestionViewHolder.secondLine.setText((java.lang.CharSequence) splitAddress.second);
            } else {
                suggestionViewHolder.firstLine.setText(suggestion.event.displayName);
                suggestionViewHolder.secondLine.setText(suggestion.destination.getAddressForDisplay());
            }
            if (suggestion.isCalendar()) {
                suggestionViewHolder.thirdLine.setText(this.context.getString(com.navdy.client.R.string.event_times, new java.lang.Object[]{suggestion.event.getStartTime(), suggestion.event.getEndTime()}));
                suggestionViewHolder.thirdLine.setVisibility(View.VISIBLE);
                return;
            }
            suggestionViewHolder.thirdLine.setVisibility(View.GONE);
        }
    }

    private void onBindSuggestionClickListeners(com.navdy.client.app.ui.homescreen.SuggestionsViewHolder viewHolder, com.navdy.client.app.framework.models.Suggestion suggestion, com.navdy.client.app.ui.homescreen.SuggestionsViewHolder suggestionViewHolder, boolean shouldSendToHud) {
        android.view.View background = viewHolder.background;
        android.view.View.OnClickListener clickListener = new com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.Anon7(viewHolder, shouldSendToHud, background, suggestion);
        suggestionViewHolder.row.setOnClickListener(clickListener);
        suggestionViewHolder.infoButton.setOnClickListener(clickListener);
        suggestionViewHolder.row.setOnLongClickListener(new com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.Anon8(shouldSendToHud, background, viewHolder, suggestion));
    }

    private boolean triggerLongClick(boolean shouldSendToHud, android.view.View background, int position, com.navdy.client.app.framework.models.Suggestion suggestion) {
        if (this.longClickListener == null || (!shouldSendToHud && !suggestion.isPendingTrip())) {
            return false;
        }
        this.longClickListener.onItemLongClick(background, position, suggestion);
        return true;
    }

    private void setBackground(android.view.View view, com.navdy.client.app.framework.models.Suggestion.SuggestionType type, boolean selected) {
        if (view != null && type != com.navdy.client.app.framework.models.Suggestion.SuggestionType.ACTIVE_TRIP) {
            if (selected) {
                view.setBackgroundColor(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.grey_light_card));
            } else {
                view.setBackgroundColor(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.white));
            }
        }
    }

    private void setViewToSelectedMode(android.view.View view, com.navdy.client.app.framework.models.Suggestion.SuggestionType suggestionType) {
        if (view != null) {
            setBackground(view, suggestionType, true);
            android.widget.ImageView illustration = getIllustrationView(view);
            if (illustration != null) {
                illustration.setTag(illustration.getDrawable());
                illustration.setImageResource(com.navdy.client.R.drawable.icon_suggested_edit);
            }
            if (suggestionType != com.navdy.client.app.framework.models.Suggestion.SuggestionType.PENDING_TRIP && suggestionType != com.navdy.client.app.framework.models.Suggestion.SuggestionType.ACTIVE_TRIP) {
                android.support.v4.view.ViewCompat.setElevation(view, SELECTED_SUGGESTION_ELEVATION);
            }
        }
    }

    private void setViewToNormalMode(android.view.View view, com.navdy.client.app.framework.models.Suggestion.SuggestionType suggestionType) {
        if (view != null) {
            setBackground(view, suggestionType, false);
            android.widget.ImageView illustration = getIllustrationView(view);
            if (illustration != null) {
                illustration.setImageDrawable((android.graphics.drawable.Drawable) illustration.getTag());
            }
            android.support.v4.view.ViewCompat.setElevation(view, 0.0f);
        }
    }

    private android.widget.ImageView getIllustrationView(android.view.View view) {
        if (view == null) {
            return null;
        }
        return (android.widget.ImageView) view.findViewById(com.navdy.client.R.id.illustration);
    }

    private void addActiveTripCard(com.navdy.client.app.framework.models.Destination destination) {
        addTripCard(destination, com.navdy.client.app.framework.models.Suggestion.SuggestionType.ACTIVE_TRIP);
    }

    private void addPendingTripCard(com.navdy.client.app.framework.models.Destination destination) {
        addTripCard(destination, com.navdy.client.app.framework.models.Suggestion.SuggestionType.PENDING_TRIP);
    }

    @android.support.annotation.MainThread
    private void addTripCard(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.models.Suggestion.SuggestionType suggestionType) {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        com.navdy.client.app.framework.util.SuggestionManager.setTripAsync(new com.navdy.client.app.framework.models.Suggestion(destination, suggestionType));
    }

    @android.support.annotation.MainThread
    private void removeTripCard() {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        com.navdy.client.app.framework.util.SuggestionManager.setTripAsync(null);
    }

    private boolean firstItemIsActiveTrip() {
        return firstItemIs(com.navdy.client.app.framework.models.Suggestion.SuggestionType.ACTIVE_TRIP);
    }

    private boolean firstItemIsPendingTrip() {
        return firstItemIs(com.navdy.client.app.framework.models.Suggestion.SuggestionType.PENDING_TRIP);
    }

    @android.support.annotation.MainThread
    private boolean firstItemIs(com.navdy.client.app.framework.models.Suggestion.SuggestionType suggestionType) {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        return this.suggestions.size() > 0 && this.suggestions.get(0) != null && ((com.navdy.client.app.framework.models.Suggestion) this.suggestions.get(0)).getType() == suggestionType;
    }
}
