package com.navdy.client.app.ui.settings;

public class SettingsUtils {
    private static final java.lang.String FLAT_SUPPORTED = "flatsupported";
    private static final java.lang.String MAKE = "make";
    private static final java.lang.String MEDIUM_SUPPORTED = "mediumsupported";
    private static final java.lang.String MODEL = "model";
    private static final java.lang.String RECOMMENDED = "recommended";
    private static final java.lang.String TALL_SUPPORTED = "tallsupported";
    private static final java.lang.String YEAR = "year";
    private static java.lang.String lastMake = "";
    private static java.lang.String lastModel = "";
    private static com.navdy.client.app.framework.models.MountInfo lastMountInfo = new com.navdy.client.app.framework.models.MountInfo();
    private static java.lang.String lastYear = "";
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.settings.SettingsUtils.class);
    private static boolean updateSettingsUponNextConnection = false;

    static class Anon1 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Boolean> {
        Anon1() {
        }

        /* access modifiers changed from: protected */
        public java.lang.Boolean doInBackground(java.lang.Void... voids) {
            boolean z;
            if (!com.navdy.client.app.ui.settings.SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue() || !com.navdy.client.app.ui.settings.SettingsUtils.sendHudSettingsBasedOnSharedPrefValue() || !com.navdy.client.app.ui.settings.SettingsUtils.sendDialSettingsToTheHudBasedOnSharedPrefValue() || !com.navdy.client.app.ui.settings.SettingsUtils.sendNavSettingsToTheHudBasedOnSharedPrefValue() || !com.navdy.client.app.ui.settings.SettingsUtils.sendMessagingSettingsToTheHudBasedOnSharedPrefValue() || !com.navdy.client.app.ui.glances.GlanceUtils.sendGlancesSettingsToTheHudBasedOnSharedPrefValue()) {
                z = false;
            } else {
                z = true;
            }
            return java.lang.Boolean.valueOf(z);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(java.lang.Boolean success) {
            if (success.booleanValue()) {
                com.navdy.client.app.ui.settings.SettingsUtils.setUpdateSettingsUponNextConnection(false);
            } else {
                com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.settings_all_updated_failure, new java.lang.Object[0]);
            }
        }
    }

    static class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem val$unitSystem;

        Anon2(com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem unitSystem) {
            this.val$unitSystem = unitSystem;
        }

        public void run() {
            com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences().edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.UNIT_SYSTEM, this.val$unitSystem.name()).apply();
            com.navdy.client.app.ui.settings.SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
        }
    }

    static class Anon3 implements java.lang.Runnable {
        final /* synthetic */ boolean val$isLimited;

        Anon3(boolean z) {
            this.val$isLimited = z;
        }

        public void run() {
            com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.LIMIT_CELLULAR_DATA, this.val$isLimited).apply();
            com.navdy.client.app.ui.settings.SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
        }
    }

    static class Anon4 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode val$featureMode;

        Anon4(com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode featureMode) {
            this.val$featureMode = featureMode;
        }

        public void run() {
            com.navdy.client.app.ui.settings.SettingsUtils.incrementDriverProfileSerial();
            com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.FEATURE_MODE, this.val$featureMode.toString()).apply();
            com.navdy.client.app.ui.settings.SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
        }
    }

    static class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
            java.lang.String obdScanSettingString = com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_OFF.toString();
            if (!com.navdy.client.app.ui.settings.SettingsUtils.carIsBlacklistedForObdData()) {
                obdScanSettingString = com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_ON.toString();
            }
            com.navdy.client.app.ui.settings.SettingsUtils.logger.d("OBD Setting changed: " + obdScanSettingString);
            sharedPrefs.edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.HUD_OBD_ON, obdScanSettingString).apply();
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Settings.PROFILE_SETTINGS_CHANGED);
            com.navdy.client.app.ui.settings.SettingsUtils.incrementDriverProfileSerial();
            com.navdy.client.app.ui.settings.SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
        }
    }

    static class SettingsLogger {
        private android.content.SharedPreferences customerPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
        private android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();

        SettingsLogger() {
        }

        void logBoolean(android.content.SharedPreferences preferences, java.lang.String key, boolean defaultValue) {
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i(key + com.navdy.client.app.framework.util.CrashlyticsAppender.SEPARATOR + preferences.getBoolean(key, defaultValue));
        }

        void logString(android.content.SharedPreferences preferences, java.lang.String key, java.lang.String defaultValue) {
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i(key + com.navdy.client.app.framework.util.CrashlyticsAppender.SEPARATOR + preferences.getString(key, defaultValue));
        }

        void logLong(android.content.SharedPreferences preferences, java.lang.String key, long defaultValue) {
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i(key + com.navdy.client.app.framework.util.CrashlyticsAppender.SEPARATOR + preferences.getLong(key, defaultValue));
        }

        void logInt(android.content.SharedPreferences preferences, java.lang.String key, int defaultValue) {
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i(key + com.navdy.client.app.framework.util.CrashlyticsAppender.SEPARATOR + preferences.getInt(key, defaultValue));
        }

        void logFloat(android.content.SharedPreferences preferences, java.lang.String key, float defaultValue) {
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i(key + com.navdy.client.app.framework.util.CrashlyticsAppender.SEPARATOR + preferences.getFloat(key, defaultValue));
        }

        boolean profileSetup() {
            return com.navdy.client.app.tracking.Tracker.isUserRegistered();
        }

        boolean glancesSetup() {
            return com.navdy.client.app.ui.base.BaseActivity.weHaveNotificationPermission();
        }

        boolean displaySetup() {
            return com.navdy.client.app.framework.DeviceConnection.isConnected();
        }

        boolean carSetup() {
            return !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.customerPreferences.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "")) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.customerPreferences.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "")) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.customerPreferences.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, ""));
        }

        java.lang.CharSequence audioOutput() {
            return ((android.media.MediaRouter) com.navdy.client.app.NavdyApplication.getAppContext().getSystemService("media_router")).getSelectedRoute(1).getName();
        }

        void logConfiguration() {
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("USER CONFIGURATION ==========");
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("--- Client Version ---");
            logString(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.LAST_BUILD_VERSION, "1.0");
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("App Version: " + com.navdy.client.app.ui.homescreen.HomescreenActivity.getAppVersionString());
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("--- Navigation preferences ---");
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_ROUTE_CALCULATION, false);
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_AUTO_RECALC, false);
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_HIGHWAYS, true);
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_TOLL_ROADS, true);
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_FERRIES, true);
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_TUNNELS, true);
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_UNPAVED_ROADS, true);
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_AUTO_TRAINS, true);
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("--- Driver preferences ---");
            logLong(this.customerPreferences, com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.SERIAL_NUM, com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.SERIAL_NUM_DEFAULT.longValue());
            logString(this.customerPreferences, com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.FULL_NAME, "");
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("--- Audio preferences ---");
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_ALL_AUDIO, true);
            logInt(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_HUD_VOLUME, 100);
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_MENU_FEEDBACK, true);
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_NOTIFICATIONS, true);
            logInt(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_PHONE_VOLUME, 75);
            logFloat(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_TTS_VOLUME, 0.6f);
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_PLAY_WELCOME_MESSAGE, false);
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_TURN_BY_TURN_INSTRUCTIONS, true);
            logString(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_VOICE, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_VOICE_DEFAULT);
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_SPEED_WARNINGS, false);
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_CAMERA_WARNINGS, true);
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("--- Current audio routing ---");
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("output: " + audioOutput());
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("HFP connected: " + com.navdy.client.app.framework.util.TTSAudioRouter.isHFPSpeakerConnected());
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("A2DP connected: " + com.navdy.client.app.framework.util.TTSAudioRouter.isA2DPSpeakerConnected());
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("--- Software update ---");
            logInt(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.OTA_VERSION, -1);
            logString(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.OTA_URL, "");
            logString(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.OTA_DESCRIPTION, "");
            logLong(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.OTA_SIZE, 0);
            logString(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.OTA_STATUS, com.navdy.client.app.ui.settings.SettingsConstants.OTA_STATUS_DEFAULT);
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.OTA_IS_INCREMENTAL, false);
            logInt(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.OTA_FROM_VERSION, 0);
            logBoolean(this.sharedPreferences, "FORCE_FULL_UPDATE", false);
            logString(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.OTA_META_DATA, "");
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("--- Glances ---");
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.GLANCES, false);
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.GLANCES_READ_ALOUD, true);
            logBoolean(this.sharedPreferences, com.navdy.client.app.ui.settings.SettingsConstants.GLANCES_SHOW_CONTENT, false);
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("--- Onboarding completion ---");
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("profile setup: " + profileSetup());
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("display setup: " + displaySetup());
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("car setup: " + carSetup());
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("glances setup: " + glancesSetup());
            com.navdy.client.app.ui.settings.SettingsUtils.logger.i("========== END USER CONFIGURATION");
        }
    }

    public static void onSharedPrefsDowngrade(int oldVersion, int newVersion) {
        setSharedPrefsVersionToCurrent();
    }

    public static void onSharedPrefsUpgrade(int oldVersion, int newVersion) {
        if (oldVersion <= 0) {
            com.navdy.client.app.ui.homescreen.CalendarUtils.convertAllClendarNameSharedPrefsToCalendarIds();
        }
        setSharedPrefsVersionToCurrent();
    }

    public static void setSharedPrefsVersionToCurrent() {
        android.content.SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(com.navdy.client.app.ui.settings.SettingsConstants.SHARED_PREF_VERSION, 1);
        editor.apply();
    }

    private static synchronized void setUpdateSettingsUponNextConnection(boolean updateNextTime) {
        synchronized (com.navdy.client.app.ui.settings.SettingsUtils.class) {
            updateSettingsUponNextConnection = updateNextTime;
        }
    }

    private static synchronized boolean getUpdateSettingsUponNextConnection() {
        boolean z;
        synchronized (com.navdy.client.app.ui.settings.SettingsUtils.class) {
            z = updateSettingsUponNextConnection;
        }
        return z;
    }

    public static void updateAllSettingsIfNecessary() {
        if (getUpdateSettingsUponNextConnection()) {
            new com.navdy.client.app.ui.settings.SettingsUtils.Anon1().execute(new java.lang.Void[0]);
        }
    }

    public static void incrementDriverProfileSerial() {
        android.content.SharedPreferences customerPrefs = getCustomerPreferences();
        customerPrefs.edit().putLong(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.SERIAL_NUM, 1 + customerPrefs.getLong(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.SERIAL_NUM, com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.SERIAL_NUM_DEFAULT.longValue())).apply();
    }

    @android.support.annotation.WorkerThread
    public static boolean sendDriverProfileToTheHudBasedOnSharedPrefValue() {
        com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction dialLongPressAction;
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        android.content.SharedPreferences customerPrefs = getCustomerPreferences();
        long serial = customerPrefs.getLong(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.SERIAL_NUM, com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.SERIAL_NUM_DEFAULT.longValue());
        logger.d("serial: " + serial);
        java.lang.String fullName = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.FULL_NAME, "");
        java.lang.String email = customerPrefs.getString("email", "");
        java.lang.String carYear = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "");
        java.lang.String carMake = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "");
        java.lang.String carModel = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
        android.content.SharedPreferences settingsPrefs = getSharedPreferences();
        boolean autoOnEnabled = settingsPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_AUTO_ON, true);
        boolean uiScalingEnabled = settingsPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_UI_SCALING_ON, false);
        com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode featureMode = com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode.valueOf(settingsPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.FEATURE_MODE, com.navdy.client.app.ui.settings.SettingsConstants.FEATURE_MODE_DEFAULT));
        logger.i("featureMode: " + featureMode);
        com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat displayFormat = uiScalingEnabled ? com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat.DISPLAY_FORMAT_COMPACT : com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat.DISPLAY_FORMAT_NORMAL;
        java.lang.String phoneName = android.os.Build.MODEL;
        java.lang.String photoChecksum = com.navdy.client.app.framework.servicehandler.PhotoServiceHandler.buildPhotoChecksum(com.navdy.client.app.tracking.Tracker.getUserProfilePhoto());
        java.lang.String locale = java.util.Locale.getDefault().toString();
        com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem unitSystem = com.navdy.client.app.framework.i18n.I18nManager.getInstance().getUnitSystem();
        com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting obdScanSetting = com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.valueOf(settingsPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.HUD_OBD_ON, com.navdy.client.app.ui.settings.SettingsConstants.HUD_OBD_ON_DEFAULT));
        long obdBlacklistLastModified = getObdBlacklistLastModifiedDate();
        boolean limitCellularData = isLimitingCellularData();
        java.util.ArrayList<java.lang.String> additionalLocales = new java.util.ArrayList<>();
        android.content.Context appContext = com.navdy.client.app.NavdyApplication.getAppContext();
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            android.os.LocaleList locales = appContext.getResources().getConfiguration().getLocales();
            for (int i = 0; i < locales.size(); i++) {
                java.util.Locale availableLocale = locales.get(i);
                if (availableLocale != null) {
                    java.lang.String language = availableLocale.toString();
                    if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(locale, language)) {
                        additionalLocales.add(language);
                    }
                }
            }
        }
        if (settingsPrefs.getInt(com.navdy.client.app.ui.settings.SettingsConstants.DIAL_LONG_PRESS_ACTION, com.navdy.client.app.ui.settings.SettingsConstants.DIAL_LONG_PRESS_ACTION_DEFAULT) == com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH.getValue()) {
            dialLongPressAction = com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH;
        } else {
            dialLongPressAction = com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_VOICE_ASSISTANT;
        }
        return sendDriverProfileSettingsToTheHud(new com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder().serial_number(java.lang.Long.valueOf(serial)).driver_name(fullName).device_name(phoneName).profile_is_public(java.lang.Boolean.valueOf(false)).photo_checksum(photoChecksum).driver_email(email).car_make(carMake).car_model(carModel).car_year(carYear).auto_on_enabled(java.lang.Boolean.valueOf(autoOnEnabled)).display_format(displayFormat).locale(locale).additionalLocales(additionalLocales).unit_system(unitSystem).feature_mode(featureMode).obdScanSetting(obdScanSetting).limit_bandwidth(java.lang.Boolean.valueOf(limitCellularData)).obdBlacklistLastModified(java.lang.Long.valueOf(obdBlacklistLastModified)).dial_long_press_action(dialLongPressAction).build());
    }

    @android.support.annotation.Nullable
    public static com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem getUnitSystem() {
        java.lang.String unitSystemPreference = getCustomerPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.UNIT_SYSTEM, "");
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(unitSystemPreference, com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_IMPERIAL.name())) {
            return com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_IMPERIAL;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(unitSystemPreference, com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_METRIC.name())) {
            return com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_METRIC;
        }
        return null;
    }

    public static void setUnitSystem(com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem unitSystem) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.settings.SettingsUtils.Anon2(unitSystem), 1);
    }

    public static boolean isLimitingCellularData() {
        return getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.LIMIT_CELLULAR_DATA, false);
    }

    static void setLimitCellularData(boolean isLimited) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.settings.SettingsUtils.Anon3(isLimited), 1);
    }

    public static void setFeatureMode(com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode featureMode) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.settings.SettingsUtils.Anon4(featureMode), 1);
    }

    public static com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode getFeatureMode() {
        return com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode.valueOf(getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.FEATURE_MODE, com.navdy.client.app.ui.settings.SettingsConstants.FEATURE_MODE_DEFAULT));
    }

    public static boolean isBetaUser() {
        return getFeatureMode() == com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode.FEATURE_MODE_BETA;
    }

    private static boolean sendDriverProfileSettingsToTheHud(com.navdy.service.library.events.preferences.DriverProfilePreferences driverProfilePrefs) {
        boolean deviceIsConnected;
        if (driverProfilePrefs == null) {
            return false;
        }
        com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice == null || !remoteDevice.isConnected()) {
            deviceIsConnected = false;
        } else {
            deviceIsConnected = true;
        }
        if (!deviceIsConnected) {
            setUpdateSettingsUponNextConnection(true);
            return false;
        }
        com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate.Builder updateBldr = new com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate.Builder();
        updateBldr.status = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        updateBldr.serial_number = driverProfilePrefs.serial_number;
        updateBldr.preferences = driverProfilePrefs;
        return remoteDevice.postEvent((com.squareup.wire.Message) updateBldr.build());
    }

    static boolean sendHudSettings(boolean gestureIsEnabled) {
        return sendInputSettingsToTheHud(buildInputPreferences(getSharedPreferences().getLong(com.navdy.client.app.ui.settings.SettingsConstants.HUD_SERIAL_NUM, 0), gestureIsEnabled));
    }

    private static boolean sendHudSettingsBasedOnSharedPrefValue() {
        return sendHudSettings(getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_GESTURE, true));
    }

    public static boolean sendDialSettingsToTheHudBasedOnSharedPrefValue() {
        android.content.SharedPreferences sharedPrefs = getSharedPreferences();
        return sendInputSettingsToTheHud(buildInputPreferences(sharedPrefs.getLong(com.navdy.client.app.ui.settings.SettingsConstants.HUD_SERIAL_NUM, 0), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_GESTURE, true)));
    }

    @android.support.annotation.NonNull
    private static com.navdy.service.library.events.preferences.InputPreferences buildInputPreferences(long serialNumber, boolean useGesture) {
        return new com.navdy.service.library.events.preferences.InputPreferences(java.lang.Long.valueOf(serialNumber), java.lang.Boolean.valueOf(useGesture), com.navdy.service.library.events.preferences.InputPreferences.DialSensitivity.DIAL_SENSITIVITY_STANDARD);
    }

    private static boolean sendInputSettingsToTheHud(com.navdy.service.library.events.preferences.InputPreferences inputPrefs) {
        boolean deviceIsConnected;
        if (inputPrefs == null) {
            return false;
        }
        com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice == null || !remoteDevice.isConnected()) {
            deviceIsConnected = false;
        } else {
            deviceIsConnected = true;
        }
        if (!deviceIsConnected) {
            setUpdateSettingsUponNextConnection(true);
            return false;
        }
        com.navdy.service.library.events.preferences.InputPreferencesUpdate.Builder updateBldr = new com.navdy.service.library.events.preferences.InputPreferencesUpdate.Builder();
        updateBldr.status = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        updateBldr.serial_number = inputPrefs.serial_number;
        updateBldr.preferences = inputPrefs;
        return remoteDevice.postEvent((com.squareup.wire.Message) updateBldr.build());
    }

    public static boolean sendSpeakerSettingsToTheHudBasedOnSharedPrefValue() {
        android.content.SharedPreferences sharedPrefs = getSharedPreferences();
        return sendDisplaySpeakerSettingsToTheHud(buildDisplaySpeakerPreferences(sharedPrefs.getLong(com.navdy.client.app.ui.settings.SettingsConstants.DISPLAY_AUDIO_SERIAL_NUM, 0), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_ALL_AUDIO, true), sharedPrefs.getInt(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_HUD_VOLUME, 100), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_MENU_FEEDBACK, true), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_NOTIFICATIONS, true)));
    }

    @android.support.annotation.NonNull
    private static com.navdy.service.library.events.preferences.DisplaySpeakerPreferences buildDisplaySpeakerPreferences(long serialNumber, boolean masterSound, int volumeLevel, boolean feedbackSound, boolean notificationSound) {
        return new com.navdy.service.library.events.preferences.DisplaySpeakerPreferences(java.lang.Long.valueOf(serialNumber), java.lang.Boolean.valueOf(masterSound), java.lang.Integer.valueOf(volumeLevel), java.lang.Boolean.valueOf(feedbackSound), java.lang.Boolean.valueOf(notificationSound));
    }

    private static boolean sendDisplaySpeakerSettingsToTheHud(com.navdy.service.library.events.preferences.DisplaySpeakerPreferences speakerPrefs) {
        boolean deviceIsConnected;
        com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice == null || !remoteDevice.isConnected()) {
            deviceIsConnected = false;
        } else {
            deviceIsConnected = true;
        }
        if (!deviceIsConnected) {
            setUpdateSettingsUponNextConnection(true);
            return false;
        }
        com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesUpdate.Builder updateBldr = new com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesUpdate.Builder();
        updateBldr.status = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        updateBldr.serial_number = speakerPrefs.serial_number;
        updateBldr.preferences = speakerPrefs;
        return remoteDevice.postEvent((com.squareup.wire.Message) updateBldr.build());
    }

    public static boolean sendNavSettingsToTheHudBasedOnSharedPrefValue() {
        android.content.SharedPreferences sharedPrefs = getSharedPreferences();
        return sendNavSettingsToTheHud(buildNavigationPreferences(sharedPrefs.getLong("nav_serial_number", 0), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_ROUTE_CALCULATION, false), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_AUTO_RECALC, false), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_HIGHWAYS, true), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_TOLL_ROADS, true), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_FERRIES, true), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_TUNNELS, true), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_UNPAVED_ROADS, true), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_AUTO_TRAINS, true), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_SPEED_WARNINGS, false), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_CAMERA_WARNINGS, true), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_TURN_BY_TURN_INSTRUCTIONS, true)));
    }

    static boolean sendNavSettingsToTheHud(com.navdy.service.library.events.preferences.NavigationPreferences navPrefs) {
        boolean deviceIsConnected;
        if (navPrefs == null) {
            return false;
        }
        com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice == null || !remoteDevice.isConnected()) {
            deviceIsConnected = false;
        } else {
            deviceIsConnected = true;
        }
        if (!deviceIsConnected) {
            setUpdateSettingsUponNextConnection(true);
            return false;
        }
        com.navdy.service.library.events.preferences.NavigationPreferencesUpdate.Builder updateBldr = new com.navdy.service.library.events.preferences.NavigationPreferencesUpdate.Builder();
        updateBldr.status = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        updateBldr.serial_number = navPrefs.serial_number;
        updateBldr.preferences = navPrefs;
        return remoteDevice.postEvent((com.squareup.wire.Message) updateBldr.build());
    }

    public static boolean sendAudioPreferencesToDisplay(boolean updated) {
        android.content.SharedPreferences sharedPrefs = getSharedPreferences();
        return sendAudioPreferencesToTheDisplay(new com.navdy.service.library.events.preferences.AudioPreferences.Builder().serial_number(java.lang.Long.valueOf(updated ? incrementSerialNumber(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_SERIAL_NUM) : sharedPrefs.getLong(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_SERIAL_NUM, 0))).drivingBehaviorAlerts(java.lang.Boolean.valueOf(sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_DRIVE_SCORE_WARNINGS, true))).build());
    }

    static boolean sendAudioPreferencesToTheDisplay(com.navdy.service.library.events.preferences.AudioPreferences audioPreferences) {
        boolean deviceIsConnected;
        if (audioPreferences == null) {
            return false;
        }
        com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice == null || !remoteDevice.isConnected()) {
            deviceIsConnected = false;
        } else {
            deviceIsConnected = true;
        }
        if (deviceIsConnected) {
            return remoteDevice.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.preferences.AudioPreferencesUpdate.Builder().status(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS).serial_number(audioPreferences.serial_number).preferences(audioPreferences).build());
        }
        setUpdateSettingsUponNextConnection(true);
        return false;
    }

    @android.support.annotation.NonNull
    static com.navdy.service.library.events.preferences.NavigationPreferences buildNavigationPreferences(long serialNumber, boolean calculateShortestRouteIsOn, boolean autoRecalcIsOn, boolean highwaysIsOn, boolean tollRoadsIsOn, boolean ferriesIsOn, boolean tunnelsIsOn, boolean unpavedRoadsIsOn, boolean autoTrainsIsOn, boolean spokenSpeedLimitWarningsAreOn, boolean spokenCameraWarningsAreOn, boolean spokenTurnByTurnIsOn) {
        com.navdy.service.library.events.preferences.NavigationPreferences.Builder navPrefBldr = new com.navdy.service.library.events.preferences.NavigationPreferences.Builder();
        navPrefBldr.serial_number = java.lang.Long.valueOf(serialNumber);
        navPrefBldr.routingType = calculateShortestRouteIsOn ? com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType.ROUTING_SHORTEST : com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType.ROUTING_FASTEST;
        navPrefBldr.rerouteForTraffic = autoRecalcIsOn ? com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic.REROUTE_AUTOMATIC : com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic.REROUTE_CONFIRM;
        navPrefBldr.allowHighways = java.lang.Boolean.valueOf(highwaysIsOn);
        navPrefBldr.allowTollRoads = java.lang.Boolean.valueOf(tollRoadsIsOn);
        navPrefBldr.allowFerries = java.lang.Boolean.valueOf(ferriesIsOn);
        navPrefBldr.allowTunnels = java.lang.Boolean.valueOf(tunnelsIsOn);
        navPrefBldr.allowUnpavedRoads = java.lang.Boolean.valueOf(unpavedRoadsIsOn);
        navPrefBldr.allowAutoTrains = java.lang.Boolean.valueOf(autoTrainsIsOn);
        navPrefBldr.spokenSpeedLimitWarnings = java.lang.Boolean.valueOf(spokenSpeedLimitWarningsAreOn);
        navPrefBldr.spokenCameraWarnings = java.lang.Boolean.valueOf(spokenCameraWarningsAreOn);
        navPrefBldr.spokenTurnByTurn = java.lang.Boolean.valueOf(spokenTurnByTurnIsOn);
        return navPrefBldr.build();
    }

    public static boolean sendMessagingSettingsToTheHudBasedOnSharedPrefValue() {
        return sendMessagingSettingsToTheHud(com.navdy.client.app.ui.settings.MessagingSettingsActivity.getRepliesFromSharedPrefs(), java.lang.Long.valueOf(getSharedPreferences().getLong("nav_serial_number", 0)));
    }

    static boolean sendMessagingSettingsToTheHud(java.util.ArrayList<java.lang.String> cannedReplies, java.lang.Long serialNumber) {
        boolean deviceIsConnected;
        if (cannedReplies == null) {
            return false;
        }
        com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice == null || !remoteDevice.isConnected()) {
            deviceIsConnected = false;
        } else {
            deviceIsConnected = true;
        }
        if (!deviceIsConnected) {
            setUpdateSettingsUponNextConnection(true);
            return false;
        }
        com.navdy.service.library.events.glances.CannedMessagesUpdate.Builder cannedResponses = new com.navdy.service.library.events.glances.CannedMessagesUpdate.Builder();
        cannedResponses.status = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        cannedResponses.serial_number = serialNumber;
        cannedResponses.cannedMessage = cannedReplies;
        return remoteDevice.postEvent((com.squareup.wire.Message) cannedResponses.build());
    }

    public static android.content.SharedPreferences getSharedPreferences() {
        return com.navdy.client.app.NavdyApplication.getAppContext().getSharedPreferences(com.navdy.client.app.ui.settings.SettingsConstants.SETTINGS, 0);
    }

    public static android.content.SharedPreferences getCustomerPreferences() {
        return com.navdy.client.app.NavdyApplication.getAppContext().getSharedPreferences(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CUSTOMER_PREF_FILE, 0);
    }

    public static android.content.SharedPreferences getPermissionsSharedPreferences() {
        return com.navdy.client.app.NavdyApplication.getAppContext().getSharedPreferences(com.navdy.client.app.ui.settings.SettingsConstants.PERMISSIONS, 0);
    }

    public static long incrementSerialNumber(java.lang.String serialNumKey) {
        android.content.SharedPreferences sharedPrefs = getSharedPreferences();
        long serialNumber = sharedPrefs.getLong(serialNumKey, 0) + 1;
        sharedPrefs.edit().putLong(serialNumKey, serialNumber).apply();
        return serialNumber;
    }

    public static java.lang.String getDialerPackage(android.content.pm.PackageManager packageManager) {
        android.content.Intent i = new android.content.Intent("android.intent.action.CALL");
        i.setData(android.net.Uri.parse("tel:123456789"));
        android.content.pm.ResolveInfo resolveInfo = packageManager.resolveActivity(i, 0);
        if (resolveInfo != null) {
            android.content.pm.ActivityInfo activityInfo = resolveInfo.activityInfo;
            if (activityInfo != null) {
                return activityInfo.packageName;
            }
        }
        return "com.android.dialer";
    }

    public static java.lang.String getSmsPackage() {
        return android.provider.Telephony.Sms.getDefaultSmsPackage(com.navdy.client.app.NavdyApplication.getAppContext());
    }

    public static boolean sendGlancesSettingsToTheHud(com.navdy.service.library.events.preferences.NotificationPreferences notifPrefs) {
        boolean deviceIsConnected;
        if (notifPrefs == null) {
            return false;
        }
        com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice == null || !remoteDevice.isConnected()) {
            deviceIsConnected = false;
        } else {
            deviceIsConnected = true;
        }
        if (!deviceIsConnected) {
            setUpdateSettingsUponNextConnection(true);
            return false;
        }
        com.navdy.service.library.events.preferences.NotificationPreferencesUpdate.Builder updateBldr = new com.navdy.service.library.events.preferences.NotificationPreferencesUpdate.Builder();
        updateBldr.status = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        updateBldr.serial_number = notifPrefs.serial_number;
        updateBldr.preferences = notifPrefs;
        return remoteDevice.postEvent((com.squareup.wire.Message) updateBldr.build());
    }

    public static java.lang.String getCarYearMakeModelString() {
        android.content.SharedPreferences customerPrefs = getCustomerPreferences();
        return java.lang.String.format("%s %s %s", new java.lang.Object[]{customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, ""), customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, ""), customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "")}).trim();
    }

    public static boolean carIsBlacklistedForObdData() {
        android.content.SharedPreferences customerPrefs = getCustomerPreferences();
        java.lang.String makeFromSharedPref = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "");
        java.lang.String yearFromSharedPref = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "");
        java.lang.String modelFromSharedPref = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
        boolean modelMatches = false;
        boolean makeMatches = false;
        boolean yearMatches = false;
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(makeFromSharedPref) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(yearFromSharedPref) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(modelFromSharedPref)) {
            logger.d("No car model was found");
            return false;
        }
        java.lang.String makeFromSharedPref2 = makeFromSharedPref.trim();
        java.lang.String yearFromSharedPref2 = yearFromSharedPref.trim();
        java.lang.String modelFromSharedPref2 = modelFromSharedPref.trim();
        android.util.JsonReader reader = null;
        try {
            java.io.InputStreamReader inputStreamReader = new java.io.InputStreamReader(com.navdy.client.app.NavdyApplication.getAppContext().getResources().openRawResource(com.navdy.client.R.raw.blacklist));
            android.util.JsonReader reader2 = new android.util.JsonReader(inputStreamReader);
            while (reader2.hasNext()) {
                try {
                    if (reader2.peek() == android.util.JsonToken.BEGIN_ARRAY) {
                        reader2.beginArray();
                    }
                    if (reader2.peek() == android.util.JsonToken.BEGIN_OBJECT) {
                        reader2.beginObject();
                    }
                    while (reader2.hasNext()) {
                        java.lang.String name = reader2.nextName();
                        char c = 65535;
                        switch (name.hashCode()) {
                            case 3343854:
                                if (name.equals(MAKE)) {
                                    c = 1;
                                    break;
                                }
                                break;
                            case 3704893:
                                if (name.equals(YEAR)) {
                                    c = 2;
                                    break;
                                }
                                break;
                            case 104069929:
                                if (name.equals(MODEL)) {
                                    c = 0;
                                    break;
                                }
                                break;
                        }
                        switch (c) {
                            case 0:
                                java.lang.String model = reader2.nextString();
                                if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(model)) {
                                    break;
                                } else {
                                    modelMatches = model.equalsIgnoreCase(modelFromSharedPref2);
                                    break;
                                }
                            case 1:
                                java.lang.String make = reader2.nextString();
                                if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(make)) {
                                    break;
                                } else {
                                    makeMatches = make.equalsIgnoreCase(makeFromSharedPref2);
                                    break;
                                }
                            case 2:
                                java.lang.String year = reader2.nextString();
                                if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(year)) {
                                    break;
                                } else {
                                    yearMatches = year.equalsIgnoreCase(yearFromSharedPref2);
                                    break;
                                }
                            default:
                                reader2.skipValue();
                                break;
                        }
                    }
                    reader2.endObject();
                    if (makeMatches && yearMatches && modelMatches) {
                        logger.d("car is blacklisted: " + makeFromSharedPref2 + org.droidparts.contract.SQL.DDL.SEPARATOR + modelFromSharedPref2 + org.droidparts.contract.SQL.DDL.SEPARATOR + yearFromSharedPref2);
                        com.navdy.service.library.util.IOUtils.closeStream(reader2);
                        return true;
                    }
                } catch (java.lang.Exception e) {
                    e = e;
                    reader = reader2;
                    try {
                        logger.e("Exception found: " + e);
                        com.navdy.service.library.util.IOUtils.closeStream(reader);
                        logger.d("car is not blacklisted: " + makeFromSharedPref2 + org.droidparts.contract.SQL.DDL.SEPARATOR + modelFromSharedPref2 + org.droidparts.contract.SQL.DDL.SEPARATOR + yearFromSharedPref2);
                        return false;
                    } catch (Throwable th) {
                        th = th;
                        com.navdy.service.library.util.IOUtils.closeStream(reader);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    reader = reader2;
                    com.navdy.service.library.util.IOUtils.closeStream(reader);
                    throw th;
                }
            }
            reader2.endArray();
            com.navdy.service.library.util.IOUtils.closeStream(reader2);
            android.util.JsonReader jsonReader = reader2;
        } catch (java.lang.Exception e2) {
            e = e2;
            logger.e("Exception found: " + e);
            com.navdy.service.library.util.IOUtils.closeStream(reader);
            logger.d("car is not blacklisted: " + makeFromSharedPref2 + org.droidparts.contract.SQL.DDL.SEPARATOR + modelFromSharedPref2 + org.droidparts.contract.SQL.DDL.SEPARATOR + yearFromSharedPref2);
            return false;
        }
        logger.d("car is not blacklisted: " + makeFromSharedPref2 + org.droidparts.contract.SQL.DDL.SEPARATOR + modelFromSharedPref2 + org.droidparts.contract.SQL.DDL.SEPARATOR + yearFromSharedPref2);
        return false;
    }

    @android.support.annotation.WorkerThread
    @android.support.annotation.NonNull
    public static com.navdy.client.app.framework.models.MountInfo getMountInfo() {
        android.content.SharedPreferences customerPrefs = getCustomerPreferences();
        java.lang.String yearFromSharedPref = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "");
        java.lang.String makeFromSharedPref = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "");
        java.lang.String modelFromSharedPref = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
        if (android.text.TextUtils.equals(lastYear, yearFromSharedPref) && android.text.TextUtils.equals(lastMake, makeFromSharedPref) && android.text.TextUtils.equals(lastModel, modelFromSharedPref)) {
            return lastMountInfo;
        }
        boolean modelMatches = false;
        boolean makeMatches = false;
        boolean yearMatches = false;
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(makeFromSharedPref) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(yearFromSharedPref) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(modelFromSharedPref)) {
            logger.d("No car model was found");
            lastMountInfo = new com.navdy.client.app.framework.models.MountInfo();
            return lastMountInfo;
        }
        java.lang.String makeFromSharedPref2 = makeFromSharedPref.trim();
        java.lang.String yearFromSharedPref2 = yearFromSharedPref.trim();
        java.lang.String modelFromSharedPref2 = modelFromSharedPref.trim();
        android.util.JsonReader reader = null;
        try {
            java.io.InputStreamReader inputStreamReader = new java.io.InputStreamReader(com.navdy.client.app.NavdyApplication.getAppContext().getResources().openRawResource(com.navdy.client.R.raw.mount_compatibility));
            android.util.JsonReader reader2 = new android.util.JsonReader(inputStreamReader);
            while (reader2.hasNext()) {
                try {
                    if (reader2.peek() == android.util.JsonToken.BEGIN_ARRAY) {
                        reader2.beginArray();
                    }
                    if (reader2.peek() == android.util.JsonToken.BEGIN_OBJECT) {
                        reader2.beginObject();
                    }
                    com.navdy.client.app.framework.models.MountInfo mountInfo = new com.navdy.client.app.framework.models.MountInfo();
                    while (reader2.hasNext()) {
                        java.lang.String name = reader2.nextName();
                        char c = 65535;
                        switch (name.hashCode()) {
                            case -1063986983:
                                if (name.equals(MEDIUM_SUPPORTED)) {
                                    c = 4;
                                    break;
                                }
                                break;
                            case -604532063:
                                if (name.equals(TALL_SUPPORTED)) {
                                    c = 5;
                                    break;
                                }
                                break;
                            case 3343854:
                                if (name.equals(MAKE)) {
                                    c = 1;
                                    break;
                                }
                                break;
                            case 3704893:
                                if (name.equals(YEAR)) {
                                    c = 2;
                                    break;
                                }
                                break;
                            case 104069929:
                                if (name.equals(MODEL)) {
                                    c = 0;
                                    break;
                                }
                                break;
                            case 1437916763:
                                if (name.equals("recommended")) {
                                    c = 6;
                                    break;
                                }
                                break;
                            case 1642540373:
                                if (name.equals(FLAT_SUPPORTED)) {
                                    c = 3;
                                    break;
                                }
                                break;
                        }
                        switch (c) {
                            case 0:
                                java.lang.String model = reader2.nextString();
                                if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(model)) {
                                    break;
                                } else {
                                    modelMatches = model.equalsIgnoreCase(modelFromSharedPref2);
                                    break;
                                }
                            case 1:
                                java.lang.String make = reader2.nextString();
                                if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(make)) {
                                    break;
                                } else {
                                    makeMatches = make.equalsIgnoreCase(makeFromSharedPref2);
                                    break;
                                }
                            case 2:
                                java.lang.String year = reader2.nextString();
                                if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(year)) {
                                    break;
                                } else {
                                    yearMatches = year.equalsIgnoreCase(yearFromSharedPref2);
                                    break;
                                }
                            case 3:
                                mountInfo.shortSupported = reader2.nextBoolean();
                                break;
                            case 4:
                                mountInfo.mediumSupported = reader2.nextBoolean();
                                break;
                            case 5:
                                mountInfo.tallSupported = reader2.nextBoolean();
                                break;
                            case 6:
                                mountInfo.recommendedMount = com.navdy.client.app.framework.models.MountInfo.MountType.getMountTypeForName(reader2.nextString());
                                break;
                            default:
                                reader2.skipValue();
                                break;
                        }
                    }
                    reader2.endObject();
                    if (makeMatches && yearMatches && modelMatches) {
                        lastMountInfo = mountInfo;
                        com.navdy.service.library.util.IOUtils.closeStream(reader2);
                        return mountInfo;
                    }
                } catch (java.lang.Exception e) {
                    e = e;
                    reader = reader2;
                    try {
                        logger.e("Exception found: " + e);
                        com.navdy.service.library.util.IOUtils.closeStream(reader);
                        logger.d("car is not blacklisted: " + makeFromSharedPref2 + org.droidparts.contract.SQL.DDL.SEPARATOR + modelFromSharedPref2 + org.droidparts.contract.SQL.DDL.SEPARATOR + yearFromSharedPref2);
                        lastMountInfo = new com.navdy.client.app.framework.models.MountInfo();
                        return lastMountInfo;
                    } catch (Throwable th) {
                        th = th;
                        com.navdy.service.library.util.IOUtils.closeStream(reader);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    reader = reader2;
                    com.navdy.service.library.util.IOUtils.closeStream(reader);
                    throw th;
                }
            }
            reader2.endArray();
            com.navdy.service.library.util.IOUtils.closeStream(reader2);
            android.util.JsonReader jsonReader = reader2;
        } catch (java.lang.Exception e2) {
            e = e2;
            logger.e("Exception found: " + e);
            com.navdy.service.library.util.IOUtils.closeStream(reader);
            logger.d("car is not blacklisted: " + makeFromSharedPref2 + org.droidparts.contract.SQL.DDL.SEPARATOR + modelFromSharedPref2 + org.droidparts.contract.SQL.DDL.SEPARATOR + yearFromSharedPref2);
            lastMountInfo = new com.navdy.client.app.framework.models.MountInfo();
            return lastMountInfo;
        }
        logger.d("car is not blacklisted: " + makeFromSharedPref2 + org.droidparts.contract.SQL.DDL.SEPARATOR + modelFromSharedPref2 + org.droidparts.contract.SQL.DDL.SEPARATOR + yearFromSharedPref2);
        lastMountInfo = new com.navdy.client.app.framework.models.MountInfo();
        return lastMountInfo;
    }

    public static boolean isUsingDefaultObdScanSetting() {
        com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting obdScanSetting = com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.valueOf(getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.HUD_OBD_ON, com.navdy.client.app.ui.settings.SettingsConstants.HUD_OBD_ON_DEFAULT));
        return obdScanSetting.equals(com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_OFF) || obdScanSetting.equals(com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_ON);
    }

    public static boolean obdScanIsOn() {
        return com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.valueOf(getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.HUD_OBD_ON, com.navdy.client.app.ui.settings.SettingsConstants.HUD_OBD_ON_DEFAULT)).equals(com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_ON);
    }

    public static void setDefaultObdSettingDependingOnBlacklistAsync() {
        logger.d("setDefaultObdSettingDependingOnBlacklistAsync");
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.settings.SettingsUtils.Anon5(), 1);
    }

    public static void checkObdSettingIfBuildVersionChanged() {
        android.content.SharedPreferences sharedPrefs = getSharedPreferences();
        if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(sharedPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.LAST_BUILD_VERSION, ""), "1.0")) {
            logger.d("build version has changed. Checking obd setting");
            if (isUsingDefaultObdScanSetting()) {
                setDefaultObdSettingDependingOnBlacklistAsync();
                return;
            }
            android.content.SharedPreferences.Editor editor = sharedPrefs.edit();
            boolean overrodeBlacklist = sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_BLACKLIST_OVERRIDDEN, false);
            logger.d("overrodeBlacklist: " + overrodeBlacklist);
            if (carIsBlacklistedForObdData() && !overrodeBlacklist) {
                editor.putString(com.navdy.client.app.ui.settings.SettingsConstants.HUD_OBD_ON, com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_OFF.toString());
            }
            editor.putString(com.navdy.client.app.ui.settings.SettingsConstants.LAST_BUILD_VERSION, "1.0");
            editor.apply();
        }
    }

    private static long getObdBlacklistLastModifiedDate() {
        java.lang.String modifiedDate = "0";
        java.io.InputStream inputStream = null;
        java.io.BufferedReader bufferedReader = null;
        try {
            inputStream = com.navdy.client.app.NavdyApplication.getAppContext().getResources().openRawResource(com.navdy.client.R.raw.modified);
            java.io.BufferedReader bufferedReader2 = new java.io.BufferedReader(new java.io.InputStreamReader(inputStream));
            try {
                java.lang.StringBuilder stringBuilder = new java.lang.StringBuilder();
                while (true) {
                    java.lang.String newLine = bufferedReader2.readLine();
                    if (newLine == null) {
                        break;
                    }
                    stringBuilder.append(newLine);
                }
                modifiedDate = stringBuilder.toString();
                com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                com.navdy.service.library.util.IOUtils.closeStream(bufferedReader2);
                java.io.BufferedReader bufferedReader3 = bufferedReader2;
            } catch (java.lang.Exception e) {
                e = e;
                bufferedReader = bufferedReader2;
                try {
                    logger.e("Exception found: " + e);
                    com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                    com.navdy.service.library.util.IOUtils.closeStream(bufferedReader);
                    logger.d("the last modified date: " + modifiedDate);
                    return java.lang.Long.parseLong(modifiedDate);
                } catch (Throwable th) {
                    th = th;
                    com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                    com.navdy.service.library.util.IOUtils.closeStream(bufferedReader);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedReader = bufferedReader2;
                com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                com.navdy.service.library.util.IOUtils.closeStream(bufferedReader);
                throw th;
            }
        } catch (java.lang.Exception e2) {
            e = e2;
            logger.e("Exception found: " + e);
            com.navdy.service.library.util.IOUtils.closeStream(inputStream);
            com.navdy.service.library.util.IOUtils.closeStream(bufferedReader);
            logger.d("the last modified date: " + modifiedDate);
            return java.lang.Long.parseLong(modifiedDate);
        }
        logger.d("the last modified date: " + modifiedDate);
        return java.lang.Long.parseLong(modifiedDate);
    }

    public static void checkLegacyCapabilityList(java.util.List<com.navdy.service.library.events.LegacyCapability> legacyCapabilityList) {
        if (legacyCapabilityList == null || legacyCapabilityList.isEmpty()) {
            logger.d("capability list is empty");
            return;
        }
        android.content.SharedPreferences sharedPrefs = getSharedPreferences();
        boolean compactCapable = false;
        boolean voiceSearchCapable = false;
        boolean musicCapable = false;
        for (com.navdy.service.library.events.LegacyCapability legacyCapability : legacyCapabilityList) {
            if (legacyCapability.equals(com.navdy.service.library.events.LegacyCapability.CAPABILITY_COMPACT_UI)) {
                compactCapable = true;
            } else if (legacyCapability.equals(com.navdy.service.library.events.LegacyCapability.CAPABILITY_VOICE_SEARCH)) {
                voiceSearchCapable = true;
            } else if (legacyCapability.equals(com.navdy.service.library.events.LegacyCapability.CAPABILITY_LOCAL_MUSIC_BROWSER)) {
                musicCapable = true;
            }
        }
        processCapabilities(sharedPrefs, compactCapable, voiceSearchCapable, musicCapable, false, false, false);
    }

    public static void checkCapabilities(com.navdy.service.library.events.Capabilities capabilities) {
        if (capabilities == null) {
            logger.d("device info has no capabilities.");
        } else {
            processCapabilities(getSharedPreferences(), java.lang.Boolean.TRUE.equals(capabilities.compactUi), java.lang.Boolean.TRUE.equals(capabilities.voiceSearch), java.lang.Boolean.TRUE.equals(capabilities.localMusicBrowser), java.lang.Boolean.TRUE.equals(capabilities.searchResultList), java.lang.Boolean.TRUE.equals(capabilities.cannedResponseToSms), java.lang.Boolean.TRUE.equals(capabilities.customDialLongPress));
        }
    }

    private static void processCapabilities(android.content.SharedPreferences sharedPrefs, boolean compactCapable, boolean voiceSearchCapable, boolean musicCapable, boolean searchResultListCapable, boolean cannedResponseToSms, boolean customDialLongPress) {
        sharedPrefs.edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_COMPACT_CAPABLE, compactCapable).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_VOICE_SEARCH_CAPABLE, voiceSearchCapable).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_LOCAL_MUSIC_BROWSER_CAPABLE, musicCapable).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_SEARCH_RESULT_LIST_CAPABLE, searchResultListCapable).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_CANNED_RESPONSE_CAPABLE, cannedResponseToSms).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_DIAL_LONG_PRESS_CAPABLE, customDialLongPress).apply();
        if (java.lang.Boolean.TRUE.equals(java.lang.Boolean.valueOf(cannedResponseToSms))) {
            getSharedPreferences().edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.MESSAGING_HAS_SEEN_CAPABLE_HUD, true).apply();
        }
    }
}
