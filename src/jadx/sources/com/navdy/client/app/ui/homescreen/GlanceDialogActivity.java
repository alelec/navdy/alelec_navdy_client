package com.navdy.client.app.ui.homescreen;

public class GlanceDialogActivity extends com.navdy.client.app.ui.base.BaseActivity {
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.dialog_glance_demo);
    }

    public void sendATestGlance(android.view.View v) {
        com.navdy.client.app.ui.glances.GlanceUtils.sendTestGlance(this, this.logger);
    }

    public void onCloseClick(android.view.View view) {
        onBackPressed();
    }
}
