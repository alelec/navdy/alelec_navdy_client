package com.navdy.client.app.ui.settings;

public class GestureDialogActivity extends com.navdy.client.app.ui.base.BaseActivity {
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.dialog_gestures);
    }

    protected void onResume() {
        super.onResume();
        android.widget.Button primaryButton = (android.widget.Button) findViewById(com.navdy.client.R.id.primary_button);
        android.widget.Button secondaryButton = (android.widget.Button) findViewById(com.navdy.client.R.id.secondary_button);
        if (com.navdy.client.app.framework.DeviceConnection.isConnected()) {
            if (primaryButton != null) {
                primaryButton.setText(com.navdy.client.R.string.try_gestures);
            }
            if (secondaryButton != null) {
                secondaryButton.setVisibility(View.VISIBLE);
                return;
            }
            return;
        }
        if (primaryButton != null) {
            primaryButton.setText(com.navdy.client.R.string.watch_video);
        }
        if (secondaryButton != null) {
            secondaryButton.setVisibility(View.GONE);
        }
    }

    public void onCloseClick(android.view.View view) {
        onBackPressed();
    }

    public void onPrimaryButtonClick(android.view.View view) {
        if (com.navdy.client.app.framework.DeviceConnection.isConnected()) {
            doTryGestureClick(view);
        } else {
            doWatchVideoClick(view);
        }
    }

    public void doWatchVideoClick(android.view.View view) {
        java.lang.String videoUrl = getString(com.navdy.client.R.string.gestures_video_url);
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.class);
        intent.putExtra(com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.EXTRA_VIDEO_URL, videoUrl);
        startActivity(intent);
    }

    public void doTryGestureClick(android.view.View view) {
        if (com.navdy.client.app.framework.DeviceConnection.isConnected()) {
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_GESTURE_LEARNING).build());
            showLongToast(com.navdy.client.R.string.please_look_at_the_hud, new java.lang.Object[0]);
            return;
        }
        showLongToast(com.navdy.client.R.string.navdy_not_connected, new java.lang.Object[0]);
    }
}
