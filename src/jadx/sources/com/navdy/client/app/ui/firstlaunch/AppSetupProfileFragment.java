package com.navdy.client.app.ui.firstlaunch;

public class AppSetupProfileFragment extends com.navdy.client.app.ui.base.BaseSupportFragment {
    private android.content.SharedPreferences customerPrefs;
    private android.widget.EditText emailInput;
    private android.widget.EditText nameInput;
    private android.view.View rootView;

    @android.support.annotation.Nullable
    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        this.rootView = inflater.inflate(com.navdy.client.R.layout.fle_app_setup_bottom_card_profile, null);
        this.customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
        this.nameInput = (android.widget.EditText) this.rootView.findViewById(com.navdy.client.R.id.enter_name);
        this.emailInput = (android.widget.EditText) this.rootView.findViewById(com.navdy.client.R.id.enter_email);
        android.support.v4.app.FragmentActivity activity = getActivity();
        if (activity instanceof com.navdy.client.app.ui.firstlaunch.AppSetupActivity) {
            ((com.navdy.client.app.ui.firstlaunch.AppSetupActivity) activity).doProfileOnCreate(this);
        }
        if (this.nameInput == null || this.emailInput == null) {
            return this.rootView;
        }
        return this.rootView;
    }

    public android.view.View getRootView() {
        return this.rootView;
    }

    public void onResume() {
        super.onResume();
        java.lang.String fullName = this.customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.FULL_NAME, "");
        java.lang.String email = this.customerPrefs.getString("email", "");
        if (this.nameInput != null) {
            this.nameInput.setText(fullName);
        }
        if (this.emailInput != null) {
            this.emailInput.setText(email);
        }
    }

    public java.lang.String getNameText() {
        if (this.nameInput != null) {
            return java.lang.String.valueOf(this.nameInput.getText());
        }
        return null;
    }
}
