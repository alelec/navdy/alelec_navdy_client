package com.navdy.client.app.ui.settings;

public final class HFPAudioDelaySettingsActivity$$InjectAdapter extends dagger.internal.Binding<com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity> implements javax.inject.Provider<com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity>, dagger.MembersInjector<com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity> {
    private dagger.internal.Binding<android.content.SharedPreferences> sharedPreferences;
    private dagger.internal.Binding<com.navdy.client.app.ui.base.BaseEditActivity> supertype;
    private dagger.internal.Binding<com.navdy.client.app.framework.util.TTSAudioRouter> ttsAudioRouter;

    public HFPAudioDelaySettingsActivity$$InjectAdapter() {
        super("com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity", "members/com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity", false, com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.ttsAudioRouter = linker.requestBinding("com.navdy.client.app.framework.util.TTSAudioRouter", com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity.class, getClass().getClassLoader());
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.client.app.ui.base.BaseEditActivity", com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.ttsAudioRouter);
        injectMembersBindings.add(this.sharedPreferences);
        injectMembersBindings.add(this.supertype);
    }

    public com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity get() {
        com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity result = new com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity object) {
        object.ttsAudioRouter = (com.navdy.client.app.framework.util.TTSAudioRouter) this.ttsAudioRouter.get();
        object.sharedPreferences = (android.content.SharedPreferences) this.sharedPreferences.get();
        this.supertype.injectMembers(object);
    }
}
