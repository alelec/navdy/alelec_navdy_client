package com.navdy.client.app.ui.homescreen;

public class InfoDialogActivity extends com.navdy.client.app.ui.base.BaseActivity {
    public static final java.lang.String EXTRA_LAYOUT = "layout";

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        android.content.Intent intent = getIntent();
        if (intent == null) {
            finish();
        } else {
            setContentView(intent.getIntExtra(EXTRA_LAYOUT, com.navdy.client.R.layout.dialog_voice_search));
        }
    }

    public void onButtonClick(android.view.View v) {
        finish();
    }

    public void onCloseClick(android.view.View view) {
        onBackPressed();
    }
}
