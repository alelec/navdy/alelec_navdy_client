package com.navdy.client.app.ui.homescreen;

public class SuggestionsViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
    public android.view.View background;
    public android.widget.TextView firstLine;
    public android.widget.ImageView icon;
    public android.widget.ImageButton infoButton;
    public android.widget.ImageView rightChevron;
    public android.view.View row;
    public android.widget.TextView secondLine;
    public android.view.View separator;
    public com.navdy.client.app.ui.customviews.UnitSystemTextView subIllustrationText;
    public android.widget.TextView thirdLine;

    @android.annotation.SuppressLint({"ResourceType"})
    public SuggestionsViewHolder(android.view.View itemView) {
        super(itemView);
        this.background = itemView.findViewById(com.navdy.client.R.id.background);
        this.row = itemView.findViewById(com.navdy.client.R.id.card_row);
        this.separator = itemView.findViewById(com.navdy.client.R.id.separator);
        this.icon = (android.widget.ImageView) itemView.findViewById(com.navdy.client.R.id.illustration);
        this.firstLine = (android.widget.TextView) itemView.findViewById(com.navdy.client.R.id.card_first_line);
        this.secondLine = (android.widget.TextView) itemView.findViewById(com.navdy.client.R.id.card_second_line);
        this.subIllustrationText = (com.navdy.client.app.ui.customviews.UnitSystemTextView) itemView.findViewById(com.navdy.client.R.id.card_sub_illustration_text);
        this.thirdLine = (android.widget.TextView) itemView.findViewById(com.navdy.client.R.id.card_third_line);
        this.rightChevron = (android.widget.ImageView) itemView.findViewById(com.navdy.client.R.id.right_chevron);
        this.infoButton = (android.widget.ImageButton) itemView.findViewById(com.navdy.client.R.id.nav_button);
        if (this.thirdLine != null) {
            this.thirdLine.setVisibility(View.GONE);
        }
    }
}
