package com.navdy.client.app.ui.glances;

public class AppNotificationsActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    private com.navdy.client.app.ui.glances.GlancesAppAdapter listAdaptor = null;
    private android.content.pm.PackageManager packageManager;
    private android.content.SharedPreferences sharedPrefs;

    private class LoadApplications extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {

        class Anon1 implements java.util.Comparator<android.content.pm.ApplicationInfo> {
            Anon1() {
            }

            public int compare(android.content.pm.ApplicationInfo lhs, android.content.pm.ApplicationInfo rhs) {
                if (lhs == rhs) {
                    return 0;
                }
                if (lhs == null) {
                    return -1;
                }
                if (rhs == null) {
                    return 1;
                }
                try {
                    if ("/system/priv-app/Keyguard.apk".equals(lhs.sourceDir) || "/system/priv-app/Keyguard.apk".equals(rhs.sourceDir)) {
                        return lhs.packageName.compareTo(rhs.packageName);
                    }
                    java.lang.CharSequence obj1 = lhs.loadLabel(com.navdy.client.app.ui.glances.AppNotificationsActivity.this.packageManager);
                    java.lang.CharSequence obj2 = rhs.loadLabel(com.navdy.client.app.ui.glances.AppNotificationsActivity.this.packageManager);
                    if (obj1 == obj2) {
                        return 0;
                    }
                    if (obj1 == null) {
                        return -1;
                    }
                    if (obj2 == null) {
                        return 1;
                    }
                    return obj1.toString().compareToIgnoreCase(obj2.toString());
                } catch (java.lang.Exception e) {
                    return lhs.packageName.compareTo(rhs.packageName);
                }
            }
        }

        private LoadApplications() {
        }

        /* access modifiers changed from: protected */
        public java.lang.Void doInBackground(java.lang.Void... params) {
            java.lang.String dialerPackage = com.navdy.client.app.ui.settings.SettingsUtils.getDialerPackage(com.navdy.client.app.ui.glances.AppNotificationsActivity.this.packageManager);
            com.navdy.client.app.ui.glances.AppNotificationsActivity.this.logger.d("GLANCES: The dialer package is: " + dialerPackage);
            if (!com.navdy.client.app.ui.glances.AppNotificationsActivity.this.sharedPrefs.contains(dialerPackage)) {
                com.navdy.client.app.ui.glances.GlanceUtils.saveGlancesConfigurationChanges(dialerPackage, true);
            }
            java.lang.String messengerPackage = com.navdy.client.app.ui.settings.SettingsUtils.getSmsPackage();
            com.navdy.client.app.ui.glances.AppNotificationsActivity.this.logger.d("GLANCES: The messenger package is: " + messengerPackage);
            if (!com.navdy.client.app.ui.glances.AppNotificationsActivity.this.sharedPrefs.contains(messengerPackage)) {
                com.navdy.client.app.ui.glances.GlanceUtils.saveGlancesConfigurationChanges(messengerPackage, true);
            }
            java.util.List<android.content.pm.ApplicationInfo> appList = com.navdy.client.app.ui.glances.AppNotificationsActivity.this.removeAppsThatWeDontWant(com.navdy.client.app.ui.glances.AppNotificationsActivity.this.packageManager.getInstalledApplications(128));
            try {
                java.util.Collections.sort(appList, new com.navdy.client.app.ui.glances.AppNotificationsActivity.LoadApplications.Anon1());
            } catch (java.lang.Exception e) {
                com.navdy.client.app.ui.glances.AppNotificationsActivity.this.logger.e("Unable to sort the list of apps.", e);
            }
            android.content.Context context = com.navdy.client.app.ui.glances.AppNotificationsActivity.this.getApplicationContext();
            if (context == null) {
                context = com.navdy.client.app.NavdyApplication.getAppContext();
            }
            if (context != null) {
                com.navdy.client.app.ui.glances.AppNotificationsActivity.this.listAdaptor = new com.navdy.client.app.ui.glances.GlancesAppAdapter(context, com.navdy.client.R.layout.app_list_row, appList);
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(java.lang.Void result) {
            android.widget.ListView listView = (android.widget.ListView) com.navdy.client.app.ui.glances.AppNotificationsActivity.this.findViewById(com.navdy.client.R.id.app_list);
            if (listView != null) {
                listView.setAdapter(com.navdy.client.app.ui.glances.AppNotificationsActivity.this.listAdaptor);
            }
            super.onPostExecute(result);
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.app_notifications);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.notification_glances).build();
        this.packageManager = getPackageManager();
        this.sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        android.widget.ListView appList = (android.widget.ListView) findViewById(com.navdy.client.R.id.app_list);
        if (appList != null) {
            appList.setEmptyView(findViewById(com.navdy.client.R.id.app_list_empty_view));
        }
    }

    public void onResume() {
        super.onResume();
        new com.navdy.client.app.ui.glances.AppNotificationsActivity.LoadApplications().execute(new java.lang.Void[0]);
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.NOTIFICATION_GLANCES);
    }

    private java.util.List<android.content.pm.ApplicationInfo> removeAppsThatWeDontWant(java.util.List<android.content.pm.ApplicationInfo> list) {
        java.util.ArrayList<android.content.pm.ApplicationInfo> appList = new java.util.ArrayList<>();
        for (android.content.pm.ApplicationInfo info : list) {
            try {
                if (this.packageManager.getLaunchIntentForPackage(info.packageName) != null && !com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(getPackageName(), info.packageName) && !com.navdy.client.app.ui.glances.GlanceUtils.isWhiteListedApp(info.packageName)) {
                    appList.add(info);
                }
            } catch (java.lang.Exception e) {
                e.printStackTrace();
            }
        }
        return appList;
    }
}
