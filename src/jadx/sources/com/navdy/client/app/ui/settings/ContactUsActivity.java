package com.navdy.client.app.ui.settings;

public class ContactUsActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity implements com.navdy.client.app.framework.util.SupportTicketService.OnLogCollectedInterface {
    private static final int DELAY_BEFORE_FINISHING_ACTIVITY = 3000;
    private static final int DIALOG_PHOTO_LIMIT_EXCEEDED_ID = 1;
    private static final int DIALOG_PICK_FROM_GALLERY_OPTION = 0;
    private static final int DIALOG_TAKE_PHOTO_OPTION = 1;
    public static final java.lang.String DISPLAY_LOG_CONTAIN_FILTER = "display_log";
    public static final java.lang.String EXTRA_DEFAULT_PROBLEM_TYPE = "default_problem_type";
    private static final int FILE_SIZE_LIMIT = 2097152;
    private static final int GALLERY_IMAGE_SIZE = 32;
    private static final int MAX_PIXEL_DIMENSION = 1280;
    private static final int PHOTO_ATTACHMENT_LIMIT = 5;
    private static final int STORAGE_PERMISSIONS_REQUEST_CODE = 3;
    public static final java.lang.String TEMP_PHOTO_FILENAME = "temp_photo_filename";
    private android.widget.TextView attachPhotoTextView;
    private java.util.ArrayList<java.io.File> attachments = new java.util.ArrayList<>();
    private java.lang.String currentTicketId = "";
    private java.util.ArrayList<java.lang.ref.WeakReference<android.app.Dialog>> dialogs = new java.util.ArrayList<>();
    private java.lang.Runnable dismissDialogsAndFinishRunnable = new com.navdy.client.app.ui.settings.ContactUsActivity.Anon2();
    private boolean displayLogAttached;
    private java.lang.String email;
    private android.widget.EditText emailAddress;
    private java.lang.Runnable failedToGetStorageRunnable = new com.navdy.client.app.ui.settings.ContactUsActivity.Anon1();
    private boolean folderHasBeenCreatedForThisTicket;
    private java.util.Iterator<android.widget.ImageView> imageViewIterator;
    private android.support.v7.widget.Toolbar myToolbar;
    private int photoAttachmentCount = 0;
    private android.widget.EditText problemInput;
    private com.navdy.client.app.ui.customviews.CustomSpinner problemTypes;
    private java.lang.String tempPhotoFilename;
    private boolean userWantsLogs;
    private java.lang.String vin;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.app.ui.settings.ContactUsActivity.this.logger.v("failed to get storage permission for logs");
            com.navdy.client.app.ui.settings.ContactUsActivity.this.showGetStoragePermissionDialog();
        }
    }

    class Anon10 implements android.content.DialogInterface.OnClickListener {
        Anon10() {
        }

        public void onClick(android.content.DialogInterface dialogInterface, int i) {
            android.content.Intent intent = new android.content.Intent();
            intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(android.net.Uri.parse("package:" + com.navdy.client.app.ui.settings.ContactUsActivity.this.getPackageName()));
            com.navdy.client.app.ui.settings.ContactUsActivity.this.startActivityForResult(intent, 3);
        }
    }

    class Anon11 implements android.content.DialogInterface.OnClickListener {
        Anon11() {
        }

        public void onClick(android.content.DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    class Anon12 implements android.content.DialogInterface.OnClickListener {
        Anon12() {
        }

        public void onClick(android.content.DialogInterface dialogInterface, int i) {
            switch (i) {
                case 0:
                    com.navdy.client.app.ui.PhotoUtils.getPhotoFromGallery(com.navdy.client.app.ui.settings.ContactUsActivity.this);
                    return;
                case 1:
                    com.navdy.client.app.ui.settings.ContactUsActivity.this.tempPhotoFilename = com.navdy.service.library.util.IOUtils.getTempFilename();
                    com.navdy.client.app.ui.PhotoUtils.takePhoto(com.navdy.client.app.ui.settings.ContactUsActivity.this, com.navdy.client.app.ui.settings.ContactUsActivity.this.logger, com.navdy.client.app.ui.settings.ContactUsActivity.this.tempPhotoFilename);
                    return;
                default:
                    return;
            }
        }
    }

    class Anon13 implements java.lang.Runnable {
        final /* synthetic */ android.content.Intent val$responseIntent;

        Anon13(android.content.Intent intent) {
            this.val$responseIntent = intent;
        }

        public void run() {
            java.io.File tempFile = null;
            if (this.val$responseIntent != null) {
                android.os.Bundle bundle = this.val$responseIntent.getExtras();
                if (bundle != null) {
                    android.graphics.Bitmap image = (android.graphics.Bitmap) bundle.get(com.navdy.client.app.framework.util.CarMdClient.DATA);
                    com.navdy.client.app.ui.settings.ContactUsActivity.this.logger.d("image status: " + image);
                    if (image != null) {
                        tempFile = new java.io.File(android.os.Environment.getExternalStorageDirectory(), com.navdy.client.app.ui.settings.ContactUsActivity.this.tempPhotoFilename);
                        com.navdy.client.app.ui.settings.ContactUsActivity.this.compressBitmapToFile(image, tempFile, android.graphics.Bitmap.CompressFormat.JPEG, 85);
                    }
                }
            }
            if (tempFile == null && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(com.navdy.client.app.ui.settings.ContactUsActivity.this.tempPhotoFilename)) {
                com.navdy.client.app.ui.settings.ContactUsActivity.this.logger.d("retrieving photo from storage");
                tempFile = com.navdy.service.library.util.IOUtils.getTempFile(com.navdy.client.app.ui.settings.ContactUsActivity.this.logger, com.navdy.client.app.ui.settings.ContactUsActivity.this.tempPhotoFilename);
            }
            if (tempFile != null && tempFile.exists()) {
                android.graphics.Bitmap bitmap = com.navdy.client.app.framework.util.ImageUtils.getScaledBitmap(tempFile, (int) com.navdy.client.app.ui.settings.ContactUsActivity.MAX_PIXEL_DIMENSION, (int) com.navdy.client.app.ui.settings.ContactUsActivity.MAX_PIXEL_DIMENSION);
                if (bitmap == null) {
                    com.navdy.client.app.ui.settings.ContactUsActivity.this.logger.e("bitmap was null");
                    com.navdy.client.app.ui.settings.ContactUsActivity.this.showFailureSnackBar();
                    return;
                }
                if (tempFile.exists() && tempFile.length() > 2097152) {
                    com.navdy.client.app.ui.settings.ContactUsActivity.this.compressBitmapToFile(bitmap, tempFile, android.graphics.Bitmap.CompressFormat.JPEG, 85);
                }
                java.io.File photoFile = com.navdy.service.library.util.IOUtils.getTempFile(com.navdy.client.app.ui.settings.ContactUsActivity.this.logger, com.navdy.client.app.ui.settings.ContactUsActivity.this.tempPhotoFilename);
                if (photoFile != null) {
                    com.navdy.client.app.ui.settings.ContactUsActivity.this.attachments.add(photoFile);
                    com.navdy.client.app.ui.settings.ContactUsActivity.this.showSuccessSnackBar();
                    com.navdy.client.app.ui.settings.ContactUsActivity.this.incrementPhotoAttachments();
                    com.navdy.client.app.ui.settings.ContactUsActivity.this.attachPhotoTextView.setText(com.navdy.client.R.string.attach_another_photo);
                    if (com.navdy.client.app.ui.settings.ContactUsActivity.this.imageViewIterator != null && com.navdy.client.app.ui.settings.ContactUsActivity.this.imageViewIterator.hasNext()) {
                        com.navdy.client.app.ui.settings.ContactUsActivity.this.addPhotoToGallery(bitmap);
                    }
                } else {
                    com.navdy.client.app.ui.settings.ContactUsActivity.this.showFailureSnackBar();
                }
                bitmap.recycle();
            }
        }
    }

    class Anon14 implements java.lang.Runnable {
        Anon14() {
        }

        public void run() {
            com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.storage_permission_not_granted, new java.lang.Object[0]);
        }
    }

    class Anon15 implements com.navdy.client.app.framework.util.ImageUtils.StreamFactory {
        final /* synthetic */ android.net.Uri val$uri;

        Anon15(android.net.Uri uri) {
            this.val$uri = uri;
        }

        public java.io.InputStream getInputStream() throws java.io.FileNotFoundException {
            return com.navdy.client.app.ui.settings.ContactUsActivity.this.getContentResolver().openInputStream(this.val$uri);
        }
    }

    class Anon16 implements android.view.View.OnClickListener {
        Anon16() {
        }

        public void onClick(android.view.View view) {
            com.navdy.client.app.ui.settings.ContactUsActivity.this.showAttachPhotoDialog();
        }
    }

    class Anon17 implements java.lang.Runnable {
        Anon17() {
        }

        public void run() {
            com.navdy.client.app.ui.settings.ContactUsActivity.this.startSubmitTicketService();
        }
    }

    class Anon18 implements java.lang.Runnable {
        Anon18() {
        }

        public void run() {
            com.navdy.client.app.ui.settings.ContactUsActivity.this.startSubmitTicketService();
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.client.app.ui.settings.ContactUsActivity.this.dismissAllDialogs();
            com.navdy.client.app.ui.settings.ContactUsActivity.this.finish();
        }
    }

    class Anon3 implements com.navdy.client.app.ui.customviews.CustomSpinner.OnSpinnerStateChangedListener {
        final /* synthetic */ android.widget.ArrayAdapter val$introAdapter;
        final /* synthetic */ android.widget.ArrayAdapter val$problemAdapter;

        Anon3(android.widget.ArrayAdapter arrayAdapter, android.widget.ArrayAdapter arrayAdapter2) {
            this.val$problemAdapter = arrayAdapter;
            this.val$introAdapter = arrayAdapter2;
        }

        public void spinnerOpened() {
            com.navdy.client.app.ui.settings.ContactUsActivity.this.problemTypes.setAdapter(this.val$problemAdapter);
        }

        public void spinnerClosed() {
            com.navdy.client.app.ui.settings.ContactUsActivity.this.logger.v("getSelectedItemPosition: " + com.navdy.client.app.ui.settings.ContactUsActivity.this.problemTypes.getSelectedItemPosition());
            com.navdy.client.app.ui.settings.ContactUsActivity.this.updateSubmitButton();
            if (com.navdy.client.app.ui.settings.ContactUsActivity.this.problemTypes.getSelectedItemPosition() == 0) {
                com.navdy.client.app.ui.settings.ContactUsActivity.this.problemTypes.setAdapter(this.val$introAdapter);
            }
            if (com.navdy.client.app.ui.settings.ContactUsActivity.this.problemTypes.getSelectedItemPosition() == 9) {
                com.navdy.client.app.ui.settings.ContactUsActivity.this.problemInput.setHint(com.navdy.client.R.string.settings_report_a_problem_log_attachment_hint);
            } else {
                com.navdy.client.app.ui.settings.ContactUsActivity.this.problemInput.setHint(com.navdy.client.R.string.settings_report_a_problem_hint);
            }
        }
    }

    class Anon4 implements android.text.TextWatcher {
        Anon4() {
        }

        public void beforeTextChanged(java.lang.CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(java.lang.CharSequence s, int start, int before, int count) {
        }

        public void afterTextChanged(android.text.Editable s) {
            com.navdy.client.app.ui.settings.ContactUsActivity.this.email = com.navdy.client.app.ui.settings.ContactUsActivity.this.emailAddress.getText().toString();
            com.navdy.client.app.ui.settings.ContactUsActivity.this.updateSubmitButton();
        }
    }

    class Anon5 implements android.text.TextWatcher {
        Anon5() {
        }

        public void beforeTextChanged(java.lang.CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(java.lang.CharSequence s, int start, int before, int count) {
        }

        public void afterTextChanged(android.text.Editable s) {
            com.navdy.client.app.ui.settings.ContactUsActivity.this.updateSubmitButton();
        }
    }

    class Anon6 implements android.view.MenuItem.OnMenuItemClickListener {
        Anon6() {
        }

        public boolean onMenuItemClick(android.view.MenuItem menuItem) {
            com.navdy.client.app.ui.settings.ContactUsActivity.this.showMoreDetailsRequiredDialog();
            return true;
        }
    }

    class Anon7 implements java.lang.Runnable {
        Anon7() {
        }

        public void run() {
            com.navdy.client.app.ui.settings.ContactUsActivity.this.showProgressDialog();
            com.navdy.client.app.framework.util.SupportTicketService.collectLogs(com.navdy.client.app.ui.settings.ContactUsActivity.this);
        }
    }

    class Anon8 implements java.lang.Runnable {
        Anon8() {
        }

        public void run() {
            com.navdy.client.app.ui.settings.ContactUsActivity.this.showSingleChoiceDialogWithMessage();
        }
    }

    class Anon9 implements android.content.DialogInterface.OnDismissListener {
        Anon9() {
        }

        public void onDismiss(android.content.DialogInterface dialogInterface) {
            com.navdy.client.app.ui.settings.ContactUsActivity.this.finish();
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.settings_contact_us);
        this.myToolbar = (android.support.v7.widget.Toolbar) findViewById(com.navdy.client.R.id.my_toolbar);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.contact_us).build();
        this.emailAddress = (android.widget.EditText) findViewById(com.navdy.client.R.id.enter_email_address);
        this.problemTypes = (com.navdy.client.app.ui.customviews.CustomSpinner) findViewById(com.navdy.client.R.id.problem_types);
        this.problemInput = (android.widget.EditText) findViewById(com.navdy.client.R.id.problem_input);
        this.attachPhotoTextView = (android.widget.TextView) findViewById(com.navdy.client.R.id.attach_image);
        setupGallery();
        if (this.problemTypes != null) {
            android.widget.ArrayAdapter<java.lang.CharSequence> introAdapter = android.widget.ArrayAdapter.createFromResource(this, com.navdy.client.R.array.settings_report_a_problem_select_a_topic, 17367048);
            introAdapter.setDropDownViewResource(17367049);
            android.widget.ArrayAdapter<java.lang.CharSequence> problemAdapter = android.widget.ArrayAdapter.createFromResource(this, com.navdy.client.R.array.settings_report_a_problem_problem_types, 17367048);
            problemAdapter.setDropDownViewResource(17367049);
            this.problemTypes.setAdapter(introAdapter);
            this.problemTypes.setListener(new com.navdy.client.app.ui.settings.ContactUsActivity.Anon3(problemAdapter, introAdapter));
            android.content.Intent intent = getIntent();
            if (intent != null) {
                int defaultProblemType = intent.getIntExtra(EXTRA_DEFAULT_PROBLEM_TYPE, -1);
                if (defaultProblemType > 0) {
                    this.problemTypes.setAdapter(problemAdapter);
                    this.problemTypes.setSelection(defaultProblemType);
                }
            }
        }
        android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
        this.userWantsLogs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUTO_ATTACH_LOGS_ENABLED, false);
        if (this.emailAddress != null) {
            this.email = com.navdy.client.app.framework.util.SupportTicketService.getEmail();
            if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.email)) {
                this.email = customerPrefs.getString("email", "");
            }
            this.emailAddress.setText(this.email);
            this.emailAddress.setSelection(this.email.length());
            if (com.zendesk.sdk.network.impl.ZendeskConfig.INSTANCE.isInitialized()) {
                this.emailAddress.setEnabled(false);
            } else {
                this.emailAddress.addTextChangedListener(new com.navdy.client.app.ui.settings.ContactUsActivity.Anon4());
            }
        }
        this.problemInput.addTextChangedListener(new com.navdy.client.app.ui.settings.ContactUsActivity.Anon5());
        new com.navdy.client.app.ui.settings.SettingsUtils.SettingsLogger().logConfiguration();
    }

    private void updateSubmitButton() {
        if (!emailIsValid() || !messageIsValid() || !problemTypeIsValid()) {
            disableSubmitButton();
        } else {
            showSubmitButton();
        }
    }

    private boolean emailIsValid() {
        return this.emailAddress != null && com.navdy.client.app.framework.util.StringUtils.isValidEmail(this.emailAddress.getText());
    }

    private boolean messageIsValid() {
        return this.problemInput != null && this.problemInput.getText().length() > 0;
    }

    private boolean problemTypeIsValid() {
        return this.problemTypes != null && this.problemTypes.getSelectedItemPosition() > 0;
    }

    public boolean onPrepareOptionsMenu(android.view.Menu menu) {
        addSubmitButtonToMenu(menu);
        disableSubmitButton();
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        if (item.getItemId() == com.navdy.client.R.id.btn_submit) {
            createTicket();
        }
        return super.onOptionsItemSelected(item);
    }

    private void addSubmitButtonToMenu(android.view.Menu menu) {
        menu.add(0, com.navdy.client.R.id.btn_submit, 0, com.navdy.client.R.string.settings_report_a_problem_submit).setShowAsAction(2);
    }

    private void hideSubmitButton() {
        if (this.myToolbar != null) {
            android.view.Menu menu = this.myToolbar.getMenu();
            if (menu != null) {
                menu.removeItem(com.navdy.client.R.id.btn_submit);
            }
        }
    }

    private void showSubmitButton() {
        if (this.myToolbar != null) {
            android.view.Menu menu = this.myToolbar.getMenu();
            if (menu != null) {
                menu.removeItem(com.navdy.client.R.id.btn_submit);
                addSubmitButtonToMenu(menu);
            }
        }
    }

    private void disableSubmitButton() {
        if (this.myToolbar != null) {
            android.view.Menu menu = this.myToolbar.getMenu();
            if (menu != null) {
                android.view.MenuItem sendButton = menu.findItem(com.navdy.client.R.id.btn_submit);
                if (sendButton != null) {
                    sendButton.setOnMenuItemClickListener(new com.navdy.client.app.ui.settings.ContactUsActivity.Anon6());
                }
            }
        }
    }

    protected void onResume() {
        super.onResume();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.Settings.CONTACT);
    }

    protected void onSaveInstanceState(android.os.Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.tempPhotoFilename)) {
            savedInstanceState.putString(TEMP_PHOTO_FILENAME, this.tempPhotoFilename);
        }
    }

    protected void onRestoreInstanceState(android.os.Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.tempPhotoFilename)) {
            this.tempPhotoFilename = savedInstanceState.getString(TEMP_PHOTO_FILENAME, this.tempPhotoFilename);
        }
    }

    @com.squareup.otto.Subscribe
    public void onObdStatusResponse(com.navdy.service.library.events.obd.ObdStatusResponse obdStatusResponse) {
        this.vin = obdStatusResponse.vin != null ? obdStatusResponse.vin : getResources().getString(com.navdy.client.R.string.unknown);
    }

    private void createTicket() {
        this.logger.v("create a Request Ticket");
        if (findViewById(com.navdy.client.R.id.master_layout) == null || (emailIsValid() && problemTypeIsValid() && messageIsValid())) {
            com.navdy.client.app.framework.util.SystemUtils.dismissKeyboard(this);
            if (this.userWantsLogs) {
                requestStoragePermission(new com.navdy.client.app.ui.settings.ContactUsActivity.Anon7(), this.failedToGetStorageRunnable);
            } else {
                requestStoragePermission(new com.navdy.client.app.ui.settings.ContactUsActivity.Anon8(), this.failedToGetStorageRunnable);
            }
        } else if (!isFinishing() && !isDestroyed()) {
            showMoreDetailsRequiredDialog();
        }
    }

    @android.support.annotation.MainThread
    private void showSuccessfulTicketDialog() {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        if (!com.navdy.client.app.ui.base.BaseActivity.isEnding(this)) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setTitle(com.navdy.client.R.string.request_sent);
            builder.setMessage(com.navdy.client.R.string.request_sent_to_navdy);
            builder.setPositiveButton(com.navdy.client.R.string.got_it, null);
            builder.setOnDismissListener(new com.navdy.client.app.ui.settings.ContactUsActivity.Anon9());
            android.app.AlertDialog dialog = builder.create();
            this.dialogs.add(new java.lang.ref.WeakReference(dialog));
            dialog.show();
        }
    }

    public void showSingleChoiceDialogWithMessage() {
        android.view.View dialogView = getLayoutInflater().inflate(com.navdy.client.R.layout.dialog_for_attaching_logs, null);
        android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(this).setView(dialogView).create();
        ((android.widget.TextView) dialogView.findViewById(com.navdy.client.R.id.dialog_detail)).setText(com.navdy.client.app.framework.util.StringUtils.fromHtml((int) com.navdy.client.R.string.attach_logs_desc));
        this.dialogs.add(new java.lang.ref.WeakReference(dialog));
        dialog.show();
    }

    public void onPrivacyPolicyClick(android.view.View view) {
        android.content.Intent browserIntent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.WebViewActivity.class);
        browserIntent.putExtra("type", com.navdy.client.app.ui.WebViewActivity.PRIVACY);
        startActivity(browserIntent);
    }

    public void onOnceClick(android.view.View view) {
        dismissAllDialogs();
        this.userWantsLogs = true;
        showProgressDialog();
        com.navdy.client.app.framework.util.SupportTicketService.collectLogs(this);
    }

    public void onNoLogsClick(android.view.View view) {
        dismissAllDialogs();
        startSubmitTicketService();
    }

    public void onAlwaysClick(android.view.View view) {
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUTO_ATTACH_LOGS_ENABLED, true).apply();
        onOnceClick(view);
    }

    /* access modifiers changed from: private */
    @android.support.annotation.MainThread
    public void showGetStoragePermissionDialog() {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        if (!com.navdy.client.app.ui.base.BaseActivity.isEnding(this)) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle(com.navdy.client.R.string.we_need_storage_permission_for_tickets);
            builder.setMessage(com.navdy.client.R.string.how_to_enable_storage);
            builder.setPositiveButton(com.navdy.client.R.string.go_to_permission_settings, new com.navdy.client.app.ui.settings.ContactUsActivity.Anon10());
            builder.setNegativeButton(com.navdy.client.R.string.cancel_request, new com.navdy.client.app.ui.settings.ContactUsActivity.Anon11());
            android.app.AlertDialog dialog = builder.create();
            this.dialogs.add(new java.lang.ref.WeakReference(dialog));
            dialog.show();
        }
    }

    /* access modifiers changed from: private */
    @android.support.annotation.MainThread
    public void showAttachPhotoDialog() {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        if (!com.navdy.client.app.ui.base.BaseActivity.isEnding(this)) {
            if (this.photoAttachmentCount >= 5) {
                showSimpleDialog(1, getString(com.navdy.client.R.string.max_photos_exceeded), getString(com.navdy.client.R.string.max_photos_exceeded_explanation));
                return;
            }
            java.lang.CharSequence[] options = getResources().getStringArray(com.navdy.client.R.array.settings_contact_us_photo_options);
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle(com.navdy.client.R.string.attach_image_title);
            builder.setItems(options, new com.navdy.client.app.ui.settings.ContactUsActivity.Anon12());
            android.app.AlertDialog dialog = builder.create();
            this.dialogs.add(new java.lang.ref.WeakReference(dialog));
            dialog.show();
        }
    }

    /* access modifiers changed from: private */
    @android.support.annotation.MainThread
    public void showMoreDetailsRequiredDialog() {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        if (!com.navdy.client.app.ui.base.BaseActivity.isEnding(this)) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setTitle(com.navdy.client.R.string.more_details_required);
            java.lang.StringBuilder problemsString = new java.lang.StringBuilder(getString(com.navdy.client.R.string.we_need_a_bit_more));
            if (!emailIsValid()) {
                problemsString.append(getString(com.navdy.client.R.string.enter_an_accurate_email_bulletpoint));
            }
            if (!problemTypeIsValid()) {
                problemsString.append(getString(com.navdy.client.R.string.select_a_topic_bulletpoint));
            }
            if (!messageIsValid()) {
                problemsString.append(getString(com.navdy.client.R.string.describe_the_problem_bulletpoint));
            }
            builder.setMessage(problemsString);
            builder.setPositiveButton(com.navdy.client.R.string.ok, null);
            android.app.AlertDialog dialog = builder.create();
            this.dialogs.add(new java.lang.ref.WeakReference(dialog));
            dialog.show();
        }
    }

    public void attachPhotoToZendesk(android.view.View view) {
        showAttachPhotoDialog();
    }

    public void onActivityResult(int requestCode, int resultCode, android.content.Intent responseIntent) {
        if (resultCode == -1) {
            if (requestCode == 1) {
                requestStoragePermission(new com.navdy.client.app.ui.settings.ContactUsActivity.Anon13(responseIntent), new com.navdy.client.app.ui.settings.ContactUsActivity.Anon14());
            } else if (requestCode == 2 && responseIntent != null && responseIntent.getData() != null) {
                try {
                    android.net.Uri uri = responseIntent.getData();
                    if (uri != null) {
                        android.graphics.Bitmap bitmap = com.navdy.client.app.framework.util.ImageUtils.getScaledBitmap((com.navdy.client.app.framework.util.ImageUtils.StreamFactory) new com.navdy.client.app.ui.settings.ContactUsActivity.Anon15(uri), (int) MAX_PIXEL_DIMENSION, (int) MAX_PIXEL_DIMENSION);
                        if (bitmap != null) {
                            java.lang.String fileName = com.navdy.service.library.util.IOUtils.getTempFilename();
                            com.navdy.client.app.tracking.Tracker.saveBitmapToInternalStorage(bitmap, fileName);
                            java.io.File galleryPhoto = new java.io.File(com.navdy.client.app.NavdyApplication.getAppContext().getFilesDir() + java.io.File.separator, fileName);
                            if (galleryPhoto.exists()) {
                                this.attachments.add(galleryPhoto);
                                showSuccessSnackBar();
                                incrementPhotoAttachments();
                                this.attachPhotoTextView.setText(com.navdy.client.R.string.attach_another_photo);
                                if (this.imageViewIterator != null && this.imageViewIterator.hasNext()) {
                                    addPhotoToGallery(bitmap);
                                }
                            } else {
                                this.logger.v("photo file is null");
                                showFailureSnackBar();
                            }
                            bitmap.recycle();
                        }
                    }
                } catch (java.lang.Exception e) {
                    this.logger.e("failed to get photo gallery image: " + e);
                    showFailureSnackBar();
                }
            } else if (requestCode == 3) {
                createTicket();
            }
        }
        super.onActivityResult(requestCode, resultCode, responseIntent);
    }

    private void compressBitmapToFile(android.graphics.Bitmap bitmap, java.io.File outputFile, android.graphics.Bitmap.CompressFormat format, int quality) {
        java.io.BufferedOutputStream outputStream = null;
        try {
            java.io.BufferedOutputStream outputStream2 = new java.io.BufferedOutputStream(new java.io.FileOutputStream(outputFile));
            try {
                bitmap.compress(format, quality, outputStream2);
                com.navdy.service.library.util.IOUtils.closeStream(outputStream2);
                java.io.BufferedOutputStream bufferedOutputStream = outputStream2;
            } catch (java.lang.Exception e) {
                e = e;
                outputStream = outputStream2;
                try {
                    this.logger.e("Exception compressing bitmap: " + e);
                    com.navdy.service.library.util.IOUtils.closeStream(outputStream);
                } catch (Throwable th) {
                    th = th;
                    com.navdy.service.library.util.IOUtils.closeStream(outputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                outputStream = outputStream2;
                com.navdy.service.library.util.IOUtils.closeStream(outputStream);
                throw th;
            }
        } catch (java.lang.Exception e2) {
            e = e2;
            this.logger.e("Exception compressing bitmap: " + e);
            com.navdy.service.library.util.IOUtils.closeStream(outputStream);
        }
    }

    private void showFailureSnackBar() {
        if (!isFinishing()) {
            hideProgressDialog();
            android.view.View view = findViewById(com.navdy.client.R.id.master_layout);
            if (view != null) {
                android.support.design.widget.Snackbar.make(view, (int) com.navdy.client.R.string.photo_upload_failed, 0).setAction((int) com.navdy.client.R.string.retry_button, (android.view.View.OnClickListener) new com.navdy.client.app.ui.settings.ContactUsActivity.Anon16()).show();
            }
        }
    }

    private void showSuccessSnackBar() {
        if (!isFinishing()) {
            hideProgressDialog();
            android.view.View view = findViewById(com.navdy.client.R.id.master_layout);
            if (view != null) {
                android.support.design.widget.Snackbar.make(view, (int) com.navdy.client.R.string.photo_upload_succeeded, 0).show();
            }
        }
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (this.problemTypes != null && this.problemTypes.hasBeenOpened() && hasFocus) {
            this.problemTypes.spinnerClosed();
        }
    }

    private org.json.JSONObject createJSONFromTicketContents(java.lang.String ticketDescription, java.lang.String vin2) {
        try {
            org.json.JSONObject jsonObject = new org.json.JSONObject();
            jsonObject.put(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_TYPE, getTagFromProblemType());
            jsonObject.put(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_DESCRIPTION, ticketDescription);
            jsonObject.put(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_VIN, vin2);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.emailAddress.getText())) {
                jsonObject.put(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_EMAIL, this.emailAddress.getText().toString());
            }
            jsonObject.put(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_HAS_DISPLAY_LOG, this.displayLogAttached);
            if (!this.userWantsLogs || this.displayLogAttached) {
                jsonObject.put(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_ACTION, com.navdy.client.app.ui.settings.ZendeskConstants.ACTION_UPLOAD_DELETE);
                return jsonObject;
            }
            jsonObject.put(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_ACTION, com.navdy.client.app.ui.settings.ZendeskConstants.ACTION_UPLOAD_AWAIT_HUD_LOG);
            return jsonObject;
        } catch (Throwable e) {
            this.logger.e("Failed to create JSON from Ticket Contents: " + e);
            return null;
        }
    }

    private java.lang.String getTagFromProblemType() {
        return getResources().getStringArray(com.navdy.client.R.array.settings_report_a_problem_problem_types_tags)[this.problemTypes.getSelectedItemPosition()];
    }

    private void createReadMeIfDoesNotExist() {
        java.io.File readMe = new java.io.File(com.navdy.client.app.framework.PathManager.getInstance().getTicketFolderPath() + java.io.File.separator + com.navdy.client.app.framework.util.SupportTicketService.TICKET_FOLDER_README_FILENAME);
        if (!readMe.exists()) {
            try {
                java.io.BufferedWriter bufferedWriter = new java.io.BufferedWriter(new java.io.FileWriter(readMe, true));
                bufferedWriter.append(getString(com.navdy.client.R.string.zendesk_ticket_readme_message));
                bufferedWriter.newLine();
                bufferedWriter.close();
            } catch (java.lang.Exception e) {
                this.logger.e("There was an exception creating the ReadMe file: " + e);
            }
        }
    }

    private void setupGallery() {
        java.util.ArrayList<android.widget.ImageView> imageViewArrayList = new java.util.ArrayList<>();
        imageViewArrayList.add((android.widget.ImageView) findViewById(com.navdy.client.R.id.attach_photo_icon));
        imageViewArrayList.add((android.widget.ImageView) findViewById(com.navdy.client.R.id.attach_photo_icon_2));
        imageViewArrayList.add((android.widget.ImageView) findViewById(com.navdy.client.R.id.attach_photo_icon_3));
        imageViewArrayList.add((android.widget.ImageView) findViewById(com.navdy.client.R.id.attach_photo_icon_4));
        imageViewArrayList.add((android.widget.ImageView) findViewById(com.navdy.client.R.id.attach_photo_icon_5));
        this.imageViewIterator = imageViewArrayList.iterator();
    }

    private void addPhotoToGallery(android.graphics.Bitmap attachmentPhoto) {
        android.graphics.Bitmap icon = android.graphics.Bitmap.createScaledBitmap(attachmentPhoto, 32, 32, true);
        if (icon != null) {
            android.widget.ImageView imageView = (android.widget.ImageView) this.imageViewIterator.next();
            imageView.setImageBitmap(icon);
            imageView.setVisibility(View.VISIBLE);
        }
    }

    @com.squareup.otto.Subscribe
    public void onReceiveSubmitTicketFinishedEvent(com.navdy.client.app.framework.util.SupportTicketService.SubmitTicketFinishedEvent submitTicketFinishedEvent) {
        if (submitTicketFinishedEvent.getTicketId().equals(this.currentTicketId)) {
            if (submitTicketFinishedEvent.getConditionsMet()) {
                this.dismissDialogsAndFinishRunnable.run();
            } else {
                this.handler.postDelayed(this.dismissDialogsAndFinishRunnable, com.here.odnp.config.OdnpConfigStatic.CELL_NO_CHANGE_LIMITER_TIME);
            }
        }
    }

    protected void onPause() {
        dismissAllDialogs();
        super.onPause();
    }

    private void dismissAllDialogs() {
        java.util.ListIterator<java.lang.ref.WeakReference<android.app.Dialog>> dialogIterator = this.dialogs.listIterator();
        while (dialogIterator.hasNext()) {
            java.lang.ref.WeakReference<android.app.Dialog> weakReferenceToDialog = (java.lang.ref.WeakReference) dialogIterator.next();
            if (weakReferenceToDialog.get() != null) {
                android.app.Dialog dialog = (android.app.Dialog) weakReferenceToDialog.get();
                if (dialog != null) {
                    dialog.dismiss();
                }
            } else {
                dialogIterator.remove();
            }
        }
    }

    public void onLogCollectionSucceeded(java.util.ArrayList<java.io.File> logAttachments) {
        this.logger.i("onLogCollectionSucceeded: " + logAttachments.size());
        if (!logAttachments.isEmpty() && this.attachments != null) {
            this.attachments.addAll(logAttachments);
            java.util.Iterator it = this.attachments.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((java.io.File) it.next()).getName().contains("display_log")) {
                        this.displayLogAttached = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            if (!this.displayLogAttached) {
                this.logger.d("display log was not found");
            }
        }
        runOnUiThread(new com.navdy.client.app.ui.settings.ContactUsActivity.Anon17());
    }

    public void onLogCollectionFailed() {
        this.logger.i("onLogCollectionFailed");
        runOnUiThread(new com.navdy.client.app.ui.settings.ContactUsActivity.Anon18());
    }

    private void startSubmitTicketService() {
        java.lang.String ticketDesc = this.problemInput.getText().toString();
        hideProgressDialog();
        if (!this.folderHasBeenCreatedForThisTicket) {
            if (!com.navdy.client.app.framework.util.SupportTicketService.createFolderForTicket(this.attachments, createJSONFromTicketContents(ticketDesc, this.vin))) {
                this.logger.e("creating folder for ticket failed");
                return;
            } else {
                this.folderHasBeenCreatedForThisTicket = true;
                createReadMeIfDoesNotExist();
            }
        } else {
            this.logger.v("folder for this ticket already exists");
        }
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        android.content.Intent i = new android.content.Intent(context, com.navdy.client.app.framework.util.SupportTicketService.class);
        i.putExtra(com.navdy.client.app.framework.util.SupportTicketService.EXTRA_RESET_COUNTER, true);
        i.putExtra(com.navdy.client.app.framework.util.SupportTicketService.EXTRA_EMAIL, this.email);
        this.currentTicketId = java.util.UUID.randomUUID().toString();
        i.putExtra(com.navdy.client.app.framework.util.SupportTicketService.EXTRA_TICKET_RANDOM_ID, this.currentTicketId);
        context.startService(i);
        hideSubmitButton();
        showSuccessfulTicketDialog();
    }

    public void onSelectProblemInputClick(android.view.View view) {
        this.problemInput.requestFocus();
        ((android.view.inputmethod.InputMethodManager) getSystemService("input_method")).toggleSoftInput(2, 0);
    }

    public void incrementPhotoAttachments() {
        this.photoAttachmentCount++;
        if (this.photoAttachmentCount >= 5) {
            this.attachPhotoTextView.setTextColor(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.grey_hint));
        }
    }
}
