package com.navdy.client.app.ui.firstlaunch;

public class AppSetupBottomCardGenericFragment extends com.navdy.client.app.ui.base.BaseSupportFragment {
    public static final java.lang.String SCREEN = "screen";
    private android.widget.TextView blueText;
    private android.widget.Button button;
    private android.widget.TextView description;
    private com.navdy.client.app.ui.firstlaunch.AppSetupScreen screen = com.navdy.client.app.ui.firstlaunch.AppSetupPagerAdapter.getScreen(0);
    private android.widget.TextView title;

    public void setArguments(android.os.Bundle args) {
        super.setArguments(args);
        this.screen = (com.navdy.client.app.ui.firstlaunch.AppSetupScreen) args.getParcelable("screen");
    }

    @android.support.annotation.Nullable
    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fle_app_setup_bottom_card_generic, null);
        this.title = (android.widget.TextView) rootView.findViewById(com.navdy.client.R.id.title);
        this.description = (android.widget.TextView) rootView.findViewById(com.navdy.client.R.id.desc);
        this.blueText = (android.widget.TextView) rootView.findViewById(com.navdy.client.R.id.blue_text);
        this.button = (android.widget.Button) rootView.findViewById(com.navdy.client.R.id.next_step);
        showNormal();
        return rootView;
    }

    public void showNormal() {
        showThis(this.screen.titleRes, this.screen.descriptionRes, this.screen.blueTextRes, this.screen.buttonRes);
    }

    public void showFail() {
        showThis(this.screen.titleFailRes, this.screen.descriptionFailRes, this.screen.blueTextRes, this.screen.buttonFailRes);
    }

    private void showThis(int titleRes, int descriptionRes, int blueTextRes, int buttonRes) {
        if (this.title != null) {
            if (titleRes > 0) {
                this.title.setText(titleRes);
                this.title.setVisibility(View.VISIBLE);
            } else {
                this.title.setVisibility(View.GONE);
            }
        }
        if (this.description != null) {
            if (descriptionRes > 0) {
                this.description.setText(com.navdy.client.app.framework.util.StringUtils.fromHtml(descriptionRes));
                this.description.setVisibility(View.VISIBLE);
            } else {
                this.description.setVisibility(View.GONE);
            }
        }
        if (this.blueText != null) {
            if (blueTextRes > 0) {
                this.blueText.setText(blueTextRes);
                this.blueText.setVisibility(View.VISIBLE);
            } else {
                this.blueText.setVisibility(View.GONE);
            }
        }
        if (this.button == null) {
            return;
        }
        if (buttonRes > 0) {
            this.button.setText(buttonRes);
            this.button.setVisibility(View.VISIBLE);
            return;
        }
        this.button.setVisibility(View.GONE);
    }
}
