package com.navdy.client.app.ui.settings;

public class CarSettingsActivity extends com.navdy.client.app.ui.base.BaseEditActivity {
    private android.widget.Switch autoOn;
    private android.widget.RadioButton compact;
    private android.widget.LinearLayout compactUiLinearLayout;
    private android.content.SharedPreferences customerPrefs;
    private android.widget.RadioButton normal;
    private java.lang.String obdEnabledString = "";
    private android.widget.Switch obdSwitch;
    private android.widget.CompoundButton.OnCheckedChangeListener onObdSwitchChangedListener = new com.navdy.client.app.ui.settings.CarSettingsActivity.Anon1();
    private android.content.SharedPreferences sharedPrefs;
    private android.widget.TextView vehicleInfo;

    class Anon1 implements android.widget.CompoundButton.OnCheckedChangeListener {

        /* renamed from: com.navdy.client.app.ui.settings.CarSettingsActivity$Anon1$Anon1 reason: collision with other inner class name */
        class C0068Anon1 implements java.lang.Runnable {

            /* renamed from: com.navdy.client.app.ui.settings.CarSettingsActivity$Anon1$Anon1$Anon1 reason: collision with other inner class name */
            class C0069Anon1 implements java.lang.Runnable {

                /* renamed from: com.navdy.client.app.ui.settings.CarSettingsActivity$Anon1$Anon1$Anon1$Anon1 reason: collision with other inner class name */
                class C0070Anon1 implements android.content.DialogInterface.OnClickListener {
                    C0070Anon1() {
                    }

                    public void onClick(android.content.DialogInterface dialogInterface, int whichButton) {
                        if (whichButton == -1) {
                            com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_BLACKLIST_OVERRIDDEN, true).apply();
                            com.navdy.client.app.ui.settings.CarSettingsActivity.this.obdEnabledString = com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_ON.toString();
                            return;
                        }
                        com.navdy.client.app.ui.settings.CarSettingsActivity.this.obdSwitch.setChecked(false);
                    }
                }

                /* renamed from: com.navdy.client.app.ui.settings.CarSettingsActivity$Anon1$Anon1$Anon1$Anon2 */
                class Anon2 implements android.content.DialogInterface.OnCancelListener {
                    Anon2() {
                    }

                    public void onCancel(android.content.DialogInterface dialogInterface) {
                        com.navdy.client.app.ui.settings.CarSettingsActivity.this.obdSwitch.setChecked(false);
                    }
                }

                C0069Anon1() {
                }

                public void run() {
                    com.navdy.client.app.ui.settings.CarSettingsActivity.this.showQuestionDialog(com.navdy.client.R.string.settings_obd_warning_title, com.navdy.client.R.string.settings_obd_warning_description, com.navdy.client.R.string.ok, com.navdy.client.R.string.cancel, new com.navdy.client.app.ui.settings.CarSettingsActivity.Anon1.C0068Anon1.C0069Anon1.C0070Anon1(), new com.navdy.client.app.ui.settings.CarSettingsActivity.Anon1.C0068Anon1.C0069Anon1.Anon2());
                }
            }

            /* renamed from: com.navdy.client.app.ui.settings.CarSettingsActivity$Anon1$Anon1$Anon2 */
            class Anon2 implements java.lang.Runnable {
                Anon2() {
                }

                public void run() {
                    com.navdy.client.app.ui.settings.CarSettingsActivity.this.obdSwitch.setEnabled(true);
                }
            }

            C0068Anon1() {
            }

            public void run() {
                com.navdy.client.app.ui.settings.CarSettingsActivity.this.somethingChanged = true;
                if (com.navdy.client.app.ui.settings.CarSettingsActivity.this.obdSwitch != null) {
                    if (!com.navdy.client.app.ui.settings.CarSettingsActivity.this.obdSwitch.isChecked()) {
                        com.navdy.client.app.ui.settings.CarSettingsActivity.this.obdEnabledString = com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_OFF.toString();
                    } else if (com.navdy.client.app.ui.settings.SettingsUtils.carIsBlacklistedForObdData()) {
                        com.navdy.client.app.ui.settings.CarSettingsActivity.this.runOnUiThread(new com.navdy.client.app.ui.settings.CarSettingsActivity.Anon1.C0068Anon1.C0069Anon1());
                    } else {
                        com.navdy.client.app.ui.settings.CarSettingsActivity.this.obdEnabledString = com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_ON.toString();
                    }
                }
                com.navdy.client.app.ui.settings.CarSettingsActivity.this.runOnUiThread(new com.navdy.client.app.ui.settings.CarSettingsActivity.Anon1.C0068Anon1.Anon2());
            }
        }

        Anon1() {
        }

        public void onCheckedChanged(android.widget.CompoundButton compoundButton, boolean isEnabled) {
            com.navdy.client.app.ui.settings.CarSettingsActivity.this.obdSwitch.setEnabled(false);
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.settings.CarSettingsActivity.Anon1.C0068Anon1(), 1);
            com.navdy.client.app.ui.settings.CarSettingsActivity.this.obdSwitch.setEnabled(true);
        }
    }

    class Anon2 extends android.os.AsyncTask<java.lang.Object, java.lang.Boolean, java.lang.Boolean> {
        final /* synthetic */ boolean val$autoOnIsEnabled;
        final /* synthetic */ boolean val$uiScalingIsOn;

        Anon2(boolean z, boolean z2) {
            this.val$uiScalingIsOn = z;
            this.val$autoOnIsEnabled = z2;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            com.navdy.client.app.ui.settings.CarSettingsActivity.this.showProgressDialog();
        }

        /* access modifiers changed from: protected */
        public java.lang.Boolean doInBackground(java.lang.Object... params) {
            com.navdy.client.app.ui.settings.CarSettingsActivity.this.sharedPrefs.edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_UI_SCALING_ON, this.val$uiScalingIsOn).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_AUTO_ON, this.val$autoOnIsEnabled).putString(com.navdy.client.app.ui.settings.SettingsConstants.HUD_OBD_ON, com.navdy.client.app.ui.settings.CarSettingsActivity.this.obdEnabledString).apply();
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Settings.PROFILE_SETTINGS_CHANGED);
            com.navdy.client.app.ui.settings.SettingsUtils.incrementDriverProfileSerial();
            return java.lang.Boolean.valueOf(com.navdy.client.app.ui.settings.SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(java.lang.Boolean success) {
            if (success.booleanValue()) {
                com.navdy.client.app.ui.base.BaseActivity.showShortToast(com.navdy.client.R.string.settings_car_edit_succeeded, new java.lang.Object[0]);
            } else {
                com.navdy.client.app.ui.base.BaseActivity.showShortToast(com.navdy.client.R.string.settings_need_to_be_connected_to_hud, new java.lang.Object[0]);
            }
            com.navdy.client.app.ui.settings.CarSettingsActivity.this.hideProgressDialog();
            super.onPostExecute(success);
        }
    }

    public void onNormalClick(android.view.View view) {
        this.somethingChanged = true;
        updateUiScalingSetting(false);
    }

    public void onCompactClick(android.view.View view) {
        this.somethingChanged = true;
        updateUiScalingSetting(true);
    }

    private void updateUiScalingSetting(boolean uiScalingIsOn) {
        this.normal.setChecked(!uiScalingIsOn);
        this.compact.setChecked(uiScalingIsOn);
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.settings_car);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.menu_car).build();
        this.vehicleInfo = (android.widget.TextView) findViewById(com.navdy.client.R.id.car_info);
        this.autoOn = (android.widget.Switch) findViewById(com.navdy.client.R.id.auto_on);
        this.obdSwitch = (android.widget.Switch) findViewById(com.navdy.client.R.id.obd_on);
        this.normal = (android.widget.RadioButton) findViewById(com.navdy.client.R.id.normal);
        this.compact = (android.widget.RadioButton) findViewById(com.navdy.client.R.id.compact);
        this.compactUiLinearLayout = (android.widget.LinearLayout) findViewById(com.navdy.client.R.id.compact_ui_linear_layout);
    }

    protected void onResume() {
        super.onResume();
        this.customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
        this.sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        boolean autoOnEnabled = this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_AUTO_ON, true);
        this.obdEnabledString = this.sharedPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.HUD_OBD_ON, com.navdy.client.app.ui.settings.SettingsConstants.HUD_OBD_ON_DEFAULT);
        if (this.vehicleInfo != null) {
            java.lang.String carInfo = com.navdy.client.app.ui.settings.SettingsUtils.getCarYearMakeModelString();
            if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(carInfo)) {
                carInfo = getString(com.navdy.client.R.string.tap_to_set_car_info);
            }
            this.vehicleInfo.setText(carInfo);
        }
        if (this.obdEnabledString.equals(com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_ON.toString()) || this.obdEnabledString.equals(com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting.SCAN_ON.toString())) {
            initCompoundButton(this.obdSwitch, true, this.onObdSwitchChangedListener);
        } else {
            initCompoundButton(this.obdSwitch, false, this.onObdSwitchChangedListener);
        }
        initCompoundButton(this.autoOn, autoOnEnabled);
        if (this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_COMPACT_CAPABLE, false)) {
            this.compactUiLinearLayout.setVisibility(View.VISIBLE);
            updateUiScalingSetting(this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_UI_SCALING_ON, false));
            return;
        }
        this.compactUiLinearLayout.setVisibility(View.INVISIBLE);
    }

    public void onPause() {
        this.obdSwitch.setOnCheckedChangeListener(null);
        super.onPause();
    }

    public void startEditInfoActivity(android.view.View view) {
        startActivity(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.class));
    }

    public void startObdLocatorActivity(android.view.View view) {
        startActivity(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.CarInfoActivity.class));
    }

    protected void saveChanges() {
        boolean uiScalingIsOn = this.compact.isChecked();
        boolean autoOnIsEnabled = this.autoOn.isChecked();
        this.logger.d("current obd setting: " + this.obdEnabledString);
        if (this.sharedPrefs != null) {
            new com.navdy.client.app.ui.settings.CarSettingsActivity.Anon2(uiScalingIsOn, autoOnIsEnabled).execute(new java.lang.Object[0]);
        } else {
            showShortToast(com.navdy.client.R.string.settings_car_edit_failed, new java.lang.Object[0]);
        }
    }

    public void onAutoOnClick(android.view.View view) {
        if (this.autoOn != null && this.autoOn.isEnabled()) {
            this.autoOn.performClick();
        }
    }

    public void onObdSettingChangeClick(android.view.View view) {
        if (this.obdSwitch != null && this.obdSwitch.isEnabled()) {
            this.obdSwitch.performClick();
        }
    }
}
