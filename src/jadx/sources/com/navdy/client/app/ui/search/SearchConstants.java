package com.navdy.client.app.ui.search;

public class SearchConstants {
    public static final java.lang.String ACTION_SEARCH = "com.navdy.client.actions.SEARCH_ACTION";
    public static final java.lang.String ACTION_SEND = "ACTION_SEND";
    public static final int CONTACT_CODE = 4;
    public static final int DETAILS_CODE = 5;
    public static final java.lang.String EXTRA_IS_VOICE_SEARCH = "extra_is_voice_search";
    public static final int FAVORITE_CODE = 1;
    public static final java.lang.String GOOGLE_ASSISTANT_SHORT_URL = "https://g.co";
    public static final java.lang.String GOOGLE_MAPS_SHORT_URL = "https://goo.gl/maps/";
    public static final java.lang.String GOOGLE_MAPS_TO_SYNTAX = "to";
    public static final java.lang.String GOOGLE_MAPS_VIA_SYNTAX = "via";
    public static final java.lang.String GOOGLE_SEARCH_SHORT_URL = "https://www.google.com/search";
    public static final int HOME_CODE = 2;
    public static final int SEARCH_CODE = 0;
    public static final java.lang.String SEARCH_RESULT = "search_result";
    public static final int SEARCH_SOURCE_AUTOCOMPLETE = 0;
    public static final int SEARCH_SOURCE_CONTACT = 1;
    public static final int SEARCH_SOURCE_MAP = 3;
    public static final int SEARCH_SOURCE_RESULT = 2;
    public static final java.lang.String SEARCH_TYPE_EXTRA = "search_type";
    public static final int WORK_CODE = 3;
    public static final java.lang.String YELP_MAPS_URL = "https://www.yelp.com/biz/";

    public enum SEARCH_SOURCE {
        SEARCH_AUTOCOMPLETE(0),
        SEARCH_CONTACT(1),
        SEARCH_RESULT(2),
        SEARCH_MAP(3);
        
        private int code;

        private SEARCH_SOURCE(int i) {
            this.code = i;
        }

        public int getCode() {
            return this.code;
        }
    }

    public enum SEARCH_TYPES {
        SEARCH(0),
        FAVORITE(1),
        HOME(2),
        WORK(3),
        CONTACT(4);
        
        private int code;

        private SEARCH_TYPES(int i) {
            this.code = i;
        }

        public int getCode() {
            return this.code;
        }
    }
}
