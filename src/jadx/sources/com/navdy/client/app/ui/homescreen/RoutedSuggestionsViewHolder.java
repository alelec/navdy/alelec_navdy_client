package com.navdy.client.app.ui.homescreen;

class RoutedSuggestionsViewHolder extends com.navdy.client.app.ui.homescreen.SuggestionsViewHolder {
    private static final long ETA_RECOMPUTE_RATE = java.util.concurrent.TimeUnit.MINUTES.toMillis(15);
    private static final long ETA_REFRESH_RATE = java.util.concurrent.TimeUnit.SECONDS.toMillis(30);
    private static final int UNKNOWN_DURATION = -1;
    static android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.class);
    private java.lang.Runnable etaRefresher = new com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.Anon2();
    private java.lang.String etaString;
    private long lastRouteCalculation = 0;
    private int routeDurationWithTraffic = -1;
    private int routeDurationWithoutTraffic = -1;
    private int routeLength;
    private com.navdy.client.app.framework.models.Suggestion suggestion = null;

    class Anon1 implements com.navdy.client.app.framework.navigation.HereRouteManager.Listener {
        final /* synthetic */ com.navdy.client.app.framework.models.Suggestion val$suggestion;

        Anon1(com.navdy.client.app.framework.models.Suggestion suggestion) {
            this.val$suggestion = suggestion;
        }

        public void onPreCalculation(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.HereRouteManager.RouteHandle routeHandle) {
        }

        public void onRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.HereRouteManager.Error error, com.here.android.mpa.routing.Route route) {
            com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.this.handleRouteCalculated(route, this.val$suggestion);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.this.updateCalendarEta();
            long now = new java.util.Date().getTime();
            if (com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.this.isCalendarEvent() && com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.this.getTimeWhenWeGetThere() > com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.this.suggestion.event.endTimestamp) {
                com.navdy.client.app.framework.util.SuggestionManager.onCalendarChanged(true);
            } else if (now > com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.this.lastRouteCalculation + com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.ETA_RECOMPUTE_RATE) {
                com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.this.calculateEtas(com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.this.suggestion);
            } else if (com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.this.isCalendarEvent()) {
                com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.handler.postDelayed(com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.this.etaRefresher, com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.ETA_REFRESH_RATE);
            }
        }
    }

    RoutedSuggestionsViewHolder(android.view.View itemView) {
        super(itemView);
    }

    void onBindViewHolder(com.navdy.client.app.framework.models.Suggestion suggestion2) {
        this.suggestion = suggestion2;
        if (this.etaString == null && this.routeLength == 0) {
            calculateEtas(suggestion2);
            return;
        }
        this.secondLine.setText(this.etaString);
        this.subIllustrationText.setDistance((double) this.routeLength);
        restartRefresher();
    }

    private void calculateEtas(com.navdy.client.app.framework.models.Suggestion suggestion2) {
        double latitude;
        double longitude;
        this.lastRouteCalculation = new java.util.Date().getTime();
        if (suggestion2 != null && suggestion2.destination != null && !com.navdy.client.app.ui.settings.SettingsUtils.isLimitingCellularData()) {
            if (suggestion2.destination.hasValidNavCoordinates()) {
                latitude = suggestion2.destination.navigationLat;
                longitude = suggestion2.destination.navigationLong;
            } else if (suggestion2.destination.hasValidDisplayCoordinates()) {
                latitude = suggestion2.destination.displayLat;
                longitude = suggestion2.destination.displayLong;
            } else {
                logger.e("No location for calculateEtaForFirstDestination");
                return;
            }
            com.navdy.client.app.framework.navigation.HereRouteManager.getInstance().calculateRoute(latitude, longitude, new com.navdy.client.app.ui.homescreen.RoutedSuggestionsViewHolder.Anon1(suggestion2));
        }
    }

    private void handleRouteCalculated(com.here.android.mpa.routing.Route route, com.navdy.client.app.framework.models.Suggestion suggestion2) {
        if (route == null) {
            logger.e("Attempting to handleRouteCalculated on a null route !");
        } else if (suggestion2 == null) {
            logger.e("Attempting to handleRouteCalculated on a null suggestion !");
        } else {
            this.routeDurationWithTraffic = route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, com.here.android.mpa.routing.Route.WHOLE_ROUTE).getDuration();
            this.routeDurationWithoutTraffic = route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.DISABLED, com.here.android.mpa.routing.Route.WHOLE_ROUTE).getDuration();
            this.routeLength = route.getLength();
            updateCalendarEta();
            this.subIllustrationText.setVisibility(View.VISIBLE);
            this.subIllustrationText.setDistance((double) this.routeLength);
            restartRefresher();
        }
    }

    private boolean isCalendarEvent() {
        return this.suggestion.event != null;
    }

    private void restartRefresher() {
        handler.removeCallbacks(this.etaRefresher);
        handler.postDelayed(this.etaRefresher, ETA_REFRESH_RATE);
    }

    private void updateCalendarEta() {
        int etaResString;
        if (this.suggestion == null) {
            logger.w("Can't update calendar ETA on a null suggestion.");
        } else if (isCalendarEvent()) {
            android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
            if (this.routeDurationWithTraffic != -1) {
                long timeLeft = this.suggestion.event.startTimestamp - getTimeWhenWeGetThere();
                if (timeLeft > 0) {
                    etaResString = com.navdy.client.R.string.depart_within;
                    this.secondLine.setTextColor(android.support.v4.content.ContextCompat.getColor(context, com.navdy.client.R.color.grey_text));
                } else {
                    etaResString = com.navdy.client.R.string.youll_arrive_late;
                    this.secondLine.setTextColor(android.support.v4.content.ContextCompat.getColor(context, com.navdy.client.R.color.red));
                }
                java.lang.String deltaText = com.navdy.client.debug.util.FormatUtils.formatDurationFromSecondsToSecondsMinutesHours(java.lang.Math.abs((int) (timeLeft / 1000)));
                if (timeLeft == 0) {
                    this.etaString = context.getString(com.navdy.client.R.string.youll_arrive_right_when_it_starts);
                } else {
                    this.etaString = context.getString(etaResString, new java.lang.Object[]{deltaText});
                }
                if (((double) this.routeDurationWithTraffic) / ((double) this.routeDurationWithoutTraffic) >= 1.25d) {
                    this.secondLine.setText(context.getString(com.navdy.client.R.string.heavy_traffic, new java.lang.Object[]{this.etaString}));
                    return;
                }
                this.secondLine.setText(this.etaString);
            }
        }
    }

    private long getTimeWhenWeGetThere() {
        return ((long) (this.routeDurationWithTraffic * 1000)) + new java.util.Date().getTime();
    }
}
