package com.navdy.client.app.ui.firstlaunch;

public class InstallCardFragment extends com.navdy.client.app.ui.base.BaseSupportFragment {
    public static final java.lang.String LAYOUT_ID = "layoutId";
    private int layoutId;
    public com.navdy.client.app.framework.models.MountInfo.MountType mountType;
    protected android.view.View rootView;

    public void setMountType(com.navdy.client.app.framework.models.MountInfo.MountType type) {
        this.mountType = type;
    }

    public void setLayoutId(int layoutId2) {
        this.layoutId = layoutId2;
    }

    @android.support.annotation.Nullable
    public android.view.View onCreateView(android.view.LayoutInflater inflater, @android.support.annotation.Nullable android.view.ViewGroup container, @android.support.annotation.Nullable android.os.Bundle savedInstanceState) {
        boolean showNewShortMountAssets = false;
        if (savedInstanceState != null) {
            this.layoutId = savedInstanceState.getInt(LAYOUT_ID);
        }
        com.navdy.client.app.framework.util.ImageCache imageCache = null;
        com.navdy.client.app.ui.firstlaunch.InstallActivity activity = (com.navdy.client.app.ui.firstlaunch.InstallActivity) getActivity();
        if (activity != null) {
            imageCache = activity.imageCache;
        }
        this.rootView = inflater.inflate(this.layoutId, container, false);
        android.widget.ImageView imageView = (android.widget.ImageView) this.rootView.findViewById(com.navdy.client.R.id.illustration);
        if (imageView != null) {
            java.lang.String box = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.BOX, "Old_Box");
            if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(box, "Old_Box")) {
                showNewShortMountAssets = true;
            }
            switch (this.layoutId) {
                case com.navdy.client.R.layout.fle_install_dial /*2130903151*/:
                    com.navdy.client.app.framework.util.ImageUtils.loadImage(imageView, com.navdy.client.R.drawable.image_fle_installation_dial, imageCache);
                    break;
                case com.navdy.client.R.layout.fle_install_overview /*2130903157*/:
                    int overview_asset = com.navdy.client.R.drawable.asset_install_2016_short_pov;
                    if (this.mountType != com.navdy.client.app.framework.models.MountInfo.MountType.SHORT) {
                        overview_asset = com.navdy.client.R.drawable.asset_install_medium_pov;
                    } else if (showNewShortMountAssets) {
                        overview_asset = com.navdy.client.R.drawable.asset_install_2017_short_pov;
                    }
                    com.navdy.client.app.framework.util.ImageUtils.loadImage(imageView, overview_asset, imageCache);
                    if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(box, "New_Box_Plus_Mounts")) {
                        android.widget.TextView checkText2 = (android.widget.TextView) this.rootView.findViewById(com.navdy.client.R.id.check_text_2);
                        if (checkText2 != null) {
                            checkText2.setText(com.navdy.client.R.string.whats_in_the_mount_kit_box);
                            break;
                        }
                    }
                    break;
                case com.navdy.client.R.layout.fle_install_secure_mount /*2130903158*/:
                    int secureAsset = com.navdy.client.R.drawable.installation_image_secure_2016;
                    if (showNewShortMountAssets) {
                        secureAsset = com.navdy.client.R.drawable.installation_image_secure_2017;
                    }
                    com.navdy.client.app.framework.util.ImageUtils.loadImage(imageView, secureAsset, imageCache);
                    break;
                case com.navdy.client.R.layout.fle_install_tidying_up /*2130903160*/:
                    com.navdy.client.app.framework.util.ImageUtils.loadImage(imageView, com.navdy.client.R.drawable.image_fle_installation_tidying, imageCache);
                    break;
                case com.navdy.client.R.layout.fle_install_turn_on /*2130903161*/:
                    com.navdy.client.app.framework.util.ImageUtils.loadImage(imageView, com.navdy.client.R.drawable.image_fle_installation_power, imageCache);
                    break;
            }
        }
        return this.rootView;
    }

    public void onSaveInstanceState(android.os.Bundle outState) {
        outState.putInt(LAYOUT_ID, this.layoutId);
        super.onSaveInstanceState(outState);
    }

    public java.lang.String getScreen() {
        switch (this.layoutId) {
            case com.navdy.client.R.layout.fle_install_dial /*2130903151*/:
                return com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.DIAL;
            case com.navdy.client.R.layout.fle_install_lens_check /*2130903153*/:
                return com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.LENS_POSITION;
            case com.navdy.client.R.layout.fle_install_locate_obd /*2130903154*/:
                return com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.PLUG_OBD;
            case com.navdy.client.R.layout.fle_install_mounts /*2130903156*/:
                return com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.SHORT_MOUNT;
            case com.navdy.client.R.layout.fle_install_overview /*2130903157*/:
                return com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.OVERVIEW;
            case com.navdy.client.R.layout.fle_install_secure_mount /*2130903158*/:
                return com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.SECURE;
            case com.navdy.client.R.layout.fle_install_tidying_up /*2130903160*/:
                return com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.TIDY_UP;
            case com.navdy.client.R.layout.fle_install_turn_on /*2130903161*/:
                return com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.POWER_ON;
            default:
                return null;
        }
    }
}
