package com.navdy.client.app.ui.firstlaunch;

public class ScreenLightActivity extends com.navdy.client.app.ui.base.BaseActivity {
    private static final java.lang.String WAKE_LOCK_TAG = "TORCH_WAKE_LOCK";
    private com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.firstlaunch.ScreenLightActivity.class);
    private android.os.PowerManager.WakeLock wakeLock;

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.screen_light);
    }

    protected void onResume() {
        super.onResume();
        startWakeLock();
        com.navdy.client.app.framework.FlashlightManager.getInstance().setScreenToFullBrightness(getWindow());
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.SCREEN_LIGHT);
    }

    protected void onPause() {
        com.navdy.client.app.framework.FlashlightManager.getInstance().setScreenBackToNormalBrightness(getWindow());
        stopWakeLock();
        super.onPause();
    }

    private void startWakeLock() {
        if (this.wakeLock == null) {
            this.logger.d("Getting a new WakeLock");
            android.os.PowerManager pm = (android.os.PowerManager) getSystemService("power");
            this.logger.d("PowerManager acquired");
            this.wakeLock = pm.newWakeLock(1, WAKE_LOCK_TAG);
            this.logger.d("WakeLock set");
        }
        this.wakeLock.acquire();
        this.logger.d("WakeLock acquired");
    }

    private void stopWakeLock() {
        if (this.wakeLock != null) {
            this.wakeLock.release();
            this.logger.d("WakeLock released");
        }
    }

    public void onScreenTap(android.view.View view) {
        finish();
    }
}
