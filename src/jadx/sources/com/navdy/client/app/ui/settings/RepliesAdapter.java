package com.navdy.client.app.ui.settings;

class RepliesAdapter extends android.support.v7.widget.RecyclerView.Adapter<com.navdy.client.app.ui.settings.ReplyViewHolder> {
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_REPLY = 1;
    private com.navdy.client.app.framework.util.ImageCache imageCache = new com.navdy.client.app.framework.util.ImageCache();
    private java.lang.Runnable itemMovedCallback;
    private android.support.v7.widget.helper.ItemTouchHelper.Callback itemTouchHelperCallback = new com.navdy.client.app.ui.settings.RepliesAdapter.Anon2();
    private final java.util.ArrayList<java.lang.String> replies;
    private com.navdy.client.app.ui.settings.MessagingSettingsActivity.ReplyClickListener replyClickListener;

    class Anon1 implements android.view.View.OnClickListener {
        final /* synthetic */ com.navdy.client.app.ui.settings.ReplyViewHolder val$holder;
        final /* synthetic */ java.lang.String val$reply;

        Anon1(java.lang.String str, com.navdy.client.app.ui.settings.ReplyViewHolder replyViewHolder) {
            this.val$reply = str;
            this.val$holder = replyViewHolder;
        }

        public void onClick(android.view.View v) {
            com.navdy.client.app.ui.settings.RepliesAdapter.this.replyClickListener.onReplyClick(this.val$reply, this.val$holder.more);
        }
    }

    class Anon2 extends android.support.v7.widget.helper.ItemTouchHelper.Callback {
        Anon2() {
        }

        public int getMovementFlags(android.support.v7.widget.RecyclerView recyclerView, android.support.v7.widget.RecyclerView.ViewHolder viewHolder) {
            if (viewHolder.getItemViewType() == 1) {
                return makeMovementFlags(3, 48);
            }
            return 0;
        }

        public boolean onMove(android.support.v7.widget.RecyclerView recyclerView, android.support.v7.widget.RecyclerView.ViewHolder viewHolder, android.support.v7.widget.RecyclerView.ViewHolder target) {
            if (viewHolder.getItemViewType() != 1 || com.navdy.client.app.ui.settings.RepliesAdapter.this.replies == null) {
                return false;
            }
            int fromPos = viewHolder.getAdapterPosition() - 1;
            int toPos = target.getAdapterPosition() - 1;
            if (toPos < 0 || fromPos < 0 || toPos >= com.navdy.client.app.ui.settings.RepliesAdapter.this.replies.size() || fromPos >= com.navdy.client.app.ui.settings.RepliesAdapter.this.replies.size()) {
                return false;
            }
            if (fromPos == toPos) {
                return true;
            }
            if (toPos >= fromPos) {
                java.util.Collections.rotate(com.navdy.client.app.ui.settings.RepliesAdapter.this.replies.subList(fromPos, toPos + 1), -1);
            } else {
                java.util.Collections.rotate(com.navdy.client.app.ui.settings.RepliesAdapter.this.replies.subList(toPos, fromPos + 1), 1);
            }
            com.navdy.client.app.ui.settings.RepliesAdapter.this.notifyItemMoved(fromPos + 1, toPos + 1);
            return true;
        }

        public void onMoved(android.support.v7.widget.RecyclerView recyclerView, android.support.v7.widget.RecyclerView.ViewHolder viewHolder, int fromPos, android.support.v7.widget.RecyclerView.ViewHolder target, int toPos, int x, int y) {
            super.onMoved(recyclerView, viewHolder, fromPos, target, toPos, x, y);
            if (com.navdy.client.app.ui.settings.RepliesAdapter.this.itemMovedCallback != null) {
                com.navdy.client.app.ui.settings.RepliesAdapter.this.itemMovedCallback.run();
            }
        }

        public void onSwiped(android.support.v7.widget.RecyclerView.ViewHolder viewHolder, int direction) {
        }

        public boolean isItemViewSwipeEnabled() {
            return false;
        }

        public boolean isLongPressDragEnabled() {
            return true;
        }
    }

    RepliesAdapter(java.util.ArrayList<java.lang.String> replies2, com.navdy.client.app.ui.settings.MessagingSettingsActivity.ReplyClickListener replyClickListener2, java.lang.Runnable itemMovedCallback2, com.navdy.client.app.framework.util.ImageCache imageCache2) {
        this.replies = replies2;
        this.replyClickListener = replyClickListener2;
        this.itemMovedCallback = itemMovedCallback2;
        this.imageCache = imageCache2;
    }

    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        }
        if (this.replies == null || position <= this.replies.size()) {
            return 1;
        }
        return 2;
    }

    public com.navdy.client.app.ui.settings.ReplyViewHolder onCreateViewHolder(android.view.ViewGroup parent, int viewType) {
        android.view.View v;
        if (viewType == 0) {
            v = android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.settings_messaging_header, parent, false);
            android.widget.ImageView img = (android.widget.ImageView) v.findViewById(com.navdy.client.R.id.illustration);
            if (img != null) {
                com.navdy.client.app.framework.util.ImageUtils.loadImage(img, com.navdy.client.R.drawable.image_quick_messages, this.imageCache);
            }
        } else {
            v = viewType == 2 ? android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.padding_footer_layout, parent, false) : android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.settings_messaging_list_item, parent, false);
        }
        return new com.navdy.client.app.ui.settings.ReplyViewHolder(v);
    }

    public void onBindViewHolder(com.navdy.client.app.ui.settings.ReplyViewHolder holder, int position) {
        if (holder.getItemViewType() == 1) {
            java.lang.String reply = (java.lang.String) this.replies.get(position - 1);
            if (reply != null) {
                android.view.View.OnClickListener clickListener = new com.navdy.client.app.ui.settings.RepliesAdapter.Anon1(reply, holder);
                if (holder.text != null) {
                    holder.text.setText(reply);
                    holder.text.setOnClickListener(clickListener);
                }
                if (holder.more != null) {
                    holder.more.setOnClickListener(clickListener);
                }
            }
        }
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getItemCount() {
        return (this.replies != null ? this.replies.size() : 0) + 2;
    }

    public java.util.ArrayList<java.lang.String> getList() {
        return this.replies;
    }

    public void replace(java.lang.String oldMessage, java.lang.String newMessage) {
        int i = this.replies.indexOf(oldMessage);
        this.replies.set(i, newMessage);
        notifyItemChanged(i + 1);
    }

    public void add(java.lang.String message) {
        if (this.replies.indexOf(message) >= 0) {
            com.navdy.client.app.ui.base.BaseActivity.showShortToast(com.navdy.client.R.string.message_already_exists, new java.lang.Object[0]);
            return;
        }
        this.replies.add(message);
        notifyDataSetChanged();
    }

    public void delete(java.lang.String message) {
        int i = this.replies.indexOf(message);
        if (i >= 0) {
            this.replies.remove(i);
            notifyItemRemoved(i + 1);
        }
    }

    android.support.v7.widget.helper.ItemTouchHelper.Callback getItemTouchHelperCallback() {
        return this.itemTouchHelperCallback;
    }
}
