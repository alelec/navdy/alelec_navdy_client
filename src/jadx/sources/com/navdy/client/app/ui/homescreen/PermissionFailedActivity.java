package com.navdy.client.app.ui.homescreen;

public class PermissionFailedActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    public static final java.lang.String EXTRA_BUTTON_RES = "extra_button_res";
    public static final java.lang.String EXTRA_DESCRIPTION_RES = "extra_description_res";
    public static final java.lang.String EXTRA_TITLE_RES = "extra_title_res";

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.mic_permission_failed);
        android.widget.TextView title = (android.widget.TextView) findViewById(com.navdy.client.R.id.title);
        android.widget.TextView description = (android.widget.TextView) findViewById(com.navdy.client.R.id.desc);
        android.widget.TextView blueText = (android.widget.TextView) findViewById(com.navdy.client.R.id.blue_text);
        android.widget.Button button = (android.widget.Button) findViewById(com.navdy.client.R.id.next_step);
        android.content.Intent intent = getIntent();
        int titleRes = intent.getIntExtra(EXTRA_TITLE_RES, com.navdy.client.R.string.fle_app_setup_default_title_fail);
        int descriptionRes = intent.getIntExtra(EXTRA_DESCRIPTION_RES, com.navdy.client.R.string.fle_app_setup_default_desc_fail);
        int buttonTextRes = intent.getIntExtra(EXTRA_BUTTON_RES, com.navdy.client.R.string.fle_app_setup_default_button_fail);
        if (title != null) {
            title.setText(titleRes);
            title.setVisibility(View.VISIBLE);
        }
        if (description != null) {
            description.setText(com.navdy.client.app.framework.util.StringUtils.fromHtml(descriptionRes));
            description.setVisibility(View.VISIBLE);
        }
        if (blueText != null) {
            blueText.setVisibility(View.GONE);
        }
        if (button != null) {
            button.setText(buttonTextRes);
            button.setVisibility(View.VISIBLE);
        }
    }

    public void onDescriptionClick(android.view.View view) {
        com.navdy.client.app.framework.util.SystemUtils.goToSystemSettingsAppInfoForOurApp(this);
    }

    public void onButtonClick(android.view.View v) {
        finish();
    }
}
