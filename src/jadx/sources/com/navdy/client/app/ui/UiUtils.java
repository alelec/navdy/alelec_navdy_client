package com.navdy.client.app.ui;

public class UiUtils {
    public static final float BLUR_RADIUS = 16.0f;
    private static android.renderscript.RenderScript rs = null;

    public static int convertDpToPx(float dp) {
        return (int) android.util.TypedValue.applyDimension(1, dp, com.navdy.client.app.NavdyApplication.getAppContext().getResources().getDisplayMetrics());
    }

    public static android.graphics.Bitmap blurThisBitmap(android.graphics.Bitmap bitmapOriginal) {
        try {
            return blurThisBitmapTheFancyWay(bitmapOriginal);
        } catch (java.lang.Exception e) {
            return blurThisBitmapTheGhettoWay(bitmapOriginal);
        }
    }

    private static android.graphics.Bitmap blurThisBitmapTheGhettoWay(android.graphics.Bitmap bitmapOriginal) {
        android.graphics.BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
        options.inSampleSize = 8;
        java.io.ByteArrayOutputStream stream = null;
        try {
            java.io.ByteArrayOutputStream stream2 = new java.io.ByteArrayOutputStream();
            try {
                bitmapOriginal.compress(android.graphics.Bitmap.CompressFormat.PNG, 50, stream2);
                byte[] byteArray = stream2.toByteArray();
                android.graphics.Bitmap blurryBitmap = android.graphics.BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
                com.navdy.service.library.util.IOUtils.closeStream(stream2);
                return blurryBitmap;
            } catch (Throwable th) {
                th = th;
                stream = stream2;
                com.navdy.service.library.util.IOUtils.closeStream(stream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            com.navdy.service.library.util.IOUtils.closeStream(stream);
            throw th;
        }
    }

    private static android.graphics.Bitmap blurThisBitmapTheFancyWay(android.graphics.Bitmap bitmapOriginal) {
        android.content.Context applicationContext = com.navdy.client.app.NavdyApplication.getAppContext();
        if (rs == null) {
            rs = android.renderscript.RenderScript.create(applicationContext);
        }
        android.renderscript.Allocation input = null;
        android.renderscript.Allocation output = null;
        android.renderscript.ScriptIntrinsicBlur script = null;
        try {
            input = android.renderscript.Allocation.createFromBitmap(rs, bitmapOriginal);
            output = android.renderscript.Allocation.createTyped(rs, input.getType());
            script = android.renderscript.ScriptIntrinsicBlur.create(rs, android.renderscript.Element.U8_4(rs));
            script.setRadius(16.0f);
            script.setInput(input);
            script.forEach(output);
            android.graphics.Bitmap blurryBitmap = bitmapOriginal.copy(android.graphics.Bitmap.Config.ARGB_8888, false);
            output.copyTo(blurryBitmap);
            return blurryBitmap;
        } finally {
            if (input != null) {
                input.destroy();
            }
            if (output != null) {
                output.destroy();
            }
            if (script != null) {
                script.destroy();
            }
        }
    }

    public static int getGoogleMapBottomPadding(@android.support.annotation.NonNull android.content.res.Resources resources) {
        return resources.getDimensionPixelSize(com.navdy.client.R.dimen.card_height) + resources.getDimensionPixelSize(com.navdy.client.R.dimen.section_header_height) + resources.getDimensionPixelSize(com.navdy.client.R.dimen.google_map_bottom_overlap);
    }

    public static int getHereMapBottomPadding(@android.support.annotation.NonNull android.content.res.Resources resources) {
        return resources.getDimensionPixelSize(com.navdy.client.R.dimen.trip_card_height) + resources.getDimensionPixelSize(com.navdy.client.R.dimen.here_map_bottom_overlap);
    }

    public static int getTabsHeight(@android.support.annotation.NonNull android.content.res.Resources resources) {
        return resources.getDimensionPixelSize(com.navdy.client.R.dimen.tab_bar_height);
    }

    public static int getDisplayHeight(@android.support.annotation.NonNull android.app.Activity activity) {
        android.view.Display display = activity.getWindowManager().getDefaultDisplay();
        android.graphics.Point size = new android.graphics.Point();
        display.getSize(size);
        return size.y;
    }

    public static int getActionBarHeight(@android.support.annotation.NonNull android.app.Activity activity) {
        android.util.TypedValue tv = new android.util.TypedValue();
        if (activity.getTheme().resolveAttribute(16843499, tv, true)) {
            return android.util.TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources().getDisplayMetrics());
        }
        return 0;
    }
}
