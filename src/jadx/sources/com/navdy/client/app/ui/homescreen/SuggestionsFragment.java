package com.navdy.client.app.ui.homescreen;

public class SuggestionsFragment extends com.navdy.client.app.ui.base.BaseSupportFragment implements com.navdy.client.ota.OTAUpdateUIClient, android.content.ServiceConnection, com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener, com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener {
    private static final int HERE_PADDING = com.navdy.client.app.framework.map.MapUtils.hereMapSidePadding;
    private static final int HERE_TOP_PADDING;
    private static final java.lang.String LAST_ARRIVAL_MARKER_STRING;
    private static final int MIN_DISTANCE_REBUILD_SUGGESTIONS = 500;
    private static final boolean VERBOSE = true;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.homescreen.SuggestionsFragment.class);
    private final com.navdy.client.app.framework.util.CustomItemClickListener clickListener = new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon3();
    private com.here.android.mpa.mapping.MapMarker destinationMarker;
    private com.navdy.client.app.ui.base.BaseGoogleMapFragment googleMapFragment;
    private com.navdy.client.app.ui.base.BaseHereMapFragment hereMapFragment;
    private com.google.android.gms.maps.model.Marker lastArrivalMarker;
    private com.navdy.service.library.events.location.Coordinate lastUpdate;
    private android.support.v7.widget.LinearLayoutManager layoutManager;
    private final com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener locationListener = new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon1();
    private final java.lang.Object lock = new java.lang.Object();
    private final com.navdy.client.app.framework.location.NavdyLocationManager navdyLocationManager = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance();
    private final com.navdy.client.app.framework.navigation.NavdyRouteHandler navdyRouteHandler = com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance();
    private android.widget.RelativeLayout offlineBanner;
    private final com.navdy.client.app.framework.util.CustomSuggestionsLongItemClickListener onLongClickListener = new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon2();
    private com.navdy.client.ota.OTAUpdateServiceInterface otaUpdateService;
    private com.here.android.mpa.mapping.MapPolyline routePolyline;
    private com.here.android.mpa.mapping.MapPolyline routeProgressPolyline;
    private com.navdy.client.app.ui.homescreen.SuggestionsFragment.SuggestionScrollListener scrollListener;
    com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter suggestionAdapter;
    private java.lang.ref.WeakReference<com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener> suggestionListener = new java.lang.ref.WeakReference<>(this);
    private android.support.v7.widget.RecyclerView suggestionRecycler;

    class Anon1 implements com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener {
        Anon1() {
        }

        public void onPhoneLocationChanged(@android.support.annotation.NonNull com.navdy.service.library.events.location.Coordinate phoneLocation) {
            if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter == null) {
                return;
            }
            if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.lastUpdate == null || com.navdy.client.app.framework.map.MapUtils.distanceBetween(phoneLocation, com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.lastUpdate) > 500.0d) {
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.lastUpdate = phoneLocation;
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter.rebuildSuggestions();
            }
        }

        public void onCarLocationChanged(@android.support.annotation.NonNull com.navdy.service.library.events.location.Coordinate carLocation) {
            if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter == null) {
                return;
            }
            if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.lastUpdate == null || com.navdy.client.app.framework.map.MapUtils.distanceBetween(carLocation, com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.lastUpdate) > 500.0d) {
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.lastUpdate = carLocation;
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter.rebuildSuggestions();
            }
        }
    }

    class Anon10 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized {
        Anon10() {
        }

        public void onInit(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.setDestinationMarker(hereMap, null);
            com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.setRoutePolyline(hereMap, null);
            com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.setProgressPolyline(hereMap, null);
        }

        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        }
    }

    class Anon11 implements com.navdy.client.app.ui.base.BaseGoogleMapFragment.OnShowMapListener {
        Anon11() {
        }

        public void onShow() {
            com.navdy.client.app.ui.homescreen.SuggestionsFragment.logger.v("onShow googleMap");
        }
    }

    class Anon2 implements com.navdy.client.app.framework.util.CustomSuggestionsLongItemClickListener {
        Anon2() {
        }

        public boolean onItemLongClick(android.view.View selectedCardRow, int actionModePosition, com.navdy.client.app.framework.models.Suggestion suggestion) {
            com.navdy.client.app.ui.homescreen.HomescreenActivity homescreenActivity = (com.navdy.client.app.ui.homescreen.HomescreenActivity) com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.getActivity();
            if (homescreenActivity == null || homescreenActivity.isFinishing()) {
                return false;
            }
            homescreenActivity.startSelectionActionMode(new com.navdy.client.app.ui.homescreen.SuggestionsFragment.CustomSuggestionsActionCallback(suggestion, homescreenActivity, actionModePosition, selectedCardRow));
            return true;
        }
    }

    class Anon3 implements com.navdy.client.app.framework.util.CustomItemClickListener {

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.client.app.ui.homescreen.HomescreenActivity val$activity;

            Anon1(com.navdy.client.app.ui.homescreen.HomescreenActivity homescreenActivity) {
                this.val$activity = homescreenActivity;
            }

            public void run() {
                this.val$activity.goToGlancesTab();
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.startActivity(new android.content.Intent(com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.getContext(), com.navdy.client.app.ui.homescreen.GlanceDialogActivity.class));
            }
        }

        class Anon10 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;
            final /* synthetic */ com.navdy.client.app.tracking.SetDestinationTracker val$setDestinationTracker;

            Anon10(com.navdy.client.app.tracking.SetDestinationTracker setDestinationTracker, com.navdy.client.app.framework.models.Destination destination) {
                this.val$setDestinationTracker = setDestinationTracker;
                this.val$destination = destination;
            }

            public void run() {
                this.val$setDestinationTracker.tagSetDestinationEvent(this.val$destination);
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.navdyRouteHandler.requestNewRoute(this.val$destination);
            }
        }

        class Anon2 implements java.lang.Runnable {
            Anon2() {
            }

            public void run() {
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.startActivity(new android.content.Intent(com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.getContext(), com.navdy.client.app.ui.homescreen.MicPermissionDialogActivity.class));
            }
        }

        /* renamed from: com.navdy.client.app.ui.homescreen.SuggestionsFragment$Anon3$Anon3 reason: collision with other inner class name */
        class C0064Anon3 implements java.lang.Runnable {
            C0064Anon3() {
            }

            public void run() {
                android.content.Intent i = new android.content.Intent(com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.getContext(), com.navdy.client.app.ui.homescreen.InfoDialogActivity.class);
                i.putExtra(com.navdy.client.app.ui.homescreen.InfoDialogActivity.EXTRA_LAYOUT, com.navdy.client.R.layout.dialog_voice_search);
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.startActivity(i);
            }
        }

        class Anon4 implements java.lang.Runnable {
            Anon4() {
            }

            public void run() {
                android.content.Intent i = new android.content.Intent(com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.getContext(), com.navdy.client.app.ui.homescreen.InfoDialogActivity.class);
                i.putExtra(com.navdy.client.app.ui.homescreen.InfoDialogActivity.EXTRA_LAYOUT, com.navdy.client.R.layout.dialog_google_now);
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.startActivity(i);
            }
        }

        class Anon5 implements java.lang.Runnable {
            Anon5() {
            }

            public void run() {
                android.content.Intent i = new android.content.Intent(com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.getContext(), com.navdy.client.app.ui.homescreen.InfoDialogActivity.class);
                i.putExtra(com.navdy.client.app.ui.homescreen.InfoDialogActivity.EXTRA_LAYOUT, com.navdy.client.R.layout.dialog_hud_local_music_browser);
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.startActivity(i);
            }
        }

        class Anon6 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.client.app.ui.homescreen.HomescreenActivity val$activity;

            Anon6(com.navdy.client.app.ui.homescreen.HomescreenActivity homescreenActivity) {
                this.val$activity = homescreenActivity;
            }

            public void run() {
                this.val$activity.goToGesture();
            }
        }

        class Anon7 implements java.lang.Runnable {
            Anon7() {
            }

            public void run() {
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.startActivity(new android.content.Intent(com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.getContext(), com.navdy.client.app.ui.settings.GestureDialogActivity.class));
            }
        }

        class Anon8 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.client.app.ui.homescreen.HomescreenActivity val$activity;

            Anon8(com.navdy.client.app.ui.homescreen.HomescreenActivity homescreenActivity) {
                this.val$activity = homescreenActivity;
            }

            public void run() {
                this.val$activity.goToFavoritesTab();
                com.navdy.client.app.ui.search.SearchActivity.startSearchActivityFor(com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES.HOME, false, this.val$activity);
            }
        }

        class Anon9 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.client.app.ui.homescreen.HomescreenActivity val$activity;

            Anon9(com.navdy.client.app.ui.homescreen.HomescreenActivity homescreenActivity) {
                this.val$activity = homescreenActivity;
            }

            public void run() {
                this.val$activity.goToFavoritesTab();
                com.navdy.client.app.ui.search.SearchActivity.startSearchActivityFor(com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES.WORK, false, this.val$activity);
            }
        }

        Anon3() {
        }

        public void onClick(android.view.View v, int position) {
            boolean isHeader;
            com.navdy.client.app.ui.homescreen.SuggestionsFragment.logger.v("position clicked: " + position);
            if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter == null) {
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.logger.e("onClick with a null adapter! WTF!");
                return;
            }
            com.navdy.client.app.framework.models.Suggestion suggestion = com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter.getItem(position);
            if (position == 0) {
                isHeader = true;
            } else {
                isHeader = false;
            }
            if (isHeader || (v instanceof com.navdy.client.app.ui.customviews.TripCardView)) {
                boolean isInPendingTrip = com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.navdyRouteHandler.isInOneOfThePendingTripStates();
                boolean isInActiveTrip = com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.navdyRouteHandler.isInOneOfTheActiveTripStates();
                if (isInPendingTrip) {
                    com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter.clickPendingTripCard(com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.getActivity());
                } else if (isInActiveTrip) {
                    com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter.clickActiveTripCard();
                } else {
                    com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.startActivity(new android.content.Intent(com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.getContext(), com.navdy.client.app.ui.search.DropPinActivity.class));
                }
            } else if (suggestion != null) {
                com.navdy.client.app.ui.homescreen.HomescreenActivity activity = (com.navdy.client.app.ui.homescreen.HomescreenActivity) com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.getActivity();
                if (suggestion.getType() == com.navdy.client.app.framework.models.Suggestion.SuggestionType.LOADING) {
                    return;
                }
                if (suggestion.isTip()) {
                    switch (suggestion.getType()) {
                        case ENABLE_MICROPHONE:
                            handleTipClick(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_MICROPHONE_TIP, new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon3.Anon2());
                            return;
                        case VOICE_SEARCH:
                            handleTipClick(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_MICROPHONE_TIP, new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon3.C0064Anon3());
                            return;
                        case GOOGLE_NOW:
                            handleTipClick(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_GOOGLE_NOW_TIP, new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon3.Anon4());
                            return;
                        case HUD_LOCAL_MUSIC_BROWSER:
                            handleTipClick(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_LOCAL_MUSIC_BROWSER_TIP, new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon3.Anon5());
                            return;
                        case ENABLE_GESTURES:
                            handleTipClick(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_GESTURE_TIP, new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon3.Anon6(activity));
                            return;
                        case TRY_GESTURES:
                            handleTipClick(com.navdy.client.app.ui.settings.SettingsConstants.USER_TRIED_GESTURES_ONCE_BEFORE, new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon3.Anon7());
                            return;
                        case ADD_HOME:
                            handleTipClick(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_ADD_HOME_TIP, new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon3.Anon8(activity));
                            return;
                        case ADD_WORK:
                            handleTipClick(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_ADD_WORK_TIP, new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon3.Anon9(activity));
                            return;
                        case ADD_FAVORITE:
                            com.navdy.client.app.ui.search.SearchActivity.startSearchActivityFor(com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES.FAVORITE, false, activity);
                            return;
                        case OTA:
                            com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.startActivity(new android.content.Intent(com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.getContext(), com.navdy.client.app.ui.settings.OtaSettingsActivity.class));
                            return;
                        case DEMO_VIDEO:
                            com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.startActivity(new android.content.Intent(com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.getContext(), com.navdy.client.app.ui.homescreen.DemoVideoDialogActivity.class));
                            return;
                        default:
                            handleTipClick(com.navdy.client.app.ui.settings.SettingsConstants.USER_ENABLED_GLANCES_ONCE_BEFORE, new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon3.Anon1(activity));
                            return;
                    }
                } else {
                    com.navdy.client.app.framework.models.Destination destination = suggestion.destination;
                    com.navdy.client.app.ui.homescreen.SuggestionsFragment.logger.v("destination object status: " + destination.toString());
                    com.navdy.client.app.tracking.SetDestinationTracker setDestinationTracker = com.navdy.client.app.tracking.SetDestinationTracker.getInstance();
                    setDestinationTracker.setSourceValue(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.SOURCE_VALUES.SUGGESTION_LIST);
                    setDestinationTracker.setDestinationType(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.TYPE_VALUES.getSetDestinationTypeFromSuggestion(suggestion));
                    if (v.getId() != com.navdy.client.R.id.nav_button) {
                        activity.showRequestNewRouteDialog(new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon3.Anon10(setDestinationTracker, destination));
                    } else if (v instanceof android.widget.ImageButton) {
                        com.navdy.client.app.ui.details.DetailsActivity.startDetailsActivity(destination, activity);
                    }
                }
            }
        }

        private void handleTipClick(java.lang.String sharedPrefKey, java.lang.Runnable runnable) {
            android.content.SharedPreferences.Editor sharedPrefEditor = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit();
            sharedPrefEditor.putBoolean(sharedPrefKey, true);
            sharedPrefEditor.apply();
            if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter != null) {
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter.rebuildSuggestions();
            }
            runnable.run();
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ java.util.ArrayList val$suggestions;

        Anon4(java.util.ArrayList arrayList) {
            this.val$suggestions = arrayList;
        }

        public void run() {
            if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter != null) {
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter.onSuggestionBuildComplete(this.val$suggestions);
            }
            android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
            if (sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.FIRST_TIME_ON_HOMESCREEN, true)) {
                sharedPrefs.edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.FIRST_TIME_ON_HOMESCREEN, false).apply();
                if (!sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_WATCHED_THE_DEMO, false)) {
                    com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.startActivity(new android.content.Intent(com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.getContext(), com.navdy.client.app.ui.homescreen.DemoVideoDialogActivity.class));
                }
            }
        }
    }

    class Anon5 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.ota.OTAUpdateService.State val$state;

        Anon5(com.navdy.client.ota.OTAUpdateService.State state) {
            this.val$state = state;
        }

        public void run() {
            com.navdy.client.app.framework.util.SuggestionManager.updateOtaStatus(this.val$state);
        }
    }

    class Anon6 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized {
        final /* synthetic */ com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo val$progress;

        Anon6(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo navdyRouteInfo) {
            this.val$progress = navdyRouteInfo;
        }

        public void onInit(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.setProgressPolyline(hereMap, this.val$progress.getProgress());
        }

        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        }
    }

    class Anon7 implements com.google.android.gms.maps.OnMapReadyCallback {
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;

        Anon7(com.navdy.client.app.framework.models.Destination destination) {
            this.val$destination = destination;
        }

        public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
            com.google.android.gms.maps.model.BitmapDescriptor icon = com.google.android.gms.maps.model.BitmapDescriptorFactory.fromResource(this.val$destination.getRecentPinAsset());
            com.google.android.gms.maps.model.LatLng position = new com.google.android.gms.maps.model.LatLng(this.val$destination.getDisplayLat(), this.val$destination.getDisplayLng());
            if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.lastArrivalMarker != null) {
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.lastArrivalMarker.setIcon(icon);
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.lastArrivalMarker.setPosition(position);
                return;
            }
            com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.lastArrivalMarker = googleMap.addMarker(new com.google.android.gms.maps.model.MarkerOptions().position(position).title(com.navdy.client.app.ui.homescreen.SuggestionsFragment.LAST_ARRIVAL_MARKER_STRING).icon(icon));
        }
    }

    class Anon8 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized {
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;

        Anon8(com.navdy.client.app.framework.models.Destination destination) {
            this.val$destination = destination;
        }

        public void onInit(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.setDestinationMarker(hereMap, this.val$destination);
            com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.hereMapFragment.centerOnUserLocationAndDestination(this.val$destination, com.here.android.mpa.mapping.Map.Animation.LINEAR);
        }

        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        }
    }

    class Anon9 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentReady {
        final /* synthetic */ com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo val$route;

        Anon9(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo navdyRouteInfo) {
            this.val$route = navdyRouteInfo;
        }

        public void onReady(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            com.here.android.mpa.common.GeoPolyline geoPolyline = this.val$route.getRoute();
            com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.setRoutePolyline(hereMap, geoPolyline);
            if (geoPolyline != null) {
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.hereMapFragment.centerOnRoute(geoPolyline, com.here.android.mpa.mapping.Map.Animation.LINEAR);
            }
        }

        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        }
    }

    private class CustomSuggestionsActionCallback implements android.support.v7.view.ActionMode.Callback {
        private final int actionModePosition;
        private final com.navdy.client.app.ui.homescreen.HomescreenActivity homescreenActivity;
        private final android.view.View selectedCardRow;
        private final com.navdy.client.app.framework.models.Suggestion suggestion;

        class Anon1 implements com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback {
            Anon1() {
            }

            public void onQueryCompleted(int nbRows, android.net.Uri uri) {
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.hideProgressDialog();
                if (nbRows <= 0) {
                    com.navdy.client.app.ui.homescreen.SuggestionsFragment.CustomSuggestionsActionCallback.this.showFavoriteEditErrorToast();
                    return;
                }
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.CustomSuggestionsActionCallback.this.showFavoriteEditSuccessToast();
                if (!com.navdy.client.app.ui.homescreen.SuggestionsFragment.CustomSuggestionsActionCallback.this.homescreenActivity.isFinishing()) {
                    com.navdy.client.app.ui.homescreen.SuggestionsFragment.CustomSuggestionsActionCallback.this.homescreenActivity.onFavoriteListChanged();
                }
            }
        }

        class Anon2 implements android.content.DialogInterface.OnClickListener {
            final /* synthetic */ com.navdy.client.app.framework.models.Suggestion val$suggestion;

            Anon2(com.navdy.client.app.framework.models.Suggestion suggestion) {
                this.val$suggestion = suggestion;
            }

            public void onClick(android.content.DialogInterface dialog, int which) {
                this.val$suggestion.destination.doNotSuggest = true;
                com.navdy.client.app.framework.suggestion.DestinationSuggestionService.reset(com.navdy.client.app.NavdyApplication.getAppContext());
                this.val$suggestion.destination.updateDoNotSuggestAsync();
                if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter != null) {
                    com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter.rebuildSuggestions();
                }
            }
        }

        CustomSuggestionsActionCallback(com.navdy.client.app.framework.models.Suggestion suggestion2, com.navdy.client.app.ui.homescreen.HomescreenActivity homescreenActivity2, int actionModePosition2, android.view.View selectedCardRow2) {
            this.suggestion = suggestion2;
            this.homescreenActivity = homescreenActivity2;
            this.actionModePosition = actionModePosition2;
            this.selectedCardRow = selectedCardRow2;
        }

        public boolean onCreateActionMode(android.support.v7.view.ActionMode mode, android.view.Menu menu) {
            android.view.MenuInflater inflater = mode.getMenuInflater();
            if (this.suggestion.isActiveTrip()) {
                inflater.inflate(com.navdy.client.R.menu.menu_active_trip_edit, menu);
                mode.setTitle((int) com.navdy.client.R.string.active_trip_title);
            } else if (this.suggestion.isPendingTrip()) {
                inflater.inflate(com.navdy.client.R.menu.menu_next_trip_edit, menu);
                mode.setTitle((int) com.navdy.client.R.string.next_trip_title);
            } else if (this.suggestion.canBeRoutedTo()) {
                inflater.inflate(com.navdy.client.R.menu.menu_suggestion_edit, menu);
                mode.setTitle((int) com.navdy.client.R.string.edit_suggestion_title);
            }
            return true;
        }

        public boolean onPrepareActionMode(android.support.v7.view.ActionMode mode, android.view.Menu menu) {
            if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter != null) {
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter.setSelectedSuggestion(this.selectedCardRow, this.suggestion.getType(), this.actionModePosition);
                com.navdy.client.app.framework.models.Suggestion suggestion2 = com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter.getItem(this.actionModePosition);
                if (!(suggestion2 == null || suggestion2.destination == null || !suggestion2.destination.isFavoriteDestination())) {
                    com.navdy.client.app.ui.homescreen.SuggestionsFragment.logger.v("Suggestion is a Favorite. Save option is disabled");
                    menu.findItem(com.navdy.client.R.id.menu_save).setVisible(false);
                }
            }
            hideTabs();
            return true;
        }

        public boolean onActionItemClicked(android.support.v7.view.ActionMode mode, android.view.MenuItem item) {
            if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter == null) {
                return false;
            }
            com.navdy.client.app.framework.models.Suggestion suggestion2 = com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter.getItem(this.actionModePosition);
            switch (item.getItemId()) {
                case com.navdy.client.R.id.menu_save /*2131756066*/:
                    saveSuggestionAsFavorite(suggestion2);
                    break;
                case com.navdy.client.R.id.menu_stop /*2131756067*/:
                    stopRoute(suggestion2);
                    break;
                case com.navdy.client.R.id.menu_delete /*2131756069*/:
                    deleteSuggestionInMenu(suggestion2);
                    break;
                default:
                    return false;
            }
            mode.finish();
            return true;
        }

        public void onDestroyActionMode(android.support.v7.view.ActionMode mode) {
            com.navdy.client.app.ui.homescreen.SuggestionsFragment.logger.v("onDestroyActionMode from Suggestion: " + mode);
            if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter != null) {
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter.endSelectionMode();
            }
            showTabs();
        }

        private void hideTabs() {
            if (this.homescreenActivity != null && !this.homescreenActivity.isFinishing()) {
                android.view.View tabs = this.homescreenActivity.findViewById(com.navdy.client.R.id.tabs_cover);
                if (tabs != null) {
                    tabs.setVisibility(View.VISIBLE);
                }
            }
        }

        private void showTabs() {
            if (this.homescreenActivity != null && !this.homescreenActivity.isFinishing()) {
                android.view.View tabs = this.homescreenActivity.findViewById(com.navdy.client.R.id.tabs_cover);
                if (tabs != null) {
                    tabs.setVisibility(View.GONE);
                }
            }
        }

        private void saveSuggestionAsFavorite(com.navdy.client.app.framework.models.Suggestion suggestion2) {
            com.navdy.client.app.ui.homescreen.SuggestionsFragment.logger.v("menu save");
            if (suggestion2.destination != null) {
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.logger.v("suggestion type: " + suggestion2.getType());
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.logger.v("suggestion name: " + suggestion2.destination.name);
                com.navdy.client.app.framework.models.Destination destination = suggestion2.destination;
                if (destination.isFavoriteDestination()) {
                    showFavoriteEditErrorToast();
                }
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.showProgressDialog();
                destination.saveDestinationAsFavoritesAsync(-1, new com.navdy.client.app.ui.homescreen.SuggestionsFragment.CustomSuggestionsActionCallback.Anon1());
            }
        }

        private void deleteSuggestionInMenu(com.navdy.client.app.framework.models.Suggestion suggestion2) {
            com.navdy.client.app.ui.homescreen.SuggestionsFragment.logger.v("menu delete");
            if (suggestion2 != null && suggestion2.destination != null) {
                new android.app.AlertDialog.Builder(com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.getActivity()).setTitle(com.navdy.client.R.string.are_you_sure).setMessage(com.navdy.client.R.string.do_not_suggest_desc).setPositiveButton(com.navdy.client.R.string.YES, new com.navdy.client.app.ui.homescreen.SuggestionsFragment.CustomSuggestionsActionCallback.Anon2(suggestion2)).setNegativeButton(com.navdy.client.R.string.no_thanks, null).show();
            }
        }

        private void stopRoute(com.navdy.client.app.framework.models.Suggestion suggestion2) {
            com.navdy.client.app.ui.homescreen.SuggestionsFragment.logger.v("menu stop");
            com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.navdyRouteHandler.stopRouting();
            if (this.suggestion.isPendingTrip() && suggestion2 != null && suggestion2.destination != null) {
                if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter != null) {
                    com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter.endSelectionMode();
                }
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.scrollToTheTop();
            }
        }

        private void showFavoriteEditSuccessToast() {
            com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.toast_favorite_edit_saved, new java.lang.Object[0]);
        }

        private void showFavoriteEditErrorToast() {
            com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.toast_favorite_edit_error, new java.lang.Object[0]);
        }
    }

    private class SuggestionScrollListener extends android.support.v7.widget.RecyclerView.OnScrollListener {
        private boolean drawerIsOpen;
        private boolean drawerWasOpen;
        private int mapSizeInPixels;
        private int y;

        private SuggestionScrollListener() {
            this.mapSizeInPixels = 0;
            this.y = 0;
            this.drawerIsOpen = false;
            this.drawerWasOpen = false;
        }

        /* synthetic */ SuggestionScrollListener(com.navdy.client.app.ui.homescreen.SuggestionsFragment x0, com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon1 x1) {
            this();
        }

        public void onScrolled(android.support.v7.widget.RecyclerView recyclerView, int dx, int dy) {
            boolean z = false;
            this.y += dy;
            if (this.y < 0) {
                this.y = 0;
            }
            if (this.drawerIsOpen != this.drawerWasOpen) {
                return;
            }
            if ((this.drawerWasOpen && this.y < this.mapSizeInPixels) || (!this.drawerWasOpen && this.y > 0)) {
                if (!this.drawerWasOpen) {
                    z = true;
                }
                this.drawerWasOpen = z;
                updateChevron(this.drawerWasOpen);
            }
        }

        public void onScrollStateChanged(android.support.v7.widget.RecyclerView recyclerView, int newState) {
            if (newState == 0) {
                if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter == null) {
                    if (this.mapSizeInPixels == 0) {
                        return;
                    }
                } else if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.hereMapFragment == null || !com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.hereMapFragment.isVisible()) {
                    this.mapSizeInPixels = com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter.getGoogleMapHeight();
                } else {
                    this.mapSizeInPixels = com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionAdapter.getHereMapHeight();
                }
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.logger.d("Scrolled to: " + this.y + " drawer was opened: " + this.drawerIsOpen + " mapSizeInPixels: " + this.mapSizeInPixels);
                if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.layoutManager != null) {
                    if (!this.drawerIsOpen && this.y > 0) {
                        if (this.y < this.mapSizeInPixels) {
                            com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.layoutManager.scrollToPositionWithOffset(0, -this.mapSizeInPixels);
                            this.y = this.mapSizeInPixels;
                        }
                        this.drawerIsOpen = true;
                    } else if (this.drawerIsOpen && this.y < this.mapSizeInPixels) {
                        if (this.y != 0) {
                            if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionRecycler != null) {
                                com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionRecycler.smoothScrollToPosition(0);
                            }
                            this.y = 0;
                        }
                        this.drawerIsOpen = false;
                    }
                    this.drawerWasOpen = this.drawerIsOpen;
                    updateChevron(this.drawerIsOpen);
                    return;
                }
                com.navdy.client.app.ui.homescreen.SuggestionsFragment.logger.e("Unable to get layoutManager !");
            }
        }

        public void reset() {
            this.y = 0;
            this.drawerIsOpen = false;
            updateChevron(false);
        }

        private void updateChevron(boolean drawerIsOpen2) {
            android.widget.ImageView chevron = null;
            if (com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionRecycler != null) {
                chevron = (android.widget.ImageView) com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.suggestionRecycler.findViewById(com.navdy.client.R.id.chevron);
            }
            if (chevron != null) {
                if (drawerIsOpen2) {
                    chevron.setImageResource(com.navdy.client.R.drawable.icon_suggest_arrow_down);
                } else {
                    chevron.setImageResource(com.navdy.client.R.drawable.ic_chevron);
                }
            }
            android.support.v4.app.FragmentActivity activity = com.navdy.client.app.ui.homescreen.SuggestionsFragment.this.getActivity();
            if (activity != null && !activity.isFinishing()) {
                ((com.navdy.client.app.ui.homescreen.HomescreenActivity) activity).hideConnectionBanner(null);
            }
        }
    }

    static {
        android.content.res.Resources res = com.navdy.client.app.NavdyApplication.getAppContext().getResources();
        HERE_TOP_PADDING = res.getDimensionPixelSize(com.navdy.client.R.dimen.search_bar_height) + (res.getDimensionPixelSize(com.navdy.client.R.dimen.search_bar_margin) * 2) + com.navdy.client.app.framework.map.MapUtils.hereMapTopDownPadding;
        LAST_ARRIVAL_MARKER_STRING = res.getString(com.navdy.client.R.string.last_arrival_marker);
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = null;
        android.support.v4.app.FragmentActivity activity = getActivity();
        try {
            rootView = inflater.inflate(com.navdy.client.R.layout.hs_fragment_suggestions, container, false);
            com.navdy.client.app.framework.Injector.inject(com.navdy.client.app.NavdyApplication.getAppContext(), this);
            this.offlineBanner = (android.widget.RelativeLayout) rootView.findViewById(com.navdy.client.R.id.offline_banner);
        } catch (Throwable t) {
            logger.e("Error while inflating the suggestions fragment.", t);
            com.navdy.client.app.framework.util.GmsUtils.finishIfGmsIsNotUpToDate(activity);
        }
        this.googleMapFragment = (com.navdy.client.app.ui.base.BaseGoogleMapFragment) activity.getFragmentManager().findFragmentById(com.navdy.client.R.id.home_fragment_google_map_fragment);
        if (this.googleMapFragment != null) {
            this.googleMapFragment.hide();
        }
        this.hereMapFragment = (com.navdy.client.app.ui.base.BaseHereMapFragment) activity.getFragmentManager().findFragmentById(com.navdy.client.R.id.home_fragment_here_map);
        if (this.hereMapFragment != null) {
            this.hereMapFragment.hide();
            this.hereMapFragment.setUsableArea(HERE_TOP_PADDING, HERE_PADDING, HERE_PADDING, HERE_PADDING);
        }
        if (rootView != null) {
            setRecyclerView(rootView);
        }
        com.navdy.client.app.framework.util.SuggestionManager.addListener(this.suggestionListener);
        return rootView;
    }

    @android.support.annotation.WorkerThread
    public void onSuggestionBuildComplete(java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> suggestions) {
        this.handler.post(new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon4(suggestions));
    }

    public void onResume() {
        super.onResume();
        this.lastUpdate = this.navdyLocationManager.getSmartStartCoordinates();
        updateOfflineBannerVisibility();
        if (this.suggestionAdapter == null) {
            initSuggestionAdapter();
        } else {
            try {
                this.suggestionAdapter.rebuildSuggestions();
            } catch (java.lang.Exception e) {
                logger.e("rebuilding suggestions failed: " + this.suggestionAdapter);
            }
        }
        connectToService();
        this.navdyRouteHandler.addListener(this);
        this.navdyLocationManager.addListener(this.locationListener);
    }

    public void onPause() {
        this.navdyLocationManager.removeListener(this.locationListener);
        this.navdyRouteHandler.removeListener(this);
        if (this.suggestionAdapter != null) {
            this.suggestionAdapter.endSelectionMode();
            this.suggestionAdapter.stopRefreshingAllEtas();
        }
        if (this.otaUpdateService != null) {
            this.otaUpdateService.unregisterUIClient();
        }
        disconnectService();
        super.onPause();
    }

    public void onDestroy() {
        com.navdy.client.app.framework.util.SuggestionManager.removeListener(this.suggestionListener);
        super.onDestroy();
    }

    public void onServiceConnected(android.content.ComponentName name, android.os.IBinder service) {
        if (service != null) {
            this.otaUpdateService = (com.navdy.client.ota.OTAUpdateServiceInterface) service;
            this.otaUpdateService.registerUIClient(this);
            if (!this.otaUpdateService.isCheckingForUpdate()) {
                com.navdy.client.ota.OTAUpdateService.State state = this.otaUpdateService.getOTAUpdateState();
                if (state != com.navdy.client.ota.OTAUpdateService.State.UPLOADING && state != com.navdy.client.ota.OTAUpdateService.State.DOWNLOADING_UPDATE) {
                    this.otaUpdateService.checkForUpdate();
                }
            }
        }
    }

    public void onServiceDisconnected(android.content.ComponentName name) {
        this.otaUpdateService = null;
    }

    public void onErrorCheckingForUpdate(com.navdy.client.ota.OTAUpdateUIClient.Error error) {
    }

    public void onStateChanged(com.navdy.client.ota.OTAUpdateService.State state, com.navdy.client.ota.model.UpdateInfo updateInfo) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon5(state), 1);
    }

    public void onDownloadProgress(com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus progress, long bytesDownloaded, byte percentage) {
    }

    public void onUploadProgress(com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus progress, long completed, byte percentage) {
    }

    public void onPendingRouteCalculating(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        clearMapObjects();
        addTripDestinationMarkerAndCenterMap(destination);
    }

    public void onPendingRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.Nullable com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo pendingRoute) {
        clearMapObjects();
        if (pendingRoute != null) {
            addTripDestinationMarkerAndCenterMap(pendingRoute.getDestination());
        }
        if (error == com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error.NONE) {
            addTripRouteAndCenterMap(pendingRoute);
        }
    }

    public void onRouteCalculating(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        clearLastArrival();
        if (this.hereMapFragment != null) {
            this.hereMapFragment.centerOnUserLocation();
        }
    }

    public void onRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
        startMapRoute(route);
    }

    public void onRouteStarted(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
        logger.v("onRouteStarted");
        startMapRoute(route);
    }

    public void onTripProgress(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo progress) {
        if (this.hereMapFragment == null) {
            logger.e("Calling onTripProgress with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon6(progress));
        }
    }

    public void onReroute() {
    }

    public void onRouteArrived(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        if (this.googleMapFragment == null) {
            logger.e("Calling onRouteArrived with a null googleMapFragment");
        } else {
            this.googleMapFragment.getMapAsync(new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon7(destination));
        }
    }

    public void onStopRoute() {
        logger.v("onStopRoute");
        clearMapObjects();
        showGoogleMap();
        this.suggestionAdapter.rebuildSuggestions();
    }

    private void setRecyclerView(android.view.View rootView) {
        this.suggestionRecycler = (android.support.v7.widget.RecyclerView) rootView.findViewById(com.navdy.client.R.id.home_fragment_recycler_view);
        this.layoutManager = new android.support.v7.widget.LinearLayoutManager(getContext());
        this.layoutManager.setOrientation(1);
        this.scrollListener = new com.navdy.client.app.ui.homescreen.SuggestionsFragment.SuggestionScrollListener(this, null);
        if (this.suggestionRecycler != null) {
            this.suggestionRecycler.setLayoutManager(this.layoutManager);
            this.suggestionRecycler.addOnScrollListener(this.scrollListener);
        }
    }

    private void initSuggestionAdapter() {
        android.content.res.Resources resources = getResources();
        android.app.Activity activity = getActivity();
        int displayHeight = com.navdy.client.app.ui.UiUtils.getDisplayHeight(activity);
        int toolbarHeight = com.navdy.client.app.ui.UiUtils.getActionBarHeight(activity);
        int tabsHeight = com.navdy.client.app.ui.UiUtils.getTabsHeight(resources);
        this.suggestionAdapter = new com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter(getContext(), ((displayHeight - toolbarHeight) - tabsHeight) - com.navdy.client.app.ui.UiUtils.getGoogleMapBottomPadding(resources), ((displayHeight - toolbarHeight) - tabsHeight) - com.navdy.client.app.ui.UiUtils.getHereMapBottomPadding(resources), resources.getDimensionPixelSize(com.navdy.client.R.dimen.list_header_height));
        if (this.suggestionRecycler != null) {
            this.suggestionRecycler.setAdapter(this.suggestionAdapter);
        }
        this.suggestionAdapter.setClickListener(this.clickListener);
        this.suggestionAdapter.setSuggestionLongClickListener(this.onLongClickListener);
    }

    private void addTripDestinationMarkerAndCenterMap(com.navdy.client.app.framework.models.Destination destination) {
        scrollToTheTop();
        showHereMap();
        if (this.hereMapFragment == null) {
            logger.e("Calling addTripDestinationMarkerAndCenterMap with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon8(destination));
        }
    }

    private void addTripRouteAndCenterMap(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
        if (this.hereMapFragment == null) {
            logger.e("Calling addTripRouteAndCenterMap with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenReady(new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon9(route));
        }
    }

    private void clearMapObjects() {
        if (this.hereMapFragment == null) {
            logger.e("Calling clearMapObjects with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon10());
        }
    }

    private void setDestinationMarker(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap, @android.support.annotation.Nullable com.navdy.client.app.framework.models.Destination destination) {
        com.here.android.mpa.mapping.MapMarker marker;
        if (destination != null) {
            marker = destination.getHereMapMarker();
        } else {
            marker = null;
        }
        synchronized (this.lock) {
            if (this.destinationMarker != null) {
                hereMap.removeMapObject(this.destinationMarker);
            }
            this.destinationMarker = marker;
            if (this.destinationMarker != null) {
                hereMap.addMapObject(this.destinationMarker);
            }
        }
    }

    private void setRoutePolyline(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap, @android.support.annotation.Nullable com.here.android.mpa.common.GeoPolyline route) {
        com.here.android.mpa.mapping.MapPolyline polyline;
        if (route != null) {
            polyline = com.navdy.client.app.framework.map.MapUtils.generateRoutePolyline(route);
        } else {
            polyline = null;
        }
        synchronized (this.lock) {
            if (this.routePolyline != null) {
                hereMap.removeMapObject(this.routePolyline);
            }
            this.routePolyline = polyline;
            if (this.routePolyline != null) {
                hereMap.addMapObject(this.routePolyline);
            }
        }
    }

    private synchronized void setProgressPolyline(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap, @android.support.annotation.Nullable com.here.android.mpa.common.GeoPolyline progressPolyline) {
        com.here.android.mpa.mapping.MapPolyline polyline;
        if (progressPolyline != null) {
            polyline = com.navdy.client.app.framework.map.MapUtils.generateProgressPolyline(progressPolyline);
        } else {
            polyline = null;
        }
        synchronized (this.lock) {
            if (this.routeProgressPolyline != null) {
                hereMap.removeMapObject(this.routeProgressPolyline);
            }
            this.routeProgressPolyline = polyline;
            if (this.routeProgressPolyline != null) {
                hereMap.addMapObject(this.routeProgressPolyline);
            }
        }
    }

    private void clearLastArrival() {
        if (this.lastArrivalMarker != null) {
            this.lastArrivalMarker.remove();
            this.lastArrivalMarker = null;
        }
    }

    private void showHereMap() {
        logger.v("showHereMap");
        if (!isInForeground()) {
            logger.w("showHereMap, not in foreground, no-op");
            return;
        }
        if (this.googleMapFragment != null) {
            this.googleMapFragment.hide();
        }
        if (this.hereMapFragment != null) {
            this.hereMapFragment.show();
        }
        scrollToTheTop();
    }

    private void showGoogleMap() {
        logger.v("showGoogleMap");
        if (!isInForeground()) {
            logger.w("showGoogleMap, not in foreground, no-op");
            return;
        }
        if (this.hereMapFragment != null) {
            this.hereMapFragment.hide();
        }
        if (this.googleMapFragment != null) {
            this.googleMapFragment.show(new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon11());
        }
        scrollToTheTop();
    }

    private void startMapRoute(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
        clearMapObjects();
        addTripDestinationMarkerAndCenterMap(route.getDestination());
        addTripRouteAndCenterMap(route);
    }

    private void connectToService() {
        android.content.Context context = getContext();
        try {
            context.bindService(com.navdy.client.ota.OTAUpdateService.getServiceIntent(context), this, 1);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }

    private void disconnectService() {
        try {
            getContext().unbindService(this);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }

    private void scrollToTheTop() {
        if (this.layoutManager != null) {
            this.layoutManager.scrollToPosition(0);
        }
        if (this.scrollListener != null) {
            this.scrollListener.reset();
        }
    }

    public void onDetach() {
        if (this.suggestionAdapter != null) {
            this.suggestionAdapter.clearAllAnimations();
            this.suggestionAdapter.stopRefreshingAllEtas();
        }
        super.onDetach();
    }

    @com.squareup.otto.Subscribe
    public void handleReachabilityStateChange(com.navdy.client.app.framework.servicehandler.NetworkStatusManager.ReachabilityEvent reachabilityEvent) {
        if (reachabilityEvent != null) {
            logger.v("reachability event received: " + reachabilityEvent.isReachable);
            if (this.suggestionAdapter != null) {
                this.suggestionAdapter.notifyDataSetChanged();
            }
            updateOfflineBannerVisibility();
        }
    }

    private void updateOfflineBannerVisibility() {
        if (this.offlineBanner != null) {
            this.offlineBanner.setVisibility(com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet() ? 8 : 0);
        }
    }
}
