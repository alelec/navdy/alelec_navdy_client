package com.navdy.client.app.ui.customviews;

public class MultipleChoiceLayout$ChoiceViewHolder$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.app.ui.customviews.MultipleChoiceLayout.ChoiceViewHolder target, java.lang.Object source) {
        target.textView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.text, "field 'textView'");
        target.highlightView = finder.findRequiredView(source, com.navdy.client.R.id.highlight, "field 'highlightView'");
    }

    public static void reset(com.navdy.client.app.ui.customviews.MultipleChoiceLayout.ChoiceViewHolder target) {
        target.textView = null;
        target.highlightView = null;
    }
}
