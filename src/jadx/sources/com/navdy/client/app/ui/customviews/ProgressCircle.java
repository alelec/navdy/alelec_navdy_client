package com.navdy.client.app.ui.customviews;

public class ProgressCircle extends android.view.View {
    private float endAngleNaturalized;
    private final com.navdy.service.library.log.Logger logger;
    private android.graphics.RectF oval;
    private android.graphics.Paint paint;
    private float startAngle;
    private float strokeWidth;
    private float sweepAngle;

    class Anon1 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon1() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator valueAnimator) {
            com.navdy.client.app.ui.customviews.ProgressCircle.this.logger.v("animationupdate: " + valueAnimator.getAnimatedValue());
            com.navdy.client.app.ui.customviews.ProgressCircle.this.sweepAngle = ((java.lang.Float) valueAnimator.getAnimatedValue()).floatValue();
            com.navdy.client.app.ui.customviews.ProgressCircle.this.invalidate();
        }
    }

    public ProgressCircle(android.content.Context context) {
        this(context, null, 0);
    }

    public ProgressCircle(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
        this.logger.v("ProgressCircle(Context, Attrs)");
    }

    public ProgressCircle(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.logger = new com.navdy.service.library.log.Logger(getClass());
        this.paint = new android.graphics.Paint();
        this.strokeWidth = 30.0f;
        this.oval = new android.graphics.RectF();
        this.startAngle = 90.0f;
        this.sweepAngle = 270.0f;
        this.endAngleNaturalized = 1.0f;
        this.logger.v("progressCircle initialized");
        this.paint.setColor(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.blue));
        this.paint.setStrokeWidth(this.strokeWidth);
        this.paint.setStyle(android.graphics.Paint.Style.STROKE);
        this.paint.setFlags(1);
    }

    public void setProgressColor(int color) {
        this.logger.v("setProgressColor");
        this.paint.setColor(color);
    }

    public void setProgress(float progress) {
        this.logger.v("setProgress to: " + progress);
        if (progress > 1.0f || progress < 0.0f) {
            throw new java.lang.RuntimeException("Value must be between 0 and 1: " + progress);
        }
        this.endAngleNaturalized = progress;
        invalidate();
    }

    protected void onDraw(android.graphics.Canvas canvas) {
        super.onDraw(canvas);
        this.logger.v("onDraw");
        this.oval.set(this.strokeWidth / 2.0f, this.strokeWidth / 2.0f, ((float) getWidth()) - (this.strokeWidth / 2.0f), ((float) getWidth()) - (this.strokeWidth / 2.0f));
        canvas.drawArc(this.oval, this.startAngle, this.sweepAngle, true, this.paint);
    }

    public void startAnimation() {
        this.logger.v("startAnimation");
        android.animation.ValueAnimator valueAnimator = android.animation.ValueAnimator.ofFloat(new float[]{this.sweepAngle, this.endAngleNaturalized});
        valueAnimator.addUpdateListener(new com.navdy.client.app.ui.customviews.ProgressCircle.Anon1());
        valueAnimator.setDuration(com.here.odnp.config.OdnpConfigStatic.MIN_ALARM_TIMER_INTERVAL);
        valueAnimator.setInterpolator(new android.view.animation.AccelerateDecelerateInterpolator());
        valueAnimator.start();
    }
}
