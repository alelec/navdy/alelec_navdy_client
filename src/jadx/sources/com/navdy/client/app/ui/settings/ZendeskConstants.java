package com.navdy.client.app.ui.settings;

public class ZendeskConstants {
    public static final java.lang.String ACTION_FETCH_HUD_LOG_UPLOAD_DELETE = "fetch_hud_log_upload_delete";
    public static final java.lang.String ACTION_UPLOAD_AWAIT_HUD_LOG = "upload_await_hud_log";
    public static final java.lang.String ACTION_UPLOAD_DELETE = "upload_delete";
    public static final java.lang.String DISPLAY_LOG = "display_log";
    public static final java.lang.Long DISPLAY_SERIAL_NUMBER = java.lang.Long.valueOf(24218023);
    public static final java.lang.Long DISPLAY_SOFTWARE_VERSION = java.lang.Long.valueOf(24228286);
    public static final java.lang.Long LOCALE = java.lang.Long.valueOf(24186753);
    public static final java.lang.Long MOBILE_APP_NAME = java.lang.Long.valueOf(24186683);
    public static final java.lang.Long MOBILE_APP_VERSION = java.lang.Long.valueOf(24186733);
    public static final java.lang.Long MOBILE_DEVICE_MAKE = java.lang.Long.valueOf(24488583);
    public static final java.lang.Long MOBILE_DEVICE_MODEL = java.lang.Long.valueOf(24530046);
    public static final java.lang.Long MOBILE_OS_VERSION = java.lang.Long.valueOf(24488603);
    public static final java.lang.Long PROBLEM_TYPES = java.lang.Long.valueOf(24228326);
    public static final java.lang.String TICKET_ACTION = "ticket_action";
    public static final java.lang.String TICKET_ATTACHMENTS_FILENAME = "attachments.zip";
    public static final java.lang.String TICKET_DESCRIPTION = "ticket_description";
    public static final java.lang.String TICKET_EMAIL = "ticket_email";
    public static final java.lang.String TICKET_HAS_DISPLAY_LOG = "ticket_has_display_log";
    public static final java.lang.String TICKET_JSON_FILENAME = "ticket_contents.txt";
    public static final java.lang.String TICKET_TYPE = "ticket_type";
    public static final java.lang.String TICKET_VIN = "ticket_vin";
    public static final java.lang.Long VEHICLE_MAKE = java.lang.Long.valueOf(24268513);
    public static final java.lang.Long VEHICLE_MODEL = java.lang.Long.valueOf(24228156);
    public static final java.lang.Long VEHICLE_YEAR = java.lang.Long.valueOf(24228296);
    public static final java.lang.Long VIN = java.lang.Long.valueOf(24228306);
}
