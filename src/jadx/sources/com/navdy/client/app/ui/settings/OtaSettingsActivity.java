package com.navdy.client.app.ui.settings;

public class OtaSettingsActivity extends com.navdy.client.app.ui.base.BaseEditActivity implements android.content.ServiceConnection, com.navdy.client.ota.OTAUpdateUIClient {
    public static final java.lang.String ARGUMENT_CONFIRMATION_MESSAGE = "message_confirmation";
    public static final int DIALOG_DOWNLOAD_UNSTABLE_CONFIRMATION = 2;
    public static final int DIALOG_DOWNLOAD_USING_MOBILE_DATA = 1;
    public static final int DIALOG_ERROR = 3;
    private static final int MAX_LINES_FOR_HIDDEN_DESCRIPTION = 2;
    private static final double SCREEN_PERCENTAGE_FOR_IMAGE = 0.35d;
    private static final int START_INDEX_OF_HEX_COLOR = 3;
    private android.widget.TextView appVersion;
    private com.navdy.client.debug.util.S3Constants.BuildType buildType;
    private android.widget.Switch buildTypeUnstableSwitch;
    private android.widget.Button button;
    private android.widget.TextView description;
    private android.widget.TextView downloadVersion;
    private android.widget.TextView hardwareSupportTitle;
    private android.widget.TextView hudVersion;
    private android.widget.ImageView image;
    private java.util.concurrent.atomic.AtomicBoolean isBound = new java.util.concurrent.atomic.AtomicBoolean(false);
    private android.widget.TextView moreOrLess;
    private com.navdy.client.ota.OTAUpdateServiceInterface otaUpdateService;
    private android.widget.ProgressBar progressBar;
    private android.widget.ProgressBar progressBar2;
    private android.widget.RelativeLayout progressBarContainer;
    private android.widget.TextView progressData;
    private android.widget.TextView progressPercentage;
    private android.content.SharedPreferences sharedPrefs;
    private boolean showingMoreInfo = false;
    private android.widget.TextView subtitle;
    private android.widget.TextView subtitle2;
    private android.widget.TextView title;
    private boolean viewsInitialized;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.ota.OTAUpdateUIClient.Error val$error;

        Anon1(com.navdy.client.ota.OTAUpdateUIClient.Error error) {
            this.val$error = error;
        }

        public void run() {
            if (com.navdy.client.app.ui.settings.OtaSettingsActivity.this.isInForeground()) {
                com.navdy.client.app.ui.settings.OtaSettingsActivity.this.setUIState(false, com.navdy.client.app.ui.settings.OtaSettingsActivity.this.getState(), com.navdy.client.app.ui.settings.OtaSettingsActivity.this.getUpdateInfo());
                java.lang.String message = null;
                java.lang.String title = com.navdy.client.app.ui.settings.OtaSettingsActivity.this.getString(com.navdy.client.R.string.error);
                switch (com.navdy.client.app.ui.settings.OtaSettingsActivity.Anon9.$SwitchMap$com$navdy$client$ota$OTAUpdateUIClient$Error[this.val$error.ordinal()]) {
                    case 1:
                        if (!com.navdy.client.app.framework.util.SystemUtils.isAirplaneModeOn(com.navdy.client.app.NavdyApplication.getAppContext())) {
                            message = com.navdy.client.app.ui.settings.OtaSettingsActivity.this.getString(com.navdy.client.R.string.no_connectivity);
                            break;
                        } else {
                            message = com.navdy.client.app.ui.settings.OtaSettingsActivity.this.getString(com.navdy.client.R.string.air_plane_mode);
                            break;
                        }
                    case 2:
                        message = com.navdy.client.app.ui.settings.OtaSettingsActivity.this.getString(com.navdy.client.R.string.server_error);
                        break;
                }
                com.navdy.client.app.ui.settings.OtaSettingsActivity.this.showSimpleDialog(3, title, message);
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.ota.OTAUpdateService.State val$state;
        final /* synthetic */ com.navdy.client.ota.model.UpdateInfo val$updateInfo;

        Anon2(com.navdy.client.ota.OTAUpdateService.State state, com.navdy.client.ota.model.UpdateInfo updateInfo) {
            this.val$state = state;
            this.val$updateInfo = updateInfo;
        }

        public void run() {
            if (com.navdy.client.app.ui.settings.OtaSettingsActivity.this.isInForeground()) {
                com.navdy.client.app.ui.settings.OtaSettingsActivity.this.setUIState(false, this.val$state, this.val$updateInfo);
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ byte val$percent;
        final /* synthetic */ com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus val$progress;
        final /* synthetic */ long val$totalDownloaded;

        Anon3(byte b, com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus downloadUpdateStatus, long j) {
            this.val$percent = b;
            this.val$progress = downloadUpdateStatus;
            this.val$totalDownloaded = j;
        }

        public void run() {
            if (com.navdy.client.app.ui.settings.OtaSettingsActivity.this.isInForeground()) {
                java.lang.String title = com.navdy.client.app.ui.settings.OtaSettingsActivity.this.getString(com.navdy.client.R.string.error);
                switch (com.navdy.client.app.ui.settings.OtaSettingsActivity.Anon9.$SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[this.val$progress.ordinal()]) {
                    case 1:
                        com.navdy.client.app.ui.settings.OtaSettingsActivity.this.showSimpleDialog(3, title, com.navdy.client.app.ui.settings.OtaSettingsActivity.this.getString(com.navdy.client.R.string.download_failed));
                        return;
                    case 2:
                        if (com.navdy.client.app.framework.util.SystemUtils.isAirplaneModeOn(com.navdy.client.app.NavdyApplication.getAppContext())) {
                            com.navdy.client.app.ui.settings.OtaSettingsActivity.this.showSimpleDialog(3, title, com.navdy.client.app.ui.settings.OtaSettingsActivity.this.getString(com.navdy.client.R.string.air_plane_mode));
                            return;
                        }
                        com.navdy.client.app.ui.settings.OtaSettingsActivity.this.showSimpleDialog(3, title, com.navdy.client.app.ui.settings.OtaSettingsActivity.this.getString(com.navdy.client.R.string.no_connectivity));
                        return;
                    case 3:
                        com.navdy.client.app.ui.settings.OtaSettingsActivity.this.showSimpleDialog(3, title, com.navdy.client.app.ui.settings.OtaSettingsActivity.this.getString(com.navdy.client.R.string.not_enough_space));
                        return;
                    case 4:
                        com.navdy.client.app.ui.settings.OtaSettingsActivity.this.setDownloadProgressPercentage(this.val$percent, this.val$totalDownloaded);
                        return;
                    case 5:
                        com.navdy.client.app.ui.settings.OtaSettingsActivity.this.setDownloadProgressPercentage(100, this.val$totalDownloaded);
                        return;
                    default:
                        com.navdy.client.app.ui.settings.OtaSettingsActivity.this.logger.e("Unsupported progress: " + this.val$progress);
                        return;
                }
            }
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ byte val$percent;
        final /* synthetic */ com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus val$progress;
        final /* synthetic */ long val$totalUploaded;

        Anon4(com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus uploadToHUDStatus, byte b, long j) {
            this.val$progress = uploadToHUDStatus;
            this.val$percent = b;
            this.val$totalUploaded = j;
        }

        public void run() {
            if (com.navdy.client.app.ui.settings.OtaSettingsActivity.this.isInForeground()) {
                java.lang.String title = com.navdy.client.app.ui.settings.OtaSettingsActivity.this.getString(com.navdy.client.R.string.error);
                switch (com.navdy.client.app.ui.settings.OtaSettingsActivity.Anon9.$SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus[this.val$progress.ordinal()]) {
                    case 1:
                        com.navdy.client.app.ui.settings.OtaSettingsActivity.this.showSimpleDialog(3, title, com.navdy.client.app.ui.settings.OtaSettingsActivity.this.getString(com.navdy.client.R.string.upload_to_device_failed));
                        return;
                    case 2:
                        com.navdy.client.app.ui.settings.OtaSettingsActivity.this.showSimpleDialog(3, title, com.navdy.client.app.ui.settings.OtaSettingsActivity.this.getString(com.navdy.client.R.string.not_connected_to_device));
                        return;
                    case 3:
                        com.navdy.client.app.ui.settings.OtaSettingsActivity.this.setUploadProgressPercentage(this.val$percent, this.val$totalUploaded);
                        return;
                    case 4:
                        com.navdy.client.app.ui.settings.OtaSettingsActivity.this.setUploadProgressPercentage(100, this.val$totalUploaded);
                        return;
                    default:
                        return;
                }
            }
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            com.navdy.client.app.ui.settings.OtaSettingsActivity.this.setupView();
        }
    }

    class Anon6 implements android.content.DialogInterface.OnClickListener {
        Anon6() {
        }

        public void onClick(android.content.DialogInterface dialog, int which) {
            com.navdy.client.app.ui.settings.OtaSettingsActivity.this.logger.d("Download on LTE approved");
            com.navdy.client.app.ui.settings.OtaSettingsActivity.this.otaUpdateService.setNetworkDownloadApproval(true);
            com.navdy.client.app.ui.settings.OtaSettingsActivity.this.otaUpdateService.downloadOTAUpdate();
        }
    }

    class Anon7 implements android.content.DialogInterface.OnClickListener {
        Anon7() {
        }

        public void onClick(android.content.DialogInterface dialog, int which) {
            com.navdy.client.app.ui.settings.OtaSettingsActivity.this.logger.d("Download on LTE cancelled");
        }
    }

    class Anon8 implements android.widget.CompoundButton.OnCheckedChangeListener {

        class Anon1 implements android.content.DialogInterface.OnClickListener {
            final /* synthetic */ com.navdy.client.ota.OTAUpdateService.State val$state;

            Anon1(com.navdy.client.ota.OTAUpdateService.State state) {
                this.val$state = state;
            }

            public void onClick(android.content.DialogInterface dialogInterface, int isPositive) {
                if (isPositive == -1) {
                    com.navdy.client.app.ui.settings.OtaSettingsActivity.this.setUnstableSource(true);
                    com.navdy.client.app.ui.settings.SettingsUtils.setFeatureMode(com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode.FEATURE_MODE_BETA);
                    com.navdy.client.app.ui.settings.OtaSettingsActivity.this.cancelAndRecheckForUpdate(this.val$state);
                    return;
                }
                com.navdy.client.app.ui.settings.OtaSettingsActivity.this.setUnstableSource(false);
            }
        }

        Anon8() {
        }

        public void onCheckedChanged(android.widget.CompoundButton buttonView, boolean isChecked) {
            com.navdy.client.app.ui.settings.OtaSettingsActivity.this.somethingChanged = true;
            com.navdy.client.ota.OTAUpdateService.State state = com.navdy.client.app.ui.settings.OtaSettingsActivity.this.getState();
            com.navdy.client.app.ui.settings.OtaSettingsActivity.sLogger.d("State of the update process :" + state.name());
            if (!isChecked) {
                com.navdy.client.app.ui.settings.OtaSettingsActivity.this.setUnstableSource(false);
                com.navdy.client.app.ui.settings.SettingsUtils.setFeatureMode(com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode.FEATURE_MODE_RELEASE);
                com.navdy.client.app.ui.settings.OtaSettingsActivity.this.cancelAndRecheckForUpdate(state);
                return;
            }
            int confirmationMessage = com.navdy.client.R.string.beta_disclaimer;
            switch (com.navdy.client.app.ui.settings.OtaSettingsActivity.Anon9.$SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType[com.navdy.client.app.ui.settings.OtaSettingsActivity.this.buildType.ordinal()]) {
                case 1:
                    confirmationMessage = com.navdy.client.R.string.beta_disclaimer;
                    break;
                case 2:
                    confirmationMessage = com.navdy.client.R.string.unstable_disclaimer;
                    break;
            }
            com.navdy.client.app.ui.settings.OtaSettingsActivity.this.showQuestionDialog(com.navdy.client.R.string.warning, confirmationMessage, com.navdy.client.R.string.ok, com.navdy.client.R.string.cancel, new com.navdy.client.app.ui.settings.OtaSettingsActivity.Anon8.Anon1(state), null);
        }
    }

    static /* synthetic */ class Anon9 {
        static final /* synthetic */ int[] $SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType = new int[com.navdy.client.debug.util.S3Constants.BuildType.values().length];
        static final /* synthetic */ int[] $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus = new int[com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.values().length];
        static final /* synthetic */ int[] $SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus = new int[com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus.values().length];
        static final /* synthetic */ int[] $SwitchMap$com$navdy$client$ota$OTAUpdateUIClient$Error = new int[com.navdy.client.ota.OTAUpdateUIClient.Error.values().length];

        static {
            try {
                $SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType[com.navdy.client.debug.util.S3Constants.BuildType.user.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType[com.navdy.client.debug.util.S3Constants.BuildType.eng.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus[com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus.UPLOAD_FAILED.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus[com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus.DEVICE_NOT_CONNECTED.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus[com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus.UPLOADING.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus[com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus.COMPLETED.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.DOWNLOAD_FAILED.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.NO_CONNECTIVITY.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.NOT_ENOUGH_SPACE.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.DOWNLOADING.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.COMPLETED.ordinal()] = 5;
            } catch (java.lang.NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateUIClient$Error[com.navdy.client.ota.OTAUpdateUIClient.Error.NO_CONNECTIVITY.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateUIClient$Error[com.navdy.client.ota.OTAUpdateUIClient.Error.SERVER_ERROR.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e13) {
            }
            $SwitchMap$com$navdy$client$ota$OTAUpdateService$State = new int[com.navdy.client.ota.OTAUpdateService.State.values().length];
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[com.navdy.client.ota.OTAUpdateService.State.UP_TO_DATE.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[com.navdy.client.ota.OTAUpdateService.State.UPDATE_AVAILABLE.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e15) {
            }
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[com.navdy.client.ota.OTAUpdateService.State.DOWNLOADING_UPDATE.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e16) {
            }
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[com.navdy.client.ota.OTAUpdateService.State.READY_TO_UPLOAD.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError e17) {
            }
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[com.navdy.client.ota.OTAUpdateService.State.UPLOADING.ordinal()] = 5;
            } catch (java.lang.NoSuchFieldError e18) {
            }
            try {
                $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[com.navdy.client.ota.OTAUpdateService.State.READY_TO_INSTALL.ordinal()] = 6;
            } catch (java.lang.NoSuchFieldError e19) {
            }
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.settings_ota);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.menu_navdy_display).build();
        this.title = (android.widget.TextView) findViewById(com.navdy.client.R.id.title);
        this.subtitle = (android.widget.TextView) findViewById(com.navdy.client.R.id.subtitle);
        this.subtitle2 = (android.widget.TextView) findViewById(com.navdy.client.R.id.subtitle2);
        this.image = (android.widget.ImageView) findViewById(com.navdy.client.R.id.image);
        this.progressBarContainer = (android.widget.RelativeLayout) findViewById(com.navdy.client.R.id.progress_container);
        this.progressBar = (android.widget.ProgressBar) findViewById(com.navdy.client.R.id.progress_bar);
        this.progressBar2 = (android.widget.ProgressBar) findViewById(com.navdy.client.R.id.progress_bar2);
        this.progressPercentage = (android.widget.TextView) findViewById(com.navdy.client.R.id.progress_percentage);
        this.progressData = (android.widget.TextView) findViewById(com.navdy.client.R.id.progress_data);
        this.button = (android.widget.Button) findViewById(com.navdy.client.R.id.button);
        this.description = (android.widget.TextView) findViewById(com.navdy.client.R.id.description);
        this.moreOrLess = (android.widget.TextView) findViewById(com.navdy.client.R.id.more_or_less);
        this.hudVersion = (android.widget.TextView) findViewById(com.navdy.client.R.id.hud_version);
        this.appVersion = (android.widget.TextView) findViewById(com.navdy.client.R.id.app_version);
        this.buildTypeUnstableSwitch = (android.widget.Switch) findViewById(com.navdy.client.R.id.build_type);
        this.hardwareSupportTitle = (android.widget.TextView) findViewById(com.navdy.client.R.id.release_notes_title);
        this.downloadVersion = (android.widget.TextView) findViewById(com.navdy.client.R.id.download_version);
        if (this.title == null || this.subtitle == null || this.subtitle2 == null || this.image == null || this.downloadVersion == null || this.progressBarContainer == null || this.progressBar == null || this.progressBar2 == null || this.progressPercentage == null || this.progressData == null || this.button == null || this.description == null || this.moreOrLess == null || this.hudVersion == null || this.appVersion == null || this.buildTypeUnstableSwitch == null) {
            throw new android.content.res.Resources.NotFoundException("Layout element");
        }
        this.sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        setBuildSources();
        this.appVersion.setText(com.navdy.client.app.ui.homescreen.HomescreenActivity.getAppVersionString());
        getIntent().putExtra(com.navdy.client.ota.OTAUpdateService.EXTRA_OTA_UPDATE_UI, false);
        this.viewsInitialized = true;
        if (this.otaUpdateService != null) {
            setupView();
        }
    }

    protected void onResume() {
        super.onResume();
        connectToService();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.Settings.UPDATE);
        adjustImageContainerHeight();
    }

    public void onPause() {
        super.onPause();
        if (this.otaUpdateService != null) {
            this.otaUpdateService.unregisterUIClient();
            disconnectService();
        }
    }

    public void onButtonClick(android.view.View view) {
        switch (getState()) {
            case UP_TO_DATE:
                this.otaUpdateService.checkForUpdate();
                return;
            case UPDATE_AVAILABLE:
                startDownload();
                return;
            case DOWNLOADING_UPDATE:
                this.otaUpdateService.cancelDownload();
                this.otaUpdateService.toggleAutoDownload(false);
                this.otaUpdateService.checkForUpdate();
                return;
            case READY_TO_UPLOAD:
            case UPLOADING:
                this.otaUpdateService.cancelUpload();
                this.otaUpdateService.toggleAutoDownload(false);
                this.otaUpdateService.resetUpdate();
                this.otaUpdateService.checkForUpdate();
                return;
            case READY_TO_INSTALL:
                this.otaUpdateService.resetUpdate();
                this.otaUpdateService.checkForUpdate();
                return;
            default:
                this.logger.e("Unsupported state for a button click: " + getState());
                return;
        }
    }

    public void onMoreInfoClick(android.view.View view) {
        this.showingMoreInfo = !this.showingMoreInfo;
        showReleaseNotes(this.showingMoreInfo);
    }

    private void connectToService() {
        android.content.Context context = getApplicationContext();
        android.content.Intent serviceIntent = com.navdy.client.ota.OTAUpdateService.getServiceIntent(context);
        if (context != null) {
            this.isBound.set(context.bindService(serviceIntent, this, 1));
        }
    }

    private void disconnectService() {
        android.content.Context context = getApplicationContext();
        if (context != null && this.isBound.get()) {
            try {
                context.unbindService(this);
            } catch (java.lang.Exception e) {
                this.logger.e("Unable to unbind from OTA Service.", e);
            }
        }
    }

    public void onErrorCheckingForUpdate(com.navdy.client.ota.OTAUpdateUIClient.Error error) {
        this.handler.post(new com.navdy.client.app.ui.settings.OtaSettingsActivity.Anon1(error));
    }

    public void onStateChanged(com.navdy.client.ota.OTAUpdateService.State state, com.navdy.client.ota.model.UpdateInfo updateInfo) {
        this.handler.post(new com.navdy.client.app.ui.settings.OtaSettingsActivity.Anon2(state, updateInfo));
    }

    public void onDownloadProgress(com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus progress, long totalDownloaded, byte percent) {
        this.handler.post(new com.navdy.client.app.ui.settings.OtaSettingsActivity.Anon3(percent, progress, totalDownloaded));
    }

    public void onUploadProgress(com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus progress, long totalUploaded, byte percent) {
        this.handler.post(new com.navdy.client.app.ui.settings.OtaSettingsActivity.Anon4(progress, percent, totalUploaded));
    }

    private void setDownloadProgressPercentage(int percentage, long totalDownloaded) {
        setThisProgressPercentage(this.progressBar, percentage, totalDownloaded);
        this.progressBar2.setProgress(1);
    }

    private void setUploadProgressPercentage(int percentage, long totalUploaded) {
        if (percentage < 1) {
            percentage = 1;
        }
        setThisProgressPercentage(this.progressBar2, percentage, totalUploaded);
        this.progressBar.setProgress(100);
    }

    private void setThisProgressPercentage(android.widget.ProgressBar progressBar3, int percentage, long doneSoFar) {
        progressBar3.setProgress(percentage);
        this.progressPercentage.setText(java.lang.String.format(java.util.Locale.getDefault(), "%1$d%%", new java.lang.Object[]{java.lang.Integer.valueOf(percentage)}));
        com.navdy.client.ota.model.UpdateInfo updateInfo = getUpdateInfo();
        if (updateInfo != null) {
            android.content.Context applicationContext = getApplicationContext();
            this.progressData.setText(java.lang.String.format("%1$s / %2$s", new java.lang.Object[]{android.text.format.Formatter.formatShortFileSize(applicationContext, doneSoFar), android.text.format.Formatter.formatShortFileSize(applicationContext, updateInfo.size)}));
        }
    }

    public void onServiceConnected(android.content.ComponentName name, android.os.IBinder service) {
        if (service != null) {
            this.otaUpdateService = (com.navdy.client.ota.OTAUpdateServiceInterface) service;
            this.otaUpdateService.registerUIClient(this);
            if (!this.otaUpdateService.isCheckingForUpdate()) {
                com.navdy.client.ota.OTAUpdateService.State state = this.otaUpdateService.getOTAUpdateState();
                if (!(state == com.navdy.client.ota.OTAUpdateService.State.UPLOADING || state == com.navdy.client.ota.OTAUpdateService.State.DOWNLOADING_UPDATE)) {
                    this.otaUpdateService.checkForUpdate();
                }
            }
            if (this.viewsInitialized) {
                this.handler.post(new com.navdy.client.app.ui.settings.OtaSettingsActivity.Anon5());
            }
        }
    }

    public void onServiceDisconnected(android.content.ComponentName name) {
        this.otaUpdateService = null;
    }

    protected void saveChanges() {
        showShortToast(com.navdy.client.R.string.settings_ota_succeeded, new java.lang.Object[0]);
        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Settings.OTA_SETTINGS_CHANGED);
    }

    private void setupView() {
        setUIState(isCheckingForUpdate(), getState(), getUpdateInfo(), getLastKnownUploadSize());
    }

    private void setUIState(boolean checkingForUpdate, com.navdy.client.ota.OTAUpdateService.State state, com.navdy.client.ota.model.UpdateInfo updateInfo) {
        setUIState(checkingForUpdate, state, updateInfo, 0);
    }

    private void setUIState(boolean checkingForUpdate, com.navdy.client.ota.OTAUpdateService.State state, com.navdy.client.ota.model.UpdateInfo updateInfo, long lastKnownUpdateSize) {
        sLogger.v("setUIState; checkingForUpdate=" + checkingForUpdate + "; state=" + state + "; updateInfo.versionName=" + (updateInfo != null ? updateInfo.versionName : "updateInfo is null!!!") + "; lastKnownUpdateSize=" + lastKnownUpdateSize);
        android.content.Context context = getApplicationContext();
        if (context != null) {
            this.hudVersion.setText(com.navdy.client.app.framework.util.StringUtils.fromHtml(getHUDVersionText()));
            this.subtitle2.setVisibility(View.GONE);
            this.progressBarContainer.setVisibility(View.GONE);
            this.downloadVersion.setVisibility(View.GONE);
            this.hardwareSupportTitle.setVisibility(View.GONE);
            this.description.setVisibility(View.GONE);
            this.button.setEnabled(true);
            this.button.setVisibility(View.VISIBLE);
            setBuildSources();
            showReleaseNotes(this.showingMoreInfo);
            if (checkingForUpdate) {
                this.title.setText(com.navdy.client.R.string.checking_for_updates);
                this.subtitle.setText(com.navdy.client.R.string.contacting_navdy_servers);
                com.navdy.client.app.framework.util.ImageUtils.loadImage(this.image, com.navdy.client.R.drawable.image_navdy_checking, null);
                this.button.setText(com.navdy.client.R.string.checking_for_update);
                this.button.setEnabled(false);
                return;
            }
            switch (state) {
                case UP_TO_DATE:
                    this.title.setText(com.navdy.client.R.string.up_to_date);
                    this.subtitle.setText(com.navdy.client.R.string.navdy_display_up_to_date);
                    this.subtitle2.setVisibility(View.VISIBLE);
                    this.subtitle2.setText(getCurrentHUDVersionText());
                    com.navdy.client.app.framework.util.ImageUtils.loadImage(this.image, com.navdy.client.R.drawable.image_navdy_up_to_date, null);
                    this.button.setText(com.navdy.client.R.string.check_for_update);
                    return;
                case UPDATE_AVAILABLE:
                    this.title.setText(com.navdy.client.R.string.update_available);
                    if (updateInfo != null) {
                        this.subtitle.setText(java.lang.String.format(getText(com.navdy.client.R.string.update_version_text).toString(), new java.lang.Object[]{com.navdy.client.ota.model.UpdateInfo.getFormattedVersionName(updateInfo), android.text.format.Formatter.formatShortFileSize(context, updateInfo.size)}));
                    } else {
                        this.subtitle.setText(com.navdy.client.R.string.no_update_info);
                    }
                    this.subtitle2.setVisibility(View.VISIBLE);
                    this.subtitle2.setText(getCurrentHUDVersionText());
                    com.navdy.client.app.framework.util.ImageUtils.loadImage(this.image, com.navdy.client.R.drawable.image_navdy_update_available, null);
                    this.button.setText(com.navdy.client.R.string.download_update);
                    return;
                case DOWNLOADING_UPDATE:
                    this.title.setText(com.navdy.client.R.string.downloading_update);
                    this.subtitle.setText(com.navdy.client.R.string.we_are_downloading_update);
                    this.subtitle2.setVisibility(View.GONE);
                    com.navdy.client.app.framework.util.ImageUtils.loadImage(this.image, com.navdy.client.R.drawable.image_phone_downloading, null);
                    this.button.setText(com.navdy.client.R.string.cancel_download);
                    makeProgressBarVisibleAndSetInfoVersion(updateInfo);
                    this.downloadVersion.setVisibility(View.VISIBLE);
                    setDownloadProgressPercentage(1, 0);
                    return;
                case READY_TO_UPLOAD:
                    this.title.setText(com.navdy.client.R.string.ready_to_transfer_update);
                    this.subtitle.setText(com.navdy.client.R.string.ready_to_transfer_update_desc);
                    this.subtitle2.setVisibility(View.GONE);
                    com.navdy.client.app.framework.util.ImageUtils.loadImage(this.image, com.navdy.client.R.drawable.image_ready_to_transfer, null);
                    makeProgressBarVisibleAndSetInfoVersion(updateInfo);
                    setUploadProgressPercentage(0, 0);
                    this.progressData.setText(com.navdy.client.R.string.waiting_for_navdy);
                    this.progressPercentage.setText("");
                    this.button.setText(com.navdy.client.R.string.cancel_download);
                    return;
                case UPLOADING:
                    long total = updateInfo != null ? updateInfo.size : 0;
                    setUploadProgressPercentage(total > 0 ? (int) ((((float) lastKnownUpdateSize) / ((float) total)) * 100.0f) : 0, lastKnownUpdateSize);
                    this.title.setText(com.navdy.client.R.string.transferring_update);
                    this.subtitle.setText(com.navdy.client.R.string.transferring_update_desc);
                    this.subtitle2.setVisibility(View.GONE);
                    com.navdy.client.app.framework.util.ImageUtils.loadImage(this.image, com.navdy.client.R.drawable.image_transferring, null);
                    makeProgressBarVisibleAndSetInfoVersion(updateInfo);
                    setUploadProgressPercentage(0, 0);
                    this.button.setText(com.navdy.client.R.string.cancel_download);
                    return;
                case READY_TO_INSTALL:
                    this.title.setText(com.navdy.client.R.string.ready_to_install);
                    this.subtitle.setText(com.navdy.client.R.string.ready_to_install_desc);
                    this.subtitle2.setVisibility(View.GONE);
                    com.navdy.client.app.framework.util.ImageUtils.loadImage(this.image, com.navdy.client.R.drawable.image_ready, null);
                    if (updateInfo != null) {
                        setUploadProgressPercentage(100, updateInfo.size);
                        makeProgressBarVisibleAndSetInfoVersion(updateInfo);
                    }
                    this.button.setText(com.navdy.client.R.string.cancel_download);
                    return;
                default:
                    return;
            }
        }
    }

    public void makeProgressBarVisibleAndSetInfoVersion(com.navdy.client.ota.model.UpdateInfo updateInfo) {
        this.logger.d("makeProgressBarVisibleAndSetInfoVersion: " + updateInfo);
        if (updateInfo != null) {
            this.progressBarContainer.setVisibility(View.VISIBLE);
            if (android.os.Build.VERSION.SDK_INT <= 21) {
                android.graphics.drawable.LayerDrawable progressBar1LayerDrawable = (android.graphics.drawable.LayerDrawable) this.progressBar.getProgressDrawable();
                android.graphics.drawable.Drawable background = progressBar1LayerDrawable.findDrawableByLayerId(com.navdy.client.R.id.progress_bar_background_1);
                android.graphics.drawable.Drawable progress = progressBar1LayerDrawable.findDrawableByLayerId(com.navdy.client.R.id.progress_bar_progress_1);
                android.graphics.drawable.LayerDrawable progressBar2LayerDrawable = (android.graphics.drawable.LayerDrawable) this.progressBar2.getProgressDrawable();
                android.graphics.drawable.Drawable background2 = progressBar2LayerDrawable.findDrawableByLayerId(com.navdy.client.R.id.progress_bar_background_2);
                android.graphics.drawable.Drawable progress2 = progressBar2LayerDrawable.findDrawableByLayerId(com.navdy.client.R.id.progress_bar_progress_2);
                background.setColorFilter(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.grey_2), android.graphics.PorterDuff.Mode.SRC_IN);
                progress.setColorFilter(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.purple_warm), android.graphics.PorterDuff.Mode.SRC_IN);
                background2.setColorFilter(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.grey_2), android.graphics.PorterDuff.Mode.SRC_IN);
                progress2.setColorFilter(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.blue), android.graphics.PorterDuff.Mode.SRC_IN);
            }
            this.downloadVersion.setVisibility(View.VISIBLE);
            this.downloadVersion.setText(java.lang.String.format("v%1$s", new java.lang.Object[]{com.navdy.client.ota.model.UpdateInfo.getFormattedVersionName(updateInfo)}));
        }
    }

    private com.navdy.client.ota.OTAUpdateService.State getState() {
        if (this.otaUpdateService != null) {
            return this.otaUpdateService.getOTAUpdateState();
        }
        return com.navdy.client.ota.OTAUpdateService.State.UP_TO_DATE;
    }

    /* access modifiers changed from: private */
    @android.support.annotation.Nullable
    public com.navdy.client.ota.model.UpdateInfo getUpdateInfo() {
        com.navdy.client.ota.model.UpdateInfo updateInfo = null;
        if (this.otaUpdateService != null) {
            updateInfo = this.otaUpdateService.getUpdateInfo();
        }
        if (updateInfo == null) {
            updateInfo = com.navdy.client.ota.OTAUpdateService.bReadUpdateInfo();
        }
        this.logger.v("updateInfo status: " + updateInfo);
        return updateInfo;
    }

    private long getLastKnownUploadSize() {
        if (this.otaUpdateService == null || getState() != com.navdy.client.ota.OTAUpdateService.State.UPLOADING) {
            return 0;
        }
        return this.otaUpdateService.lastKnownUploadSize();
    }

    private boolean isCheckingForUpdate() {
        return this.otaUpdateService != null && this.otaUpdateService.isCheckingForUpdate();
    }

    private java.lang.String getCurrentHUDVersionText() {
        if (this.otaUpdateService == null) {
            return "";
        }
        java.lang.String resourceText = getText(com.navdy.client.R.string.current_hud_version_is).toString();
        java.lang.String hudBuildVersionText = this.otaUpdateService.getHUDBuildVersionText();
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(hudBuildVersionText)) {
            return getString(com.navdy.client.R.string.current_hud_version_unknown);
        }
        return java.lang.String.format(resourceText, new java.lang.Object[]{hudBuildVersionText});
    }

    private java.lang.String getHUDVersionText() {
        if (this.otaUpdateService == null) {
            return null;
        }
        java.lang.String resourceText = getText(com.navdy.client.R.string.current_hud_version).toString();
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.otaUpdateService.getHUDBuildVersionText())) {
            return getString(com.navdy.client.R.string.unknown);
        }
        return java.lang.String.format(resourceText, new java.lang.Object[]{this.otaUpdateService.getHUDBuildVersionText()});
    }

    private void startDownload() {
        android.content.Context appContext = getApplicationContext();
        if (this.otaUpdateService == null) {
            return;
        }
        if (com.navdy.client.app.framework.util.SystemUtils.isAirplaneModeOn(appContext)) {
            showSimpleDialog(3, getString(com.navdy.client.R.string.error), getString(com.navdy.client.R.string.air_plane_mode));
        } else if (!com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(appContext)) {
            showSimpleDialog(3, getString(com.navdy.client.R.string.error), getString(com.navdy.client.R.string.no_connectivity));
        } else if (!com.navdy.service.library.util.SystemUtils.isConnectedToWifi(appContext)) {
            showSimpleDialog(1, getString(com.navdy.client.R.string.large_file_over_network), getString(com.navdy.client.R.string.large_file_over_network_desc));
        } else {
            setUIState(true, null, null);
            this.otaUpdateService.downloadOTAUpdate();
        }
    }

    public android.app.Dialog createDialog(int id, android.os.Bundle arguments) {
        android.app.Dialog dialog = super.createDialog(id, arguments);
        android.app.AlertDialog alert = (android.app.AlertDialog) dialog;
        if (dialog != null) {
            switch (id) {
                case 1:
                    alert.setButton(-1, getString(com.navdy.client.R.string.ok), new com.navdy.client.app.ui.settings.OtaSettingsActivity.Anon6());
                    alert.setButton(-2, getString(com.navdy.client.R.string.cancel), new com.navdy.client.app.ui.settings.OtaSettingsActivity.Anon7());
                    return alert;
            }
        }
        return dialog;
    }

    private void setBuildSources() {
        boolean z = false;
        if (this.buildType == null || this.buildType != com.navdy.client.ota.OTAUpdateService.getBuildType()) {
            sLogger.d("Setting build source view");
            com.navdy.client.debug.util.S3Constants.BuildSource buildSource = com.navdy.client.ota.OTAUpdateService.getBuildSource();
            sLogger.d("Build source :" + buildSource.name());
            com.navdy.client.debug.util.S3Constants.BuildSource[] sources = com.navdy.client.ota.OTAUpdateService.getBuildSources();
            this.buildType = com.navdy.client.ota.OTAUpdateService.getBuildType();
            sLogger.d("Build Type :" + this.buildType.name());
            int i = 0;
            int index = 0;
            for (com.navdy.client.debug.util.S3Constants.BuildSource source : sources) {
                if (buildSource == source) {
                    index = i;
                }
                i++;
            }
            android.widget.Switch switchR = this.buildTypeUnstableSwitch;
            if (index == 0) {
                z = true;
            }
            switchR.setChecked(z);
            this.buildTypeUnstableSwitch.setOnCheckedChangeListener(new com.navdy.client.app.ui.settings.OtaSettingsActivity.Anon8());
        }
    }

    /* access modifiers changed from: private */
    @android.annotation.SuppressLint({"CommitPrefEdits"})
    public void setUnstableSource(boolean unstable) {
        char c;
        com.navdy.client.debug.util.S3Constants.BuildSource[] sources = com.navdy.client.ota.OTAUpdateService.getBuildSources();
        if (unstable) {
            c = 0;
        } else {
            c = 1;
        }
        com.navdy.client.debug.util.S3Constants.BuildSource source = sources[c];
        com.navdy.client.ota.OTAUpdateService.persistUserPreferredBuildSource(source);
        sLogger.d("Set to unstable source ? :" + unstable);
        sLogger.d("Setting to the source  :" + source.name());
        this.sharedPrefs.edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.OTA_STATUS, com.navdy.client.app.ui.settings.SettingsConstants.OTA_STATUS_DEFAULT).putInt(com.navdy.client.app.ui.settings.SettingsConstants.OTA_VERSION, -1).putString(com.navdy.client.app.ui.settings.SettingsConstants.OTA_URL, "").putString(com.navdy.client.app.ui.settings.SettingsConstants.OTA_DESCRIPTION, "").putLong(com.navdy.client.app.ui.settings.SettingsConstants.OTA_SIZE, 0).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.OTA_IS_INCREMENTAL, false).putInt(com.navdy.client.app.ui.settings.SettingsConstants.OTA_FROM_VERSION, 0).commit();
        setUIState(true, com.navdy.client.ota.OTAUpdateService.State.UP_TO_DATE, null);
        setupView();
    }

    private void cancelAndRecheckForUpdate(com.navdy.client.ota.OTAUpdateService.State state) {
        switch (state) {
            case DOWNLOADING_UPDATE:
                this.otaUpdateService.cancelDownload();
                this.otaUpdateService.checkForUpdate();
                return;
            case UPLOADING:
                this.otaUpdateService.cancelUpload();
                this.otaUpdateService.resetUpdate();
                this.otaUpdateService.checkForUpdate();
                return;
            default:
                return;
        }
    }

    private void showReleaseNotes(boolean more) {
        sLogger.d("Show release notes, more : " + more);
        try {
            java.lang.String releaseNotesText = com.navdy.client.ota.model.UpdateInfo.extractReleaseNotes(getUpdateInfo());
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(releaseNotesText)) {
                this.description.setText(releaseNotesText);
                this.moreOrLess.setVisibility(View.VISIBLE);
                this.hardwareSupportTitle.setVisibility(View.VISIBLE);
                this.description.setVisibility(View.VISIBLE);
                if (more) {
                    expandAnimationForTextView(this.description);
                    this.description.setMaxLines(Integer.MAX_VALUE);
                    this.moreOrLess.setText(com.navdy.client.R.string.less_info);
                    return;
                }
                collapseAnimationForTextView(this.description);
                this.description.setMaxLines(2);
                this.description.setEllipsize(android.text.TextUtils.TruncateAt.END);
                this.moreOrLess.setText(com.navdy.client.R.string.more_info);
            }
        } catch (java.lang.Exception e) {
            this.logger.e("exception found");
            sLogger.d("No release notes found");
            this.moreOrLess.setVisibility(View.GONE);
            this.description.setVisibility(View.GONE);
        }
    }

    public void expandAnimationForTextView(android.widget.TextView textView) {
        textView.setMaxLines(Integer.MAX_VALUE);
    }

    public void collapseAnimationForTextView(android.widget.TextView textView) {
        textView.setMaxLines(2);
    }

    public void adjustImageContainerHeight() {
        try {
            android.util.DisplayMetrics displayMetrics = com.navdy.client.app.NavdyApplication.getAppContext().getResources().getDisplayMetrics();
            if (this.image != null) {
                android.widget.LinearLayout.LayoutParams layoutParams = (android.widget.LinearLayout.LayoutParams) this.image.getLayoutParams();
                layoutParams.height = java.lang.Double.valueOf(((double) displayMetrics.heightPixels) * SCREEN_PERCENTAGE_FOR_IMAGE).intValue();
                this.image.setLayoutParams(layoutParams);
            }
        } catch (java.lang.Exception e) {
            this.logger.e("There was a problem changing the layout height of the ota container: " + e);
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        java.lang.System.gc();
    }

    public void onUnstableVersionClick(android.view.View view) {
        if (this.buildTypeUnstableSwitch != null && this.buildTypeUnstableSwitch.isEnabled()) {
            this.buildTypeUnstableSwitch.performClick();
        }
    }
}
