package com.navdy.client.app.ui.search;

class SearchViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
    public static final int DIVIDER_HEIGHT = com.navdy.client.app.ui.UiUtils.convertDpToPx(1.0f);
    protected android.view.View bottomItemDivider;
    private android.widget.TextView details;
    private com.navdy.client.app.ui.customviews.UnitSystemTextView distance;
    private com.navdy.client.app.ui.customviews.DestinationImageView image;
    protected android.view.View itemView;
    protected android.widget.ImageButton navButton;
    private android.widget.TextView price;
    protected android.view.View row;
    private android.widget.TextView title;

    public enum PhotoType {
        RESOURCE_IMAGE,
        CONTACT
    }

    SearchViewHolder(android.view.View itemView2) {
        super(itemView2);
        this.itemView = itemView2;
        this.row = itemView2.findViewById(com.navdy.client.R.id.search_row);
        if (this.row == null) {
            this.row = itemView2.findViewById(com.navdy.client.R.id.header_container);
        }
        this.title = (android.widget.TextView) itemView2.findViewById(com.navdy.client.R.id.search_row_title);
        this.details = (android.widget.TextView) itemView2.findViewById(com.navdy.client.R.id.search_row_details);
        this.distance = (com.navdy.client.app.ui.customviews.UnitSystemTextView) itemView2.findViewById(com.navdy.client.R.id.search_row_distance);
        this.price = (android.widget.TextView) itemView2.findViewById(com.navdy.client.R.id.search_row_price);
        this.image = (com.navdy.client.app.ui.customviews.DestinationImageView) itemView2.findViewById(com.navdy.client.R.id.search_row_image);
        this.navButton = (android.widget.ImageButton) itemView2.findViewById(com.navdy.client.R.id.nav_button);
        this.bottomItemDivider = itemView2.findViewById(com.navdy.client.R.id.bottom_item_divider);
    }

    public void setTitle(java.lang.String text, int colorResource) {
        android.widget.TextView textView = this.title;
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(text)) {
            text = "";
        }
        textView.setText(text);
        this.title.setTextColor(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), colorResource));
    }

    public void setTitle(android.text.Spannable text) {
        this.title.setText(text);
    }

    public void setTitle(java.lang.String text) {
        this.title.setText(text);
    }

    void setDetails(java.lang.String text, int colorResource) {
        this.details.setVisibility(com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(text) ? 8 : 0);
        this.details.setText(text);
        this.details.setTextColor(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), colorResource));
    }

    public void setDetails(java.lang.String text) {
        this.details.setVisibility(com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(text) ? 8 : 0);
        this.details.setText(text);
    }

    public void setDetails(android.text.Spannable text) {
        this.details.setVisibility(com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(text) ? 8 : 0);
        this.details.setText(text);
    }

    void setDistance(double distanceInMeters) {
        this.distance.setVisibility(distanceInMeters <= 0.0d ? 8 : 0);
        this.distance.setDistance(distanceInMeters);
    }

    void setPrice(java.lang.String priceString) {
        this.price.setVisibility(com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(priceString) ? 8 : 0);
        this.price.setText(priceString);
    }

    public void setImage(com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem searchItem, boolean isAutocomplete) {
        this.image.clearInitials();
        this.image.setImageResource(searchItem.getRes(isAutocomplete));
    }

    public void setImage(android.graphics.Bitmap bitmap) {
        this.image.clearInitials();
        this.image.setImage(bitmap);
    }

    public void setImage(java.lang.String name) {
        this.image.setImage(name);
    }

    void showNavButton() {
        this.navButton.setVisibility(View.VISIBLE);
    }

    void hideNavButton() {
        this.navButton.setVisibility(View.INVISIBLE);
    }

    void showDeleteButton() {
        this.navButton.setVisibility(View.VISIBLE);
        this.navButton.setImageResource(com.navdy.client.R.drawable.icon_delete);
    }

    void hideBottomDivider() {
        if (this.bottomItemDivider != null) {
            this.bottomItemDivider.setVisibility(View.GONE);
        }
    }

    void showShortDivider() {
        changeDividerAlignment(true);
    }

    void showLongDivider() {
        changeDividerAlignment(false);
    }

    private void changeDividerAlignment(boolean showShort) {
        if (this.bottomItemDivider != null) {
            android.widget.RelativeLayout.LayoutParams layoutParams = new android.widget.RelativeLayout.LayoutParams(-1, DIVIDER_HEIGHT);
            if (showShort) {
                layoutParams.addRule(5, com.navdy.client.R.id.text_section);
                layoutParams.addRule(18, com.navdy.client.R.id.text_section);
            }
            layoutParams.addRule(12);
            this.bottomItemDivider.setLayoutParams(layoutParams);
            this.bottomItemDivider.setVisibility(View.VISIBLE);
        }
    }
}
