package com.navdy.client.app.ui.firstlaunch;

public class EditCarInfoActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    static final java.lang.String APP_SETUP = "app_setup";
    static final java.lang.String EXTRA_NEXT_STEP = "extra_next_step";
    static final java.lang.String INSTALL = "installation_flow";
    private static final int OTHER_DIALOG_ID = 1;
    private static final java.lang.String STATE_SELECTOR_MODE = "state_selector_mode";
    private boolean ignoreTheNextMakeSelection = false;
    private boolean ignoreTheNextModelSelection = false;
    private boolean ignoreTheNextYearSelection = false;
    private boolean isInSelectorMode = true;
    private android.widget.Spinner make;
    final android.widget.AdapterView.OnItemSelectedListener makeSelectionListener = new com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.Anon3();
    private java.lang.String makeString;
    private android.widget.EditText makeTv;
    private android.widget.Spinner model;
    final android.widget.AdapterView.OnItemSelectedListener modelSelectionListener = new com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.Anon4();
    private java.lang.String modelString;
    private android.widget.EditText modelTv;
    private android.widget.Spinner year;
    private java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation>>> yearMap = new java.util.HashMap<>();
    final android.widget.AdapterView.OnItemSelectedListener yearSelectionListener = new com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.Anon2();
    private java.lang.String yearString;
    private android.widget.EditText yearTv;

    class Anon1 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
        Anon1() {
        }

        protected void onPreExecute() {
            com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.loadImage(com.navdy.client.R.id.illustration, com.navdy.client.R.drawable.image_car_info);
        }

        /* access modifiers changed from: protected */
        public java.lang.Void doInBackground(java.lang.Void... params) {
            com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.yearMap = com.navdy.client.app.ui.firstlaunch.CarMdUtils.buildCarList();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(java.lang.Void aVoid) {
            com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.hideProgressDialog();
            if (com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.isInSelectorMode) {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.initCarSelectorScreen();
            } else {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.showManualEntry(null);
            }
        }
    }

    class Anon2 implements android.widget.AdapterView.OnItemSelectedListener {
        Anon2() {
        }

        public void onItemSelected(android.widget.AdapterView<?> adapterView, android.view.View view, int selectedYearPosition, long id) {
            handleSelection(selectedYearPosition);
        }

        public void onNothingSelected(android.widget.AdapterView<?> adapterView) {
            handleSelection(-1);
        }

        private void handleSelection(int selectedYearPosition) {
            if (com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.ignoreTheNextYearSelection) {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.ignoreTheNextYearSelection = false;
                return;
            }
            com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.logger.d("onItemSelected selectedYearPosition: " + selectedYearPosition);
            if (selectedYearPosition >= com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.year.getCount() - 1) {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.showDialogAboutOther();
                return;
            }
            com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.yearString = (java.lang.String) com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.year.getSelectedItem();
            com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.updateSpinnersWithSelection();
        }
    }

    class Anon3 implements android.widget.AdapterView.OnItemSelectedListener {
        Anon3() {
        }

        public void onItemSelected(android.widget.AdapterView<?> adapterView, android.view.View view, int selectedMakePosition, long id) {
            handleSelection(selectedMakePosition);
        }

        public void onNothingSelected(android.widget.AdapterView<?> adapterView) {
            handleSelection(-1);
        }

        private void handleSelection(int selectedMakePosition) {
            if (com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.ignoreTheNextMakeSelection) {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.ignoreTheNextMakeSelection = false;
                return;
            }
            com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.logger.d("onItemSelected selectedMakePosition: " + selectedMakePosition);
            if (selectedMakePosition >= com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.make.getCount() - 1) {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.showDialogAboutOther();
                return;
            }
            com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.makeString = (java.lang.String) com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.make.getSelectedItem();
            com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.updateSpinnersWithSelection();
        }
    }

    class Anon4 implements android.widget.AdapterView.OnItemSelectedListener {
        Anon4() {
        }

        public void onItemSelected(android.widget.AdapterView<?> adapterView, android.view.View view, int selectedModelPosition, long id) {
            handleSelection(selectedModelPosition);
        }

        public void onNothingSelected(android.widget.AdapterView<?> adapterView) {
            handleSelection(-1);
        }

        private void handleSelection(int selectedModelPosition) {
            if (com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.ignoreTheNextModelSelection) {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.ignoreTheNextModelSelection = false;
                return;
            }
            com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.logger.d("onItemSelected selectedModelPosition: " + selectedModelPosition);
            if (selectedModelPosition >= com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.model.getCount() - 1) {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.showDialogAboutOther();
                return;
            }
            com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.modelString = (java.lang.String) com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.model.getSelectedItem();
            com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.updateSpinnersWithSelection();
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            if (com.navdy.client.app.ui.settings.SettingsUtils.isUsingDefaultObdScanSetting()) {
                com.navdy.client.app.ui.settings.SettingsUtils.setDefaultObdSettingDependingOnBlacklistAsync();
            }
        }
    }

    class Anon6 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$makeString;
        final /* synthetic */ java.lang.String val$modelString;
        final /* synthetic */ java.lang.String val$yearString;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.goToNextStep();
            }
        }

        Anon6(java.lang.String str, java.lang.String str2, java.lang.String str3) {
            this.val$yearString = str;
            this.val$makeString = str2;
            this.val$modelString = str3;
        }

        public void run() {
            com.navdy.client.app.ui.firstlaunch.CarMdUtils.downloadObdCrimePhoto(this.val$yearString, this.val$makeString, this.val$modelString, new com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.Anon6.Anon1());
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$nextStep;

        Anon7(java.lang.String str) {
            this.val$nextStep = str;
        }

        public void run() {
            com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.hideProgressDialog();
            if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.val$nextStep, com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.INSTALL)) {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.startActivity(new android.content.Intent(com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.getApplicationContext(), com.navdy.client.app.ui.firstlaunch.CheckMountActivity.class));
            } else if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.val$nextStep, com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.APP_SETUP)) {
                com.navdy.client.app.ui.firstlaunch.AppSetupActivity.goToAppSetup(com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this);
            } else {
                android.content.Intent intent = new android.content.Intent(com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.getApplicationContext(), com.navdy.client.app.ui.firstlaunch.CarInfoActivity.class);
                intent.putExtra(com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.EXTRA_NEXT_STEP, this.val$nextStep);
                com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.startActivity(intent);
                com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.this.finish();
            }
        }
    }

    protected void onCreate(@android.support.annotation.Nullable android.os.Bundle savedInstanceState) {
        boolean z = true;
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_vehicle_car_md_info);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.select_your_car).build();
        if (!com.navdy.client.app.ui.firstlaunch.CarMdUtils.hasCachedCarList()) {
            showProgressDialog();
        }
        if (savedInstanceState != null) {
            java.lang.String yr = savedInstanceState.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, this.yearString);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(yr)) {
                this.yearString = yr;
            }
            java.lang.String ma = savedInstanceState.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, this.makeString);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(ma)) {
                this.makeString = ma;
            }
            java.lang.String mo = savedInstanceState.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, this.modelString);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(mo)) {
                this.modelString = mo;
            }
            if (savedInstanceState.getInt(STATE_SELECTOR_MODE, 1) != 1) {
                z = false;
            }
            this.isInSelectorMode = z;
        }
        new com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.Anon1().execute(new java.lang.Void[0]);
    }

    public void onSaveInstanceState(android.os.Bundle outState) {
        int i;
        super.onSaveInstanceState(outState);
        if (this.isInSelectorMode) {
            if (this.year == null || this.make == null || this.model == null) {
                this.logger.e("Missing layout element.");
                return;
            }
            int yearInt = this.year.getSelectedItemPosition();
            int makeInt = this.make.getSelectedItemPosition();
            int modelInt = this.model.getSelectedItemPosition();
            if (yearInt != 0) {
                this.yearString = this.year.getSelectedItem().toString();
            }
            if (makeInt != 0) {
                this.makeString = this.make.getSelectedItem().toString();
            }
            if (modelInt != 0) {
                this.modelString = this.model.getSelectedItem().toString();
            }
        } else if (this.yearTv == null || this.makeTv == null || this.modelTv == null) {
            this.logger.e("Missing layout element.");
            return;
        } else {
            this.yearString = this.yearTv.getText().toString();
            this.makeString = this.makeTv.getText().toString();
            this.modelString = this.modelTv.getText().toString();
        }
        outState.putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, this.yearString);
        outState.putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, this.makeString);
        outState.putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, this.modelString);
        java.lang.String str = STATE_SELECTOR_MODE;
        if (this.isInSelectorMode) {
            i = 1;
        } else {
            i = 0;
        }
        outState.putInt(str, i);
    }

    protected void onResume() {
        super.onResume();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.EDIT_CAR_INFO);
    }

    public void showManualEntry(android.view.View view) {
        setContentView((int) com.navdy.client.R.layout.fle_vehicle_car_info);
        loadImage(com.navdy.client.R.id.illustration, com.navdy.client.R.drawable.image_car_info);
        initManualEntryScreen();
    }

    public void showCarSelector(android.view.View view) {
        setContentView((int) com.navdy.client.R.layout.fle_vehicle_car_md_info);
        loadImage(com.navdy.client.R.id.illustration, com.navdy.client.R.drawable.image_car_info);
        initCarSelectorScreen();
    }

    private void initCarSelectorScreen() {
        this.year = (android.widget.Spinner) findViewById(com.navdy.client.R.id.pick_a_year);
        this.make = (android.widget.Spinner) findViewById(com.navdy.client.R.id.pick_a_make);
        this.model = (android.widget.Spinner) findViewById(com.navdy.client.R.id.pick_a_model);
        if (this.year == null || this.make == null || this.model == null) {
            this.logger.e("Missing layout element.");
            return;
        }
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.yearString) && com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.makeString) && com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.modelString)) {
            android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
            this.yearString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "");
            this.makeString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "");
            this.modelString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
        }
        updateSpinnersWithSelection();
        this.year.setOnItemSelectedListener(this.yearSelectionListener);
        this.make.setOnItemSelectedListener(this.makeSelectionListener);
        this.model.setOnItemSelectedListener(this.modelSelectionListener);
        this.isInSelectorMode = true;
    }

    private void updateSpinnersWithSelection() {
        this.ignoreTheNextYearSelection = true;
        this.ignoreTheNextMakeSelection = true;
        this.ignoreTheNextModelSelection = true;
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        java.util.ArrayList<java.lang.String> yearList = new java.util.ArrayList<>();
        yearList.add(context.getString(com.navdy.client.R.string.settings_profile_year_label));
        java.util.ArrayList arrayList = new java.util.ArrayList(this.yearMap.keySet());
        java.util.Collections.sort(arrayList, java.util.Collections.reverseOrder());
        yearList.addAll(arrayList);
        yearList.add(context.getString(com.navdy.client.R.string.other));
        this.year.setAdapter(new org.droidparts.adapter.widget.StringSpinnerAdapter(this.year, (java.util.List<java.lang.String>) yearList));
        this.logger.d("setting year list to: " + yearList);
        int yearSelection = yearList.indexOf(this.yearString);
        if (yearSelection >= 0) {
            this.year.setSelection(yearSelection);
        }
        java.util.ArrayList<java.lang.String> makeList = new java.util.ArrayList<>();
        makeList.add(context.getString(com.navdy.client.R.string.settings_profile_make_label));
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.yearString)) {
            java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation>> makeMap = (java.util.HashMap) this.yearMap.get(this.yearString);
            if (makeMap != null) {
                java.util.ArrayList<java.lang.String> makesSorted = new java.util.ArrayList<>(makeMap.keySet());
                java.util.Collections.sort(makesSorted);
                makeList.addAll(makesSorted);
            }
        }
        makeList.add(context.getString(com.navdy.client.R.string.other));
        this.make.setAdapter(new org.droidparts.adapter.widget.StringSpinnerAdapter(this.make, (java.util.List<java.lang.String>) makeList));
        this.logger.d("setting make list to: " + makeList);
        int makeSelection = makeList.indexOf(this.makeString);
        if (makeSelection >= 0) {
            this.make.setSelection(makeSelection);
        }
        java.util.ArrayList<java.lang.String> modelList = new java.util.ArrayList<>();
        modelList.add(context.getString(com.navdy.client.R.string.settings_profile_model_label));
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.yearString) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.makeString)) {
            java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation>> makeMap2 = (java.util.HashMap) this.yearMap.get(this.yearString);
            if (makeMap2 != null) {
                java.util.HashMap<java.lang.String, com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation> modelMap = (java.util.HashMap) makeMap2.get(this.makeString);
                if (modelMap != null) {
                    java.util.ArrayList<java.lang.String> modelsSorted = new java.util.ArrayList<>(modelMap.keySet());
                    java.util.Collections.sort(modelsSorted);
                    modelList.addAll(modelsSorted);
                }
            }
        }
        modelList.add(context.getString(com.navdy.client.R.string.other));
        this.model.setAdapter(new org.droidparts.adapter.widget.StringSpinnerAdapter(this.model, (java.util.List<java.lang.String>) modelList));
        this.logger.d("setting model list to: " + modelList);
        int modelSelection = modelList.indexOf(this.modelString);
        if (modelSelection >= 0) {
            this.model.setSelection(modelSelection);
        }
    }

    public void onSetCarMdInfoClick(android.view.View view) {
        if (this.year == null || this.make == null || this.model == null) {
            this.logger.e("Missing layout element.");
            return;
        }
        android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
        int yearInt = this.year.getSelectedItemPosition();
        int makeInt = this.make.getSelectedItemPosition();
        int modelInt = this.model.getSelectedItemPosition();
        java.lang.String yearString2 = this.year.getSelectedItem().toString();
        java.lang.String makeString2 = this.make.getSelectedItem().toString();
        java.lang.String modelString2 = this.model.getSelectedItem().toString();
        if (yearInt == 0 || yearInt >= this.year.getCount() - 1 || makeInt == 0 || makeInt >= this.make.getCount() - 1 || modelInt == 0 || modelInt >= this.model.getCount() - 1 || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(yearString2) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(makeString2) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(modelString2)) {
            showLongToast(com.navdy.client.R.string.please_enter_car_info, new java.lang.Object[0]);
        } else {
            saveCarInfoAndDownloadObdInfo(customerPrefs, yearString2, makeString2, modelString2, false);
        }
    }

    public void saveCarInfoAndDownloadObdInfo(android.content.SharedPreferences customerPrefs, java.lang.String yearString2, java.lang.String makeString2, java.lang.String modelString2, boolean comesFromManualEntry) {
        showProgressDialog();
        boolean isInFle = com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(getIntent().getStringExtra(EXTRA_NEXT_STEP), INSTALL);
        com.navdy.client.app.tracking.Tracker.resetPhotoAndLocation(customerPrefs);
        saveCarInfo(customerPrefs, yearString2, makeString2, modelString2, comesFromManualEntry, isInFle);
        saveObdInfo(customerPrefs, com.navdy.client.app.ui.firstlaunch.CarMdUtils.getObdLocation(yearString2, makeString2, modelString2, this.yearMap));
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.Anon5(), 1);
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.Anon6(yearString2, makeString2, modelString2), 3);
    }

    public void onBackClick(android.view.View view) {
        finish();
    }

    private void goToNextStep() {
        runOnUiThread(new com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.Anon7(getIntent().getStringExtra(EXTRA_NEXT_STEP)));
    }

    private void showDialogAboutOther() {
        showSimpleDialog(1, getString(com.navdy.client.R.string.manual_entry), getString(com.navdy.client.R.string.manual_entry_dialog_description));
        showManualEntry(null);
    }

    private void initManualEntryScreen() {
        this.yearTv = (android.widget.EditText) findViewById(com.navdy.client.R.id.enter_year);
        this.makeTv = (android.widget.EditText) findViewById(com.navdy.client.R.id.enter_make);
        this.modelTv = (android.widget.EditText) findViewById(com.navdy.client.R.id.enter_model);
        if (this.yearTv == null || this.makeTv == null || this.modelTv == null) {
            this.logger.e("Missing layout element.");
            return;
        }
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.yearString) && com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.makeString) && com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.modelString)) {
            android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
            this.yearString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "");
            this.makeString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "");
            this.modelString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
        }
        this.yearTv.setText(this.yearString);
        this.makeTv.setText(this.makeString);
        this.modelTv.setText(this.modelString);
        this.isInSelectorMode = false;
    }

    public void onSetCarInfoClick(android.view.View view) {
        android.widget.EditText year2 = (android.widget.EditText) findViewById(com.navdy.client.R.id.enter_year);
        android.widget.EditText make2 = (android.widget.EditText) findViewById(com.navdy.client.R.id.enter_make);
        android.widget.EditText model2 = (android.widget.EditText) findViewById(com.navdy.client.R.id.enter_model);
        if (year2 == null || make2 == null || model2 == null) {
            this.logger.e("Missing layout element.");
            return;
        }
        java.lang.String yearString2 = year2.getText().toString();
        java.lang.String makeString2 = make2.getText().toString();
        java.lang.String modelString2 = model2.getText().toString();
        if (!fieldsAreValid(yearString2, makeString2, modelString2)) {
            showLongToast(com.navdy.client.R.string.please_enter_car_info, new java.lang.Object[0]);
            return;
        }
        saveCarInfoAndDownloadObdInfo(com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences(), yearString2, makeString2, modelString2, true);
    }

    public boolean fieldsAreValid(java.lang.String yearString2, java.lang.String makeString2, java.lang.String modelString2) {
        return !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(yearString2) && yearString2.matches("[12][0-9]{3}") && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(makeString2) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(modelString2);
    }

    public static void saveCarInfo(@android.support.annotation.NonNull android.content.SharedPreferences customerPrefs, @android.support.annotation.NonNull java.lang.String yearString2, @android.support.annotation.NonNull java.lang.String makeString2, @android.support.annotation.NonNull java.lang.String modelString2, boolean comesFromManualEntry, boolean isInFle) {
        java.lang.String oldYearString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "");
        java.lang.String oldMakeString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "");
        java.lang.String oldModelString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
        if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(oldYearString, yearString2) || !com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(oldMakeString, makeString2) || !com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(oldModelString, modelString2)) {
            sLogger.v("Resetting car md data.");
            com.navdy.client.app.tracking.Tracker.resetPhotoAndLocation(customerPrefs);
            java.util.HashMap<java.lang.String, java.lang.String> attributes = new java.util.HashMap<>(3);
            attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_YEAR, yearString2);
            attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_MAKE, makeString2);
            attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_MODEL, modelString2);
            attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.DURING_FLE, isInFle ? "True" : "False");
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.CAR_INFO_CHANGED, attributes);
        }
        long serial = customerPrefs.getLong(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.SERIAL_NUM, com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.SERIAL_NUM_DEFAULT.longValue());
        java.lang.String yearString3 = yearString2.trim();
        java.lang.String makeString3 = makeString2.trim();
        java.lang.String modelString3 = modelString2.trim();
        customerPrefs.edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, yearString3).putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, makeString3).putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, modelString3).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MANUAL_ENTRY, comesFromManualEntry).putLong(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.SERIAL_NUM, 1 + serial).apply();
        com.navdy.client.app.framework.LocalyticsManager.setProfileAttribute(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_YEAR, yearString3);
        com.navdy.client.app.framework.LocalyticsManager.setProfileAttribute(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_MAKE, makeString3);
        com.navdy.client.app.framework.LocalyticsManager.setProfileAttribute(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_MODEL, modelString3);
        com.navdy.client.app.framework.LocalyticsManager.setProfileAttribute(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_MANUAL_ENTRY, java.lang.Boolean.toString(comesFromManualEntry));
    }

    public static void saveObdInfo(@android.support.annotation.NonNull android.content.SharedPreferences customerPrefs, @android.support.annotation.Nullable com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation obdLocation) {
        if (obdLocation == null) {
            com.navdy.client.app.tracking.Tracker.resetPhotoAndLocation(customerPrefs);
        } else {
            customerPrefs.edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_OBD_LOCATION_NOTE, obdLocation.note).putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_OBD_LOCATION_ACCESS_NOTE, obdLocation.accessNote).putInt(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_OBD_LOCATION_NUM, obdLocation.location).apply();
        }
    }
}
