package com.navdy.client.app.ui.firstlaunch;

public class InstallPagerAdapter extends android.support.v4.app.FragmentStatePagerAdapter {
    @android.support.annotation.LayoutRes
    private static final int[] MEDIUM_TALL_MOUNT_FLOW = {com.navdy.client.R.layout.fle_install_overview, com.navdy.client.R.layout.fle_install_medium_or_tall_mount, com.navdy.client.R.layout.fle_install_lens_check, com.navdy.client.R.layout.fle_install_locate_obd, com.navdy.client.R.layout.fle_install_tidying_up, com.navdy.client.R.layout.fle_install_turn_on, com.navdy.client.R.layout.fle_install_dial};
    @android.support.annotation.LayoutRes
    private static final int[] SHORT_MOUNT_FLOW = {com.navdy.client.R.layout.fle_install_overview, com.navdy.client.R.layout.fle_install_short_mount, com.navdy.client.R.layout.fle_install_lens_check, com.navdy.client.R.layout.fle_install_secure_mount, com.navdy.client.R.layout.fle_install_locate_obd, com.navdy.client.R.layout.fle_install_tidying_up, com.navdy.client.R.layout.fle_install_turn_on, com.navdy.client.R.layout.fle_install_dial};
    static final int STEP_LENS_CHECK = 2;
    private static final int STEP_MEDIUM_TALL_MOUNT_LOCATE_OBD = 3;
    private static final int STEP_SHORT_MOUNT_LOCATE_OBD = 4;
    private int[] currentFlow;
    private boolean hasPickedMount;
    private final com.navdy.service.library.log.Logger logger;
    private final android.content.SharedPreferences sharedPreferences;
    private boolean weHaveCarInfo;

    InstallPagerAdapter(android.support.v4.app.FragmentManager manager) {
        super(manager);
        this.logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.firstlaunch.InstallPagerAdapter.class);
        this.currentFlow = SHORT_MOUNT_FLOW;
        this.hasPickedMount = false;
        this.weHaveCarInfo = com.navdy.client.app.tracking.Tracker.weHaveCarInfo();
        this.sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        this.currentFlow = getInstallFlow(getCurrentMountType());
        notifyDataSetChanged();
    }

    int getLocateObdStepIndex() {
        if (getCurrentMountType() == com.navdy.client.app.framework.models.MountInfo.MountType.SHORT) {
            return 4;
        }
        return 3;
    }

    public int getCount() {
        boolean hasSkippedInstall = false;
        if (this.sharedPreferences != null) {
            hasSkippedInstall = this.sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HAS_SKIPPED_INSTALL, false);
        }
        if (!this.hasPickedMount && !hasSkippedInstall) {
            return 3;
        }
        if (this.weHaveCarInfo || hasSkippedInstall) {
            return this.currentFlow.length;
        }
        return getLocateObdStepIndex() + 1;
    }

    static int getStepForCurrentPosition(int currentPosition, com.navdy.client.app.framework.models.MountInfo.MountType currentMountType) {
        if (currentMountType == com.navdy.client.app.framework.models.MountInfo.MountType.SHORT) {
            return SHORT_MOUNT_FLOW[currentPosition];
        }
        return MEDIUM_TALL_MOUNT_FLOW[currentPosition];
    }

    static int getPositionForStep(int step, int[] currentFlow2) {
        if (currentFlow2 != null) {
            for (int i = 0; i < currentFlow2.length; i++) {
                if (step == currentFlow2[i]) {
                    return i;
                }
            }
        }
        return 0;
    }

    @android.support.annotation.NonNull
    public com.navdy.client.app.ui.firstlaunch.InstallCardFragment getItem(int position) {
        int layout = this.currentFlow[position];
        switch (layout) {
            case com.navdy.client.R.layout.fle_install_lens_check /*2130903153*/:
                com.navdy.client.app.ui.firstlaunch.InstallCardFragment f = new com.navdy.client.app.ui.firstlaunch.InstallLensCheckFragment();
                f.setMountType(getCurrentMountType());
                return f;
            case com.navdy.client.R.layout.fle_install_locate_obd /*2130903154*/:
                return new com.navdy.client.app.ui.firstlaunch.InstallLocateObdFragment();
            case com.navdy.client.R.layout.fle_install_medium_or_tall_mount /*2130903155*/:
            case com.navdy.client.R.layout.fle_install_mounts /*2130903156*/:
            case com.navdy.client.R.layout.fle_install_short_mount /*2130903159*/:
                com.navdy.client.app.ui.firstlaunch.InstallCardFragment f2 = new com.navdy.client.app.ui.firstlaunch.InstallMountFragment();
                f2.setMountType(getCurrentMountType());
                return f2;
            default:
                com.navdy.client.app.ui.firstlaunch.InstallCardFragment f3 = new com.navdy.client.app.ui.firstlaunch.InstallCardFragment();
                f3.setLayoutId(layout);
                f3.setMountType(getCurrentMountType());
                return f3;
        }
    }

    public void destroyItem(android.view.ViewGroup container, int position, java.lang.Object object) {
        super.destroyItem(container, position, object);
        this.logger.v("Destroying item at position " + position);
    }

    void setHasPickedMount(boolean hasPickedMount2) {
        this.hasPickedMount = hasPickedMount2;
        notifyDataSetChanged();
    }

    com.navdy.client.app.framework.models.MountInfo.MountType getCurrentMountType() {
        return getCurrentMountType(this.sharedPreferences);
    }

    static com.navdy.client.app.framework.models.MountInfo.MountType getCurrentMountType(android.content.SharedPreferences sharedPreferences2) {
        if (sharedPreferences2 != null) {
            try {
                return com.navdy.client.app.framework.models.MountInfo.MountType.getMountTypeForValue(sharedPreferences2.getString(com.navdy.client.app.ui.settings.SettingsConstants.MOUNT_TYPE, com.navdy.client.app.framework.models.MountInfo.MountType.SHORT.getValue()));
            } catch (java.lang.ClassCastException e) {
            }
        }
        return com.navdy.client.app.framework.models.MountInfo.MountType.SHORT;
    }

    void setCurrentMountType(com.navdy.client.app.framework.models.MountInfo.MountType currentMountType) {
        if (this.sharedPreferences != null) {
            this.sharedPreferences.edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.MOUNT_TYPE, currentMountType.getValue()).apply();
        }
        this.currentFlow = getInstallFlow(currentMountType);
        setHasPickedMount(true);
    }

    static int[] getInstallFlow(com.navdy.client.app.framework.models.MountInfo.MountType mountType) {
        if (mountType == com.navdy.client.app.framework.models.MountInfo.MountType.SHORT) {
            return SHORT_MOUNT_FLOW;
        }
        return MEDIUM_TALL_MOUNT_FLOW;
    }

    public void notifyDataSetChanged() {
        this.weHaveCarInfo = com.navdy.client.app.tracking.Tracker.weHaveCarInfo();
        super.notifyDataSetChanged();
    }

    java.lang.String getScreenAtPosition(int position) {
        return getItem(position).getScreen();
    }

    boolean isOnStepBeforeCablePicker(int currentItem) {
        return currentItem == getLocateObdStepIndex() + -1;
    }

    boolean isOnStepAfterCablePicker(int currentItem) {
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.POWER_CABLE_SELECTION, "OBD"), com.navdy.client.app.ui.settings.SettingsConstants.POWER_CABLE_SELECTION_CLA)) {
            if (currentItem == getLocateObdStepIndex() + 1) {
                return true;
            }
            return false;
        } else if (currentItem != getLocateObdStepIndex()) {
            return false;
        } else {
            return true;
        }
    }

    boolean isOnStepAfterLocatingObd(int currentItem) {
        if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.POWER_CABLE_SELECTION, "OBD"), com.navdy.client.app.ui.settings.SettingsConstants.POWER_CABLE_SELECTION_CLA) && currentItem == getLocateObdStepIndex() + 1) {
            return true;
        }
        return false;
    }
}
