package com.navdy.client.app.ui.settings;

public class HFPAudioDelaySettingsActivity extends com.navdy.client.app.ui.base.BaseEditActivity implements android.view.View.OnClickListener {
    @butterknife.InjectView(2131755671)
    android.widget.Button btnPlaySequence;
    @butterknife.InjectView(2131755670)
    com.navdy.client.app.ui.customviews.MultipleChoiceLayout multipleChoiceLayout;
    private java.lang.String playingSequenceStatus;
    @javax.inject.Inject
    android.content.SharedPreferences sharedPreferences;
    @javax.inject.Inject
    com.navdy.client.app.framework.util.TTSAudioRouter ttsAudioRouter;
    private int userPreferredDelayLevel;

    class Anon1 implements com.navdy.client.app.ui.customviews.MultipleChoiceLayout.ChoiceListener {
        Anon1() {
        }

        public void onChoiceSelected(java.lang.String text, int index) {
            com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity.sLogger.d("User selected delay level " + index);
            com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity.this.userPreferredDelayLevel = index;
            com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity.this.ttsAudioRouter.setPreferredHFPDelayLevel(com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity.this.userPreferredDelayLevel);
            com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity.this.somethingChanged = true;
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        com.navdy.client.app.framework.Injector.inject(com.navdy.client.app.NavdyApplication.getAppContext(), this);
        this.playingSequenceStatus = getString(com.navdy.client.R.string.playing_sequence);
        setContentView((int) com.navdy.client.R.layout.hfp_delay_settings_layout);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.speech_delay).build();
        butterknife.ButterKnife.inject((java.lang.Object) this, (android.app.Activity) this);
        this.btnPlaySequence.setOnClickListener(this);
        this.userPreferredDelayLevel = this.sharedPreferences.getInt(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_HFP_DELAY_LEVEL, 1);
        this.multipleChoiceLayout.setSelectedIndex(this.userPreferredDelayLevel);
        this.multipleChoiceLayout.setChoiceListener(new com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity.Anon1());
    }

    public void onClick(android.view.View view) {
        if (view == this.btnPlaySequence) {
            this.ttsAudioRouter.playSpeechDelaySequence();
            com.navdy.client.app.ui.settings.AudioDialogActivity.startAudioStatusActivity(this, this.playingSequenceStatus);
        }
    }

    public static void start(android.content.Context context) {
        context.startActivity(new android.content.Intent(context, com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity.class));
    }

    protected void saveChanges() {
        this.sharedPreferences.edit().putInt(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_HFP_DELAY_LEVEL, this.userPreferredDelayLevel).apply();
    }
}
