package com.navdy.client.app.tracking;

public final class Tracker {
    private static final boolean ENABLED = false;
    private static final boolean VERBOSE = false;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.tracking.Tracker.class);

    public static void tagEvent(java.lang.String eventName) {
    }

    public static void tagEvent(java.lang.String eventName, java.util.Map<java.lang.String, java.lang.String> map) {
    }

    public static void tagScreen(java.lang.String screen) {
    }

    public static void registerUser(java.lang.String email, java.lang.String fullName) {
        com.navdy.client.app.framework.LocalyticsManager.setCustomerEmail(email);
        com.navdy.client.app.framework.LocalyticsManager.setCustomerFullName(fullName);
        android.content.SharedPreferences.Editor editor = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences().edit();
        editor.putString("email", email);
        editor.putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.FULL_NAME, fullName);
        editor.apply();
        com.navdy.client.app.ui.settings.SettingsUtils.incrementDriverProfileSerial();
    }

    public static void saveUserPhotoToInternalStorage(android.graphics.Bitmap photo) {
        saveBitmapToInternalStorage(photo, com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.USER_PHOTO_FILE_NAME);
    }

    public static void saveObdImageToInternalStorage(android.graphics.Bitmap photo) {
        saveBitmapToInternalStorage(photo, com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.OBD_IMAGE_FILE_NAME);
    }

    public static void resetPhotoAndLocation(@android.support.annotation.NonNull android.content.SharedPreferences customerPrefs) {
        customerPrefs.edit().remove(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_OBD_LOCATION_NOTE).remove(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_OBD_LOCATION_ACCESS_NOTE).remove(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_OBD_LOCATION_NUM).apply();
        deleteObdImageFromInternalStorage();
    }

    private static void deleteObdImageFromInternalStorage() {
        deleteImageFromInternalStorage(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.OBD_IMAGE_FILE_NAME);
    }

    private static void deleteImageFromInternalStorage(java.lang.String filename) {
        try {
            com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.app.NavdyApplication.getAppContext().getFilesDir() + java.io.File.pathSeparator + filename);
        } catch (Throwable e) {
            logger.e(e);
        }
    }

    private static void saveBitmapToInternalStorage(android.graphics.Bitmap photo, java.lang.String filename, android.graphics.Bitmap.CompressFormat compressFormat, int quality) {
        if (photo == null) {
            deleteImageFromInternalStorage(filename);
            return;
        }
        java.io.FileOutputStream outputStream = null;
        try {
            outputStream = com.navdy.client.app.NavdyApplication.getAppContext().openFileOutput(filename, 0);
            photo.compress(compressFormat, quality, outputStream);
        } catch (Throwable e) {
            logger.e(e);
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(outputStream);
        }
    }

    public static void saveBitmapToInternalStorage(android.graphics.Bitmap photo, java.lang.String filename) {
        saveBitmapToInternalStorage(photo, filename, android.graphics.Bitmap.CompressFormat.JPEG, 90);
    }

    @android.support.annotation.Nullable
    public static android.graphics.Bitmap getUserProfilePhoto() {
        return getBitmapFromInternalStorage(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.USER_PHOTO_FILE_NAME);
    }

    public static android.graphics.Bitmap getScaledProfilePhoto(int size) {
        try {
            java.io.File file = new java.io.File(com.navdy.client.app.NavdyApplication.getAppContext().getFilesDir() + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.USER_PHOTO_FILE_NAME);
            if (!file.exists()) {
                return null;
            }
            android.graphics.Bitmap bitmap = com.navdy.client.app.framework.util.ImageUtils.getScaledBitmap(file, size, size);
            if (bitmap == null) {
                return null;
            }
            logger.v("PhotoServiceHandler dim=" + size);
            return bitmap;
        } catch (Throwable t) {
            logger.e("PhotoServiceHandler:", t);
            return null;
        }
    }

    @android.support.annotation.Nullable
    public static android.graphics.Bitmap getObdImage() {
        return getBitmapFromInternalStorage(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.OBD_IMAGE_FILE_NAME);
    }

    @android.support.annotation.Nullable
    public static android.graphics.Bitmap getBitmapFromInternalStorage(java.lang.String filename) {
        java.lang.String filename2 = com.navdy.client.app.NavdyApplication.getAppContext().getFilesDir() + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + filename;
        if (!new java.io.File(filename2).exists()) {
            return null;
        }
        boolean z = false;
        try {
            return android.graphics.BitmapFactory.decodeFile(filename2);
        } catch (Throwable e) {
            logger.e(e);
            return z;
        }
    }

    public static void eraseUserProfilePhoto() {
        try {
            new java.io.File(com.navdy.client.app.NavdyApplication.getAppContext().getFilesDir() + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.USER_PHOTO_FILE_NAME).delete();
        } catch (java.lang.Exception e) {
            logger.e("Unable to erase user profile photo.", e);
        }
    }

    public static boolean isUserRegistered() {
        android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
        return !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.FULL_NAME, null)) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(customerPrefs.getString("email", null));
    }

    public static boolean hasUserSkipped() {
        return com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.SKIPPED_SIGNUP, false);
    }

    public static int calculateInSampleSize(android.graphics.BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            int halfHeight = height / 2;
            int halfWidth = width / 2;
            while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public static boolean weHaveCarInfo() {
        android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
        return !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "")) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "")) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, ""));
    }

    public static java.lang.String getUserId() {
        boolean z = false;
        try {
            return com.localytics.android.Localytics.getCustomerId();
        } catch (java.lang.Exception e) {
            android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
            java.lang.String name = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.FULL_NAME, z);
            java.lang.String email = customerPrefs.getString("email", z);
            if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(name) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(email)) {
                return z;
            }
            return name + " <" + email + ">";
        }
    }
}
