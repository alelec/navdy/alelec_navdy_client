package com.navdy.client.app.providers;

public class DbUtils {
    private static boolean areValidParams(android.database.Cursor cursor, java.lang.String columnName) {
        return cursor != null && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(columnName);
    }

    @android.support.annotation.Nullable
    public static java.lang.String getString(@android.support.annotation.Nullable android.database.Cursor cursor, @android.support.annotation.Nullable java.lang.String columnName) {
        if (!areValidParams(cursor, columnName)) {
            return null;
        }
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    public static int getInt(@android.support.annotation.Nullable android.database.Cursor cursor, @android.support.annotation.Nullable java.lang.String columnName) {
        if (!areValidParams(cursor, columnName)) {
            return 0;
        }
        return cursor.getInt(cursor.getColumnIndex(columnName));
    }

    public static long getLong(@android.support.annotation.Nullable android.database.Cursor cursor, @android.support.annotation.Nullable java.lang.String columnName) {
        if (!areValidParams(cursor, columnName)) {
            return 0;
        }
        return cursor.getLong(cursor.getColumnIndex(columnName));
    }

    public static boolean getBoolean(@android.support.annotation.Nullable android.database.Cursor cursor, @android.support.annotation.Nullable java.lang.String columnName) {
        if (areValidParams(cursor, columnName) && cursor.getInt(cursor.getColumnIndex(columnName)) > 0) {
            return true;
        }
        return false;
    }
}
