package com.navdy.client.app.providers;

public class NavdyContentProvider extends android.content.ContentProvider {
    public static final java.lang.String CREATE_DESTINATIONS_TABLE = " CREATE TABLE IF NOT EXISTS destinations ( _id INTEGER PRIMARY KEY, place_id TEXT, last_place_id_refresh INTEGER, displat REAL, displong REAL, navlat REAL, navlong REAL, place_name TEXT NOT NULL, address TEXT NOT NULL, street_number TEXT, street_name TEXT, city TEXT, state TEXT, zip_code TEXT, country TEXT, countryCode TEXT, do_not_suggest INT, last_routed_date INTEGER, favorite_label TEXT, favorite_listing_order INTEGER, is_special INTEGER, place_detail_json TEXT, precision_level INTEGER, type INTEGER, contact_lookup_key TEXT, last_known_contact_id INTEGER, last_contact_lookup INTEGER);";
    public static final java.lang.String CREATE_DESTINATION_CACHE_TABLE = " CREATE TABLE IF NOT EXISTS destination_cache ( _id INTEGER PRIMARY KEY, last_response_date INTEGER, location TEXT, destination_id INTEGER);";
    public static final java.lang.String CREATE_PLAYLISTS_TABLE = " CREATE TABLE IF NOT EXISTS playlists ( playlist_id INTEGER, playlist_name STRING );";
    public static final java.lang.String CREATE_PLAYLIST_MEMBERS_TABLE = " CREATE TABLE IF NOT EXISTS playlist_members ( playlist_id INTEGER, SourceId STRING, artist STRING, album STRING, title STRING );";
    public static final java.lang.String CREATE_SEARCH_HISTORY_TABLE = " CREATE TABLE IF NOT EXISTS search_history ( _id INTEGER PRIMARY KEY, last_searched_on INTEGER, query TEXT);";
    public static final java.lang.String CREATE_TRIPS_TABLE = " CREATE TABLE IF NOT EXISTS trips ( _id INTEGER PRIMARY KEY, trip_number INTEGER, start_time INTEGER, start_time_zone_n_dst INTEGER, start_odometer INTEGER, start_lat REAL, start_long REAL, end_time INTEGER, end_odometer INTEGER, end_lat REAL, end_long REAL, destination_id INTEGER, arrived_at_destination INTEGER);";
    private static java.util.HashMap<java.lang.String, java.lang.String> destinationCacheValues;
    private static java.util.HashMap<java.lang.String, java.lang.String> destinationsValues;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.providers.NavdyContentProvider.class);
    private static java.util.HashMap<java.lang.String, java.lang.String> playlistMembersValues;
    private static java.util.HashMap<java.lang.String, java.lang.String> playlistsValues;
    private static java.util.HashMap<java.lang.String, java.lang.String> searchHistoryValues;
    private static android.database.sqlite.SQLiteDatabase sqlDB;
    private static java.util.HashMap<java.lang.String, java.lang.String> tripsValues;
    public static final android.content.UriMatcher uriMatcher = new android.content.UriMatcher(-1);

    static class Anon1 implements com.navdy.client.app.providers.NavdyContentProvider.CacheQuerier {
        final /* synthetic */ android.database.sqlite.SQLiteDatabase val$db;

        Anon1(android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
            this.val$db = sQLiteDatabase;
        }

        public android.database.Cursor runQuery(java.lang.String[] projection, java.lang.String selection, java.lang.String[] selectionArgs) {
            return this.val$db.query("destination_cache", projection, selection, selectionArgs, null, null, null);
        }
    }

    static class Anon2 implements com.navdy.client.app.providers.NavdyContentProvider.CacheQuerier {
        final /* synthetic */ android.content.ContentResolver val$cr;

        Anon2(android.content.ContentResolver contentResolver) {
            this.val$cr = contentResolver;
        }

        public android.database.Cursor runQuery(java.lang.String[] projection, java.lang.String selection, java.lang.String[] selectionArgs) {
            return this.val$cr.query(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATION_CACHE_CONTENT_URI, projection, selection, selectionArgs, null, null);
        }
    }

    private interface CacheQuerier {
        android.database.Cursor runQuery(java.lang.String[] strArr, java.lang.String str, java.lang.String[] strArr2);
    }

    private static class DatabaseHelper extends android.database.sqlite.SQLiteOpenHelper {
        android.content.Context context;

        DatabaseHelper(android.content.Context context2) {
            super(context2, com.navdy.client.app.providers.NavdyContentProviderConstants.DB_NAME, null, 19);
            this.context = context2;
        }

        public void onCreate(android.database.sqlite.SQLiteDatabase db) {
            db.execSQL(com.navdy.client.app.providers.NavdyContentProvider.CREATE_DESTINATIONS_TABLE);
            db.execSQL(com.navdy.client.app.providers.NavdyContentProvider.CREATE_TRIPS_TABLE);
            db.execSQL(com.navdy.client.app.providers.NavdyContentProvider.CREATE_SEARCH_HISTORY_TABLE);
            db.execSQL(com.navdy.client.app.providers.NavdyContentProvider.CREATE_DESTINATION_CACHE_TABLE);
            db.execSQL(com.navdy.client.app.providers.NavdyContentProvider.CREATE_PLAYLISTS_TABLE);
            db.execSQL(com.navdy.client.app.providers.NavdyContentProvider.CREATE_PLAYLIST_MEMBERS_TABLE);
        }

        public void onUpgrade(android.database.sqlite.SQLiteDatabase db, int oldVersion, int newVersion) {
            com.navdy.client.app.providers.NavdyContentProvider.logger.v("onUpgrade oldVersion: " + oldVersion + " newVersion: " + newVersion);
            if (oldVersion <= 7) {
                db.execSQL("ALTER TABLE destinations ADD COLUMN place_detail_json TEXT");
            }
            if (oldVersion <= 8) {
                db.execSQL(com.navdy.client.app.providers.NavdyContentProvider.CREATE_SEARCH_HISTORY_TABLE);
            }
            if (oldVersion <= 9) {
                db.execSQL(com.navdy.client.app.providers.NavdyContentProvider.CREATE_DESTINATION_CACHE_TABLE);
                db.execSQL("ALTER TABLE destinations ADD COLUMN precision_level INTEGER DEFAULT " + com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN.getValue());
            }
            if (oldVersion <= 10) {
                db.execSQL("ALTER TABLE destinations ADD COLUMN type INTEGER DEFAULT " + com.navdy.client.app.framework.models.Destination.Type.UNKNOWN.getValue());
                com.navdy.client.app.providers.NavdyContentProvider.goThroughAllDestinationBlobsAndSetType(db);
            }
            if (oldVersion <= 11) {
                db.execSQL("ALTER TABLE trips ADD COLUMN start_time_zone_n_dst INTEGER DEFAULT " + com.navdy.client.app.framework.util.SystemUtils.getTimeZoneAndDaylightSavingOffset(java.lang.Long.valueOf(new java.util.Date().getTime())));
            }
            if (oldVersion <= 12) {
                db.execSQL(com.navdy.client.app.providers.NavdyContentProvider.CREATE_PLAYLISTS_TABLE);
                db.execSQL(com.navdy.client.app.providers.NavdyContentProvider.CREATE_PLAYLIST_MEMBERS_TABLE);
            }
            if (oldVersion <= 13) {
                db.execSQL("ALTER TABLE destinations ADD COLUMN countryCode TEXT");
                com.navdy.client.app.providers.NavdyContentProvider.goThroughAllDestinationBlobsAndSetCountryCode(db);
            }
            if (oldVersion <= 14) {
                db.execSQL("ALTER TABLE destinations ADD COLUMN contact_lookup_key TEXT");
                db.execSQL("ALTER TABLE destinations ADD COLUMN last_known_contact_id INTEGER");
                db.execSQL("ALTER TABLE destinations ADD COLUMN last_contact_lookup INTEGER");
            }
            if (oldVersion <= 18) {
                com.navdy.client.app.providers.NavdyContentProvider.goThroughAllFavoriteDestinationsAndLinkContacts(db, this.context);
            }
        }

        public void onDowngrade(android.database.sqlite.SQLiteDatabase db, int oldVersion, int newVersion) {
            dumpAndRecreateDb(db);
        }

        private void dumpAndRecreateDb(android.database.sqlite.SQLiteDatabase db) {
            db.execSQL("DROP TABLE IF EXISTS destinations");
            db.execSQL("DROP TABLE IF EXISTS trips");
            db.execSQL("DROP TABLE IF EXISTS search_history");
            db.execSQL("DROP TABLE IF EXISTS destination_cache");
            db.execSQL("DROP TABLE IF EXISTS playlists");
            db.execSQL("DROP TABLE IF EXISTS playlist_members");
            onCreate(db);
        }
    }

    public interface QueryResultCallback {
        void onQueryCompleted(int i, @android.support.annotation.Nullable android.net.Uri uri);
    }

    static {
        uriMatcher.addURI(com.navdy.client.app.providers.NavdyContentProviderConstants.PROVIDER_NAME, "destinations", 1);
        uriMatcher.addURI(com.navdy.client.app.providers.NavdyContentProviderConstants.PROVIDER_NAME, "trips", 2);
        uriMatcher.addURI(com.navdy.client.app.providers.NavdyContentProviderConstants.PROVIDER_NAME, "search_history", 3);
        uriMatcher.addURI(com.navdy.client.app.providers.NavdyContentProviderConstants.PROVIDER_NAME, "destination_cache", 4);
        uriMatcher.addURI(com.navdy.client.app.providers.NavdyContentProviderConstants.PROVIDER_NAME, "playlists", 5);
        uriMatcher.addURI(com.navdy.client.app.providers.NavdyContentProviderConstants.PROVIDER_NAME, "playlist_members", 6);
    }

    public java.lang.String getType(@android.support.annotation.NonNull android.net.Uri uri) {
        switch (uriMatcher.match(uri)) {
            case 1:
                return "vnd.android.cursor.dir/destinations";
            case 2:
                return "vnd.android.cursor.dir/trips";
            case 3:
                return "vnd.android.cursor.dir/search_history";
            case 4:
                return "vnd.android.cursor.dir/destination_cache";
            case 5:
                return "vnd.android.cursor.dir/playlists";
            case 6:
                return "vnd.android.cursor.dir/playlist_members";
            default:
                throw new java.lang.IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @android.support.annotation.NonNull
    private java.lang.String getTableName(@android.support.annotation.NonNull android.net.Uri uri) {
        switch (uriMatcher.match(uri)) {
            case 1:
                return "destinations";
            case 2:
                return "trips";
            case 3:
                return "search_history";
            case 4:
                return "destination_cache";
            case 5:
                return "playlists";
            case 6:
                return "playlist_members";
            default:
                throw new java.lang.IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @android.support.annotation.NonNull
    private java.lang.String getTableNameForQuery(@android.support.annotation.NonNull android.net.Uri uri) {
        switch (uriMatcher.match(uri)) {
            case 1:
                return "destinations";
            case 2:
                return "trips";
            case 3:
                return "search_history";
            case 4:
                return "destination_cache";
            case 5:
                return "playlists";
            case 6:
                return "playlist_members";
            default:
                throw new java.lang.IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @android.support.annotation.NonNull
    private android.net.Uri getContentUri(@android.support.annotation.NonNull android.net.Uri baseUri) {
        switch (uriMatcher.match(baseUri)) {
            case 1:
                return com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI;
            case 2:
                return com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_CONTENT_URI;
            case 3:
                return com.navdy.client.app.providers.NavdyContentProviderConstants.SEARCH_HISTORY_CONTENT_URI;
            case 4:
                return com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATION_CACHE_CONTENT_URI;
            case 5:
                return com.navdy.client.app.providers.NavdyContentProviderConstants.PLAYLISTS_CONTENT_URI;
            case 6:
                return com.navdy.client.app.providers.NavdyContentProviderConstants.PLAYLIST_MEMBERS_CONTENT_URI;
            default:
                throw new java.lang.IllegalArgumentException("Unknown URI: " + baseUri);
        }
    }

    @android.support.annotation.NonNull
    private java.util.HashMap<java.lang.String, java.lang.String> getValues(@android.support.annotation.NonNull android.net.Uri uri) {
        switch (uriMatcher.match(uri)) {
            case 1:
                return destinationsValues;
            case 2:
                return tripsValues;
            case 3:
                return searchHistoryValues;
            case 4:
                return destinationCacheValues;
            case 5:
                return playlistsValues;
            case 6:
                return playlistMembersValues;
            default:
                throw new java.lang.IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    public static synchronized android.database.sqlite.SQLiteDatabase getSqlDb(android.content.Context context) {
        android.database.sqlite.SQLiteDatabase sQLiteDatabase;
        synchronized (com.navdy.client.app.providers.NavdyContentProvider.class) {
            if (sqlDB == null) {
                sqlDB = new com.navdy.client.app.providers.NavdyContentProvider.DatabaseHelper(context).getWritableDatabase();
            }
            sQLiteDatabase = sqlDB;
        }
        return sQLiteDatabase;
    }

    public boolean onCreate() {
        return getSqlDb(getContext()) != null;
    }

    public android.database.Cursor query(@android.support.annotation.NonNull android.net.Uri uri, java.lang.String[] projection, java.lang.String selection, java.lang.String[] selectionArgs, java.lang.String sortOrder) {
        android.database.sqlite.SQLiteQueryBuilder queryBuilder = new android.database.sqlite.SQLiteQueryBuilder();
        queryBuilder.setTables(getTableNameForQuery(uri));
        queryBuilder.setProjectionMap(getValues(uri));
        android.database.Cursor cursor = queryBuilder.query(sqlDB, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContentResolverSafely(), uri);
        return cursor;
    }

    private android.content.ContentResolver getContentResolverSafely() {
        android.content.Context context = getContext();
        if (context == null) {
            return null;
        }
        return context.getContentResolver();
    }

    private void notifyChangeSafely(android.net.Uri _uri) {
        android.content.ContentResolver contentResolver = getContentResolverSafely();
        if (contentResolver != null) {
            contentResolver.notifyChange(_uri, null);
        }
    }

    public android.net.Uri insert(@android.support.annotation.NonNull android.net.Uri uri, android.content.ContentValues values) {
        long rowId = sqlDB.insert(getTableName(uri), null, values);
        if (rowId > 0) {
            android.net.Uri uri_to_new_data = android.content.ContentUris.withAppendedId(getContentUri(uri), rowId);
            notifyChangeSafely(uri_to_new_data);
            return uri_to_new_data;
        }
        logger.e("Insert failed !");
        return null;
    }

    public int delete(@android.support.annotation.NonNull android.net.Uri uri, java.lang.String selection, java.lang.String[] selectionArgs) {
        int rowsDeleted = sqlDB.delete(getTableName(uri), selection, selectionArgs);
        notifyChangeSafely(uri);
        return rowsDeleted;
    }

    public int update(@android.support.annotation.NonNull android.net.Uri uri, android.content.ContentValues values, java.lang.String selection, java.lang.String[] selectionArgs) {
        int rowsUpdated = sqlDB.update(getTableName(uri), values, selection, selectionArgs);
        notifyChangeSafely(uri);
        return rowsUpdated;
    }

    private static void goThroughAllDestinationBlobsAndSetType(android.database.sqlite.SQLiteDatabase db) {
        android.database.Cursor cursor = null;
        try {
            android.database.sqlite.SQLiteDatabase sQLiteDatabase = db;
            cursor = sQLiteDatabase.query("destinations", new java.lang.String[]{"_id", com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PLACE_DETAIL_JSON}, "place_detail_json <> ''", null, null, null, null);
            if (cursor == null) {
                logger.e("Unable to get a destination cursor for destination type upgrade!");
                com.navdy.service.library.util.IOUtils.closeStream(cursor);
                return;
            }
            while (cursor.moveToNext()) {
                int id = com.navdy.client.app.providers.DbUtils.getInt(cursor, "_id");
                com.navdy.client.app.framework.models.Destination.Type destinationType = com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult.parseTypes(new org.json.JSONObject(com.navdy.client.app.providers.DbUtils.getString(cursor, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PLACE_DETAIL_JSON)).getJSONArray("types"));
                android.content.ContentValues contentValues = new android.content.ContentValues();
                contentValues.put("type", java.lang.Integer.valueOf(destinationType.getValue()));
                android.database.sqlite.SQLiteDatabase sQLiteDatabase2 = db;
                sQLiteDatabase2.update("destinations", contentValues, "_id=?", new java.lang.String[]{java.lang.String.valueOf(id)});
            }
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
        } catch (org.json.JSONException e) {
            e.printStackTrace();
        } catch (Throwable th) {
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
            throw th;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: CFG modification limit reached, blocks count: 141 */
    public static void goThroughAllDestinationBlobsAndSetCountryCode(android.database.sqlite.SQLiteDatabase db) {
        int id;
        java.lang.String countryCode;
        android.database.Cursor cursor = null;
        try {
            android.database.sqlite.SQLiteDatabase sQLiteDatabase = db;
            cursor = sQLiteDatabase.query("destinations", new java.lang.String[]{"_id", com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PLACE_DETAIL_JSON}, "place_detail_json <> ''", null, null, null, null);
            if (cursor == null) {
                logger.e("Unable to get a destination cursor for country code upgrade!");
                com.navdy.service.library.util.IOUtils.closeStream(cursor);
            } else {
                do {
                    if (cursor.moveToNext()) {
                        id = com.navdy.client.app.providers.DbUtils.getInt(cursor, "_id");
                        org.json.JSONObject jSONObject = new org.json.JSONObject(com.navdy.client.app.providers.DbUtils.getString(cursor, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PLACE_DETAIL_JSON));
                        org.json.JSONArray addressComponents = jSONObject.optJSONArray("address_components");
                        countryCode = null;
                        if (addressComponents != null) {
                            for (int aci = 0; aci < addressComponents.length(); aci++) {
                                try {
                                    org.json.JSONObject obj = addressComponents.getJSONObject(aci);
                                    org.json.JSONArray types = obj.getJSONArray("types");
                                    int ti = 0;
                                    while (true) {
                                        if (ti < types.length()) {
                                            if (com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_COUNTRY.equals(types.getString(ti))) {
                                                countryCode = obj.getString("short_name");
                                                break;
                                            }
                                            ti++;
                                        }
                                    }
                                } catch (java.lang.Exception e) {
                                }
                            }
                        }
                    } else {
                        com.navdy.service.library.util.IOUtils.closeStream(cursor);
                    }
                } while (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(countryCode));
                android.content.ContentValues contentValues = new android.content.ContentValues();
                contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_COUNTRY_CODE, countryCode);
                android.database.sqlite.SQLiteDatabase sQLiteDatabase2 = db;
                sQLiteDatabase2.update("destinations", contentValues, "_id=?", new java.lang.String[]{java.lang.String.valueOf(id)});
            }
        } catch (org.json.JSONException e2) {
            e2.printStackTrace();
        } catch (Throwable th) {
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
            throw th;
        }
    }

    private static void goThroughAllFavoriteDestinationsAndLinkContacts(@android.support.annotation.NonNull android.database.sqlite.SQLiteDatabase db, @android.support.annotation.NonNull android.content.Context context) {
        com.navdy.client.app.framework.models.ContactModel matchedContact;
        logger.d("goThroughAllFavoriteDestinationsAndLinkContacts");
        java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinations = getContactsWithoutLookupKey(db);
        if (destinations == null || destinations.size() <= 0) {
            logger.d("Found no contact destination without contact lookup key");
            return;
        }
        android.content.ContentResolver contentResolver = context.getContentResolver();
        java.util.Iterator it = destinations.iterator();
        while (it.hasNext()) {
            com.navdy.client.app.framework.models.Destination d = (com.navdy.client.app.framework.models.Destination) it.next();
            java.util.List<com.navdy.client.app.framework.models.ContactModel> contacts = com.navdy.client.app.framework.util.ContactsManager.getContactsPreferablyWithPhoneNumber(d.name, contentResolver);
            if (contacts == null || contacts.size() == 0) {
                logger.d("Looking up " + d.name + " returned 0 contacts");
            } else {
                logger.d("Looking up " + d.name + " returned " + contacts.size() + " contacts");
                if (contacts.size() == 1) {
                    matchedContact = (com.navdy.client.app.framework.models.ContactModel) contacts.get(0);
                } else {
                    matchedContact = getMatchingContact(d.rawAddressNotForDisplay, d.streetNumber, d.streetName, contentResolver, contacts, getCacheKeysForDestinationId(db, d.id));
                }
                if (matchedContact != null) {
                    logger.d("Matching contact for " + d.name + " is " + matchedContact);
                    android.content.ContentValues contentValues = new android.content.ContentValues();
                    contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTACT_LOOKUP_KEY, matchedContact.lookupKey);
                    contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LAST_KNOWN_CONTACT_ID, java.lang.Long.valueOf(matchedContact.id));
                    contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LAST_CONTACT_LOOKUP, java.lang.Long.valueOf(new java.util.Date().getTime()));
                    db.update("destinations", contentValues, "_id=?", new java.lang.String[]{java.lang.String.valueOf(d.id)});
                } else {
                    logger.d("No matching contact for " + d.name);
                }
            }
        }
    }

    @android.support.annotation.Nullable
    private static java.util.ArrayList<com.navdy.client.app.framework.models.Destination> getContactsWithoutLookupKey(@android.support.annotation.NonNull android.database.sqlite.SQLiteDatabase db) {
        java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinations = null;
        android.database.Cursor cursor = null;
        try {
            android.database.sqlite.SQLiteDatabase sQLiteDatabase = db;
            cursor = sQLiteDatabase.query("destinations", new java.lang.String[]{"_id", com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAME, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_ADDRESS, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_STREET_NUMBER, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_STREET_NAME}, "type = ? AND contact_lookup_key is null", new java.lang.String[]{java.lang.String.valueOf(com.navdy.client.app.framework.models.Destination.Type.CONTACT.getValue())}, null, null, null);
            if (cursor == null || cursor.getCount() <= 0) {
                logger.e("Unable to get a destination cursor for contact linking upgrade!");
                com.navdy.service.library.util.IOUtils.closeStream(cursor);
                return null;
            }
            logger.d("cursor has " + cursor.getCount() + " destination(s)");
            java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinations2 = new java.util.ArrayList<>(cursor.getCount());
            while (cursor.moveToNext()) {
                try {
                    com.navdy.client.app.framework.models.Destination d = new com.navdy.client.app.framework.models.Destination();
                    d.id = com.navdy.client.app.providers.DbUtils.getInt(cursor, "_id");
                    d.name = com.navdy.client.app.providers.DbUtils.getString(cursor, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAME);
                    d.rawAddressNotForDisplay = com.navdy.client.app.providers.DbUtils.getString(cursor, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_ADDRESS);
                    d.streetNumber = com.navdy.client.app.providers.DbUtils.getString(cursor, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_STREET_NUMBER);
                    d.streetName = com.navdy.client.app.providers.DbUtils.getString(cursor, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_STREET_NAME);
                    destinations2.add(d);
                } catch (java.lang.Exception e) {
                    e = e;
                    destinations = destinations2;
                    try {
                        logger.e("Something went wrong while trying to get a destination cursor for contact linking upgrade!", e);
                        com.navdy.service.library.util.IOUtils.closeStream(cursor);
                        return destinations;
                    } catch (Throwable th) {
                        th = th;
                        com.navdy.service.library.util.IOUtils.closeStream(cursor);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    java.util.ArrayList<com.navdy.client.app.framework.models.Destination> arrayList = destinations2;
                    com.navdy.service.library.util.IOUtils.closeStream(cursor);
                    throw th;
                }
            }
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
            destinations = destinations2;
            return destinations;
        } catch (java.lang.Exception e2) {
            e = e2;
            logger.e("Something went wrong while trying to get a destination cursor for contact linking upgrade!", e);
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
            return destinations;
        }
    }

    @android.support.annotation.Nullable
    @android.support.annotation.CheckResult
    public static java.util.ArrayList<java.lang.String> getCacheKeysForDestinationId(int destinationId) {
        return getCacheKeysForDestinationId(com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver(), destinationId);
    }

    @android.support.annotation.Nullable
    @android.support.annotation.CheckResult
    private static java.util.ArrayList<java.lang.String> getCacheKeysForDestinationId(@android.support.annotation.NonNull android.database.sqlite.SQLiteDatabase db, int destinationId) {
        return getCacheKeysForDestinationId((com.navdy.client.app.providers.NavdyContentProvider.CacheQuerier) new com.navdy.client.app.providers.NavdyContentProvider.Anon1(db), destinationId);
    }

    @android.support.annotation.Nullable
    @android.support.annotation.CheckResult
    private static java.util.ArrayList<java.lang.String> getCacheKeysForDestinationId(@android.support.annotation.NonNull android.content.ContentResolver cr, int destinationId) {
        return getCacheKeysForDestinationId((com.navdy.client.app.providers.NavdyContentProvider.CacheQuerier) new com.navdy.client.app.providers.NavdyContentProvider.Anon2(cr), destinationId);
    }

    @android.support.annotation.Nullable
    @android.support.annotation.CheckResult
    private static java.util.ArrayList<java.lang.String> getCacheKeysForDestinationId(@android.support.annotation.NonNull com.navdy.client.app.providers.NavdyContentProvider.CacheQuerier cq, int destinationId) {
        if (destinationId < 0) {
            logger.e("CACHE: Can't lookup entries for a negative ID!");
            return null;
        }
        java.util.ArrayList<java.lang.String> addresses = null;
        android.database.Cursor cursor = null;
        try {
            java.lang.String str = "destination_id=?";
            cursor = cq.runQuery(new java.lang.String[]{"location"}, "destination_id=?", new java.lang.String[]{java.lang.String.valueOf(destinationId)});
            if (cursor != null) {
                java.util.ArrayList<java.lang.String> addresses2 = new java.util.ArrayList<>(cursor.getCount());
                while (cursor.moveToNext()) {
                    try {
                        addresses2.add(com.navdy.client.app.providers.DbUtils.getString(cursor, "location"));
                    } catch (java.lang.Exception e) {
                        e = e;
                        addresses = addresses2;
                        try {
                            logger.e("CACHE: Something went wrong while trying to query the HERE cache!", e);
                            com.navdy.service.library.util.IOUtils.closeStream(cursor);
                            return addresses;
                        } catch (Throwable th) {
                            th = th;
                            com.navdy.service.library.util.IOUtils.closeStream(cursor);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        java.util.ArrayList<java.lang.String> arrayList = addresses2;
                        com.navdy.service.library.util.IOUtils.closeStream(cursor);
                        throw th;
                    }
                }
                addresses = addresses2;
            }
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
            return addresses;
        } catch (java.lang.Exception e2) {
            e = e2;
            logger.e("CACHE: Something went wrong while trying to query the HERE cache!", e);
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
            return addresses;
        }
    }

    @android.support.annotation.Nullable
    private static com.navdy.client.app.framework.models.ContactModel getMatchingContact(@android.support.annotation.Nullable java.lang.String address, @android.support.annotation.Nullable java.lang.String streetNumber, @android.support.annotation.Nullable java.lang.String streetName, @android.support.annotation.NonNull android.content.ContentResolver contentResolver, @android.support.annotation.NonNull java.util.List<com.navdy.client.app.framework.models.ContactModel> contacts, @android.support.annotation.Nullable java.util.ArrayList<java.lang.String> cachedAddresses) {
        for (com.navdy.client.app.framework.models.ContactModel contact : contacts) {
            java.util.List<com.navdy.client.app.framework.models.Address> addresses = com.navdy.client.app.framework.util.ContactsManager.getAddressFromLookupKey(contentResolver, contact.lookupKey, contact.id);
            contact.addAddresses(addresses);
            java.util.Iterator it = addresses.iterator();
            while (true) {
                if (it.hasNext()) {
                    com.navdy.client.app.framework.models.Address contactAddress = (com.navdy.client.app.framework.models.Address) it.next();
                    if (isMatchingContact(address, streetNumber, streetName, contactAddress)) {
                        return contact;
                    }
                    if (cachedAddresses != null) {
                        java.lang.String fullAddress = cleanUpAddressForComparison(contactAddress.getFullAddress());
                        java.lang.String contactStreet = cleanUpAddressForComparison(contactAddress.street);
                        java.util.Iterator it2 = cachedAddresses.iterator();
                        while (it2.hasNext()) {
                            if (isSomewhatMatchingAddress(cleanUpAddressForComparison((java.lang.String) it2.next()), fullAddress, contactStreet)) {
                                return contact;
                            }
                        }
                        continue;
                    }
                }
            }
        }
        return null;
    }

    private static boolean isMatchingContact(@android.support.annotation.Nullable java.lang.String address, @android.support.annotation.Nullable java.lang.String streetNumber, @android.support.annotation.Nullable java.lang.String streetName, @android.support.annotation.NonNull com.navdy.client.app.framework.models.Address contactAddress) {
        java.lang.String fullAddress = contactAddress.getFullAddress();
        java.lang.String contactStreet = contactAddress.street;
        logger.d("Comparing " + address + " with " + fullAddress);
        logger.d("Comparing " + streetNumber + " " + streetName + " with " + contactStreet);
        java.lang.String address2 = cleanUpAddressForComparison(address);
        java.lang.String fullAddress2 = cleanUpAddressForComparison(fullAddress);
        java.lang.String contactStreet2 = cleanUpAddressForComparison(contactStreet);
        return isSomewhatMatchingAddress(address2, fullAddress2, contactStreet2) || (com.navdy.client.app.framework.util.StringUtils.containsAfterTrim(contactStreet2, streetNumber) && com.navdy.client.app.framework.util.StringUtils.containsAfterTrim(contactStreet2, streetName)) || (com.navdy.client.app.framework.util.StringUtils.containsAfterTrim(fullAddress2, streetNumber) && com.navdy.client.app.framework.util.StringUtils.containsAfterTrim(fullAddress2, streetName));
    }

    @android.support.annotation.Nullable
    private static java.lang.String cleanUpAddressForComparison(@android.support.annotation.Nullable java.lang.String address) {
        return com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(address) ? address : address.replaceAll("[,\n\t]", " ").replaceAll(" +", " ").trim();
    }

    private static boolean isSomewhatMatchingAddress(@android.support.annotation.Nullable java.lang.String cachedAddress, @android.support.annotation.Nullable java.lang.String fullAddress, @android.support.annotation.Nullable java.lang.String contactStreet) {
        logger.d("Comparing " + cachedAddress + " with " + fullAddress + " and " + contactStreet);
        return com.navdy.client.app.framework.util.StringUtils.containsAfterTrim(fullAddress, cachedAddress) || com.navdy.client.app.framework.util.StringUtils.containsAfterTrim(cachedAddress, fullAddress) || com.navdy.client.app.framework.util.StringUtils.containsAfterTrim(cachedAddress, contactStreet);
    }

    public static android.database.Cursor getDestinationCursor() {
        return getDestinationCursor(new android.util.Pair(null, null));
    }

    public static android.database.Cursor getDestinationCursor(@android.support.annotation.NonNull android.util.Pair<java.lang.String, java.lang.String[]> selection) {
        return getDestinationCursor(selection, "_id ASC");
    }

    public static android.database.Cursor getDestinationCursor(@android.support.annotation.NonNull android.util.Pair<java.lang.String, java.lang.String[]> selection, java.lang.String sortOrder) {
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        return contentResolver.query(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PROJECTION, (java.lang.String) selection.first, (java.lang.String[]) selection.second, sortOrder);
    }

    public static com.navdy.client.app.framework.models.Destination getDestinationItemAt(android.database.Cursor cursor, int position) {
        if (cursor == null || !cursor.moveToPosition(position)) {
            return null;
        }
        int id = cursor.getInt(cursor.getColumnIndex("_id"));
        java.lang.String placeId = cursor.getString(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PLACE_ID));
        long lastPlaceIdRefresh = cursor.getLong(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LAST_PLACE_ID_REFRESH));
        double displayLat = cursor.getDouble(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LAT));
        double displayLong = cursor.getDouble(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LONG));
        double navigationLat = cursor.getDouble(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LAT));
        double navigationLong = cursor.getDouble(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LONG));
        java.lang.String name = cursor.getString(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAME));
        java.lang.String address = cursor.getString(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_ADDRESS));
        java.lang.String streetNumber = cursor.getString(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_STREET_NUMBER));
        java.lang.String streetName = cursor.getString(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_STREET_NAME));
        java.lang.String city = cursor.getString(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CITY));
        java.lang.String state = cursor.getString(cursor.getColumnIndex("state"));
        java.lang.String zipCode = cursor.getString(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_ZIP_CODE));
        java.lang.String country = cursor.getString(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_COUNTRY));
        java.lang.String countryCode = cursor.getString(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_COUNTRY_CODE));
        boolean doNotSuggest = cursor.getInt(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_DO_NOT_SUGGEST)) == 1;
        com.navdy.client.app.framework.models.Destination d = new com.navdy.client.app.framework.models.Destination(id, placeId, lastPlaceIdRefresh, displayLat, displayLong, navigationLat, navigationLong, name, address, streetNumber, streetName, city, state, zipCode, country, countryCode, doNotSuggest, cursor.getLong(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LAST_ROUTED_DATE)), cursor.getString(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LABEL)), cursor.getInt(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_ORDER)), cursor.getInt(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_FAVORITE_TYPE)), cursor.getString(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PLACE_DETAIL_JSON)), com.navdy.client.app.framework.models.Destination.Precision.get(cursor.getInt(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PRECISION_LEVEL))), com.navdy.client.app.framework.models.Destination.Type.get(cursor.getInt(cursor.getColumnIndex("type"))), cursor.getString(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTACT_LOOKUP_KEY)), cursor.getLong(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LAST_KNOWN_CONTACT_ID)), cursor.getLong(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LAST_CONTACT_LOOKUP)));
        d.rawAddressVariations = getCacheKeysForDestinationId(id);
        return d;
    }

    public static com.navdy.client.app.framework.models.Destination getThisDestination(int destinationId) {
        try {
            return getDestinationItemFromSelection(new android.util.Pair<>("_id = ?", new java.lang.String[]{java.lang.String.valueOf(destinationId)}));
        } catch (java.lang.NumberFormatException e) {
            logger.e("Exception while parsing destination ID", e);
            return null;
        }
    }

    public static com.navdy.client.app.framework.models.Destination getThisDestination(java.lang.String destinationId) {
        try {
            return getDestinationItemFromSelection(new android.util.Pair<>("_id = ?", new java.lang.String[]{destinationId}));
        } catch (java.lang.NumberFormatException e) {
            logger.e("Exception while parsing destination ID", e);
            return null;
        }
    }

    public static com.navdy.client.app.framework.models.Destination getThisDestination(com.navdy.client.app.framework.models.Destination destination) {
        if (destination == null) {
            return null;
        }
        try {
            android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
            android.util.Pair<java.lang.String, java.lang.String[]> comparisonSelectionClause = destination.getComparisonSelectionClause();
            if (comparisonSelectionClause == null) {
                return destination;
            }
            android.database.Cursor c = contentResolver.query(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PROJECTION, (java.lang.String) comparisonSelectionClause.first, (java.lang.String[]) comparisonSelectionClause.second, null);
            if (c != null && c.moveToFirst()) {
                destination = getDestinationItemAt(c, 0);
            }
            com.navdy.service.library.util.IOUtils.closeStream(c);
            return destination;
        } catch (java.lang.Exception e) {
            logger.i("Destination not found in db for: " + destination);
            return destination;
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(null);
        }
    }

    public static com.navdy.client.app.framework.models.Destination getThisDestinationWithPlaceId(java.lang.String placeId) {
        return getDestinationItemFromSelection(new android.util.Pair<>("place_id = ?", new java.lang.String[]{placeId}));
    }

    public static java.util.ArrayList<com.navdy.client.app.framework.models.Destination> getDestinationsFromSearchQuery(@android.support.annotation.NonNull java.lang.String query) {
        logger.v("query: " + query);
        java.lang.String query2 = "%" + query.trim().replaceAll(" ", "%") + "%";
        java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinations = new java.util.ArrayList<>();
        try {
            android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
            if (contentResolver == null) {
                return null;
            }
            android.database.Cursor cursor = contentResolver.query(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PROJECTION, "place_name like ? OR favorite_label like ? OR address like ?", new java.lang.String[]{query2, query2, query2}, "place_name ASC");
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    com.navdy.client.app.framework.models.Destination destination = getThisDestination(cursor.getInt(cursor.getColumnIndex("_id")));
                    if (destination != null) {
                        destination.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.RECENT_PLACES.getValue();
                        destinations.add(destination);
                    }
                }
            }
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
            logger.d("getDestinationsFromSearchQuery: " + destinations);
            return destinations;
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(null);
        }
    }

    public static java.util.ArrayList<java.lang.String> getSearchHistoryFromSearchQuery(java.lang.String constraint) {
        java.util.ArrayList<java.lang.String> pastQueries = new java.util.ArrayList<>();
        try {
            android.content.ContentResolver cr = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
            if (cr == null) {
                return null;
            }
            android.database.Cursor c = cr.query(com.navdy.client.app.providers.NavdyContentProviderConstants.SEARCH_HISTORY_CONTENT_URI, new java.lang.String[]{"query"}, "query like ?", new java.lang.String[]{constraint + "%"}, "last_searched_on DESC");
            while (c != null && c.moveToNext()) {
                pastQueries.add(c.getString(0));
            }
            com.navdy.service.library.util.IOUtils.closeStream(c);
            return pastQueries;
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(null);
        }
    }

    @android.support.annotation.Nullable
    public static com.navdy.client.app.framework.models.Destination getDestinationItemFromSelection(@android.support.annotation.NonNull android.util.Pair<java.lang.String, java.lang.String[]> selection) {
        android.database.Cursor cursor = null;
        try {
            cursor = getDestinationCursor(selection);
            return getDestinationItemAt(cursor, 0);
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
        }
    }

    @android.support.annotation.Nullable
    public static java.lang.String getNonStalePlaceDetailInfoFor(java.lang.String placeId) {
        com.navdy.client.app.framework.models.Destination destination = getThisDestinationWithPlaceId(placeId);
        if (destination == null || destination.isPlaceDetailsInfoStale()) {
            return null;
        }
        return destination.placeDetailJson;
    }

    public static android.database.Cursor getRecentsCursor() {
        return getRecentsCursor(new android.util.Pair(null, null));
    }

    public static android.database.Cursor getRecentsCursor(@android.support.annotation.NonNull android.util.Pair<java.lang.String, java.lang.String[]> selection) {
        android.util.Pair<java.lang.String, java.lang.String[]> selection2 = restrictSelectionToRecents(selection);
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        return contentResolver.query(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PROJECTION, (java.lang.String) selection2.first, (java.lang.String[]) selection2.second, "last_routed_date DESC");
    }

    @android.support.annotation.NonNull
    public static android.util.Pair<java.lang.String, java.lang.String[]> restrictSelectionToRecents(android.util.Pair<java.lang.String, java.lang.String[]> selection) {
        if (selection == null || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim((java.lang.CharSequence) selection.first)) {
            return new android.util.Pair<>("last_routed_date <> 0", null);
        }
        return new android.util.Pair<>("(" + ((java.lang.String) selection.first) + ") AND (" + com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LAST_ROUTED_DATE + " <> 0)", selection.second);
    }

    public static com.navdy.client.app.framework.models.Destination getRecentItemFromSelection(@android.support.annotation.NonNull android.util.Pair<java.lang.String, java.lang.String[]> selection) {
        return getDestinationItemFromSelection(restrictSelectionToRecents(selection));
    }

    public static android.database.Cursor getFavoritesCursor() {
        return getFavoritesCursor(new android.util.Pair(null, null));
    }

    public static android.database.Cursor getFavoritesCursor(@android.support.annotation.NonNull android.util.Pair<java.lang.String, java.lang.String[]> selection) {
        android.util.Pair<java.lang.String, java.lang.String[]> selection2 = restrictSelectionToFavorites(selection);
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        return contentResolver.query(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PROJECTION, (java.lang.String) selection2.first, (java.lang.String[]) selection2.second, "destinations.favorite_listing_order ASC");
    }

    @android.support.annotation.NonNull
    public static android.util.Pair<java.lang.String, java.lang.String[]> restrictSelectionToFavorites(android.util.Pair<java.lang.String, java.lang.String[]> selection) {
        java.lang.String str = "is_special <> 0";
        if (selection == null || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim((java.lang.CharSequence) selection.first)) {
            return new android.util.Pair<>("is_special <> 0", null);
        }
        return new android.util.Pair<>("(" + ((java.lang.String) selection.first) + ") AND (" + "is_special <> 0" + ")", selection.second);
    }

    public static com.navdy.client.app.framework.models.Destination getHome() {
        return getFavoriteItemFromSelection(new android.util.Pair("is_special ==  ?", new java.lang.String[]{java.lang.String.valueOf(-3)}));
    }

    public static com.navdy.client.app.framework.models.Destination getWork() {
        return getFavoriteItemFromSelection(new android.util.Pair("is_special ==  ?", new java.lang.String[]{java.lang.String.valueOf(-2)}));
    }

    public static boolean isHomeSet() {
        return getHome() != null;
    }

    public static boolean isWorkSet() {
        return getWork() != null;
    }

    public static com.navdy.client.app.framework.models.Destination getFavoriteItemFromSelection(@android.support.annotation.NonNull android.util.Pair<java.lang.String, java.lang.String[]> selection) {
        return getDestinationItemFromSelection(restrictSelectionToFavorites(selection));
    }

    public static android.database.Cursor getTripsCursor() {
        return getTripsCursor(new android.util.Pair(null, null));
    }

    public static android.database.Cursor getTripsCursor(@android.support.annotation.NonNull android.util.Pair<java.lang.String, java.lang.String[]> selection) {
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        return contentResolver.query(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_CONTENT_URI, com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_PROJECTION, (java.lang.String) selection.first, (java.lang.String[]) selection.second, "start_time DESC");
    }

    public static long getMaxTripId() {
        long j = -1;
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver != null) {
            android.database.Cursor cursor = null;
            try {
                cursor = contentResolver.query(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_CONTENT_URI, new java.lang.String[]{"MAX(_id)"}, null, null, null);
                if (cursor == null || !cursor.moveToFirst()) {
                    com.navdy.service.library.util.IOUtils.closeStream(cursor);
                } else {
                    j = cursor.getLong(0);
                    cursor.close();
                }
            } finally {
                com.navdy.service.library.util.IOUtils.closeStream(cursor);
            }
        }
        return j;
    }

    public static android.database.Cursor getGroupedTripsCursor() {
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        return contentResolver.query(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_CONTENT_URI, new java.lang.String[]{"count(_id) as occurrences", com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_DESTINATION_ID}, "destination_id != 0) group by (destination_id", null, "occurrences DESC");
    }

    public static com.navdy.client.app.framework.models.Trip getTripsItemAt(android.database.Cursor cursor, int position) {
        if (cursor == null || !cursor.moveToPosition(position)) {
            return null;
        }
        return new com.navdy.client.app.framework.models.Trip(cursor.getInt(cursor.getColumnIndex("_id")), cursor.getLong(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_TRIP_NUMBER)), cursor.getLong(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_START_TIME)), cursor.getInt(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_START_TIME_ZONE_N_DST)), cursor.getInt(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_START_ODOMETER)), cursor.getDouble(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_START_LAT)), cursor.getDouble(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_START_LONG)), cursor.getLong(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_END_TIME)), cursor.getInt(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_END_ODOMETER)), cursor.getDouble(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_END_LAT)), cursor.getDouble(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_END_LONG)), cursor.getLong(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_ARRIVED_AT_DESTINATION)), cursor.getInt(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_DESTINATION_ID)));
    }

    public static com.navdy.client.app.framework.models.Trip getThisTrip(java.lang.String tripNumber) {
        return getTripFromSelection(new android.util.Pair<>("trip_number=?", new java.lang.String[]{java.lang.String.valueOf(tripNumber)}));
    }

    @android.support.annotation.Nullable
    public static com.navdy.client.app.framework.models.Trip getTripFromSelection(@android.support.annotation.NonNull android.util.Pair<java.lang.String, java.lang.String[]> selection) {
        android.database.Cursor cursor = null;
        try {
            cursor = getTripsCursor(selection);
            return getTripsItemAt(cursor, 0);
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
        }
    }

    public static android.database.Cursor getSearchHistoryCursor() {
        return getSearchHistoryCursor(new android.util.Pair(null, null));
    }

    @android.support.annotation.WorkerThread
    public static android.database.Cursor getSearchHistoryCursor(@android.support.annotation.NonNull android.util.Pair<java.lang.String, java.lang.String[]> selection) {
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        return contentResolver.query(com.navdy.client.app.providers.NavdyContentProviderConstants.SEARCH_HISTORY_CONTENT_URI, com.navdy.client.app.providers.NavdyContentProviderConstants.SEARCH_HISTORY_PROJECTION, (java.lang.String) selection.first, (java.lang.String[]) selection.second, "last_searched_on DESC");
    }

    @android.support.annotation.WorkerThread
    public static int addToSearchHistory(java.lang.String query) {
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        android.content.ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(query) || query.equals(context.getString(com.navdy.client.R.string.gas)) || query.equals(context.getString(com.navdy.client.R.string.parking)) || query.equals(context.getString(com.navdy.client.R.string.food)) || query.equals(context.getString(com.navdy.client.R.string.atm))) {
            return -1;
        }
        android.content.ContentValues contentValues = new android.content.ContentValues();
        contentValues.put("last_searched_on", java.lang.Long.valueOf(new java.util.Date().getTime()));
        android.database.Cursor cursor = null;
        try {
            cursor = contentResolver.query(com.navdy.client.app.providers.NavdyContentProviderConstants.SEARCH_HISTORY_CONTENT_URI, com.navdy.client.app.providers.NavdyContentProviderConstants.SEARCH_HISTORY_PROJECTION, "lower(query)=?", new java.lang.String[]{query.toLowerCase()}, null);
            if (cursor == null || !cursor.moveToFirst()) {
                com.navdy.service.library.util.IOUtils.closeStream(cursor);
                contentValues.put("query", query);
                return contentResolver.insert(com.navdy.client.app.providers.NavdyContentProviderConstants.SEARCH_HISTORY_CONTENT_URI, contentValues) != null ? 1 : -1;
            }
            return contentResolver.update(com.navdy.client.app.providers.NavdyContentProviderConstants.SEARCH_HISTORY_CONTENT_URI, contentValues, "_id=?", new java.lang.String[]{java.lang.String.valueOf(cursor.getInt(cursor.getColumnIndex("_id")))});
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
        }
    }

    @android.support.annotation.WorkerThread
    public static int removeFromSearchHistory(int id) {
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            return -1;
        }
        return contentResolver.delete(com.navdy.client.app.providers.NavdyContentProviderConstants.SEARCH_HISTORY_CONTENT_URI, "_id=?", new java.lang.String[]{java.lang.String.valueOf(id)});
    }

    @android.support.annotation.Nullable
    @android.support.annotation.WorkerThread
    public static com.navdy.client.app.framework.models.DestinationCacheEntry getCacheEntryIfExists(@android.support.annotation.Nullable java.lang.String location) {
        logger.d("CACHE: Checking the cache for " + location);
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(location)) {
            logger.e("CACHE: Can't lookup entries for a null location!");
            return null;
        }
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            logger.e("CACHE: Unable to get a content resolver!");
            return null;
        }
        removeOutOfDateCacheEntries();
        android.database.Cursor cursor = null;
        try {
            cursor = contentResolver.query(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATION_CACHE_CONTENT_URI, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATION_CACHE_PROJECTION, "location=?", new java.lang.String[]{location}, null);
            if (cursor == null || !cursor.moveToFirst()) {
                com.navdy.service.library.util.IOUtils.closeStream(cursor);
                logger.d("CACHE: Couldn't find any cache entry for this location: " + location);
                return null;
            }
            int id = cursor.getInt(cursor.getColumnIndex("_id"));
            long lastResponseDate = cursor.getLong(cursor.getColumnIndex("last_response_date"));
            java.lang.String locationString = cursor.getString(cursor.getColumnIndex("location"));
            int destinationId = cursor.getInt(cursor.getColumnIndex(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_DESTINATION_ID));
            com.navdy.client.app.framework.models.DestinationCacheEntry dce = new com.navdy.client.app.framework.models.DestinationCacheEntry(id, lastResponseDate, locationString, destinationId);
            dce.destination = getThisDestination(destinationId);
            logger.d("CACHE: Found this cache entry for this location: " + dce);
            return dce;
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
        }
    }

    @android.support.annotation.WorkerThread
    private static void removeOutOfDateCacheEntries() {
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver != null) {
            int nbDeleted = contentResolver.delete(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATION_CACHE_CONTENT_URI, "last_response_date<? AND destination_id = -1", new java.lang.String[]{java.lang.String.valueOf(new java.util.Date().getTime() - com.navdy.client.app.providers.NavdyContentProviderConstants.MAX_DESTINATION_CACHE_AGE)});
            if (nbDeleted > 0) {
                logger.d("CACHE: Removed " + nbDeleted + " old entry(s) from the cache");
            }
        }
    }

    @android.support.annotation.WorkerThread
    public static void addToCacheIfNotAlreadyIn(@android.support.annotation.NonNull java.lang.String address, @android.support.annotation.Nullable com.navdy.client.app.framework.models.Destination destination) {
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(address)) {
            com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
            if (com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver() != null) {
                if (destination != null && !destination.hasValidNavCoordinates()) {
                    destination = null;
                }
                logger.d("CACHE: addToCacheIfNotAlreadyIn: address = " + address + " destination = " + destination);
                if (getCacheEntryIfExists(address) == null) {
                    insertNewDestinationInCache(address, destination);
                } else {
                    logger.d("CACHE: Location already present in the cache so will not add it.");
                }
            }
        }
    }

    @android.support.annotation.WorkerThread
    private static void insertNewDestinationInCache(@android.support.annotation.NonNull java.lang.String location, @android.support.annotation.Nullable com.navdy.client.app.framework.models.Destination destination) {
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(location)) {
            logger.d("CACHE: Inserting the following destination in the cache: location = " + location + " | destination = " + destination);
            if (destination == null || destination.id > 0) {
                com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
                android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
                if (contentResolver != null) {
                    removeOldestEntryIfCacheMaxSizeExceeded();
                    int destinationId = destination != null ? destination.id : -1;
                    android.content.ContentValues contentValues = new android.content.ContentValues();
                    contentValues.put("last_response_date", java.lang.Long.valueOf(new java.util.Date().getTime()));
                    contentValues.put("location", location);
                    contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_DESTINATION_ID, java.lang.Integer.valueOf(destinationId));
                    contentResolver.insert(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATION_CACHE_CONTENT_URI, contentValues);
                    return;
                }
                return;
            }
            throw new java.lang.RuntimeException("Trying to insert a non null destination that has no ID! This should never happen");
        }
    }

    @android.support.annotation.WorkerThread
    private static void removeOldestEntryIfCacheMaxSizeExceeded() {
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver != null) {
            android.database.Cursor cursor = null;
            try {
                cursor = contentResolver.query(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATION_CACHE_CONTENT_URI, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATION_CACHE_PROJECTION, null, null, "last_response_date ASC");
                if (cursor != null) {
                    int count = cursor.getCount();
                    long nbEntriesToRemove = ((long) count) - 1000;
                    if (nbEntriesToRemove > 0) {
                        logger.d("CACHE: Too many entries in the cache. Removing " + nbEntriesToRemove + " out of " + count);
                    }
                    for (int i = 0; ((long) i) < nbEntriesToRemove; i++) {
                        if (cursor.moveToPosition(i)) {
                            removeThisCacheEntry(cursor.getInt(cursor.getColumnIndex("_id")));
                        }
                    }
                }
            } finally {
                com.navdy.service.library.util.IOUtils.closeStream(cursor);
            }
        }
    }

    @android.support.annotation.WorkerThread
    private static void removeThisCacheEntry(int id) {
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver != null) {
            contentResolver.delete(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATION_CACHE_CONTENT_URI, "_id=?", new java.lang.String[]{java.lang.String.valueOf(id)});
        }
    }

    public static void clearCache() {
        android.database.sqlite.SQLiteDatabase database = getSqlDb(com.navdy.client.app.NavdyApplication.getAppContext());
        database.delete("destination_cache", null, null);
        database.execSQL(CREATE_DESTINATION_CACHE_TABLE);
    }

    @android.support.annotation.WorkerThread
    public static android.database.Cursor getPlaylistsCursor() {
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        return com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver().query(com.navdy.client.app.providers.NavdyContentProviderConstants.PLAYLISTS_CONTENT_URI, com.navdy.client.app.providers.NavdyContentProviderConstants.PLAYLISTS_PROJECTION, null, null, com.navdy.client.app.framework.util.MusicDbUtils.getOrderString("playlist_name"));
    }

    @android.support.annotation.WorkerThread
    public static android.database.Cursor getPlaylistMembersCursor(int playlistId) {
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        java.lang.String[] args = {java.lang.String.valueOf(playlistId)};
        return contentResolver.query(com.navdy.client.app.providers.NavdyContentProviderConstants.PLAYLIST_MEMBERS_CONTENT_URI, com.navdy.client.app.providers.NavdyContentProviderConstants.PLAYLIST_MEMBERS_PROJECTION, "playlist_id = ?", args, null);
    }

    public static void addPlaylistToDb(int playlistId, java.lang.String playlistName) {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        android.content.ContentValues playlistContentValues = new android.content.ContentValues(2);
        playlistContentValues.put("playlist_id", java.lang.Integer.valueOf(playlistId));
        playlistContentValues.put("playlist_name", playlistName);
        context.getContentResolver().insert(com.navdy.client.app.providers.NavdyContentProviderConstants.PLAYLISTS_CONTENT_URI, playlistContentValues);
    }

    public static void addPlaylistMemberToDb(int playlistId, java.lang.String sourceId, java.lang.String artist, java.lang.String album, java.lang.String title) {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        android.content.ContentValues memberContentValues = new android.content.ContentValues(5);
        memberContentValues.put("playlist_id", java.lang.Integer.valueOf(playlistId));
        memberContentValues.put("SourceId", sourceId);
        memberContentValues.put("artist", artist);
        memberContentValues.put("album", album);
        memberContentValues.put("title", title);
        context.getContentResolver().insert(com.navdy.client.app.providers.NavdyContentProviderConstants.PLAYLIST_MEMBERS_CONTENT_URI, memberContentValues);
    }

    @android.support.annotation.WorkerThread
    public static void deleteAllPlaylists() {
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        contentResolver.delete(com.navdy.client.app.providers.NavdyContentProviderConstants.PLAYLISTS_CONTENT_URI, null, null);
        contentResolver.delete(com.navdy.client.app.providers.NavdyContentProviderConstants.PLAYLIST_MEMBERS_CONTENT_URI, null, null);
    }

    @android.support.annotation.WorkerThread
    public static void deletePlaylist(int playlistId) {
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        java.lang.String query = "playlist_id = ?";
        java.lang.String[] args = {java.lang.String.valueOf(playlistId)};
        contentResolver.delete(com.navdy.client.app.providers.NavdyContentProviderConstants.PLAYLISTS_CONTENT_URI, query, args);
        contentResolver.delete(com.navdy.client.app.providers.NavdyContentProviderConstants.PLAYLIST_MEMBERS_CONTENT_URI, query, args);
    }
}
