package com.navdy.client.app.framework.models;

public class UserAccountInfo {
    private static com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.models.UserAccountInfo.class);
    public java.lang.String email;
    public java.lang.String fullName;
    public android.graphics.Bitmap photo;

    public UserAccountInfo(java.lang.String fullName2, java.lang.String email2, android.graphics.Bitmap photo2) {
        this.fullName = fullName2;
        this.email = email2;
        this.photo = photo2;
    }

    public static com.navdy.client.app.framework.models.UserAccountInfo getUserAccountInfo() {
        android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
        return new com.navdy.client.app.framework.models.UserAccountInfo(customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.FULL_NAME, ""), customerPrefs.getString("email", ""), com.navdy.client.app.tracking.Tracker.getUserProfilePhoto());
    }

    public boolean hasInfo() {
        return !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.fullName) || !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.email) || this.photo != null;
    }
}
