package com.navdy.client.app.framework;

public final class Injector {
    private static final java.lang.String INJECTOR_SERVICE = "com.navdy.app.client.injector";

    public static dagger.ObjectGraph obtain(android.content.Context context) {
        return (dagger.ObjectGraph) context.getSystemService(INJECTOR_SERVICE);
    }

    public static void inject(android.content.Context context, java.lang.Object object) {
        dagger.ObjectGraph objectGraph = obtain(context);
        if (objectGraph != null) {
            objectGraph.inject(object);
            return;
        }
        throw new java.lang.IllegalArgumentException("Please provide application context");
    }

    public static boolean matchesService(java.lang.String name) {
        return INJECTOR_SERVICE.equals(name);
    }
}
