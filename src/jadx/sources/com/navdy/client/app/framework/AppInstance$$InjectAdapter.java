package com.navdy.client.app.framework;

public final class AppInstance$$InjectAdapter extends dagger.internal.Binding<com.navdy.client.app.framework.AppInstance> implements dagger.MembersInjector<com.navdy.client.app.framework.AppInstance> {
    private dagger.internal.Binding<com.navdy.client.app.framework.servicehandler.NetworkStatusManager> mNetworkStatus;
    private dagger.internal.Binding<com.navdy.service.library.util.Listenable> supertype;

    public AppInstance$$InjectAdapter() {
        super(null, "members/com.navdy.client.app.framework.AppInstance", false, com.navdy.client.app.framework.AppInstance.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mNetworkStatus = linker.requestBinding("com.navdy.client.app.framework.servicehandler.NetworkStatusManager", com.navdy.client.app.framework.AppInstance.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.service.library.util.Listenable", com.navdy.client.app.framework.AppInstance.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mNetworkStatus);
        injectMembersBindings.add(this.supertype);
    }

    public void injectMembers(com.navdy.client.app.framework.AppInstance object) {
        object.mNetworkStatus = (com.navdy.client.app.framework.servicehandler.NetworkStatusManager) this.mNetworkStatus.get();
        this.supertype.injectMembers(object);
    }
}
