package com.navdy.client.app.framework.navigation;

public final class NavdyRouteHandler {
    private static final java.lang.String CACHED_ARRIVED_DESTINATION = "cached_arrived_destination";
    public static final int INVALID_DISTANCE = -1;
    public static final int INVALID_DURATION = -1;
    private static final long PENDING_ROUTE_REFRESH = 9000000;
    private static final java.lang.String SAVED_DESTINATION = "saved_destination";
    private static final java.lang.String SHARED_PREF_SAVED_ROUTE = "saved_route";
    private static final boolean VERBOSE = true;
    private static final com.navdy.client.app.framework.navigation.NavdyRouteHandler instance = new com.navdy.client.app.framework.navigation.NavdyRouteHandler();
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.navigation.NavdyRouteHandler.class);
    private final com.navdy.client.app.framework.AppInstance appInstance = com.navdy.client.app.framework.AppInstance.getInstance();
    private com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error currentError;
    private com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo currentRoute;
    private com.navdy.client.app.framework.navigation.NavdyRouteHandler.State currentState;
    private final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private com.navdy.client.app.framework.models.Destination lastDestinationArrived = getLastDestinationArrived();
    private final java.util.List<java.lang.ref.WeakReference<com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener>> listeners = new java.util.ArrayList();
    private final com.navdy.client.app.framework.navigation.NavigationHelper navigationHelper = new com.navdy.client.app.framework.navigation.NavigationHelper();
    private com.navdy.client.app.framework.navigation.HereRouteManager.RouteHandle pendingRouteHandle;
    private final java.lang.Runnable refreshPendingRoute = new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon1();
    private boolean requestedHighAccuracy;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentState == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.PENDING_ROUTE && !com.navdy.client.app.ui.settings.SettingsUtils.isLimitingCellularData()) {
                com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.calculatePendingRoute(com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentRoute.destination);
            }
        }
    }

    class Anon10 implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.CallListener {
        Anon10() {
        }

        public void call(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener listener) {
            listener.onRouteArrived(com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.lastDestinationArrived);
        }
    }

    class Anon11 implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.CallListener {
        Anon11() {
        }

        public void call(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener listener) {
            listener.onStopRoute();
        }
    }

    class Anon12 implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.CallListener {
        Anon12() {
        }

        public void call(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener listener) {
            listener.onPendingRouteCalculated(com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentError, com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentRoute);
        }
    }

    class Anon13 implements com.navdy.client.app.framework.navigation.HereRouteManager.Listener {
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;

        class Anon1 implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.CallListener {
            Anon1() {
            }

            public void call(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener listener) {
                listener.onPendingRouteCalculated(com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentError, com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentRoute);
            }
        }

        Anon13(com.navdy.client.app.framework.models.Destination destination) {
            this.val$destination = destination;
        }

        public void onPreCalculation(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.HereRouteManager.RouteHandle routeHandle) {
            com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.pendingRouteHandle = routeHandle;
        }

        public void onRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.HereRouteManager.Error error, com.here.android.mpa.routing.Route route) {
            if (com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentState == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.CALCULATING_PENDING_ROUTE) {
                com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentState = com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.PENDING_ROUTE;
                com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.pendingRouteHandle = null;
                if (error != com.navdy.client.app.framework.navigation.HereRouteManager.Error.NONE) {
                    com.navdy.client.app.framework.navigation.NavdyRouteHandler.logger.e("failed to calculate pending trip");
                    com.navdy.client.app.framework.navigation.NavdyRouteHandler.logger.v("requestNewRoute pending route has error " + error.name() + ", setting state to CALCULATION_FAILED");
                    com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentError = com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error.NO_ROUTES;
                } else {
                    com.navdy.client.app.framework.navigation.NavdyRouteHandler.logger.v("requestNewRoute pending route success, setting state to PENDING_TRIP");
                    com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentError = com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error.NONE;
                    com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentRoute = new com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo(this.val$destination, null, new com.here.android.mpa.common.GeoPolyline(route.getRouteGeometry()), null, null, route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.DISABLED, com.here.android.mpa.routing.Route.WHOLE_ROUTE).getDuration(), route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, com.here.android.mpa.routing.Route.WHOLE_ROUTE).getDuration(), route.getLength());
                    com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.handler.postDelayed(com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.refreshPendingRoute, com.navdy.client.app.framework.navigation.NavdyRouteHandler.PENDING_ROUTE_REFRESH);
                }
                if (!com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.appInstance.isHudMapEngineReady()) {
                    com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.callListeners(new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon13.Anon1());
                }
            }
        }
    }

    class Anon14 implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.CallListener {
        Anon14() {
        }

        public void call(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener listener) {
            listener.onTripProgress(com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentRoute);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;
        final /* synthetic */ boolean val$initiatedOnHud;

        class Anon1 implements com.here.android.mpa.common.OnEngineInitListener {

            /* renamed from: com.navdy.client.app.framework.navigation.NavdyRouteHandler$Anon2$Anon1$Anon1 reason: collision with other inner class name */
            class C0046Anon1 implements com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback {
                final /* synthetic */ java.lang.String val$newRequestId;

                C0046Anon1(java.lang.String str) {
                    this.val$newRequestId = str;
                }

                public void onSuccess(com.navdy.client.app.framework.models.Destination destination) {
                    prepareNavigation(destination, this.val$newRequestId, com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon2.this.val$initiatedOnHud);
                }

                public void onFailure(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error error) {
                    if (error != com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error.NONE) {
                        com.navdy.client.app.framework.navigation.NavdyRouteHandler.logger.w("error while calling processDestination before navigating to it");
                    }
                    prepareNavigation(destination, this.val$newRequestId, com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon2.this.val$initiatedOnHud);
                }

                private void prepareNavigation(com.navdy.client.app.framework.models.Destination destination, java.lang.String newRequestId, boolean initiatedOnHud) {
                    if (com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentState != com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.CALCULATING_ROUTES) {
                        com.navdy.client.app.framework.navigation.NavdyRouteHandler.logger.i("processDestination complete, but state is not CALCULATING_ROUTES anymore, no-op");
                    } else if (!com.navdy.client.app.framework.models.Destination.equals(destination, com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentRoute.destination)) {
                        com.navdy.client.app.framework.navigation.NavdyRouteHandler.logger.i("processDestination complete, but current destinations are not the same, no-op");
                    } else {
                        com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.navigationHelper.navigateToDestination(destination, newRequestId, initiatedOnHud);
                    }
                }
            }

            /* renamed from: com.navdy.client.app.framework.navigation.NavdyRouteHandler$Anon2$Anon1$Anon2 reason: collision with other inner class name */
            class C0047Anon2 implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.CallListener {
                C0047Anon2() {
                }

                public void call(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener listener) {
                    listener.onRouteCalculating(com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon2.this.val$destination);
                }
            }

            class Anon3 implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.CallListener {
                Anon3() {
                }

                public void call(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener listener) {
                    listener.onPendingRouteCalculating(com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentRoute.destination);
                }
            }

            Anon1() {
            }

            public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
                com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentError = com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error.NONE;
                if (!com.navdy.client.app.framework.DeviceConnection.isConnected() || !com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.appInstance.isHudMapEngineReady()) {
                    com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.setPendingTrip(com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon2.this.val$destination);
                    com.navdy.client.app.framework.navigation.NavdyRouteHandler.logger.v("requestNewRoute, setting state to CALCULATING_PENDING_ROUTE");
                    com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentState = com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.CALCULATING_PENDING_ROUTE;
                    com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentRoute = new com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo(com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon2.this.val$destination, null, null, null, null, -1, -1, -1);
                    com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.callListeners(new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon2.Anon1.Anon3());
                    com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.calculatePendingRoute(com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon2.this.val$destination);
                    return;
                }
                com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.removeLastDestinationArrived();
                com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.removePendingTrip();
                com.navdy.client.app.framework.navigation.NavdyRouteHandler.logger.v("requestNewRoute, setting state to CALCULATING_ROUTES");
                com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentState = com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.CALCULATING_ROUTES;
                java.lang.String newRequestId = java.util.UUID.randomUUID().toString();
                com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentRoute = new com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo(com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon2.this.val$destination, null, null, newRequestId, null, -1, -1, -1);
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.processDestination(com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon2.this.val$destination, new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon2.Anon1.C0046Anon1(newRequestId));
                com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.callListeners(new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon2.Anon1.C0047Anon2());
            }
        }

        Anon2(com.navdy.client.app.framework.models.Destination destination, boolean z) {
            this.val$destination = destination;
            this.val$initiatedOnHud = z;
        }

        public void run() {
            com.navdy.client.app.framework.map.HereMapsManager.getInstance().addOnInitializedListener(new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon2.Anon1());
        }
    }

    class Anon3 implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.CallListener {
        Anon3() {
        }

        public void call(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener listener) {
            listener.onStopRoute();
        }
    }

    class Anon4 implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.CallListener {
        Anon4() {
        }

        public void call(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener listener) {
            listener.onRouteCalculating(com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentRoute.destination);
        }
    }

    class Anon5 implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.CallListener {
        Anon5() {
        }

        public void call(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener listener) {
            listener.onRouteCalculated(com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentError, com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentRoute);
        }
    }

    class Anon6 implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.CallListener {
        Anon6() {
        }

        public void call(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener listener) {
            listener.onRouteCalculated(com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentError, com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentRoute);
        }
    }

    class Anon7 implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.CallListener {
        Anon7() {
        }

        public void call(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener listener) {
            listener.onRouteStarted(com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentRoute);
        }
    }

    class Anon8 implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.CallListener {
        Anon8() {
        }

        public void call(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener listener) {
            listener.onRouteStarted(com.navdy.client.app.framework.navigation.NavdyRouteHandler.this.currentRoute);
        }
    }

    class Anon9 implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.CallListener {
        Anon9() {
        }

        public void call(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener listener) {
            listener.onReroute();
        }
    }

    private interface CallListener {
        void call(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener navdyRouteListener);
    }

    public enum Error {
        NONE,
        NO_ROUTES
    }

    public static class NavdyRouteInfo {
        public static final double HEAVY_TRAFFIC_THRESHOLD = 1.25d;
        /* access modifiers changed from: private */
        @android.support.annotation.Nullable
        public java.lang.String cancelHandle;
        /* access modifiers changed from: private */
        @android.support.annotation.NonNull
        public final com.navdy.client.app.framework.models.Destination destination;
        private int distanceToDestination;
        public final boolean isTrafficHeavy;
        /* access modifiers changed from: private */
        @android.support.annotation.Nullable
        public com.here.android.mpa.common.GeoPolyline progress;
        @android.support.annotation.Nullable
        final java.lang.String requestId;
        /* access modifiers changed from: private */
        @android.support.annotation.Nullable
        public final com.here.android.mpa.common.GeoPolyline route;
        @android.support.annotation.Nullable
        final java.lang.String routeId;
        private int timeToDestination;
        @android.support.annotation.Nullable
        public final java.lang.String via;

        /* synthetic */ NavdyRouteInfo(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo x0, com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon1 x1) {
            this(x0);
        }

        NavdyRouteInfo(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination2, @android.support.annotation.Nullable java.lang.String via2, @android.support.annotation.Nullable com.here.android.mpa.common.GeoPolyline route2, @android.support.annotation.Nullable java.lang.String requestId2, @android.support.annotation.Nullable java.lang.String routeId2, int duration, int durationWithTraffic, int distanceToDestination2) {
            boolean z = false;
            this.via = via2;
            this.requestId = requestId2;
            this.routeId = routeId2;
            this.destination = destination2;
            this.route = route2;
            this.distanceToDestination = distanceToDestination2;
            if (durationWithTraffic != -1) {
                this.timeToDestination = durationWithTraffic;
                if (((double) durationWithTraffic) / ((double) duration) >= 1.25d) {
                    z = true;
                }
                this.isTrafficHeavy = z;
                return;
            }
            this.timeToDestination = duration;
            this.isTrafficHeavy = false;
        }

        private NavdyRouteInfo(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo navdyRouteInfo) {
            this.via = navdyRouteInfo.via;
            this.requestId = navdyRouteInfo.requestId;
            this.routeId = navdyRouteInfo.routeId;
            this.isTrafficHeavy = navdyRouteInfo.isTrafficHeavy;
            this.destination = navdyRouteInfo.destination;
            this.route = navdyRouteInfo.route;
            this.progress = navdyRouteInfo.progress;
            this.cancelHandle = navdyRouteInfo.cancelHandle;
            this.timeToDestination = navdyRouteInfo.timeToDestination;
            this.distanceToDestination = navdyRouteInfo.distanceToDestination;
        }

        @android.support.annotation.NonNull
        public com.navdy.client.app.framework.models.Destination getDestination() {
            return this.destination;
        }

        @android.support.annotation.Nullable
        public com.here.android.mpa.common.GeoPolyline getRoute() {
            return this.route;
        }

        @android.support.annotation.Nullable
        public com.here.android.mpa.common.GeoPolyline getProgress() {
            return this.progress;
        }

        public void setHandle(java.lang.String cancelHandle2) {
            this.cancelHandle = cancelHandle2;
        }

        public int getTimeToDestination() {
            return this.timeToDestination;
        }

        public int getDistanceToDestination() {
            return this.distanceToDestination;
        }

        public boolean equals(java.lang.Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo that = (com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo) o;
            if (this.isTrafficHeavy == that.isTrafficHeavy && this.timeToDestination == that.timeToDestination && this.distanceToDestination == that.distanceToDestination && this.via != null) {
                return this.via.equals(that.via);
            }
            if (that.via == null && this.requestId != null) {
                return this.requestId.equals(that.requestId);
            }
            if (that.requestId == null && this.routeId != null) {
                return this.routeId.equals(that.routeId);
            }
            if (that.routeId == null && this.destination.equals(that.destination) && this.route != null) {
                return this.route.equals(that.route);
            }
            if (that.route == null && this.progress != null) {
                return this.progress.equals(that.progress);
            }
            if (that.progress == null && this.cancelHandle != null) {
                return this.cancelHandle.equals(that.cancelHandle);
            }
            if (that.cancelHandle != null) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int result;
            int i;
            int i2;
            int i3;
            int i4;
            int i5;
            int i6 = 0;
            if (this.via != null) {
                result = this.via.hashCode();
            } else {
                result = 0;
            }
            int i7 = result * 31;
            if (this.requestId != null) {
                i = this.requestId.hashCode();
            } else {
                i = 0;
            }
            int i8 = (i7 + i) * 31;
            if (this.routeId != null) {
                i2 = this.routeId.hashCode();
            } else {
                i2 = 0;
            }
            int i9 = (i8 + i2) * 31;
            if (this.isTrafficHeavy) {
                i3 = 1;
            } else {
                i3 = 0;
            }
            int hashCode = (((i9 + i3) * 31) + this.destination.hashCode()) * 31;
            if (this.route != null) {
                i4 = this.route.hashCode();
            } else {
                i4 = 0;
            }
            int i10 = (hashCode + i4) * 31;
            if (this.progress != null) {
                i5 = this.progress.hashCode();
            } else {
                i5 = 0;
            }
            int i11 = (i10 + i5) * 31;
            if (this.cancelHandle != null) {
                i6 = this.cancelHandle.hashCode();
            }
            return ((((i11 + i6) * 31) + this.timeToDestination) * 31) + this.distanceToDestination;
        }
    }

    public interface NavdyRouteListener {
        void onPendingRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo navdyRouteInfo);

        void onPendingRouteCalculating(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination);

        void onReroute();

        void onRouteArrived(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination);

        void onRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo navdyRouteInfo);

        void onRouteCalculating(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination);

        void onRouteStarted(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo navdyRouteInfo);

        void onStopRoute();

        void onTripProgress(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo navdyRouteInfo);
    }

    public enum State {
        INACTIVE,
        CALCULATING_PENDING_ROUTE,
        PENDING_ROUTE,
        CALCULATING_ROUTES,
        ROUTE_CALCULATED,
        EN_ROUTE
    }

    public static com.navdy.client.app.framework.navigation.NavdyRouteHandler getInstance() {
        return instance;
    }

    @com.squareup.otto.Subscribe
    public void onTripUpdate(com.navdy.service.library.events.TripUpdate tripUpdate) {
        if (this.currentState == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.EN_ROUTE) {
            logger.v("Received trip update: " + tripUpdate);
            if (!com.navdy.client.app.framework.map.HereMapsManager.getInstance().isInitialized()) {
                logger.e("Here Map Engine not initialized yet.");
                return;
            }
            com.navdy.service.library.events.location.LatLong currentLatLng = tripUpdate.current_position;
            if (currentLatLng != null) {
                int tta = -1;
                int distance = -1;
                if (tripUpdate.estimated_time_remaining != null) {
                    tta = tripUpdate.estimated_time_remaining.intValue();
                }
                if (tripUpdate.distance_to_destination != null) {
                    distance = tripUpdate.distance_to_destination.intValue();
                }
                updateCurrentProgress(new com.here.android.mpa.common.GeoCoordinate(currentLatLng.latitude.doubleValue(), currentLatLng.longitude.doubleValue()), tta, distance);
            }
        }
    }

    private NavdyRouteHandler() {
        logger.v("init, setting state to INACTIVE");
        this.currentState = com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.INACTIVE;
        this.currentError = com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error.NONE;
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
        com.navdy.client.app.framework.models.Destination pendingDestination = getPendingTrip();
        if (pendingDestination != null) {
            requestNewRoute(pendingDestination);
        }
    }

    @android.support.annotation.Nullable
    public com.navdy.client.app.framework.models.Destination getCurrentDestination() {
        if (this.currentRoute != null) {
            return this.currentRoute.destination;
        }
        return null;
    }

    public boolean isInOneOfThePendingTripStates() {
        return this.currentState == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.CALCULATING_PENDING_ROUTE || this.currentState == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.PENDING_ROUTE;
    }

    public boolean isInOneOfTheActiveTripStates() {
        return this.currentState == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.CALCULATING_ROUTES || this.currentState == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.ROUTE_CALCULATED || this.currentState == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.EN_ROUTE;
    }

    public void addListener(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener navdyRouteListener) {
        this.listeners.add(new java.lang.ref.WeakReference(navdyRouteListener));
        switch (this.currentState) {
            case INACTIVE:
                if (this.lastDestinationArrived != null) {
                    navdyRouteListener.onRouteArrived(this.lastDestinationArrived);
                }
                navdyRouteListener.onStopRoute();
                return;
            case CALCULATING_PENDING_ROUTE:
                navdyRouteListener.onPendingRouteCalculating(this.currentRoute.destination);
                return;
            case PENDING_ROUTE:
                navdyRouteListener.onPendingRouteCalculated(this.currentError, this.currentRoute);
                return;
            case CALCULATING_ROUTES:
                navdyRouteListener.onRouteCalculating(this.currentRoute.destination);
                return;
            case ROUTE_CALCULATED:
                navdyRouteListener.onRouteCalculated(this.currentError, this.currentRoute);
                return;
            case EN_ROUTE:
                navdyRouteListener.onRouteStarted(this.currentRoute);
                if (this.currentRoute.progress != null && this.currentRoute.progress.length() > 1.0d) {
                    navdyRouteListener.onTripProgress(this.currentRoute);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void removeListener(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener navdyRouteListener) {
        java.util.ListIterator<java.lang.ref.WeakReference<com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener>> iterator = this.listeners.listIterator();
        while (iterator.hasNext()) {
            com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener listener = (com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener) ((java.lang.ref.WeakReference) iterator.next()).get();
            if (listener == null || listener.equals(navdyRouteListener)) {
                iterator.remove();
            }
        }
    }

    public com.navdy.client.app.framework.navigation.NavdyRouteHandler.State getCurrentState() {
        return this.currentState;
    }

    public void requestNewRoute(com.navdy.client.app.framework.models.Destination destination) {
        requestNewRoute(destination, false);
    }

    public void requestNewRoute(com.navdy.client.app.framework.models.Destination destination, boolean initiatedOnHud) {
        if (destination == null) {
            logger.w("attempted to call requestNewRoute with no destination, no-op");
            return;
        }
        if (!initiatedOnHud && !this.requestedHighAccuracy) {
            try {
                if (android.provider.Settings.Secure.getInt(com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver(), "location_mode") != 3) {
                    this.requestedHighAccuracy = true;
                    android.content.Intent intent = new android.content.Intent(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.app.ui.settings.LocationDialogActivity.class);
                    intent.addFlags(268435456);
                    intent.putExtra(com.navdy.client.app.ui.settings.LocationDialogActivity.PENDING_DESTINATION, destination);
                    com.navdy.client.app.NavdyApplication.getAppContext().startActivity(intent);
                    return;
                }
            } catch (android.provider.Settings.SettingNotFoundException e) {
                logger.e("location setting not found: " + e);
            }
        }
        this.handler.removeCallbacks(this.refreshPendingRoute);
        destination.refreshPlaceIdDataAndUpdateListsAsync(new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon2(destination, initiatedOnHud));
    }

    public void retryRoute() {
        if (this.currentRoute == null) {
            logger.w("retryRoute but currentRoute is null, no-op");
        } else {
            requestNewRoute(this.currentRoute.destination);
        }
    }

    public void stopRouting() {
        if (this.currentState == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.INACTIVE) {
            logger.w("called stopRouting when state is INACTIVE, no-op");
            return;
        }
        switch (this.currentState) {
            case CALCULATING_PENDING_ROUTE:
                if (this.pendingRouteHandle != null) {
                    this.pendingRouteHandle.cancel();
                }
                this.pendingRouteHandle = null;
                break;
            case PENDING_ROUTE:
                this.handler.removeCallbacks(this.refreshPendingRoute);
                break;
            case CALCULATING_ROUTES:
                if (this.currentRoute.cancelHandle == null) {
                    logger.w("stopRouting, cancel handle can't be null");
                } else {
                    this.navigationHelper.cancelRouteCalculation(this.currentRoute.cancelHandle);
                }
                this.navigationHelper.cancelRoute(this.currentRoute);
                break;
            case ROUTE_CALCULATED:
            case EN_ROUTE:
                this.navigationHelper.cancelRoute(this.currentRoute);
                break;
        }
        removePendingTrip();
        setStoppedState();
        callListeners(new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon3());
    }

    @com.squareup.otto.Subscribe
    public void onHUDConnected(com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent event) {
        logger.v("HUD is connected");
        if (this.currentState == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.INACTIVE) {
            this.navigationHelper.getNavigationSessionState();
        } else if (this.currentState == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.PENDING_ROUTE) {
            this.handler.removeCallbacks(this.refreshPendingRoute);
        }
    }

    @com.squareup.otto.Subscribe
    public void onHUDDisconnected(com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent event) {
        logger.v("HUD is disconnected");
        if (this.currentState != com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.INACTIVE) {
            stopRouting();
        }
    }

    @com.squareup.otto.Subscribe
    public void onDisplayInitiatedRequestEvent(com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.DisplayInitiatedRequestEvent event) {
        if (this.currentState == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.CALCULATING_ROUTES || this.currentState == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.CALCULATING_PENDING_ROUTE || this.currentState == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.PENDING_ROUTE) {
            logger.i("onDisplayInitiatedRequestEvent but we have a pending route on the phone");
            return;
        }
        event.destination.updateLastRoutedDateInDb();
        event.destination.refreshPlaceIdDataAndUpdateListsAsync();
        removeLastDestinationArrived();
        removePendingTrip();
        logger.v("onDisplayInitiatedRequestEvent, setting state to CALCULATING_ROUTES");
        this.currentState = com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.CALCULATING_ROUTES;
        this.currentError = com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error.NONE;
        this.currentRoute = new com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo(event.destination, null, null, event.requestId, null, -1, -1, -1);
        callListeners(new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon4());
    }

    @com.squareup.otto.Subscribe
    public void onRouteSubmitProgress(com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.RouteCalculationProgress event) {
        if (this.currentState != com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.CALCULATING_ROUTES) {
            logger.w("onRouteSubmitProgress, state is not CALCULATING_ROUTES, no-op");
        } else if (this.currentRoute.requestId != null && !this.currentRoute.requestId.equals(event.requestId)) {
            logger.w("onRouteSubmitProgress, requestIds do not match, no-op");
        } else if (this.currentRoute.cancelHandle == null) {
            com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo updatedRoute = new com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo(this.currentRoute, null);
            updatedRoute.setHandle(event.handle);
            this.currentRoute = updatedRoute;
        }
    }

    @com.squareup.otto.Subscribe
    public void onRoutesCalculatedEvent(com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.RoutesCalculatedEvent event) {
        if (this.currentState != com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.CALCULATING_ROUTES) {
            logger.w("onRoutesCalculatedEvent, state is not CALCULATING_ROUTES, no-op");
        } else if (this.currentRoute.requestId == null || this.currentRoute.requestId.equals(event.requestId)) {
            com.navdy.client.app.framework.models.Destination currentDestination = this.currentRoute.destination;
            java.lang.String cancelHandle = this.currentRoute.cancelHandle;
            logger.v("onRoutesCalculatedEvent, setting state to ROUTE_CALCULATED");
            this.currentState = com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.ROUTE_CALCULATED;
            this.currentError = com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error.NONE;
            this.currentRoute = null;
            for (com.navdy.service.library.events.navigation.NavigationRouteResult navigationRouteResult : event.routes) {
                com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route = new com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo(currentDestination, navigationRouteResult.via, new com.here.android.mpa.common.GeoPolyline(getRouteGeo(navigationRouteResult.routeLatLongs)), event.requestId, navigationRouteResult.routeId, navigationRouteResult.duration.intValue(), navigationRouteResult.duration_traffic.intValue(), navigationRouteResult.length.intValue());
                route.setHandle(cancelHandle);
                if (this.currentRoute == null) {
                    this.currentRoute = route;
                }
            }
            callListeners(new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon5());
        } else {
            logger.w("onRoutesCalculatedEvent, requestIds are not the same, no-op");
        }
    }

    @com.squareup.otto.Subscribe
    public void onRoutesCalculationErrorEvent(com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.RoutesCalculationErrorEvent event) {
        if (this.currentState != com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.CALCULATING_ROUTES) {
            logger.w("onRoutesCalculationErrorEvent, state is not CALCULATING_ROUTES, no-op");
            return;
        }
        logger.v("onRoutesCalculationErrorEvent, setting state to CALCULATION_FAILED");
        this.currentState = com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.ROUTE_CALCULATED;
        this.currentError = com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error.NO_ROUTES;
        callListeners(new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon6());
    }

    @com.squareup.otto.Subscribe
    public void onStartActiveTripEvent(com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.StartActiveTripEvent event) {
        if (isInOneOfThePendingTripStates()) {
            logger.w("onStartActiveTripEvent, state is not an active route state, no-op");
        } else if (this.currentRoute == null || !com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(event.routeId, this.currentRoute.routeId)) {
            logger.i("onStartActiveTripEvent, routeIds in current and new are not the same");
            if (event.destination == null || event.route == null) {
                logger.w("trying to recover from insufficient data from the HUD");
                this.navigationHelper.getNavigationSessionState();
                return;
            }
            this.currentRoute = new com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo(event.destination, event.route.via, new com.here.android.mpa.common.GeoPolyline(getRouteGeo(event.route.routeLatLongs)), null, event.route.routeId, event.route.duration.intValue(), event.route.duration_traffic.intValue(), event.route.length.intValue());
            setEnRouteState();
            callListeners(new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon8());
        } else {
            setEnRouteState();
            callListeners(new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon7());
        }
    }

    @com.squareup.otto.Subscribe
    public void onRerouteActiveTripEvent(com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.RerouteActiveTripEvent event) {
        if (this.currentState != com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.EN_ROUTE) {
            logger.w("onRerouteActiveTripEvent, state is not EN_ROUTE, no-op");
        }
        this.navigationHelper.getNavigationSessionState();
        callListeners(new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon9());
    }

    @com.squareup.otto.Subscribe
    public void onArrivedTripEvent(com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.ArrivedTripEvent event) {
        if (this.currentState != com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.EN_ROUTE) {
            logger.w("onArrivedTripEvent, state must be EN_ROUTE, no-op");
            return;
        }
        com.navdy.client.app.ui.homescreen.CalendarUtils.forceCalendarRefresh();
        setLastDestinationArrived(this.lastDestinationArrived);
        callListeners(new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon10());
    }

    @com.squareup.otto.Subscribe
    public void onStopActiveTripEvent(com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.StopActiveTripEvent event) {
        boolean sameRequestIds;
        boolean sameRouteIds;
        boolean sameHandles;
        switch (this.currentState) {
            case INACTIVE:
                logger.v("onStopActiveTripEvent, state is already INACTIVE, no-op");
                return;
            case CALCULATING_PENDING_ROUTE:
            case PENDING_ROUTE:
                logger.w("onStopActiveTripEvent, state can't be " + this.currentState.name());
                return;
            default:
                logger.v("onStopActiveTripEvent.requestId: " + event.requestId);
                logger.v("currentRoute.requestId: " + this.currentRoute.requestId);
                logger.v("onStopActiveTripEvent.routeId: " + event.routeId);
                logger.v("currentRoute.routeId: " + this.currentRoute.routeId);
                logger.v("onStopActiveTripEvent.handle: " + event.handle);
                logger.v("currentRoute.handle: " + this.currentRoute.cancelHandle);
                if (event.requestId == null || !com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(event.requestId, this.currentRoute.requestId)) {
                    sameRequestIds = false;
                } else {
                    sameRequestIds = true;
                }
                if (event.routeId == null || !com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(event.routeId, this.currentRoute.routeId)) {
                    sameRouteIds = false;
                } else {
                    sameRouteIds = true;
                }
                if (event.handle == null || !com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(event.handle, this.currentRoute.cancelHandle)) {
                    sameHandles = false;
                } else {
                    sameHandles = true;
                }
                if (sameRequestIds || sameRouteIds || sameHandles) {
                    setStoppedState();
                    callListeners(new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon11());
                    this.navigationHelper.getNavigationSessionState();
                    return;
                }
                logger.w("canceling different stuff, no-op");
                return;
        }
    }

    @com.squareup.otto.Subscribe
    public void onHudReadyEvent(com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.HUDReadyEvent event) {
        logger.v("HUD is ready");
        if (this.currentState == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.CALCULATING_PENDING_ROUTE || this.currentState == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.PENDING_ROUTE) {
            logger.i("sending pending route to " + this.currentRoute.destination);
            requestNewRoute(this.currentRoute.destination);
        }
    }

    @com.squareup.otto.Subscribe
    public void onLimitCellDataEvent(com.navdy.client.app.ui.settings.GeneralSettingsActivity.LimitCellDataEvent event) {
        if (this.currentState != com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.PENDING_ROUTE) {
            return;
        }
        if (event.hasLimitedCellData) {
            this.handler.removeCallbacks(this.refreshPendingRoute);
        } else {
            calculatePendingRoute(this.currentRoute.destination);
        }
    }

    private void calculatePendingRoute(com.navdy.client.app.framework.models.Destination destination) {
        double latitude;
        double longitude;
        if (com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(destination.navigationLat, destination.navigationLong)) {
            latitude = destination.navigationLat;
            longitude = destination.navigationLong;
        } else if (com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(destination.displayLat, destination.displayLong)) {
            latitude = destination.displayLat;
            longitude = destination.displayLong;
        } else {
            this.currentState = com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.PENDING_ROUTE;
            this.currentError = com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error.NO_ROUTES;
            callListeners(new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon12());
            return;
        }
        com.navdy.client.app.framework.navigation.HereRouteManager.getInstance().calculateRoute(latitude, longitude, new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon13(destination));
    }

    private void updateCurrentProgress(com.here.android.mpa.common.GeoCoordinate coords, int tta, int distanceToDestination) {
        logger.v("updateCurrentProgressPolyline: " + coords);
        logger.v("updateCurrentProgressTTA: " + tta);
        logger.v("updateCurrentProgressDistance: " + distanceToDestination);
        if (this.currentRoute == null || this.currentRoute.route == null) {
            logger.w("trying to make progress without a route, no-op");
            return;
        }
        com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo updatedRoute = new com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo(this.currentRoute, null);
        updatedRoute.timeToDestination = tta;
        updatedRoute.distanceToDestination = distanceToDestination;
        int indexOnRoute = 0;
        if (updatedRoute.route != null) {
            indexOnRoute = updatedRoute.route.getNearestIndex(coords);
        }
        if (indexOnRoute > 1) {
            updatedRoute.progress = new com.here.android.mpa.common.GeoPolyline();
            for (int i = 0; i <= indexOnRoute; i++) {
                updatedRoute.progress.add(updatedRoute.route.getPoint(i));
            }
        }
        this.currentRoute = updatedRoute;
        callListeners(new com.navdy.client.app.framework.navigation.NavdyRouteHandler.Anon14());
    }

    private java.util.List<com.here.android.mpa.common.GeoCoordinate> getRouteGeo(java.util.List<java.lang.Float> routeLatLongs) {
        java.util.List<com.here.android.mpa.common.GeoCoordinate> routeGeo = new java.util.ArrayList<>();
        for (int i = 0; i < routeLatLongs.size() - 1; i += 2) {
            routeGeo.add(new com.here.android.mpa.common.GeoCoordinate((double) ((java.lang.Float) routeLatLongs.get(i)).floatValue(), (double) ((java.lang.Float) routeLatLongs.get(i + 1)).floatValue()));
        }
        return routeGeo;
    }

    private void setEnRouteState() {
        logger.v("setEnRouteState, setting state to EN_ROUTE");
        this.currentState = com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.EN_ROUTE;
        this.currentError = com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error.NONE;
        com.navdy.client.app.framework.models.Destination currentDestination = getCurrentDestination();
        if (currentDestination != null) {
            currentDestination.lastRoutedDate = new java.util.Date().getTime();
            currentDestination.updateLastRoutedDateInDbAsync();
        }
    }

    private void setStoppedState() {
        logger.v("setStoppedState, setting state to INACTIVE");
        this.currentState = com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.INACTIVE;
        this.currentError = com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error.NONE;
        this.currentRoute = null;
        com.navdy.client.app.framework.util.SuggestionManager.forceSuggestionFullRefresh();
        com.navdy.client.app.framework.util.SuggestionManager.rebuildSuggestionListAndSendToHudAsync();
        com.navdy.client.app.framework.location.NavdyLocationManager.forceLastKnownCountryCodeUpdate();
    }

    private void callListeners(com.navdy.client.app.framework.navigation.NavdyRouteHandler.CallListener callListener) {
        for (java.lang.ref.WeakReference<com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener> listenerWeakReference : this.listeners) {
            com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener listener = (com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener) listenerWeakReference.get();
            if (listener != null) {
                callListener.call(listener);
            }
        }
    }

    private com.navdy.client.app.framework.models.Destination getPendingTrip() {
        return getCachedDestination(SAVED_DESTINATION);
    }

    private void setPendingTrip(com.navdy.client.app.framework.models.Destination destination) {
        setCachedDestination(SAVED_DESTINATION, destination);
    }

    private void removePendingTrip() {
        removeCachedDestination(SAVED_DESTINATION);
    }

    private com.navdy.client.app.framework.models.Destination getLastDestinationArrived() {
        return getCachedDestination(CACHED_ARRIVED_DESTINATION);
    }

    private void setLastDestinationArrived(com.navdy.client.app.framework.models.Destination destination) {
        this.lastDestinationArrived = this.currentRoute.destination;
        setCachedDestination(CACHED_ARRIVED_DESTINATION, destination);
    }

    private void removeLastDestinationArrived() {
        this.lastDestinationArrived = null;
        removeCachedDestination(CACHED_ARRIVED_DESTINATION);
    }

    private com.navdy.client.app.framework.models.Destination getCachedDestination(java.lang.String cacheKey) {
        java.lang.String destinationString = com.navdy.client.app.NavdyApplication.getAppContext().getSharedPreferences(SHARED_PREF_SAVED_ROUTE, 0).getString(cacheKey, null);
        logger.d("Cached destination is: " + destinationString);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destinationString)) {
            return (com.navdy.client.app.framework.models.Destination) new com.google.gson.Gson().fromJson(destinationString, com.navdy.client.app.framework.models.Destination.class);
        }
        return null;
    }

    private void setCachedDestination(java.lang.String cacheKey, com.navdy.client.app.framework.models.Destination destination) {
        if (destination == null) {
            logger.e("Unable to save pending trip. Provided destination is null !");
            return;
        }
        android.content.SharedPreferences.Editor editor = com.navdy.client.app.NavdyApplication.getAppContext().getSharedPreferences(SHARED_PREF_SAVED_ROUTE, 0).edit();
        try {
            editor.putString(cacheKey, new com.google.gson.Gson().toJson((java.lang.Object) destination));
            editor.apply();
            logger.d("Saving this as pending trip: " + destination);
        } catch (com.google.gson.JsonSyntaxException | java.lang.IllegalStateException e) {
            logger.e((java.lang.Throwable) e);
        }
    }

    private void removeCachedDestination(java.lang.String cacheKey) {
        android.content.SharedPreferences.Editor editor = com.navdy.client.app.NavdyApplication.getAppContext().getSharedPreferences(SHARED_PREF_SAVED_ROUTE, 0).edit();
        try {
            editor.remove(cacheKey);
            editor.apply();
            logger.d("Removed the pending trip.");
        } catch (com.google.gson.JsonSyntaxException | java.lang.IllegalStateException e) {
            logger.e((java.lang.Throwable) e);
        }
    }
}
