package com.navdy.client.app.framework.servicehandler;

public class PhotoServiceHandler {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.PhotoServiceHandler.class);
    private android.content.Context context;
    private int thumbnailSize = com.navdy.client.app.NavdyApplication.getAppContext().getResources().getInteger(com.navdy.client.R.integer.thumbnailImageDimension);

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.photo.PhotoRequest val$request;

        Anon1(com.navdy.service.library.events.photo.PhotoRequest photoRequest) {
            this.val$request = photoRequest;
        }

        public void run() {
            android.graphics.Bitmap photo = null;
            try {
                if (com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT.equals(this.val$request.photoType)) {
                    photo = com.navdy.client.app.framework.servicehandler.PhotoServiceHandler.this.getContactPhoto(this.val$request.identifier, com.navdy.client.app.framework.servicehandler.PhotoServiceHandler.this.thumbnailSize);
                } else if (com.navdy.service.library.events.photo.PhotoType.PHOTO_ALBUM_ART.equals(this.val$request.photoType)) {
                    photo = com.navdy.client.app.framework.util.MusicUtils.getMusicPhoto(this.val$request.identifier);
                } else if (com.navdy.service.library.events.photo.PhotoType.PHOTO_DRIVER_PROFILE.equals(this.val$request.photoType)) {
                    photo = com.navdy.client.app.tracking.Tracker.getScaledProfilePhoto(com.navdy.client.app.framework.servicehandler.PhotoServiceHandler.this.thumbnailSize);
                }
                if (photo == null) {
                    com.navdy.client.app.framework.servicehandler.PhotoServiceHandler.sLogger.v("PhotoServiceHandler:Cannot find requested photo:" + this.val$request.identifier);
                } else {
                    com.navdy.client.app.framework.servicehandler.PhotoServiceHandler.sLogger.v("PhotoServiceHandler:found photo:" + this.val$request.identifier);
                }
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) com.navdy.client.app.framework.servicehandler.PhotoServiceHandler.buildPhotoResponse(photo, this.val$request));
            } catch (Throwable t) {
                com.navdy.client.app.framework.servicehandler.PhotoServiceHandler.sLogger.e("PhotoServiceHandler:" + this.val$request.identifier, t);
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.photo.PhotoResponse(null, com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, null, this.val$request.identifier, null, this.val$request.photoType));
            }
        }
    }

    class Anon2 implements com.navdy.client.app.framework.util.ImageUtils.StreamFactory {
        final /* synthetic */ android.content.ContentResolver val$cr;
        final /* synthetic */ android.net.Uri val$photoUri;

        Anon2(android.content.ContentResolver contentResolver, android.net.Uri uri) {
            this.val$cr = contentResolver;
            this.val$photoUri = uri;
        }

        public java.io.InputStream getInputStream() throws java.io.FileNotFoundException {
            return android.provider.ContactsContract.Contacts.openContactPhotoInputStream(this.val$cr, this.val$photoUri);
        }
    }

    public PhotoServiceHandler(android.content.Context context2) {
        this.context = context2;
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    @com.squareup.otto.Subscribe
    public void onPhotoRequest(com.navdy.service.library.events.photo.PhotoRequest request) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.PhotoServiceHandler.Anon1(request), 1);
    }

    public static java.lang.String buildPhotoChecksum(android.graphics.Bitmap photo) {
        java.lang.String checksum = "";
        if (photo == null) {
            sLogger.w("Trying to buildPhotoChecksum from a null photo.");
            return checksum;
        }
        try {
            checksum = com.navdy.service.library.util.IOUtils.hashForBytes(com.navdy.service.library.util.IOUtils.bitmap2ByteBuffer(photo));
        } catch (java.lang.Exception e) {
            sLogger.d("Error computing checksum for the bitmap data", e);
        }
        return checksum;
    }

    private static com.navdy.service.library.events.photo.PhotoResponse buildPhotoResponse(android.graphics.Bitmap photo, com.navdy.service.library.events.photo.PhotoRequest request) {
        if (photo == null) {
            return new com.navdy.service.library.events.photo.PhotoResponse(null, com.navdy.service.library.events.RequestStatus.REQUEST_NOT_AVAILABLE, null, request.identifier, null, request.photoType);
        }
        boolean changed = true;
        java.lang.String newHash = null;
        byte[] data = com.navdy.service.library.util.IOUtils.bitmap2ByteBuffer(photo);
        try {
            newHash = com.navdy.service.library.util.IOUtils.hashForBytes(data);
            if (newHash != null && newHash.equals(request.photoChecksum)) {
                changed = false;
            }
        } catch (java.lang.Exception e) {
            sLogger.d("Error computing hash for the bitmap data", e);
        }
        okio.ByteString byteStringPhoto = okio.ByteString.of(data);
        sLogger.v("PhotoServiceHandler: changed:" + changed + " size:" + byteStringPhoto.size());
        return new com.navdy.service.library.events.photo.PhotoResponse(changed ? byteStringPhoto : null, com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, null, request.identifier, newHash, request.photoType);
    }

    private android.graphics.Bitmap getContactPhoto(java.lang.String identifier, int thumbnailSize2) {
        if (identifier == null || identifier.length() == 0) {
            return null;
        }
        if (!isNumber(identifier)) {
            return getContactPhotoViaName(identifier, thumbnailSize2);
        }
        android.graphics.Bitmap bitmap = getContactPhotoViaPhoneNumber(identifier, thumbnailSize2);
        if (bitmap == null) {
            return getContactPhotoViaName(identifier, thumbnailSize2);
        }
        return bitmap;
    }

    private android.graphics.Bitmap getContactPhotoViaPhoneNumber(java.lang.String identifier, int thumbnailSize2) {
        android.net.Uri phoneUri = android.net.Uri.withAppendedPath(android.provider.ContactsContract.PhoneLookup.CONTENT_FILTER_URI, android.net.Uri.encode(identifier));
        android.content.ContentResolver cr = this.context.getContentResolver();
        android.database.Cursor contactCursor = null;
        try {
            contactCursor = cr.query(phoneUri, new java.lang.String[]{"_id"}, null, null, null);
            if (contactCursor != null && contactCursor.moveToFirst()) {
                return getContactPhoto(cr, contactCursor, thumbnailSize2);
            }
            com.navdy.service.library.util.IOUtils.closeStream(contactCursor);
            return null;
        } catch (Throwable t) {
            sLogger.e(t);
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(contactCursor);
        }
    }

    private android.graphics.Bitmap getContactPhotoViaName(java.lang.String identifier, int thumbnailSize2) {
        android.content.ContentResolver cr = this.context.getContentResolver();
        java.lang.String selection = "lower(display_name) LIKE '" + identifier.toLowerCase() + "%'";
        android.database.Cursor contact = null;
        try {
            contact = cr.query(android.provider.ContactsContract.Contacts.CONTENT_URI, new java.lang.String[]{"_id"}, selection, null, null);
            if (contact != null && contact.moveToFirst()) {
                return getContactPhoto(cr, contact, thumbnailSize2);
            }
            com.navdy.service.library.util.IOUtils.closeStream(contact);
            return null;
        } catch (Throwable t) {
            sLogger.e(t);
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(contact);
        }
    }

    private android.graphics.Bitmap getContactPhoto(android.content.ContentResolver cr, android.database.Cursor contact, int thumbnailSize2) {
        android.net.Uri photoUri = android.content.ContentUris.withAppendedId(android.provider.ContactsContract.Contacts.CONTENT_URI, contact.getLong(contact.getColumnIndex("_id")));
        if (photoUri != null) {
            return com.navdy.client.app.framework.util.ImageUtils.getScaledBitmap((com.navdy.client.app.framework.util.ImageUtils.StreamFactory) new com.navdy.client.app.framework.servicehandler.PhotoServiceHandler.Anon2(cr, photoUri), thumbnailSize2, thumbnailSize2);
        }
        return null;
    }

    private boolean isNumber(java.lang.String identifier) {
        try {
            java.lang.Long.parseLong(identifier.replaceAll("[^a-zA-Z0-9\\._]+", ""));
            return true;
        } catch (Throwable th) {
            return false;
        }
    }
}
