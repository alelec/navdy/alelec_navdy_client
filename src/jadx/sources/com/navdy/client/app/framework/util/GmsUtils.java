package com.navdy.client.app.framework.util;

public class GmsUtils {
    private static final int LATEST_VERSION_OF_GMS = 10084000;
    private static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.GmsUtils.class);

    public enum GmsState {
        UP_TO_DATE,
        OUT_OF_DATE,
        MISSING
    }

    public static com.navdy.client.app.framework.util.GmsUtils.GmsState checkIfMissingOrOutOfDate() {
        android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        int gmsVersion = sharedPreferences.getInt(com.navdy.client.app.ui.settings.SettingsConstants.GMS_VERSION_NUMBER, -1);
        boolean gmsIsMissing = sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.GMS_IS_MISSING, false);
        sLogger.i("From Shared Prefs: GMS version code is " + gmsVersion + " and gmsIsMissing is " + gmsIsMissing);
        if (!gmsIsMissing && gmsVersion != -1 && isUpToDateGmsVersion(gmsVersion)) {
            return com.navdy.client.app.framework.util.GmsUtils.GmsState.UP_TO_DATE;
        }
        android.content.pm.PackageManager packageManager = com.navdy.client.app.NavdyApplication.getAppContext().getPackageManager();
        if (packageManager != null) {
            boolean found = false;
            java.util.Iterator it = packageManager.getInstalledPackages(128).iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                android.content.pm.PackageInfo packageInfo = (android.content.pm.PackageInfo) it.next();
                if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(packageInfo.packageName, "com.google.android.gms")) {
                    found = true;
                    sLogger.i("Found GMS. version code is " + packageInfo.versionCode + " and versionName is " + packageInfo.versionName);
                    android.content.SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putInt(com.navdy.client.app.ui.settings.SettingsConstants.GMS_VERSION_NUMBER, packageInfo.versionCode);
                    edit.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.GMS_IS_MISSING, false);
                    edit.apply();
                    if (!isUpToDateGmsVersion(packageInfo.versionCode)) {
                        return com.navdy.client.app.framework.util.GmsUtils.GmsState.OUT_OF_DATE;
                    }
                }
            }
            if (!found) {
                sLogger.e("Could not find GMS !");
                android.content.SharedPreferences.Editor edit2 = sharedPreferences.edit();
                edit2.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.GMS_IS_MISSING, true);
                edit2.apply();
                return com.navdy.client.app.framework.util.GmsUtils.GmsState.MISSING;
            }
        }
        return com.navdy.client.app.framework.util.GmsUtils.GmsState.UP_TO_DATE;
    }

    public static boolean isUpToDateGmsVersion(int versionCode) {
        return versionCode == -1 || versionCode >= LATEST_VERSION_OF_GMS;
    }

    public static boolean finishIfGmsIsNotUpToDate(android.app.Activity activity) {
        com.navdy.client.app.framework.util.GmsUtils.GmsState gmsState = checkIfMissingOrOutOfDate();
        if (gmsState != com.navdy.client.app.framework.util.GmsUtils.GmsState.UP_TO_DATE) {
            if (gmsState == com.navdy.client.app.framework.util.GmsUtils.GmsState.OUT_OF_DATE) {
                sLogger.e("OUTDATED GOOGLE PLAY SERVICES!");
                com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.outdated_gms_title, new java.lang.Object[0]);
            } else if (gmsState == com.navdy.client.app.framework.util.GmsUtils.GmsState.MISSING) {
                sLogger.e("NO GOOGLE PLAY SERVICES!");
                com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.no_google_play_services, new java.lang.Object[0]);
            }
            if (activity != null) {
                sLogger.e("Killing activity, GMS error");
                activity.finish();
            } else {
                sLogger.e("KILLING APP, GMS ERROR");
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        }
        if (gmsState != com.navdy.client.app.framework.util.GmsUtils.GmsState.UP_TO_DATE) {
            return true;
        }
        return false;
    }
}
