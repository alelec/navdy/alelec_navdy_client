package com.navdy.client.app.framework.search;

public class MusicBrainzArtworkSearch {
    private static final java.lang.String API_URL_RELEASE_PREFIX = "http://musicbrains.org/ws/2/release/?fmt=json&limit=1&query=";
    private static final java.lang.String ARTIST_ALBUM_SEPARATOR = " // ";
    private static final int BITMAP_CACHE_SIZE = 10;
    private static final java.lang.String COVER_ART_URL_POSTFIX = "/front-250";
    private static final java.lang.String COVER_ART_URL_PREFIX = "http://coverartarchive.org/release/";
    private static final android.util.LruCache<java.lang.String, android.graphics.Bitmap> artworkCache = new android.util.LruCache<>(10);
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.search.MusicBrainzArtworkSearch.class);

    public static android.graphics.Bitmap getArtwork(java.lang.String artist, java.lang.String album) {
        sLogger.v("Sending artwork query for " + album + " from " + artist);
        boolean isArtistSet = !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(artist);
        boolean isAlbumSet = !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(album);
        if (!isArtistSet && !isAlbumSet) {
            return null;
        }
        java.lang.String cacheKey = buildCacheKey(artist, album);
        android.graphics.Bitmap result = (android.graphics.Bitmap) artworkCache.get(cacheKey);
        if (result != null) {
            sLogger.v("Found artwork in MusicBrainz client class cache");
            return result;
        }
        java.lang.StringBuilder queryBuilder = new java.lang.StringBuilder();
        if (isArtistSet) {
            queryBuilder.append("artist:\"").append(artist).append("\"");
        }
        if (isAlbumSet) {
            if (isArtistSet) {
                queryBuilder.append(org.droidparts.contract.SQL.AND);
            }
            queryBuilder.append("release:\"").append(album).append("\"");
        }
        if (queryBuilder.length() == 0) {
            sLogger.w("No artist nor album name is given");
            return null;
        }
        try {
            org.json.JSONObject albumData = com.navdy.service.library.util.IOUtils.getJSONFromURL(API_URL_RELEASE_PREFIX + java.net.URLEncoder.encode(queryBuilder.toString(), "UTF-8"));
            if (albumData == null) {
                sLogger.e("Album not found on MusicBrainz");
                return null;
            }
            try {
                byte[] img = com.navdy.service.library.util.IOUtils.downloadImage(COVER_ART_URL_PREFIX + ((org.json.JSONObject) albumData.getJSONArray("releases").get(0)).getString("id") + COVER_ART_URL_POSTFIX);
                android.graphics.Bitmap result2 = android.graphics.BitmapFactory.decodeByteArray(img, 0, img.length);
                sLogger.v("Got album artwork from MusicBrainz with size " + img.length);
                artworkCache.put(cacheKey, result2);
                return result2;
            } catch (java.lang.IndexOutOfBoundsException | java.lang.NullPointerException | org.json.JSONException e) {
                sLogger.e("Cannot read album's ID from MusicBrainz data", e);
                return null;
            }
        } catch (java.io.UnsupportedEncodingException e2) {
            sLogger.e("Cannot build query for album ID", e2);
            return null;
        }
    }

    private static java.lang.String buildCacheKey(@android.support.annotation.Nullable java.lang.String artist, @android.support.annotation.Nullable java.lang.String album) {
        return ARTIST_ALBUM_SEPARATOR + album;
    }
}
