package com.navdy.client.app.framework.suggestion;

public class DestinationSuggestionService extends android.app.IntentService {
    private static final java.lang.String BUCKET_NAME = "navdy-trip-data";
    public static final java.lang.String COMMAND_AUTO_UPLOAD_TRIP_DATA = "COMMAND_AUTO_UPLOAD";
    public static final java.lang.String COMMAND_POPULATE = "COMMAND_POPULATE";
    public static final java.lang.String COMMAND_RESET = "COMMAND_RESET";
    public static final java.lang.String COMMAND_SUGGEST = "COMMAND_SUGGEST";
    public static final java.lang.String COMMAND_UPLOAD_TRIP_DATA = "COMMAND_UPLOAD";
    private static final java.lang.String EXTRA_LOCAL = "EXTRA_LOCAL";
    private static final long MINIMUM_INTERVAL_BETWEEN_UPLOADS = java.util.concurrent.TimeUnit.DAYS.toMillis(1);
    private static com.navdy.client.app.framework.models.Destination NO_SUGGESTION = null;
    public static final java.lang.String PLACE_SUGGESTION_LIBRARY_NAME = "placesuggestion";
    private static final java.lang.String PREFERENCE_LAST_UPLOAD_ID = "PREF_LAST_UPLOAD_ID";
    private static final java.lang.String PREFERENCE_LAST_UPLOAD_TIME = "PREF_LAST_UPLOAD_TIME";
    private static final java.lang.String SERVICE_NAME = "SERVICE_NAME";
    public static final int SUGGESTED_DESTINATION_TRESHOLD = 200;
    public static final int TRIP_TRESHOLD_FOR_UPLOAD = 10;
    public static final int TRIP_UPLOAD_SERVICE_REQ = 256;
    private static boolean sColdStart = true;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.suggestion.DestinationSuggestionService.class);
    private static long sMaxIdentifier = -1;

    class Anon1 implements com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener {
        Anon1() {
        }

        public void onSuggestionBuildComplete(java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> suggestions) {
            if (com.navdy.client.app.framework.suggestion.DestinationSuggestionService.this.firstSuggestionHasValidDestination(suggestions)) {
                com.navdy.client.app.framework.suggestion.DestinationSuggestionService.this.sendSuggestionToHud(new com.navdy.client.app.framework.models.Suggestion(((com.navdy.client.app.framework.models.Suggestion) suggestions.get(0)).destination, com.navdy.client.app.framework.models.Suggestion.SuggestionType.CALENDAR), com.navdy.service.library.events.places.SuggestedDestination.SuggestionType.SUGGESTION_CALENDAR);
                return;
            }
            com.navdy.client.app.framework.suggestion.DestinationSuggestionService.this.suggestDestinationBasedOnContext(false);
        }
    }

    class Anon2 implements com.navdy.client.app.framework.navigation.HereRouteManager.Listener {
        final /* synthetic */ com.navdy.service.library.events.destination.Destination val$destinationMessage;
        final /* synthetic */ com.navdy.service.library.events.places.SuggestedDestination.SuggestionType val$suggestionType;

        Anon2(com.navdy.service.library.events.destination.Destination destination, com.navdy.service.library.events.places.SuggestedDestination.SuggestionType suggestionType) {
            this.val$destinationMessage = destination;
            this.val$suggestionType = suggestionType;
        }

        public void onPreCalculation(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.HereRouteManager.RouteHandle routeHandle) {
        }

        public void onRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.HereRouteManager.Error error, com.here.android.mpa.routing.Route route) {
            int routeDurationWithTraffic = -1;
            if (error == com.navdy.client.app.framework.navigation.HereRouteManager.Error.NONE && route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, com.here.android.mpa.routing.Route.WHOLE_ROUTE) != null) {
                routeDurationWithTraffic = route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, com.here.android.mpa.routing.Route.WHOLE_ROUTE).getDuration();
            }
            com.navdy.client.app.framework.suggestion.DestinationSuggestionService.this.sendSuggestion(new com.navdy.service.library.events.places.SuggestedDestination(this.val$destinationMessage, java.lang.Integer.valueOf(routeDurationWithTraffic), this.val$suggestionType));
        }
    }

    class Anon3 {
        final /* synthetic */ com.squareup.otto.Bus val$bus;
        final /* synthetic */ long val$maxTripId;
        final /* synthetic */ android.content.SharedPreferences val$sharedPreferences;

        Anon3(android.content.SharedPreferences sharedPreferences, long j, com.squareup.otto.Bus bus) {
            this.val$sharedPreferences = sharedPreferences;
            this.val$maxTripId = j;
            this.val$bus = bus;
        }

        @com.squareup.otto.Subscribe
        public void onProgress(com.navdy.client.app.framework.suggestion.DestinationSuggestionService.UploadResult uploadResult) {
            if (uploadResult == com.navdy.client.app.framework.suggestion.DestinationSuggestionService.UploadResult.SUCCESS) {
                this.val$sharedPreferences.edit().putLong(com.navdy.client.app.framework.suggestion.DestinationSuggestionService.PREFERENCE_LAST_UPLOAD_TIME, java.lang.System.currentTimeMillis()).putLong(com.navdy.client.app.framework.suggestion.DestinationSuggestionService.PREFERENCE_LAST_UPLOAD_ID, this.val$maxTripId).apply();
                com.navdy.client.app.framework.suggestion.DestinationSuggestionService.this.scheduleNextCheckForUpdate();
            }
            this.val$bus.unregister(this);
        }
    }

    class Anon4 implements com.amazonaws.mobileconnectors.s3.transferutility.TransferListener {
        final /* synthetic */ com.squareup.otto.Bus val$bus;
        final /* synthetic */ java.io.File val$file;

        Anon4(java.io.File file, com.squareup.otto.Bus bus) {
            this.val$file = file;
            this.val$bus = bus;
        }

        public void onStateChanged(int id, com.amazonaws.mobileconnectors.s3.transferutility.TransferState state) {
            com.navdy.client.app.framework.suggestion.DestinationSuggestionService.sLogger.d("Uploading File " + this.val$file.getName() + ", onStatChanged : " + id + org.droidparts.contract.SQL.DDL.SEPARATOR + state.name());
            if (state == com.amazonaws.mobileconnectors.s3.transferutility.TransferState.COMPLETED) {
                if (this.val$bus != null) {
                    this.val$bus.post(com.navdy.client.app.framework.suggestion.DestinationSuggestionService.UploadResult.SUCCESS);
                }
            } else if (state == com.amazonaws.mobileconnectors.s3.transferutility.TransferState.FAILED && this.val$bus != null) {
                this.val$bus.post(com.navdy.client.app.framework.suggestion.DestinationSuggestionService.UploadResult.FAILURE);
            }
        }

        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            com.navdy.client.app.framework.suggestion.DestinationSuggestionService.sLogger.d("Uploading File " + this.val$file.getName() + ", onProgressChanged : " + id + ", Size :" + bytesCurrent + ", of " + bytesTotal);
        }

        public void onError(int id, java.lang.Exception ex) {
            com.navdy.client.app.framework.suggestion.DestinationSuggestionService.sLogger.e("Uploading File " + this.val$file.getName() + ", onError : " + id, ex);
            if (this.val$bus != null) {
                this.val$bus.post(com.navdy.client.app.framework.suggestion.DestinationSuggestionService.UploadResult.FAILURE);
            }
        }
    }

    private enum UploadResult {
        SUCCESS,
        FAILURE
    }

    public static native void learn(double d, double d2, long j, double d3, double d4, int i, int i2, int i3);

    public static native void reset();

    public static native com.navdy.client.app.framework.suggestion.SuggestedDestination suggestDestination(double d, double d2, int i, int i2, int i3);

    static {
        NO_SUGGESTION = new com.navdy.client.app.framework.models.Destination();
        java.lang.System.loadLibrary(PLACE_SUGGESTION_LIBRARY_NAME);
        NO_SUGGESTION = new com.navdy.client.app.framework.models.Destination();
        NO_SUGGESTION.setId(-1);
    }

    public void onCreate() {
        super.onCreate();
    }

    public DestinationSuggestionService() {
        super(SERVICE_NAME);
    }

    protected void onHandleIntent(android.content.Intent intent) {
        if (intent == null) {
            sLogger.e("received a call for onHandleIntent with a null intent!");
            return;
        }
        java.lang.String action = intent.getAction();
        if (COMMAND_SUGGEST.equals(action)) {
            handleSuggest(intent);
        } else if (COMMAND_POPULATE.equals(action)) {
            handlePopulate();
        } else if (COMMAND_UPLOAD_TRIP_DATA.equals(action)) {
            handleUploadTripData();
        } else if (COMMAND_AUTO_UPLOAD_TRIP_DATA.equals(action)) {
            handleAutoTripUpload();
        } else if (COMMAND_RESET.equals(action)) {
            reset();
            sColdStart = true;
        }
    }

    private void handleSuggest(android.content.Intent intent) {
        boolean forThePhoneSuggestionList = intent.getBooleanExtra(EXTRA_LOCAL, true);
        sLogger.d("onHandleIntent : Command suggest, forThePhoneSuggestionList = " + forThePhoneSuggestionList);
        try {
            learnAllTrips();
            if (forThePhoneSuggestionList) {
                suggestDestinationBasedOnContext(true);
                return;
            }
            com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
            if (remoteDevice == null || !remoteDevice.isConnected()) {
                sLogger.d("Remote device is not connected, so not making a suggestion");
            } else if (com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance().getCurrentState() == com.navdy.client.app.framework.navigation.NavdyRouteHandler.State.EN_ROUTE) {
                sLogger.d("There is an active trip going on in HUD, so not sending suggestion");
            } else {
                com.navdy.client.app.framework.util.SuggestionManager.buildCalendarSuggestions(new com.navdy.client.app.framework.suggestion.DestinationSuggestionService.Anon1());
            }
        } catch (Throwable t) {
            sLogger.e("Error during destination suggestion ", t);
        }
    }

    private boolean firstSuggestionHasValidDestination(java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> suggestions) {
        return (suggestions == null || suggestions.size() <= 0 || suggestions.get(0) == null || ((com.navdy.client.app.framework.models.Suggestion) suggestions.get(0)).destination == null || !((com.navdy.client.app.framework.models.Suggestion) suggestions.get(0)).destination.hasOneValidSetOfCoordinates()) ? false : true;
    }

    private void suggestDestinationBasedOnContext(boolean forThePhoneSuggestionList) {
        sLogger.d("Suggest Destination based on the context For suggestion list :" + forThePhoneSuggestionList);
        java.util.Date date = new java.util.Date(java.lang.System.currentTimeMillis());
        java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.getDefault());
        cal.setTime(date);
        int hour = cal.get(11);
        int minute = cal.get(12);
        int dayOfTheWeek = cal.get(7);
        com.navdy.client.app.framework.suggestion.SuggestedDestination destination = null;
        com.navdy.service.library.events.location.Coordinate currentCoordinate = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance().getSmartStartCoordinates();
        if (currentCoordinate != null) {
            sLogger.d("Current location is known: " + currentCoordinate.latitude + org.droidparts.contract.SQL.DDL.SEPARATOR + currentCoordinate.longitude);
            destination = suggestDestination(currentCoordinate.latitude.doubleValue(), currentCoordinate.longitude.doubleValue(), hour, minute, dayOfTheWeek);
        } else {
            sLogger.d("Current location is unknown");
            if (forThePhoneSuggestionList) {
                destination = suggestDestination(0.0d, 0.0d, hour, minute, dayOfTheWeek);
            }
        }
        com.navdy.client.app.framework.models.Destination savedDestination = null;
        if (destination != null) {
            savedDestination = com.navdy.client.app.providers.NavdyContentProvider.getThisDestination((int) destination.uniqueIdentifier);
            if (savedDestination != null) {
                android.location.Location currentLocation = new android.location.Location("");
                if (currentCoordinate != null) {
                    currentLocation.setLatitude(currentCoordinate.latitude.doubleValue());
                    currentLocation.setLongitude(currentCoordinate.longitude.doubleValue());
                }
                android.location.Location location = new android.location.Location("");
                location.setLatitude(savedDestination.displayLat);
                location.setLongitude(savedDestination.displayLong);
                if (((double) location.distanceTo(currentLocation)) <= 200.0d) {
                    if (forThePhoneSuggestionList) {
                        com.navdy.client.app.framework.util.BusProvider.getInstance().post(NO_SUGGESTION);
                    }
                    sLogger.d("Not suggesting as the destination with the identifier is very close to the current location");
                    sLogger.d("DestinationEngine , suggested destiantion,  ID: " + destination.uniqueIdentifier + ", Lat: " + destination.latitude + ", Long: " + destination.longitude + ", Frequency: " + destination.frequency + ", Probability: " + destination.probability);
                    return;
                } else if (forThePhoneSuggestionList) {
                    com.navdy.client.app.framework.util.BusProvider.getInstance().post(savedDestination);
                } else {
                    sLogger.d("DestinationEngine: suggested destiantion: ID = " + destination.uniqueIdentifier + ", Lat = " + destination.latitude + ", Long = " + destination.longitude + ", Frequency = " + destination.frequency + ", Probability = " + destination.probability);
                    com.navdy.client.app.framework.models.Suggestion suggestion = new com.navdy.client.app.framework.models.Suggestion(savedDestination, com.navdy.client.app.framework.models.Suggestion.SuggestionType.RECOMMENDATION);
                    sendSuggestionToHud(suggestion, com.navdy.service.library.events.places.SuggestedDestination.SuggestionType.SUGGESTION_RECOMMENDATION);
                }
            }
        }
        if (forThePhoneSuggestionList && savedDestination == null) {
            com.navdy.client.app.framework.util.BusProvider.getInstance().post(NO_SUGGESTION);
        }
    }

    private void sendSuggestionToHud(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Suggestion suggestion, com.navdy.service.library.events.places.SuggestedDestination.SuggestionType suggestionType) {
        double longitude;
        double latitude;
        com.navdy.service.library.events.destination.Destination destinationMessage = suggestion.toProtobufDestinationObject();
        if (destinationMessage != null) {
            sLogger.d("DestinationEngine , destination message,  Title :" + destinationMessage.destination_title + ", Subtitle :" + destinationMessage.destination_subtitle + ", Type :" + destinationMessage.suggestion_type + ", Is Recommendation :" + destinationMessage.is_recommendation + ", Address :" + destinationMessage.full_address + ", Unique ID: " + destinationMessage.identifier);
            if (destinationMessage.navigation_position.latitude.doubleValue() == 0.0d && destinationMessage.navigation_position.longitude.doubleValue() == 0.0d) {
                latitude = destinationMessage.display_position.latitude.doubleValue();
                longitude = destinationMessage.display_position.longitude.doubleValue();
            } else {
                latitude = destinationMessage.navigation_position.latitude.doubleValue();
                longitude = destinationMessage.navigation_position.longitude.doubleValue();
            }
            com.navdy.client.app.framework.navigation.HereRouteManager.getInstance().calculateRoute(latitude, longitude, new com.navdy.client.app.framework.suggestion.DestinationSuggestionService.Anon2(destinationMessage, suggestionType));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x01cb A[SYNTHETIC, Splitter:B:30:0x01cb] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x01d0 A[Catch:{ IOException -> 0x02a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x02b4 A[SYNTHETIC, Splitter:B:67:0x02b4] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x02b9 A[Catch:{ IOException -> 0x02bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:91:? A[RETURN, SYNTHETIC] */
    private void handlePopulate() {
        java.io.File file = new java.io.File(android.os.Environment.getExternalStorageDirectory() + java.io.File.separator + "data.csv");
        if (file.exists()) {
            java.io.FileReader reader = null;
            java.io.BufferedReader buffReader = null;
            try {
                java.io.FileReader fileReader = new java.io.FileReader(file);
                try {
                    java.io.BufferedReader bufferedReader = new java.io.BufferedReader(fileReader);
                    while (true) {
                        try {
                            java.lang.String line = bufferedReader.readLine();
                            if (line == null) {
                                break;
                            }
                            if (!line.startsWith("Z_PK")) {
                                java.lang.String[] parts = line.split(",");
                                long id = java.lang.Long.parseLong(parts[0]);
                                long tripNumber = java.lang.Long.parseLong(parts[5]);
                                long arrivedDestination = !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(parts[6]) ? java.lang.Long.parseLong(parts[6]) : -1;
                                long chosenDestination = !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(parts[7]) ? java.lang.Long.parseLong(parts[7]) : -1;
                                long inferredDestination = !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(parts[8]) ? java.lang.Long.parseLong(parts[8]) : -1;
                                long destinationId = arrivedDestination != -1 ? arrivedDestination : chosenDestination != -1 ? chosenDestination : inferredDestination != -1 ? inferredDestination : -1;
                                double destLatitude = java.lang.Double.parseDouble(parts[9]);
                                double destLongitude = java.lang.Double.parseDouble(parts[10]);
                                long endTime = (long) (java.lang.Double.parseDouble(parts[11]) + 9.783072E8d);
                                double startLatitude = java.lang.Double.parseDouble(parts[12]);
                                double startLongitude = java.lang.Double.parseDouble(parts[13]);
                                long startTime = (long) (java.lang.Double.parseDouble(parts[14]) + 9.783072E8d);
                                sLogger.d("Start Time " + startTime);
                                int offset = com.navdy.client.app.framework.util.SystemUtils.getTimeZoneAndDaylightSavingOffset(java.lang.Long.valueOf(startTime));
                                java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.getDefault());
                                cal.setTime(new java.util.Date(1000 * startTime));
                                int hour = cal.get(11);
                                int minute = cal.get(12);
                                int dayOfTheWeek = cal.get(7);
                                sLogger.d("MM/DD/YYY " + cal.get(2) + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + cal.get(5) + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + cal.get(1) + " , Day :" + dayOfTheWeek);
                                new com.navdy.client.app.framework.models.Trip((int) id, tripNumber, startTime, offset, 0, startLatitude, startLongitude, endTime, 0, destLatitude, destLongitude, arrivedDestination, (int) destinationId).saveToDb(this);
                                learn(destLatitude, destLongitude, destinationId, startLatitude, startLongitude, hour, minute, dayOfTheWeek);
                            }
                        } catch (Throwable th) {
                            th = th;
                            buffReader = bufferedReader;
                            reader = fileReader;
                            if (buffReader != null) {
                            }
                            if (reader != null) {
                            }
                            throw th;
                        }
                    }
                    for (int day = 1; day <= 7; day++) {
                        for (int hour2 = 0; hour2 < 24; hour2++) {
                            sLogger.d("Day :" + day + ", Hour :" + hour2);
                            com.navdy.client.app.framework.suggestion.SuggestedDestination destination = suggestDestination(0.0d, 0.0d, hour2, 0, day);
                            if (destination != null) {
                                sLogger.d("Destination :" + destination.latitude + org.droidparts.contract.SQL.DDL.SEPARATOR + destination.longitude + " ; ID :" + destination.uniqueIdentifier);
                            }
                        }
                    }
                    if (bufferedReader != null) {
                        try {
                            bufferedReader.close();
                        } catch (java.io.IOException e) {
                            sLogger.e("Error closing the buffered reader");
                            return;
                        }
                    }
                    if (fileReader != null) {
                        fileReader.close();
                    }
                } catch (Throwable th2) {
                    th = th2;
                    reader = fileReader;
                    if (buffReader != null) {
                    }
                    if (reader != null) {
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                t = th3;
                sLogger.e("Exception ", t);
                if (buffReader != null) {
                }
                if (reader != null) {
                }
            }
        }
    }

    private void handleAutoTripUpload() {
        android.database.Cursor tripCursor;
        sLogger.d("handleAutoTripUpload : checking if we need an auto upload");
        android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        long lastUploadTimeStamp = sharedPreferences.getLong(PREFERENCE_LAST_UPLOAD_TIME, 0);
        long currentTime = java.lang.System.currentTimeMillis();
        long lastUploadedTripId = sharedPreferences.getLong(PREFERENCE_LAST_UPLOAD_ID, -1);
        long maxTripId = com.navdy.client.app.providers.NavdyContentProvider.getMaxTripId();
        boolean dataBaseChanged = maxTripId < lastUploadedTripId;
        if (dataBaseChanged) {
            sLogger.d("Database has changed");
        }
        if (dataBaseChanged || ((currentTime - lastUploadTimeStamp > MINIMUM_INTERVAL_BETWEEN_UPLOADS && (lastUploadedTripId == -1 || lastUploadedTripId < maxTripId)) || maxTripId > 10 + lastUploadedTripId)) {
            sLogger.d("Uploading the trip DB");
            android.database.Cursor tripCursor2 = null;
            if (lastUploadedTripId > 0) {
                try {
                    tripCursor = getTripCursor(lastUploadedTripId);
                } catch (Throwable th) {
                    com.navdy.service.library.util.IOUtils.closeStream(tripCursor2);
                    throw th;
                }
            } else {
                tripCursor = com.navdy.client.app.providers.NavdyContentProvider.getTripsCursor();
            }
            java.lang.String fullName = (com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.FULL_NAME, com.navdy.client.app.ui.settings.SettingsConstants.LAST_HUD_UUID_DEFAULT) + "_" + android.os.Build.MANUFACTURER + "_" + android.os.Build.MODEL).replaceAll("[^a-zA-Z0-9.-]", "_");
            java.text.SimpleDateFormat simpleDateFormat = new java.text.SimpleDateFormat("_MM_dd_yyyy_HH_mm_ss_SSS", java.util.Locale.US);
            java.lang.String uniqueName = fullName + simpleDateFormat.format(new java.util.Date(java.lang.System.currentTimeMillis())) + "_Android_V" + 1 + ".csv";
            sLogger.d("Uploading the file to S3, Key : " + uniqueName);
            com.squareup.otto.Bus bus = new com.squareup.otto.Bus(com.squareup.otto.ThreadEnforcer.ANY);
            bus.register(new com.navdy.client.app.framework.suggestion.DestinationSuggestionService.Anon3(sharedPreferences, maxTripId, bus));
            uploadToS3(uniqueName, tripCursor, bus);
            com.navdy.service.library.util.IOUtils.closeStream(tripCursor);
            scheduleNextCheckForUpdate();
        }
    }

    private void scheduleNextCheckForUpdate() {
        sLogger.d("Scheduling for next check for update");
        android.content.Intent intent = new android.content.Intent(this, com.navdy.client.app.framework.suggestion.DestinationSuggestionService.class);
        intent.setAction(COMMAND_AUTO_UPLOAD_TRIP_DATA);
        ((android.app.AlarmManager) getSystemService("alarm")).set(1, java.lang.System.currentTimeMillis() + MINIMUM_INTERVAL_BETWEEN_UPLOADS, android.app.PendingIntent.getService(this, 256, intent, 268435456));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:105:0x020b, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x020c, code lost:
        sLogger.e("Error closing the cursor ", r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0214, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0215, code lost:
        sLogger.e("Error closing the file writer ", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x01a6, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x01a7, code lost:
        if (r19 != null) goto L_0x01a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
        r19.post(com.navdy.client.app.framework.suggestion.DestinationSuggestionService.UploadResult.FAILURE);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01b0, code lost:
        sLogger.e("Error closing the cursor ", r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01b8, code lost:
        r10 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01b9, code lost:
        r4 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
        r18.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01b8 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:20:0x005f] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01bc A[SYNTHETIC, Splitter:B:79:0x01bc] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01c1 A[SYNTHETIC, Splitter:B:82:0x01c1] */
    private void uploadToS3(java.lang.String fileName, android.database.Cursor tripDbCursor, com.squareup.otto.Bus bus) {
        java.lang.String str;
        double d;
        double d2;
        java.io.FileWriter fileWriter = null;
        try {
            java.io.File file = new java.io.File(getFilesDir() + java.io.File.separator + fileName);
            if (file.exists()) {
                com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.client.app.NavdyApplication.getAppContext(), file.getAbsolutePath());
            }
            if (!file.createNewFile()) {
                if (tripDbCursor != null) {
                    try {
                        tripDbCursor.close();
                    } catch (Throwable t) {
                        sLogger.e("Error closing the cursor ", t);
                    }
                }
                if (fileWriter != null) {
                    try {
                        fileWriter.close();
                    } catch (java.io.IOException e) {
                        sLogger.e("Error closing the file writer ", e);
                    }
                }
            } else {
                java.io.FileWriter fileWriter2 = new java.io.FileWriter(file, true);
                if (tripDbCursor != null) {
                    try {
                        if (tripDbCursor.getCount() > 0) {
                            fileWriter2.write("TRIP_ID,TRIP_NUMBER,TRIP_START_TIME,TRIP_START_TIME_ZONE_N_DST,TRIP_ODOMETER,TRIP_LATITUDE,TRIP_LONGITUDE,TRIP_END_TIME,TRIP_END_ODOMETER,TRIP_END_LATITUDE,TRIP_END_LONGITUDE,TRIP_ARRIVED_AT_DESTINATION,TRIP_DESTINATION_ID,TRIP_DESTINATION_LAT,TRIP_DESTINATION_LONG\n");
                            while (tripDbCursor.moveToNext()) {
                                com.navdy.client.app.framework.models.Trip trip = com.navdy.client.app.providers.NavdyContentProvider.getTripsItemAt(tripDbCursor, tripDbCursor.getPosition());
                                if (trip != null) {
                                    com.navdy.client.app.framework.models.Destination savedDestination = com.navdy.client.app.providers.NavdyContentProvider.getThisDestination(trip.destinationId);
                                    java.lang.StringBuilder append = new java.lang.StringBuilder().append(trip.id).append(",").append(trip.tripNumber).append(",").append(trip.startTime).append(",").append(trip.offset).append(",").append(trip.startOdometer).append(",").append(trip.startLat).append(",").append(trip.startLong).append(",").append(trip.endTime).append(",").append(trip.endOdometer).append(",").append(trip.endLat).append(",").append(trip.endLong).append(",");
                                    if (trip.arrivedAtDestination == 0) {
                                        str = "false";
                                    } else {
                                        str = "true";
                                    }
                                    java.lang.StringBuilder append2 = append.append(str).append(",").append(trip.destinationId <= 0 ? "NA" : java.lang.Integer.valueOf(trip.destinationId)).append(",");
                                    if (savedDestination != null) {
                                        d = savedDestination.navigationLat;
                                    } else {
                                        d = 0.0d;
                                    }
                                    java.lang.StringBuilder append3 = append2.append(d).append(",");
                                    if (savedDestination != null) {
                                        d2 = savedDestination.navigationLong;
                                    } else {
                                        d2 = 0.0d;
                                    }
                                    fileWriter2.write(append3.append(d2).append("\n").toString());
                                }
                            }
                            tripDbCursor.close();
                            fileWriter2.flush();
                            uploadFileToS3(file, bus);
                            if (tripDbCursor != null) {
                                try {
                                    tripDbCursor.close();
                                } catch (Throwable t2) {
                                    sLogger.e("Error closing the cursor ", t2);
                                }
                            }
                            if (fileWriter2 != null) {
                                try {
                                    fileWriter2.close();
                                    java.io.FileWriter fileWriter3 = fileWriter2;
                                    return;
                                } catch (java.io.IOException e2) {
                                    sLogger.e("Error closing the file writer ", e2);
                                    java.io.FileWriter fileWriter4 = fileWriter2;
                                    return;
                                }
                            } else {
                                return;
                            }
                        }
                    } catch (Throwable th) {
                    }
                }
                if (bus != null) {
                    bus.post(com.navdy.client.app.framework.suggestion.DestinationSuggestionService.UploadResult.FAILURE);
                }
                if (tripDbCursor != null) {
                    try {
                        tripDbCursor.close();
                    } catch (Throwable t3) {
                        sLogger.e("Error closing the cursor ", t3);
                    }
                }
                if (fileWriter2 != null) {
                    try {
                        fileWriter2.close();
                    } catch (java.io.IOException e3) {
                        sLogger.e("Error closing the file writer ", e3);
                    }
                }
                java.io.FileWriter fileWriter5 = fileWriter2;
            }
        } catch (Throwable th2) {
            if (bus != null) {
                try {
                    bus.post(com.navdy.client.app.framework.suggestion.DestinationSuggestionService.UploadResult.FAILURE);
                } catch (Throwable th3) {
                    th = th3;
                    if (tripDbCursor != null) {
                    }
                    if (fileWriter != null) {
                    }
                    throw th;
                }
            }
            sLogger.e("Error while dumping the trip database to the csv file");
            if (tripDbCursor != null) {
                try {
                    tripDbCursor.close();
                } catch (Throwable t4) {
                    sLogger.e("Error closing the cursor ", t4);
                }
            }
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (java.io.IOException e4) {
                    sLogger.e("Error closing the file writer ", e4);
                }
            }
        }
    }

    private void handleUploadTripData() {
        sLogger.d("Uploading the trip data base to the S3");
        java.lang.String fullName = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.FULL_NAME, com.navdy.client.app.ui.settings.SettingsConstants.LAST_HUD_UUID_DEFAULT).replaceAll("[^a-zA-Z0-9.-]", "_");
        uploadToS3(fullName + "_" + android.os.Build.SERIAL + new java.text.SimpleDateFormat("_MM_dd_yyyy_HH_mm_ss_SSS", java.util.Locale.US).format(new java.util.Date(java.lang.System.currentTimeMillis())) + ".csv", getTripCursor(), null);
    }

    private void learnAllTrips() {
        android.database.Cursor cursor = null;
        try {
            cursor = getTripCursor();
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    com.navdy.client.app.framework.models.Trip trip = com.navdy.client.app.providers.NavdyContentProvider.getTripsItemAt(cursor, cursor.getPosition());
                    if (trip != null) {
                        learn(trip);
                    }
                }
                sColdStart = false;
            }
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
        }
    }

    private void uploadFileToS3(java.io.File file, com.squareup.otto.Bus bus) {
        if (file != null && file.exists()) {
            sLogger.d("Length of the file being uploaded: " + file.length());
            if (file.length() != 0) {
                new com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility(com.navdy.client.ota.impl.OTAUpdateManagerImpl.createS3Client(), this).upload(BUCKET_NAME, file.getName(), file).setTransferListener(new com.navdy.client.app.framework.suggestion.DestinationSuggestionService.Anon4(file, bus));
            } else if (bus != null) {
                bus.post(com.navdy.client.app.framework.suggestion.DestinationSuggestionService.UploadResult.FAILURE);
            }
        }
    }

    private void sendSuggestion(com.navdy.service.library.events.places.SuggestedDestination suggestedDestination) {
        com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice != null && remoteDevice.isConnected()) {
            remoteDevice.postEvent((com.squareup.wire.Message) suggestedDestination);
        }
    }

    private android.database.Cursor getTripCursor() {
        if (sColdStart) {
            sLogger.d("cold start, getting all the trips for learning");
            return com.navdy.client.app.providers.NavdyContentProvider.getTripsCursor();
        }
        sLogger.d("warm start, getting trips greater than " + sMaxIdentifier);
        return getTripCursor(sMaxIdentifier);
    }

    private android.database.Cursor getTripCursor(long maxId) {
        return com.navdy.client.app.providers.NavdyContentProvider.getTripsCursor(new android.util.Pair("_id > ?", new java.lang.String[]{java.lang.Long.toString(maxId)}));
    }

    private void learn(com.navdy.client.app.framework.models.Trip trip) {
        if (((long) trip.id) > sMaxIdentifier) {
            sMaxIdentifier = (long) trip.id;
        }
        long destinationIdentifier = trip.destinationId > 0 ? (long) trip.destinationId : -1;
        com.navdy.client.app.framework.models.Destination savedDestination = com.navdy.client.app.providers.NavdyContentProvider.getThisDestination((int) destinationIdentifier);
        if (savedDestination == null || !savedDestination.doNotSuggest) {
            java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.getDefault());
            cal.setTime(new java.util.Date(trip.startTime));
            learn(trip.endLat, trip.endLong, destinationIdentifier, trip.startLat, trip.startLong, cal.get(11), cal.get(12), cal.get(7));
        }
    }

    public static void suggestDestination(android.content.Context context, boolean local) {
        android.content.Intent intent = new android.content.Intent(context, com.navdy.client.app.framework.suggestion.DestinationSuggestionService.class);
        intent.setAction(COMMAND_SUGGEST);
        intent.putExtra(EXTRA_LOCAL, local);
        context.startService(intent);
    }

    public static void uploadTripDatabase(android.content.Context context, boolean manual) {
        if (!manual) {
            sLogger.d("UploadTripDatabase :Not a debug build so skipping");
            return;
        }
        android.content.Intent intent = new android.content.Intent(context, com.navdy.client.app.framework.suggestion.DestinationSuggestionService.class);
        intent.setAction(manual ? COMMAND_UPLOAD_TRIP_DATA : COMMAND_AUTO_UPLOAD_TRIP_DATA);
        context.startService(intent);
    }

    public static void reset(android.content.Context context) {
        android.content.Intent intent = new android.content.Intent(context, com.navdy.client.app.framework.suggestion.DestinationSuggestionService.class);
        intent.setAction(COMMAND_RESET);
        context.startService(intent);
    }
}
