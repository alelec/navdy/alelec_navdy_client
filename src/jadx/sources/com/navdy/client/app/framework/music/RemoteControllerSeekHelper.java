package com.navdy.client.app.framework.music;

public class RemoteControllerSeekHelper implements com.navdy.client.app.framework.servicehandler.MusicServiceHandler.MusicSeekHelper, java.lang.Runnable {
    private static final android.os.Handler handler = new android.os.Handler();
    private final int CONSECUTIVE_BREAK = 300;
    private final int STEP = 10000;
    private final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.music.RemoteControllerSeekHelper.class);
    private final android.media.RemoteController remoteController;
    private int signedStep;

    public RemoteControllerSeekHelper(android.media.RemoteController remoteController2) {
        this.remoteController = remoteController2;
    }

    public void executeMusicSeekAction(com.navdy.service.library.events.audio.MusicEvent.Action action) {
        this.logger.v("Executing music action " + java.lang.String.valueOf(action));
        if (com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_FAST_FORWARD_STOP.equals(action) || com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_REWIND_STOP.equals(action)) {
            handler.removeCallbacks(this);
            return;
        }
        this.signedStep = com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_FAST_FORWARD_START.equals(action) ? 10000 : -10000;
        handler.post(this);
    }

    public void run() {
        int oldPosition = (int) this.remoteController.getEstimatedMediaPosition();
        if (oldPosition < 0) {
            this.logger.i("Music position unavailable, can't seek");
            return;
        }
        int newPosition = oldPosition + this.signedStep;
        com.navdy.client.app.framework.servicehandler.MusicServiceHandler musicServiceHandler = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance();
        if (!musicServiceHandler.isMusicPlayerActive()) {
            this.logger.e("Tried to seek with no current track");
            return;
        }
        com.navdy.service.library.events.audio.MusicTrackInfo currentTrack = musicServiceHandler.getCurrentMediaTrackInfo();
        if (newPosition < 0) {
            newPosition = 0;
        } else if (currentTrack.duration != null && currentTrack.duration.intValue() > 0 && newPosition > currentTrack.duration.intValue()) {
            newPosition = currentTrack.duration.intValue();
        }
        this.logger.v("Setting new position: " + oldPosition + " -> " + newPosition);
        this.remoteController.seekTo((long) newPosition);
        handler.postDelayed(this, 300);
    }
}
