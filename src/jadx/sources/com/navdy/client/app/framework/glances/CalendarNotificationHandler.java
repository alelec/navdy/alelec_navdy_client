package com.navdy.client.app.framework.glances;

public class CalendarNotificationHandler {
    private static com.navdy.service.library.log.Logger sLogger = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.sLogger;

    public static void handleCalendarNotification(android.service.notification.StatusBarNotification sbn) {
        java.lang.String packageName = sbn.getPackageName();
        if (com.navdy.client.app.ui.glances.GlanceUtils.isThisGlanceEnabledAsWellAsGlobal(packageName)) {
            char c = 65535;
            switch (packageName.hashCode()) {
                case -456066902:
                    if (packageName.equals(com.navdy.client.app.framework.glances.GlanceConstants.ANDROID_CALENDAR)) {
                        c = 0;
                        break;
                    }
                    break;
                case 578428293:
                    if (packageName.equals(com.navdy.client.app.framework.glances.GlanceConstants.GOOGLE_CALENDAR)) {
                        c = 1;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                case 1:
                    handleGoogleCalendarNotification(sbn);
                    return;
                default:
                    sLogger.w("calendar notification not handled [" + packageName + "]");
                    return;
            }
        }
    }

    private static void handleGoogleCalendarNotification(android.service.notification.StatusBarNotification sbn) {
        java.lang.String packageName = sbn.getPackageName();
        android.app.Notification notification = sbn.getNotification();
        android.os.Bundle extras = android.support.v7.app.NotificationCompat.getExtras(notification);
        java.lang.String title = extras.getString("android.title");
        java.lang.String bigtext = extras.getString("android.bigText");
        java.lang.String text = extras.getString("android.text");
        java.lang.String when = null;
        java.lang.String location = null;
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(bigtext)) {
            text = bigtext;
        }
        if (text != null) {
            int i = text.indexOf("\n");
            if (i != -1) {
                when = text.substring(0, i);
                location = text.substring(i + 1);
            } else if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(bigtext)) {
                title = title + com.navdy.client.app.framework.util.CrashlyticsAppender.SEPARATOR + text;
            } else {
                int i2 = text.indexOf(",");
                if (i2 != -1) {
                    when = text.substring(0, i2);
                    location = text.substring(i2 + 1);
                } else {
                    when = text;
                }
            }
        }
        boolean hasLocationAction = false;
        if (com.navdy.client.app.framework.glances.GlancesHelper.getActions(notification).contains(com.navdy.client.app.framework.glances.GlanceConstants.ACTION_MAP)) {
            hasLocationAction = true;
        } else {
            location = null;
        }
        java.lang.String id = com.navdy.client.app.framework.glances.GlancesHelper.getId();
        if (title != null) {
            title = title.trim();
        }
        if (when != null) {
            when = when.trim();
        }
        if (location != null) {
            location = location.trim();
        }
        sLogger.v("[navdyinfo-gcalendar] title[" + title + "] when[" + when + "] location[" + location + "] hasLocationAction[" + hasLocationAction + "] uuid[" + id + "]");
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        com.navdy.service.library.events.glances.KeyValue keyValue = new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TITLE.name(), title);
        data.add(keyValue);
        com.navdy.service.library.events.glances.KeyValue keyValue2 = new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME_STR.name(), when);
        data.add(keyValue2);
        if (hasLocationAction) {
            com.navdy.service.library.events.glances.KeyValue keyValue3 = new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_LOCATION.name(), location);
            data.add(keyValue3);
        }
        com.navdy.client.app.framework.glances.GlancesHelper.sendEvent(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_CALENDAR).provider(packageName).id(id).postTime(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).glanceData(data).build());
    }
}
