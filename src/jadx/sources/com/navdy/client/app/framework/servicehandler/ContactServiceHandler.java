package com.navdy.client.app.framework.servicehandler;

public class ContactServiceHandler {
    public static final int DEFAULT_MAX_CONTACT = 30;
    private static com.navdy.client.app.framework.servicehandler.ContactServiceHandler singleton;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.contacts.FavoriteContactsRequest val$request;
        final /* synthetic */ com.navdy.service.library.events.contacts.FavoriteContactsResponse.Builder val$responseBuilder;

        Anon1(com.navdy.service.library.events.contacts.FavoriteContactsRequest favoriteContactsRequest, com.navdy.service.library.events.contacts.FavoriteContactsResponse.Builder builder) {
            this.val$request = favoriteContactsRequest;
            this.val$responseBuilder = builder;
        }

        public void run() {
            java.util.Iterator<com.navdy.client.app.framework.models.ContactModel> contactIterator = com.navdy.client.app.framework.servicehandler.ContactServiceHandler.this.sanitizeContacts(com.navdy.client.app.framework.util.ContactsManager.getInstance().getFavoriteContactsWithPhone()).iterator();
            java.util.List<com.navdy.service.library.events.contacts.Contact> outboundList = new java.util.ArrayList<>();
            while (contactIterator.hasNext() && outboundList.size() < this.val$request.maxContacts.intValue()) {
                com.navdy.client.app.framework.models.ContactModel contact = (com.navdy.client.app.framework.models.ContactModel) contactIterator.next();
                if (contact.favorite && contact.phoneNumbers != null) {
                    for (com.navdy.client.app.framework.models.PhoneNumberModel phoneNumber : contact.phoneNumbers) {
                        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(phoneNumber.number)) {
                            outboundList.add(new com.navdy.service.library.events.contacts.Contact(contact.name, phoneNumber.number, com.navdy.client.app.framework.servicehandler.ContactServiceHandler.this.getNumberType(phoneNumber.type), null));
                        }
                    }
                }
            }
            this.val$responseBuilder.status(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS).contacts(outboundList);
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) this.val$responseBuilder.build());
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.contacts.ContactRequest val$request;
        final /* synthetic */ com.navdy.service.library.events.contacts.ContactResponse.Builder val$responseBuilder;

        Anon2(com.navdy.service.library.events.contacts.ContactRequest contactRequest, com.navdy.service.library.events.contacts.ContactResponse.Builder builder) {
            this.val$request = contactRequest;
            this.val$responseBuilder = builder;
        }

        public void run() {
            java.util.List<com.navdy.client.app.framework.models.ContactModel> contactList = com.navdy.client.app.framework.util.ContactsManager.getInstance().getContactsWithoutLoadingPhotos(this.val$request.identifier, java.util.EnumSet.of(com.navdy.client.app.framework.util.ContactsManager.Attributes.PHONE));
            java.util.List<com.navdy.service.library.events.contacts.Contact> list = new java.util.ArrayList<>(4);
            if (contactList.size() > 0) {
                com.navdy.client.app.framework.models.ContactModel c = (com.navdy.client.app.framework.models.ContactModel) contactList.get(0);
                if (c.phoneNumbers != null) {
                    for (com.navdy.client.app.framework.models.PhoneNumberModel phoneNumber : c.phoneNumbers) {
                        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(phoneNumber.number)) {
                            list.add(new com.navdy.service.library.events.contacts.Contact(c.name, phoneNumber.number, com.navdy.client.app.framework.servicehandler.ContactServiceHandler.this.getNumberType(phoneNumber.type), null));
                        }
                    }
                }
            }
            this.val$responseBuilder.identifier(this.val$request.identifier).status(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS).contacts(list);
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) this.val$responseBuilder.build());
        }
    }

    public static synchronized com.navdy.client.app.framework.servicehandler.ContactServiceHandler getInstance() {
        com.navdy.client.app.framework.servicehandler.ContactServiceHandler contactServiceHandler;
        synchronized (com.navdy.client.app.framework.servicehandler.ContactServiceHandler.class) {
            if (singleton == null) {
                singleton = new com.navdy.client.app.framework.servicehandler.ContactServiceHandler();
            }
            contactServiceHandler = singleton;
        }
        return contactServiceHandler;
    }

    private ContactServiceHandler() {
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    @com.squareup.otto.Subscribe
    public void onDeviceConnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent event) {
        forceSendFavoriteContactsToHud();
    }

    public void forceSendFavoriteContactsToHud() {
        onFavoriteContactsRequest(new com.navdy.service.library.events.contacts.FavoriteContactsRequest.Builder().maxContacts(java.lang.Integer.valueOf(30)).build());
    }

    @com.squareup.otto.Subscribe
    public void onFavoriteContactsRequest(com.navdy.service.library.events.contacts.FavoriteContactsRequest request) {
        com.navdy.service.library.events.contacts.FavoriteContactsResponse.Builder responseBuilder = new com.navdy.service.library.events.contacts.FavoriteContactsResponse.Builder();
        if (!com.navdy.client.app.ui.homescreen.HomescreenActivity.weHaveContactsPermission()) {
            responseBuilder.status(com.navdy.service.library.events.RequestStatus.REQUEST_NOT_AVAILABLE);
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) responseBuilder.build());
            return;
        }
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.ContactServiceHandler.Anon1(request, responseBuilder), 1);
    }

    private java.util.List<com.navdy.client.app.framework.models.ContactModel> sanitizeContacts(java.util.List<com.navdy.client.app.framework.models.ContactModel> list) {
        java.util.List<com.navdy.client.app.framework.models.ContactModel> sanitizedList = new java.util.ArrayList<>();
        for (com.navdy.client.app.framework.models.ContactModel contact : list) {
            contact.deduplicatePhoneNumbers();
            sanitizedList.add(contact);
        }
        return sanitizedList;
    }

    @com.squareup.otto.Subscribe
    public void onContactRequest(com.navdy.service.library.events.contacts.ContactRequest request) {
        com.navdy.service.library.events.contacts.ContactResponse.Builder responseBuilder = new com.navdy.service.library.events.contacts.ContactResponse.Builder();
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(request.identifier)) {
            responseBuilder.identifier(request.identifier).status(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST);
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) responseBuilder.build());
        } else if (!com.navdy.client.app.ui.homescreen.HomescreenActivity.weHaveContactsPermission()) {
            responseBuilder.identifier(request.identifier).status(com.navdy.service.library.events.RequestStatus.REQUEST_NOT_AVAILABLE);
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) responseBuilder.build());
        } else {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.ContactServiceHandler.Anon2(request, responseBuilder), 1);
        }
    }

    private com.navdy.service.library.events.contacts.PhoneNumberType getNumberType(int type) {
        switch (type) {
            case 1:
                return com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_HOME;
            case 2:
                return com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_MOBILE;
            case 3:
                return com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_WORK;
            default:
                return com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_OTHER;
        }
    }
}
