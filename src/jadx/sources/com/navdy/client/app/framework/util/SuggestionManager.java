package com.navdy.client.app.framework.util;

public class SuggestionManager {
    private static final int MAX_CALENDAR_SUGGESTIONS = 2;
    private static final long MAX_EARLY_FOR_CAL_SUGG = java.util.concurrent.TimeUnit.HOURS.toMillis(1);
    private static final int MAX_RECENT_SUGGESTIONS = 23;
    private static final int MAX_RECOMMENDATIONS = 1;
    private static final float SUGGESTION_DISTANCE_LIMIT = 200.0f;
    private static java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> cachedHistory = new java.util.ArrayList<>(20);
    private static java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> cachedImportantTips = new java.util.ArrayList<>();
    private static java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> cachedRecommendations = new java.util.ArrayList<>();
    private static java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> cachedUnimportantTips = new java.util.ArrayList<>();
    private static java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> history = new java.util.ArrayList<>(20);
    private static java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> importantTips = new java.util.ArrayList<>();
    private static java.util.concurrent.atomic.AtomicBoolean isBuildingSuggestionList = new java.util.concurrent.atomic.AtomicBoolean(false);
    private static long lastListGeneration = 0;
    private static android.location.Location lastUserLocation = new android.location.Location("");
    private static java.util.ArrayList<java.lang.ref.WeakReference<com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener>> listeners = new java.util.ArrayList<>();
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.SuggestionManager.class);
    private static final com.navdy.client.app.framework.location.NavdyLocationManager navdyLocationManager = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance();
    private static com.navdy.client.app.framework.models.Suggestion ota = null;
    private static java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> recommendations = new java.util.ArrayList<>();
    private static com.navdy.client.app.framework.models.Suggestion trip = null;
    private static java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> unimportantTips = new java.util.ArrayList<>();

    static class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.framework.models.Suggestion val$newTrip;

        Anon1(com.navdy.client.app.framework.models.Suggestion suggestion) {
            this.val$newTrip = suggestion;
        }

        public void run() {
            com.navdy.client.app.framework.util.SuggestionManager.setTrip(this.val$newTrip);
        }
    }

    static class Anon2 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
        Anon2() {
        }

        /* access modifiers changed from: protected */
        public java.lang.Void doInBackground(java.lang.Void... params) {
            com.navdy.client.app.framework.util.SuggestionManager.buildSuggestionList(true);
            return null;
        }
    }

    static class Anon3 implements com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener {
        final /* synthetic */ boolean val$sendToHud;

        Anon3(boolean z) {
            this.val$sendToHud = z;
        }

        public void onSuggestionBuildComplete(java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> recommendations) {
            com.navdy.client.app.framework.util.SuggestionManager.setRecommendations(recommendations);
            com.navdy.client.app.framework.util.SuggestionManager.finishSuggestionBuild(this.val$sendToHud);
        }
    }

    static class Anon4 implements com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener {
        final /* synthetic */ com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener val$callback;

        class Anon1 implements com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener {
            Anon1() {
            }

            public void onSuggestionBuildComplete(java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> calendarEventsAndRecommendations) {
                com.navdy.client.app.framework.util.SuggestionManager.Anon4.this.val$callback.onSuggestionBuildComplete(calendarEventsAndRecommendations);
            }
        }

        Anon4(com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener suggestionBuildListener) {
            this.val$callback = suggestionBuildListener;
        }

        public void onSuggestionBuildComplete(java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> calendarSuggestions) {
            com.navdy.client.app.framework.util.SuggestionManager.buildMachineLearnedRecommendation(calendarSuggestions, new com.navdy.client.app.framework.util.SuggestionManager.Anon4.Anon1());
        }
    }

    static class Anon5 implements com.navdy.client.app.ui.homescreen.CalendarUtils.CalendarEventReader {
        final /* synthetic */ java.util.ArrayList val$calendarSuggestions;
        final /* synthetic */ com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener val$callback;

        class Anon1 implements java.util.Comparator<com.navdy.client.app.framework.models.CalendarEvent> {
            final /* synthetic */ long val$now;

            Anon1(long j) {
                this.val$now = j;
            }

            public int compare(com.navdy.client.app.framework.models.CalendarEvent lhs, com.navdy.client.app.framework.models.CalendarEvent rhs) {
                return (int) (java.lang.Math.abs(this.val$now - lhs.startTimestamp) - java.lang.Math.abs(this.val$now - rhs.startTimestamp));
            }
        }

        class Anon2 implements com.navdy.client.app.framework.navigation.HereRouteManager.Listener {
            final /* synthetic */ int val$addedCount;
            final /* synthetic */ com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener val$callback;
            final /* synthetic */ com.navdy.client.app.framework.models.CalendarEvent val$event;
            final /* synthetic */ java.util.List val$events;
            final /* synthetic */ com.navdy.client.app.framework.models.Destination val$finalDestination;
            final /* synthetic */ int val$i;

            Anon2(com.navdy.client.app.framework.models.CalendarEvent calendarEvent, java.util.List list, int i, int i2, com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener suggestionBuildListener, com.navdy.client.app.framework.models.Destination destination) {
                this.val$event = calendarEvent;
                this.val$events = list;
                this.val$i = i;
                this.val$addedCount = i2;
                this.val$callback = suggestionBuildListener;
                this.val$finalDestination = destination;
            }

            public void onPreCalculation(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.HereRouteManager.RouteHandle routeHandle) {
            }

            public void onRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.HereRouteManager.Error error, @android.support.annotation.Nullable com.here.android.mpa.routing.Route route) {
                int i = 1;
                if (!willArriveOnTime(route, this.val$event)) {
                    com.navdy.client.app.framework.util.SuggestionManager.logger.d("We would not arrive on time for " + this.val$event.displayName + " so will not recommend");
                    com.navdy.client.app.framework.util.SuggestionManager.Anon5.this.processNextCalEvent(this.val$events, this.val$i + 1, this.val$addedCount, this.val$callback);
                    return;
                }
                com.navdy.client.app.framework.models.Suggestion suggestion = new com.navdy.client.app.framework.models.Suggestion(this.val$finalDestination, com.navdy.client.app.framework.models.Suggestion.SuggestionType.CALENDAR, this.val$event);
                suggestion.calculateSuggestedRoute = true;
                boolean success = com.navdy.client.app.framework.util.SuggestionManager.addToSuggestions(suggestion, com.navdy.client.app.framework.util.SuggestionManager.Anon5.this.val$calendarSuggestions);
                int i2 = this.val$addedCount;
                if (!success) {
                    i = 0;
                }
                int newCount = i2 + i;
                com.navdy.client.app.framework.util.SuggestionManager.logger.d((success ? "Was" : "Was not") + " able to add it as a calendarSuggestions. addedCount = " + newCount);
                com.navdy.client.app.framework.util.SuggestionManager.Anon5.this.processNextCalEvent(this.val$events, this.val$i + 1, newCount, this.val$callback);
            }

            private boolean willArriveOnTime(com.here.android.mpa.routing.Route route, com.navdy.client.app.framework.models.CalendarEvent event) {
                if (route == null) {
                    return false;
                }
                int durationWithTraffic = route.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, com.here.android.mpa.routing.Route.WHOLE_ROUTE).getDuration() * 1000;
                long now = new java.util.Date().getTime();
                com.navdy.client.app.framework.util.SuggestionManager.logger.d("It will take " + com.navdy.client.debug.util.FormatUtils.formatDurationFromSecondsToSecondsMinutesHours(durationWithTraffic / 1000) + " to get to " + event.displayName);
                com.navdy.client.app.framework.util.SuggestionManager.logger.d("You would arrive on " + new java.util.Date(((long) durationWithTraffic) + now));
                com.navdy.client.app.framework.util.SuggestionManager.logger.d(event.displayName + " starts on " + new java.util.Date(event.startTimestamp) + " and ends on " + new java.util.Date(event.endTimestamp));
                if (((long) durationWithTraffic) + now >= event.endTimestamp || ((long) durationWithTraffic) + now <= event.startTimestamp - com.navdy.client.app.framework.util.SuggestionManager.MAX_EARLY_FOR_CAL_SUGG) {
                    return false;
                }
                return true;
            }
        }

        Anon5(com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener suggestionBuildListener, java.util.ArrayList arrayList) {
            this.val$callback = suggestionBuildListener;
            this.val$calendarSuggestions = arrayList;
        }

        public void onCalendarEventsRead(java.util.List<com.navdy.client.app.framework.models.CalendarEvent> events) {
            if (events == null || events.size() <= 0) {
                com.navdy.client.app.framework.util.SuggestionManager.logger.d("No Calendar Event to process");
                this.val$callback.onSuggestionBuildComplete(this.val$calendarSuggestions);
                return;
            }
            java.util.Collections.sort(events, new com.navdy.client.app.framework.util.SuggestionManager.Anon5.Anon1(java.lang.System.currentTimeMillis()));
            processNextCalEvent(events, 0, 0, this.val$callback);
        }

        private void processNextCalEvent(java.util.List<com.navdy.client.app.framework.models.CalendarEvent> events, int i, int addedCount, com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener callback) {
            if (addedCount >= 2 || i >= events.size()) {
                callback.onSuggestionBuildComplete(this.val$calendarSuggestions);
                return;
            }
            com.navdy.client.app.framework.models.CalendarEvent event = (com.navdy.client.app.framework.models.CalendarEvent) events.get(i);
            com.navdy.client.app.framework.util.SuggestionManager.logger.d("Trying to add this calendar entry to the list of suggestions: " + event.toString());
            com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
            destination.rawAddressNotForDisplay = event.location;
            if (event.latLng != null) {
                destination.navigationLat = event.latLng.latitude;
                destination.navigationLong = event.latLng.longitude;
            }
            com.navdy.client.app.framework.models.Destination finalDestination = com.navdy.client.app.providers.NavdyContentProvider.getThisDestination(destination);
            finalDestination.setIsCalendarEvent(true);
            com.navdy.client.app.framework.navigation.HereRouteManager.getInstance().calculateRoute(destination.navigationLat, destination.navigationLong, new com.navdy.client.app.framework.util.SuggestionManager.Anon5.Anon2(event, events, i, addedCount, callback, finalDestination));
        }
    }

    static class Anon6 {
        final /* synthetic */ java.util.ArrayList val$calendarEventsAndRecommendations;
        final /* synthetic */ com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener val$callback;

        Anon6(java.util.ArrayList arrayList, com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener suggestionBuildListener) {
            this.val$calendarEventsAndRecommendations = arrayList;
            this.val$callback = suggestionBuildListener;
        }

        @com.squareup.otto.Subscribe
        public void onDestinationSuggestion(com.navdy.client.app.framework.models.Destination suggestedDestination) {
            if (suggestedDestination == null || suggestedDestination.id <= 0) {
                com.navdy.client.app.framework.util.SuggestionManager.logger.v("Machine learning did not return any destination recommendation.");
            } else {
                com.navdy.client.app.framework.util.SuggestionManager.logger.v("Machine learning returned a destination recommendation: " + suggestedDestination);
                com.navdy.client.app.framework.models.Suggestion recommendation = com.navdy.client.app.framework.util.SuggestionManager.makeSuggestion(suggestedDestination, com.navdy.client.app.framework.models.Suggestion.SuggestionType.RECOMMENDATION);
                recommendation.calculateSuggestedRoute = true;
                com.navdy.client.app.framework.util.SuggestionManager.addToSuggestions(recommendation, this.val$calendarEventsAndRecommendations);
            }
            this.val$callback.onSuggestionBuildComplete(this.val$calendarEventsAndRecommendations);
            com.navdy.client.app.framework.util.BusProvider.getInstance().unregister(this);
        }
    }

    static class Anon7 implements com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener {
        final /* synthetic */ boolean val$sendToHud;

        Anon7(boolean z) {
            this.val$sendToHud = z;
        }

        public void onSuggestionBuildComplete(java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> calendarSuggestions) {
            java.util.Iterator it = com.navdy.client.app.framework.util.SuggestionManager.recommendations.iterator();
            while (it.hasNext()) {
                com.navdy.client.app.framework.models.Suggestion recommendation = (com.navdy.client.app.framework.models.Suggestion) it.next();
                if (recommendation.getType() != com.navdy.client.app.framework.models.Suggestion.SuggestionType.CALENDAR) {
                    calendarSuggestions.add(recommendation);
                }
            }
            com.navdy.client.app.framework.util.SuggestionManager.recommendations = calendarSuggestions;
            com.navdy.client.app.framework.util.SuggestionManager.finishSuggestionBuild(this.val$sendToHud);
        }
    }

    public interface SuggestionBuildListener {
        @android.support.annotation.WorkerThread
        void onSuggestionBuildComplete(java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> arrayList);
    }

    public static synchronized void addListener(@android.support.annotation.NonNull java.lang.ref.WeakReference<com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener> listener) {
        synchronized (com.navdy.client.app.framework.util.SuggestionManager.class) {
            listeners.add(listener);
        }
    }

    public static synchronized void removeListener(@android.support.annotation.NonNull java.lang.ref.WeakReference<com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener> listener) {
        synchronized (com.navdy.client.app.framework.util.SuggestionManager.class) {
            listeners.remove(listener);
        }
    }

    private static synchronized void notifyListenersIfAny(java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> suggestions) {
        synchronized (com.navdy.client.app.framework.util.SuggestionManager.class) {
            if (listeners != null) {
                java.util.Iterator it = listeners.iterator();
                while (it.hasNext()) {
                    com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener suggestionBuildListener = (com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener) ((java.lang.ref.WeakReference) it.next()).get();
                    if (suggestionBuildListener != null) {
                        suggestionBuildListener.onSuggestionBuildComplete(suggestions);
                    }
                }
            }
        }
    }

    @android.support.annotation.MainThread
    public static synchronized void setTripAsync(@android.support.annotation.Nullable com.navdy.client.app.framework.models.Suggestion newTrip) {
        synchronized (com.navdy.client.app.framework.util.SuggestionManager.class) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.util.SuggestionManager.Anon1(newTrip), 1);
        }
    }

    /* access modifiers changed from: private */
    @android.support.annotation.WorkerThread
    public static synchronized void setTrip(@android.support.annotation.Nullable com.navdy.client.app.framework.models.Suggestion newTrip) {
        synchronized (com.navdy.client.app.framework.util.SuggestionManager.class) {
            if ((trip != null && newTrip == null) || ((trip == null && newTrip != null) || (trip != null && !trip.equals(newTrip)))) {
                trip = newTrip;
                notifyListenersIfAny(getSuggestions());
            }
        }
    }

    public static com.navdy.client.app.framework.models.Suggestion getTrip() {
        return trip;
    }

    private static synchronized void setRecommendations(java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> recommendations2) {
        synchronized (com.navdy.client.app.framework.util.SuggestionManager.class) {
            recommendations = recommendations2;
        }
    }

    public static synchronized void forceSuggestionFullRefresh() {
        synchronized (com.navdy.client.app.framework.util.SuggestionManager.class) {
            lastListGeneration = 0;
            lastUserLocation = new android.location.Location("");
            com.navdy.client.app.ui.homescreen.CalendarUtils.forceCalendarRefresh();
        }
    }

    private static synchronized void insertOtaTip() {
        synchronized (com.navdy.client.app.framework.util.SuggestionManager.class) {
            if (ota == null) {
                com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
                android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
                destination.name = context.getString(com.navdy.client.R.string.sw_update_available);
                destination.rawAddressNotForDisplay = context.getString(com.navdy.client.R.string.tap_to_download);
                ota = new com.navdy.client.app.framework.models.Suggestion(destination, com.navdy.client.app.framework.models.Suggestion.SuggestionType.OTA);
            }
        }
    }

    private static synchronized void removeOtaTip() {
        synchronized (com.navdy.client.app.framework.util.SuggestionManager.class) {
            ota = null;
        }
    }

    @android.support.annotation.WorkerThread
    public static synchronized void updateOtaStatus(com.navdy.client.ota.OTAUpdateService.State state) {
        synchronized (com.navdy.client.app.framework.util.SuggestionManager.class) {
            com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
            boolean somethingChanged = false;
            boolean otaWasHere = ota != null;
            if (state == com.navdy.client.ota.OTAUpdateService.State.UPDATE_AVAILABLE) {
                insertOtaTip();
                somethingChanged = true;
            } else if (otaWasHere) {
                removeOtaTip();
                somethingChanged = true;
            }
            if (somethingChanged) {
                notifyListenersIfAny(getSuggestions());
            }
        }
    }

    @android.support.annotation.NonNull
    public static synchronized java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> getSuggestions() {
        java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> suggestions;
        synchronized (com.navdy.client.app.framework.util.SuggestionManager.class) {
            suggestions = new java.util.ArrayList<>(23);
            if (trip != null) {
                suggestions.add(trip);
            }
            if (cachedImportantTips.size() > 0) {
                addThisHeader(com.navdy.client.R.string.tip, suggestions);
                suggestions.add(cachedImportantTips.get(0));
            }
            if (trip == null && cachedRecommendations.size() > 0) {
                com.navdy.client.app.framework.models.Suggestion recommendationHeader = buildHeaderFor(com.navdy.client.R.string.suggested_places);
                suggestions.add(recommendationHeader);
                int successCount = 0;
                java.util.Iterator it = cachedRecommendations.iterator();
                while (it.hasNext()) {
                    if (addToSuggestions((com.navdy.client.app.framework.models.Suggestion) it.next(), suggestions)) {
                        successCount++;
                    }
                }
                if (successCount <= 0) {
                    suggestions.remove(recommendationHeader);
                }
            }
            if (cachedImportantTips.size() <= 0) {
                if (cachedUnimportantTips.size() > 0) {
                    addThisHeader(com.navdy.client.R.string.tip, suggestions);
                    suggestions.add(cachedUnimportantTips.get(0));
                } else if (ota != null) {
                    suggestions.add(ota);
                }
            }
            if (cachedHistory.size() > 0) {
                com.navdy.client.app.framework.models.Suggestion historyHeader = buildHeaderFor(com.navdy.client.R.string.history);
                suggestions.add(historyHeader);
                int successCount2 = 0;
                java.util.Iterator it2 = cachedHistory.iterator();
                while (it2.hasNext()) {
                    com.navdy.client.app.framework.models.Suggestion s = (com.navdy.client.app.framework.models.Suggestion) it2.next();
                    if (successCount2 >= 20) {
                        break;
                    } else if (addToSuggestionsOnlyCheckForDuplicates(s, suggestions)) {
                        successCount2++;
                    }
                }
                if (successCount2 <= 0) {
                    suggestions.remove(historyHeader);
                }
            }
            if (suggestions.size() <= 0) {
                suggestions = getEmptySuggestionState();
            }
        }
        return suggestions;
    }

    @android.support.annotation.MainThread
    public static java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> getEmptySuggestionState() {
        java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> suggestions = new java.util.ArrayList<>();
        com.navdy.client.app.framework.models.Suggestion trip2 = getTripSuggestion();
        if (trip2 != null) {
            suggestions.add(trip2);
        } else {
            addThisHeader(com.navdy.client.R.string.space, suggestions);
        }
        suggestions.add(new com.navdy.client.app.framework.models.Suggestion(null, com.navdy.client.app.framework.models.Suggestion.SuggestionType.LOADING));
        return suggestions;
    }

    private static com.navdy.client.app.framework.models.Suggestion getAddFavoriteSuggestion() {
        com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        destination.name = context.getString(com.navdy.client.R.string.add_favorite);
        destination.rawAddressNotForDisplay = context.getString(com.navdy.client.R.string.tap_to_add_new_favorite);
        return new com.navdy.client.app.framework.models.Suggestion(destination, com.navdy.client.app.framework.models.Suggestion.SuggestionType.ADD_FAVORITE);
    }

    public static void rebuildSuggestionListAndSendToHudAsync() {
        new com.navdy.client.app.framework.util.SuggestionManager.Anon2().execute(new java.lang.Void[0]);
    }

    @android.support.annotation.WorkerThread
    public static synchronized void buildSuggestionList(boolean sendToHud) {
        synchronized (com.navdy.client.app.framework.util.SuggestionManager.class) {
            com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
            logger.v("Building suggestion list.");
            if (isBuildingSuggestionList.get()) {
                logger.v("Already in the process of building the suggestion list. Ignoring request.");
            } else {
                isBuildingSuggestionList.set(true);
                trip = null;
                importantTips = new java.util.ArrayList<>();
                unimportantTips = new java.util.ArrayList<>();
                history = new java.util.ArrayList<>(20);
                cachedImportantTips = new java.util.ArrayList<>();
                cachedRecommendations = new java.util.ArrayList<>();
                cachedUnimportantTips = new java.util.ArrayList<>();
                cachedHistory = new java.util.ArrayList<>(20);
                android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
                trip = getTripSuggestion();
                buildImportantTipsSuggestions(sharedPreferences);
                buildUnimportantTipsSuggestions(sharedPreferences);
                buildHistorySuggestions();
                buildCalendarAndMachineLearnedSuggestions(new com.navdy.client.app.framework.util.SuggestionManager.Anon3(sendToHud));
            }
        }
    }

    private static com.navdy.client.app.framework.models.Suggestion getTripSuggestion() {
        com.navdy.client.app.framework.navigation.NavdyRouteHandler navdyRouteHandler = com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance();
        if (navdyRouteHandler.isInOneOfTheActiveTripStates()) {
            return new com.navdy.client.app.framework.models.Suggestion(navdyRouteHandler.getCurrentDestination(), com.navdy.client.app.framework.models.Suggestion.SuggestionType.ACTIVE_TRIP);
        }
        if (navdyRouteHandler.isInOneOfThePendingTripStates()) {
            return new com.navdy.client.app.framework.models.Suggestion(navdyRouteHandler.getCurrentDestination(), com.navdy.client.app.framework.models.Suggestion.SuggestionType.PENDING_TRIP);
        }
        return null;
    }

    private static void buildImportantTipsSuggestions(android.content.SharedPreferences sharedPreferences) {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        if (sharedPreferences != null && !sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_WATCHED_THE_DEMO, false)) {
            com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
            destination.name = context.getString(com.navdy.client.R.string.learn_how);
            destination.rawAddressNotForDisplay = context.getString(com.navdy.client.R.string.watch_feature_video);
            importantTips.add(new com.navdy.client.app.framework.models.Suggestion(destination, com.navdy.client.app.framework.models.Suggestion.SuggestionType.DEMO_VIDEO));
        }
        if (sharedPreferences != null && sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_GESTURE, true) && !sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_TRIED_GESTURES_ONCE_BEFORE, false)) {
            com.navdy.client.app.framework.models.Destination destination2 = new com.navdy.client.app.framework.models.Destination();
            destination2.name = context.getString(com.navdy.client.R.string.learning_gestures);
            destination2.rawAddressNotForDisplay = context.getString(com.navdy.client.R.string.learning_gestures_subtitle);
            importantTips.add(new com.navdy.client.app.framework.models.Suggestion(destination2, com.navdy.client.app.framework.models.Suggestion.SuggestionType.TRY_GESTURES));
        }
        if (sharedPreferences != null && !sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_MICROPHONE_TIP, false) && sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_VOICE_SEARCH_CAPABLE, false)) {
            if (com.navdy.client.app.ui.base.BaseActivity.weHaveMicrophonePermission()) {
                com.navdy.client.app.framework.models.Destination destination3 = new com.navdy.client.app.framework.models.Destination();
                destination3.name = context.getString(com.navdy.client.R.string.voice_search);
                destination3.rawAddressNotForDisplay = context.getString(com.navdy.client.R.string.voice_search_description);
                importantTips.add(new com.navdy.client.app.framework.models.Suggestion(destination3, com.navdy.client.app.framework.models.Suggestion.SuggestionType.VOICE_SEARCH));
            } else {
                com.navdy.client.app.framework.models.Destination destination4 = new com.navdy.client.app.framework.models.Destination();
                destination4.name = context.getString(com.navdy.client.R.string.enable_microphone);
                destination4.rawAddressNotForDisplay = context.getString(com.navdy.client.R.string.enable_microphone_description);
                importantTips.add(new com.navdy.client.app.framework.models.Suggestion(destination4, com.navdy.client.app.framework.models.Suggestion.SuggestionType.ENABLE_MICROPHONE));
            }
        }
        if (sharedPreferences != null && !sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_GOOGLE_NOW_TIP, false)) {
            com.navdy.client.app.framework.models.Destination destination5 = new com.navdy.client.app.framework.models.Destination();
            destination5.name = context.getString(com.navdy.client.R.string.google_now_tip);
            destination5.rawAddressNotForDisplay = context.getString(com.navdy.client.R.string.google_now_tip_subtitle);
            importantTips.add(new com.navdy.client.app.framework.models.Suggestion(destination5, com.navdy.client.app.framework.models.Suggestion.SuggestionType.GOOGLE_NOW));
        }
        if (sharedPreferences != null && !sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_LOCAL_MUSIC_BROWSER_TIP, false) && sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_LOCAL_MUSIC_BROWSER_CAPABLE, false)) {
            com.navdy.client.app.framework.models.Destination destination6 = new com.navdy.client.app.framework.models.Destination();
            destination6.name = context.getString(com.navdy.client.R.string.hud_local_music_browser_tip);
            destination6.rawAddressNotForDisplay = context.getString(com.navdy.client.R.string.hud_local_music_browser_tip_subtitle);
            importantTips.add(new com.navdy.client.app.framework.models.Suggestion(destination6, com.navdy.client.app.framework.models.Suggestion.SuggestionType.HUD_LOCAL_MUSIC_BROWSER));
        }
        if (sharedPreferences != null && !sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES, false) && !sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ENABLED_GLANCES_ONCE_BEFORE, false)) {
            com.navdy.client.app.framework.models.Destination destination7 = new com.navdy.client.app.framework.models.Destination();
            destination7.name = context.getString(com.navdy.client.R.string.enable_glances);
            destination7.rawAddressNotForDisplay = context.getString(com.navdy.client.R.string.enable_glances_description);
            importantTips.add(new com.navdy.client.app.framework.models.Suggestion(destination7, com.navdy.client.app.framework.models.Suggestion.SuggestionType.ENABLE_GLANCES));
        }
    }

    @android.support.annotation.WorkerThread
    private static void buildUnimportantTipsSuggestions(android.content.SharedPreferences sharedPreferences) {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        if (sharedPreferences != null) {
            if (!sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_GESTURE, true) && !sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_GESTURE_TIP, false)) {
                com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
                destination.name = context.getString(com.navdy.client.R.string.enable_gestures);
                destination.rawAddressNotForDisplay = context.getString(com.navdy.client.R.string.enable_gestures_description);
                unimportantTips.add(new com.navdy.client.app.framework.models.Suggestion(destination, com.navdy.client.app.framework.models.Suggestion.SuggestionType.ENABLE_GESTURES));
            }
            if (com.navdy.client.app.providers.NavdyContentProvider.getHome() == null && !sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_ADD_HOME_TIP, false)) {
                com.navdy.client.app.framework.models.Destination destination2 = new com.navdy.client.app.framework.models.Destination();
                destination2.name = context.getString(com.navdy.client.R.string.add_home);
                destination2.rawAddressNotForDisplay = context.getString(com.navdy.client.R.string.tap_to_set_home_address);
                unimportantTips.add(new com.navdy.client.app.framework.models.Suggestion(destination2, com.navdy.client.app.framework.models.Suggestion.SuggestionType.ADD_HOME));
            }
            if (com.navdy.client.app.providers.NavdyContentProvider.getWork() == null && !sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_ADD_WORK_TIP, false)) {
                com.navdy.client.app.framework.models.Destination destination3 = new com.navdy.client.app.framework.models.Destination();
                destination3.name = context.getString(com.navdy.client.R.string.add_work);
                destination3.rawAddressNotForDisplay = context.getString(com.navdy.client.R.string.tap_to_set_work_address);
                unimportantTips.add(new com.navdy.client.app.framework.models.Suggestion(destination3, com.navdy.client.app.framework.models.Suggestion.SuggestionType.ADD_WORK));
            }
        }
        android.database.Cursor favoritesCursor = null;
        try {
            favoritesCursor = com.navdy.client.app.providers.NavdyContentProvider.getFavoritesCursor(new android.util.Pair("is_special != ? AND is_special != ?", new java.lang.String[]{java.lang.String.valueOf(-3), java.lang.String.valueOf(-2)}));
            if (favoritesCursor != null && favoritesCursor.getCount() <= 0) {
                unimportantTips.add(getAddFavoriteSuggestion());
            }
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(favoritesCursor);
        }
    }

    private static void buildHistorySuggestions() {
        assignDestinationIdsToTripsWithKnownDestinations();
        android.database.Cursor c = null;
        try {
            c = com.navdy.client.app.providers.NavdyContentProvider.getRecentsCursor(new android.util.Pair("do_not_suggest != 1", null));
            if (c != null && c.moveToFirst()) {
                do {
                    history.add(makeSuggestion(com.navdy.client.app.providers.NavdyContentProvider.getDestinationItemAt(c, c.getPosition()), com.navdy.client.app.framework.models.Suggestion.SuggestionType.RECENT));
                    if (history.size() >= 23) {
                        break;
                    }
                } while (c.moveToNext());
            }
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(c);
        }
    }

    /* JADX INFO: finally extract failed */
    private static void assignDestinationIdsToTripsWithKnownDestinations() {
        java.util.ArrayList<com.navdy.client.app.framework.models.Trip> trips = new java.util.ArrayList<>();
        try {
            android.database.Cursor c = com.navdy.client.app.providers.NavdyContentProvider.getTripsCursor(new android.util.Pair("destination_id == 0 AND end_lat != 0 AND end_long != 0", null));
            if (c != null && c.moveToFirst()) {
                for (int i = 0; i < c.getCount(); i++) {
                    trips.add(com.navdy.client.app.providers.NavdyContentProvider.getTripsItemAt(c, i));
                }
            }
            com.navdy.service.library.util.IOUtils.closeStream(c);
            java.util.Iterator it = trips.iterator();
            while (it.hasNext()) {
                com.navdy.client.app.framework.models.Trip trip2 = (com.navdy.client.app.framework.models.Trip) it.next();
                android.database.Cursor destinationCursor = null;
                try {
                    destinationCursor = com.navdy.client.app.providers.NavdyContentProvider.getDestinationCursor(com.navdy.client.app.framework.models.Destination.getCoordinateBoundingBoxSelectionClause(trip2.endLat, trip2.endLong));
                    if (destinationCursor != null && destinationCursor.moveToFirst()) {
                        com.navdy.client.app.framework.models.Destination closestDestination = null;
                        float shortestDistance = -1.0f;
                        while (!destinationCursor.isAfterLast()) {
                            com.navdy.client.app.framework.models.Destination d = com.navdy.client.app.providers.NavdyContentProvider.getDestinationItemAt(destinationCursor, destinationCursor.getPosition());
                            android.location.Location loc1 = new android.location.Location("");
                            android.location.Location loc2 = new android.location.Location("");
                            float distance = -1.0f;
                            loc1.setLatitude(trip2.endLat);
                            loc1.setLongitude(trip2.endLong);
                            if (d.navigationLat != 0.0d || d.navigationLong != 0.0d) {
                                loc2.setLatitude(d.navigationLat);
                                loc2.setLongitude(d.navigationLong);
                                distance = loc1.distanceTo(loc2);
                            } else if (!(d.displayLat == 0.0d && d.displayLong == 0.0d)) {
                                loc2.setLatitude(d.displayLat);
                                loc2.setLongitude(d.displayLong);
                                distance = loc1.distanceTo(loc2);
                            }
                            if (distance < SUGGESTION_DISTANCE_LIMIT && (closestDestination == null || (distance != -1.0f && distance < shortestDistance))) {
                                closestDestination = d;
                                shortestDistance = distance;
                            }
                            destinationCursor.moveToNext();
                        }
                        if (closestDestination != null) {
                            trip2.setDestinationIdAndSaveToDb(closestDestination.id);
                            if (closestDestination.lastRoutedDate > trip2.startTime) {
                                closestDestination.lastRoutedDate = trip2.startTime;
                                closestDestination.updateLastRoutedDateInDb();
                            }
                        }
                    }
                } finally {
                    com.navdy.service.library.util.IOUtils.closeStream(destinationCursor);
                }
            }
        } catch (Throwable th) {
            com.navdy.service.library.util.IOUtils.closeStream(null);
            throw th;
        }
    }

    private static void buildCalendarAndMachineLearnedSuggestions(com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener callback) {
        long now = new java.util.Date().getTime();
        android.location.Location currentPhoneLocation = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance().getSmartStartLocation();
        if (now - lastListGeneration >= com.navdy.client.app.ui.homescreen.CalendarUtils.EVENTS_STALENESS_LIMIT || !isCloseToLastRecommendation(currentPhoneLocation)) {
            lastListGeneration = now;
            lastUserLocation = currentPhoneLocation;
            logger.d("Building new Calendar events and Machine learned recommendation");
            buildCalendarSuggestions(new com.navdy.client.app.framework.util.SuggestionManager.Anon4(callback));
            return;
        }
        logger.d("Using cached Calendar events and Machine learned recommendation");
        callback.onSuggestionBuildComplete(recommendations);
    }

    private static boolean isCloseToLastRecommendation(android.location.Location currentPhoneLocation) {
        if (currentPhoneLocation == null || lastUserLocation == null || com.navdy.client.app.framework.map.MapUtils.distanceBetween(currentPhoneLocation, lastUserLocation) >= 100.0f) {
            return false;
        }
        return true;
    }

    public static void buildCalendarSuggestions(com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener callback) {
        java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> calendarSuggestions = new java.util.ArrayList<>(20);
        boolean calendarSuggestionsAreEnabled = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.CALENDARS_ENABLED, true);
        if (!com.navdy.client.app.ui.base.BaseActivity.weHaveCalendarPermission() || !calendarSuggestionsAreEnabled) {
            callback.onSuggestionBuildComplete(calendarSuggestions);
            return;
        }
        logger.d("============ Calendar Events ============");
        com.navdy.client.app.ui.homescreen.CalendarUtils.readCalendarEvent(new com.navdy.client.app.framework.util.SuggestionManager.Anon5(callback, calendarSuggestions), true);
    }

    private static void buildMachineLearnedRecommendation(java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> calendarEventsAndRecommendations, com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener callback) {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(new com.navdy.client.app.framework.util.SuggestionManager.Anon6(calendarEventsAndRecommendations, callback));
        logger.v("Calling the Machine learning algorithm for a destination recommendation.");
        com.navdy.client.app.framework.suggestion.DestinationSuggestionService.suggestDestination(context, true);
    }

    private static synchronized void finishSuggestionBuild(boolean sendToHud) {
        synchronized (com.navdy.client.app.framework.util.SuggestionManager.class) {
            cachedImportantTips = importantTips;
            cachedRecommendations = recommendations;
            cachedUnimportantTips = unimportantTips;
            cachedHistory = history;
            java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> suggestions = getSuggestions();
            if (sendToHud) {
                com.navdy.client.app.framework.util.VersioningUtils.increaseVersionAndSendTheseSuggestionsToDisplayAsync(suggestions);
            }
            if (suggestionListIsEmpty()) {
                unimportantTips.add(getAddFavoriteSuggestion());
                suggestions = getSuggestions();
            }
            isBuildingSuggestionList.set(false);
            notifyListenersIfAny(suggestions);
        }
    }

    private static boolean suggestionListIsEmpty() {
        return trip == null && ota == null && importantTips.size() <= 0 && recommendations.size() <= 0 && unimportantTips.size() <= 0 && history.size() <= 0;
    }

    private static boolean addToSuggestions(com.navdy.client.app.framework.models.Suggestion suggestion, java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> suggestions) {
        return addToSuggestions(suggestion, suggestions, false);
    }

    private static boolean addToSuggestionsOnlyCheckForDuplicates(com.navdy.client.app.framework.models.Suggestion suggestion, java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> suggestions) {
        return addToSuggestions(suggestion, suggestions, true);
    }

    private static boolean addToSuggestions(com.navdy.client.app.framework.models.Suggestion suggestion, java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> suggestions, boolean onlyCheckForDuplicates) {
        if (suggestions == null) {
            logger.v("Can't add to suggestions list because the list is null!");
            return false;
        } else if (suggestion == null) {
            logger.v("Can't add to suggestions list because the suggestion is null!");
            return false;
        } else if (suggestion.destination == null) {
            logger.v("Can't add to suggestions list because the destination inside the suggestion is null!");
            return false;
        } else if (suggestion.destination.doNotSuggest) {
            logger.v("Can't add to suggestions list because the destination inside the suggestion is marked as doNotSuggest.");
            return false;
        } else if (isDuplicate(suggestion, suggestions)) {
            return false;
        } else {
            if (!onlyCheckForDuplicates) {
                if (suggestions.size() > 20) {
                    logger.v("Can't add to suggestions list because the list is full.");
                    return false;
                } else if (isTooCloseToCurrentLocation(suggestion.destination)) {
                    logger.v("Can't add to suggestions list because the destination is too close.");
                    return false;
                }
            }
            suggestions.add(suggestion);
            return true;
        }
    }

    private static boolean isTooCloseToCurrentLocation(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        com.navdy.service.library.events.location.Coordinate destinationCoord = com.navdy.client.app.framework.map.MapUtils.buildNewCoordinate(destination.displayLat, destination.displayLong);
        if (!com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(destinationCoord)) {
            destinationCoord = com.navdy.client.app.framework.map.MapUtils.buildNewCoordinate(destination.navigationLat, destination.navigationLong);
        }
        if (!com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(destinationCoord)) {
            logger.d("The destination has no coordinates so assuming we are too close");
            return true;
        }
        com.navdy.service.library.events.location.Coordinate smartStart = navdyLocationManager.getSmartStartCoordinates();
        if (smartStart == null) {
            logger.d("No smart start coordinates so assuming we are too close");
            return true;
        }
        double distanceBetween = com.navdy.client.app.framework.map.MapUtils.distanceBetween(destinationCoord, smartStart);
        logger.d("There is " + distanceBetween + "m. between the user and " + destination.toShortString());
        if (distanceBetween >= 200.0d) {
            return false;
        }
        return true;
    }

    private static void addThisHeader(int stringRes, @android.support.annotation.NonNull java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> suggestions) {
        suggestions.add(buildHeaderFor(stringRes));
    }

    @android.support.annotation.NonNull
    private static com.navdy.client.app.framework.models.Suggestion buildHeaderFor(int stringRes) {
        return new com.navdy.client.app.framework.models.Suggestion(new com.navdy.client.app.framework.models.Destination(com.navdy.client.app.NavdyApplication.getAppContext().getString(stringRes), ""), com.navdy.client.app.framework.models.Suggestion.SuggestionType.SECTION_HEADER);
    }

    private static boolean isDuplicate(com.navdy.client.app.framework.models.Suggestion suggestion, java.util.ArrayList<com.navdy.client.app.framework.models.Suggestion> suggestions) {
        java.util.Iterator it = suggestions.iterator();
        while (it.hasNext()) {
            if (com.navdy.client.app.framework.models.Destination.equals(((com.navdy.client.app.framework.models.Suggestion) it.next()).destination, suggestion.destination)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    @android.support.annotation.NonNull
    public static com.navdy.client.app.framework.models.Suggestion makeSuggestion(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.models.Suggestion.SuggestionType suggestionType) {
        return new com.navdy.client.app.framework.models.Suggestion(destination, suggestionType);
    }

    public static void onCalendarChanged(boolean sendToHud) {
        buildCalendarSuggestions(new com.navdy.client.app.framework.util.SuggestionManager.Anon7(sendToHud));
    }
}
