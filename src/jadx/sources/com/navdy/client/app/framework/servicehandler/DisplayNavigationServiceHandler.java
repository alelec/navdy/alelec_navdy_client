package com.navdy.client.app.framework.servicehandler;

public class DisplayNavigationServiceHandler {
    private static final boolean VERBOSE = false;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.class);
    @android.annotation.SuppressLint({"StaticFieldLeak"})
    private static final com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler singleton = new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler();
    private final com.squareup.otto.Bus bus = com.navdy.client.app.framework.util.BusProvider.getInstance();

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationRouteRequest val$event;

        Anon1(com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest) {
            this.val$event = navigationRouteRequest;
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.this.handleNavigationRouteRequest(this.val$event);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationSessionRouteChange val$event;

        Anon2(com.navdy.service.library.events.navigation.NavigationSessionRouteChange navigationSessionRouteChange) {
            this.val$event = navigationSessionRouteChange;
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.this.handleNavigationSessionRouteChange(this.val$event);
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationSessionStatusEvent val$event;

        Anon3(com.navdy.service.library.events.navigation.NavigationSessionStatusEvent navigationSessionStatusEvent) {
            this.val$event = navigationSessionStatusEvent;
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.this.handleNavigationSessionStatusEvent(this.val$event);
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationRouteStatus val$event;

        Anon4(com.navdy.service.library.events.navigation.NavigationRouteStatus navigationRouteStatus) {
            this.val$event = navigationRouteStatus;
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.this.handleNavigationRouteStatus(this.val$event);
        }
    }

    class Anon5 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationRouteResponse val$event;

        Anon5(com.navdy.service.library.events.navigation.NavigationRouteResponse navigationRouteResponse) {
            this.val$event = navigationRouteResponse;
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.this.handleNavigationRouteResponse(this.val$event);
        }
    }

    class Anon6 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationSessionDeferRequest val$event;

        Anon6(com.navdy.service.library.events.navigation.NavigationSessionDeferRequest navigationSessionDeferRequest) {
            this.val$event = navigationSessionDeferRequest;
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.this.handleNavigationSessionDeferRequest(this.val$event);
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationRouteCancelRequest val$event;

        Anon7(com.navdy.service.library.events.navigation.NavigationRouteCancelRequest navigationRouteCancelRequest) {
            this.val$event = navigationRouteCancelRequest;
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.this.handleNavigationRouteCancelRequest(this.val$event);
        }
    }

    class Anon8 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.navigation.NavigationSessionResponse val$event;

        Anon8(com.navdy.service.library.events.navigation.NavigationSessionResponse navigationSessionResponse) {
            this.val$event = navigationSessionResponse;
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.this.handleNavigationSessionResponse(this.val$event);
        }
    }

    class Anon9 implements com.here.android.mpa.common.OnEngineInitListener {
        final /* synthetic */ java.lang.Object val$event;

        Anon9(java.lang.Object obj) {
            this.val$event = obj;
        }

        public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
            if (error != com.here.android.mpa.common.OnEngineInitListener.Error.NONE) {
                com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.logger.e("can't post navigation events, error " + error.name());
            } else {
                com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.this.bus.post(this.val$event);
            }
        }
    }

    public static class ArrivedTripEvent {
    }

    private static class DeferStartActiveTripEvent {
        public final int delay;
        final long expireTimestamp;
        final java.lang.String requestId;
        final java.lang.String routeId;

        DeferStartActiveTripEvent(java.lang.String requestId2, java.lang.String routeId2, int delay2, long expireTimestamp2) {
            this.requestId = requestId2;
            this.routeId = routeId2;
            this.delay = delay2;
            this.expireTimestamp = expireTimestamp2;
        }
    }

    public static class DisplayInitiatedRequestEvent {
        public final com.navdy.client.app.framework.models.Destination destination;
        public final java.lang.String requestId;

        DisplayInitiatedRequestEvent(java.lang.String requestId2, com.navdy.client.app.framework.models.Destination destination2) {
            this.requestId = requestId2;
            this.destination = destination2;
        }
    }

    public static class HUDReadyEvent {
    }

    public static class RerouteActiveTripEvent {
        @android.support.annotation.Nullable
        public final com.navdy.client.app.framework.models.Destination destination;
        @android.support.annotation.NonNull
        public final com.navdy.service.library.events.navigation.NavigationRouteResult route;
        @android.support.annotation.NonNull
        final java.lang.String routeId;

        RerouteActiveTripEvent(@android.support.annotation.Nullable com.navdy.client.app.framework.models.Destination destination2, @android.support.annotation.NonNull com.navdy.service.library.events.navigation.NavigationRouteResult route2, @android.support.annotation.NonNull java.lang.String routeId2) {
            this.destination = destination2;
            this.route = route2;
            this.routeId = routeId2;
        }
    }

    public static class RouteCalculationProgress {
        public final java.lang.String handle;
        public final int progress;
        public final java.lang.String requestId;

        RouteCalculationProgress(java.lang.String requestId2, java.lang.String handle2, int progress2) {
            this.requestId = requestId2;
            this.handle = handle2;
            this.progress = progress2;
        }
    }

    public static class RoutesCalculatedEvent {
        public final java.lang.String requestId;
        public final java.util.List<com.navdy.service.library.events.navigation.NavigationRouteResult> routes;

        RoutesCalculatedEvent(java.util.List<com.navdy.service.library.events.navigation.NavigationRouteResult> routes2, java.lang.String requestId2) {
            this.routes = routes2;
            this.requestId = requestId2;
        }
    }

    public static class RoutesCalculationErrorEvent {
        @android.support.annotation.Nullable
        final java.lang.String cancelHandle;
        public final com.navdy.service.library.events.RequestStatus status;

        RoutesCalculationErrorEvent(com.navdy.service.library.events.RequestStatus status2, @android.support.annotation.Nullable java.lang.String cancelHandle2) {
            this.status = status2;
            this.cancelHandle = cancelHandle2;
        }
    }

    public static class StartActiveTripEvent {
        public final com.navdy.client.app.framework.models.Destination destination;
        public final com.navdy.service.library.events.navigation.NavigationRouteResult route;
        public final java.lang.String routeId;

        StartActiveTripEvent(com.navdy.client.app.framework.models.Destination destination2, com.navdy.service.library.events.navigation.NavigationRouteResult route2, java.lang.String routeId2) {
            this.destination = destination2;
            this.route = route2;
            this.routeId = routeId2;
        }
    }

    public static class StopActiveTripEvent {
        @android.support.annotation.Nullable
        public final java.lang.String handle;
        @android.support.annotation.Nullable
        public final java.lang.String requestId;
        @android.support.annotation.Nullable
        public final java.lang.String routeId;

        StopActiveTripEvent(@android.support.annotation.Nullable java.lang.String requestId2, @android.support.annotation.Nullable java.lang.String routeId2, @android.support.annotation.Nullable java.lang.String handle2) {
            this.requestId = requestId2;
            this.routeId = routeId2;
            this.handle = handle2;
        }
    }

    public static synchronized com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler getInstance() {
        com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler displayNavigationServiceHandler;
        synchronized (com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.class) {
            displayNavigationServiceHandler = singleton;
        }
        return displayNavigationServiceHandler;
    }

    private DisplayNavigationServiceHandler() {
        this.bus.register(this);
    }

    @com.squareup.otto.Subscribe
    public void onNavigationRouteRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.Anon1(event), 1);
    }

    @com.squareup.otto.Subscribe
    public void onNavigationSessionRouteChange(com.navdy.service.library.events.navigation.NavigationSessionRouteChange event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.Anon2(event), 1);
    }

    @com.squareup.otto.Subscribe
    public void onNavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionStatusEvent event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.Anon3(event), 1);
    }

    @com.squareup.otto.Subscribe
    public void onNavigationRouteStatus(com.navdy.service.library.events.navigation.NavigationRouteStatus event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.Anon4(event), 1);
    }

    @com.squareup.otto.Subscribe
    public void onNavigationRouteResponse(com.navdy.service.library.events.navigation.NavigationRouteResponse event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.Anon5(event), 1);
    }

    @com.squareup.otto.Subscribe
    public void onNavigationSessionDeferRequest(com.navdy.service.library.events.navigation.NavigationSessionDeferRequest event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.Anon6(event), 1);
    }

    @com.squareup.otto.Subscribe
    public void onNavigationRouteCancelRequest(com.navdy.service.library.events.navigation.NavigationRouteCancelRequest event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.Anon7(event), 1);
    }

    @com.squareup.otto.Subscribe
    public void onNavigationSessionResponse(com.navdy.service.library.events.navigation.NavigationSessionResponse event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.Anon8(event), 1);
    }

    private void handleNavigationRouteRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest) {
        if (navigationRouteRequest != null) {
            com.navdy.client.app.framework.models.Destination destination = null;
            java.lang.String destinationId = navigationRouteRequest.destination_identifier;
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destinationId)) {
                try {
                    destination = com.navdy.client.app.providers.NavdyContentProvider.getThisDestination(java.lang.Integer.parseInt(destinationId));
                } catch (java.lang.Exception e) {
                    logger.e("Unable to look up the destination for id " + destinationId, e);
                }
            }
            if (destination == null && navigationRouteRequest.requestDestination != null) {
                destination = new com.navdy.client.app.framework.models.Destination(navigationRouteRequest.requestDestination);
            }
            if (destination == null) {
                logger.w("handleNavigationRouteRequest, destination is null, no-op");
            } else {
                postWhenReady(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.DisplayInitiatedRequestEvent(navigationRouteRequest.requestId, destination));
            }
        }
    }

    private void handleNavigationSessionRouteChange(com.navdy.service.library.events.navigation.NavigationSessionRouteChange navigationSessionRouteChange) {
        if (navigationSessionRouteChange == null || navigationSessionRouteChange.newRoute == null) {
            logger.e("handleNavigationSessionRouteChange called with a null navigationSessionRouteChange or the newRoute is null.");
        } else {
            postWhenReady(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.StartActiveTripEvent(getDestination(navigationSessionRouteChange.newRoute, null), navigationSessionRouteChange.newRoute, navigationSessionRouteChange.newRoute.routeId));
        }
    }

    private void handleNavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionStatusEvent navigationSessionStatusEvent) {
        switch (navigationSessionStatusEvent.sessionState) {
            case NAV_SESSION_STARTED:
                postWhenReady(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.StartActiveTripEvent(getDestination(navigationSessionStatusEvent.route, navigationSessionStatusEvent.destination_identifier), navigationSessionStatusEvent.route, navigationSessionStatusEvent.routeId));
                return;
            case NAV_SESSION_REROUTED:
                postWhenReady(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.RerouteActiveTripEvent(getDestination(navigationSessionStatusEvent.route, navigationSessionStatusEvent.destination_identifier), navigationSessionStatusEvent.route, navigationSessionStatusEvent.routeId));
                return;
            case NAV_SESSION_ARRIVED:
                postWhenReady(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.ArrivedTripEvent());
                return;
            case NAV_SESSION_STOPPED:
                postWhenReady(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.StopActiveTripEvent(null, navigationSessionStatusEvent.routeId, null));
                return;
            case NAV_SESSION_ENGINE_READY:
                postWhenReady(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.HUDReadyEvent());
                return;
            default:
                return;
        }
    }

    private void postWhenReady(java.lang.Object event) {
        com.navdy.client.app.framework.map.HereMapsManager.getInstance().addOnInitializedListener(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.Anon9(event));
    }

    private void handleNavigationRouteStatus(com.navdy.service.library.events.navigation.NavigationRouteStatus navigationRouteStatus) {
        postWhenReady(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.RouteCalculationProgress(navigationRouteStatus.requestId, navigationRouteStatus.handle, navigationRouteStatus.progress.intValue()));
    }

    private void handleNavigationRouteResponse(com.navdy.service.library.events.navigation.NavigationRouteResponse navigationRouteResponse) {
        switch (navigationRouteResponse.status) {
            case REQUEST_SUCCESS:
                postWhenReady(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.RoutesCalculatedEvent(navigationRouteResponse.results, navigationRouteResponse.requestId));
                return;
            case REQUEST_NOT_READY:
            case REQUEST_NO_LOCATION_SERVICE:
            case REQUEST_SERVICE_ERROR:
            case REQUEST_UNKNOWN_ERROR:
            case REQUEST_NOT_AVAILABLE:
            case REQUEST_INVALID_REQUEST:
                java.lang.String cancelHandle = null;
                if (navigationRouteResponse.status == com.navdy.service.library.events.RequestStatus.REQUEST_ALREADY_IN_PROGRESS) {
                    cancelHandle = navigationRouteResponse.statusDetail;
                }
                postWhenReady(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.RoutesCalculationErrorEvent(navigationRouteResponse.status, cancelHandle));
                return;
            case REQUEST_CANCELLED:
                postWhenReady(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.StopActiveTripEvent(navigationRouteResponse.requestId, null, null));
                return;
            default:
                return;
        }
    }

    private void handleNavigationSessionDeferRequest(com.navdy.service.library.events.navigation.NavigationSessionDeferRequest navigationSessionDeferRequest) {
        postWhenReady(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.DeferStartActiveTripEvent(navigationSessionDeferRequest.requestId, navigationSessionDeferRequest.routeId, navigationSessionDeferRequest.delay.intValue(), navigationSessionDeferRequest.expireTimestamp.longValue()));
    }

    private void handleNavigationRouteCancelRequest(com.navdy.service.library.events.navigation.NavigationRouteCancelRequest navigationRouteCancelRequest) {
        postWhenReady(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.StopActiveTripEvent(null, null, navigationRouteCancelRequest.handle));
    }

    private void handleNavigationSessionResponse(com.navdy.service.library.events.navigation.NavigationSessionResponse navigationSessionResponse) {
        logger.d("navigationSessionResponse:\n\t" + navigationSessionResponse);
        if (navigationSessionResponse.pendingSessionState == com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED) {
            postWhenReady(new com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.StopActiveTripEvent(null, navigationSessionResponse.routeId, null));
        }
    }

    @android.support.annotation.Nullable
    private com.navdy.client.app.framework.models.Destination getDestination(com.navdy.service.library.events.navigation.NavigationRouteResult navigationRouteResult, java.lang.String destinationId) {
        com.navdy.client.app.framework.models.Destination destination = null;
        if (destinationId != null) {
            destination = com.navdy.client.app.providers.NavdyContentProvider.getThisDestination(destinationId);
        }
        if (destination != null || navigationRouteResult == null) {
            return destination;
        }
        com.navdy.client.app.framework.models.Destination destination2 = new com.navdy.client.app.framework.models.Destination(navigationRouteResult);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destinationId)) {
            return destination2;
        }
        com.navdy.client.app.framework.models.Destination destinationFromNavRouteResult = com.navdy.client.app.providers.NavdyContentProvider.getThisDestination(destination2);
        if (destinationFromNavRouteResult != null) {
            return destinationFromNavRouteResult;
        }
        return destination2;
    }
}
