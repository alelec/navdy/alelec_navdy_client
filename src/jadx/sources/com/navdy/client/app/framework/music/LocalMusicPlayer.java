package com.navdy.client.app.framework.music;

public class LocalMusicPlayer {
    private static final int PROGRESS_DELAY = 1000;
    private static final int SCRUB_DELAY = 300;
    private static final int SCRUB_STEP = 10000;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.music.LocalMusicPlayer.class);
    private final float DEFAULT_VOLUME = 1.0f;
    private final float DUCKING_VOLUME = 0.23f;
    private android.media.AudioManager.OnAudioFocusChangeListener audioFocusChangeListener = new com.navdy.client.app.framework.music.LocalMusicPlayer.Anon4();
    private int currentPosition = -1;
    private int currentTrackIndex = -1;
    private java.lang.Runnable fastForwardRunnable = new com.navdy.client.app.framework.music.LocalMusicPlayer.Anon2();
    private long firstUnplayableTrack = -1;
    private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private boolean isDuckedDueToAudioFocusLoss = false;
    private boolean isPausedDueToAudioFocusLoss = false;
    private com.navdy.client.app.framework.music.LocalMusicPlayer.Listener listener;
    private android.media.MediaPlayer mediaPlayer;
    private java.util.List<com.navdy.service.library.events.audio.MusicTrackInfo> originalList;
    private java.lang.Runnable progressUpdateRunnable = new com.navdy.client.app.framework.music.LocalMusicPlayer.Anon1();
    private java.lang.Runnable rewindRunnable = new com.navdy.client.app.framework.music.LocalMusicPlayer.Anon3();
    private com.navdy.service.library.events.audio.MusicShuffleMode shuffleMode = com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF;
    private java.util.List<com.navdy.service.library.events.audio.MusicTrackInfo> tracks;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (com.navdy.client.app.framework.music.LocalMusicPlayer.this.listener == null) {
                return;
            }
            if (com.navdy.client.app.framework.music.LocalMusicPlayer.this.mediaPlayer == null || !com.navdy.client.app.framework.music.LocalMusicPlayer.this.mediaPlayer.isPlaying()) {
                com.navdy.client.app.framework.music.LocalMusicPlayer.logger.w("Media player isn't playing - did you release it without clearing callbacks?");
                return;
            }
            com.navdy.client.app.framework.music.LocalMusicPlayer.this.currentPosition = com.navdy.client.app.framework.music.LocalMusicPlayer.this.mediaPlayer.getCurrentPosition();
            com.navdy.client.app.framework.music.LocalMusicPlayer.this.listener.onPositionUpdate(com.navdy.client.app.framework.music.LocalMusicPlayer.this.currentPosition, com.navdy.client.app.framework.music.LocalMusicPlayer.this.mediaPlayer.getDuration());
            com.navdy.client.app.framework.music.LocalMusicPlayer.this.handler.postDelayed(this, 1000);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            if (com.navdy.client.app.framework.music.LocalMusicPlayer.this.mediaPlayer != null) {
                com.navdy.client.app.framework.music.LocalMusicPlayer.this.seek(10000);
                com.navdy.client.app.framework.music.LocalMusicPlayer.this.handler.postDelayed(this, 300);
                return;
            }
            com.navdy.client.app.framework.music.LocalMusicPlayer.logger.w("Media player is null");
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            if (com.navdy.client.app.framework.music.LocalMusicPlayer.this.mediaPlayer != null) {
                com.navdy.client.app.framework.music.LocalMusicPlayer.this.seek(-10000);
                com.navdy.client.app.framework.music.LocalMusicPlayer.this.handler.postDelayed(this, 300);
                return;
            }
            com.navdy.client.app.framework.music.LocalMusicPlayer.logger.w("Media player is null");
        }
    }

    class Anon4 implements android.media.AudioManager.OnAudioFocusChangeListener {
        Anon4() {
        }

        public void onAudioFocusChange(int focusChange) {
            com.navdy.client.app.framework.music.LocalMusicPlayer.logger.i("onAudioFocusChange " + focusChange);
            if (focusChange >= 1) {
                if (com.navdy.client.app.framework.music.LocalMusicPlayer.this.isDuckedDueToAudioFocusLoss) {
                    com.navdy.client.app.framework.music.LocalMusicPlayer.this.isDuckedDueToAudioFocusLoss = false;
                    com.navdy.client.app.framework.music.LocalMusicPlayer.this.unduck();
                }
                if (com.navdy.client.app.framework.music.LocalMusicPlayer.this.isPausedDueToAudioFocusLoss) {
                    com.navdy.client.app.framework.music.LocalMusicPlayer.this.isPausedDueToAudioFocusLoss = false;
                    com.navdy.client.app.framework.music.LocalMusicPlayer.this.play();
                }
            } else if (focusChange == -3) {
                com.navdy.client.app.framework.music.LocalMusicPlayer.this.isDuckedDueToAudioFocusLoss = true;
                com.navdy.client.app.framework.music.LocalMusicPlayer.this.duck();
            } else if (focusChange == -2) {
                com.navdy.client.app.framework.music.LocalMusicPlayer.this.isPausedDueToAudioFocusLoss = true;
                com.navdy.client.app.framework.music.LocalMusicPlayer.this.pause(false);
            } else {
                com.navdy.client.app.framework.music.LocalMusicPlayer.this.pause(true);
            }
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            com.navdy.client.app.framework.music.LocalMusicPlayer.this.next();
        }
    }

    class Anon6 implements android.media.MediaPlayer.OnCompletionListener {
        Anon6() {
        }

        public void onCompletion(android.media.MediaPlayer mp) {
            com.navdy.client.app.framework.music.LocalMusicPlayer.logger.i("onCompletion");
            com.navdy.client.app.framework.music.LocalMusicPlayer.this.next();
        }
    }

    public interface Listener {
        void onMetadataUpdate(com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo);

        void onPlaybackStateUpdate(com.navdy.service.library.events.audio.MusicPlaybackState musicPlaybackState);

        void onPositionUpdate(int i, int i2);
    }

    public void initWithQueue(java.util.List<com.navdy.service.library.events.audio.MusicTrackInfo> tracks2) {
        logger.d("init");
        this.originalList = tracks2;
        this.tracks = this.originalList;
        this.shuffleMode = com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF;
        this.currentTrackIndex = -1;
        this.currentPosition = -1;
        this.firstUnplayableTrack = -1;
    }

    public void setListener(com.navdy.client.app.framework.music.LocalMusicPlayer.Listener listener2) {
        this.listener = listener2;
    }

    public com.navdy.client.app.framework.music.LocalMusicPlayer.Listener getListener() {
        return this.listener;
    }

    public boolean isActive() {
        return this.tracks != null;
    }

    public void play(int index) {
        logger.i("play " + index);
        this.handler.removeCallbacks(this.progressUpdateRunnable);
        if (index == this.currentTrackIndex && this.mediaPlayer != null && this.mediaPlayer.getCurrentPosition() == this.currentPosition) {
            this.mediaPlayer.start();
            return;
        }
        createMediaPlayer(index);
        if (this.mediaPlayer == null) {
            logger.e("MediaPlayer init failed");
            this.currentTrackIndex = index;
            if (((long) index) != this.firstUnplayableTrack) {
                this.handler.post(new com.navdy.client.app.framework.music.LocalMusicPlayer.Anon5());
            } else {
                logger.e("No playable tracks in the queue");
            }
            if (this.firstUnplayableTrack == -1) {
                this.firstUnplayableTrack = (long) index;
                return;
            }
            return;
        }
        this.firstUnplayableTrack = -1;
        if (this.isDuckedDueToAudioFocusLoss) {
            this.mediaPlayer.setVolume(0.23f, 0.23f);
        }
        this.mediaPlayer.start();
        if (index == this.currentTrackIndex && this.currentPosition != -1) {
            logger.i("resume - seeking to position " + this.currentPosition);
            this.mediaPlayer.seekTo(this.currentPosition);
        }
        this.currentTrackIndex = index;
        this.mediaPlayer.setOnCompletionListener(new com.navdy.client.app.framework.music.LocalMusicPlayer.Anon6());
        if (this.listener != null) {
            this.listener.onMetadataUpdate(new com.navdy.service.library.events.audio.MusicTrackInfo.Builder((com.navdy.service.library.events.audio.MusicTrackInfo) this.tracks.get(this.currentTrackIndex)).shuffleMode(this.shuffleMode).build());
            this.handler.post(this.progressUpdateRunnable);
        }
    }

    private void createMediaPlayer(int index) {
        if (this.mediaPlayer != null) {
            releaseCurrentPlayer();
        }
        if (index < 0 || index >= this.tracks.size()) {
            logger.e("Index out of range");
            return;
        }
        java.lang.String trackIdStr = ((com.navdy.service.library.events.audio.MusicTrackInfo) this.tracks.get(index)).trackId;
        try {
            android.net.Uri uri = android.net.Uri.parse("content://media/external/audio/media/" + java.lang.Long.parseLong(trackIdStr));
            logger.d("creating media player for index " + index + ", URI: " + uri);
            int result = getAudioManager().requestAudioFocus(this.audioFocusChangeListener, 3, 1);
            if (result != 1) {
                logger.w("Couldn't get audio focus: " + result);
            }
            try {
                this.mediaPlayer = android.media.MediaPlayer.create(com.navdy.client.app.NavdyApplication.getAppContext(), uri);
            } catch (Throwable t) {
                logger.e("MediaPlayer.create() threw " + t);
            }
        } catch (java.lang.NumberFormatException e) {
            logger.w("Invalid track ID " + trackIdStr);
        }
    }

    public void play() {
        play(this.currentTrackIndex);
    }

    public void pause(boolean releaseMediaPlayer) {
        logger.i("pause " + this.currentTrackIndex);
        this.handler.removeCallbacks(this.progressUpdateRunnable);
        if (this.mediaPlayer == null) {
            logger.e("no mediaPlayer!");
            return;
        }
        this.currentPosition = this.mediaPlayer.getCurrentPosition();
        if (this.listener != null) {
            this.listener.onPlaybackStateUpdate(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED);
        }
        this.mediaPlayer.pause();
        if (releaseMediaPlayer) {
            releaseCurrentPlayer();
        }
    }

    public void next() {
        this.currentPosition = -1;
        this.currentTrackIndex++;
        int numTracks = this.tracks.size();
        if (numTracks > 0) {
            this.currentTrackIndex %= numTracks;
        }
        play(this.currentTrackIndex);
    }

    public void previous() {
        this.currentPosition = -1;
        if (this.mediaPlayer == null || this.mediaPlayer.getCurrentPosition() <= 2000) {
            this.currentTrackIndex--;
            int numTracks = this.tracks.size();
            if (numTracks > 0) {
                this.currentTrackIndex = (this.currentTrackIndex + numTracks) % numTracks;
            } else {
                this.currentTrackIndex = 0;
            }
            play(this.currentTrackIndex);
            return;
        }
        this.mediaPlayer.seekTo(0);
    }

    public void startFastForward() {
        if (this.mediaPlayer == null) {
            logger.e("no mediaPlayer!");
        } else {
            this.handler.post(this.fastForwardRunnable);
        }
    }

    public void stopFastForward() {
        this.handler.removeCallbacks(this.fastForwardRunnable);
    }

    public void startRewind() {
        if (this.mediaPlayer == null) {
            logger.e("no mediaPlayer!");
        } else {
            this.handler.post(this.rewindRunnable);
        }
    }

    public void stopRewind() {
        this.handler.removeCallbacks(this.rewindRunnable);
    }

    public void shuffle(com.navdy.service.library.events.audio.MusicShuffleMode shuffleMode2) {
        if (this.shuffleMode == shuffleMode2) {
            logger.e("Already set shuffle mode " + shuffleMode2);
            return;
        }
        java.lang.String currentTrackId = null;
        if (this.currentTrackIndex != -1) {
            currentTrackId = ((com.navdy.service.library.events.audio.MusicTrackInfo) this.tracks.get(this.currentTrackIndex)).trackId;
        }
        if (shuffleMode2 == com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS) {
            java.util.List<com.navdy.service.library.events.audio.MusicTrackInfo> shuffledList = new java.util.ArrayList<>(this.tracks);
            java.util.Collections.shuffle(shuffledList);
            this.tracks = shuffledList;
        } else if (shuffleMode2 == com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF) {
            this.tracks = this.originalList;
        } else {
            logger.e("Unsupported shuffle mode " + shuffleMode2);
            return;
        }
        if (currentTrackId != null) {
            int i = 0;
            while (true) {
                if (i >= this.tracks.size()) {
                    break;
                } else if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(((com.navdy.service.library.events.audio.MusicTrackInfo) this.tracks.get(i)).trackId, currentTrackId)) {
                    this.currentTrackIndex = i;
                    break;
                } else {
                    i++;
                }
            }
        }
        this.shuffleMode = shuffleMode2;
        if (this.listener != null && this.currentTrackIndex != -1) {
            this.listener.onMetadataUpdate(new com.navdy.service.library.events.audio.MusicTrackInfo.Builder((com.navdy.service.library.events.audio.MusicTrackInfo) this.tracks.get(this.currentTrackIndex)).shuffleMode(shuffleMode2).build());
        }
    }

    private void seek(int positionChange) {
        this.currentPosition = this.mediaPlayer.getCurrentPosition();
        this.mediaPlayer.seekTo(this.currentPosition + positionChange);
    }

    private void duck() {
        if (this.mediaPlayer == null) {
            logger.e("no mediaPlayer!");
        } else {
            this.mediaPlayer.setVolume(0.23f, 0.23f);
        }
    }

    private void unduck() {
        if (this.mediaPlayer == null) {
            logger.e("no mediaPlayer!");
        } else {
            this.mediaPlayer.setVolume(1.0f, 1.0f);
        }
    }

    private void releaseCurrentPlayer() {
        this.handler.removeCallbacksAndMessages(null);
        this.mediaPlayer.reset();
        this.mediaPlayer.release();
        this.mediaPlayer = null;
        if (!this.isDuckedDueToAudioFocusLoss) {
            getAudioManager().abandonAudioFocus(this.audioFocusChangeListener);
        }
    }

    private android.media.AudioManager getAudioManager() {
        return (android.media.AudioManager) com.navdy.client.app.NavdyApplication.getAppContext().getSystemService("audio");
    }
}
