package com.navdy.client.app.framework.callcontrol;

public class TelephonySupport implements com.navdy.client.app.framework.callcontrol.TelephonyInterface {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.callcontrol.TelephonySupport.class);
    private android.content.Context context;
    private android.telephony.TelephonyManager telephonyManager;

    public TelephonySupport(android.content.Context context2) {
        this.context = context2;
        this.telephonyManager = (android.telephony.TelephonyManager) context2.getSystemService("phone");
    }

    public void acceptRingingCall() {
        android.content.Intent intent = new android.content.Intent("android.intent.action.MEDIA_BUTTON");
        intent.putExtra("android.intent.extra.KEY_EVENT", new android.view.KeyEvent(0, 79));
        this.context.sendOrderedBroadcast(intent, null);
        android.content.Intent intent2 = new android.content.Intent("android.intent.action.MEDIA_BUTTON");
        intent2.putExtra("android.intent.extra.KEY_EVENT", new android.view.KeyEvent(1, 79));
        this.context.sendOrderedBroadcast(intent2, null);
    }

    public void rejectRingingCall() {
        endCall();
    }

    public void endCall() {
        try {
            endPhoneCall();
        } catch (Throwable ex) {
            sLogger.d("endCall failed -" + ex.toString(), ex);
        }
    }

    public void toggleMute() {
        try {
            java.lang.reflect.Method methodGetITelephony = java.lang.Class.forName(this.telephonyManager.getClass().getName()).getDeclaredMethod("getITelephony", new java.lang.Class[0]);
            methodGetITelephony.setAccessible(true);
            java.lang.Object telephonyInterface = methodGetITelephony.invoke(this.telephonyManager, new java.lang.Object[0]);
            java.lang.Class telephonyInterfaceClass = java.lang.Class.forName(telephonyInterface.getClass().getName());
            java.lang.reflect.Method methodCallState = telephonyInterfaceClass.getDeclaredMethod("getCallState", new java.lang.Class[0]);
            methodCallState.setAccessible(true);
            java.lang.reflect.Method methodMute = telephonyInterfaceClass.getDeclaredMethod("mute", new java.lang.Class[]{java.lang.Boolean.TYPE});
            methodMute.setAccessible(true);
            if (((java.lang.Integer) methodCallState.invoke(telephonyInterface, new java.lang.Object[0])).intValue() == 2) {
                methodMute.invoke(telephonyInterface, new java.lang.Object[]{java.lang.Boolean.valueOf(true)});
                return;
            }
            methodMute.invoke(telephonyInterface, new java.lang.Object[]{java.lang.Boolean.valueOf(false)});
        } catch (java.lang.Exception ex) {
            sLogger.e("toggleMute failed", ex);
        }
    }

    public static void endPhoneCall() throws java.lang.Exception {
        android.telephony.TelephonyManager telephonyManager2 = (android.telephony.TelephonyManager) com.navdy.client.app.NavdyApplication.getAppContext().getSystemService("phone");
        java.lang.reflect.Method methodGetITelephony = java.lang.Class.forName(telephonyManager2.getClass().getName()).getDeclaredMethod("getITelephony", new java.lang.Class[0]);
        methodGetITelephony.setAccessible(true);
        java.lang.Object telephonyInterface = methodGetITelephony.invoke(telephonyManager2, new java.lang.Object[0]);
        java.lang.Class.forName(telephonyInterface.getClass().getName()).getDeclaredMethod("endCall", new java.lang.Class[0]).invoke(telephonyInterface, new java.lang.Object[0]);
    }
}
