package com.navdy.client.app.framework.servicehandler;

public class BatteryStatusManager {
    private static final int LEVEL_EXTREMELY_LOW = 10;
    private static final int LEVEL_LOW = 20;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.BatteryStatusManager.class);
    private boolean lastCharging;
    private int lastLevel = -1;
    private com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus lastStatus;
    private android.content.BroadcastReceiver receiver = new com.navdy.client.app.framework.servicehandler.BatteryStatusManager.Anon1();
    private boolean registered;
    private com.navdy.service.library.device.RemoteDevice remoteDevice;

    class Anon1 extends android.content.BroadcastReceiver {
        Anon1() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            int status = intent.getIntExtra("status", -1);
            boolean isCharging = status == 2 || status == 5;
            int level = intent.getIntExtra("level", -1);
            int scale = intent.getIntExtra("scale", -1);
            if (level == -1 || scale == -1) {
                com.navdy.client.app.framework.servicehandler.BatteryStatusManager.sLogger.v("[battery] invalid info");
                return;
            }
            com.navdy.service.library.events.callcontrol.PhoneBatteryStatus phoneBatteryStatus = com.navdy.client.app.framework.servicehandler.BatteryStatusManager.this.getPhoneBatteryStatus((level * 100) / scale, isCharging);
            if (phoneBatteryStatus != null) {
                com.navdy.client.app.framework.servicehandler.BatteryStatusManager.this.sendEvent(phoneBatteryStatus);
            }
        }
    }

    public BatteryStatusManager(com.navdy.service.library.device.RemoteDevice remoteDevice2) {
        this.remoteDevice = remoteDevice2;
    }

    public synchronized void register() {
        if (!this.registered) {
            com.navdy.client.app.NavdyApplication.getAppContext().registerReceiver(this.receiver, new android.content.IntentFilter("android.intent.action.BATTERY_CHANGED"));
            this.registered = true;
        }
        this.lastLevel = -1;
        this.lastCharging = false;
        this.lastStatus = null;
    }

    public synchronized void unregister() {
        if (this.registered) {
            com.navdy.client.app.NavdyApplication.getAppContext().unregisterReceiver(this.receiver);
            this.registered = false;
        }
        this.lastLevel = -1;
        this.lastCharging = false;
        this.lastStatus = null;
    }

    private void sendEvent(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus status) {
        try {
            this.remoteDevice.postEvent((com.squareup.wire.Message) status);
            sLogger.v("[battery] status sent");
        } catch (Throwable t) {
            sLogger.e("[battery]", t);
        }
    }

    private com.navdy.service.library.events.callcontrol.PhoneBatteryStatus getPhoneBatteryStatus(int newLevel, boolean charging) {
        com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus status;
        if (newLevel > 20) {
            status = com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus.BATTERY_OK;
        } else if (newLevel > 10 && newLevel <= 20) {
            status = com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus.BATTERY_LOW;
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.BATTERY_LOW);
        } else if (newLevel <= 0 || newLevel > 20) {
            return null;
        } else {
            status = com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus.BATTERY_EXTREMELY_LOW;
        }
        if (this.lastLevel != -1 && this.lastStatus == status && this.lastCharging == charging) {
            return null;
        }
        this.lastCharging = charging;
        this.lastLevel = newLevel;
        this.lastStatus = status;
        sLogger.v("[battery] status[" + this.lastStatus + "] level [" + this.lastLevel + "] charging[" + charging + "]");
        return new com.navdy.service.library.events.callcontrol.PhoneBatteryStatus(this.lastStatus, java.lang.Integer.valueOf(this.lastLevel), java.lang.Boolean.valueOf(this.lastCharging));
    }
}
