package com.navdy.client.app.framework.servicehandler;

public class CalendarHandler {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.CalendarHandler.class);
    private com.navdy.client.app.framework.util.CalendarObserver calendarObserver = new com.navdy.client.app.framework.util.CalendarObserver(new android.os.Handler());
    private android.content.Context context;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.CalendarHandler.this.context.getContentResolver().registerContentObserver(android.provider.CalendarContract.Events.CONTENT_URI, false, com.navdy.client.app.framework.servicehandler.CalendarHandler.this.calendarObserver);
        }
    }

    public CalendarHandler(android.content.Context context2) {
        this.context = context2;
        registerObserver();
    }

    public void registerObserver() {
        sLogger.d("registerObserver");
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.CalendarHandler.Anon1(), 1);
    }

    public void unregisterObserver() {
        sLogger.d("unregisterObserver");
        this.context.getContentResolver().unregisterContentObserver(this.calendarObserver);
    }

    public com.navdy.client.app.framework.util.CalendarObserver getCalendarObserver() {
        return this.calendarObserver;
    }
}
