package com.navdy.client.app.framework.util;

public class NetworkUtils {
    public static boolean isNetworkAvailable() {
        android.net.NetworkInfo activeNetworkInfo = ((android.net.ConnectivityManager) com.navdy.client.app.NavdyApplication.getAppContext().getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
