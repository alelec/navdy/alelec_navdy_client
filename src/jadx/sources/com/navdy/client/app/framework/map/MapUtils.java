package com.navdy.client.app.framework.map;

public class MapUtils {
    private static final int EARTH_RADIUS = 6371000;
    private static final double MAX_LAT = java.lang.Math.toRadians(90.0d);
    private static final double MAX_LON = java.lang.Math.toRadians(180.0d);
    private static final double METERS_PER_FOOT = 0.3048d;
    private static final int METERS_PER_KM = 1000;
    public static final double METERS_PER_MILE = 1609.34d;
    private static final double MIN_LAT = java.lang.Math.toRadians(-90.0d);
    private static final double MIN_LON = java.lang.Math.toRadians(-180.0d);
    private static final boolean VERBOSE = false;
    public static final int hereMapSidePadding;
    public static final int hereMapTopDownPadding;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.map.MapUtils.class);

    static class Anon1 extends android.os.AsyncTask<java.lang.Object, java.lang.Object, java.util.List<android.location.Address>> {
        final /* synthetic */ java.lang.Runnable val$callback;
        final /* synthetic */ com.google.android.gms.maps.model.LatLng val$center;
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;

        Anon1(com.google.android.gms.maps.model.LatLng latLng, com.navdy.client.app.framework.models.Destination destination, java.lang.Runnable runnable) {
            this.val$center = latLng;
            this.val$destination = destination;
            this.val$callback = runnable;
        }

        protected java.util.List<android.location.Address> doInBackground(java.lang.Object[] params) {
            if (com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet()) {
                try {
                    return new android.location.Geocoder(com.navdy.client.app.NavdyApplication.getAppContext(), java.util.Locale.getDefault()).getFromLocation(this.val$center.latitude, this.val$center.longitude, 1);
                } catch (java.lang.Exception e) {
                    com.navdy.client.app.framework.map.MapUtils.logger.e("Unable to reverse geocode the current position.", e);
                }
            } else {
                if (this.val$destination.hasValidDisplayCoordinates()) {
                    com.navdy.client.app.framework.map.MapUtils.logger.v("processDestination, destination does not have address, assigning display coordinates as address");
                    this.val$destination.setRawAddressNotForDisplay(this.val$destination.displayLat + "," + this.val$destination.displayLong);
                } else if (this.val$destination.hasValidNavCoordinates()) {
                    com.navdy.client.app.framework.map.MapUtils.logger.v("processDestination, destination does not have address, assigning nav coordinates as address");
                    this.val$destination.setRawAddressNotForDisplay(this.val$destination.navigationLat + "," + this.val$destination.navigationLong);
                }
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(java.util.List<android.location.Address> addresses) {
            if (addresses != null && !addresses.isEmpty()) {
                android.location.Address address = (android.location.Address) addresses.get(0);
                this.val$destination.setStreetNumber(address.getFeatureName());
                this.val$destination.setStreetName(address.getThoroughfare());
                this.val$destination.setCity(address.getLocality());
                this.val$destination.setZipCode(address.getPostalCode());
                this.val$destination.setState(address.getAdminArea());
                this.val$destination.setCountry(address.getCountryName());
                this.val$destination.setCountryCode(address.getCountryCode());
                this.val$destination.setName(address.getAddressLine(0));
                java.util.ArrayList<java.lang.String> addressFragments = new java.util.ArrayList<>();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressFragments.add(address.getAddressLine(i));
                }
                java.lang.String addressString = android.text.TextUtils.join(java.lang.System.getProperty("line.separator"), addressFragments);
                com.navdy.client.app.framework.map.MapUtils.logger.d("Full address = " + addressString);
                this.val$destination.setRawAddressNotForDisplay(addressString);
            }
            this.val$callback.run();
        }
    }

    private enum UnitSystem {
        METRIC,
        IMPERIAL
    }

    static {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        android.graphics.drawable.BitmapDrawable markerDrawable = (android.graphics.drawable.BitmapDrawable) android.support.v4.content.ContextCompat.getDrawable(context, com.navdy.client.R.drawable.icon_pin_place);
        android.graphics.drawable.BitmapDrawable positionMarkerDrawable = (android.graphics.drawable.BitmapDrawable) android.support.v4.content.ContextCompat.getDrawable(context, com.navdy.client.R.drawable.icon_user_location);
        hereMapSidePadding = java.lang.Math.max(markerDrawable.getBitmap().getWidth(), positionMarkerDrawable.getBitmap().getWidth());
        hereMapTopDownPadding = java.lang.Math.max(markerDrawable.getBitmap().getHeight(), positionMarkerDrawable.getBitmap().getHeight());
    }

    public static java.util.List<com.google.android.gms.maps.model.LatLng> getGmsLatLongsFromRouteResult(com.navdy.service.library.events.navigation.NavigationRouteResult routeResult) {
        java.util.List<com.google.android.gms.maps.model.LatLng> result = new java.util.ArrayList<>();
        if (!(routeResult == null || routeResult.routeLatLongs == null || routeResult.routeLatLongs.size() % 2 == 1)) {
            java.util.ListIterator<java.lang.Float> iterator = routeResult.routeLatLongs.listIterator();
            while (iterator.hasNext()) {
                result.add(new com.google.android.gms.maps.model.LatLng((double) ((java.lang.Float) iterator.next()).floatValue(), (double) ((java.lang.Float) iterator.next()).floatValue()));
            }
        }
        return result;
    }

    public static java.lang.String formatDistance(int meters, com.navdy.client.app.framework.map.MapUtils.UnitSystem unitSystem) {
        android.content.res.Resources resources = com.navdy.client.app.NavdyApplication.getAppContext().getResources();
        if (unitSystem == com.navdy.client.app.framework.map.MapUtils.UnitSystem.METRIC) {
            if (((float) meters) >= 10000.0f) {
                return resources.getString(com.navdy.client.R.string.kilometers_abbrev, new java.lang.Object[]{java.lang.String.valueOf((int) ((double) java.lang.Math.round(((double) meters) / 1000.0d)))});
            } else if (meters > 1000) {
                return resources.getString(com.navdy.client.R.string.kilometers_abbrev, new java.lang.Object[]{java.lang.String.format(java.util.Locale.getDefault(), "%.1f", new java.lang.Object[]{java.lang.Double.valueOf(((double) java.lang.Math.round((((double) meters) / 1000.0d) * 10.0d)) / 10.0d)})});
            } else {
                return resources.getString(com.navdy.client.R.string.meters_abbrev, new java.lang.Object[]{java.lang.String.valueOf(meters)});
            }
        } else if (((double) meters) >= 16093.4d) {
            return resources.getString(com.navdy.client.R.string.miles_abbrev, new java.lang.Object[]{java.lang.String.valueOf((int) ((double) java.lang.Math.round(((double) meters) / 1609.34d)))});
        } else if (((double) meters) >= 160.934d) {
            return resources.getString(com.navdy.client.R.string.miles_abbrev, new java.lang.Object[]{java.lang.String.format(java.util.Locale.getDefault(), "%.1f", new java.lang.Object[]{java.lang.Double.valueOf(((double) java.lang.Math.round((((double) meters) / 1609.34d) * 10.0d)) / 10.0d)})});
        } else {
            return resources.getString(com.navdy.client.R.string.feet_abbrev, new java.lang.Object[]{java.lang.String.valueOf(((double) java.lang.Math.round((((double) meters) / METERS_PER_FOOT) * 10.0d)) / 10.0d)});
        }
    }

    public static void setMarkerImageResource(com.google.android.gms.maps.model.Marker marker, int resId) {
        if (marker != null) {
            try {
                marker.setIcon(com.google.android.gms.maps.model.BitmapDescriptorFactory.fromResource(resId));
            } catch (java.lang.Exception e) {
                logger.e("Unable to set the marker image. marker = " + marker);
            }
        }
    }

    @android.support.annotation.Nullable
    public static com.here.android.mpa.mapping.MapPolyline generateRoutePolyline(@android.support.annotation.NonNull com.here.android.mpa.common.GeoPolyline route) {
        return generatePolyline(route, com.navdy.client.R.color.blue_here_maps, com.navdy.client.R.integer.current_route_z_index);
    }

    @android.support.annotation.Nullable
    public static com.here.android.mpa.mapping.MapPolyline generateProgressPolyline(@android.support.annotation.NonNull com.here.android.mpa.common.GeoPolyline progress) {
        return generatePolyline(progress, com.navdy.client.R.color.black, com.navdy.client.R.integer.current_progress_z_index);
    }

    @android.support.annotation.Nullable
    public static com.here.android.mpa.mapping.MapPolyline generateAlternateRoutePolyline(@android.support.annotation.NonNull com.here.android.mpa.common.GeoPolyline geoPolyline) {
        return generatePolyline(geoPolyline, com.navdy.client.R.color.grey_here_maps, com.navdy.client.R.integer.alternate_route_z_index);
    }

    @org.jetbrains.annotations.Contract("null, _ -> fail; !null, null -> fail")
    public static float distanceBetween(android.location.Location location1, android.location.Location location2) {
        if (location1 != null && location2 != null) {
            return distanceBetween(location1.getLatitude(), location1.getLongitude(), location2.getLatitude(), location2.getLongitude());
        }
        throw new java.lang.IllegalArgumentException("one or more arguments are null");
    }

    @org.jetbrains.annotations.Contract("null, _ -> fail; !null, null -> fail")
    public static float distanceBetween(com.navdy.service.library.events.location.Coordinate coordinate, android.location.Location location) {
        if (coordinate != null && location != null) {
            return distanceBetween(coordinate.latitude.doubleValue(), coordinate.longitude.doubleValue(), location.getLatitude(), location.getLongitude());
        }
        throw new java.lang.IllegalArgumentException("one or more arguments are null");
    }

    @org.jetbrains.annotations.Contract("null, _ -> fail; !null, null -> fail")
    public static double distanceBetween(com.navdy.service.library.events.location.Coordinate first, com.navdy.service.library.events.location.Coordinate second) {
        if (first != null && second != null) {
            return (double) distanceBetween(first.latitude.doubleValue(), first.longitude.doubleValue(), second.latitude.doubleValue(), second.longitude.doubleValue());
        }
        throw new java.lang.IllegalArgumentException("one or more arguments are null");
    }

    @org.jetbrains.annotations.Contract("null, _ -> fail; !null, null -> fail")
    public static float distanceBetween(com.google.android.gms.maps.model.LatLng first, com.google.android.gms.maps.model.LatLng second) {
        if (first != null && second != null) {
            return distanceBetween(first.latitude, first.longitude, second.latitude, second.longitude);
        }
        throw new java.lang.IllegalArgumentException("one or more arguments are null");
    }

    @org.jetbrains.annotations.Contract("null, _ -> fail; !null, null -> fail")
    public static float distanceBetween(com.navdy.service.library.events.location.Coordinate coordinate, com.google.android.gms.maps.model.LatLng latLng) {
        if (coordinate != null && latLng != null) {
            return distanceBetween(coordinate.latitude.doubleValue(), coordinate.longitude.doubleValue(), latLng.latitude, latLng.longitude);
        }
        throw new java.lang.IllegalArgumentException("one or more arguments are null");
    }

    private static float distanceBetween(double firstLat, double firstLng, double secondLat, double secondLng) {
        float[] results = new float[1];
        android.location.Location.distanceBetween(firstLat, firstLng, secondLat, secondLng, results);
        return results[0];
    }

    private static boolean areCloseToEachOther(com.navdy.client.app.framework.models.Destination d1, com.navdy.client.app.framework.models.Destination d2, double coordLimit, float distanceLimit) {
        return areCloseToEachOther(d1.displayLat, d1.displayLong, d2.displayLat, d2.displayLong, coordLimit, distanceLimit) || areCloseToEachOther(d1.navigationLat, d1.navigationLong, d2.navigationLat, d2.navigationLong, coordLimit, distanceLimit) || areCloseToEachOther(d1.displayLat, d1.displayLong, d2.navigationLat, d2.navigationLong, coordLimit, distanceLimit) || areCloseToEachOther(d1.navigationLat, d1.navigationLong, d2.displayLat, d2.displayLong, coordLimit, distanceLimit);
    }

    private static boolean areCloseToEachOther(double latitude1, double longitude1, double latitude2, double longitude2, double coordLimit, float distanceLimit) {
        return !(latitude1 == 0.0d && longitude1 == 0.0d) && !(latitude2 == 0.0d && longitude2 == 0.0d) && java.lang.Math.abs(latitude1 - latitude2) < coordLimit && java.lang.Math.abs(longitude1 - longitude2) < coordLimit && distanceBetween(latitude1, longitude1, latitude2, longitude2) < distanceLimit;
    }

    public static android.location.Location[] getBoundingBox(double latitude, double longitude, double distance) {
        double minLon;
        double maxLon;
        double latRad = java.lang.Math.toRadians(latitude);
        double longRad = java.lang.Math.toRadians(longitude);
        double angularDistance = distance / 6371000.0d;
        double minLat = latRad - angularDistance;
        double maxLat = latRad + angularDistance;
        if (minLat <= MIN_LAT || maxLat >= MAX_LAT) {
            minLat = java.lang.Math.max(minLat, MIN_LAT);
            maxLat = java.lang.Math.min(maxLat, MAX_LAT);
            minLon = MIN_LON;
            maxLon = MAX_LON;
        } else {
            double deltaLon = java.lang.Math.asin(java.lang.Math.sin(angularDistance) / java.lang.Math.cos(latRad));
            minLon = longRad - deltaLon;
            if (minLon < MIN_LON) {
                minLon += 6.283185307179586d;
            }
            maxLon = longRad + deltaLon;
            if (maxLon > MAX_LON) {
                maxLon -= 6.283185307179586d;
            }
        }
        android.location.Location min = new android.location.Location("");
        min.setLatitude(java.lang.Math.toDegrees(minLat));
        min.setLongitude(java.lang.Math.toDegrees(minLon));
        android.location.Location max = new android.location.Location("");
        max.setLatitude(java.lang.Math.toDegrees(maxLat));
        max.setLongitude(java.lang.Math.toDegrees(maxLon));
        return new android.location.Location[]{min, max};
    }

    public static boolean isValidSetOfCoordinates(double latitude, double longitude) {
        return (latitude == 0.0d && longitude == 0.0d) ? false : true;
    }

    public static boolean isValidSetOfCoordinates(com.google.android.gms.maps.model.LatLng latLng) {
        return latLng != null && isValidSetOfCoordinates(latLng.latitude, latLng.longitude);
    }

    public static boolean isValidSetOfCoordinates(com.navdy.service.library.events.location.Coordinate coordinates) {
        return coordinates != null && isValidSetOfCoordinates(coordinates.latitude.doubleValue(), coordinates.longitude.doubleValue());
    }

    public static boolean isValidSetOfCoordinates(android.location.Location location) {
        return location != null && isValidSetOfCoordinates(location.getLatitude(), location.getLongitude());
    }

    public static boolean isValidSetOfCoordinates(com.here.android.mpa.common.GeoCoordinate coordinate) {
        return coordinate != null && isValidSetOfCoordinates(coordinate.getLatitude(), coordinate.getLongitude());
    }

    public static com.navdy.service.library.events.location.Coordinate buildNewCoordinate(double latitude, double longitude) {
        return new com.navdy.service.library.events.location.Coordinate.Builder().latitude(java.lang.Double.valueOf(latitude)).longitude(java.lang.Double.valueOf(longitude)).build();
    }

    private static com.here.android.mpa.mapping.MapPolyline generatePolyline(@android.support.annotation.Nullable com.here.android.mpa.common.GeoPolyline geoPolyline, int colorId, int zIndexId) {
        if (geoPolyline == null) {
            return null;
        }
        com.here.android.mpa.mapping.MapPolyline mapPolyline = new com.here.android.mpa.mapping.MapPolyline(geoPolyline);
        android.content.res.Resources resources = com.navdy.client.app.NavdyApplication.getAppContext().getResources();
        mapPolyline.setLineWidth(resources.getInteger(com.navdy.client.R.integer.map_polyline_width));
        mapPolyline.setLineColor(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), colorId));
        mapPolyline.setZIndex(resources.getInteger(zIndexId));
        return mapPolyline;
    }

    public static void doReverseGeocodingFor(@android.support.annotation.NonNull com.google.android.gms.maps.model.LatLng center, @android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination, @android.support.annotation.NonNull java.lang.Runnable callback) {
        if (!isValidSetOfCoordinates(center)) {
            logger.v("Trying to reverse geocode a location with invalid coordinates");
            callback.run();
        }
        new com.navdy.client.app.framework.map.MapUtils.Anon1(center, destination, callback).execute(new java.lang.Object[0]);
    }

    public static com.google.android.gms.maps.model.LatLng parseLatLng(java.lang.String string) {
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(string)) {
            return null;
        }
        java.lang.String str = string;
        try {
            str = java.net.URLDecoder.decode(string, java.nio.charset.Charset.defaultCharset().name());
        } catch (java.io.UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        java.util.regex.Matcher matcher = java.util.regex.Pattern.compile(com.navdy.client.app.framework.util.StringUtils.LAT_LNG_REGEXP).matcher(str);
        if (!matcher.matches() || matcher.groupCount() != 2) {
            logger.v("Can't parse lat/lng: " + str);
            return null;
        }
        try {
            float lat = java.lang.Float.parseFloat(matcher.group(1));
            float lng = java.lang.Float.parseFloat(matcher.group(2));
            if (lat == 0.0f && lng == 0.0f) {
                return null;
            }
            return new com.google.android.gms.maps.model.LatLng((double) lat, (double) lng);
        } catch (java.lang.NumberFormatException e2) {
            logger.v("Non-float lat/lng: " + str);
            return null;
        }
    }
}
