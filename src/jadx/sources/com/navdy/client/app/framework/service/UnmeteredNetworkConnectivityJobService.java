package com.navdy.client.app.framework.service;

public class UnmeteredNetworkConnectivityJobService extends android.app.job.JobService {
    public static final int UNMETERED_NETWORK_SERVICE = 101;
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.service.UnmeteredNetworkConnectivityJobService.class);

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.app.service.DataCollectionService.sendDataToS3IfOnWiFi();
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.client.app.service.DataCollectionService.getInstance().cancelAnyOngoingUpload();
        }
    }

    public boolean onStartJob(android.app.job.JobParameters jobParameters) {
        logger.d("onStartJob");
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.service.UnmeteredNetworkConnectivityJobService.Anon1(), 1);
        return false;
    }

    public boolean onStopJob(android.app.job.JobParameters jobParameters) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.service.UnmeteredNetworkConnectivityJobService.Anon2(), 1);
        boolean pendingSensorDataExist = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.PENDING_SENSOR_DATA_EXIST, false);
        logger.d("onStopJob - JobService will reschedule: " + pendingSensorDataExist);
        return pendingSensorDataExist;
    }

    public static void scheduleNetworkServiceForNougatOrAbove() {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
            android.app.job.JobScheduler jobScheduler = (android.app.job.JobScheduler) context.getSystemService(android.app.job.JobScheduler.class);
            if (jobScheduler == null || jobScheduler.getPendingJob(101) == null) {
                logger.d("Schedule network connectivity job service");
                android.app.job.JobInfo job = new android.app.job.JobInfo.Builder(101, new android.content.ComponentName(context, com.navdy.client.app.framework.service.UnmeteredNetworkConnectivityJobService.class)).setRequiredNetworkType(2).build();
                if (jobScheduler != null) {
                    jobScheduler.schedule(job);
                    return;
                }
                return;
            }
            logger.d("NetworkConnectivityJobService already pending");
        }
    }
}
