package com.navdy.client.app.framework;

public class FlashlightManager {
    private static com.navdy.client.app.framework.FlashlightManager instance = null;
    private android.hardware.Camera cam;
    private boolean flashlightIsOn = false;
    private boolean isCapableOfFlashlighting = false;
    private com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.FlashlightManager.class);
    private float previousScreenBrightness = 0.5f;

    private FlashlightManager() {
        android.content.pm.PackageManager packageManager = com.navdy.client.app.NavdyApplication.getAppContext().getPackageManager();
        if (packageManager != null) {
            this.isCapableOfFlashlighting = packageManager.hasSystemFeature("android.hardware.camera.flash");
        }
    }

    public static com.navdy.client.app.framework.FlashlightManager getInstance() {
        if (instance == null) {
            instance = new com.navdy.client.app.framework.FlashlightManager();
        }
        return instance;
    }

    public void switchLight(android.app.Activity activity) {
        if (this.flashlightIsOn) {
            turnFlashLightOff();
        } else {
            turnFlashLightOn(activity);
        }
    }

    public void turnFlashLightOn(android.app.Activity activity) {
        try {
            if (!this.flashlightIsOn) {
                if (!this.isCapableOfFlashlighting) {
                    throw new java.lang.Exception("Phone not capable of flashlighting");
                }
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    setTorchModeOn(true);
                } else {
                    this.cam = android.hardware.Camera.open();
                    android.hardware.Camera.Parameters p = this.cam.getParameters();
                    p.setFlashMode("torch");
                    this.cam.setParameters(p);
                    this.cam.startPreview();
                }
                this.flashlightIsOn = true;
            }
        } catch (java.lang.Exception e) {
            this.isCapableOfFlashlighting = false;
            this.logger.e("Unable to turn the flashlight on the normal way. Using the screen brightness instead. " + e.getMessage());
            startScreenLightActivity(activity);
        }
    }

    public void turnFlashLightOff() {
        try {
            if (this.flashlightIsOn) {
                if (!this.isCapableOfFlashlighting) {
                    throw new java.lang.Exception("Phone not capable of flashlighting");
                }
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    setTorchModeOn(false);
                } else {
                    this.cam.stopPreview();
                    this.cam.release();
                    this.cam = null;
                }
                this.flashlightIsOn = false;
            }
        } catch (java.lang.Exception e) {
            this.logger.e("Unable to turn the flashlight off the normal way. " + e.getMessage());
        }
    }

    @android.annotation.TargetApi(23)
    public void setTorchModeOn(boolean isOn) throws android.hardware.camera2.CameraAccessException {
        android.hardware.camera2.CameraManager manager = (android.hardware.camera2.CameraManager) com.navdy.client.app.NavdyApplication.getAppContext().getSystemService("camera");
        if (manager != null) {
            java.lang.String[] cameraList = manager.getCameraIdList();
            if (cameraList.length > 0) {
                manager.setTorchMode(cameraList[0], isOn);
            }
        }
    }

    public void startScreenLightActivity(android.app.Activity activity) {
        activity.startActivity(new android.content.Intent(activity.getApplicationContext(), com.navdy.client.app.ui.firstlaunch.ScreenLightActivity.class));
    }

    public void setScreenToFullBrightness(android.view.Window window) {
        android.view.WindowManager.LayoutParams layout = window.getAttributes();
        this.previousScreenBrightness = layout.screenBrightness;
        layout.screenBrightness = 1.0f;
        window.setAttributes(layout);
    }

    public void setScreenBackToNormalBrightness(android.view.Window window) {
        android.view.WindowManager.LayoutParams layout = window.getAttributes();
        layout.screenBrightness = this.previousScreenBrightness;
        window.setAttributes(layout);
    }
}
