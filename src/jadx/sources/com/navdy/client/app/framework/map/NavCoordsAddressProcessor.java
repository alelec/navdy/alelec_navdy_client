package com.navdy.client.app.framework.map;

public class NavCoordsAddressProcessor {
    private static final float MAX_DISTANCE_BETWEEN_COORDINATES_WITHOUT_STREET_NUMBER = 2000.0f;
    private static final float MAX_DISTANCE_BETWEEN_COORDINATES_WITH_STREET_NUMBER = 1000.0f;
    private static final boolean VERBOSE = false;
    private static final android.os.HandlerThread bgHandlerThread = new android.os.HandlerThread("BgHandlerThread");
    private static final android.os.Handler bgThreadHandler = new android.os.Handler(bgHandlerThread.getLooper());
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.class);
    private static final android.os.Handler uiThreadHandler = new android.os.Handler(android.os.Looper.getMainLooper());

    static class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback val$callback;
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;

        /* renamed from: com.navdy.client.app.framework.map.NavCoordsAddressProcessor$Anon1$Anon1 reason: collision with other inner class name */
        class C0043Anon1 implements com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener {
            C0043Anon1() {
            }

            public void onGoogleSearchResult(java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> destinations, com.navdy.client.app.framework.search.GooglePlacesSearch.Query queryType, com.navdy.client.app.framework.search.GooglePlacesSearch.Error error) {
                if (destinations != null && !destinations.isEmpty()) {
                    ((com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult) destinations.listIterator().next()).toModelDestinationObject(com.navdy.client.app.framework.models.Destination.SearchType.DETAILS, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon1.this.val$destination);
                }
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon1.this.doCoordAndAddressLookup();
            }
        }

        class Anon2 implements java.lang.Runnable {
            Anon2() {
            }

            public void run() {
                if (com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon1.this.val$callback != null) {
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon1.this.val$callback.onSuccess(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon1.this.val$destination);
                }
            }
        }

        Anon1(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback onCompleteCallback) {
            this.val$destination = destination;
            this.val$callback = onCompleteCallback;
        }

        public void run() {
            this.val$destination.reloadSelfFromDatabase();
            if (!this.val$destination.isPlaceDetailsInfoStale() || this.val$destination.placeId == null) {
                doCoordAndAddressLookup();
            } else {
                new com.navdy.client.app.framework.search.GooglePlacesSearch(new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon1.C0043Anon1(), com.navdy.client.app.framework.map.NavCoordsAddressProcessor.bgThreadHandler).runDetailsSearchWebApi(this.val$destination.placeId);
            }
        }

        private void doCoordAndAddressLookup() {
            com.google.android.gms.maps.model.LatLng latLng;
            if (this.val$destination.hasAlreadyBeenProcessed() || (this.val$destination.hasValidNavCoordinates() && this.val$destination.hasDetailedAddress())) {
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("destination has good data but it's not stored in cache or DB, saving and calling back with success ");
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.saveToCacheAndDb(this.val$destination, null, null, null, this.val$destination.precisionLevel, this.val$callback);
            } else if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.val$destination.rawAddressNotForDisplay)) {
                if (this.val$destination.hasValidDisplayCoordinates()) {
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("processDestination, destination does not have address, assigning display coordinates as address");
                    this.val$destination.setRawAddressNotForDisplay(this.val$destination.displayLat + "," + this.val$destination.displayLong);
                    latLng = new com.google.android.gms.maps.model.LatLng(this.val$destination.displayLat, this.val$destination.displayLong);
                } else if (this.val$destination.hasValidNavCoordinates()) {
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("processDestination, destination does not have address, assigning nav coordinates as address");
                    this.val$destination.setRawAddressNotForDisplay(this.val$destination.navigationLat + "," + this.val$destination.navigationLong);
                    latLng = new com.google.android.gms.maps.model.LatLng(this.val$destination.navigationLat, this.val$destination.navigationLong);
                } else {
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.w("processDestination, destination does not have address or coords, calling back with error");
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.callbackWithError(this.val$callback, this.val$destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error.INVALID_REQUEST);
                    return;
                }
                com.navdy.client.app.framework.map.MapUtils.doReverseGeocodingFor(latLng, this.val$destination, new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon1.Anon2());
            } else {
                if (!this.val$destination.isFavoriteDestination() && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.val$destination.name) && !com.navdy.client.app.framework.i18n.AddressUtils.isTitleIsInTheAddress(this.val$destination.name, this.val$destination.rawAddressNotForDisplay) && !this.val$destination.isCalendarEvent() && !this.val$destination.isContact()) {
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("destination is not favorite, trying adding name with address");
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.tryHerePlacesSearchNativeApiWithNameAndAddress(this.val$destination, this.val$callback);
                    return;
                }
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("destination is favorite/contact, trying just with address");
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.tryHerePlacesSearchNativeApiWithAddress(this.val$destination, this.val$callback);
            }
        }
    }

    static class Anon2 implements com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback {
        final /* synthetic */ com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback val$callback;
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$processedDestination;

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.here.android.mpa.common.GeoCoordinate val$displayCoords;
            final /* synthetic */ com.here.android.mpa.common.GeoCoordinate val$navigationCoords;
            final /* synthetic */ com.navdy.client.app.framework.models.Destination.Precision val$precision;
            final /* synthetic */ com.here.android.mpa.search.Address val$structuredAddress;

            Anon1(com.here.android.mpa.common.GeoCoordinate geoCoordinate, com.here.android.mpa.common.GeoCoordinate geoCoordinate2, com.here.android.mpa.search.Address address, com.navdy.client.app.framework.models.Destination.Precision precision) {
                this.val$navigationCoords = geoCoordinate;
                this.val$displayCoords = geoCoordinate2;
                this.val$structuredAddress = address;
                this.val$precision = precision;
            }

            public void run() {
                if (this.val$navigationCoords == null || com.navdy.client.app.framework.map.NavCoordsAddressProcessor.isInvalidOrExceedsThreshold(new com.google.android.gms.maps.model.LatLng(this.val$navigationCoords.getLatitude(), this.val$navigationCoords.getLongitude()), com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon2.this.val$processedDestination)) {
                    if (this.val$navigationCoords != null) {
                        com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("tryHerePlacesSearchNativeApiWithNameAndAddress returned bad navCoords: " + this.val$navigationCoords.getLatitude() + "," + this.val$navigationCoords.getLongitude() + "; trying just with address now");
                    } else {
                        com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("tryHerePlacesSearchNativeApiWithNameAndAddress returned null navCoords; trying just with address now");
                    }
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.tryHerePlacesSearchNativeApiWithAddress(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon2.this.val$processedDestination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon2.this.val$callback);
                    return;
                }
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("tryHerePlacesSearchNativeApiWithNameAndAddress returned good navCoords: " + this.val$navigationCoords.getLatitude() + "," + this.val$navigationCoords.getLongitude() + "; saving and calling back success");
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.saveToCacheAndDb(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon2.this.val$processedDestination, com.navdy.client.app.framework.map.MapUtils.buildNewCoordinate(this.val$displayCoords.getLatitude(), this.val$displayCoords.getLongitude()), com.navdy.client.app.framework.map.MapUtils.buildNewCoordinate(this.val$navigationCoords.getLatitude(), this.val$navigationCoords.getLongitude()), this.val$structuredAddress, this.val$precision, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon2.this.val$callback);
            }
        }

        /* renamed from: com.navdy.client.app.framework.map.NavCoordsAddressProcessor$Anon2$Anon2 reason: collision with other inner class name */
        class C0044Anon2 implements java.lang.Runnable {
            final /* synthetic */ com.here.android.mpa.search.ErrorCode val$error;

            C0044Anon2(com.here.android.mpa.search.ErrorCode errorCode) {
                this.val$error = errorCode;
            }

            public void run() {
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("tryHerePlacesSearchNativeApiWithNameAndAddress returned HERE error[" + this.val$error.name() + "];" + " trying just with address now");
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.tryHerePlacesSearchNativeApiWithAddress(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon2.this.val$processedDestination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon2.this.val$callback);
            }
        }

        Anon2(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback onCompleteCallback) {
            this.val$processedDestination = destination;
            this.val$callback = onCompleteCallback;
        }

        public void onComplete(@android.support.annotation.NonNull com.here.android.mpa.common.GeoCoordinate displayCoords, @android.support.annotation.Nullable com.here.android.mpa.common.GeoCoordinate navigationCoords, @android.support.annotation.Nullable com.here.android.mpa.search.Address structuredAddress, com.navdy.client.app.framework.models.Destination.Precision precision) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon2.Anon1(navigationCoords, displayCoords, structuredAddress, precision), 13);
        }

        public void onError(@android.support.annotation.NonNull com.here.android.mpa.search.ErrorCode error) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon2.C0044Anon2(error), 13);
        }
    }

    static class Anon3 implements com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback {
        final /* synthetic */ com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback val$callback;
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$processedDestination;

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.here.android.mpa.search.Address val$address;
            final /* synthetic */ com.here.android.mpa.common.GeoCoordinate val$displayCoords;
            final /* synthetic */ com.here.android.mpa.common.GeoCoordinate val$navigationCoords;
            final /* synthetic */ com.navdy.client.app.framework.models.Destination.Precision val$precision;

            Anon1(com.here.android.mpa.common.GeoCoordinate geoCoordinate, com.here.android.mpa.search.Address address, com.here.android.mpa.common.GeoCoordinate geoCoordinate2, com.navdy.client.app.framework.models.Destination.Precision precision) {
                this.val$navigationCoords = geoCoordinate;
                this.val$address = address;
                this.val$displayCoords = geoCoordinate2;
                this.val$precision = precision;
            }

            public void run() {
                if (this.val$navigationCoords == null || com.navdy.client.app.framework.map.NavCoordsAddressProcessor.isInvalidOrExceedsThreshold(new com.google.android.gms.maps.model.LatLng(this.val$navigationCoords.getLatitude(), this.val$navigationCoords.getLongitude()), com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon3.this.val$processedDestination)) {
                    if (this.val$navigationCoords != null) {
                        com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("tryHerePlacesSearchNativeApiWithAddress returned bad navCoords: " + this.val$navigationCoords.getLatitude() + "," + this.val$navigationCoords.getLongitude() + "; trying with Google now");
                    } else {
                        com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("tryHerePlacesSearchNativeApiWithAddress returned null navCoords; trying with Google now");
                    }
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.callGoogleDirectionsWebApi(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon3.this.val$processedDestination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon3.this.val$callback);
                    return;
                }
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("tryHerePlacesSearchNativeApiWithAddress returned good navCoords: " + this.val$navigationCoords.getLatitude() + "," + this.val$navigationCoords.getLongitude() + "; saving and calling back success. New address is: " + this.val$address);
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.saveToCacheAndDb(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon3.this.val$processedDestination, com.navdy.client.app.framework.map.MapUtils.buildNewCoordinate(this.val$displayCoords.getLatitude(), this.val$displayCoords.getLongitude()), com.navdy.client.app.framework.map.MapUtils.buildNewCoordinate(this.val$navigationCoords.getLatitude(), this.val$navigationCoords.getLongitude()), this.val$address, this.val$precision, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon3.this.val$callback);
            }
        }

        class Anon2 implements java.lang.Runnable {
            final /* synthetic */ com.here.android.mpa.search.ErrorCode val$error;

            Anon2(com.here.android.mpa.search.ErrorCode errorCode) {
                this.val$error = errorCode;
            }

            public void run() {
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("tryHerePlacesSearchNativeApiWithAddress returned HERE error[" + this.val$error.name() + "];" + " trying with Google now");
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.callGoogleDirectionsWebApi(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon3.this.val$processedDestination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon3.this.val$callback);
            }
        }

        Anon3(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback onCompleteCallback) {
            this.val$processedDestination = destination;
            this.val$callback = onCompleteCallback;
        }

        public void onComplete(@android.support.annotation.NonNull com.here.android.mpa.common.GeoCoordinate displayCoords, @android.support.annotation.Nullable com.here.android.mpa.common.GeoCoordinate navigationCoords, @android.support.annotation.Nullable com.here.android.mpa.search.Address address, com.navdy.client.app.framework.models.Destination.Precision precision) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon3.Anon1(navigationCoords, address, displayCoords, precision), 13);
        }

        public void onError(@android.support.annotation.NonNull com.here.android.mpa.search.ErrorCode error) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon3.Anon2(error), 13);
        }
    }

    static class Anon4 implements com.navdy.client.app.framework.map.NavCoordsAddressProcessor.NavigationCoordinatesRetrievalCallback {
        final /* synthetic */ com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback val$callback;
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$processedDestination;

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.here.android.mpa.search.Address val$address;
            final /* synthetic */ com.navdy.service.library.events.location.Coordinate val$displayCoords;
            final /* synthetic */ com.navdy.service.library.events.location.Coordinate val$navCoords;
            final /* synthetic */ com.navdy.client.app.framework.models.Destination.Precision val$precisionLevel;

            Anon1(com.navdy.service.library.events.location.Coordinate coordinate, com.navdy.service.library.events.location.Coordinate coordinate2, com.here.android.mpa.search.Address address, com.navdy.client.app.framework.models.Destination.Precision precision) {
                this.val$navCoords = coordinate;
                this.val$displayCoords = coordinate2;
                this.val$address = address;
                this.val$precisionLevel = precision;
            }

            public void run() {
                if (!com.navdy.client.app.framework.map.NavCoordsAddressProcessor.isInvalidOrExceedsThreshold(new com.google.android.gms.maps.model.LatLng(this.val$navCoords.latitude.doubleValue(), this.val$navCoords.longitude.doubleValue()), com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon4.this.val$processedDestination)) {
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("callGoogleDirectionsWebApi returned good navCoords: " + this.val$navCoords.latitude + "," + this.val$navCoords.longitude + "; saving and calling back success");
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.saveToCacheAndDb(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon4.this.val$processedDestination, this.val$displayCoords, this.val$navCoords, this.val$address, this.val$precisionLevel, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon4.this.val$callback);
                    return;
                }
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("callGoogleDirectionsWebApi returned bad navCoords: " + this.val$navCoords.latitude + "," + this.val$navCoords.longitude + "; can't do anything else, saving and calling back success");
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.saveToCacheAndDb(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon4.this.val$processedDestination, this.val$displayCoords, null, this.val$address, this.val$precisionLevel, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon4.this.val$callback);
            }
        }

        class Anon2 implements java.lang.Runnable {
            final /* synthetic */ java.lang.String val$error;

            Anon2(java.lang.String str) {
                this.val$error = str;
            }

            public void run() {
                if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.val$error, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error.SERVICE_ERROR.name())) {
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("callGoogleDirectionsWebApi returned Google service error; calling back failure");
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.callbackWithError(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon4.this.val$callback, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon4.this.val$processedDestination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error.BAD_COORDS);
                    return;
                }
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.v("callGoogleDirectionsWebApi returned Google error; can't do anything else, saving and calling back success");
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.saveToCacheAndDb(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon4.this.val$processedDestination, null, null, null, com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon4.this.val$callback);
            }
        }

        Anon4(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback onCompleteCallback) {
            this.val$processedDestination = destination;
            this.val$callback = onCompleteCallback;
        }

        public void onSuccess(@android.support.annotation.NonNull com.navdy.service.library.events.location.Coordinate displayCoords, @android.support.annotation.NonNull com.navdy.service.library.events.location.Coordinate navCoords, @android.support.annotation.Nullable com.here.android.mpa.search.Address address, com.navdy.client.app.framework.models.Destination.Precision precisionLevel) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon4.Anon1(navCoords, displayCoords, address, precisionLevel), 13);
        }

        public void onFailure(java.lang.String error) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon4.Anon2(error), 13);
        }
    }

    static class Anon5 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$HERE_GEOCODER_BASE_URL;
        final /* synthetic */ java.lang.String val$address;
        final /* synthetic */ com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback val$callback;

        Anon5(java.lang.String str, java.lang.String str2, com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback onGeocodeCompleteCallback) {
            this.val$HERE_GEOCODER_BASE_URL = str;
            this.val$address = str2;
            this.val$callback = onGeocodeCompleteCallback;
        }

        public void run() {
            java.io.InputStream inputStream = null;
            try {
                java.lang.String urlStr = this.val$HERE_GEOCODER_BASE_URL + java.net.URLEncoder.encode(this.val$address, "UTF-8");
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.d("Calling HERE with URL: https://geocoder.cit.api.here.com/6.2/geocode.json?app_id=HERE_GEO_APP_ID&app_code=HERE_GEO_APP_TOKEN&gen=9&searchtext=" + java.net.URLEncoder.encode(this.val$address, "UTF-8"));
                java.net.URL url = new java.net.URL(urlStr);
                javax.net.ssl.HttpsURLConnection urlConnection = (javax.net.ssl.HttpsURLConnection) url.openConnection();
                int httpResponse = urlConnection.getResponseCode();
                java.lang.Double dispLat = java.lang.Double.valueOf(0.0d);
                java.lang.Double dispLong = java.lang.Double.valueOf(0.0d);
                java.lang.Double navLat = java.lang.Double.valueOf(0.0d);
                java.lang.Double navLong = java.lang.Double.valueOf(0.0d);
                com.here.android.mpa.search.Address hereAddress = new com.here.android.mpa.search.Address();
                if (httpResponse < 200 || httpResponse >= 300) {
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.d("callWebHereGeocoderApi: response code:" + httpResponse);
                    this.val$callback.onError(com.here.android.mpa.search.ErrorCode.BAD_REQUEST);
                } else {
                    inputStream = urlConnection.getInputStream();
                    org.json.JSONObject jSONObject = new org.json.JSONObject(com.navdy.service.library.util.IOUtils.convertInputStreamToString(inputStream, "UTF-8"));
                    org.json.JSONObject location = ((org.json.JSONObject) jSONObject.getJSONObject("Response").getJSONArray("View").get(0)).getJSONArray("Result").getJSONObject(0).getJSONObject("Location");
                    org.json.JSONObject dispPos = location.getJSONObject("DisplayPosition");
                    java.lang.String dispLatStr = dispPos.optString("Latitude");
                    java.lang.String dispLonStr = dispPos.optString("Longitude");
                    dispLat = java.lang.Double.valueOf(dispLatStr);
                    dispLong = java.lang.Double.valueOf(dispLonStr);
                    org.json.JSONObject navPos = location.getJSONArray("NavigationPosition").getJSONObject(0);
                    java.lang.String navLatStr = navPos.optString("Latitude");
                    java.lang.String navLonStr = navPos.optString("Longitude");
                    navLat = java.lang.Double.valueOf(navLatStr);
                    navLong = java.lang.Double.valueOf(navLonStr);
                    org.json.JSONObject address = location.getJSONObject("Address");
                    hereAddress.setText(address.optString("Label"));
                    hereAddress.setCountryName(address.optString("Country"));
                    hereAddress.setState(address.optString("State"));
                    hereAddress.setCounty(address.optString("County"));
                    hereAddress.setCity(address.optString("City"));
                    hereAddress.setPostalCode(address.optString("PostalCode"));
                    org.json.JSONArray additionalData = address.optJSONArray("AdditionalData");
                    if (additionalData != null) {
                        for (int i = 0; i < additionalData.length(); i++) {
                            org.json.JSONObject data = additionalData.getJSONObject(i);
                            if (data.has("CountryName")) {
                                hereAddress.setCountryName(data.optString("CountryName"));
                            }
                            if (data.has("CountyName")) {
                                hereAddress.setCounty(data.optString("CountyName"));
                            }
                            if (data.has("StateName")) {
                                hereAddress.setState(data.optString("StateName"));
                            }
                        }
                    }
                }
                if (navLat.doubleValue() == 0.0d || navLong.doubleValue() == 0.0d) {
                    this.val$callback.onError(com.here.android.mpa.search.ErrorCode.BAD_LOCATION);
                } else {
                    com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback onGeocodeCompleteCallback = this.val$callback;
                    com.here.android.mpa.common.GeoCoordinate geoCoordinate = new com.here.android.mpa.common.GeoCoordinate(dispLat.doubleValue(), dispLong.doubleValue());
                    onGeocodeCompleteCallback.onComplete(geoCoordinate, new com.here.android.mpa.common.GeoCoordinate(navLat.doubleValue(), navLong.doubleValue()), hereAddress, com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN);
                }
            } catch (Throwable t) {
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.e("getNavigationCoordinate", t);
                this.val$callback.onError(com.here.android.mpa.search.ErrorCode.SERVER_INTERNAL);
            } finally {
                com.navdy.service.library.util.IOUtils.closeStream(inputStream);
            }
        }
    }

    static class Anon6 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$HERE_SEARCH_BASE_URL;
        final /* synthetic */ java.lang.String val$address;
        final /* synthetic */ com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback val$callback;
        final /* synthetic */ android.content.Context val$context;

        class Anon1 implements com.android.volley.Response.Listener<org.json.JSONObject> {
            final /* synthetic */ com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback val$callback;
            final /* synthetic */ com.here.android.mpa.search.Address val$hereAddress;
            final /* synthetic */ java.lang.Double val$navLat;
            final /* synthetic */ java.lang.Double val$navLong;

            Anon1(java.lang.Double d, java.lang.Double d2, com.here.android.mpa.search.Address address, com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback onGeocodeCompleteCallback) {
                this.val$navLat = d;
                this.val$navLong = d2;
                this.val$hereAddress = address;
                this.val$callback = onGeocodeCompleteCallback;
            }

            public void onResponse(org.json.JSONObject response) {
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon6.this.parseMetaDataResponse(response, this.val$navLat, this.val$navLong, this.val$hereAddress, this.val$callback);
            }
        }

        class Anon2 implements com.android.volley.Response.ErrorListener {
            final /* synthetic */ com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback val$callback;
            final /* synthetic */ com.here.android.mpa.search.Address val$hereAddress;
            final /* synthetic */ java.lang.Double val$navLat;
            final /* synthetic */ java.lang.Double val$navLong;

            Anon2(java.lang.Double d, java.lang.Double d2, com.here.android.mpa.search.Address address, com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback onGeocodeCompleteCallback) {
                this.val$navLat = d;
                this.val$navLong = d2;
                this.val$hereAddress = address;
                this.val$callback = onGeocodeCompleteCallback;
            }

            public void onErrorResponse(com.android.volley.VolleyError error) {
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.e("Error occured while trying to retrieve more info about a HERE places search result. ", error);
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon6.this.finalizeWebPlaceSearch(java.lang.Double.valueOf(0.0d), java.lang.Double.valueOf(0.0d), this.val$navLat, this.val$navLong, this.val$hereAddress, this.val$callback);
            }
        }

        Anon6(com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback onGeocodeCompleteCallback, java.lang.String str, java.lang.String str2, android.content.Context context) {
            this.val$callback = onGeocodeCompleteCallback;
            this.val$address = str;
            this.val$HERE_SEARCH_BASE_URL = str2;
            this.val$context = context;
        }

        public void run() {
            java.io.InputStream inputStream = null;
            try {
                com.navdy.service.library.events.location.Coordinate currentLocation = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance().getSmartStartCoordinates();
                if (currentLocation == null) {
                    this.val$callback.onError(com.here.android.mpa.search.ErrorCode.BAD_LOCATION);
                    return;
                }
                java.lang.String params = currentLocation.latitude + "," + currentLocation.longitude + "&q=" + java.net.URLEncoder.encode(this.val$address, "UTF-8");
                java.lang.String urlStr = this.val$HERE_SEARCH_BASE_URL + params;
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.d("Calling HERE with URL: https://places.cit.api.here.com/places/v1/discover/search?app_id=HERE_GEO_APP_ID&app_code=HERE_GEO_APP_TOKEN&at=" + params);
                java.net.URL url = new java.net.URL(urlStr);
                javax.net.ssl.HttpsURLConnection urlConnection = (javax.net.ssl.HttpsURLConnection) url.openConnection();
                int httpResponse = urlConnection.getResponseCode();
                com.here.android.mpa.search.Address hereAddress = new com.here.android.mpa.search.Address();
                if (httpResponse < 200 || httpResponse >= 300) {
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.d("callWebHereGeocoderApi: response code:" + httpResponse);
                    this.val$callback.onError(com.here.android.mpa.search.ErrorCode.BAD_REQUEST);
                } else {
                    inputStream = urlConnection.getInputStream();
                    org.json.JSONObject jSONObject = new org.json.JSONObject(com.navdy.service.library.util.IOUtils.convertInputStreamToString(inputStream, "UTF-8"));
                    org.json.JSONObject item = jSONObject.getJSONObject("results").getJSONArray("items").getJSONObject(0);
                    android.util.Pair<java.lang.Double, java.lang.Double> navCoords = parseLocationJson(item);
                    java.lang.Double navLat = (java.lang.Double) navCoords.first;
                    java.lang.Double navLong = (java.lang.Double) navCoords.second;
                    java.lang.String title = item.optString("title");
                    hereAddress.setText(title + item.optString("vicinity"));
                    java.lang.String metaDataUrl = item.optString("href");
                    if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(metaDataUrl)) {
                        fetchMetaData(metaDataUrl, navLat, navLong, hereAddress, this.val$callback);
                    } else {
                        finalizeWebPlaceSearch(java.lang.Double.valueOf(0.0d), java.lang.Double.valueOf(0.0d), navLat, navLong, hereAddress, this.val$callback);
                    }
                }
                com.navdy.service.library.util.IOUtils.closeStream(inputStream);
            } catch (Throwable t) {
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.e("getNavigationCoordinate", t);
                this.val$callback.onError(com.here.android.mpa.search.ErrorCode.SERVER_INTERNAL);
            } finally {
                com.navdy.service.library.util.IOUtils.closeStream(inputStream);
            }
        }

        private android.util.Pair<java.lang.Double, java.lang.Double> parseLocationJson(org.json.JSONObject item) throws org.json.JSONException {
            org.json.JSONArray position = item.getJSONArray("position");
            return new android.util.Pair<>(java.lang.Double.valueOf(position.get(0).toString()), java.lang.Double.valueOf(position.get(1).toString()));
        }

        private com.here.android.mpa.search.Address parseAddressJson(org.json.JSONObject location) throws org.json.JSONException {
            com.here.android.mpa.search.Address hereAddress = new com.here.android.mpa.search.Address();
            org.json.JSONObject address = location.getJSONObject(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_ADDRESS);
            hereAddress.setText(address.optString(com.navdy.client.app.framework.glances.GlanceConstants.CAR_EXT_CONVERSATION_MESSAGES_TEXT));
            hereAddress.setHouseNumber(address.optString("house"));
            hereAddress.setStreet(address.optString("street"));
            hereAddress.setCity(address.optString(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CITY));
            hereAddress.setState(address.optString("stateCode"));
            hereAddress.setPostalCode(address.optString("postalCode"));
            hereAddress.setCounty(address.optString(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_COUNTRY));
            return hereAddress;
        }

        private void fetchMetaData(java.lang.String metaDataUrl, java.lang.Double navLat, java.lang.Double navLong, com.here.android.mpa.search.Address hereAddress, com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback callback) {
            if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(metaDataUrl)) {
                finalizeWebPlaceSearch(java.lang.Double.valueOf(0.0d), java.lang.Double.valueOf(0.0d), navLat, navLong, hereAddress, callback);
                return;
            }
            com.android.volley.toolbox.JsonObjectRequest jsonObjectRequest = new com.android.volley.toolbox.JsonObjectRequest(0, metaDataUrl, null, new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon6.Anon1(navLat, navLong, hereAddress, callback), new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon6.Anon2(navLat, navLong, hereAddress, callback));
            com.android.volley.toolbox.Volley.newRequestQueue(this.val$context).add(jsonObjectRequest);
        }

        private void parseMetaDataResponse(org.json.JSONObject response, java.lang.Double navLat, java.lang.Double navLong, com.here.android.mpa.search.Address hereAddress, com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback callback) {
            try {
                org.json.JSONObject location = response.getJSONObject("location");
                android.util.Pair<java.lang.Double, java.lang.Double> navCoords = parseLocationJson(location);
                navLat = (java.lang.Double) navCoords.first;
                navLong = (java.lang.Double) navCoords.second;
                hereAddress = parseAddressJson(location);
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.d("Here returned the result with URL: " + response.optString("view"));
                finalizeWebPlaceSearch(java.lang.Double.valueOf(0.0d), java.lang.Double.valueOf(0.0d), navLat, navLong, hereAddress, callback);
            } catch (org.json.JSONException e) {
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.e("Json Error occured while trying to retrieve more info about a HERE places search result. ", e);
                finalizeWebPlaceSearch(java.lang.Double.valueOf(0.0d), java.lang.Double.valueOf(0.0d), navLat, navLong, hereAddress, callback);
            } catch (Throwable th) {
                java.lang.Throwable th2 = th;
                finalizeWebPlaceSearch(java.lang.Double.valueOf(0.0d), java.lang.Double.valueOf(0.0d), navLat, navLong, hereAddress, callback);
                throw th2;
            }
        }

        private void finalizeWebPlaceSearch(java.lang.Double dispLat, java.lang.Double dispLong, java.lang.Double navLat, java.lang.Double navLong, com.here.android.mpa.search.Address hereAddress, com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback callback) {
            if (navLat.doubleValue() == 0.0d || navLong.doubleValue() == 0.0d) {
                callback.onError(com.here.android.mpa.search.ErrorCode.BAD_LOCATION);
            } else {
                callback.onComplete(new com.here.android.mpa.common.GeoCoordinate(dispLat.doubleValue(), dispLong.doubleValue()), new com.here.android.mpa.common.GeoCoordinate(navLat.doubleValue(), navLong.doubleValue()), hereAddress, com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN);
            }
        }
    }

    static class Anon7 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback val$callback;
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;

        Anon7(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback onCompleteCallback, com.navdy.client.app.framework.models.Destination destination) {
            this.val$callback = onCompleteCallback;
            this.val$destination = destination;
        }

        public void run() {
            if (this.val$callback != null) {
                this.val$callback.onSuccess(this.val$destination);
            }
        }
    }

    static class Anon8 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback val$callback;
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;
        final /* synthetic */ com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error val$error;

        Anon8(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback onCompleteCallback, com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error error) {
            this.val$callback = onCompleteCallback;
            this.val$destination = destination;
            this.val$error = error;
        }

        public void run() {
            if (this.val$callback != null) {
                this.val$callback.onFailure(this.val$destination, this.val$error);
            }
        }
    }

    static class Anon9 implements com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener {
        final /* synthetic */ com.navdy.client.app.framework.map.NavCoordsAddressProcessor.NavigationCoordinatesRetrievalCallback val$callback;
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ java.util.List val$destinations;
            final /* synthetic */ com.navdy.client.app.framework.search.GooglePlacesSearch.Error val$error;

            /* renamed from: com.navdy.client.app.framework.map.NavCoordsAddressProcessor$Anon9$Anon1$Anon1 reason: collision with other inner class name */
            class C0045Anon1 implements java.lang.Runnable {
                final /* synthetic */ com.navdy.client.app.framework.models.Destination.Precision val$finalPrecision;
                final /* synthetic */ java.lang.String val$lat;
                final /* synthetic */ java.lang.String val$lng;

                C0045Anon1(java.lang.String str, java.lang.String str2, com.navdy.client.app.framework.models.Destination.Precision precision) {
                    this.val$lat = str;
                    this.val$lng = str2;
                    this.val$finalPrecision = precision;
                }

                public void run() {
                    com.navdy.service.library.events.location.Coordinate latLngCoord = com.navdy.client.app.framework.map.MapUtils.buildNewCoordinate(java.lang.Double.parseDouble(this.val$lat), java.lang.Double.parseDouble(this.val$lng));
                    if (com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon9.Anon1.this.val$error != com.navdy.client.app.framework.search.GooglePlacesSearch.Error.NONE) {
                        com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon9.this.val$callback.onFailure(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon9.Anon1.this.val$error.name());
                    } else {
                        com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon9.this.val$callback.onSuccess(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon9.this.val$destination.getDisplayCoordinate(), latLngCoord, null, this.val$finalPrecision);
                    }
                }
            }

            class Anon2 implements java.lang.Runnable {
                Anon2() {
                }

                public void run() {
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon9.this.val$callback.onFailure(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error.BAD_COORDS.name());
                }
            }

            Anon1(java.util.List list, com.navdy.client.app.framework.search.GooglePlacesSearch.Error error) {
                this.val$destinations = list;
                this.val$error = error;
            }

            public void run() {
                boolean foundValidNavCoordinates = false;
                if (this.val$destinations != null && this.val$destinations.size() > 0) {
                    for (int i = 0; i < this.val$destinations.size() && !foundValidNavCoordinates; i++) {
                        com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult result = (com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult) this.val$destinations.get(i);
                        if (result != null) {
                            java.lang.String lat = result.lat;
                            java.lang.String lng = result.lng;
                            com.navdy.client.app.framework.models.Destination.Precision precision = com.navdy.client.app.framework.models.Destination.Precision.PRECISE;
                            if (result.types != null) {
                                java.lang.String[] strArr = result.types;
                                int length = strArr.length;
                                int i2 = 0;
                                while (true) {
                                    if (i2 >= length) {
                                        break;
                                    }
                                    java.lang.String type = strArr[i2];
                                    if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type, "locality") || com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type, "postal_code") || com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_COUNTRY) || com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type, "administrative_area_level_1") || com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type, "neighborhood") || com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type, "route")) {
                                        precision = com.navdy.client.app.framework.models.Destination.Precision.IMPRECISE;
                                    } else {
                                        i2++;
                                    }
                                }
                                precision = com.navdy.client.app.framework.models.Destination.Precision.IMPRECISE;
                            }
                            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(lat) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(lng)) {
                                foundValidNavCoordinates = true;
                                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.uiThreadHandler.post(new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon9.Anon1.C0045Anon1(lat, lng, precision));
                            }
                        }
                    }
                }
                if (!foundValidNavCoordinates) {
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.e("Google Directions API failed.");
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.logger.e("Could not find valid coordinates out of: " + this.val$destinations);
                    com.navdy.client.app.framework.map.NavCoordsAddressProcessor.uiThreadHandler.post(new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon9.Anon1.Anon2());
                }
            }
        }

        Anon9(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.NavigationCoordinatesRetrievalCallback navigationCoordinatesRetrievalCallback, com.navdy.client.app.framework.models.Destination destination) {
            this.val$callback = navigationCoordinatesRetrievalCallback;
            this.val$destination = destination;
        }

        public void onGoogleSearchResult(java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> destinations, com.navdy.client.app.framework.search.GooglePlacesSearch.Query queryType, com.navdy.client.app.framework.search.GooglePlacesSearch.Error error) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon9.Anon1(destinations, error), 13);
        }
    }

    public enum Error {
        NONE,
        INVALID_REQUEST,
        BAD_COORDS,
        SERVICE_ERROR,
        NO_INTERNET,
        CACHE_NO_COORDS
    }

    public interface NavigationCoordinatesRetrievalCallback {
        void onFailure(java.lang.String str);

        void onSuccess(@android.support.annotation.NonNull com.navdy.service.library.events.location.Coordinate coordinate, @android.support.annotation.NonNull com.navdy.service.library.events.location.Coordinate coordinate2, @android.support.annotation.Nullable com.here.android.mpa.search.Address address, com.navdy.client.app.framework.models.Destination.Precision precision);
    }

    public interface OnCompleteCallback {
        void onFailure(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error error);

        void onSuccess(com.navdy.client.app.framework.models.Destination destination);
    }

    static {
        bgHandlerThread.start();
    }

    public static void processDestination(@android.support.annotation.Nullable com.navdy.client.app.framework.models.Destination destination, @android.support.annotation.Nullable com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback callback) {
        if (destination == null) {
            logger.w("processDestination, destination is null, calling back with error");
            callbackWithError(callback, null, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error.INVALID_REQUEST);
            return;
        }
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon1(destination, callback), 13);
    }

    private static void tryHerePlacesSearchNativeApiWithNameAndAddress(com.navdy.client.app.framework.models.Destination processedDestination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback callback) {
        java.lang.String query = com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.name_plus_address, new java.lang.Object[]{processedDestination.name, processedDestination.rawAddressNotForDisplay});
        logger.v("tryHerePlacesSearchNativeApiWithNameAndAddress, query is: " + query);
        com.navdy.client.app.framework.map.HereGeocoder.makeRequest(query, new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon2(processedDestination, callback));
    }

    private static void tryHerePlacesSearchNativeApiWithAddress(com.navdy.client.app.framework.models.Destination processedDestination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback callback) {
        com.navdy.client.app.framework.map.HereGeocoder.makeRequest(processedDestination.rawAddressNotForDisplay, new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon3(processedDestination, callback));
    }

    private static void callGoogleDirectionsWebApi(com.navdy.client.app.framework.models.Destination processedDestination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback callback) {
        getCoordinatesFromGoogleDirectionsWebApi(processedDestination, new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon4(processedDestination, callback));
    }

    public static void callWebHereGeocoderApi(java.lang.String address, com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback callback) {
        java.lang.String str = "https://geocoder.cit.api.here.com/6.2/geocode.json?app_id=";
        java.lang.String str2 = "&app_code=";
        java.lang.String str3 = "&gen=9&searchtext=";
        java.lang.String str4 = "Response";
        java.lang.String str5 = "View";
        java.lang.String str6 = "Result";
        java.lang.String str7 = "Location";
        java.lang.String str8 = "DisplayPosition";
        java.lang.String str9 = "NavigationPosition";
        java.lang.String str10 = "Latitude";
        java.lang.String str11 = "Longitude";
        java.lang.String str12 = "Address";
        java.lang.String str13 = "Label";
        java.lang.String str14 = "Country";
        java.lang.String str15 = "State";
        java.lang.String str16 = "County";
        java.lang.String str17 = "City";
        java.lang.String str18 = "PostalCode";
        java.lang.String str19 = "AdditionalData";
        java.lang.String str20 = "CountryName";
        java.lang.String str21 = "CountyName";
        java.lang.String str22 = "StateName";
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        java.lang.String HERE_GEO_APP_ID = com.navdy.client.app.framework.util.CredentialsUtils.getCredentials(context.getString(com.navdy.client.R.string.metadata_here_geo_app_id));
        java.lang.String HERE_GEO_APP_TOKEN = com.navdy.client.app.framework.util.CredentialsUtils.getCredentials(context.getString(com.navdy.client.R.string.metadata_here_geo_app_token));
        java.lang.String HERE_GEOCODER_BASE_URL = "https://geocoder.cit.api.here.com/6.2/geocode.json?app_id=" + HERE_GEO_APP_ID + "&app_code=" + HERE_GEO_APP_TOKEN + "&gen=9&searchtext=";
        java.lang.String str23 = "https://geocoder.cit.api.here.com/6.2/geocode.json?app_id=HERE_GEO_APP_ID&app_code=HERE_GEO_APP_TOKEN&gen=9&searchtext=";
        if (HERE_GEO_APP_ID == null || HERE_GEO_APP_TOKEN == null) {
            logger.e("no here token in manifest");
            callback.onError(com.here.android.mpa.search.ErrorCode.INVALID_CREDENTIALS);
            return;
        }
        logger.v("using here geocoder to get nav position");
        com.navdy.service.library.task.TaskManager instance = com.navdy.service.library.task.TaskManager.getInstance();
        com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon5 anon5 = new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon5(HERE_GEOCODER_BASE_URL, address, callback);
        instance.execute(anon5, 3);
    }

    public static void callWebHerePlacesSearchApi(java.lang.String address, com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback callback) {
        java.lang.String str = "https://places.cit.api.here.com/places/v1/discover/search?app_id=";
        java.lang.String str2 = "&app_code=";
        java.lang.String str3 = "&at=";
        java.lang.String str4 = "&q=";
        java.lang.String str5 = "results";
        java.lang.String str6 = "items";
        java.lang.String str7 = "location";
        java.lang.String str8 = "position";
        java.lang.String str9 = "title";
        java.lang.String str10 = "vicinity";
        java.lang.String str11 = "href";
        java.lang.String str12 = com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_ADDRESS;
        java.lang.String str13 = com.navdy.client.app.framework.glances.GlanceConstants.CAR_EXT_CONVERSATION_MESSAGES_TEXT;
        java.lang.String str14 = "house";
        java.lang.String str15 = "street";
        java.lang.String str16 = com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CITY;
        java.lang.String str17 = "stateCode";
        java.lang.String str18 = com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_COUNTRY;
        java.lang.String str19 = "postalCode";
        java.lang.String str20 = "view";
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        java.lang.String HERE_GEO_APP_ID = com.navdy.client.app.framework.util.CredentialsUtils.getCredentials(context.getString(com.navdy.client.R.string.metadata_here_geo_app_id));
        java.lang.String HERE_GEO_APP_TOKEN = com.navdy.client.app.framework.util.CredentialsUtils.getCredentials(context.getString(com.navdy.client.R.string.metadata_here_geo_app_token));
        java.lang.String HERE_SEARCH_BASE_URL = "https://places.cit.api.here.com/places/v1/discover/search?app_id=" + HERE_GEO_APP_ID + "&app_code=" + HERE_GEO_APP_TOKEN + "&at=";
        java.lang.String str21 = "https://places.cit.api.here.com/places/v1/discover/search?app_id=HERE_GEO_APP_ID&app_code=HERE_GEO_APP_TOKEN&at=";
        if (HERE_GEO_APP_ID == null || HERE_GEO_APP_TOKEN == null) {
            logger.e("no here token in manifest");
            callback.onError(com.here.android.mpa.search.ErrorCode.INVALID_CREDENTIALS);
            return;
        }
        logger.v("using here geocoder to get nav position");
        com.navdy.service.library.task.TaskManager instance = com.navdy.service.library.task.TaskManager.getInstance();
        com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon6 anon6 = new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon6(callback, address, HERE_SEARCH_BASE_URL, context);
        instance.execute(anon6, 3);
    }

    private static void saveToCacheAndDb(@android.support.annotation.Nullable com.navdy.client.app.framework.models.Destination destination, @android.support.annotation.Nullable com.navdy.service.library.events.location.Coordinate displayCoords, @android.support.annotation.Nullable com.navdy.service.library.events.location.Coordinate navigationCoords, @android.support.annotation.Nullable com.here.android.mpa.search.Address address, @android.support.annotation.Nullable com.navdy.client.app.framework.models.Destination.Precision precision, @android.support.annotation.Nullable com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback callback) {
        if (destination == null) {
            logger.w("saveToCacheAndDb, destination is null, no-op");
            callbackWithError(callback, null, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error.INVALID_REQUEST);
        } else if (!com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet()) {
            logger.v("saveToCacheAndDb, no internet so not saving to cache, no-op");
            callbackWithError(callback, destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error.SERVICE_ERROR);
        } else {
            double displayLat = 0.0d;
            double displayLng = 0.0d;
            if (com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(displayCoords)) {
                displayLat = displayCoords.latitude.doubleValue();
                displayLng = displayCoords.longitude.doubleValue();
            }
            double navigationLat = 0.0d;
            double navigationLng = 0.0d;
            if (com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(navigationCoords)) {
                navigationLat = navigationCoords.latitude.doubleValue();
                navigationLng = navigationCoords.longitude.doubleValue();
            }
            java.lang.String originalAddress = destination.rawAddressNotForDisplay;
            destination.handleNewCoordsAndAddress(displayLat, displayLng, navigationLat, navigationLng, address, precision);
            com.navdy.client.app.providers.NavdyContentProvider.addToCacheIfNotAlreadyIn(originalAddress, destination);
            callbackWithSuccess(callback, destination);
        }
    }

    private static void callbackWithSuccess(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback callback, com.navdy.client.app.framework.models.Destination destination) {
        uiThreadHandler.post(new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon7(callback, destination));
    }

    private static void callbackWithError(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback callback, com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error error) {
        uiThreadHandler.post(new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon8(callback, destination, error));
    }

    public static void getCoordinatesFromGoogleDirectionsWebApi(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.NavigationCoordinatesRetrievalCallback callback) {
        if (!com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet()) {
            callback.onFailure(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error.NO_INTERNET.name());
        }
        new com.navdy.client.app.framework.search.GooglePlacesSearch(new com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Anon9(callback, destination)).runDirectionsSearchWebApi(destination);
    }

    private static boolean isInvalidOrExceedsThreshold(com.google.android.gms.maps.model.LatLng latLng, com.navdy.client.app.framework.models.Destination destination) {
        if (!com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(latLng)) {
            return true;
        }
        if (!destination.hasValidDisplayCoordinates()) {
            return false;
        }
        float[] results = new float[1];
        android.location.Location.distanceBetween(destination.displayLat, destination.displayLong, latLng.latitude, latLng.longitude, results);
        if (((double) results[0]) <= getDistanceThresholdFor(destination)) {
            return false;
        }
        logger.v("Max Distance exceeded between display and navigation coordinates. Nav coords are: " + latLng + " Display coords are: " + destination.displayLat + org.droidparts.contract.SQL.DDL.SEPARATOR + destination.displayLong);
        return true;
    }

    public static double getDistanceThresholdFor(com.navdy.client.app.framework.models.Destination destination) {
        float coordinateThreshold = MAX_DISTANCE_BETWEEN_COORDINATES_WITHOUT_STREET_NUMBER;
        if (destination.hasStreetNumber()) {
            coordinateThreshold = MAX_DISTANCE_BETWEEN_COORDINATES_WITH_STREET_NUMBER;
        }
        return (double) coordinateThreshold;
    }
}
