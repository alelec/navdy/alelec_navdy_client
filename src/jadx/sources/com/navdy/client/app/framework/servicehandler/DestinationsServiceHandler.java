package com.navdy.client.app.framework.servicehandler;

public class DestinationsServiceHandler {
    private static final boolean VERBOSE = false;
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.class);
    private static com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler singleton = null;

    static class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType val$destinationType;

        Anon1(com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType destinationType) {
            this.val$destinationType = destinationType;
        }

        public void run() {
            switch (com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.Anon2.$SwitchMap$com$navdy$client$app$framework$servicehandler$DestinationsServiceHandler$DestinationType[this.val$destinationType.ordinal()]) {
                case 1:
                    com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.sendFavoritesToDisplay();
                    return;
                case 2:
                    com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.sendSuggestionsToDisplay();
                    return;
                default:
                    return;
            }
        }
    }

    static /* synthetic */ class Anon2 {
        static final /* synthetic */ int[] $SwitchMap$com$navdy$client$app$framework$servicehandler$DestinationsServiceHandler$DestinationType = new int[com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType.values().length];

        static {
            try {
                $SwitchMap$com$navdy$client$app$framework$servicehandler$DestinationsServiceHandler$DestinationType[com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType.FAVORITES.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$navdy$client$app$framework$servicehandler$DestinationsServiceHandler$DestinationType[com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType.SUGGESTIONS.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
        }
    }

    public enum DestinationType {
        SUGGESTIONS,
        FAVORITES
    }

    private DestinationsServiceHandler() {
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    public static synchronized com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler getInstance() {
        com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler destinationsServiceHandler;
        synchronized (com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.class) {
            if (singleton == null) {
                singleton = new com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler();
            }
            destinationsServiceHandler = singleton;
        }
        return destinationsServiceHandler;
    }

    @com.squareup.otto.Subscribe
    public void onFavoriteDestinationsRequest(com.navdy.service.library.events.FavoriteDestinationsRequest favoriteDestinationsRequest) {
        logger.d("Request for Favorite Destinations received.");
        long favoritesVersion = com.navdy.client.app.framework.util.VersioningUtils.getCurrentVersion(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType.FAVORITES);
        if (((java.lang.Long) com.squareup.wire.Wire.get(favoriteDestinationsRequest.serial_number, java.lang.Long.valueOf(-1))).longValue() == favoritesVersion) {
            logger.v("Favorites Version up to date");
            sendRemoteMessage(new com.navdy.service.library.events.places.FavoriteDestinationsUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_VERSION_IS_CURRENT, null, java.lang.Long.valueOf(favoritesVersion), null));
            return;
        }
        sendDestinationsToDisplay(com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType.FAVORITES);
    }

    @com.squareup.otto.Subscribe
    public void onRecommendedDestinationsRequest(com.navdy.service.library.events.destination.RecommendedDestinationsRequest recommendedDestinationsRequest) {
        logger.d("Request for Recommended Destinations Received.");
        sendDestinationsToDisplay(com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType.SUGGESTIONS);
    }

    public static void sendDestinationsToDisplay(com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType destinationType) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.Anon1(destinationType), 1);
    }

    private static synchronized void sendFavoritesToDisplay() {
        synchronized (com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.class) {
            android.database.Cursor cursor = null;
            try {
                long favoritesVersion = com.navdy.client.app.framework.util.VersioningUtils.getCurrentVersion(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType.FAVORITES);
                cursor = com.navdy.client.app.providers.NavdyContentProvider.getFavoritesCursor();
                if (cursor == null) {
                    sendRemoteMessage(new com.navdy.service.library.events.places.FavoriteDestinationsUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, null, java.lang.Long.valueOf(favoritesVersion), new java.util.ArrayList()));
                    logger.v("closing cursor");
                    com.navdy.service.library.util.IOUtils.closeStream(cursor);
                } else {
                    int cursorCount = cursor.getCount();
                    java.util.ArrayList<com.navdy.service.library.events.destination.Destination> protobufDestinations = new java.util.ArrayList<>(cursorCount);
                    for (int i = 0; i < cursorCount; i++) {
                        com.navdy.client.app.framework.models.Destination destination = com.navdy.client.app.providers.NavdyContentProvider.getDestinationItemAt(cursor, i);
                        if (destination != null && destination.hasOneValidSetOfCoordinates()) {
                            protobufDestinations.add(destination.toProtobufDestinationObject());
                        }
                    }
                    if (protobufDestinations.size() > 0) {
                    }
                    sendRemoteMessage(new com.navdy.service.library.events.places.FavoriteDestinationsUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, null, java.lang.Long.valueOf(favoritesVersion), protobufDestinations));
                    logger.v("closing cursor");
                    com.navdy.service.library.util.IOUtils.closeStream(cursor);
                }
            } catch (Throwable th) {
                logger.v("closing cursor");
                com.navdy.service.library.util.IOUtils.closeStream(cursor);
                throw th;
            }
        }
    }

    private static void sendSuggestionsToDisplay() {
        com.navdy.client.app.framework.util.SuggestionManager.buildSuggestionList(true);
    }

    public static synchronized void sendSuggestionsToDisplay(long version, java.util.List<com.navdy.client.app.framework.models.Suggestion> suggestions) {
        synchronized (com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.class) {
            if (suggestions != null) {
                if (suggestions.size() > 0) {
                    logger.i("sendSuggestionsToDisplay sending list of size: " + suggestions.size());
                    java.util.ArrayList<com.navdy.service.library.events.destination.Destination> protobufDestinations = new java.util.ArrayList<>(suggestions.size());
                    for (com.navdy.client.app.framework.models.Suggestion suggestion : suggestions) {
                        if (!(suggestion == null || suggestion.destination == null || !suggestion.shouldSendToHud())) {
                            protobufDestinations.add(suggestion.toProtobufDestinationObject());
                        }
                    }
                    finalizeSuggestionsSendToNavdy(version, protobufDestinations);
                }
            }
            logger.i("sendSuggestionsToDisplay list is empty");
            sendRemoteMessage(new com.navdy.service.library.events.destination.RecommendedDestinationsUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, null, java.lang.Long.valueOf(version), new java.util.ArrayList()));
        }
    }

    private static void finalizeSuggestionsSendToNavdy(long version, java.util.List<com.navdy.service.library.events.destination.Destination> protobufDestinations) {
        logger.i("finalizeSuggestionsSendToNavdy sending list of size: " + protobufDestinations.size());
        if (protobufDestinations.size() > 0) {
            sendRemoteMessage(new com.navdy.service.library.events.destination.RecommendedDestinationsUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, null, java.lang.Long.valueOf(version), protobufDestinations));
        } else {
            sendRemoteMessage(new com.navdy.service.library.events.destination.RecommendedDestinationsUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, "No suggestions to send", java.lang.Long.valueOf(0), null));
        }
    }

    private static void sendRemoteMessage(com.squareup.wire.Message message) {
        if (!com.navdy.client.app.framework.DeviceConnection.postEvent(message)) {
            logger.e("Unable to send message: " + message);
        }
    }
}
