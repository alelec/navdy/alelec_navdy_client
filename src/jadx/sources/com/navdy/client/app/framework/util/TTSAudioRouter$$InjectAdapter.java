package com.navdy.client.app.framework.util;

public final class TTSAudioRouter$$InjectAdapter extends dagger.internal.Binding<com.navdy.client.app.framework.util.TTSAudioRouter> implements javax.inject.Provider<com.navdy.client.app.framework.util.TTSAudioRouter>, dagger.MembersInjector<com.navdy.client.app.framework.util.TTSAudioRouter> {
    private dagger.internal.Binding<android.content.SharedPreferences> preferences;

    public TTSAudioRouter$$InjectAdapter() {
        super("com.navdy.client.app.framework.util.TTSAudioRouter", "members/com.navdy.client.app.framework.util.TTSAudioRouter", false, com.navdy.client.app.framework.util.TTSAudioRouter.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.preferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.client.app.framework.util.TTSAudioRouter.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.preferences);
    }

    public com.navdy.client.app.framework.util.TTSAudioRouter get() {
        com.navdy.client.app.framework.util.TTSAudioRouter result = new com.navdy.client.app.framework.util.TTSAudioRouter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.client.app.framework.util.TTSAudioRouter object) {
        object.preferences = (android.content.SharedPreferences) this.preferences.get();
    }
}
