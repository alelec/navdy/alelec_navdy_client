package com.navdy.client.app.framework.util;

public class SubmitTicketWorker {
    private static final java.lang.String ANDROID_BACKGROUND_SUBMISSION = "android_background_submission";
    private static final int INTERNET_ERROR_CODE = -1;
    private static final int MALFORMED_TICKET = 422;
    private static final long TEN_DAYS_IN_MILLIS = java.util.concurrent.TimeUnit.DAYS.toMillis(10);
    private static final boolean VERBOSE = true;
    private static final java.lang.String ZIP_MIME_TYPE = "application/zip";
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.SubmitTicketWorker.class);
    private com.navdy.client.app.framework.util.SubmitTicketWorker.OnFinishSubmittingTickets callback;
    private java.io.File displayLog;
    private int failCount = 0;
    private java.util.concurrent.atomic.AtomicBoolean isRunning = new java.util.concurrent.atomic.AtomicBoolean(false);
    private final com.zendesk.sdk.network.impl.ProviderStore providerStore = com.zendesk.sdk.network.impl.ZendeskConfig.INSTANCE.provider();
    private boolean showSuccessNotification;
    private int successCount = 0;
    private int ticketCount = 0;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ java.io.File val$ticketFolder;

        Anon1(java.io.File file) {
            this.val$ticketFolder = file;
        }

        public void run() {
            if (!this.val$ticketFolder.exists()) {
                com.navdy.client.app.framework.util.SubmitTicketWorker.this.incrementFailureCount();
                return;
            }
            com.navdy.client.app.framework.util.SubmitTicketWorker.logger.i("Now processing: " + this.val$ticketFolder.getName());
            try {
                if (java.lang.System.currentTimeMillis() - com.navdy.client.app.framework.util.SubmitTicketWorker.TEN_DAYS_IN_MILLIS >= this.val$ticketFolder.lastModified()) {
                    com.navdy.client.app.framework.util.SubmitTicketWorker.logger.v("deleting old folder: " + this.val$ticketFolder.getAbsolutePath());
                    com.navdy.service.library.util.IOUtils.deleteDirectory(com.navdy.client.app.NavdyApplication.getAppContext(), this.val$ticketFolder);
                    com.navdy.client.app.framework.util.SubmitTicketWorker.this.incrementSuccessCount();
                    return;
                }
                java.io.File attachments = new java.io.File(this.val$ticketFolder.getAbsolutePath() + java.io.File.separator + "attachments.zip");
                com.navdy.client.app.framework.util.SubmitTicketWorker.logger.v("attachment filepath: " + this.val$ticketFolder.getAbsolutePath() + java.io.File.separator + "attachments.zip");
                java.io.File content = new java.io.File(this.val$ticketFolder.getAbsolutePath() + java.io.File.separator + com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_JSON_FILENAME);
                com.navdy.client.app.framework.util.SubmitTicketWorker.logger.v("content filepath: " + this.val$ticketFolder.getAbsolutePath() + java.io.File.separator + com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_JSON_FILENAME);
                org.json.JSONObject jsonObject = com.navdy.client.app.framework.util.SubmitTicketWorker.getJSONObjectFromFile(content);
                if (jsonObject == null) {
                    com.navdy.client.app.framework.util.SubmitTicketWorker.logger.d("No JSON Object found in the folder " + this.val$ticketFolder.getName());
                    com.navdy.client.app.framework.util.SubmitTicketWorker.this.incrementFailureCount();
                } else if (!jsonObject.optString(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_ACTION, "").equals(com.navdy.client.app.ui.settings.ZendeskConstants.ACTION_FETCH_HUD_LOG_UPLOAD_DELETE)) {
                    com.navdy.client.app.framework.util.SubmitTicketWorker.this.submitTicket(jsonObject, attachments, this.val$ticketFolder);
                } else if (com.navdy.client.app.framework.util.SubmitTicketWorker.this.displayLog == null || !com.navdy.client.app.framework.util.SubmitTicketWorker.this.displayLog.exists()) {
                    com.navdy.client.app.framework.util.SubmitTicketWorker.this.incrementSuccessCount();
                } else {
                    com.navdy.client.app.framework.util.SubmitTicketWorker.this.attachLogsAndUpdateTicketAction(jsonObject, new java.io.File[]{com.navdy.client.app.framework.util.SubmitTicketWorker.this.displayLog}, attachments);
                    com.navdy.client.app.framework.util.SubmitTicketWorker.this.submitTicket(jsonObject, attachments, this.val$ticketFolder);
                }
            } catch (java.lang.Exception e) {
                com.navdy.client.app.framework.util.SubmitTicketWorker.logger.e("Exception was found: " + e);
                com.navdy.client.app.framework.util.SubmitTicketWorker.this.incrementFailureCount();
            }
        }
    }

    class Anon2 extends com.zendesk.service.ZendeskCallback<com.zendesk.sdk.model.request.UploadResponse> {
        final /* synthetic */ java.io.File val$folder;
        final /* synthetic */ org.json.JSONObject val$ticketContents;

        Anon2(java.io.File file, org.json.JSONObject jSONObject) {
            this.val$folder = file;
            this.val$ticketContents = jSONObject;
        }

        public void onSuccess(com.zendesk.sdk.model.request.UploadResponse uploadResponse) {
            com.navdy.client.app.framework.util.SubmitTicketWorker.logger.v("file successfully uploaded");
            com.navdy.client.app.framework.util.SubmitTicketWorker.this.sendToZenddesk(this.val$folder, this.val$ticketContents, uploadResponse.getToken());
        }

        public void onError(com.zendesk.service.ErrorResponse errorResponse) {
            com.navdy.client.app.framework.util.SubmitTicketWorker.logger.v("file failed to upload: " + errorResponse.getReason());
            com.navdy.client.app.framework.util.SubmitTicketWorker.this.incrementFailureCount();
        }
    }

    class Anon3 extends com.zendesk.service.ZendeskCallback<com.zendesk.sdk.model.request.CreateRequest> {
        final /* synthetic */ android.content.Context val$appContext;
        final /* synthetic */ java.io.File val$folder;
        final /* synthetic */ org.json.JSONObject val$ticketContents;

        Anon3(org.json.JSONObject jSONObject, java.io.File file, android.content.Context context) {
            this.val$ticketContents = jSONObject;
            this.val$folder = file;
            this.val$appContext = context;
        }

        public void onSuccess(com.zendesk.sdk.model.request.CreateRequest createRequest) {
            com.navdy.client.app.framework.util.SubmitTicketWorker.logger.d("ticket request successful");
            com.navdy.client.app.framework.util.SubmitTicketWorker.this.showSuccessNotification = true;
            com.navdy.client.app.ui.base.BaseActivity.showShortToast(com.navdy.client.R.string.zendesk_submit_succeeded, new java.lang.Object[0]);
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.SUPPORT_TICKET_CREATED);
            try {
                if (this.val$ticketContents.optString(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_ACTION, "").equals(com.navdy.client.app.ui.settings.ZendeskConstants.ACTION_UPLOAD_AWAIT_HUD_LOG)) {
                    this.val$ticketContents.put(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_ACTION, com.navdy.client.app.ui.settings.ZendeskConstants.ACTION_FETCH_HUD_LOG_UPLOAD_DELETE);
                    com.navdy.client.app.framework.util.SupportTicketService.writeJsonObjectToFile(this.val$ticketContents, this.val$folder.getAbsolutePath(), com.navdy.client.app.framework.util.SubmitTicketWorker.logger);
                    com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.TICKETS_AWAITING_HUD_LOG_EXIST, true).apply();
                } else {
                    com.navdy.client.app.framework.util.SubmitTicketWorker.logger.d("deleting submitted ticket folder: " + this.val$folder.getAbsolutePath());
                    com.navdy.service.library.util.IOUtils.deleteDirectory(this.val$appContext, this.val$folder);
                }
            } catch (java.lang.Exception e) {
                com.navdy.client.app.framework.util.SubmitTicketWorker.logger.e("exception found trying to read JSON object: " + e);
            }
            com.navdy.client.app.framework.util.SubmitTicketWorker.this.incrementSuccessCount();
            com.navdy.client.app.framework.util.SupportTicketService.resetRetryCount();
        }

        public void onError(com.zendesk.service.ErrorResponse errorResponse) {
            com.navdy.client.app.framework.util.SubmitTicketWorker.logger.e("ticket request failed: " + errorResponse.getReason());
            switch (errorResponse.getStatus()) {
                case -1:
                    com.navdy.client.app.framework.util.SubmitTicketWorker.logger.e("Internet error");
                    break;
                case com.navdy.client.app.framework.util.SubmitTicketWorker.MALFORMED_TICKET /*422*/:
                    com.navdy.client.app.framework.util.SubmitTicketWorker.logger.e("Ticket is malformed");
                    break;
                default:
                    com.navdy.client.app.framework.util.SubmitTicketWorker.logger.e("Ticket request failed: " + errorResponse.getReason());
                    break;
            }
            com.navdy.client.app.framework.util.SubmitTicketWorker.this.incrementFailureCount();
        }
    }

    class Anon4 implements com.navdy.client.app.framework.util.SupportTicketService.OnLogCollectedInterface {
        final /* synthetic */ java.io.File[] val$folders;

        Anon4(java.io.File[] fileArr) {
            this.val$folders = fileArr;
        }

        public void onLogCollectionSucceeded(java.util.ArrayList<java.io.File> logAttachments) {
            com.navdy.client.app.framework.util.SubmitTicketWorker.logger.d("fetching hud log succeeded");
            if (!logAttachments.isEmpty()) {
                java.util.Iterator it = logAttachments.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    java.io.File log = (java.io.File) it.next();
                    if (log.getName().contains("display_log")) {
                        com.navdy.client.app.framework.util.SubmitTicketWorker.this.displayLog = log;
                        break;
                    }
                }
            }
            com.navdy.client.app.framework.util.SubmitTicketWorker.this.processAllTickets(this.val$folders);
        }

        public void onLogCollectionFailed() {
            com.navdy.client.app.framework.util.SubmitTicketWorker.logger.d("fetching hud log failed.");
            com.navdy.client.app.framework.util.SubmitTicketWorker.this.processAllTickets(this.val$folders);
        }
    }

    public interface OnFinishSubmittingTickets {
        void onFinish(int i, boolean z);
    }

    SubmitTicketWorker() {
    }

    public synchronized void start(com.navdy.client.app.framework.util.SubmitTicketWorker.OnFinishSubmittingTickets onFinishSubmittingTickets, java.io.File[] folders) {
        this.ticketCount = folders.length;
        this.callback = onFinishSubmittingTickets;
        if (folders.length > 0) {
            this.isRunning.set(true);
            if (!com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.TICKETS_AWAITING_HUD_LOG_EXIST, false) || !com.navdy.client.app.framework.AppInstance.getInstance().isDeviceConnected()) {
                processAllTickets(folders);
            } else {
                fetchHudLogAndProcessTickets(folders);
            }
        } else if (this.callback != null) {
            this.callback.onFinish(this.failCount, this.showSuccessNotification);
        }
    }

    private synchronized void processNextTicketFolder(java.io.File ticketFolder) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.util.SubmitTicketWorker.Anon1(ticketFolder), 9);
    }

    static org.json.JSONObject getJSONObjectFromFile(java.io.File file) {
        try {
            org.json.JSONObject jsonObject = new org.json.JSONObject(getStringFromFile(file));
            logger.v("jsonObject status: " + jsonObject);
            return jsonObject;
        } catch (java.lang.Exception e) {
            logger.e("getting the JSON Object from file failed: " + e);
            return null;
        }
    }

    private static java.lang.String getStringFromFile(java.io.File file) throws java.lang.Exception {
        try {
            java.io.FileInputStream fileInputStream = new java.io.FileInputStream(file);
            java.lang.String fileString = convertStreamToString(fileInputStream);
            fileInputStream.close();
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(fileString)) {
                return fileString;
            }
            logger.e("getStringFromFile failed");
            return null;
        } catch (java.lang.Exception e) {
            logger.e("Exception found: " + e);
            return null;
        }
    }

    private static java.lang.String convertStreamToString(java.io.InputStream inputStream) throws java.lang.Exception {
        try {
            java.io.BufferedReader bufferedReader = new java.io.BufferedReader(new java.io.InputStreamReader(inputStream));
            java.lang.StringBuilder stringBuilder = new java.lang.StringBuilder();
            while (true) {
                java.lang.String line = bufferedReader.readLine();
                if (line == null) {
                    break;
                }
                stringBuilder.append(line).append("\n");
            }
            bufferedReader.close();
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(stringBuilder)) {
                return stringBuilder.toString();
            }
            logger.e("convertStreamToString failed");
            return null;
        } catch (java.lang.Exception e) {
            logger.e("Exception found: " + e);
            return null;
        }
    }

    private synchronized void submitTicket(org.json.JSONObject ticketContents, java.io.File ticketAttachment, java.io.File folder) {
        logger.v("submitTicket content status: " + ticketContents + ", attachment status : " + ticketAttachment);
        if (ticketContents == null) {
            logger.e("Ticket contents are null. Deleting folder for invalid support request form:" + folder.getAbsolutePath());
            com.navdy.service.library.util.IOUtils.deleteDirectory(com.navdy.client.app.NavdyApplication.getAppContext(), folder);
            incrementFailureCount();
        } else {
            if (ticketAttachment != null) {
                if (ticketAttachment.length() > 0) {
                    logger.v("ticket attachment size: " + ticketAttachment.length());
                    this.providerStore.uploadProvider().uploadAttachment("attachments.zip", ticketAttachment, "application/zip", new com.navdy.client.app.framework.util.SubmitTicketWorker.Anon2(folder, ticketContents));
                }
            }
            sendToZenddesk(folder, ticketContents, null);
        }
    }

    private synchronized void sendToZenddesk(java.io.File folder, org.json.JSONObject ticketContents, java.lang.String attachmentToken) {
        logger.v("sendToZendesk");
        java.lang.String ticketType = ticketContents.optString(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_TYPE, "");
        java.lang.String ticketDescription = ticketContents.optString(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_DESCRIPTION, "");
        java.lang.String vin = ticketContents.optString(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_VIN, "");
        java.lang.String email = ticketContents.optString(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_EMAIL, "");
        logger.v("ticketDescription: " + ticketDescription + ", ticketType: " + ticketType);
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(ticketType) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(ticketDescription)) {
            logger.e("ticket contents were null.");
            incrementFailureCount();
        } else {
            java.util.ArrayList<com.zendesk.sdk.model.request.CustomField> customFields = new java.util.ArrayList<>();
            com.zendesk.sdk.model.request.CreateRequest request = new com.zendesk.sdk.model.request.CreateRequest();
            request.setDescription(ticketDescription);
            try {
                if (ticketContents.getString(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_ACTION).equals(com.navdy.client.app.ui.settings.ZendeskConstants.ACTION_FETCH_HUD_LOG_UPLOAD_DELETE)) {
                    logger.d("adding tag for android background submission");
                    request.setTags(java.util.Collections.singletonList(ANDROID_BACKGROUND_SUBMISSION));
                }
            } catch (java.lang.Exception e) {
                logger.e("JSON Exception: " + e);
            }
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(attachmentToken)) {
                java.util.ArrayList<java.lang.String> attachmentTokenList = new java.util.ArrayList<>();
                attachmentTokenList.add(attachmentToken);
                request.setAttachments(attachmentTokenList);
            }
            addCustomField(customFields, com.navdy.client.app.ui.settings.ZendeskConstants.PROBLEM_TYPES.longValue(), ticketType);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(email)) {
                request.setEmail(email);
            }
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(vin)) {
                addCustomField(customFields, com.navdy.client.app.ui.settings.ZendeskConstants.VIN.longValue(), vin);
            }
            android.content.Context appContext = com.navdy.client.app.NavdyApplication.getAppContext();
            addCustomFieldFromSharedPref(customFields, com.navdy.client.app.ui.settings.ZendeskConstants.VEHICLE_MAKE.longValue(), com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE);
            addCustomFieldFromSharedPref(customFields, com.navdy.client.app.ui.settings.ZendeskConstants.VEHICLE_MODEL.longValue(), com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL);
            addCustomFieldFromSharedPref(customFields, com.navdy.client.app.ui.settings.ZendeskConstants.VEHICLE_YEAR.longValue(), com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR);
            addCustomField(customFields, com.navdy.client.app.ui.settings.ZendeskConstants.MOBILE_APP_VERSION.longValue(), com.navdy.client.BuildConfig.VERSION_NAME);
            addCustomField(customFields, com.navdy.client.app.ui.settings.ZendeskConstants.MOBILE_APP_NAME.longValue(), appContext.getString(com.navdy.client.R.string.app_name));
            addCustomField(customFields, com.navdy.client.app.ui.settings.ZendeskConstants.MOBILE_DEVICE_MODEL.longValue(), android.os.Build.MODEL);
            addCustomField(customFields, com.navdy.client.app.ui.settings.ZendeskConstants.MOBILE_DEVICE_MAKE.longValue(), android.os.Build.MANUFACTURER);
            addCustomField(customFields, com.navdy.client.app.ui.settings.ZendeskConstants.MOBILE_OS_VERSION.longValue(), java.lang.String.valueOf(android.os.Build.VERSION.RELEASE));
            com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
            if (remoteDevice != null) {
                com.navdy.service.library.events.DeviceInfo info = remoteDevice.getDeviceInfo();
                if (info != null) {
                    addCustomField(customFields, com.navdy.client.app.ui.settings.ZendeskConstants.DISPLAY_SOFTWARE_VERSION.longValue(), info.systemVersion);
                    addCustomField(customFields, com.navdy.client.app.ui.settings.ZendeskConstants.DISPLAY_SERIAL_NUMBER.longValue(), info.deviceUuid);
                }
            }
            logger.i("setting custom fields: " + customFields);
            request.setCustomFields(customFields);
            com.zendesk.sdk.network.impl.ZendeskConfig.INSTANCE.setCustomFields(customFields);
            com.zendesk.sdk.network.RequestProvider requestProvider = this.providerStore.requestProvider();
            com.navdy.client.app.framework.util.SubmitTicketWorker.Anon3 anon3 = new com.navdy.client.app.framework.util.SubmitTicketWorker.Anon3(ticketContents, folder, appContext);
            requestProvider.createRequest(request, anon3);
        }
        return;
    }

    private void addCustomField(java.util.ArrayList<com.zendesk.sdk.model.request.CustomField> customFields, long field, java.lang.String value) {
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(value)) {
            customFields.add(new com.zendesk.sdk.model.request.CustomField(java.lang.Long.valueOf(field), value));
        }
    }

    private void addCustomFieldFromSharedPref(java.util.ArrayList<com.zendesk.sdk.model.request.CustomField> customFields, long field, java.lang.String sharedPrefKey) {
        java.lang.String value = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences().getString(sharedPrefKey, null);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(value)) {
            customFields.add(new com.zendesk.sdk.model.request.CustomField(java.lang.Long.valueOf(field), value));
        }
    }

    private synchronized void fetchHudLogAndProcessTickets(java.io.File[] folders) {
        logger.d("fetchHudLogAndProcessTickets");
        if (this.displayLog == null) {
            com.navdy.client.app.framework.util.SupportTicketService.collectHudLog(new com.navdy.client.app.framework.util.SubmitTicketWorker.Anon4(folders));
        } else {
            logger.d("hud log exists: " + this.displayLog);
            processAllTickets(folders);
        }
    }

    private void processAllTickets(java.io.File[] folders) {
        logger.d("processAllTickets");
        for (java.io.File folder : folders) {
            processNextTicketFolder(folder);
        }
    }

    private void attachLogsAndUpdateTicketAction(org.json.JSONObject ticketContents, java.io.File[] logs, java.io.File attachmentsZipFile) {
        logger.d("attachLogsAndUpdateTicketAction");
        com.navdy.service.library.util.IOUtils.compressFilesToZip(com.navdy.client.app.NavdyApplication.getAppContext(), logs, attachmentsZipFile.getAbsolutePath());
        try {
            ticketContents.put(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_HAS_DISPLAY_LOG, true);
            if (ticketContents.optString(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_ACTION, "").equals(com.navdy.client.app.ui.settings.ZendeskConstants.ACTION_UPLOAD_AWAIT_HUD_LOG)) {
                ticketContents.put(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_ACTION, com.navdy.client.app.ui.settings.ZendeskConstants.ACTION_UPLOAD_DELETE);
            }
        } catch (java.lang.Exception e) {
            logger.e("JSON Exception found: " + e);
        }
    }

    public synchronized boolean isRunning() {
        return this.isRunning.get();
    }

    private synchronized void incrementSuccessCount() {
        this.successCount++;
        logger.i("incrementSuccessCount: " + this.successCount);
        finishIfAllTicketsAttemptedSubmission();
    }

    private synchronized void incrementFailureCount() {
        this.failCount++;
        logger.i("incrementFailureCount: " + this.failCount);
        finishIfAllTicketsAttemptedSubmission();
    }

    private synchronized void finishIfAllTicketsAttemptedSubmission() {
        if (this.successCount + this.failCount == this.ticketCount) {
            this.isRunning.set(false);
            this.callback.onFinish(this.failCount, this.showSuccessNotification);
        }
    }
}
