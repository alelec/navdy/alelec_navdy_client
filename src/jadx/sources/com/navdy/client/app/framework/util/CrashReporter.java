package com.navdy.client.app.framework.util;

public final class CrashReporter {
    private static final com.navdy.client.app.framework.util.CrashReporter sInstance = new com.navdy.client.app.framework.util.CrashReporter();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.CrashReporter.class);
    private static volatile boolean stopCrashReporting;
    private boolean installed;

    class Anon1 implements java.lang.Thread.UncaughtExceptionHandler {
        final /* synthetic */ java.lang.Thread.UncaughtExceptionHandler val$crashlyticsHandler;

        Anon1(java.lang.Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
            this.val$crashlyticsHandler = uncaughtExceptionHandler;
        }

        public void uncaughtException(java.lang.Thread thread, java.lang.Throwable exception) {
            com.navdy.client.app.framework.util.CrashReporter.this.handleUncaughtException(thread, exception, this.val$crashlyticsHandler);
        }
    }

    public static com.navdy.client.app.framework.util.CrashReporter getInstance() {
        return sInstance;
    }

    private CrashReporter() {
    }

    public synchronized void installCrashHandler(android.content.Context context, java.lang.String userId) {
        if (!this.installed) {
            java.lang.Thread.UncaughtExceptionHandler defaultUncaughtHandler = java.lang.Thread.getDefaultUncaughtExceptionHandler();
            if (defaultUncaughtHandler != null) {
                sLogger.v("default uncaught handler:" + defaultUncaughtHandler.getClass().getName());
            } else {
                sLogger.v("default uncaught handler is null");
            }
            io.fabric.sdk.android.Fabric.with(new io.fabric.sdk.android.Fabric.Builder(context).kits(new com.crashlytics.android.Crashlytics(), new com.crashlytics.android.ndk.CrashlyticsNdk()).build());
            if (userId != null) {
                com.crashlytics.android.Crashlytics.setUserIdentifier(userId);
            }
            com.crashlytics.android.Crashlytics.setString("Serial", android.os.Build.SERIAL);
            sLogger.i("crashlytics installed with user:" + userId);
            java.lang.Thread.UncaughtExceptionHandler crashlyticsHandler = java.lang.Thread.getDefaultUncaughtExceptionHandler();
            if (crashlyticsHandler != null) {
                sLogger.v("crashlytics uncaught handler:" + crashlyticsHandler.getClass().getName());
            } else {
                sLogger.v("crashlytics uncaught handler is null");
            }
            java.lang.Thread.setDefaultUncaughtExceptionHandler(new com.navdy.client.app.framework.util.CrashReporter.Anon1(crashlyticsHandler));
            com.navdy.service.library.log.Logger.addAppender(new com.navdy.client.app.framework.util.CrashlyticsAppender());
            this.installed = true;
        }
    }

    public synchronized void setUser(java.lang.String userId) {
        if (userId != null) {
            com.crashlytics.android.Crashlytics.setUserIdentifier(userId);
        }
    }

    public void reportNonFatalException(java.lang.Throwable throwable) {
        if (this.installed) {
            try {
                com.crashlytics.android.Crashlytics.logException(throwable);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    private void log(java.lang.String msg) {
        if (this.installed) {
            try {
                com.crashlytics.android.Crashlytics.log(msg);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    private void handleUncaughtException(java.lang.Thread thread, java.lang.Throwable exception, java.lang.Thread.UncaughtExceptionHandler crashlyticsHandler) {
        if (stopCrashReporting) {
            sLogger.i("FATAL-reporting-turned-off", exception);
            return;
        }
        stopCrashReporting = true;
        java.lang.String tag = "FATAL-CRASH";
        java.lang.String msg = tag + " Uncaught exception - " + thread.getName() + ":" + thread.getId() + " ui thread id:" + android.os.Looper.getMainLooper().getThread().getId();
        android.util.Log.e(tag, msg, exception);
        sLogger.e(msg, exception);
        sLogger.i("closing logger");
        com.navdy.service.library.log.Logger.close();
        if (crashlyticsHandler != null) {
            crashlyticsHandler.uncaughtException(thread, exception);
        }
    }
}
