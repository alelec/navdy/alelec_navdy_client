package com.navdy.client.app.framework.receiver;

public class WifiReceiver extends android.content.BroadcastReceiver {
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.receiver.WifiReceiver.class);

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.app.service.DataCollectionService.sendToS3IfNotAlreadySending();
        }
    }

    public void onReceive(android.content.Context context, android.content.Intent intent) {
        logger.v("onReceive connection");
        android.content.Context appContext = com.navdy.client.app.NavdyApplication.getAppContext();
        if (com.navdy.service.library.util.SystemUtils.isConnectedToWifi(appContext) || !com.navdy.client.app.ui.settings.SettingsUtils.isLimitingCellularData()) {
            if (com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(appContext)) {
                com.navdy.client.app.framework.util.SupportTicketService.submitTicketsIfConditionsAreMet();
            }
            if (com.navdy.service.library.util.SystemUtils.isConnectedToWifi(appContext)) {
                logger.d("Wi-Fi connected. Starting OTA Service");
                android.content.Intent i = com.navdy.client.ota.OTAUpdateService.getServiceIntent(appContext);
                i.putExtra(com.navdy.client.ota.OTAUpdateService.EXTRA_WIFI_TRIGGER, true);
                appContext.startService(i);
                com.navdy.client.app.framework.suggestion.DestinationSuggestionService.uploadTripDatabase(context, false);
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.receiver.WifiReceiver.Anon1(), 1);
                return;
            }
            return;
        }
        logger.v("app is limiting cellular data, do not submit support tickets when wifi is off");
    }
}
