package com.navdy.client.app.framework.servicehandler;

public final class NetworkStatusManager$$InjectAdapter extends dagger.internal.Binding<com.navdy.client.app.framework.servicehandler.NetworkStatusManager> implements javax.inject.Provider<com.navdy.client.app.framework.servicehandler.NetworkStatusManager>, dagger.MembersInjector<com.navdy.client.app.framework.servicehandler.NetworkStatusManager> {
    private dagger.internal.Binding<com.navdy.service.library.network.http.IHttpManager> mHttpManager;

    public NetworkStatusManager$$InjectAdapter() {
        super("com.navdy.client.app.framework.servicehandler.NetworkStatusManager", "members/com.navdy.client.app.framework.servicehandler.NetworkStatusManager", false, com.navdy.client.app.framework.servicehandler.NetworkStatusManager.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mHttpManager = linker.requestBinding("com.navdy.service.library.network.http.IHttpManager", com.navdy.client.app.framework.servicehandler.NetworkStatusManager.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mHttpManager);
    }

    public com.navdy.client.app.framework.servicehandler.NetworkStatusManager get() {
        com.navdy.client.app.framework.servicehandler.NetworkStatusManager result = new com.navdy.client.app.framework.servicehandler.NetworkStatusManager();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.client.app.framework.servicehandler.NetworkStatusManager object) {
        object.mHttpManager = (com.navdy.service.library.network.http.IHttpManager) this.mHttpManager.get();
    }
}
