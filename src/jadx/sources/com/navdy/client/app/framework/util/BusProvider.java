package com.navdy.client.app.framework.util;

public final class BusProvider extends com.squareup.otto.Bus {
    private static final com.navdy.client.app.framework.util.BusProvider singleton = new com.navdy.client.app.framework.util.BusProvider();
    private final android.os.Handler mHandler = new android.os.Handler(android.os.Looper.getMainLooper());

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Object val$event;

        Anon1(java.lang.Object obj) {
            this.val$event = obj;
        }

        public void run() {
            com.navdy.client.app.framework.util.BusProvider.super.post(this.val$event);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Object val$object;

        Anon2(java.lang.Object obj) {
            this.val$object = obj;
        }

        public void run() {
            com.navdy.client.app.framework.util.BusProvider.super.register(this.val$object);
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Object val$object;

        Anon3(java.lang.Object obj) {
            this.val$object = obj;
        }

        public void run() {
            com.navdy.client.app.framework.util.BusProvider.super.unregister(this.val$object);
        }
    }

    private BusProvider() {
    }

    public static com.navdy.client.app.framework.util.BusProvider getInstance() {
        return singleton;
    }

    public void post(java.lang.Object event) {
        if (event != null) {
            this.mHandler.post(new com.navdy.client.app.framework.util.BusProvider.Anon1(event));
        }
    }

    public void register(java.lang.Object object) {
        if (object != null) {
            if (android.os.Looper.getMainLooper() == android.os.Looper.myLooper()) {
                super.register(object);
            } else {
                this.mHandler.post(new com.navdy.client.app.framework.util.BusProvider.Anon2(object));
            }
        }
    }

    public void unregister(java.lang.Object object) {
        if (object != null) {
            if (android.os.Looper.getMainLooper() == android.os.Looper.myLooper()) {
                super.unregister(object);
            } else {
                this.mHandler.post(new com.navdy.client.app.framework.util.BusProvider.Anon3(object));
            }
        }
    }
}
