package com.navdy.client.app.framework.util;

public class VersioningUtils {
    private static final java.lang.String SHARED_PREF_FAVORITES = "favorites";
    private static final java.lang.String SHARED_PREF_SUGGESTIONS = "suggestions";
    private static final java.lang.String VERSION = "version";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.VersioningUtils.class);

    static class Anon1 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Long val$serialNumber;
        final /* synthetic */ java.util.List val$suggestions;

        Anon1(java.lang.Long l, java.util.List list) {
            this.val$serialNumber = l;
            this.val$suggestions = list;
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.sendSuggestionsToDisplay(this.val$serialNumber.longValue(), this.val$suggestions);
        }
    }

    public static long getCurrentVersion(android.content.Context context, com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType destinationType) {
        return context.getSharedPreferences(getSharedPrefFor(destinationType), 0).getLong(VERSION, 0);
    }

    private static void setCurrentVersion(android.content.Context context, com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType destinationType, java.lang.Long version) {
        context.getSharedPreferences(getSharedPrefFor(destinationType), 0).edit().putLong(VERSION, version.longValue()).apply();
    }

    private static java.lang.String getSharedPrefFor(com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType destinationType) {
        switch (destinationType) {
            case FAVORITES:
                return SHARED_PREF_FAVORITES;
            default:
                return SHARED_PREF_SUGGESTIONS;
        }
    }

    public static void increaseVersionAndSendToDisplay(com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType destinationType) {
        increaseVersion(com.navdy.client.app.NavdyApplication.getAppContext(), destinationType);
        if (!com.navdy.client.app.framework.AppInstance.getInstance().isDeviceConnected()) {
            sLogger.v("HUD is not connected so ignoring the request to increaseVersionAndSendToDisplay (destinationType = " + destinationType + ")");
        } else {
            com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.sendDestinationsToDisplay(destinationType);
        }
    }

    static void increaseVersionAndSendTheseSuggestionsToDisplayAsync(java.util.List<com.navdy.client.app.framework.models.Suggestion> suggestions) {
        java.lang.Long serialNumber = increaseVersion(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType.SUGGESTIONS);
        if (!com.navdy.client.app.framework.AppInstance.getInstance().isDeviceConnected()) {
            sLogger.v("HUD is not connected so ignoring the request to increaseVersionAndSendTheseSuggestionsToDisplayAsync");
        } else {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.util.VersioningUtils.Anon1(serialNumber, suggestions), 1);
        }
    }

    @android.support.annotation.NonNull
    private static java.lang.Long increaseVersion(android.content.Context context, com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType destinationType) {
        java.lang.Long newVersion = java.lang.Long.valueOf(getCurrentVersion(context, destinationType) + 1);
        setCurrentVersion(context, destinationType, newVersion);
        return newVersion;
    }
}
