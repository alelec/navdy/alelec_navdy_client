package com.navdy.client.app.framework.receiver;

public class SmsListener extends android.content.BroadcastReceiver {
    private static final java.lang.String ACTION_SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.receiver.SmsListener.class);

    public void onReceive(android.content.Context context, android.content.Intent intent) {
        if (com.navdy.client.app.ui.glances.GlanceUtils.isThisGlanceEnabledAsWellAsGlobal(com.navdy.client.app.framework.glances.GlanceConstants.SMS_PACKAGE)) {
            com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
            if (remoteDevice != null && remoteDevice.getDeviceInfo() != null && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(remoteDevice.getDeviceInfo().protocolVersion)) {
                java.lang.String protocol = remoteDevice.getDeviceInfo().protocolVersion;
                if (intent.getAction().equals(ACTION_SMS_RECEIVED)) {
                    android.telephony.SmsMessage[] msgs = getMessagesFromIntent(intent);
                    if (msgs != null) {
                        sLogger.d("[navdyinfo-sms] sms message received");
                        for (android.telephony.SmsMessage msg : msgs) {
                            java.lang.String address = msg.getOriginatingAddress();
                            java.lang.String contactName = com.navdy.client.debug.util.Contacts.lookupNameFromPhoneNumber(context, address);
                            java.lang.String body = msg.getMessageBody();
                            if (!com.navdy.client.app.framework.glances.GlancesHelper.isHudVersionCompatible(protocol)) {
                                remoteDevice.postEvent((com.squareup.wire.Message) com.navdy.client.debug.util.NotificationBuilder.buildSmsNotification(contactName, address, body, false));
                            } else {
                                java.lang.String id = com.navdy.client.app.framework.glances.GlancesHelper.getId();
                                java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
                                com.navdy.service.library.events.glances.KeyValue keyValue = new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), contactName);
                                data.add(keyValue);
                                com.navdy.service.library.events.glances.KeyValue keyValue2 = new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name(), address);
                                data.add(keyValue2);
                                data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_IS_SMS.name(), ""));
                                com.navdy.service.library.events.glances.KeyValue keyValue3 = new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), body);
                                data.add(keyValue3);
                                java.util.List<com.navdy.service.library.events.glances.GlanceEvent.GlanceActions> actions = new java.util.ArrayList<>();
                                actions.add(com.navdy.service.library.events.glances.GlanceEvent.GlanceActions.REPLY);
                                com.navdy.client.app.framework.glances.GlancesHelper.sendEvent(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider(com.navdy.client.app.framework.glances.GlanceConstants.SMS_PACKAGE).id(id).glanceData(data).postTime(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).actions(actions).build());
                            }
                        }
                    }
                }
            }
        }
    }

    protected android.telephony.SmsMessage[] getMessagesFromIntent(android.content.Intent intent) {
        java.lang.Object[] messages = (java.lang.Object[]) intent.getSerializableExtra("pdus");
        int pduCount = messages.length;
        android.telephony.SmsMessage[] msgs = new android.telephony.SmsMessage[pduCount];
        for (int i = 0; i < pduCount; i++) {
            msgs[i] = android.telephony.SmsMessage.createFromPdu((byte[]) messages[i]);
        }
        return msgs;
    }
}
