package com.navdy.client.app.framework.util;

public class SupportTicketService extends android.app.IntentService {
    public static final java.lang.String EXTRA_CREATE_INDETERMINATE_NOTIFICATION = "create_indeterminate_notification";
    public static final java.lang.String EXTRA_EMAIL = "extra_email";
    public static final java.lang.String EXTRA_RESET_COUNTER = "reset_retry_counter";
    public static final java.lang.String EXTRA_TICKET_RANDOM_ID = "random_id";
    private static final java.lang.String INTENT_SERVICE_NAME = "SUBMIT_TICKET";
    private static final long MAX_FILE_SIZE = 2097152;
    public static final java.lang.String NEW_FOLDER_FORMAT = "yyyy-MM-dd'_'HH:mm:ss.SSS";
    private static final long PULL_LOGS_TIMEOUT = 15000;
    private static final long RETRY_INTERVAL_MILLIS = java.util.concurrent.TimeUnit.MINUTES.toMillis(45);
    private static final int RETRY_REQUEST_CODE = 1;
    private static final java.lang.String SHARED_PREF_TICKET_LAST_RETRY_DATE = "ticket_last_retry_date";
    private static final java.lang.String SHARED_PREF_TICKET_RETRY_COUNT = "ticket_retry_count";
    private static final int SHARED_PREF_TICKET_RETRY_COUNT_DEFAULT = 0;
    private static final long SHARED_PREF_TICKET_RETRY_DATE_DEFAULT = 0;
    private static final int SUBMIT_AFTER_INIT_DELAY = 10000;
    private static final java.lang.String SUPPORT_TICKET_SHARED_PREFS = "support_ticket_shared_prefs";
    private static final java.lang.String TEMP_FILE_TIMESTAMP_FORMAT = "yyyy_dd_MM-hh_mm_ss_aa";
    public static final java.lang.String TICKET_ATTACHMENTS_FILENAME = "attachments.zip";
    public static final java.lang.String TICKET_FILTER_FOR_READY_FOLDERS = "_READY";
    public static final java.lang.String TICKET_FOLDER_README_FILENAME = "readme.txt";
    private static final int TIMER_REQUEST_CODE = 0;
    private static final long TRY_MAX_COUNT = 4;
    public static final java.lang.Boolean VERBOSE = java.lang.Boolean.valueOf(true);
    private static java.lang.String email;
    public static final java.io.FilenameFilter filterOnReadyFolders = new com.navdy.client.app.framework.util.SupportTicketService.Anon1();
    private static final java.text.SimpleDateFormat format = new java.text.SimpleDateFormat(TEMP_FILE_TIMESTAMP_FORMAT, java.util.Locale.US);
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.SupportTicketService.class);
    private java.lang.String lastTicketId = "";
    private com.navdy.client.app.framework.util.SubmitTicketWorker submitTicketWorker;

    static class Anon1 implements java.io.FilenameFilter {
        Anon1() {
        }

        public boolean accept(java.io.File file, java.lang.String string) {
            return string.contains(com.navdy.client.app.framework.util.SupportTicketService.TICKET_FILTER_FOR_READY_FOLDERS);
        }
    }

    class Anon2 implements com.navdy.client.app.framework.util.SubmitTicketWorker.OnFinishSubmittingTickets {
        final /* synthetic */ java.io.File val$ticketsDirectory;

        Anon2(java.io.File file) {
            this.val$ticketsDirectory = file;
        }

        public void onFinish(int failCount, boolean showSuccessNotification) {
            java.io.File[] remainingFolders = this.val$ticketsDirectory.listFiles(com.navdy.client.app.framework.util.SupportTicketService.filterOnReadyFolders);
            android.content.SharedPreferences.Editor editor = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit();
            if (remainingFolders == null || remainingFolders.length != 0) {
                com.navdy.client.app.framework.util.SupportTicketService.logger.d("pending tickets awaiting hud logs exist");
                editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.PENDING_TICKETS_EXIST, true);
                if (com.navdy.client.app.framework.util.SupportTicketService.isBelowMaxRetryCount()) {
                    com.navdy.client.app.framework.util.SupportTicketService.scheduleAlarmForSubmittingPendingTickets(com.navdy.client.app.framework.util.SupportTicketService.RETRY_INTERVAL_MILLIS);
                }
            } else {
                com.navdy.client.app.framework.util.SupportTicketService.logger.d("no pending tickets remaining");
                editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.PENDING_TICKETS_EXIST, false);
                editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.TICKETS_AWAITING_HUD_LOG_EXIST, false);
            }
            editor.apply();
            if (failCount == 0) {
                com.navdy.client.app.framework.util.SupportTicketService.logger.d("Folders successfully submitted.");
                com.navdy.client.app.framework.util.SupportTicketService.resetRetryCount();
                com.navdy.client.app.framework.util.SupportTicketService.this.endTicketNotifications();
                if (showSuccessNotification) {
                    com.navdy.client.app.framework.util.SupportTicketService.this.buildNotificationForSuccess();
                }
            } else {
                com.navdy.client.app.framework.util.SupportTicketService.this.buildNotificationForRetry();
                if (com.navdy.client.app.framework.util.SupportTicketService.isBelowMaxRetryCount()) {
                    com.navdy.client.app.framework.util.SupportTicketService.scheduleAlarmForSubmittingPendingTickets(com.navdy.client.app.framework.util.SupportTicketService.RETRY_INTERVAL_MILLIS);
                }
                com.navdy.client.app.framework.util.SupportTicketService.this.incrementTryCount();
            }
            com.navdy.client.app.framework.util.SupportTicketService.this.incrementUpdateDate();
            com.navdy.client.app.framework.util.SupportTicketService.this.finishService(true);
        }
    }

    static class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.framework.util.SupportTicketService.OnLogCollectedInterface val$onLogCollectedInterface;

        class Anon1 implements com.navdy.client.debug.file.FileTransferManager.FileTransferListener {
            final /* synthetic */ java.util.ArrayList val$attachments;
            final /* synthetic */ java.lang.String val$logsFolder;

            Anon1(java.lang.String str, java.util.ArrayList arrayList) {
                this.val$logsFolder = str;
                this.val$attachments = arrayList;
            }

            public void onFileTransferResponse(com.navdy.service.library.events.file.FileTransferResponse response) {
                if (com.navdy.client.app.framework.util.SupportTicketService.VERBOSE.booleanValue()) {
                    com.navdy.client.app.framework.util.SupportTicketService.logger.v("onFileTransferResponse: " + response.success);
                }
                if (!response.success.booleanValue()) {
                    onError(response.error, "Error collecting display logs");
                    return;
                }
                com.navdy.client.app.framework.util.SupportTicketService.logger.v("HUD file name saved");
                this.val$attachments.add(new java.io.File(this.val$logsFolder, response.destinationFileName));
            }

            public void onFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus status) {
                if (com.navdy.client.app.framework.util.SupportTicketService.VERBOSE.booleanValue()) {
                    com.navdy.client.app.framework.util.SupportTicketService.logger.v("onFileTransferStatus: " + status.success);
                }
                if (status.success.booleanValue() && status.transferComplete.booleanValue()) {
                    com.navdy.client.app.framework.util.SupportTicketService.logger.v("Logs collected");
                    if (this.val$attachments.size() > 0) {
                        com.navdy.client.app.framework.util.SupportTicketService.Anon3.this.val$onLogCollectedInterface.onLogCollectionSucceeded(this.val$attachments);
                    } else {
                        com.navdy.client.app.framework.util.SupportTicketService.Anon3.this.val$onLogCollectedInterface.onLogCollectionFailed();
                    }
                }
            }

            public void onError(com.navdy.service.library.events.file.FileTransferError errorCode, java.lang.String error) {
                com.navdy.client.app.framework.util.SupportTicketService.logger.v("Error collecting display logs " + error);
                if (this.val$attachments.size() > 0) {
                    com.navdy.client.app.framework.util.SupportTicketService.Anon3.this.val$onLogCollectedInterface.onLogCollectionSucceeded(this.val$attachments);
                } else {
                    com.navdy.client.app.framework.util.SupportTicketService.Anon3.this.val$onLogCollectedInterface.onLogCollectionFailed();
                }
            }
        }

        Anon3(com.navdy.client.app.framework.util.SupportTicketService.OnLogCollectedInterface onLogCollectedInterface) {
            this.val$onLogCollectedInterface = onLogCollectedInterface;
        }

        public void run() {
            android.content.Context applicationContext = com.navdy.client.app.NavdyApplication.getAppContext();
            java.lang.String logsFolder = com.navdy.client.app.framework.PathManager.getInstance().getLogsFolder();
            java.lang.String timestamp = com.navdy.client.app.framework.util.SupportTicketService.format.format(java.lang.Long.valueOf(java.lang.System.currentTimeMillis()));
            java.lang.String deviceDbsFilePath = logsFolder + java.io.File.separator + "Database_" + timestamp + ".db";
            java.io.File file = new java.io.File(deviceDbsFilePath);
            java.lang.String deviceSharedPrefsFolderPath = logsFolder + java.io.File.separator + timestamp + "-SharedPrefs";
            java.io.File file2 = new java.io.File(deviceSharedPrefsFolderPath);
            java.io.File tracesFile = null;
            try {
                java.io.File file3 = new java.io.File(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.trace_file_path));
                tracesFile = file3;
            } catch (java.lang.Exception e) {
            }
            java.util.ArrayList<java.io.File> attachments = new java.util.ArrayList<>();
            try {
                com.navdy.client.app.framework.util.SupportTicketService.dumpDbs(applicationContext, deviceDbsFilePath);
                if (file.exists()) {
                    com.navdy.client.app.framework.util.SupportTicketService.logger.v("mobile client Dbs successfully dumped");
                    attachments.add(file);
                }
                if (file2.mkdirs()) {
                    com.navdy.client.app.framework.util.SupportTicketService.dumpSharedPrefs(applicationContext, deviceSharedPrefsFolderPath);
                    com.navdy.client.app.framework.util.SupportTicketService.logger.v("mobile client SharedPrefs successfully dumped");
                    java.io.File[] listOfSharedPrefsFiles = file2.listFiles();
                    if (listOfSharedPrefsFiles != null) {
                        for (java.io.File sharedPrefsFile : listOfSharedPrefsFiles) {
                            if (sharedPrefsFile != null && sharedPrefsFile.exists() && sharedPrefsFile.isFile()) {
                                attachments.add(sharedPrefsFile);
                            }
                        }
                    }
                }
                java.util.Iterator it = com.navdy.client.app.NavdyApplication.getLogFiles().iterator();
                while (it.hasNext()) {
                    java.io.File logFile = (java.io.File) it.next();
                    if (logFile != null && logFile.length() > 0) {
                        attachments.add(logFile);
                    }
                }
                if (tracesFile != null) {
                    if (tracesFile.length() > 0) {
                        attachments.add(tracesFile);
                    }
                }
                new com.navdy.client.debug.file.RemoteFileTransferManager(applicationContext, com.navdy.service.library.events.file.FileType.FILE_TYPE_LOGS, logsFolder, 15000, (com.navdy.client.debug.file.FileTransferManager.FileTransferListener) new com.navdy.client.app.framework.util.SupportTicketService.Anon3.Anon1(logsFolder, attachments)).pullFile();
            } catch (Throwable t) {
                com.navdy.client.app.framework.util.SupportTicketService.logger.v("Error collecting device logs: " + t);
                if (attachments.size() > 0) {
                    this.val$onLogCollectedInterface.onLogCollectionSucceeded(attachments);
                } else {
                    this.val$onLogCollectedInterface.onLogCollectionFailed();
                }
            }
        }
    }

    static class Anon4 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.framework.util.SupportTicketService.OnLogCollectedInterface val$onLogCollectedInterface;

        class Anon1 implements com.navdy.client.debug.file.FileTransferManager.FileTransferListener {
            final /* synthetic */ java.util.ArrayList val$attachments;
            final /* synthetic */ java.lang.String val$logsFolder;

            Anon1(java.lang.String str, java.util.ArrayList arrayList) {
                this.val$logsFolder = str;
                this.val$attachments = arrayList;
            }

            public void onFileTransferResponse(com.navdy.service.library.events.file.FileTransferResponse response) {
                if (com.navdy.client.app.framework.util.SupportTicketService.VERBOSE.booleanValue()) {
                    com.navdy.client.app.framework.util.SupportTicketService.logger.v("onFileTransferResponse: " + response.success);
                }
                if (!response.success.booleanValue()) {
                    onError(response.error, "Error collecting display logs");
                    return;
                }
                com.navdy.client.app.framework.util.SupportTicketService.logger.v("HUD log saved");
                this.val$attachments.add(new java.io.File(this.val$logsFolder, response.destinationFileName));
            }

            public void onFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus status) {
                if (com.navdy.client.app.framework.util.SupportTicketService.VERBOSE.booleanValue()) {
                    com.navdy.client.app.framework.util.SupportTicketService.logger.v("onFileTransferStatus: " + status.success);
                }
                if (status.success.booleanValue() && status.transferComplete.booleanValue()) {
                    com.navdy.client.app.framework.util.SupportTicketService.logger.v("Logs collected");
                    if (this.val$attachments.size() > 0) {
                        com.navdy.client.app.framework.util.SupportTicketService.Anon4.this.val$onLogCollectedInterface.onLogCollectionSucceeded(this.val$attachments);
                    } else {
                        com.navdy.client.app.framework.util.SupportTicketService.Anon4.this.val$onLogCollectedInterface.onLogCollectionFailed();
                    }
                }
            }

            public void onError(com.navdy.service.library.events.file.FileTransferError errorCode, java.lang.String error) {
                com.navdy.client.app.framework.util.SupportTicketService.logger.v("Error collecting display logs " + error);
                if (this.val$attachments.size() > 0) {
                    com.navdy.client.app.framework.util.SupportTicketService.Anon4.this.val$onLogCollectedInterface.onLogCollectionSucceeded(this.val$attachments);
                } else {
                    com.navdy.client.app.framework.util.SupportTicketService.Anon4.this.val$onLogCollectedInterface.onLogCollectionFailed();
                }
            }
        }

        Anon4(com.navdy.client.app.framework.util.SupportTicketService.OnLogCollectedInterface onLogCollectedInterface) {
            this.val$onLogCollectedInterface = onLogCollectedInterface;
        }

        public void run() {
            android.content.Context applicationContext = com.navdy.client.app.NavdyApplication.getAppContext();
            java.lang.String logsFolder = com.navdy.client.app.framework.PathManager.getInstance().getLogsFolder();
            java.util.ArrayList<java.io.File> attachments = new java.util.ArrayList<>();
            try {
                new com.navdy.client.debug.file.RemoteFileTransferManager(applicationContext, com.navdy.service.library.events.file.FileType.FILE_TYPE_LOGS, logsFolder, 15000, (com.navdy.client.debug.file.FileTransferManager.FileTransferListener) new com.navdy.client.app.framework.util.SupportTicketService.Anon4.Anon1(logsFolder, attachments)).pullFile();
            } catch (Throwable t) {
                com.navdy.client.app.framework.util.SupportTicketService.logger.v("Error collecting hud logs " + t);
                if (attachments.size() > 0) {
                    this.val$onLogCollectedInterface.onLogCollectionSucceeded(attachments);
                } else {
                    this.val$onLogCollectedInterface.onLogCollectionFailed();
                }
            }
        }
    }

    public interface OnLogCollectedInterface {
        void onLogCollectionFailed();

        void onLogCollectionSucceeded(java.util.ArrayList<java.io.File> arrayList);
    }

    public static class SubmitTicketFinishedEvent {
        private boolean conditionsMet;
        private java.lang.String ticketId = "";

        SubmitTicketFinishedEvent(boolean conditionsMet2, @android.support.annotation.NonNull java.lang.String ticketId2) {
            this.conditionsMet = conditionsMet2;
            this.ticketId = ticketId2;
        }

        public boolean getConditionsMet() {
            return this.conditionsMet;
        }

        @android.support.annotation.NonNull
        public java.lang.String getTicketId() {
            return this.ticketId;
        }
    }

    public static void writeJsonObjectToFile(org.json.JSONObject jsonObject, java.lang.String directory, com.navdy.service.library.log.Logger logger2) {
        java.io.FileWriter fileWriter = null;
        try {
            java.io.FileWriter fileWriter2 = new java.io.FileWriter(directory + java.io.File.separator + com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_JSON_FILENAME);
            try {
                fileWriter2.write(jsonObject.toString());
                fileWriter2.flush();
                com.navdy.service.library.util.IOUtils.closeStream(fileWriter2);
                java.io.FileWriter fileWriter3 = fileWriter2;
            } catch (java.lang.Exception e) {
                e = e;
                fileWriter = fileWriter2;
                try {
                    logger2.e("Failed to write JSON to file: " + e.toString());
                    com.navdy.service.library.util.IOUtils.closeStream(fileWriter);
                } catch (Throwable th) {
                    th = th;
                    com.navdy.service.library.util.IOUtils.closeStream(fileWriter);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileWriter = fileWriter2;
                com.navdy.service.library.util.IOUtils.closeStream(fileWriter);
                throw th;
            }
        } catch (java.lang.Exception e2) {
            e = e2;
            logger2.e("Failed to write JSON to file: " + e.toString());
            com.navdy.service.library.util.IOUtils.closeStream(fileWriter);
        }
    }

    public SupportTicketService() {
        super(INTENT_SERVICE_NAME);
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
        this.submitTicketWorker = new com.navdy.client.app.framework.util.SubmitTicketWorker();
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(email)) {
            setEmail(com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences().getString("email", ""));
        }
    }

    private synchronized void authenticateZendesk() {
        if (VERBOSE.booleanValue()) {
            logger.v("authenticateZendesk");
        }
        java.lang.String zendeskAppId = com.navdy.client.app.framework.util.CredentialsUtils.getCredentials(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.metadata_zendesk_appid));
        java.lang.String zendeskOAuth = com.navdy.client.app.framework.util.CredentialsUtils.getCredentials(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.metadata_zendesk_oauth));
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        com.zendesk.sdk.network.impl.ZendeskConfig.INSTANCE.init(context, context.getString(com.navdy.client.R.string.zendesk_url), zendeskAppId, zendeskOAuth);
        if (com.zendesk.sdk.network.impl.ZendeskConfig.INSTANCE.isInitialized()) {
            logger.d("Zendesk isInitialized == true");
            com.zendesk.sdk.network.impl.ZendeskConfig.INSTANCE.setIdentity(new com.zendesk.sdk.model.access.AnonymousIdentity.Builder().withNameIdentifier(com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.FULL_NAME, "")).withEmailIdentifier(email).build());
            scheduleAlarmForSubmittingPendingTickets(10000);
        } else {
            logger.d("Zendesk initialization failed");
            finishService(false);
        }
    }

    protected synchronized void onHandleIntent(android.content.Intent intent) {
        logger.d("SupportTicketService handling intent");
        if (!com.navdy.client.app.ui.base.BaseActivity.weHaveStoragePermission()) {
            logger.e("We do not have storage permission");
            finishService(false);
        }
        if (!com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(com.navdy.client.app.NavdyApplication.getAppContext())) {
            if (VERBOSE.booleanValue()) {
                logger.d("Network is not accessible. Scheduling another alarm for the future...");
            }
            if (android.os.Build.VERSION.SDK_INT >= 24 && !com.navdy.client.app.ui.settings.SettingsUtils.isLimitingCellularData()) {
                com.navdy.client.app.framework.service.NetworkConnectivityJobService.scheduleNetworkServiceForNougatOrAbove();
            }
            buildNotificationForRetry();
            finishService(false);
        }
        if (!com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.PENDING_TICKETS_EXIST, false)) {
            logger.d("pending tickets currently do not exist");
            finishService(false);
        }
        if (intent.getBooleanExtra(EXTRA_RESET_COUNTER, false)) {
            if (VERBOSE.booleanValue()) {
                logger.d("reset boolean extra found");
            }
            resetRetryCount();
        }
        if (intent.getBooleanExtra(EXTRA_CREATE_INDETERMINATE_NOTIFICATION, false)) {
            buildNotificationForIndeterminateProgress();
        }
        if (intent.hasExtra(EXTRA_TICKET_RANDOM_ID)) {
            this.lastTicketId = intent.getStringExtra(EXTRA_TICKET_RANDOM_ID);
        }
        java.lang.String extraEmail = intent.getStringExtra(EXTRA_EMAIL);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(extraEmail)) {
            if (VERBOSE.booleanValue()) {
                logger.d("extra intent email found: " + extraEmail);
            }
            setEmail(extraEmail);
        }
        logger.v("onHandleIntent: " + intent);
        if (this.submitTicketWorker.isRunning()) {
            logger.d("SupportTicketService is already running. Avoiding a duplicate call to intent service.");
        } else if (com.zendesk.sdk.network.impl.ZendeskConfig.INSTANCE.isInitialized()) {
            submitPendingTickets();
        } else {
            authenticateZendesk();
        }
    }

    private synchronized void submitPendingTickets() {
        try {
            logger.d("submitPendingTickets");
            java.io.File ticketsDirectory = new java.io.File(com.navdy.client.app.framework.PathManager.getInstance().getTicketFolderPath());
            java.io.File[] readyFolders = ticketsDirectory.listFiles(filterOnReadyFolders);
            if (readyFolders == null || readyFolders.length <= 0) {
                logger.v("There are no pending tickets found");
                endTicketNotifications();
                finishService(false);
            }
            this.submitTicketWorker.start(new com.navdy.client.app.framework.util.SupportTicketService.Anon2(ticketsDirectory), readyFolders);
        } catch (java.lang.Exception e) {
            logger.e("There was a problem submitting pending tickets: " + e);
        }
        return;
    }

    public static android.content.SharedPreferences getSupportTicketPreferences() {
        return com.navdy.client.app.NavdyApplication.getAppContext().getSharedPreferences(SUPPORT_TICKET_SHARED_PREFS, 0);
    }

    public static void scheduleAlarmForSubmittingPendingTickets(long interval) {
        if (com.navdy.client.app.ui.settings.SettingsUtils.isLimitingCellularData()) {
            logger.v("app is limiting cellular data, won't schedule alarm for submitting pending tickets");
            return;
        }
        if (VERBOSE.booleanValue()) {
            logger.v("scheduleAlarmForSubmittingPendingTickets retry coming in " + java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(interval) + " minutes");
        }
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        ((android.app.AlarmManager) context.getSystemService("alarm")).set(1, java.lang.System.currentTimeMillis() + interval, android.app.PendingIntent.getService(com.navdy.client.app.NavdyApplication.getAppContext(), 0, new android.content.Intent(context, com.navdy.client.app.framework.util.SupportTicketService.class), 268435456));
    }

    public void buildNotificationForRetry() {
        if (VERBOSE.booleanValue()) {
            logger.v("buildNotificationForRetry");
        }
        int failureCount = 0;
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        android.content.Intent intent = new android.content.Intent(context, com.navdy.client.app.framework.util.SupportTicketService.class);
        intent.putExtra(EXTRA_RESET_COUNTER, true);
        intent.putExtra(EXTRA_CREATE_INDETERMINATE_NOTIFICATION, true);
        android.app.PendingIntent pendingIntent = android.app.PendingIntent.getService(com.navdy.client.app.NavdyApplication.getAppContext(), 1, intent, 134217728);
        java.io.File[] folders = new java.io.File(com.navdy.client.app.framework.PathManager.getInstance().getTicketFolderPath()).listFiles(filterOnReadyFolders);
        if (folders != null) {
            for (java.io.File ticketFolder : folders) {
                java.io.File content = new java.io.File(ticketFolder.getAbsolutePath() + java.io.File.separator + com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_JSON_FILENAME);
                if (VERBOSE.booleanValue()) {
                    logger.v("content filepath: " + ticketFolder.getAbsolutePath() + java.io.File.separator + com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_JSON_FILENAME);
                }
                org.json.JSONObject jsonObject = com.navdy.client.app.framework.util.SubmitTicketWorker.getJSONObjectFromFile(content);
                if (jsonObject != null && !jsonObject.optString(com.navdy.client.app.ui.settings.ZendeskConstants.TICKET_ACTION, "").equals(com.navdy.client.app.ui.settings.ZendeskConstants.ACTION_FETCH_HUD_LOG_UPLOAD_DELETE)) {
                    failureCount++;
                }
            }
        }
        if (failureCount > 0) {
            if (VERBOSE.booleanValue()) {
                logger.v("failureCount: " + failureCount);
            }
            android.support.v4.app.NotificationCompat.Builder notificationBuilder = new android.support.v4.app.NotificationCompat.Builder(context).setSmallIcon(com.navdy.client.R.drawable.ic_navdy_logo).setContentTitle(context.getString(com.navdy.client.R.string.zendesk_unable_to_submit_x_tickets, new java.lang.Object[]{java.lang.Integer.valueOf(failureCount)})).setContentText(context.getString(com.navdy.client.R.string.zendesk_notification_failure_message)).setContentIntent(pendingIntent);
            android.app.NotificationManager notificationManager = (android.app.NotificationManager) getSystemService("notification");
            if (notificationManager != null) {
                notificationManager.notify(2, notificationBuilder.build());
            }
        } else if (VERBOSE.booleanValue()) {
            logger.v("no failures found. Not building notification for retry");
        }
    }

    public void buildNotificationForSuccess() {
        if (VERBOSE.booleanValue()) {
            logger.v("buildNotificationForSuccess");
        }
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        android.support.v4.app.NotificationCompat.Builder notificationBuilder = new android.support.v4.app.NotificationCompat.Builder(context).setSmallIcon(com.navdy.client.R.drawable.ic_navdy_logo).setContentTitle(context.getString(com.navdy.client.R.string.ticket_submission_success_title));
        android.app.NotificationManager notificationManager = (android.app.NotificationManager) getSystemService("notification");
        if (notificationManager != null) {
            notificationManager.notify(4, notificationBuilder.build());
        }
    }

    public void buildNotificationForIndeterminateProgress() {
        if (VERBOSE.booleanValue()) {
            logger.v("buildNotificationForIndeterminateProgress");
        }
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        android.support.v4.app.NotificationCompat.Builder notificationBuilder = new android.support.v4.app.NotificationCompat.Builder(context).setSmallIcon(com.navdy.client.R.drawable.ic_navdy_logo).setContentTitle(context.getString(com.navdy.client.R.string.zendesk_upload_support_request)).setProgress(100, 0, true);
        android.app.NotificationManager notificationManager = (android.app.NotificationManager) getSystemService("notification");
        if (notificationManager != null) {
            notificationManager.notify(3, notificationBuilder.build());
        }
    }

    public void endTicketNotifications() {
        if (VERBOSE.booleanValue()) {
            logger.v("endTicketNotifications");
        }
        android.app.NotificationManager notificationManager = (android.app.NotificationManager) getSystemService("notification");
        if (notificationManager != null) {
            notificationManager.cancel(3);
            notificationManager.cancel(2);
        }
    }

    public static void resetRetryCount() {
        if (VERBOSE.booleanValue()) {
            logger.v("resetRetryCount");
        }
        getSupportTicketPreferences().edit().putInt(SHARED_PREF_TICKET_RETRY_COUNT, 0).putLong(SHARED_PREF_TICKET_LAST_RETRY_DATE, 0).apply();
    }

    public static boolean isBelowMaxRetryCount() {
        if (VERBOSE.booleanValue()) {
            logger.v("isBelowMaxRetryCount");
        }
        int retryCount = getSupportTicketPreferences().getInt(SHARED_PREF_TICKET_RETRY_COUNT, 0);
        logger.v("retryCount: " + retryCount);
        if (((long) retryCount) <= 4) {
            return true;
        }
        return false;
    }

    public void incrementTryCount() {
        android.content.SharedPreferences sharedPreferences = getSupportTicketPreferences();
        int retryCount = sharedPreferences.getInt(SHARED_PREF_TICKET_RETRY_COUNT, 0);
        logger.d("incrementTryCount retryCount: " + retryCount);
        sharedPreferences.edit().putInt(SHARED_PREF_TICKET_RETRY_COUNT, retryCount + 1).apply();
    }

    public void incrementUpdateDate() {
        getSupportTicketPreferences().edit().putLong(SHARED_PREF_TICKET_LAST_RETRY_DATE, new java.util.Date().getTime()).apply();
    }

    public static boolean lastRetryDateIsOldEnough() {
        boolean lastRetryDateIsOldEnough = java.lang.System.currentTimeMillis() - RETRY_INTERVAL_MILLIS > getSupportTicketPreferences().getLong(SHARED_PREF_TICKET_LAST_RETRY_DATE, 0);
        if (VERBOSE.booleanValue()) {
            logger.d("lastRetryDateIsOldEnough: " + lastRetryDateIsOldEnough);
        }
        return lastRetryDateIsOldEnough;
    }

    public static void startSupportTicketService(android.content.Context context) {
        if (com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.PENDING_TICKETS_EXIST, false)) {
            context.startService(new android.content.Intent(context, com.navdy.client.app.framework.util.SupportTicketService.class));
        }
    }

    public static synchronized java.lang.String getEmail() {
        java.lang.String str;
        synchronized (com.navdy.client.app.framework.util.SupportTicketService.class) {
            str = email;
        }
        return str;
    }

    static synchronized void setEmail(java.lang.String email2) {
        synchronized (com.navdy.client.app.framework.util.SupportTicketService.class) {
            email = email2;
        }
    }

    public static boolean createFolderForTicket(java.util.ArrayList<java.io.File> attachments, org.json.JSONObject jsonObjectForTicketContents) {
        logger.i("jsonObject: " + jsonObjectForTicketContents);
        if (jsonObjectForTicketContents == null) {
            logger.e("jsonObject unsuccessfully created");
            return false;
        }
        java.lang.String folderName = new java.text.SimpleDateFormat(NEW_FOLDER_FORMAT, java.util.Locale.US).format(new java.util.Date(java.lang.System.currentTimeMillis()));
        if (VERBOSE.booleanValue()) {
            logger.v("new foldername: " + folderName);
        }
        java.io.File unfininishedDirectory = new java.io.File(com.navdy.client.app.framework.PathManager.getInstance().getTicketFolderPath() + java.io.File.separator + folderName);
        boolean folderSuccessfullyCreated = unfininishedDirectory.mkdirs();
        if (folderSuccessfullyCreated) {
            java.io.File[] basicAttachmentArray = (java.io.File[]) attachments.toArray(new java.io.File[attachments.size()]);
            if (!attachments.isEmpty()) {
                com.navdy.service.library.util.IOUtils.compressFilesToZip(com.navdy.client.app.NavdyApplication.getAppContext(), basicAttachmentArray, new java.io.File(unfininishedDirectory.getAbsolutePath() + java.io.File.separator + "attachments.zip").getAbsolutePath());
            }
            writeJsonObjectToFile(jsonObjectForTicketContents, unfininishedDirectory.getAbsolutePath(), logger);
            if (!unfininishedDirectory.renameTo(new java.io.File(unfininishedDirectory.getAbsolutePath() + TICKET_FILTER_FOR_READY_FOLDERS))) {
                return folderSuccessfullyCreated;
            }
            logger.v("rename worked: " + unfininishedDirectory.getAbsolutePath());
            return folderSuccessfullyCreated;
        }
        logger.e("folder failed");
        return folderSuccessfullyCreated;
    }

    public static void collectLogs(com.navdy.client.app.framework.util.SupportTicketService.OnLogCollectedInterface onLogCollectedInterface) {
        if (VERBOSE.booleanValue()) {
            logger.v("collectLogsAndThen");
        }
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.util.SupportTicketService.Anon3(onLogCollectedInterface), 1);
    }

    public static void collectHudLog(com.navdy.client.app.framework.util.SupportTicketService.OnLogCollectedInterface onLogCollectedInterface) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.util.SupportTicketService.Anon4(onLogCollectedInterface), 1);
    }

    private static synchronized void dumpDbs(android.content.Context context, java.lang.String fileName) throws java.lang.Throwable {
        synchronized (com.navdy.client.app.framework.util.SupportTicketService.class) {
            java.io.File backupDB = new java.io.File(fileName);
            if (backupDB.exists()) {
                com.navdy.service.library.util.IOUtils.deleteFile(context, fileName);
            }
            java.nio.channels.FileChannel src = null;
            java.nio.channels.FileChannel dst = null;
            try {
                java.io.File currentDB = context.getDatabasePath(com.navdy.client.app.providers.NavdyContentProviderConstants.DB_NAME);
                if (currentDB.length() > MAX_FILE_SIZE) {
                    logger.e("DB too big: " + currentDB.length() + " bytes");
                } else if (currentDB.exists()) {
                    src = new java.io.FileInputStream(currentDB).getChannel();
                    dst = new java.io.FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                } else {
                    logger.e("invalid DB path: " + currentDB);
                }
            } catch (java.lang.Exception e) {
                logger.e("Unable to pull the DB from the phone: ", e);
            } finally {
                com.navdy.service.library.util.IOUtils.closeStream(src);
                com.navdy.service.library.util.IOUtils.closeStream(dst);
            }
        }
        return;
    }

    private static synchronized void dumpSharedPrefs(android.content.Context context, java.lang.String destinationFolderPath) throws java.lang.Throwable {
        java.nio.channels.FileChannel src;
        java.nio.channels.FileChannel dst;
        synchronized (com.navdy.client.app.framework.util.SupportTicketService.class) {
            java.io.File destinationFolder = new java.io.File(destinationFolderPath);
            if (destinationFolder.exists() || destinationFolder.mkdirs()) {
                try {
                    java.io.File[] listOfSharedPrefsFiles = new java.io.File(context.getFilesDir().getParent() + "/shared_prefs/").listFiles();
                    if (listOfSharedPrefsFiles != null) {
                        for (java.io.File sharedPrefsFile : listOfSharedPrefsFiles) {
                            if (sharedPrefsFile != null && sharedPrefsFile.exists() && sharedPrefsFile.isFile()) {
                                java.io.File backupFile = new java.io.File(destinationFolder, "SharedPref_" + sharedPrefsFile.getName());
                                src = null;
                                dst = null;
                                src = new java.io.FileInputStream(sharedPrefsFile).getChannel();
                                dst = new java.io.FileOutputStream(backupFile).getChannel();
                                dst.transferFrom(src, 0, src.size());
                                com.navdy.service.library.util.IOUtils.closeStream(src);
                                com.navdy.service.library.util.IOUtils.closeStream(dst);
                            }
                        }
                    }
                } catch (java.lang.Exception e) {
                    logger.e("Unable to pull the shared prefs files from the phone: ", e);
                } catch (Throwable th) {
                    com.navdy.service.library.util.IOUtils.closeStream(src);
                    com.navdy.service.library.util.IOUtils.closeStream(dst);
                    throw th;
                }
            } else {
                logger.e("Unable to create the destination folder: " + destinationFolderPath);
            }
        }
        return;
    }

    public static void submitTicketsIfConditionsAreMet() {
        boolean isReachable = com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(com.navdy.client.app.NavdyApplication.getAppContext());
        boolean pendingTicketsExist = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.PENDING_TICKETS_EXIST, false);
        boolean lastRetryDateIsOldEnough = lastRetryDateIsOldEnough();
        boolean isBelowMaxRetryCount = isBelowMaxRetryCount();
        if (VERBOSE.booleanValue()) {
            logger.d("isReachable: " + isReachable + "\n pendingTicketExists: " + pendingTicketsExist + "\n lastRetryDateIsOldEnough: " + lastRetryDateIsOldEnough + "\n isBelowMaxRetryCount: " + isBelowMaxRetryCount);
        }
        if (isReachable && lastRetryDateIsOldEnough && isBelowMaxRetryCount && pendingTicketsExist) {
            if (VERBOSE.booleanValue()) {
                logger.d("Should submit tickets");
            }
            try {
                startSupportTicketService(com.navdy.client.app.NavdyApplication.getAppContext());
            } catch (java.lang.Exception e) {
                logger.e("Exception found: " + e);
            }
        }
    }

    public void finishService(boolean isSuccess) {
        com.navdy.client.app.framework.util.BusProvider.getInstance().post(new com.navdy.client.app.framework.util.SupportTicketService.SubmitTicketFinishedEvent(isSuccess, this.lastTicketId));
        stopSelf();
    }
}
