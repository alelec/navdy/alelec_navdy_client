package com.navdy.client.app.framework.util;

public class ImageUtils {
    private static final boolean VERBOSE = false;
    private static final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.ImageUtils.class);

    static class Anon1 implements com.navdy.client.app.framework.util.ImageUtils.StreamFactory {
        final /* synthetic */ java.io.File val$file;

        Anon1(java.io.File file) {
            this.val$file = file;
        }

        public java.io.InputStream getInputStream() throws java.io.FileNotFoundException {
            return new java.io.FileInputStream(this.val$file);
        }
    }

    public interface StreamFactory {
        java.io.InputStream getInputStream() throws java.io.FileNotFoundException;
    }

    @android.support.annotation.Nullable
    @android.support.annotation.CheckResult
    public static android.graphics.Bitmap getScaledBitmap(com.navdy.client.app.framework.util.ImageUtils.StreamFactory streamFactory, int width, int height) {
        android.graphics.Bitmap bitmap = null;
        if (streamFactory != null) {
            java.io.InputStream inputStream = null;
            try {
                inputStream = streamFactory.getInputStream();
                if (inputStream == null) {
                    logger.w("Failed to get input stream to scale");
                    if (inputStream != null) {
                        com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                    }
                } else {
                    android.graphics.BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    android.graphics.BitmapFactory.decodeStream(inputStream, null, options);
                    com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                    options.inSampleSize = calculateInSampleSize(options, width, height);
                    options.inJustDecodeBounds = false;
                    inputStream = streamFactory.getInputStream();
                    android.graphics.Bitmap bitmap2 = android.graphics.BitmapFactory.decodeStream(inputStream, null, options);
                    if (bitmap2 != null) {
                        bitmap = com.navdy.service.library.util.ScalingUtilities.createScaledBitmapAndRecycleOriginalIfScaled(bitmap2, width, height, com.navdy.service.library.util.ScalingUtilities.ScalingLogic.FIT);
                        if (inputStream != null) {
                            com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                        }
                    } else {
                        logger.e("Fetching compressed bitmap failed");
                        if (inputStream != null) {
                            com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                        }
                    }
                }
            } catch (Throwable th) {
                if (inputStream != null) {
                    com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                }
                throw th;
            }
        }
        return bitmap;
    }

    @android.support.annotation.Nullable
    @android.support.annotation.CheckResult
    public static android.graphics.Bitmap getScaledBitmap(java.io.File file, int width, int height) {
        return getScaledBitmap((com.navdy.client.app.framework.util.ImageUtils.StreamFactory) new com.navdy.client.app.framework.util.ImageUtils.Anon1(file), width, height);
    }

    @android.support.annotation.CheckResult
    private static int calculateInSampleSize(android.graphics.BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            int halfHeight = height / 2;
            int halfWidth = width / 2;
            while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    @android.support.annotation.CheckResult
    public static android.graphics.Bitmap resizeBitmap(android.graphics.Bitmap bitmap, int maxSize) {
        int height;
        int width;
        if (bitmap == null || maxSize < 0) {
            return null;
        }
        float ratio = ((float) bitmap.getWidth()) / ((float) bitmap.getHeight());
        if (ratio > 1.0f) {
            width = maxSize;
            height = (int) (((float) width) / ratio);
        } else {
            height = maxSize;
            width = (int) (((float) height) / ratio);
        }
        android.graphics.Bitmap createScaledBitmap = android.graphics.Bitmap.createScaledBitmap(bitmap, width, height, true);
        bitmap.recycle();
        return createScaledBitmap;
    }

    @android.support.annotation.UiThread
    @android.support.annotation.CheckResult
    private static android.graphics.Bitmap getBitmap(int resId) {
        android.graphics.Bitmap bitmap = android.graphics.BitmapFactory.decodeResource(com.navdy.client.app.NavdyApplication.getAppContext().getResources(), resId);
        if (bitmap != null) {
        }
        return bitmap;
    }

    @android.support.annotation.UiThread
    @android.support.annotation.CheckResult
    public static android.graphics.Bitmap getBitmap(int resId, int width, int height) {
        try {
            android.graphics.BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            android.content.res.Resources res = com.navdy.client.app.NavdyApplication.getAppContext().getResources();
            android.graphics.BitmapFactory.decodeResource(res, resId, options);
            options.inSampleSize = calculateInSampleSize(options, width, height);
            options.inJustDecodeBounds = false;
            android.graphics.Bitmap bitmap = android.graphics.BitmapFactory.decodeResource(res, resId, options);
            if (bitmap != null) {
            }
            return bitmap;
        } catch (Throwable t) {
            logger.e("Fetching compressed Bitmap failed: " + t);
            return null;
        }
    }

    @android.support.annotation.UiThread
    public static void loadImage(android.widget.ImageView imageView, int resId, com.navdy.client.app.framework.util.ImageCache imageCache) {
        if (imageCache != null) {
            android.graphics.Bitmap bitmap = imageCache.getBitmap(java.lang.String.valueOf(resId));
            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);
                return;
            }
        }
        imageView.setTag(java.lang.Integer.valueOf(resId));
        android.graphics.Bitmap bitmap2 = getBitmap(resId);
        if (bitmap2 != null) {
            java.lang.Object tag = imageView.getTag();
            if (tag != null && (tag instanceof java.lang.Integer) && ((java.lang.Integer) tag).intValue() == resId) {
                imageView.setTag(null);
                imageView.setImageBitmap(bitmap2);
                if (imageCache != null) {
                    imageCache.putBitmap(java.lang.String.valueOf(resId), bitmap2);
                    return;
                }
                return;
            }
            return;
        }
        logger.e("bitmap not loaded:" + resId);
    }
}
