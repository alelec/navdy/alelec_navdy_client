package com.navdy.client.app.framework.util;

public class StringUtils {
    public static final java.lang.String GOOGLE_MAPS_REGEXP = "cacheResponse\\(\\[\\[\\[(-?[0-9]+(\\.[0-9]+)\\,){2}(-?[0-9]+(\\.[0-9]+)){1}\\]";
    public static final java.lang.String LAT_LNG_REGEXP = "(-?[0-9]+\\.?[0-9]*), *(-?[0-9]+\\.?[0-9]*)";
    public static final java.lang.String NOT_WHITESPACE_REGEXP = "[^\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000]";
    private static final java.lang.String WHITESPACE_CHARS = "\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000";
    public static final java.lang.String WHITESPACE_REGEXP = "[\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000]";

    public static boolean equalsOrBothEmptyAfterTrim(java.lang.CharSequence s1, java.lang.CharSequence s2) {
        return (isEmptyAfterTrim(s1) && isEmptyAfterTrim(s2)) || (!isEmptyAfterTrim(s1) && !isEmptyAfterTrim(s2) && android.text.TextUtils.equals(s1.toString().trim(), s2.toString().trim()));
    }

    public static boolean isEmptyAfterTrim(java.lang.CharSequence s) {
        return s == null || s.toString().trim().length() <= 0;
    }

    public static long countOccurrencesOf(java.lang.String s, char c) {
        if (s == null) {
            return 0;
        }
        long num = 0;
        for (int i = 0; i < s.length(); i++) {
            num += (long) (s.charAt(i) == c ? 1 : 0);
        }
        return num;
    }

    public static java.lang.String join(java.lang.Iterable<?> iterable, java.lang.String separator) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        if (iterable != null) {
            java.util.Iterator<?> iterator = iterable.iterator();
            while (iterator.hasNext()) {
                sb.append(iterator.next().toString());
                if (iterator.hasNext()) {
                    sb.append(separator);
                }
            }
        }
        return sb.toString();
    }

    public static boolean isValidText(java.lang.CharSequence text) {
        return !isEmptyAfterTrim(text) && android.text.TextUtils.getTrimmedLength(text) > 0;
    }

    public static boolean isValidName(java.lang.CharSequence nameText) {
        if (!isValidText(nameText)) {
            return false;
        }
        return java.util.regex.Pattern.compile(".*\\p{L}+.*").matcher(nameText).matches();
    }

    public static boolean isValidEmail(java.lang.CharSequence emailText) {
        if (!isValidText(emailText)) {
            return false;
        }
        return java.util.regex.Pattern.compile("^([A-Za-z0-9.=!#$%&'*+\\-_~/^`|\\{}]+)@((\\[(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))\\])|(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))|([A-Za-z0-9\\-.]+\\.[A-Za-z\\-]+))$").matcher(emailText).matches();
    }

    public java.lang.String[] concatStringArrays(java.lang.String[] a, java.lang.String[] b) {
        int aLen = a.length;
        int bLen = b.length;
        java.lang.String[] c = new java.lang.String[(aLen + bLen)];
        java.lang.System.arraycopy(a, 0, c, 0, aLen);
        java.lang.System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }

    public static android.text.Spanned fromHtml(@android.support.annotation.StringRes int stringId) {
        return fromHtml(com.navdy.client.app.NavdyApplication.getAppContext().getString(stringId));
    }

    public static android.text.Spanned fromHtml(@android.support.annotation.StringRes int stringId, java.lang.Object... param) {
        return fromHtml(com.navdy.client.app.NavdyApplication.getAppContext().getString(stringId, param));
    }

    public static android.text.Spanned fromHtml(java.lang.String string) {
        if (string == null) {
            return null;
        }
        if (isEmptyAfterTrim(string)) {
            return new android.text.SpannedString(string);
        }
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            return android.text.Html.fromHtml(string, 0);
        }
        return android.text.Html.fromHtml(string);
    }

    @android.support.annotation.NonNull
    public static java.lang.String replaceSafely(@android.support.annotation.NonNull java.lang.String line, @android.support.annotation.Nullable java.lang.String lookFor, @android.support.annotation.Nullable java.lang.String replaceWith) {
        return (lookFor == null || replaceWith == null) ? line : line.replace(lookFor, replaceWith);
    }

    @android.support.annotation.NonNull
    public static java.lang.String replaceOrErase(@android.support.annotation.NonNull java.lang.String line, @android.support.annotation.Nullable java.lang.String lookFor, @android.support.annotation.Nullable java.lang.String replaceWith) {
        if (lookFor == null) {
            return line;
        }
        if (replaceWith == null) {
            replaceWith = "";
        }
        java.lang.String replaced = line.replace(lookFor, replaceWith);
        return !isEmptyAfterTrim(replaced) ? replaced.trim() : replaced;
    }

    @android.support.annotation.NonNull
    public static java.lang.String replaceAllSafely(@android.support.annotation.NonNull java.lang.String line, @android.support.annotation.Nullable java.lang.String lookFor, @android.support.annotation.Nullable java.lang.String replaceWith) {
        return (lookFor == null || replaceWith == null) ? line : line.replaceAll(lookFor, replaceWith);
    }

    public static boolean containsAfterTrim(@android.support.annotation.Nullable java.lang.String source, @android.support.annotation.Nullable java.lang.String toBeFound) {
        if (isEmptyAfterTrim(source) || isEmptyAfterTrim(toBeFound)) {
            return false;
        }
        return source.contains(toBeFound);
    }
}
