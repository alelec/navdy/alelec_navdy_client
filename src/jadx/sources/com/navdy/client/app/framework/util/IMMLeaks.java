package com.navdy.client.app.framework.util;

public class IMMLeaks {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.IMMLeaks.class);

    static class Anon1 implements android.app.Application.ActivityLifecycleCallbacks {
        Anon1() {
        }

        public void onActivityCreated(android.app.Activity activity, android.os.Bundle savedInstanceState) {
        }

        public void onActivityStarted(android.app.Activity activity) {
        }

        public void onActivityResumed(android.app.Activity activity) {
        }

        public void onActivityPaused(android.app.Activity activity) {
        }

        public void onActivityStopped(android.app.Activity activity) {
        }

        public void onActivitySaveInstanceState(android.app.Activity activity, android.os.Bundle outState) {
        }

        public void onActivityDestroyed(android.app.Activity activity) {
            try {
                if (activity instanceof com.navdy.client.app.ui.homescreen.HomescreenActivity) {
                    android.view.inputmethod.InputMethodManager inputMethodManager = (android.view.inputmethod.InputMethodManager) com.navdy.client.app.NavdyApplication.getAppContext().getSystemService("input_method");
                    java.lang.reflect.Method finishInputLockedMethod = android.view.inputmethod.InputMethodManager.class.getDeclaredMethod("finishInputLocked", new java.lang.Class[0]);
                    finishInputLockedMethod.setAccessible(true);
                    com.navdy.client.app.framework.util.IMMLeaks.sLogger.v("calling finishInputLocked");
                    finishInputLockedMethod.invoke(inputMethodManager, new java.lang.Object[0]);
                    com.navdy.client.app.framework.util.IMMLeaks.sLogger.v("called finishInputLocked");
                }
            } catch (Throwable t) {
                com.navdy.client.app.framework.util.IMMLeaks.sLogger.e(t);
            }
        }
    }

    public static void fixFocusedViewLeak(android.app.Application application) {
        try {
            application.registerActivityLifecycleCallbacks(new com.navdy.client.app.framework.util.IMMLeaks.Anon1());
            sLogger.v("IMM installed");
        } catch (Throwable t) {
            sLogger.e("IMMLeaks", t);
        }
    }
}
