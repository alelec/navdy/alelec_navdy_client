package com.navdy.client.app.framework.callcontrol;

public class TelephonySupport21HTC10 extends com.navdy.client.app.framework.callcontrol.TelephonySupport21 {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.callcontrol.TelephonySupport21HTC10.class);

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            try {
                java.lang.Runtime.getRuntime().exec("input keyevent 79");
            } catch (java.io.IOException e) {
                com.navdy.client.app.framework.callcontrol.TelephonySupport21HTC10.sLogger.e("Exception while accepting call on HTC10");
            }
        }
    }

    public TelephonySupport21HTC10(android.content.Context context) {
        super(context);
    }

    public void acceptRingingCall() {
        super.acceptRingingCall();
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.callcontrol.TelephonySupport21HTC10.Anon1(), 1);
    }
}
