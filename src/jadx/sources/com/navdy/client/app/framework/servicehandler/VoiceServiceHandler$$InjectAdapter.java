package com.navdy.client.app.framework.servicehandler;

public final class VoiceServiceHandler$$InjectAdapter extends dagger.internal.Binding<com.navdy.client.app.framework.servicehandler.VoiceServiceHandler> implements dagger.MembersInjector<com.navdy.client.app.framework.servicehandler.VoiceServiceHandler> {
    private dagger.internal.Binding<com.navdy.client.app.framework.util.TTSAudioRouter> mAudioRouter;

    public VoiceServiceHandler$$InjectAdapter() {
        super(null, "members/com.navdy.client.app.framework.servicehandler.VoiceServiceHandler", false, com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mAudioRouter = linker.requestBinding("com.navdy.client.app.framework.util.TTSAudioRouter", com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mAudioRouter);
    }

    public void injectMembers(com.navdy.client.app.framework.servicehandler.VoiceServiceHandler object) {
        object.mAudioRouter = (com.navdy.client.app.framework.util.TTSAudioRouter) this.mAudioRouter.get();
    }
}
