package com.navdy.client.app.framework.callcontrol;

public class TelephonySupport21_LG_G3 extends com.navdy.client.app.framework.callcontrol.TelephonySupport21 {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.callcontrol.TelephonySupport21_LG_G3.class);

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ android.media.session.MediaController val$m;

        Anon1(android.media.session.MediaController mediaController) {
            this.val$m = mediaController;
        }

        public void run() {
            try {
                this.val$m.dispatchMediaButtonEvent(new android.view.KeyEvent(0, 79));
                com.navdy.client.app.framework.callcontrol.TelephonySupport21_LG_G3.sLogger.i("HEADSETHOOK keydown sent:" + this.val$m.getPackageName());
                this.val$m.dispatchMediaButtonEvent(new android.view.KeyEvent(1, 79));
                com.navdy.client.app.framework.callcontrol.TelephonySupport21_LG_G3.sLogger.i("HEADSETHOOK keyup sent:" + this.val$m.getPackageName());
            } catch (Throwable t) {
                com.navdy.client.app.framework.callcontrol.TelephonySupport21_LG_G3.sLogger.e(t);
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ android.media.session.MediaController val$m;

        Anon2(android.media.session.MediaController mediaController) {
            this.val$m = mediaController;
        }

        public void run() {
            try {
                this.val$m.dispatchMediaButtonEvent(new android.view.KeyEvent(0, 79));
                com.navdy.client.app.framework.callcontrol.TelephonySupport21_LG_G3.sLogger.i("HEADSETHOOK keydown sent:" + this.val$m.getPackageName());
                com.navdy.client.app.framework.util.SystemUtils.sleep(1500);
                this.val$m.dispatchMediaButtonEvent(new android.view.KeyEvent(1, 79));
                com.navdy.client.app.framework.callcontrol.TelephonySupport21_LG_G3.sLogger.i("HEADSETHOOK keyup sent:" + this.val$m.getPackageName());
            } catch (Throwable t) {
                com.navdy.client.app.framework.callcontrol.TelephonySupport21_LG_G3.sLogger.e(t);
            }
        }
    }

    public TelephonySupport21_LG_G3(android.content.Context context) {
        super(context);
    }

    @android.annotation.TargetApi(21)
    protected void accept(android.media.session.MediaController m) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.callcontrol.TelephonySupport21_LG_G3.Anon1(m), 4);
    }

    @android.annotation.TargetApi(21)
    protected void reject(android.media.session.MediaController m) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.callcontrol.TelephonySupport21_LG_G3.Anon2(m), 4);
    }

    protected void end(android.media.session.MediaController m) {
        accept(m);
    }
}
