package com.navdy.client.app.framework.util;

public class TTSAudioRouter extends android.content.BroadcastReceiver implements android.speech.tts.TextToSpeech.OnInitListener, android.bluetooth.BluetoothProfile.ServiceListener {
    public static final int A2DP_DELAY_FOR_ALERT = 200;
    private static final int CHECK_TTS_FINISHED_DELAY = 100;
    public static final java.lang.String EXTRA_VOLUME_STREAM_TYPE = "android.media.EXTRA_VOLUME_STREAM_TYPE";
    public static final java.lang.String EXTRA_VOLUME_STREAM_VALUE = "android.media.EXTRA_VOLUME_STREAM_VALUE";
    public static final java.lang.String GOOGLE_TTS_ENGINE = "com.google.android.tts";
    private static final long HFP_CONNECTION_MAX_WAIT_TIME = 5000;
    private static final long HFP_RETRY_AFTER_FAILED_ATTEMPT = java.util.concurrent.TimeUnit.MINUTES.toMillis(5);
    public static final int MAXIMUM_PLAY_AUDIO_REQUEST_DELAY = 3000;
    public static final int MEASURED_DELAY_DEFAULT = 500;
    private static final int MESSAGE_CHECK_SCO = 3;
    private static final int MESSAGE_CLEANUP_AFTER_AUDIO = 9;
    private static final int MESSAGE_NEW_AUDIO_REQUEST = 8;
    private static final int MESSAGE_NEW_MESSAGE = 1;
    private static final int MESSAGE_SCO_CONNECTION_ATTEMPT_TIMEOUT = 4;
    private static final int MESSAGE_SCO_CONNECTION_FAILURE_RESET = 5;
    private static final int MESSAGE_SCO_STATE_CHANGED = 0;
    private static final int MESSAGE_START_VOICE_SEARCH_SESSION = 6;
    private static final int MESSAGE_STOP_VOICE_SEARCH_SESSION = 7;
    private static final int MESSAGE_TTS_FINISHED = 2;
    public static final int MINIMUM_HFP_DELAY = 500;
    public static final int SPEECH_DELAY_LEVELS = 6;
    public static final java.lang.String SPEECH_DELAY_SEQUENCE_ID_PREFIX = "NavdyTTS_SPEECH_DELAY";
    public static final java.lang.String SPEECH_DELAY_SEQUENCE_PREFIX = "SPEECH_DELAY";
    public static final int STREAM_BLUETOOTH_SCO = 6;
    public static final int STREAM_MUSIC = 3;
    public static final int STREAM_SPEAKER = 4;
    public static final int TTS_OVER_SPEAKER_DELAY = 1000;
    private static final java.lang.String UTTERANCE_ID = "NavdyTTS_";
    public static final java.lang.String VOLUME_CHANGED_ACTION = "android.media.VOLUME_CHANGED_ACTION";
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.TTSAudioRouter.class);
    private android.bluetooth.BluetoothA2dp a2dpProxy;
    private com.navdy.client.app.framework.util.TTSAudioRouter.TTSAudioStatus audioStatus = new com.navdy.client.app.framework.util.TTSAudioRouter.TTSAudioStatus();
    com.squareup.otto.Bus bus = com.navdy.client.app.framework.util.BusProvider.getInstance();
    private android.bluetooth.BluetoothHeadset hfpProxy;
    private boolean isVoiceSearchSessionInitializing = false;
    private boolean isVoiceSearchSessionStarted = false;
    private boolean mA2dpTurnedOff = false;
    private android.media.AudioManager mAudioManager;
    private volatile com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo mCurrent;
    private android.os.HandlerThread mHandlerThread = new android.os.HandlerThread("NavdyTTSThread");
    private long mLastHfpConnectionAttemptedTime;
    private java.util.concurrent.ConcurrentLinkedQueue<com.navdy.service.library.events.audio.PlayAudioRequest> mPlayAudioRequests = new java.util.concurrent.ConcurrentLinkedQueue<>();
    java.util.concurrent.atomic.AtomicBoolean mPlayThroughBluetooth = new java.util.concurrent.atomic.AtomicBoolean(false);
    java.util.concurrent.atomic.AtomicBoolean mPlayThroughHFP = new java.util.concurrent.atomic.AtomicBoolean(false);
    private com.navdy.client.app.framework.util.TTSAudioRouter.MessageHandler mRequestHandler;
    private volatile boolean mScoConnected = false;
    boolean mSpeechAvailable = false;
    private java.util.concurrent.ConcurrentHashMap<java.lang.String, com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo> mSpeechIds = new java.util.concurrent.ConcurrentHashMap<>();
    private java.util.concurrent.ConcurrentLinkedQueue<com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo> mSpeechRequests = new java.util.concurrent.ConcurrentLinkedQueue<>();
    protected android.speech.tts.TextToSpeech mTextToSpeech;
    private float mTtsVolume;
    private boolean mTurnOffSCOCalledAfterFailure = false;
    private java.util.concurrent.atomic.AtomicBoolean mTurnedScoOn = new java.util.concurrent.atomic.AtomicBoolean(false);
    private volatile boolean mWaitingForScoConnection;
    private int[] measuredTTSSequenceTimings;
    private int musicChannelAlertSoundId = -1;
    private android.media.SoundPool musicChannelSoundPool;
    private int[] newMeasuredSequenceTimings;
    private int notificationChannelAlertSoundId = -1;
    private android.media.SoundPool notificationSoundPool;
    private boolean playingSpeechDelaySequence;
    @javax.inject.Inject
    android.content.SharedPreferences preferences;
    private int preferredHFPDelay;
    private boolean scoInitialized = false;
    private com.navdy.client.app.framework.servicehandler.SpeechServiceHandler speechServiceHandler;
    private long utteranceStartTime = 0;
    private com.navdy.client.app.framework.util.TTSAudioRouter.VoiceSearchSetupListener voiceSearchSetupListener;
    private java.util.HashMap<java.lang.Integer, java.lang.Integer> volumeLevels = new java.util.HashMap<>();

    public enum AudioOutput {
        BEST_AVAILABLE,
        BLUETOOTH_MEDIA,
        PHONE_SPEAKER
    }

    private static class MessageHandler extends android.os.Handler {
        java.lang.ref.WeakReference<com.navdy.client.app.framework.util.TTSAudioRouter> mAudioRouterReference;

        MessageHandler(com.navdy.client.app.framework.util.TTSAudioRouter router, android.os.Looper looper) {
            super(looper);
            this.mAudioRouterReference = new java.lang.ref.WeakReference<>(router);
        }

        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);
            com.navdy.client.app.framework.util.TTSAudioRouter router = (com.navdy.client.app.framework.util.TTSAudioRouter) this.mAudioRouterReference.get();
            if (router != null) {
                router.handleMessage(msg.what);
            } else {
                com.navdy.client.app.framework.util.TTSAudioRouter.sLogger.e("TTS router messages received after router is GCed ");
            }
        }
    }

    private class NavdyUtteranceProgressListener extends android.speech.tts.UtteranceProgressListener {
        private NavdyUtteranceProgressListener() {
        }

        public void onStart(java.lang.String utteranceId) {
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(utteranceId) && utteranceId.startsWith(com.navdy.client.app.framework.util.TTSAudioRouter.UTTERANCE_ID)) {
                com.navdy.client.app.framework.util.TTSAudioRouter.sLogger.v("onStart:" + utteranceId);
                if (utteranceId.startsWith(com.navdy.client.app.framework.util.TTSAudioRouter.SPEECH_DELAY_SEQUENCE_ID_PREFIX)) {
                    com.navdy.client.app.framework.util.TTSAudioRouter.this.utteranceStartTime = java.lang.System.currentTimeMillis();
                }
                com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo info = (com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo) com.navdy.client.app.framework.util.TTSAudioRouter.this.mSpeechIds.get(utteranceId);
                if (info != null && info.sendNotification) {
                    com.navdy.client.app.framework.util.TTSAudioRouter.this.speechServiceHandler.sendSpeechNotification(new com.navdy.service.library.events.audio.SpeechRequestStatus(info.originalId, com.navdy.service.library.events.audio.SpeechRequestStatus.SpeechRequestStatusType.SPEECH_REQUEST_STARTING));
                }
            }
        }

        public void onStop(java.lang.String utteranceId, boolean interrupted) {
            super.onStop(utteranceId, interrupted);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(utteranceId) && utteranceId.startsWith(com.navdy.client.app.framework.util.TTSAudioRouter.UTTERANCE_ID)) {
                com.navdy.client.app.framework.util.TTSAudioRouter.this.handleTTSFinished(false, utteranceId);
            }
        }

        public void onDone(java.lang.String utteranceId) {
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(utteranceId) && utteranceId.startsWith(com.navdy.client.app.framework.util.TTSAudioRouter.UTTERANCE_ID)) {
                com.navdy.client.app.framework.util.TTSAudioRouter.this.handleTTSFinished(true, utteranceId);
            }
        }

        public void onError(java.lang.String utteranceId) {
            if (utteranceId != null && utteranceId.startsWith(com.navdy.client.app.framework.util.TTSAudioRouter.UTTERANCE_ID)) {
                com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo info = (com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo) com.navdy.client.app.framework.util.TTSAudioRouter.this.mSpeechIds.remove(utteranceId);
                com.navdy.client.app.framework.util.TTSAudioRouter.this.mCurrent = null;
                com.navdy.client.app.framework.util.TTSAudioRouter.sLogger.v("onError removed:" + utteranceId + " :" + (info != null ? "true" : "false"));
            }
            com.navdy.client.app.framework.util.TTSAudioRouter.this.mRequestHandler.sendEmptyMessage(2);
        }
    }

    public static class TTSAudioStatus {
        public int currentStreamMaxVolume;
        public int currentStreamVolume;
        public java.lang.String outputDeviceName;
        public boolean playingTTS;
        public int streamType;
        public boolean throughBluetooth;
        public boolean throughHFp;
        public boolean waitingForHfp;

        public com.navdy.client.app.framework.util.TTSAudioRouter.TTSAudioStatus copy() {
            com.navdy.client.app.framework.util.TTSAudioRouter.TTSAudioStatus ttsAudioStatus = new com.navdy.client.app.framework.util.TTSAudioRouter.TTSAudioStatus();
            ttsAudioStatus.playingTTS = this.playingTTS;
            ttsAudioStatus.currentStreamVolume = this.currentStreamVolume;
            ttsAudioStatus.currentStreamMaxVolume = this.currentStreamMaxVolume;
            ttsAudioStatus.throughBluetooth = this.throughBluetooth;
            ttsAudioStatus.throughHFp = this.throughHFp;
            ttsAudioStatus.outputDeviceName = this.outputDeviceName;
            ttsAudioStatus.waitingForHfp = this.waitingForHfp;
            ttsAudioStatus.streamType = this.streamType;
            return ttsAudioStatus;
        }

        public boolean reset() {
            boolean wasPlayingTTS = this.playingTTS;
            this.playingTTS = false;
            this.waitingForHfp = false;
            this.currentStreamVolume = 0;
            this.currentStreamMaxVolume = 0;
            this.throughBluetooth = false;
            this.throughHFp = false;
            this.outputDeviceName = "";
            this.streamType = -1;
            return wasPlayingTTS;
        }
    }

    private static class TTSInfo {
        public java.lang.String id;
        java.lang.String originalId;
        boolean sendNotification;
        int subTextCount = 1;
        public java.lang.String text;

        TTSInfo(java.lang.String text2, java.lang.String id2, java.lang.String originalId2, boolean sendNotification2) {
            this.text = text2;
            this.id = id2;
            this.originalId = originalId2;
            this.sendNotification = sendNotification2;
        }
    }

    private static class TTSVolumeChanged {
        private TTSVolumeChanged() {
        }
    }

    public interface VoiceSearchSetupListener {
        void onAudioInputReady(boolean z, boolean z2);
    }

    public void setTtsVolume(float ttsVolume) {
        this.mTtsVolume = ttsVolume;
    }

    private void setPlayThroughBluetooth(boolean playThroughBluetooth) {
        this.mPlayThroughBluetooth.set(playThroughBluetooth);
        this.mWaitingForScoConnection = false;
    }

    private void setPlayThroughHFP(boolean playThroughHFP) {
        this.mPlayThroughHFP.set(playThroughHFP);
        this.mWaitingForScoConnection = false;
    }

    public void setAudioOutput(com.navdy.client.app.framework.util.TTSAudioRouter.AudioOutput audioOutput, boolean measureVolume) {
        sLogger.d("Set Audio output " + audioOutput);
        if (audioOutput != null) {
            switch (audioOutput) {
                case BEST_AVAILABLE:
                    setPlayThroughBluetooth(true);
                    setPlayThroughHFP(true);
                    break;
                case BLUETOOTH_MEDIA:
                    setPlayThroughBluetooth(true);
                    setPlayThroughHFP(false);
                    break;
                case PHONE_SPEAKER:
                    setPlayThroughBluetooth(false);
                    setPlayThroughHFP(false);
                    break;
            }
            if (measureVolume) {
                int stream = getStream();
                sLogger.d("Stream selected " + stream);
                if (stream == 6 || (stream == 4 && isBluetoothA2dpOn())) {
                    playSilenceToMeasureVolume();
                }
            }
        }
    }

    public void onReceive(android.content.Context context, android.content.Intent intent) {
        java.lang.String action = intent.getAction();
        android.os.Bundle extras = intent.getExtras();
        char c = 65535;
        switch (action.hashCode()) {
            case -1940635523:
                if (action.equals(VOLUME_CHANGED_ACTION)) {
                    c = 1;
                    break;
                }
                break;
            case -1692127708:
                if (action.equals("android.media.ACTION_SCO_AUDIO_STATE_UPDATED")) {
                    c = 0;
                    break;
                }
                break;
            case 545516589:
                if (action.equals("android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED")) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                handleScoAudioStateChangedIntent(intent);
                return;
            case 1:
                handleVolumeChangedIntent(intent);
                return;
            case 2:
                sLogger.d("BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED");
                if (extras != null) {
                    int previousState = extras.getInt("android.bluetooth.profile.extra.PREVIOUS_STATE");
                    int currentState = extras.getInt("android.bluetooth.profile.extra.STATE");
                    android.bluetooth.BluetoothDevice device = (android.bluetooth.BluetoothDevice) extras.getParcelable("android.bluetooth.device.extra.DEVICE");
                    sLogger.d("BluetoothHeadset State changed Previous state :" + previousState + " , Current State : " + currentState + " , Device : " + (device != null ? device.getName() : "NONE"));
                    if ((previousState == 2 || previousState == 3) && currentState == 0) {
                        sLogger.d("HFP device disconnected");
                        com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
                        if (remoteDevice != null) {
                            remoteDevice.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.hudcontrol.AccelerateShutdown.Builder().reason(com.navdy.service.library.events.hudcontrol.AccelerateShutdown.AccelerateReason.ACCELERATE_REASON_HFP_DISCONNECT).build());
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void handleVolumeChangedIntent(android.content.Intent intent) {
        if (intent != null) {
            int streamType = intent.getIntExtra(EXTRA_VOLUME_STREAM_TYPE, -1);
            if (streamType == 6 || streamType == 3 || streamType == 0) {
                int streamVolume = intent.getIntExtra(EXTRA_VOLUME_STREAM_VALUE, -1);
                if (streamVolume >= 0) {
                    int localStreamType = streamType;
                    if (streamType != 3) {
                        this.volumeLevels.put(java.lang.Integer.valueOf(streamType), java.lang.Integer.valueOf(streamVolume));
                    } else if (isBluetoothA2dpOn()) {
                        sLogger.d("A2dp is on , A2dp Speaker connected " + isA2DPSpeakerConnected());
                        this.volumeLevels.put(java.lang.Integer.valueOf(3), java.lang.Integer.valueOf(streamVolume));
                        localStreamType = 3;
                    } else {
                        sLogger.d("A2dp is off , A2dp Speaker connected " + isA2DPSpeakerConnected());
                        this.volumeLevels.put(java.lang.Integer.valueOf(4), java.lang.Integer.valueOf(streamVolume));
                        localStreamType = 4;
                    }
                    if (this.audioStatus.streamType == localStreamType) {
                        this.audioStatus.currentStreamVolume = streamVolume;
                        publishAudioStatus();
                    }
                    this.bus.post(new com.navdy.client.app.framework.util.TTSAudioRouter.TTSVolumeChanged());
                }
            }
        }
    }

    private void handleScoAudioStateChangedIntent(android.content.Intent intent) {
        boolean z = true;
        if (intent.hasExtra("android.media.extra.SCO_AUDIO_STATE")) {
            int state = intent.getIntExtra("android.media.extra.SCO_AUDIO_STATE", 0);
            sLogger.d("Audio SCO state " + state);
            if (state == 0) {
                this.mTurnedScoOn.set(false);
            }
            if (this.mRequestHandler != null) {
                this.mRequestHandler.sendEmptyMessage(0);
            }
            if (state != 1) {
                z = false;
            }
            this.mScoConnected = z;
        }
    }

    public void init() {
        com.navdy.client.app.framework.Injector.inject(com.navdy.client.app.NavdyApplication.getAppContext(), this);
        this.mAudioManager = (android.media.AudioManager) com.navdy.client.app.NavdyApplication.getAppContext().getSystemService("audio");
        android.bluetooth.BluetoothAdapter.getDefaultAdapter().getProfileProxy(com.navdy.client.app.NavdyApplication.getAppContext(), this, 2);
        android.bluetooth.BluetoothAdapter.getDefaultAdapter().getProfileProxy(com.navdy.client.app.NavdyApplication.getAppContext(), this, 1);
        this.bus.register(this);
        this.musicChannelSoundPool = new android.media.SoundPool.Builder().setMaxStreams(1).setAudioAttributes(new android.media.AudioAttributes.Builder().setContentType(4).setUsage(12).setLegacyStreamType(3).build()).build();
        this.notificationSoundPool = new android.media.SoundPool.Builder().setMaxStreams(1).setAudioAttributes(new android.media.AudioAttributes.Builder().setContentType(4).setUsage(12).setLegacyStreamType(5).build()).build();
        try {
            this.musicChannelAlertSoundId = this.musicChannelSoundPool.load(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.raw.driving_behavior_alert, 1);
        } catch (java.lang.RuntimeException re) {
            sLogger.e("RuntimeException loading the sound ", re);
        }
        try {
            this.notificationChannelAlertSoundId = this.notificationSoundPool.load(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.raw.driving_behavior_alert, 1);
        } catch (java.lang.RuntimeException re2) {
            sLogger.e("RuntimeException loading the sound ", re2);
        }
        this.mTextToSpeech = new android.speech.tts.TextToSpeech(com.navdy.client.app.NavdyApplication.getAppContext(), this, GOOGLE_TTS_ENGINE);
        this.mTextToSpeech.setOnUtteranceProgressListener(new com.navdy.client.app.framework.util.TTSAudioRouter.NavdyUtteranceProgressListener());
        this.mHandlerThread.start();
        this.mRequestHandler = new com.navdy.client.app.framework.util.TTSAudioRouter.MessageHandler(this, this.mHandlerThread.getLooper());
    }

    public void close() {
        if (this.mTextToSpeech != null) {
            this.mTextToSpeech.shutdown();
            this.mTextToSpeech = null;
            this.mSpeechAvailable = false;
        }
        this.mSpeechIds.clear();
        this.mSpeechRequests.clear();
        this.mA2dpTurnedOff = false;
        com.navdy.client.app.NavdyApplication.getAppContext().unregisterReceiver(this);
    }

    private void persistNewMeasuredSequenceTimings() {
        for (int i = 0; i < 6; i++) {
            sLogger.d(i + " Old timing " + this.measuredTTSSequenceTimings[i] + " , New timing " + this.newMeasuredSequenceTimings[i]);
            this.measuredTTSSequenceTimings[i] = this.newMeasuredSequenceTimings[i];
        }
        android.content.SharedPreferences.Editor edit = this.preferences.edit();
        for (int i2 = 0; i2 < 6; i2++) {
            edit.putInt(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_HFP_DELAY_MEASURED + i2, this.newMeasuredSequenceTimings[i2]);
        }
        edit.apply();
    }

    public void resetToUserPreference() {
        setAudioOutput(com.navdy.client.app.framework.util.TTSAudioRouter.AudioOutput.values()[this.preferences.getInt(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_OUTPUT_PREFERENCE, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_OUTPUT_PREFERENCE_DEFAULT)], false);
        this.measuredTTSSequenceTimings = new int[6];
        for (int i = 0; i < 6; i++) {
            this.measuredTTSSequenceTimings[i] = this.preferences.getInt(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_HFP_DELAY_MEASURED + i, 500);
        }
        int preferredHFPDelayLevel = this.preferences.getInt(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_HFP_DELAY_LEVEL, 1);
        if (preferredHFPDelayLevel < 0 || preferredHFPDelayLevel > 6) {
            this.preferences.edit().putInt(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_HFP_DELAY_LEVEL, 1).apply();
        }
        setPreferredHFPDelayLevel(preferredHFPDelayLevel);
        this.mTtsVolume = this.preferences.getFloat(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_TTS_VOLUME, 0.6f);
        java.lang.String savedVoice = this.preferences.getString(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_VOICE, null);
        sLogger.d("Saved Voice : " + savedVoice);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            try {
                android.speech.tts.Voice selectedVoice = this.mTextToSpeech.getVoice();
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(savedVoice)) {
                    java.util.Set<android.speech.tts.Voice> voices = this.mTextToSpeech.getVoices();
                    if (voices != null) {
                        java.util.Iterator it = voices.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            android.speech.tts.Voice temp = (android.speech.tts.Voice) it.next();
                            if (temp.getName().equals(savedVoice)) {
                                selectedVoice = temp;
                                break;
                            }
                        }
                    } else {
                        sLogger.d("Voices not available");
                    }
                }
                this.mTextToSpeech.setVoice(selectedVoice);
            } catch (java.lang.Exception e) {
                sLogger.e("Text To Speech exception found on: " + android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL + ", error: " + e);
            }
        }
    }

    public void processTTSRequest(java.lang.String text, java.lang.String id, boolean sendNotification) {
        sLogger.d("New TTS request " + text + " " + this.mSpeechAvailable);
        if (this.mSpeechAvailable) {
            if (!this.playingSpeechDelaySequence || (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(id) && id.startsWith(SPEECH_DELAY_SEQUENCE_PREFIX))) {
                this.mRequestHandler.removeMessages(2);
                java.lang.String originalId = id;
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(id)) {
                    id = UTTERANCE_ID + id;
                }
                com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo info = new com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo(text, id, originalId, sendNotification);
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(id)) {
                    this.mSpeechIds.put(id, info);
                    sLogger.v("added id [" + id + "]");
                }
                this.mSpeechRequests.add(info);
                this.mRequestHandler.sendEmptyMessage(1);
            }
        }
    }

    public void cancelTTSRequest(java.lang.String id) {
        sLogger.d("Cancel TTS request " + id + " " + this.mSpeechAvailable);
        if (id != null && this.mSpeechAvailable) {
            java.lang.String id2 = UTTERANCE_ID + id;
            com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo info = (com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo) this.mSpeechIds.remove(id2);
            if (info != null) {
                boolean ret = this.mSpeechRequests.remove(info);
                if (ret || info != this.mCurrent) {
                    sLogger.v("id still exists [" + id2 + "], removed=" + ret);
                    return;
                }
                sLogger.v("stopping current text to speech");
                this.mTextToSpeech.stop();
                return;
            }
            sLogger.v("id does not exists [" + id2 + "]");
        }
    }

    public void processPlayAudioRequest(com.navdy.service.library.events.audio.PlayAudioRequest playAudioRequest) {
        this.mPlayAudioRequests.add(playAudioRequest);
        this.mRequestHandler.sendEmptyMessage(8);
    }

    public void cancelTtsAndClearQueue() {
        this.mTextToSpeech.stop();
        this.mSpeechIds.clear();
    }

    public boolean isTTSAvailable() {
        return this.mSpeechAvailable;
    }

    public void onInit(int status) {
        this.volumeLevels.put(java.lang.Integer.valueOf(6), java.lang.Integer.valueOf(this.mAudioManager.getStreamVolume(6)));
        this.volumeLevels.put(java.lang.Integer.valueOf(3), java.lang.Integer.valueOf(this.mAudioManager.getStreamVolume(3)));
        this.volumeLevels.put(java.lang.Integer.valueOf(4), java.lang.Integer.valueOf(this.mAudioManager.getStreamVolume(3)));
        resetToUserPreference();
        this.mSpeechAvailable = true;
        android.content.IntentFilter filter = new android.content.IntentFilter();
        filter.addAction("android.media.ACTION_SCO_AUDIO_STATE_UPDATED");
        filter.addAction(VOLUME_CHANGED_ACTION);
        filter.addAction("android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED");
        com.navdy.client.app.NavdyApplication.getAppContext().registerReceiver(this, filter);
    }

    private void handleTTSFinished(boolean completed, java.lang.String utteranceId) {
        java.lang.String method = completed ? "onDone" : "onStop";
        if (utteranceId.startsWith(SPEECH_DELAY_SEQUENCE_ID_PREFIX)) {
            long currentTime = java.lang.System.currentTimeMillis();
            int timeTaken = (int) (currentTime - this.utteranceStartTime);
            sLogger.d("Time for Speech delay sequence utterance " + utteranceId + " " + (currentTime - this.utteranceStartTime));
            int index = java.lang.Integer.parseInt(utteranceId.substring(SPEECH_DELAY_SEQUENCE_ID_PREFIX.length()));
            this.newMeasuredSequenceTimings[index] = timeTaken;
            if (index == 5) {
                sLogger.d("Finished playing the HFP delay sequence");
                this.playingSpeechDelaySequence = false;
                persistNewMeasuredSequenceTimings();
            }
        }
        com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo info = (com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo) this.mSpeechIds.get(utteranceId);
        if (info != null) {
            if (info.subTextCount == 1) {
                this.mSpeechIds.remove(utteranceId);
                this.mCurrent = null;
                if (info.sendNotification) {
                    this.speechServiceHandler.sendSpeechNotification(new com.navdy.service.library.events.audio.SpeechRequestStatus(info.originalId, com.navdy.service.library.events.audio.SpeechRequestStatus.SpeechRequestStatusType.SPEECH_REQUEST_STOPPED));
                }
            } else {
                info.subTextCount--;
                sLogger.d(method + " called for multi part text , remaining pieces " + info.subTextCount);
            }
        }
        this.mRequestHandler.sendEmptyMessage(2);
    }

    public boolean isSpeaking() {
        return this.mTextToSpeech.isSpeaking();
    }

    public void handleMessage(int what) {
        switch (what) {
            case 0:
                sLogger.d("SCO state changed Bluetooth SCO on :" + this.mAudioManager.isBluetoothScoOn() + ", SCO connected :" + this.mScoConnected);
                if (!isScoConnected() || this.scoInitialized) {
                    if (!isScoConnected()) {
                        this.scoInitialized = false;
                        break;
                    }
                } else {
                    waitForScoInitialization();
                    break;
                }
                break;
            case 1:
                sLogger.d("New message");
                break;
            case 2:
                sLogger.d("Finished speaking");
                break;
            case 3:
                sLogger.d("Checking if SCO connection is closed, Connected : " + this.mAudioManager.isBluetoothScoOn());
                this.mAudioManager.abandonAudioFocus(null);
                break;
            case 4:
                sLogger.d("SCO connection attempt time out");
                break;
            case 5:
                sLogger.d("SCO connection failure reset");
                break;
            case 6:
                sLogger.d("Start voice search session");
                this.isVoiceSearchSessionStarted = true;
                break;
            case 7:
                sLogger.d("Stop voice search session");
                this.isVoiceSearchSessionInitializing = false;
                this.isVoiceSearchSessionStarted = false;
                break;
            case 8:
            case 9:
                com.navdy.service.library.events.audio.PlayAudioRequest playAudioRequest = (com.navdy.service.library.events.audio.PlayAudioRequest) this.mPlayAudioRequests.poll();
                if (playAudioRequest != null) {
                    if (java.lang.System.currentTimeMillis() - playAudioRequest.timeStamp.longValue() < com.here.odnp.config.OdnpConfigStatic.CELL_NO_CHANGE_LIMITER_TIME) {
                        if (!this.mWaitingForScoConnection || !isScoConnected()) {
                            boolean isMusicActive = this.mAudioManager.isMusicActive();
                            boolean isA2DpSpeakerConnected = isA2DPSpeakerConnected();
                            boolean isTTSSpeaking = this.mTextToSpeech.isSpeaking();
                            if (isMusicActive || isTTSSpeaking) {
                                this.musicChannelSoundPool.play(this.musicChannelAlertSoundId, 1.0f, 1.0f, 1, 0, 1.0f);
                            } else {
                                if (isA2DpSpeakerConnected && !this.mA2dpTurnedOff) {
                                    setBluetoothA2dpOn(false);
                                    try {
                                        java.lang.Thread.sleep(200);
                                    } catch (java.lang.InterruptedException e) {
                                    }
                                }
                                this.notificationSoundPool.play(this.notificationChannelAlertSoundId, 1.0f, 1.0f, 1, 0, 1.0f);
                                if (this.mA2dpTurnedOff) {
                                    this.mRequestHandler.sendEmptyMessageDelayed(9, 2000);
                                    return;
                                }
                                return;
                            }
                        }
                        this.mRequestHandler.sendEmptyMessage(8);
                        return;
                    }
                    sLogger.e("Message arrived too late");
                    return;
                } else if (what == 9 && this.mA2dpTurnedOff && !this.mTextToSpeech.isSpeaking() && this.mSpeechRequests.size() == 0) {
                    setBluetoothA2dpOn(true);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
        boolean mHFPConnectionFailed = false;
        if (this.mWaitingForScoConnection && !isScoConnected()) {
            long currentTime = android.os.SystemClock.elapsedRealtime();
            mHFPConnectionFailed = currentTime - this.mLastHfpConnectionAttemptedTime > 5000;
            if (mHFPConnectionFailed) {
                sLogger.d("HFP connection has failed, HFP setting will be ignored if set");
                this.mRequestHandler.removeMessages(4);
                if (!this.mTurnOffSCOCalledAfterFailure) {
                    com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Audio.TTS_HFP_FAILURE);
                    sLogger.d("Stopping HFP after connection failure");
                    this.mTurnOffSCOCalledAfterFailure = true;
                    this.mAudioManager.stopBluetoothSco();
                }
                if (currentTime - this.mLastHfpConnectionAttemptedTime > HFP_RETRY_AFTER_FAILED_ATTEMPT) {
                    sLogger.d("Resetting the HFP failure flag as its time to attempt to start the HFP again");
                    this.mTurnedScoOn.set(false);
                    this.mTurnOffSCOCalledAfterFailure = false;
                    this.mLastHfpConnectionAttemptedTime = 0;
                    mHFPConnectionFailed = false;
                    this.mWaitingForScoConnection = false;
                }
            }
        }
        boolean playThroughHFP = this.mPlayThroughHFP.get();
        boolean isHFPSpeakerConnected = isHFPSpeakerConnected();
        boolean playThroughBluetooth = this.mPlayThroughBluetooth.get();
        boolean isScoAvailableOffCall = this.mAudioManager.isBluetoothScoAvailableOffCall();
        if (what == 6 || this.isVoiceSearchSessionInitializing) {
            sLogger.d("Start Voice search session : playThroughHFP :" + playThroughHFP + ", isHFPSpeakerConnected :" + isHFPSpeakerConnected + ", isSCOAvailableOffCall:" + isScoAvailableOffCall + " , Waiting for SCO :" + this.mWaitingForScoConnection);
            if (!playThroughBluetooth || !isHFPSpeakerConnected || !isScoAvailableOffCall) {
                if (this.voiceSearchSetupListener != null) {
                    sLogger.d("Starting voice session over phone mic as we do not have to play audio through bluetooth");
                    sLogger.d("Audio Focus :" + (this.mAudioManager.requestAudioFocus(null, 3, 4) == 1 ? "Granted" : "Failed"));
                    this.voiceSearchSetupListener.onAudioInputReady(false, false);
                }
                this.isVoiceSearchSessionInitializing = false;
            } else {
                sLogger.d("We can use SCO for voice search session");
                if (isScoConnected()) {
                    sLogger.d("SCO connected, starting voice search session over bluetooth");
                    if (!this.scoInitialized) {
                        sLogger.d("Waiting for SCO initialization");
                        waitForScoInitialization();
                    }
                    this.mRequestHandler.removeMessages(4);
                    this.mWaitingForScoConnection = false;
                    this.isVoiceSearchSessionInitializing = false;
                    this.voiceSearchSetupListener.onAudioInputReady(true, false);
                } else {
                    sLogger.d("SCO is not connected");
                    if (mHFPConnectionFailed) {
                        sLogger.d("Use phone mic as HFP connection has failed");
                        if (this.voiceSearchSetupListener != null) {
                            sLogger.d("Audio Focus :" + (this.mAudioManager.requestAudioFocus(null, 3, 4) == 1 ? "Granted" : "Failed"));
                            this.voiceSearchSetupListener.onAudioInputReady(false, true);
                        }
                    } else if (startHFPIfNeeded()) {
                        this.isVoiceSearchSessionInitializing = true;
                    } else {
                        if (this.voiceSearchSetupListener != null) {
                            sLogger.d("Starting voice session over phone mic as we do not have to play audio through bluetooth");
                            sLogger.d("Audio Focus :" + (this.mAudioManager.requestAudioFocus(null, 3, 4) == 1 ? "Granted" : "Failed"));
                            this.voiceSearchSetupListener.onAudioInputReady(false, false);
                        }
                        this.isVoiceSearchSessionInitializing = false;
                    }
                }
            }
        }
        if (this.mTextToSpeech.isSpeaking()) {
            sLogger.d("Still speaking");
            if (what == 2) {
                sLogger.d("We received TTS finished message but TTS Engine thinks its still speaking");
                this.mRequestHandler.sendEmptyMessageDelayed(2, 100);
            }
        } else if (!this.mSpeechRequests.isEmpty()) {
            sLogger.d("Processing speech request : playThroughHFP :" + playThroughHFP + ", isHFPSpeakerConnected :" + isHFPSpeakerConnected + ", isSCOAvailableOffCall:" + isScoAvailableOffCall + " , Waiting for SCO :" + this.mWaitingForScoConnection);
            sLogger.d("Is Music active :" + this.mAudioManager.isMusicActive());
            sLogger.d("Is Voice search session running " + this.isVoiceSearchSessionStarted);
            if (!playThroughBluetooth || ((this.mAudioManager.isMusicActive() && !this.playingSpeechDelaySequence && !this.isVoiceSearchSessionStarted) || ((!playThroughHFP && !this.isVoiceSearchSessionStarted) || !isHFPSpeakerConnected || !isScoAvailableOffCall || mHFPConnectionFailed))) {
                if (!playThroughBluetooth || this.mAudioManager.isMusicActive() || !playThroughHFP || !isHFPSpeakerConnected || !isScoAvailableOffCall) {
                    this.mWaitingForScoConnection = false;
                }
                routeAudioThroughNonScoChannel(playThroughBluetooth);
            } else if (isScoConnected()) {
                sLogger.d("SCO is connected");
                if (!this.scoInitialized) {
                    waitForScoInitialization();
                }
                this.mRequestHandler.removeMessages(4);
                this.mWaitingForScoConnection = false;
                sLogger.d("Audio Focus :" + (this.mAudioManager.requestAudioFocus(null, 0, 3) == 1 ? "Granted" : "Failed"));
                com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo ttsInfo = (com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo) this.mSpeechRequests.poll();
                if (ttsInfo != null) {
                    this.mCurrent = ttsInfo;
                    routeTTSAudioToHFP(ttsInfo.text, ttsInfo.id);
                }
            } else if (!startHFPIfNeeded()) {
                routeAudioThroughNonScoChannel(true);
            }
        } else {
            sLogger.d("Queue is Empty");
            if (what == 2 && this.audioStatus.reset()) {
                publishAudioStatus();
            }
            sLogger.d("Bluetooth SCO On :" + this.mAudioManager.isBluetoothScoOn() + ", SCO connected :" + this.mScoConnected + ", Did we turn on the SCO :" + this.mTurnedScoOn.get() + ", Did we detect HFP connection failure :" + mHFPConnectionFailed);
            if (!mHFPConnectionFailed && this.mTurnedScoOn.get() && isScoConnected() && !this.isVoiceSearchSessionStarted) {
                sLogger.d("Stopping HFP");
                this.mAudioManager.stopBluetoothSco();
                this.mRequestHandler.sendEmptyMessageDelayed(3, 500);
            }
            if (this.mA2dpTurnedOff) {
                sLogger.d("Enabling A2dp again");
                setBluetoothA2dpOn(true);
            }
            if (!this.isVoiceSearchSessionStarted) {
                sLogger.d("Audio Focus abandoned : " + this.mAudioManager.abandonAudioFocus(null));
            }
        }
    }

    private void routeAudioThroughNonScoChannel(boolean playThroughBluetooth) {
        sLogger.d("Bluetooth SCO On :" + this.mAudioManager.isBluetoothScoOn() + ", SCO connected :" + this.mScoConnected + ", Did we turn on the SCO :" + this.mTurnedScoOn.get());
        if (this.mAudioManager.isBluetoothScoOn() && this.mTurnedScoOn.get() && !this.isVoiceSearchSessionStarted) {
            sLogger.d("Stopping HFP");
            this.mAudioManager.setBluetoothScoOn(false);
            this.mAudioManager.stopBluetoothSco();
            this.mRequestHandler.sendEmptyMessageDelayed(3, 500);
        }
        sLogger.d("Audio Focus :" + (this.mAudioManager.requestAudioFocus(null, 3, 3) == 1 ? "Granted" : "Failed"));
        com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo ttsInfo = (com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo) this.mSpeechRequests.poll();
        if (ttsInfo != null) {
            this.mCurrent = ttsInfo;
            if (playThroughBluetooth || !isA2DPSpeakerConnected()) {
                if (isA2DPSpeakerConnected() && !isBluetoothA2dpOn()) {
                    setBluetoothA2dpOn(true);
                }
                routeTTSAudioToDefaultOutput(ttsInfo.text, ttsInfo.id);
                return;
            }
            routeTTSAudioToSpeaker(ttsInfo.text, ttsInfo.id);
        }
    }

    private boolean startHFPIfNeeded() {
        try {
            if (!this.mTurnedScoOn.get() && !this.mWaitingForScoConnection) {
                sLogger.d("Starting HFP");
                this.audioStatus.waitingForHfp = true;
                this.audioStatus.throughBluetooth = true;
                this.audioStatus.outputDeviceName = getHfpDeviceName();
                this.audioStatus.currentStreamVolume = ((java.lang.Integer) this.volumeLevels.get(java.lang.Integer.valueOf(6))).intValue();
                this.audioStatus.currentStreamMaxVolume = this.mAudioManager.getStreamMaxVolume(6);
                publishAudioStatus();
                this.mWaitingForScoConnection = true;
                this.mLastHfpConnectionAttemptedTime = android.os.SystemClock.elapsedRealtime();
                this.mTurnedScoOn.set(true);
                this.mTurnOffSCOCalledAfterFailure = false;
                this.mAudioManager.setBluetoothScoOn(true);
                this.mAudioManager.startBluetoothSco();
                this.mRequestHandler.sendEmptyMessageDelayed(4, 5000);
            }
            return true;
        } catch (Throwable t) {
            sLogger.e("Exception while staring HFP ", t);
            this.mTurnedScoOn.set(false);
            this.mTurnOffSCOCalledAfterFailure = false;
            this.mLastHfpConnectionAttemptedTime = 0;
            this.mWaitingForScoConnection = false;
            return false;
        }
    }

    private void waitForScoInitialization() {
        sLogger.d("SCO connected, waiting for initialization");
        this.scoInitialized = true;
        try {
            java.lang.Thread.sleep(this.playingSpeechDelaySequence ? 500 : (long) this.preferredHFPDelay);
        } catch (java.lang.InterruptedException e) {
            sLogger.e("TTS through HFP, Sleep interrupted ", e);
        }
    }

    private void routeTTSAudioToHFP(java.lang.String text, java.lang.String id) {
        int scoVolume = this.mAudioManager.getStreamVolume(6);
        this.volumeLevels.put(java.lang.Integer.valueOf(6), java.lang.Integer.valueOf(scoVolume));
        int maxVolume = this.mAudioManager.getStreamMaxVolume(6);
        sLogger.d("Playing text through HFP , SCO volume (" + scoVolume + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + maxVolume + ")");
        this.audioStatus.currentStreamMaxVolume = maxVolume;
        this.audioStatus.waitingForHfp = false;
        this.audioStatus.currentStreamVolume = scoVolume;
        this.audioStatus.playingTTS = true;
        this.audioStatus.outputDeviceName = getHfpDeviceName();
        this.audioStatus.throughBluetooth = true;
        this.audioStatus.throughHFp = true;
        this.audioStatus.streamType = 6;
        publishAudioStatus();
        playTTS(0, text, id);
    }

    private void playTTS(int stream, java.lang.String text, java.lang.String id) {
        if (id == null) {
            id = UTTERANCE_ID;
        }
        java.util.HashMap<java.lang.String, java.lang.String> params = new java.util.HashMap<>();
        params.put("streamType", java.lang.String.valueOf(stream));
        params.put("utteranceId", id);
        params.put("volume", java.lang.Float.toString(this.mTtsVolume));
        int maxInputLength = android.speech.tts.TextToSpeech.getMaxSpeechInputLength();
        if (text.length() < maxInputLength) {
            this.mTextToSpeech.speak(text, 0, params);
            return;
        }
        java.util.List<java.lang.String> subTexts = splitLongSpeechText(text, maxInputLength);
        int count = subTexts.size();
        com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo ttsInfo = (com.navdy.client.app.framework.util.TTSAudioRouter.TTSInfo) this.mSpeechIds.get(id);
        if (ttsInfo != null) {
            ttsInfo.subTextCount = count;
        }
        sLogger.d("The text is broken into " + count + " pieces");
        boolean firstOne = true;
        for (java.lang.String subText : subTexts) {
            this.mTextToSpeech.speak(subText, firstOne ? 0 : 1, params);
            if (firstOne) {
                firstOne = false;
            }
        }
    }

    private void routeTTSAudioToSpeaker(java.lang.String text, java.lang.String id) {
        setBluetoothA2dpOn(false);
        try {
            java.lang.Thread.sleep(1000);
        } catch (java.lang.InterruptedException e) {
            e.printStackTrace();
        }
        int streamVolume = this.mAudioManager.getStreamVolume(3);
        this.volumeLevels.put(java.lang.Integer.valueOf(4), java.lang.Integer.valueOf(streamVolume));
        int streamMaxVolume = this.mAudioManager.getStreamMaxVolume(3);
        sLogger.d("Playing text through Speaker , Volume (" + streamVolume + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + streamMaxVolume + ")");
        this.audioStatus.currentStreamMaxVolume = streamMaxVolume;
        this.audioStatus.currentStreamVolume = streamVolume;
        this.audioStatus.waitingForHfp = false;
        this.audioStatus.playingTTS = true;
        this.audioStatus.outputDeviceName = com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.phone_speaker);
        this.audioStatus.throughBluetooth = false;
        this.audioStatus.streamType = 4;
        publishAudioStatus();
        playTTS(3, text, id);
    }

    private void routeTTSAudioToDefaultOutput(java.lang.String text, java.lang.String id) {
        playTTS(3, text, id);
        int streamVolume = this.mAudioManager.getStreamVolume(3);
        if (isBluetoothA2dpOn()) {
            this.volumeLevels.put(java.lang.Integer.valueOf(3), java.lang.Integer.valueOf(streamVolume));
            this.audioStatus.streamType = 3;
        } else {
            this.volumeLevels.put(java.lang.Integer.valueOf(4), java.lang.Integer.valueOf(streamVolume));
            this.audioStatus.streamType = 4;
        }
        int streamMaxVolume = this.mAudioManager.getStreamMaxVolume(3);
        sLogger.d("Playing text through default Music stream , Volume (" + streamVolume + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + streamMaxVolume + ")");
        this.audioStatus.currentStreamMaxVolume = streamMaxVolume;
        this.audioStatus.waitingForHfp = false;
        this.audioStatus.currentStreamVolume = streamVolume;
        this.audioStatus.playingTTS = true;
        this.audioStatus.outputDeviceName = isA2DPSpeakerConnected() ? getA2DPDevice() : com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.phone_speaker);
        this.audioStatus.throughBluetooth = isA2DPSpeakerConnected();
        publishAudioStatus();
    }

    public void setBluetoothA2dpOn(boolean on) {
        android.media.IAudioService service = getService();
        if (service != null) {
            try {
                service.setBluetoothA2dpOn(on);
                this.mA2dpTurnedOff = !on;
            } catch (android.os.RemoteException e) {
                sLogger.d("Exception while turning off a2dp", e);
            }
        } else {
            sLogger.e("Unable to get instance of audio service");
        }
    }

    public boolean isBluetoothA2dpOn() {
        if (!isA2DPSpeakerConnected()) {
            return false;
        }
        android.media.IAudioService service = getService();
        if (service != null) {
            try {
                return service.isBluetoothA2dpOn();
            } catch (android.os.RemoteException e) {
                sLogger.d("Exception while getting a2dp", e);
            }
        } else {
            sLogger.e("Unable to get instance of audio service");
            return isA2DPSpeakerConnected();
        }
    }

    public static boolean isHFPSpeakerConnected() {
        return android.bluetooth.BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(1) == 2;
    }

    public static boolean isA2DPSpeakerConnected() {
        return android.bluetooth.BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(2) == 2;
    }

    public java.lang.String getA2DPDevice() {
        if (isA2DPSpeakerConnected() && this.a2dpProxy != null) {
            java.util.List<android.bluetooth.BluetoothDevice> devices = this.a2dpProxy.getConnectedDevices();
            if (devices != null && devices.size() > 0) {
                return ((android.bluetooth.BluetoothDevice) devices.get(0)).getName();
            }
        }
        return "";
    }

    public java.lang.String getHfpDeviceName() {
        android.bluetooth.BluetoothDevice device = getHfpDevice();
        if (device != null) {
            return device.getName();
        }
        return "";
    }

    public android.bluetooth.BluetoothDevice getHfpDevice() {
        if (!isHFPSpeakerConnected() || this.hfpProxy == null) {
            return null;
        }
        java.util.List<android.bluetooth.BluetoothDevice> devices = this.hfpProxy.getConnectedDevices();
        if (devices == null || devices.size() == 0) {
            return null;
        }
        for (android.bluetooth.BluetoothDevice device : devices) {
            if (this.hfpProxy.isAudioConnected(device)) {
                return device;
            }
        }
        return (android.bluetooth.BluetoothDevice) devices.get(0);
    }

    public boolean isScoConnected() {
        return this.mAudioManager.isBluetoothScoOn() && this.mScoConnected;
    }

    public android.speech.tts.TextToSpeech getTextToSpeech() {
        return this.mTextToSpeech;
    }

    private android.media.IAudioService getService() {
        boolean z = false;
        try {
            return android.media.IAudioService.Stub.asInterface((android.os.IBinder) java.lang.Class.forName("android.os.ServiceManager").getMethod("getService", new java.lang.Class[]{java.lang.String.class}).invoke(null, new java.lang.Object[]{"audio"}));
        } catch (Throwable t) {
            sLogger.d("Error getting service ", t);
            return z;
        }
    }

    public void setSpeechServiceHadler(com.navdy.client.app.framework.servicehandler.SpeechServiceHandler speechServiceHandler2) {
        this.speechServiceHandler = speechServiceHandler2;
    }

    public static java.util.List<java.lang.String> splitLongSpeechText(java.lang.String speechText, int maxInputLength) {
        if (speechText == null || maxInputLength < 2) {
            return null;
        }
        int beginningIndex = 0;
        int textLength = speechText.length();
        java.util.ArrayList<java.lang.String> subTexts = new java.util.ArrayList<>();
        do {
            int endIndex = java.lang.Math.min((beginningIndex + maxInputLength) - 1, textLength - 1);
            if (endIndex < textLength - 1) {
                while (endIndex > beginningIndex + 1) {
                    if (java.lang.Character.isLowSurrogate(speechText.charAt(endIndex))) {
                        endIndex--;
                    }
                    if (java.lang.Character.isSpaceChar(java.lang.Character.codePointAt(speechText, endIndex))) {
                        break;
                    }
                    endIndex--;
                }
                subTexts.add(speechText.substring(beginningIndex, endIndex + 1));
                beginningIndex = endIndex + 1;
            } else {
                subTexts.add(speechText.substring(beginningIndex, endIndex + 1));
                beginningIndex = endIndex + 1;
            }
        } while (beginningIndex < textLength - 1);
        return subTexts;
    }

    public synchronized void playSpeechDelaySequence() {
        if (!this.playingSpeechDelaySequence) {
            this.newMeasuredSequenceTimings = new int[6];
            this.playingSpeechDelaySequence = true;
            cancelTtsAndClearQueue();
            for (int i = 0; i < 6; i++) {
                processTTSRequest(java.lang.Integer.toString(i + 1), SPEECH_DELAY_SEQUENCE_PREFIX + i, false);
            }
        }
    }

    public void onServiceConnected(int profile, android.bluetooth.BluetoothProfile bluetoothProfile) {
        switch (profile) {
            case 1:
                this.hfpProxy = (android.bluetooth.BluetoothHeadset) bluetoothProfile;
                return;
            case 2:
                this.a2dpProxy = (android.bluetooth.BluetoothA2dp) bluetoothProfile;
                return;
            default:
                return;
        }
    }

    public void onServiceDisconnected(int profile) {
        switch (profile) {
            case 1:
                this.hfpProxy = null;
                return;
            case 2:
                this.a2dpProxy = null;
                return;
            default:
                return;
        }
    }

    public int getDelayInMillisec(int level) {
        int delay = 0;
        if (isValidDelayLevel(level)) {
            for (int i = 0; i < level; i++) {
                delay += java.lang.Math.min(this.measuredTTSSequenceTimings[i], 1000);
            }
        }
        return delay;
    }

    private boolean isValidDelayLevel(int level) {
        return level >= 0 && level < 6;
    }

    public void setPreferredHFPDelayLevel(int level) {
        if (isValidDelayLevel(level)) {
            this.preferredHFPDelay = getDelayInMillisec(level);
        }
    }

    private void playSilenceToMeasureVolume() {
        processTTSRequest(" ", null, false);
    }

    public int[] getStreamVolume() {
        int i;
        int localStreamType = getStream();
        int[] result = new int[2];
        if (this.volumeLevels.containsKey(java.lang.Integer.valueOf(localStreamType))) {
            i = ((java.lang.Integer) this.volumeLevels.get(java.lang.Integer.valueOf(localStreamType))).intValue();
        } else {
            i = 0;
        }
        result[0] = i;
        android.media.AudioManager audioManager = this.mAudioManager;
        if (localStreamType == 4) {
            localStreamType = 3;
        }
        result[1] = audioManager.getStreamMaxVolume(localStreamType);
        return result;
    }

    public int getStream() {
        int localStreamType;
        boolean playThroughBluetooth = this.mPlayThroughBluetooth.get();
        boolean playThroughPhoneCall = this.mPlayThroughHFP.get();
        boolean isMusicPlaying = this.mAudioManager.isMusicActive();
        sLogger.d("Play through bluetooth : " + playThroughBluetooth + ", playAsPhoneCall : " + playThroughPhoneCall + ", isMusicPlaying :" + isMusicPlaying);
        if (playThroughBluetooth) {
            sLogger.d("Is HFP connected : " + isHFPSpeakerConnected());
            sLogger.d("Is A2dp connected : " + isA2DPSpeakerConnected());
            if (!isMusicPlaying && playThroughPhoneCall && isHFPSpeakerConnected()) {
                localStreamType = 6;
            } else if (isBluetoothA2dpOn()) {
                localStreamType = 3;
            } else {
                localStreamType = 4;
            }
        } else {
            localStreamType = 4;
        }
        sLogger.d("getStream : stream " + localStreamType);
        return localStreamType;
    }

    @com.squareup.otto.Produce
    public com.navdy.client.app.framework.util.TTSAudioRouter.TTSAudioStatus produceAudioStatus() {
        com.navdy.client.app.framework.util.TTSAudioRouter.TTSAudioStatus audioStatusCopy = this.audioStatus.copy();
        int[] maxStreamVolume = getStreamVolume();
        audioStatusCopy.currentStreamVolume = maxStreamVolume[0];
        audioStatusCopy.currentStreamMaxVolume = maxStreamVolume[1];
        return audioStatusCopy;
    }

    @com.squareup.otto.Subscribe
    public void onHUDDisconnected(com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent event) {
        stopVoiceSearchSession();
    }

    @com.squareup.otto.Subscribe
    public void onHUDConnected(com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent event) {
        stopVoiceSearchSession();
    }

    public void publishAudioStatus() {
        this.bus.post(this.audioStatus.copy());
    }

    public void startVoiceSearchSession() {
        if (this.mRequestHandler != null) {
            this.mRequestHandler.sendEmptyMessage(6);
        }
    }

    public void stopVoiceSearchSession() {
        if (this.mRequestHandler != null) {
            this.mRequestHandler.sendEmptyMessage(7);
        }
    }

    public void setVoiceSearchSetupListener(com.navdy.client.app.framework.util.TTSAudioRouter.VoiceSearchSetupListener voiceSearchSetupListener2) {
        this.voiceSearchSetupListener = voiceSearchSetupListener2;
    }
}
