package com.navdy.client.app.framework.models;

public class Address {
    private static final boolean VERBOSE = false;
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.models.Address.class);
    public java.lang.String city;
    public java.lang.String country;
    public java.lang.String label;
    public java.lang.String name;
    public java.lang.String postBox;
    public java.lang.String postCode;
    public java.lang.String state;
    public java.lang.String street;
    public int type;

    public Address(java.lang.String pobox, java.lang.String street2, java.lang.String city2, java.lang.String state2, java.lang.String postcode, java.lang.String country2, int type2, java.lang.String name2, java.lang.String label2) {
        this.postBox = pobox;
        this.street = street2;
        this.city = city2;
        this.state = state2;
        this.postCode = postcode;
        this.country = country2;
        this.name = name2;
        this.type = type2;
        this.label = label2;
    }

    public java.lang.String toString() {
        return "Address{type=" + this.type + ", postBox='" + this.postBox + '\'' + ", street='" + this.street + '\'' + ", city='" + this.city + '\'' + ", state='" + this.state + '\'' + ", postCode='" + this.postCode + '\'' + ", country='" + this.country + '\'' + ", name='" + this.name + '\'' + ", label='" + this.label + '\'' + '}';
    }

    public java.lang.String getFullAddress() {
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.city)) {
            return this.street;
        }
        java.util.ArrayList<java.lang.String> fullAddress = new java.util.ArrayList<>();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.street)) {
            fullAddress.add(this.street);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.city)) {
            fullAddress.add(this.city);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.state) || !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.postCode)) {
            java.lang.String stateAndZipCode = "";
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.state)) {
                stateAndZipCode = this.state;
            }
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.state) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.postCode)) {
                stateAndZipCode = stateAndZipCode + " " + this.postCode;
            } else if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.postCode)) {
                stateAndZipCode = this.postCode;
            }
            fullAddress.add(stateAndZipCode);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.country)) {
            fullAddress.add(this.country);
        }
        return com.navdy.client.app.framework.util.StringUtils.join(fullAddress, org.droidparts.contract.SQL.DDL.SEPARATOR);
    }

    public boolean equals(java.lang.Object obj) {
        return (obj instanceof com.navdy.client.app.framework.models.Address) && this.type == ((com.navdy.client.app.framework.models.Address) obj).type && com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.postBox, ((com.navdy.client.app.framework.models.Address) obj).postBox) && com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.street, ((com.navdy.client.app.framework.models.Address) obj).street) && com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.city, ((com.navdy.client.app.framework.models.Address) obj).city) && com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.state, ((com.navdy.client.app.framework.models.Address) obj).state) && com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.postCode, ((com.navdy.client.app.framework.models.Address) obj).postCode) && com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.country, ((com.navdy.client.app.framework.models.Address) obj).country);
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        int hashCode = ((this.type * 31) + (this.postBox != null ? this.postBox.hashCode() : 0)) * 31;
        if (this.street != null) {
            i = this.street.hashCode();
        } else {
            i = 0;
        }
        int i6 = (hashCode + i) * 31;
        if (this.city != null) {
            i2 = this.city.hashCode();
        } else {
            i2 = 0;
        }
        int i7 = (i6 + i2) * 31;
        if (this.state != null) {
            i3 = this.state.hashCode();
        } else {
            i3 = 0;
        }
        int i8 = (i7 + i3) * 31;
        if (this.postCode != null) {
            i4 = this.postCode.hashCode();
        } else {
            i4 = 0;
        }
        int i9 = (i8 + i4) * 31;
        if (this.country != null) {
            i5 = this.country.hashCode();
        }
        return i9 + i5;
    }
}
