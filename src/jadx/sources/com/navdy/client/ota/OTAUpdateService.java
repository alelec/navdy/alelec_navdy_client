package com.navdy.client.ota;

public class OTAUpdateService extends android.app.Service implements com.navdy.client.ota.OTAUpdateListener {
    private static final long BUFFER_SPACE_FOR_FILE_DOWNLOAD = 52428800;
    public static final java.lang.String BUILD_SOURCE = "BUILD_SOURCE";
    public static final java.lang.String BUILD_TYPE = "BUILD_TYPE";
    public static final int CHECK_UPDATE_ACTIVITY_REQ = 256;
    public static final int CHECK_UPDATE_SERVICE_REQ = 128;
    public static final java.lang.String DOWNLOAD_ID = "DOWNLOAD_ID";
    public static final java.lang.String EXTRA_OTA_UPDATE_UI = "OTA_UPDATE_UI";
    public static final java.lang.String EXTRA_PERIODIC_CHECK = "PERIODIC_CHECK";
    public static final java.lang.String EXTRA_WIFI_TRIGGER = "WIFI_TRIGGER";
    public static final java.lang.String FORCE_FULL_UPDATE = "FORCE_FULL_UPDATE";
    private static final int INCREMENTAL_INDEX = 2;
    public static final java.lang.String LAST_CONNECTED_DEVICE_ID = "DEVICE_ID";
    public static final java.lang.String LAST_UPDATE_CHECK = "LAST_UPDATE_CHECK";
    private static final int MAJOR_MINOR_INCREMENTAL_LENGTH = 3;
    public static final java.lang.String PARTIAL_UPLOAD_SIZE = "PARTIAL_UPLOAD_SIZE";
    public static final java.lang.String SW_VERSION = "SW_VERSION";
    public static final java.lang.String SW_VERSION_NAME = "SW_VERSION_NAME";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.ota.OTAUpdateService.class);
    private java.util.concurrent.atomic.AtomicBoolean mCheckingForUpdate;
    private com.navdy.client.ota.Config mConfig;
    private java.util.concurrent.atomic.AtomicBoolean mIsDownloadingUpdate = new java.util.concurrent.atomic.AtomicBoolean(false);
    private java.util.concurrent.atomic.AtomicBoolean mIsStarted = new java.util.concurrent.atomic.AtomicBoolean(false);
    private java.util.concurrent.atomic.AtomicBoolean mIsUploadingUpdateToHUD = new java.util.concurrent.atomic.AtomicBoolean(false);
    private long mLastKnownUploadSize = 0;
    private volatile boolean mNetworkDownloadIsApproved = false;
    private com.navdy.client.ota.OTAUpdateServiceInterface mServiceInterface;
    private volatile com.navdy.client.ota.OTAUpdateService.State mState = com.navdy.client.ota.OTAUpdateService.State.UP_TO_DATE;
    private java.lang.ref.WeakReference<com.navdy.client.ota.OTAUpdateUIClient> mUIClientRef;
    private volatile com.navdy.client.ota.model.UpdateInfo mUpdateInfo;
    private com.navdy.client.ota.OTAUpdateManager mUpdateManager;
    private volatile boolean mUserApproved = true;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            boolean forceFullUpdate = false;
            android.content.SharedPreferences preferences = com.navdy.client.ota.OTAUpdateService.getSharedPreferences();
            java.lang.String lastConnectedDeviceId = preferences.getString(com.navdy.client.ota.OTAUpdateService.LAST_CONNECTED_DEVICE_ID, null);
            if (lastConnectedDeviceId == null || lastConnectedDeviceId.equals("")) {
                com.navdy.client.ota.OTAUpdateService.this.mCheckingForUpdate.set(false);
                com.navdy.client.ota.OTAUpdateService.this.setState(com.navdy.client.ota.OTAUpdateService.State.UP_TO_DATE);
                com.navdy.client.ota.OTAUpdateService.this.removeNotification();
                return;
            }
            int currentSwVersion = com.navdy.client.ota.OTAUpdateService.getSWVersion(lastConnectedDeviceId);
            if (com.navdy.client.ota.OTAUpdateService.needsFullUpdate(lastConnectedDeviceId) || preferences.getBoolean("FORCE_FULL_UPDATE", false)) {
                forceFullUpdate = true;
            }
            com.navdy.client.ota.OTAUpdateService.sLogger.d("Device needs full update? :" + forceFullUpdate);
            java.lang.String stateString = preferences.getString(com.navdy.client.app.ui.settings.SettingsConstants.OTA_STATUS, com.navdy.client.app.ui.settings.SettingsConstants.OTA_STATUS_DEFAULT);
            if (!(stateString == null || com.navdy.client.ota.OTAUpdateService.State.valueOf(stateString) == com.navdy.client.ota.OTAUpdateService.State.UP_TO_DATE)) {
                com.navdy.client.ota.model.UpdateInfo updateInfo = com.navdy.client.ota.OTAUpdateService.bReadUpdateInfo();
                if (updateInfo.incremental && updateInfo.fromVersion != currentSwVersion) {
                    com.navdy.client.ota.OTAUpdateService.this.setState(com.navdy.client.ota.OTAUpdateService.State.UP_TO_DATE);
                }
            }
            com.navdy.client.ota.OTAUpdateService.this.scheduleNextCheckForUpdate();
            com.navdy.client.ota.OTAUpdateService.this.mUpdateManager.checkForUpdate(currentSwVersion, forceFullUpdate);
        }
    }

    class OTAUpdateServiceInterfaceImpl extends android.os.Binder implements com.navdy.client.ota.OTAUpdateServiceInterface {
        OTAUpdateServiceInterfaceImpl() {
        }

        public boolean isCheckingForUpdate() {
            return com.navdy.client.ota.OTAUpdateService.this.mCheckingForUpdate.get();
        }

        public boolean checkForUpdate() {
            return com.navdy.client.ota.OTAUpdateService.this.checkForUpdate();
        }

        public void cancelDownload() {
            com.navdy.client.ota.OTAUpdateService.sLogger.d("Download canceled by user");
            if (com.navdy.client.ota.OTAUpdateService.this.mIsDownloadingUpdate.compareAndSet(true, false)) {
                com.navdy.client.ota.OTAUpdateService.bResetDownloadId();
                com.navdy.client.ota.OTAUpdateService.this.mUpdateManager.abortDownload();
                com.navdy.client.ota.OTAUpdateService.this.downloadStopped(false);
            }
        }

        public void cancelUpload() {
            com.navdy.client.ota.OTAUpdateService.sLogger.d("Upload cancelled by the user");
            if (com.navdy.client.ota.OTAUpdateService.this.mIsUploadingUpdateToHUD.compareAndSet(true, false)) {
                com.navdy.client.ota.OTAUpdateService.this.mUpdateManager.abortUpload();
                com.navdy.client.ota.OTAUpdateService.this.uploadStopped(false);
            }
        }

        public void toggleAutoDownload(boolean enableAutoDownload) {
            com.navdy.client.ota.OTAUpdateService.sLogger.d("toggleAutoDownload");
            com.navdy.client.ota.OTAUpdateService.this.mUserApproved = enableAutoDownload;
        }

        public void resetUpdate() {
            com.navdy.client.ota.OTAUpdateService.this.setState(com.navdy.client.ota.OTAUpdateService.State.UP_TO_DATE);
            com.navdy.client.ota.OTAUpdateService.this.discardOldUpdateData();
        }

        public com.navdy.client.ota.OTAUpdateService.State getOTAUpdateState() {
            return com.navdy.client.ota.OTAUpdateService.this.mState;
        }

        public com.navdy.client.ota.model.UpdateInfo getUpdateInfo() {
            return com.navdy.client.ota.OTAUpdateService.this.mUpdateInfo;
        }

        public void downloadOTAUpdate() {
            com.navdy.client.ota.OTAUpdateService.sLogger.d("User initiated the download");
            com.navdy.client.ota.OTAUpdateService.this.mUserApproved = true;
            checkForUpdate();
        }

        public void registerUIClient(com.navdy.client.ota.OTAUpdateUIClient client) {
            com.navdy.client.ota.OTAUpdateService.this.mUIClientRef = new java.lang.ref.WeakReference(client);
        }

        public void unregisterUIClient() {
            com.navdy.client.ota.OTAUpdateService.this.mUIClientRef = new java.lang.ref.WeakReference(null);
        }

        public java.lang.String getHUDBuildVersionText() {
            java.lang.String lastConnectedDeviceId = com.navdy.client.ota.OTAUpdateService.getDeviceId();
            if (lastConnectedDeviceId == null) {
                return "";
            }
            java.lang.String version = com.navdy.client.ota.OTAUpdateService.getSWVersionText(lastConnectedDeviceId);
            if (version == null) {
                return "";
            }
            return version;
        }

        public long lastKnownUploadSize() {
            return com.navdy.client.ota.OTAUpdateService.this.mLastKnownUploadSize;
        }

        public void setNetworkDownloadApproval(boolean isApproved) {
            com.navdy.client.ota.OTAUpdateService.this.mNetworkDownloadIsApproved = isApproved;
        }
    }

    public enum State {
        UPDATE_AVAILABLE,
        UP_TO_DATE,
        DOWNLOADING_UPDATE,
        READY_TO_UPLOAD,
        UPLOADING,
        READY_TO_INSTALL
    }

    public void onCreate() {
        super.onCreate();
        this.mUpdateManager = new com.navdy.client.ota.impl.OTAUpdateManagerImpl(this, this);
        this.mServiceInterface = new com.navdy.client.ota.OTAUpdateService.OTAUpdateServiceInterfaceImpl();
        this.mCheckingForUpdate = new java.util.concurrent.atomic.AtomicBoolean(false);
        this.mConfig = new com.navdy.client.ota.Config(getResources());
    }

    public int onStartCommand(android.content.Intent intent, int flags, int startId) {
        this.mIsStarted.set(true);
        if (intent != null && (intent.hasExtra(EXTRA_PERIODIC_CHECK) || intent.hasExtra(EXTRA_WIFI_TRIGGER))) {
            sLogger.d("Setting mUserApproved to true due to periodic check or wi-fi reachability");
            this.mUserApproved = true;
        }
        if (getDeviceId() == null) {
            stopSelf();
            return super.onStartCommand(intent, flags, startId);
        } else if (this.mIsDownloadingUpdate.get() || this.mIsUploadingUpdateToHUD.get()) {
            sLogger.d("onStartCommand: not processing the command as the service is in transient state Downloading :" + this.mIsDownloadingUpdate.get() + ", Uploading :" + this.mIsUploadingUpdateToHUD.get());
            return super.onStartCommand(intent, flags, startId);
        } else {
            this.mState = getSavedState();
            sLogger.d("onStart: Need to check for update again");
            checkForUpdate();
            return super.onStartCommand(intent, flags, startId);
        }
    }

    public android.os.IBinder onBind(android.content.Intent intent) {
        if (!this.mIsStarted.get()) {
            startService(getApplicationContext());
        }
        return this.mServiceInterface;
    }

    private boolean checkForUpdate() {
        if (this.mIsDownloadingUpdate.get() || !this.mCheckingForUpdate.compareAndSet(false, true)) {
            return false;
        }
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.ota.OTAUpdateService.Anon1(), 2);
        return true;
    }

    public void onCheckForUpdateFinished(com.navdy.client.ota.OTAUpdateListener.CheckOTAUpdateResult result, com.navdy.client.ota.model.UpdateInfo info) {
        if (this.mCheckingForUpdate.compareAndSet(true, false)) {
            com.navdy.client.ota.OTAUpdateUIClient client = client();
            if (result != com.navdy.client.ota.OTAUpdateListener.CheckOTAUpdateResult.NO_CONNECTIVITY && result != com.navdy.client.ota.OTAUpdateListener.CheckOTAUpdateResult.SERVER_ERROR) {
                bSaveLastCheckTime();
                if (result == com.navdy.client.ota.OTAUpdateListener.CheckOTAUpdateResult.AVAILABLE) {
                    com.navdy.client.ota.model.UpdateInfo updateInfo = bReadUpdateInfo();
                    if (updateInfo == null || !updateInfo.equals(info) || this.mState == com.navdy.client.ota.OTAUpdateService.State.UP_TO_DATE) {
                        bPersistUpdateInfo(info);
                        this.mUpdateInfo = info;
                        setState(com.navdy.client.ota.OTAUpdateService.State.UPDATE_AVAILABLE);
                        notifyUpdateAvailable();
                        discardOldUpdateData();
                    } else if (client != null) {
                        client.onStateChanged(this.mState, this.mUpdateInfo);
                    }
                } else if (result == com.navdy.client.ota.OTAUpdateListener.CheckOTAUpdateResult.UPTODATE) {
                    bPersistUpdateInfo(info);
                    this.mUpdateInfo = info;
                    if (client != null) {
                        client.onStateChanged(this.mState, this.mUpdateInfo);
                    }
                }
                performUpdate();
            } else if (client != null) {
                client.onErrorCheckingForUpdate(result == com.navdy.client.ota.OTAUpdateListener.CheckOTAUpdateResult.NO_CONNECTIVITY ? com.navdy.client.ota.OTAUpdateUIClient.Error.NO_CONNECTIVITY : com.navdy.client.ota.OTAUpdateUIClient.Error.SERVER_ERROR);
            }
        }
    }

    public void onDownloadProgress(com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus status, int downloadId, long completedSoFar, long total) {
        if (this.mIsDownloadingUpdate.get()) {
            if (status == com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.STARTED) {
                bSetDownloadId(downloadId);
            } else if (status == com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.PAUSED) {
                downloadStopped(false);
            } else {
                com.navdy.client.ota.OTAUpdateUIClient client = client();
                int percentage = total > 0 ? (int) ((((float) completedSoFar) / ((float) total)) * 100.0f) : 0;
                if (client != null) {
                    try {
                        client.onDownloadProgress(status, completedSoFar, (byte) percentage);
                    } catch (Throwable t) {
                        sLogger.d("Error " + t.getMessage());
                    }
                }
                if (percentage > 0 && status == com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.DOWNLOADING) {
                    publishDownloadProgress(percentage);
                }
                if (status == com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.DOWNLOAD_FAILED) {
                    sLogger.d("Download failed");
                    bResetDownloadId();
                    downloadStopped(false);
                } else if (status == com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.COMPLETED) {
                    sLogger.d("Download complete");
                    this.mLastKnownUploadSize = 0;
                    saveUploadProgress(0);
                    bResetDownloadId();
                    downloadStopped(true);
                    checkForUpdate();
                } else if (status != com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.DOWNLOADING) {
                    downloadStopped(false);
                }
            }
        }
    }

    public void onUploadProgress(com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus status, long completedSoFar, long total, com.navdy.service.library.events.file.FileTransferError error) {
        int percentage;
        com.navdy.client.ota.OTAUpdateUIClient client = client();
        if (total > 0) {
            percentage = (int) ((((float) completedSoFar) / ((float) total)) * 100.0f);
        } else {
            percentage = 0;
        }
        if (client != null) {
            client.onUploadProgress(status, completedSoFar, (byte) percentage);
        }
        if (percentage > 0 && status == com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus.UPLOADING) {
            this.mLastKnownUploadSize = completedSoFar;
            saveUploadProgress(completedSoFar);
            publishUploadProgress(percentage);
        }
        switch (status) {
            case COMPLETED:
                uploadStopped(true);
                return;
            case UPLOAD_FAILED:
                uploadStopped(false);
                return;
            case UPLOADING:
                return;
            default:
                uploadStopped(false);
                return;
        }
    }

    private void saveUploadProgress(long partialUploadSize) {
        getSharedPreferences().edit().putLong(PARTIAL_UPLOAD_SIZE, partialUploadSize).apply();
    }

    private void checkPrecondition() {
        this.mUpdateInfo = bReadUpdateInfo();
        if (this.mUpdateInfo != null) {
            java.lang.String lastConnectedDeviceId = getDeviceId();
            if (lastConnectedDeviceId != null) {
                int deviceSwVersion = getSWVersion(lastConnectedDeviceId);
                if (deviceSwVersion >= this.mUpdateInfo.version) {
                    sLogger.d("Device is already up to date. " + deviceSwVersion);
                    setState(com.navdy.client.ota.OTAUpdateService.State.UP_TO_DATE);
                    java.io.File file = getUpdateFile();
                    if (file != null && file.exists()) {
                        com.navdy.service.library.util.IOUtils.deleteFile(this, file.getAbsolutePath());
                    }
                }
            }
        }
    }

    private void performUpdate() {
        checkPrecondition();
        switch (this.mState) {
            case UP_TO_DATE:
                sLogger.d("The current sw is up to date");
                clearUpdateFiles();
                return;
            case UPDATE_AVAILABLE:
                sLogger.d("There is an update available, that needs to be downloaded");
                this.mUpdateInfo = bReadUpdateInfo();
                notifyUpdateAvailable();
                if (this.mUserApproved && canBeDownloadedInBackground()) {
                    sLogger.d("The update can be downloaded in the background, so start the download");
                    startDownload(this.mUserApproved);
                    return;
                }
                return;
            case READY_TO_UPLOAD:
                sLogger.d("The update is downloaded and ready to be uploaded to the HUD");
                this.mUpdateInfo = bReadUpdateInfo();
                tryUploading();
                return;
            case READY_TO_INSTALL:
                sLogger.d("The update has been downloaded to the device and will be installed when the device restarts");
                return;
            case DOWNLOADING_UPDATE:
                sLogger.d("The update is downloading...");
                return;
            case UPLOADING:
                sLogger.d("The update is uploading...");
                return;
            default:
                sLogger.e("Unknown OTA state: " + this.mState);
                return;
        }
    }

    private void scheduleNextCheckForUpdate() {
        sLogger.d("Scheduling for next check for update");
        android.content.Intent intent = new android.content.Intent(this, com.navdy.client.ota.OTAUpdateService.class);
        intent.putExtra(EXTRA_PERIODIC_CHECK, true);
        ((android.app.AlarmManager) getSystemService("alarm")).set(1, java.lang.System.currentTimeMillis() + this.mConfig.mUpdateInterval, android.app.PendingIntent.getService(this, 128, intent, 268435456));
    }

    private static java.lang.String getDeviceId() {
        return getSharedPreferences().getString(LAST_CONNECTED_DEVICE_ID, null);
    }

    private static java.lang.String getSWVersionText(java.lang.String deviceId) {
        android.content.SharedPreferences preferences = getSharedPreferences();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(preferences.getString(deviceId + "_" + SW_VERSION_NAME, ""))) {
            return preferences.getString(deviceId + "_" + SW_VERSION_NAME, "").split("-")[0];
        }
        return java.lang.String.valueOf(getSWVersion(deviceId));
    }

    private static int getSWVersion(java.lang.String deviceId) {
        int i = -1;
        java.lang.String swVersionString = getSharedPreferences().getString(deviceId + "_" + SW_VERSION, null);
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(swVersionString)) {
            return i;
        }
        try {
            return java.lang.Integer.parseInt(swVersionString);
        } catch (java.lang.NumberFormatException e) {
            sLogger.e("error parsing software version, not a number: " + swVersionString);
            return i;
        }
    }

    public static java.lang.String getLastDeviceVersionText() {
        return getSWVersionText(getDeviceId());
    }

    private static boolean needsFullUpdate(java.lang.String deviceId) {
        return getSharedPreferences().getBoolean(deviceId + "_" + "FORCE_FULL_UPDATE", false);
    }

    public static com.navdy.client.debug.util.S3Constants.BuildType getBuildType(java.lang.String deviceId) {
        java.lang.String buildTypeName = getSharedPreferences().getString(deviceId + "_" + BUILD_TYPE, com.navdy.client.app.ui.settings.SettingsConstants.OTA_BUILD_TYPE_DEFAULT);
        com.navdy.client.debug.util.S3Constants.BuildType buildType = com.navdy.client.debug.util.S3Constants.BuildType.user;
        try {
            return com.navdy.client.debug.util.S3Constants.BuildType.valueOf(buildTypeName);
        } catch (java.lang.IllegalArgumentException e) {
            sLogger.e("Cannot parse the build type " + buildTypeName, e);
            return buildType;
        }
    }

    private boolean canBeDownloadedInBackground() {
        android.content.Context c = com.navdy.client.app.NavdyApplication.getAppContext();
        return (this.mNetworkDownloadIsApproved && com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(c)) || com.navdy.service.library.util.SystemUtils.isConnectedToWifi(c);
    }

    private void downloadStopped(boolean completed) {
        stopForeground(true);
        notifyUpdateAvailable();
        if (!completed) {
            setState(com.navdy.client.ota.OTAUpdateService.State.UPDATE_AVAILABLE);
        } else {
            setState(com.navdy.client.ota.OTAUpdateService.State.READY_TO_UPLOAD);
        }
        this.mIsDownloadingUpdate.set(false);
    }

    private void uploadStopped(boolean completed) {
        stopForeground(true);
        removeNotification();
        if (completed) {
            setState(com.navdy.client.ota.OTAUpdateService.State.READY_TO_INSTALL);
        } else {
            setState(com.navdy.client.ota.OTAUpdateService.State.READY_TO_UPLOAD);
        }
        this.mIsUploadingUpdateToHUD.set(false);
    }

    private void clearUpdateFiles() {
        java.io.File outputFile = getUpdateFile();
        sLogger.d("Clearing the update file as the device is up to date " + outputFile.getAbsolutePath() + " , Size (Bytes) :" + outputFile.length());
        com.navdy.service.library.util.IOUtils.deleteFile(this, outputFile.getAbsolutePath());
    }

    private void startDownload(boolean userInitiated) {
        if (this.mIsDownloadingUpdate.compareAndSet(false, true)) {
            setState(com.navdy.client.ota.OTAUpdateService.State.DOWNLOADING_UPDATE);
            int downloadId = bReadLastDownloadId();
            java.io.File outputFile = getUpdateFile();
            long partiallyDownloadedFileSize = 0;
            if (outputFile.exists()) {
                if (downloadId == -1) {
                    com.navdy.service.library.util.IOUtils.deleteFile(this, outputFile.getAbsolutePath());
                } else {
                    partiallyDownloadedFileSize = outputFile.length();
                }
            }
            if (!ensureSpaceAvailableForDownload(this.mUpdateInfo.size, partiallyDownloadedFileSize)) {
                com.navdy.client.ota.OTAUpdateUIClient client = client();
                if (client != null) {
                    client.onDownloadProgress(com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.NOT_ENOUGH_SPACE, 0, 0);
                    downloadStopped(false);
                }
            }
            if (downloadId == -1) {
                try {
                    if (!outputFile.createNewFile()) {
                        sLogger.e("Unable to create the output file: " + outputFile);
                    }
                } catch (java.io.IOException e) {
                    com.navdy.client.ota.OTAUpdateUIClient client2 = client();
                    if (client2 != null) {
                        client2.onDownloadProgress(com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.DOWNLOAD_FAILED, -1, 0);
                        downloadStopped(false);
                    }
                    return;
                }
            }
            startForeground(1, getDownloadingNotification());
            this.mUpdateManager.download(this.mUpdateInfo, outputFile, downloadId);
        }
    }

    private java.io.File getUpdateFile() {
        return new java.io.File(com.navdy.client.app.framework.PathManager.getInstance().getOTAUpdateFilePath());
    }

    private boolean ensureSpaceAvailableForDownload(long sizeOfTheUpdate, long partialDownloadSize) {
        java.lang.String otaUpdateFolder = com.navdy.client.app.framework.PathManager.getInstance().getOtaUpdateFolderPath();
        android.os.StatFs statFs = new android.os.StatFs(otaUpdateFolder);
        long freeSpace = ((long) statFs.getFreeBlocks()) * ((long) statFs.getBlockSize());
        long spaceRequired = (sizeOfTheUpdate - partialDownloadSize) + BUFFER_SPACE_FOR_FILE_DOWNLOAD;
        if (sLogger.isLoggable(3) || freeSpace <= spaceRequired) {
            sLogger.i("In " + otaUpdateFolder + "; Space required: " + spaceRequired + "; Space free: " + freeSpace + " enough space:" + java.lang.Boolean.toString(freeSpace > spaceRequired));
        }
        return freeSpace > spaceRequired;
    }

    private boolean isUpdateFileValid() {
        java.io.File updateFile = new java.io.File(com.navdy.client.app.framework.PathManager.getInstance().getOTAUpdateFilePath());
        if (!updateFile.exists()) {
            return false;
        }
        if (updateFile.length() == this.mUpdateInfo.size) {
            return true;
        }
        sLogger.d("Downloaded update file is invalid");
        return false;
    }

    private void tryUploading() {
        sLogger.d("Trying to transfer the update to HUD");
        if (this.mIsUploadingUpdateToHUD.compareAndSet(false, true)) {
            com.navdy.service.library.device.RemoteDevice mRemoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
            if (mRemoteDevice == null || !mRemoteDevice.isConnected()) {
                uploadStopped(false);
                return;
            }
            sLogger.d("Device is connected, initiating transfer of update ");
            if (!isUpdateFileValid()) {
                uploadStopped(false);
                setState(com.navdy.client.ota.OTAUpdateService.State.UPDATE_AVAILABLE);
                return;
            }
            java.io.File updateFile = getUpdateFile();
            long partialUploadedSize = getSharedPreferences().getLong(PARTIAL_UPLOAD_SIZE, 0);
            this.mLastKnownUploadSize = partialUploadedSize;
            setState(com.navdy.client.ota.OTAUpdateService.State.UPLOADING);
            startForeground(1, getUploadingNotification());
            if (this.mUpdateInfo.incremental) {
                this.mUpdateManager.uploadToHUD(updateFile, partialUploadedSize, com.navdy.client.app.framework.PathManager.getInstance().getOtaUpdateFileNameOnHUD(this.mUpdateInfo.fromVersion, this.mUpdateInfo.version));
            } else {
                this.mUpdateManager.uploadToHUD(updateFile, partialUploadedSize, com.navdy.client.app.framework.PathManager.getInstance().getOtaUpdateFileNameOnHUD(this.mUpdateInfo.version));
            }
        }
    }

    private void discardOldUpdateData() {
        bResetDownloadId();
        java.io.File file = new java.io.File(com.navdy.client.app.framework.PathManager.getInstance().getOtaUpdateFolderPath());
        if (file.exists() && !file.isFile()) {
            java.io.File[] children = file.listFiles();
            if (children != null) {
                for (java.io.File child : children) {
                    com.navdy.service.library.util.IOUtils.deleteFile(this, child.getAbsolutePath());
                }
            }
        }
    }

    public static void startService(android.content.Context context) {
        context.startService(getServiceIntent(context));
    }

    public static android.content.Intent getServiceIntent(android.content.Context context) {
        return new android.content.Intent(context, com.navdy.client.ota.OTAUpdateService.class);
    }

    private void setState(com.navdy.client.ota.OTAUpdateService.State state) {
        this.mState = state;
        if (!(state == null || state == com.navdy.client.ota.OTAUpdateService.State.DOWNLOADING_UPDATE || state == com.navdy.client.ota.OTAUpdateService.State.UPLOADING)) {
            android.content.SharedPreferences preferences = getSharedPreferences();
            if (preferences != null) {
                preferences.edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.OTA_STATUS, state.name()).commit();
            }
        }
        com.navdy.client.ota.OTAUpdateUIClient client = client();
        if (client != null) {
            client.onStateChanged(state, this.mUpdateInfo);
        }
    }

    private com.navdy.client.ota.OTAUpdateService.State getSavedState() {
        java.lang.String stateString = getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.OTA_STATUS, null);
        com.navdy.client.ota.OTAUpdateService.State state = com.navdy.client.ota.OTAUpdateService.State.UP_TO_DATE;
        if (stateString != null) {
            return com.navdy.client.ota.OTAUpdateService.State.valueOf(stateString);
        }
        return state;
    }

    public static void bResetDownloadId() {
        android.content.SharedPreferences preferences = getSharedPreferences();
        if (preferences != null) {
            preferences.edit().remove(DOWNLOAD_ID).commit();
        }
    }

    public static int bReadLastDownloadId() {
        android.content.SharedPreferences preferences = getSharedPreferences();
        if (preferences != null) {
            return preferences.getInt(DOWNLOAD_ID, -1);
        }
        return -1;
    }

    public static void bSetDownloadId(int id) {
        android.content.SharedPreferences preferences = getSharedPreferences();
        if (preferences != null) {
            preferences.edit().putInt(DOWNLOAD_ID, id).commit();
        }
    }

    @android.annotation.SuppressLint({"CommitPrefEdits"})
    public static void bPersistDeviceInfo(com.navdy.service.library.events.DeviceInfo deviceInfo) {
        android.content.SharedPreferences preferences = getSharedPreferences();
        if (preferences != null) {
            preferences.edit().putString(LAST_CONNECTED_DEVICE_ID, deviceInfo.deviceId).putString(deviceInfo.deviceId + "_" + SW_VERSION, deviceInfo.systemVersion).putString(deviceInfo.deviceId + "_" + SW_VERSION_NAME, deviceInfo.clientVersion).putString(deviceInfo.deviceId + "_" + BUILD_TYPE, deviceInfo.buildType).putBoolean(deviceInfo.deviceId + "_" + "FORCE_FULL_UPDATE", java.lang.Boolean.TRUE.equals(deviceInfo.forceFullUpdate)).commit();
        }
    }

    @android.annotation.SuppressLint({"CommitPrefEdits"})
    public static void bPersistUpdateInfo(com.navdy.client.ota.model.UpdateInfo updateInfo) {
        android.content.SharedPreferences preferences = getSharedPreferences();
        if (preferences != null) {
            preferences.edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.OTA_STATUS, com.navdy.client.ota.OTAUpdateService.State.UPDATE_AVAILABLE.name()).putString(com.navdy.client.app.ui.settings.SettingsConstants.OTA_VERSION_NAME, updateInfo.versionName).putInt(com.navdy.client.app.ui.settings.SettingsConstants.OTA_VERSION, updateInfo.version).putString(com.navdy.client.app.ui.settings.SettingsConstants.OTA_URL, updateInfo.url).putString(com.navdy.client.app.ui.settings.SettingsConstants.OTA_DESCRIPTION, updateInfo.description).putLong(com.navdy.client.app.ui.settings.SettingsConstants.OTA_SIZE, updateInfo.size).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.OTA_IS_INCREMENTAL, updateInfo.incremental).putInt(com.navdy.client.app.ui.settings.SettingsConstants.OTA_FROM_VERSION, updateInfo.fromVersion).putString(com.navdy.client.app.ui.settings.SettingsConstants.OTA_META_DATA, updateInfo.metaData).commit();
        }
    }

    public static com.navdy.client.ota.model.UpdateInfo bReadUpdateInfo() {
        android.content.SharedPreferences preferences = getSharedPreferences();
        com.navdy.client.ota.model.UpdateInfo updateInfo = new com.navdy.client.ota.model.UpdateInfo();
        updateInfo.version = preferences.getInt(com.navdy.client.app.ui.settings.SettingsConstants.OTA_VERSION, -1);
        updateInfo.versionName = preferences.getString(com.navdy.client.app.ui.settings.SettingsConstants.OTA_VERSION_NAME, "");
        updateInfo.url = preferences.getString(com.navdy.client.app.ui.settings.SettingsConstants.OTA_URL, "");
        updateInfo.description = preferences.getString(com.navdy.client.app.ui.settings.SettingsConstants.OTA_DESCRIPTION, "");
        updateInfo.size = preferences.getLong(com.navdy.client.app.ui.settings.SettingsConstants.OTA_SIZE, 0);
        updateInfo.incremental = preferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.OTA_IS_INCREMENTAL, false);
        updateInfo.fromVersion = preferences.getInt(com.navdy.client.app.ui.settings.SettingsConstants.OTA_FROM_VERSION, 0);
        updateInfo.metaData = preferences.getString(com.navdy.client.app.ui.settings.SettingsConstants.OTA_META_DATA, null);
        return updateInfo;
    }

    public static void bSaveLastCheckTime() {
        android.content.SharedPreferences preferences = getSharedPreferences();
        preferences.edit().putLong(preferences.getString(LAST_CONNECTED_DEVICE_ID, null) + "_" + LAST_UPDATE_CHECK, java.lang.System.currentTimeMillis()).commit();
    }

    public static android.content.SharedPreferences getSharedPreferences() {
        return com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
    }

    private com.navdy.client.ota.OTAUpdateUIClient client() {
        if (this.mUIClientRef != null) {
            return (com.navdy.client.ota.OTAUpdateUIClient) this.mUIClientRef.get();
        }
        return null;
    }

    public static boolean isLaunchedByOtaUpdateService(android.content.Intent intent) {
        return intent != null && intent.hasExtra(EXTRA_OTA_UPDATE_UI) && intent.getBooleanExtra(EXTRA_OTA_UPDATE_UI, false);
    }

    private android.app.PendingIntent getUIPendingIntent() {
        android.content.Context appContext = getApplicationContext();
        return android.app.PendingIntent.getActivity(appContext, 256, new android.content.Intent(appContext, com.navdy.client.app.ui.settings.OtaSettingsActivity.class), 1073741824);
    }

    public void notifyUpdateAvailable() {
        android.app.Notification.Builder builder = new android.app.Notification.Builder(this);
        builder.setContentIntent(getUIPendingIntent());
        builder.setContentTitle(getString(com.navdy.client.R.string.navdy_software_update));
        builder.setSmallIcon(com.navdy.client.R.drawable.icon_status_bar);
        builder.setContentText(getString(com.navdy.client.R.string.update_available));
        builder.setAutoCancel(true);
        ((android.app.NotificationManager) getSystemService("notification")).notify(1, builder.build());
    }

    public void removeNotification() {
        ((android.app.NotificationManager) getSystemService("notification")).cancel(1);
    }

    public android.app.Notification getDownloadingNotification() {
        android.app.Notification.Builder builder = new android.app.Notification.Builder(this);
        builder.setContentIntent(getUIPendingIntent());
        builder.setContentTitle(getString(com.navdy.client.R.string.navdy_software_update));
        builder.setSmallIcon(com.navdy.client.R.drawable.icon_status_bar);
        builder.setContentText(getString(com.navdy.client.R.string.downloading_update));
        builder.setProgress(0, 0, true);
        return builder.build();
    }

    public void publishDownloadProgress(int percentage) {
        android.app.Notification.Builder builder = new android.app.Notification.Builder(this);
        builder.setContentIntent(getUIPendingIntent());
        builder.setContentTitle(getString(com.navdy.client.R.string.navdy_software_update));
        builder.setSmallIcon(com.navdy.client.R.drawable.icon_status_bar);
        builder.setContentText(getString(com.navdy.client.R.string.downloading_update));
        builder.setAutoCancel(true);
        builder.setProgress(100, percentage, false);
        ((android.app.NotificationManager) getSystemService("notification")).notify(1, builder.build());
    }

    public void publishUploadProgress(int percentage) {
        android.app.Notification.Builder builder = new android.app.Notification.Builder(this);
        builder.setContentIntent(getUIPendingIntent());
        builder.setContentTitle(getString(com.navdy.client.R.string.navdy_software_update));
        builder.setSmallIcon(com.navdy.client.R.drawable.icon_status_bar);
        builder.setContentText(getString(com.navdy.client.R.string.transferring_update));
        builder.setProgress(100, percentage, false);
        builder.setAutoCancel(true);
        ((android.app.NotificationManager) getSystemService("notification")).notify(1, builder.build());
    }

    public android.app.Notification getUploadingNotification() {
        android.app.Notification.Builder builder = new android.app.Notification.Builder(this);
        builder.setContentIntent(getUIPendingIntent());
        builder.setContentTitle(getString(com.navdy.client.R.string.navdy_software_update));
        builder.setSmallIcon(com.navdy.client.R.drawable.icon_status_bar);
        builder.setContentText(getString(com.navdy.client.R.string.transferring_update));
        builder.setProgress(0, 0, true);
        return builder.build();
    }

    public static com.navdy.client.debug.util.S3Constants.BuildSource getBuildSource() {
        com.navdy.client.debug.util.S3Constants.BuildType buildType = getBuildType();
        com.navdy.client.debug.util.S3Constants.BuildSource[] sourcesForBuildType = com.navdy.client.debug.util.S3Constants.getSourcesForBuildType(buildType);
        java.lang.String preference = getUserPreferredSourceForTheDevice();
        com.navdy.client.debug.util.S3Constants.BuildSource source = com.navdy.client.debug.util.S3Constants.getDefaultSourceForBuildType(buildType);
        try {
            source = com.navdy.client.debug.util.S3Constants.BuildSource.valueOf(preference);
        } catch (java.lang.IllegalArgumentException iae) {
            sLogger.e("Error parsing the saved user preference for OTA source " + preference, iae);
        }
        sLogger.d("User preference for the source " + preference + ", Source :" + source.name());
        boolean isSourceInAllowedSourcesForBuildType = false;
        int length = sourcesForBuildType.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            } else if (sourcesForBuildType[i] == source) {
                isSourceInAllowedSourcesForBuildType = true;
                break;
            } else {
                i++;
            }
        }
        if (isSourceInAllowedSourcesForBuildType) {
            sLogger.d("User preferred source is compatible with the build type");
            return source;
        }
        sLogger.d("User preferred source is not compatible with build type, setting to default");
        com.navdy.client.debug.util.S3Constants.BuildSource defaultSourceForBuildType = com.navdy.client.debug.util.S3Constants.getDefaultSourceForBuildType(buildType);
        persistUserPreferredBuildSource(source);
        return defaultSourceForBuildType;
    }

    public static com.navdy.client.debug.util.S3Constants.BuildSource[] getBuildSources() {
        return com.navdy.client.debug.util.S3Constants.getSourcesForBuildType(getBuildType());
    }

    public static com.navdy.client.debug.util.S3Constants.BuildType getBuildType() {
        return getBuildType(getDeviceId());
    }

    public static java.lang.String getUserPreferredSourceForTheDevice() {
        return getBuildSource(getDeviceId());
    }

    public static java.lang.String getBuildSource(java.lang.String deviceId) {
        return getSharedPreferences().getString(deviceId + "_" + BUILD_SOURCE, com.navdy.client.debug.util.S3Constants.getDefaultSourceForBuildType(getBuildType(deviceId)).name());
    }

    public static void persistUserPreferredBuildSource(com.navdy.client.debug.util.S3Constants.BuildSource source) {
        getSharedPreferences().edit().putString(getDeviceId() + "_" + BUILD_SOURCE, source.name()).commit();
    }
}
