package com.navdy.client.ota.impl;

public class OTAUpdateManagerImpl extends android.os.Binder implements com.navdy.client.ota.OTAUpdateManager {
    public static final int S3_CONNECTION_TIMEOUT = 15000;
    public static final int S3_DOWNLOAD_TIMEOUT = 60000;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.ota.impl.OTAUpdateManagerImpl.class);
    protected com.amazonaws.services.s3.AmazonS3Client mClient;
    private android.content.Context mContext;
    protected java.util.concurrent.atomic.AtomicBoolean mDownloadAborted = new java.util.concurrent.atomic.AtomicBoolean(false);
    protected com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver mDownloadObserver;
    private com.navdy.client.debug.file.RemoteFileTransferManager mRemoteFileTransferManager;
    protected com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility mTransferUtility;
    private com.navdy.client.ota.OTAUpdateListener mUpdateListener;
    protected java.util.concurrent.atomic.AtomicBoolean mUploadAborted = new java.util.concurrent.atomic.AtomicBoolean(false);

    class Anon1 implements com.amazonaws.mobileconnectors.s3.transferutility.TransferListener {
        boolean mStarted = false;
        final /* synthetic */ com.navdy.client.ota.model.UpdateInfo val$info;

        /* renamed from: com.navdy.client.ota.impl.OTAUpdateManagerImpl$Anon1$Anon1 reason: collision with other inner class name */
        class C0078Anon1 implements java.lang.Runnable {
            final /* synthetic */ int val$id;
            final /* synthetic */ com.amazonaws.mobileconnectors.s3.transferutility.TransferState val$state;

            C0078Anon1(int i, com.amazonaws.mobileconnectors.s3.transferutility.TransferState transferState) {
                this.val$id = i;
                this.val$state = transferState;
            }

            public void run() {
                if (!com.navdy.client.ota.impl.OTAUpdateManagerImpl.Anon1.this.mStarted) {
                    com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mUpdateListener.onDownloadProgress(com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.STARTED, this.val$id, 0, 0);
                    com.navdy.client.ota.impl.OTAUpdateManagerImpl.Anon1.this.mStarted = true;
                }
                try {
                    switch (com.navdy.client.ota.impl.OTAUpdateManagerImpl.Anon3.$SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[this.val$state.ordinal()]) {
                        case 1:
                            com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.d("Download completed successfully");
                            com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mUpdateListener.onDownloadProgress(com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.COMPLETED, this.val$id, 0, 0);
                            return;
                        case 2:
                            com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.d("Download failed");
                            com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mUpdateListener.onDownloadProgress(com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.DOWNLOAD_FAILED, this.val$id, 0, com.navdy.client.ota.impl.OTAUpdateManagerImpl.Anon1.this.val$info.size);
                            return;
                        case 3:
                            com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.d("Download failed");
                            com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mUpdateListener.onDownloadProgress(com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.PAUSED, this.val$id, 0, 0);
                            return;
                        case 4:
                            com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.d("Download in progress");
                            return;
                        default:
                            com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.e("Unknown state: " + this.val$state);
                            return;
                    }
                } catch (Throwable t) {
                    com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.e("Bad listener ", t);
                }
                com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.e("Bad listener ", t);
            }
        }

        class Anon2 implements java.lang.Runnable {
            final /* synthetic */ long val$bytesCurrent;
            final /* synthetic */ int val$id;

            Anon2(int i, long j) {
                this.val$id = i;
                this.val$bytesCurrent = j;
            }

            public void run() {
                try {
                    com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mUpdateListener.onDownloadProgress(com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.DOWNLOADING, this.val$id, this.val$bytesCurrent, com.navdy.client.ota.impl.OTAUpdateManagerImpl.Anon1.this.val$info.size);
                } catch (Throwable t) {
                    com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.e("Bad listener ", t);
                }
            }
        }

        class Anon3 implements java.lang.Runnable {
            final /* synthetic */ int val$id;

            Anon3(int i) {
                this.val$id = i;
            }

            public void run() {
                try {
                    com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mUpdateListener.onDownloadProgress(com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.DOWNLOAD_FAILED, this.val$id, 0, com.navdy.client.ota.impl.OTAUpdateManagerImpl.Anon1.this.val$info.size);
                } catch (Throwable t) {
                    com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.e("Bad listener ", t);
                }
            }
        }

        Anon1(com.navdy.client.ota.model.UpdateInfo updateInfo) {
            this.val$info = updateInfo;
        }

        public void onStateChanged(int id, com.amazonaws.mobileconnectors.s3.transferutility.TransferState state) {
            if (!(state == com.amazonaws.mobileconnectors.s3.transferutility.TransferState.COMPLETED || state == com.amazonaws.mobileconnectors.s3.transferutility.TransferState.IN_PROGRESS || state == com.amazonaws.mobileconnectors.s3.transferutility.TransferState.WAITING || state == com.amazonaws.mobileconnectors.s3.transferutility.TransferState.PAUSED)) {
                com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mTransferUtility.cancel(id);
            }
            if (!com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mDownloadAborted.get()) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.ota.impl.OTAUpdateManagerImpl.Anon1.C0078Anon1(id, state), 2);
            }
        }

        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            if (!com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mDownloadAborted.get()) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.ota.impl.OTAUpdateManagerImpl.Anon1.Anon2(id, bytesCurrent), 2);
            }
        }

        public void onError(int id, java.lang.Exception ex) {
            com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.e("OTA on error: ", ex.fillInStackTrace());
            if (!com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mDownloadAborted.get()) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.ota.impl.OTAUpdateManagerImpl.Anon1.Anon3(id), 2);
            }
        }
    }

    class Anon2 implements com.navdy.client.debug.file.FileTransferManager.FileTransferListener {
        final /* synthetic */ long val$total;

        Anon2(long j) {
            this.val$total = j;
        }

        public void onFileTransferResponse(com.navdy.service.library.events.file.FileTransferResponse response) {
            com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.d("Response " + response);
            if (com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mUploadAborted.get()) {
                com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.d("onFileTransferResponse: Upload is already aborted , so ignoring the response");
            } else if (!response.success.booleanValue()) {
                try {
                    com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mUpdateListener.onUploadProgress(com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus.UPLOAD_FAILED, 0, this.val$total, response.error);
                } catch (Throwable t) {
                    com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.d("Bad listener ", t);
                }
            }
        }

        public void onFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus status) {
            com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.d("Status " + status);
            if (com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mUploadAborted.get()) {
                com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.d("onFileTransferStatus: Upload is already aborted , so ignoring the status");
                return;
            }
            try {
                if (!status.success.booleanValue()) {
                    com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mUpdateListener.onUploadProgress(com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus.UPLOAD_FAILED, 0, this.val$total, status.error);
                } else if (status.transferComplete.booleanValue()) {
                    com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mUpdateListener.onUploadProgress(com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus.COMPLETED, this.val$total, this.val$total, null);
                } else {
                    com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mUpdateListener.onUploadProgress(com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus.UPLOADING, status.totalBytesTransferred.longValue(), this.val$total, null);
                }
            } catch (Throwable t) {
                com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.e("Bad listener ", t);
            }
        }

        public void onError(com.navdy.service.library.events.file.FileTransferError errorCode, java.lang.String error) {
            com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.e("Transfer Error :" + error);
            if (com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mUploadAborted.get()) {
                com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.d("onFileTransferError: Upload is already aborted , so ignoring the error");
                return;
            }
            try {
                com.navdy.client.ota.impl.OTAUpdateManagerImpl.this.mUpdateListener.onUploadProgress(com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus.UPLOAD_FAILED, 0, 0, errorCode);
            } catch (Throwable t) {
                com.navdy.client.ota.impl.OTAUpdateManagerImpl.sLogger.e("Bad listener :", t);
            }
        }
    }

    static /* synthetic */ class Anon3 {
        static final /* synthetic */ int[] $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState = new int[com.amazonaws.mobileconnectors.s3.transferutility.TransferState.values().length];

        static {
            try {
                $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[com.amazonaws.mobileconnectors.s3.transferutility.TransferState.COMPLETED.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[com.amazonaws.mobileconnectors.s3.transferutility.TransferState.FAILED.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[com.amazonaws.mobileconnectors.s3.transferutility.TransferState.PAUSED.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[com.amazonaws.mobileconnectors.s3.transferutility.TransferState.IN_PROGRESS.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError e4) {
            }
        }
    }

    public OTAUpdateManagerImpl(android.content.Context context, com.navdy.client.ota.OTAUpdateListener listener) {
        this.mContext = context;
        this.mUpdateListener = listener;
        if (context == null) {
            throw new java.lang.IllegalArgumentException("Context cannot be null");
        }
        this.mClient = createS3Client();
    }

    public static com.amazonaws.services.s3.AmazonS3Client createS3Client() {
        com.amazonaws.auth.BasicAWSCredentials credentials = new com.amazonaws.auth.BasicAWSCredentials(com.navdy.client.app.framework.util.CredentialsUtils.getCredentials(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.metadata_aws_account_id)), com.navdy.client.app.framework.util.CredentialsUtils.getCredentials(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.metadata_aws_secret)));
        com.amazonaws.ClientConfiguration config = new com.amazonaws.ClientConfiguration();
        config.setConnectionTimeout(15000);
        config.setSocketTimeout(60000);
        return new com.amazonaws.services.s3.AmazonS3Client((com.amazonaws.auth.AWSCredentials) credentials, config);
    }

    public void checkForUpdate(int currentIncrementalVersion, boolean forceFullUpdate) {
        sLogger.d("Checking for OTA update , current version :" + currentIncrementalVersion);
        if (!com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(this.mContext)) {
            sLogger.d("No connectivity for checking OTA update");
            try {
                if (this.mUpdateListener != null) {
                    this.mUpdateListener.onCheckForUpdateFinished(com.navdy.client.ota.OTAUpdateListener.CheckOTAUpdateResult.NO_CONNECTIVITY, null);
                }
            } catch (Throwable t) {
                sLogger.e("Bad listener", t);
            }
        } else {
            com.navdy.client.ota.model.UpdateInfo incrementalUpdateInfo = null;
            com.navdy.client.debug.util.S3Constants.BuildSource buildSource = getOtaTopLevelFolderName();
            if (!forceFullUpdate && currentIncrementalVersion != -1) {
                java.lang.String fileNameForIncrementalVersion = buildSource.getPrefix() + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + com.navdy.client.debug.util.S3Constants.LOOKUP_FOLDER_PREFIX + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + currentIncrementalVersion + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + com.navdy.client.debug.util.S3Constants.OTA_LOOKUP_FILE_NAME;
                try {
                    incrementalUpdateInfo = fetchUpdateInfo(buildSource.getBucket(), fileNameForIncrementalVersion);
                } catch (java.lang.Exception e) {
                    if (!(e instanceof com.amazonaws.services.s3.model.AmazonS3Exception) || ((com.amazonaws.services.s3.model.AmazonS3Exception) e).getStatusCode() != 404) {
                        sLogger.e("Server error while checking for OTA update, current version :" + currentIncrementalVersion + " " + e);
                        if (this.mUpdateListener != null) {
                            this.mUpdateListener.onCheckForUpdateFinished(com.navdy.client.ota.OTAUpdateListener.CheckOTAUpdateResult.SERVER_ERROR, null);
                            return;
                        }
                        return;
                    }
                    sLogger.v("File does not exist in Amazon S3: " + fileNameForIncrementalVersion);
                } catch (Throwable t2) {
                    sLogger.e("Bad listener ", t2);
                    return;
                }
            }
            com.navdy.client.ota.model.UpdateInfo latestUpdateInfo = null;
            try {
                latestUpdateInfo = fetchUpdateInfo(buildSource.getBucket(), buildSource.getPrefix() + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + com.navdy.client.debug.util.S3Constants.LOOKUP_FOLDER_PREFIX + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + com.navdy.client.debug.util.S3Constants.OTA_LOOKUP_FILE_NAME);
            } catch (java.lang.Exception e2) {
                sLogger.e("Server error while checking for OTA update, current version :" + currentIncrementalVersion);
                if (this.mUpdateListener != null) {
                    this.mUpdateListener.onCheckForUpdateFinished(com.navdy.client.ota.OTAUpdateListener.CheckOTAUpdateResult.SERVER_ERROR, null);
                }
            } catch (Throwable t3) {
                sLogger.e("Bad listener ", t3);
            }
            java.lang.String metaData = null;
            if (latestUpdateInfo != null) {
                java.lang.String metaDataFileName = buildSource.getPrefix() + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + java.lang.String.format(com.navdy.client.debug.util.S3Constants.OTA_RELEASE_NOTES_FILE_NAME_FORMAT, new java.lang.Object[]{java.lang.Integer.valueOf(latestUpdateInfo.version)});
                sLogger.d("Fetching the meta data from :" + metaDataFileName);
                metaData = fetchMetaData(buildSource.getBucket(), metaDataFileName);
            }
            if (latestUpdateInfo == null || currentIncrementalVersion >= latestUpdateInfo.version) {
                if (latestUpdateInfo == null) {
                    sLogger.e("Couldn't retrieve the latest info data (latestUpdateInfo == null)");
                }
                if (this.mUpdateListener != null) {
                    sLogger.d("S/w up to date");
                    try {
                        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(metaData)) {
                            latestUpdateInfo.metaData = metaData;
                        }
                        this.mUpdateListener.onCheckForUpdateFinished(com.navdy.client.ota.OTAUpdateListener.CheckOTAUpdateResult.UPTODATE, latestUpdateInfo);
                    } catch (Throwable t4) {
                        sLogger.e("Bad listener ", t4);
                    }
                }
            } else if (incrementalUpdateInfo == null || incrementalUpdateInfo.version != latestUpdateInfo.version) {
                sLogger.d("A full update is available : " + latestUpdateInfo.version);
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(metaData)) {
                    latestUpdateInfo.metaData = metaData;
                }
                try {
                    this.mUpdateListener.onCheckForUpdateFinished(com.navdy.client.ota.OTAUpdateListener.CheckOTAUpdateResult.AVAILABLE, latestUpdateInfo);
                } catch (Throwable t5) {
                    sLogger.e("Bad listener ", t5);
                }
            } else {
                sLogger.d("An incremental update is available, from :" + currentIncrementalVersion + " to:" + incrementalUpdateInfo.version);
                try {
                    incrementalUpdateInfo.incremental = true;
                    incrementalUpdateInfo.fromVersion = currentIncrementalVersion;
                    if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(metaData)) {
                        incrementalUpdateInfo.metaData = metaData;
                    }
                    this.mUpdateListener.onCheckForUpdateFinished(com.navdy.client.ota.OTAUpdateListener.CheckOTAUpdateResult.AVAILABLE, incrementalUpdateInfo);
                } catch (Throwable t6) {
                    sLogger.d("Bad listener ", t6);
                }
            }
        }
    }

    private com.navdy.client.debug.util.S3Constants.BuildSource getOtaTopLevelFolderName() {
        com.navdy.client.debug.util.S3Constants.BuildSource buildSource = com.navdy.client.debug.util.S3Constants.BuildSource.STABLE;
        if (com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences() != null) {
            return com.navdy.client.ota.OTAUpdateService.getBuildSource();
        }
        return buildSource;
    }

    public void download(com.navdy.client.ota.model.UpdateInfo info, java.io.File outputFile, int downloadId) {
        sLogger.d("Downloading the update..");
        try {
            this.mDownloadAborted.set(false);
            if (info == null || info.version <= 0 || info.url == null || info.url.equals("")) {
                throw new java.lang.IllegalArgumentException("Update information is not valid");
            }
            this.mTransferUtility = new com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility(this.mClient, this.mContext);
            boolean recordFound = false;
            if (downloadId != -1) {
                this.mDownloadObserver = this.mTransferUtility.getTransferById(downloadId);
                if (this.mDownloadObserver != null) {
                    if (this.mDownloadObserver.getState() == com.amazonaws.mobileconnectors.s3.transferutility.TransferState.COMPLETED) {
                        sLogger.d("Download completed successfully");
                        this.mUpdateListener.onDownloadProgress(com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.COMPLETED, downloadId, 0, 0);
                        return;
                    } else if (this.mDownloadObserver.getState() == com.amazonaws.mobileconnectors.s3.transferutility.TransferState.PAUSED) {
                        this.mDownloadObserver = this.mTransferUtility.resume(downloadId);
                        if (this.mDownloadObserver != null) {
                            int downloadId2 = this.mDownloadObserver.getId();
                            recordFound = true;
                        }
                    }
                }
            }
            if (!recordFound) {
                java.lang.String[] parsedResult = parseS3Url(info.url);
                java.lang.String bucket = parsedResult[0];
                java.lang.String key = parsedResult[1];
                if (bucket == null || bucket.equals("") || key == null || key.equals("")) {
                    throw new java.lang.IllegalArgumentException("Not a valid url");
                }
                this.mDownloadObserver = this.mTransferUtility.download(bucket, key, outputFile);
                int downloadId3 = this.mDownloadObserver.getId();
            }
            this.mDownloadObserver.setTransferListener(new com.navdy.client.ota.impl.OTAUpdateManagerImpl.Anon1(info));
        } catch (Throwable th) {
            if (this.mUpdateListener != null) {
                long size = 0;
                if (info != null) {
                    size = info.size;
                }
                this.mUpdateListener.onDownloadProgress(com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus.DOWNLOAD_FAILED, downloadId, 0, size);
            }
        }
    }

    public void uploadToHUD(java.io.File updateFile, long offset, java.lang.String file) {
        sLogger.d("Uploading the update file to HUD");
        this.mUploadAborted.set(false);
        long total = updateFile.length();
        this.mRemoteFileTransferManager = new com.navdy.client.debug.file.RemoteFileTransferManager(this.mContext, updateFile, com.navdy.service.library.events.file.FileType.FILE_TYPE_OTA, file, (com.navdy.client.debug.file.FileTransferManager.FileTransferListener) new com.navdy.client.ota.impl.OTAUpdateManagerImpl.Anon2(total));
        if (offset > 0) {
            this.mUpdateListener.onUploadProgress(com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus.UPLOADING, offset, total, null);
            this.mRemoteFileTransferManager.sendFile(offset);
            return;
        }
        this.mRemoteFileTransferManager.sendFile();
    }

    public void abortDownload() {
        sLogger.d("Download aborted");
        if (this.mDownloadAborted.compareAndSet(false, true) && this.mDownloadObserver != null) {
            try {
                this.mTransferUtility.cancel(this.mDownloadObserver.getId());
            } catch (Throwable t) {
                sLogger.d("Error aborting the download", t);
            }
        }
    }

    public void abortUpload() {
        sLogger.d("abortUpload: " + this.mUploadAborted.get());
        if (!this.mUploadAborted.compareAndSet(false, true)) {
            sLogger.e("Cannot abort upload as it is already aborted");
        } else if (this.mRemoteFileTransferManager != null) {
            this.mRemoteFileTransferManager.cancelFileUpload();
        }
    }

    private static java.lang.String[] parseS3Url(java.lang.String url) throws java.lang.IllegalArgumentException {
        if (url == null || !url.startsWith(com.navdy.client.debug.util.S3Constants.S3_PROTOCOL)) {
            throw new java.lang.IllegalArgumentException("Invalid url");
        }
        java.lang.String shortForm = url.substring(com.navdy.client.debug.util.S3Constants.S3_PROTOCOL.length());
        return new java.lang.String[]{shortForm.substring(0, shortForm.indexOf(com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER)), shortForm.substring(shortForm.indexOf(com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER) + 1)};
    }

    private com.navdy.client.ota.model.UpdateInfo fetchUpdateInfo(java.lang.String bucket, java.lang.String fileName) throws java.lang.Exception {
        java.lang.String jsonString = com.navdy.service.library.util.IOUtils.convertInputStreamToString(this.mClient.getObject(bucket, fileName).getObjectContent(), "UTF-8");
        sLogger.v("Fetched update info: " + jsonString);
        return (com.navdy.client.ota.model.UpdateInfo) new com.google.gson.Gson().fromJson(jsonString, com.navdy.client.ota.model.UpdateInfo.class);
    }

    private java.lang.String fetchMetaData(java.lang.String bucket, java.lang.String fileName) {
        try {
            return com.navdy.service.library.util.IOUtils.convertInputStreamToString(this.mClient.getObject(bucket, fileName).getObjectContent(), "UTF-8");
        } catch (Throwable t) {
            sLogger.e("Error while reading the meta data ", t);
            return null;
        }
    }
}
