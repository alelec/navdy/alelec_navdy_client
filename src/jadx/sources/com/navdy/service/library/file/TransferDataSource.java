package com.navdy.service.library.file;

public abstract class TransferDataSource {
    public static final java.lang.String TEST_DATA_NAME = "<TESTDATA>";
    private static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.file.TransferDataSource.class);
    protected long mCurOffset;

    private static class FileTransferDataSource extends com.navdy.service.library.file.TransferDataSource {
        java.io.File mFile;

        FileTransferDataSource(java.io.File file) {
            this.mFile = file;
        }

        public long length() {
            return this.mFile.length();
        }

        public int read(byte[] b) throws java.lang.Exception {
            java.io.RandomAccessFile f = null;
            int res = -1;
            try {
                java.io.RandomAccessFile f2 = new java.io.RandomAccessFile(this.mFile, "r");
                try {
                    f2.seek(this.mCurOffset);
                    int res2 = f2.read(b);
                    com.navdy.service.library.util.IOUtils.closeObject(f2);
                    java.io.RandomAccessFile randomAccessFile = f2;
                    return res2;
                } catch (Throwable th) {
                    f = f2;
                    com.navdy.service.library.util.IOUtils.closeObject(f);
                    return res;
                }
            } catch (Throwable th2) {
                com.navdy.service.library.util.IOUtils.closeObject(f);
                return res;
            }
        }

        public java.lang.String checkSum() {
            return com.navdy.service.library.util.IOUtils.hashForFile(this.mFile);
        }

        public java.lang.String checkSum(long length) {
            return com.navdy.service.library.util.IOUtils.hashForFile(this.mFile, length);
        }

        public java.lang.String getName() {
            return this.mFile.getName();
        }

        public java.io.File getFile() {
            return this.mFile;
        }
    }

    private static class TestDataSource extends com.navdy.service.library.file.TransferDataSource {
        static final int DATA_BUFFER_LEN = 4096;
        byte[] mDataBuffer = new byte[4096];
        long mLength;

        TestDataSource(long len) {
            this.mLength = len;
            new java.util.Random().nextBytes(this.mDataBuffer);
        }

        public long length() {
            return this.mLength;
        }

        public int read(byte[] b) {
            int di = (int) (this.mLength % 4096);
            long remaining = this.mLength - this.mCurOffset;
            int len = remaining >= ((long) b.length) ? b.length : (int) remaining;
            for (int i = 0; i < len; i++) {
                b[i] = this.mDataBuffer[di];
                di = (di + 1) % 4096;
            }
            return len;
        }

        public java.lang.String checkSum() {
            return checkSum(this.mLength);
        }

        public java.lang.String checkSum(long length) {
            long offset = 0;
            if (length > this.mLength) {
                length = this.mLength;
            }
            try {
                java.security.MessageDigest msgDigest = java.security.MessageDigest.getInstance("MD5");
                while (offset < length) {
                    long remaining = length - offset;
                    int l = remaining < 4096 ? (int) remaining : 4096;
                    msgDigest.update(this.mDataBuffer, 0, l);
                    offset += (long) l;
                }
                return com.navdy.service.library.util.IOUtils.bytesToHexString(msgDigest.digest());
            } catch (java.security.NoSuchAlgorithmException e) {
                com.navdy.service.library.file.TransferDataSource.sLogger.e("unable to get MD5 algorithm");
                return null;
            }
        }

        public java.lang.String getName() {
            return com.navdy.service.library.file.TransferDataSource.TEST_DATA_NAME;
        }

        public java.io.File getFile() {
            return null;
        }
    }

    public abstract java.lang.String checkSum();

    public abstract java.lang.String checkSum(long j);

    public abstract java.io.File getFile();

    public abstract java.lang.String getName();

    public abstract long length();

    public abstract int read(byte[] bArr) throws java.lang.Exception;

    public void seek(long offset) {
        this.mCurOffset = offset;
    }

    public static com.navdy.service.library.file.TransferDataSource fileSource(java.io.File f) {
        return new com.navdy.service.library.file.TransferDataSource.FileTransferDataSource(f);
    }

    public static com.navdy.service.library.file.TransferDataSource testSource(long length) {
        return new com.navdy.service.library.file.TransferDataSource.TestDataSource(length);
    }
}
