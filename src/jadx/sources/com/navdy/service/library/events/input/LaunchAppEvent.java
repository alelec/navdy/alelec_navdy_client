package com.navdy.service.library.events.input;

public final class LaunchAppEvent extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_APPBUNDLEID = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String appBundleID;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.input.LaunchAppEvent> {
        public java.lang.String appBundleID;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.input.LaunchAppEvent message) {
            super(message);
            if (message != null) {
                this.appBundleID = message.appBundleID;
            }
        }

        public com.navdy.service.library.events.input.LaunchAppEvent.Builder appBundleID(java.lang.String appBundleID2) {
            this.appBundleID = appBundleID2;
            return this;
        }

        public com.navdy.service.library.events.input.LaunchAppEvent build() {
            return new com.navdy.service.library.events.input.LaunchAppEvent(this);
        }
    }

    public LaunchAppEvent(java.lang.String appBundleID2) {
        this.appBundleID = appBundleID2;
    }

    private LaunchAppEvent(com.navdy.service.library.events.input.LaunchAppEvent.Builder builder) {
        this(builder.appBundleID);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.input.LaunchAppEvent)) {
            return false;
        }
        return equals((java.lang.Object) this.appBundleID, (java.lang.Object) ((com.navdy.service.library.events.input.LaunchAppEvent) other).appBundleID);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.appBundleID != null ? this.appBundleID.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
