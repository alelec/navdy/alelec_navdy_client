package com.navdy.service.library.events;

public class NavdyEventUtil {
    private static final int EXTENSION_BASE = 101;
    private static java.util.HashMap<java.lang.Class, com.squareup.wire.Extension> classToExtensionMap = new java.util.HashMap<>();
    private static int minTag;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.events.NavdyEventUtil.class);
    private static com.squareup.wire.Extension[] tagToExtensionMap = cacheExtensions();

    public static abstract class Initializer<T extends com.squareup.wire.Message> {
        public abstract T build(com.squareup.wire.Message.Builder<T> builder);
    }

    private static com.squareup.wire.Extension[] cacheExtensions() {
        java.lang.reflect.Field[] declaredFields;
        java.util.List<com.squareup.wire.Extension> list = new java.util.ArrayList<>();
        minTag = Integer.MAX_VALUE;
        int maxTag = -1;
        for (java.lang.reflect.Field field : com.navdy.service.library.events.Ext_NavdyEvent.class.getDeclaredFields()) {
            if (field.getType().equals(com.squareup.wire.Extension.class)) {
                try {
                    com.squareup.wire.Extension extension = (com.squareup.wire.Extension) field.get(null);
                    classToExtensionMap.put(extension.getMessageType(), extension);
                    int tag = extension.getTag();
                    if (tag < minTag) {
                        minTag = tag;
                    }
                    if (tag > maxTag) {
                        maxTag = tag;
                    }
                    list.add(extension);
                } catch (java.lang.IllegalAccessException e) {
                    sLogger.d("Skipping field " + field.getName());
                }
            }
        }
        com.squareup.wire.Extension[] result = new com.squareup.wire.Extension[((maxTag - minTag) + 1)];
        for (com.squareup.wire.Extension extension2 : list) {
            result[extension2.getTag() - minTag] = extension2;
        }
        return result;
    }

    private static com.squareup.wire.Extension findExtension(com.squareup.wire.Message message) {
        return (com.squareup.wire.Extension) classToExtensionMap.get(message.getClass());
    }

    private static com.squareup.wire.Extension findExtension(com.navdy.service.library.events.NavdyEvent event) {
        if (event.type == null) {
            return null;
        }
        int offset = (event.type.ordinal() + 101) - minTag;
        if (offset < 0 || offset >= tagToExtensionMap.length) {
            return null;
        }
        return tagToExtensionMap[offset];
    }

    public static com.navdy.service.library.events.NavdyEvent eventFromMessage(com.squareup.wire.Message message) {
        com.squareup.wire.Extension extension = findExtension(message);
        if (extension == null) {
            sLogger.e("Not a valid NavdyEvent protobuf message: " + message);
            throw new java.lang.IllegalArgumentException("Not a valid NavdyEvent protobuf message: " + message);
        }
        return new com.navdy.service.library.events.NavdyEvent.Builder().type((com.navdy.service.library.events.NavdyEvent.MessageType) com.squareup.wire.Message.enumFromInt(com.navdy.service.library.events.NavdyEvent.MessageType.class, extension.getTag() - 100)).setExtension(extension, message).build();
    }

    public static com.squareup.wire.Message messageFromEvent(com.navdy.service.library.events.NavdyEvent event) {
        com.squareup.wire.Extension extension = findExtension(event);
        if (extension != null) {
            return (com.squareup.wire.Message) event.getExtension(extension);
        }
        sLogger.e("Unable to find extension for event: " + event);
        return null;
    }

    public static <T extends com.squareup.wire.Message> T applyDefaults(T message) {
        return merge(message, getDefault(message.getClass()));
    }

    public static <T extends com.squareup.wire.Message> T getDefault(java.lang.Class<T> type) {
        return getDefault(type, null);
    }

    public static <T extends com.squareup.wire.Message> T getDefault(java.lang.Class<T> type, com.navdy.service.library.events.NavdyEventUtil.Initializer<T> initializer) {
        java.lang.reflect.Field[] declaredFields;
        if (type == null) {
            return null;
        }
        java.lang.Class builderType = null;
        com.squareup.wire.Message.Builder builder = null;
        try {
            builderType = getBuilder(type);
            builder = (com.squareup.wire.Message.Builder) builderType.getConstructor(new java.lang.Class[0]).newInstance(new java.lang.Object[0]);
        } catch (java.lang.ClassNotFoundException e) {
            sLogger.e(e.getMessage(), e);
        } catch (java.lang.reflect.InvocationTargetException e2) {
            sLogger.e(e2.getMessage(), e2);
        } catch (java.lang.NoSuchMethodException e3) {
            sLogger.e(e3.getMessage(), e3);
        } catch (java.lang.InstantiationException e4) {
            sLogger.e(e4.getMessage(), e4);
        } catch (java.lang.IllegalAccessException e5) {
            sLogger.e(e5.getMessage(), e5);
        }
        if (builderType == null || builder == null) {
            return null;
        }
        for (java.lang.reflect.Field field : builderType.getDeclaredFields()) {
            java.lang.Object resultingValue = null;
            try {
                if (com.squareup.wire.Message.class.isAssignableFrom(field.getType())) {
                    resultingValue = getDefault(field.getType());
                } else if (!java.lang.reflect.Modifier.isFinal(field.getModifiers()) && !field.getName().startsWith("$")) {
                    resultingValue = type.getField("DEFAULT_" + field.getName().toUpperCase()).get(null);
                }
                if (resultingValue != null) {
                    field.set(builder, resultingValue);
                }
            } catch (java.lang.IllegalAccessException e6) {
                sLogger.e(e6.getMessage(), e6);
            } catch (java.lang.NoSuchFieldException e7) {
                sLogger.e(e7.getMessage(), e7);
            }
        }
        if (initializer != null) {
            return initializer.build(builder);
        }
        return builder.build();
    }

    public static <T extends com.squareup.wire.Message> T merge(T msg, T defaultMsg) {
        java.lang.reflect.Field[] declaredFields;
        if (msg == null || defaultMsg == null) {
            return null;
        }
        java.lang.Class<? extends com.squareup.wire.Message> type = msg.getClass();
        com.squareup.wire.Message.Builder builder = null;
        java.lang.Object obj = null;
        java.lang.Class builderType = null;
        try {
            builderType = getBuilder(type);
            builder = (com.squareup.wire.Message.Builder) builderType.getConstructor(new java.lang.Class[]{type}).newInstance(new java.lang.Object[]{msg});
            obj = (com.squareup.wire.Message.Builder) builderType.getConstructor(new java.lang.Class[]{type}).newInstance(new java.lang.Object[]{defaultMsg});
        } catch (java.lang.InstantiationException e) {
            sLogger.e(e.getMessage(), e);
        } catch (java.lang.IllegalAccessException e2) {
            sLogger.e(e2.getMessage(), e2);
        } catch (java.lang.reflect.InvocationTargetException e3) {
            sLogger.e(e3.getMessage(), e3);
        } catch (java.lang.NoSuchMethodException e4) {
            sLogger.e(e4.getMessage(), e4);
        } catch (java.lang.ClassNotFoundException e5) {
            sLogger.e(e5.getMessage(), e5);
        }
        if (builderType == null || builder == null || obj == null) {
            return null;
        }
        for (java.lang.reflect.Field field : builderType.getDeclaredFields()) {
            try {
                java.lang.Object messageFieldValue = field.get(builder);
                java.lang.Object defaultMessageFieldValue = field.get(obj);
                java.lang.Object resultingValue = null;
                if (com.squareup.wire.Message.class.isAssignableFrom(field.getType())) {
                    resultingValue = merge((com.squareup.wire.Message) messageFieldValue, (com.squareup.wire.Message) defaultMessageFieldValue);
                } else if (!java.lang.reflect.Modifier.isFinal(field.getModifiers())) {
                    resultingValue = com.squareup.wire.Wire.get(messageFieldValue, defaultMessageFieldValue);
                }
                if (resultingValue != null) {
                    field.set(builder, resultingValue);
                }
            } catch (java.lang.IllegalAccessException e6) {
                sLogger.e(e6.getMessage(), e6);
            }
        }
        return builder.build();
    }

    private static <T extends com.squareup.wire.Message> java.lang.Class getBuilder(java.lang.Class<T> type) throws java.lang.ClassNotFoundException {
        if (type == null) {
            return null;
        }
        return java.lang.Class.forName(type.getName() + "$Builder");
    }
}
