package com.navdy.service.library.events.navigation;

public final class NavigationManeuverEvent extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_CURRENTROAD = "";
    public static final java.lang.String DEFAULT_DISTANCE_TO_PENDING_TURN = "";
    public static final java.lang.String DEFAULT_ETA = "";
    public static final java.lang.Long DEFAULT_ETAUTCTIME = java.lang.Long.valueOf(0);
    public static final com.navdy.service.library.events.navigation.NavigationSessionState DEFAULT_NAVIGATIONSTATE = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
    public static final java.lang.String DEFAULT_PENDING_STREET = "";
    public static final com.navdy.service.library.events.navigation.NavigationTurn DEFAULT_PENDING_TURN = com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_START;
    public static final java.lang.String DEFAULT_SPEED = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String currentRoad;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String distance_to_pending_turn;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 5, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String eta;
    @com.squareup.wire.ProtoField(tag = 8, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long etaUtcTime;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.navigation.NavigationSessionState navigationState;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String pending_street;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.navigation.NavigationTurn pending_turn;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 6, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String speed;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.navigation.NavigationManeuverEvent> {
        public java.lang.String currentRoad;
        public java.lang.String distance_to_pending_turn;
        public java.lang.String eta;
        public java.lang.Long etaUtcTime;
        public com.navdy.service.library.events.navigation.NavigationSessionState navigationState;
        public java.lang.String pending_street;
        public com.navdy.service.library.events.navigation.NavigationTurn pending_turn;
        public java.lang.String speed;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.navigation.NavigationManeuverEvent message) {
            super(message);
            if (message != null) {
                this.currentRoad = message.currentRoad;
                this.pending_turn = message.pending_turn;
                this.distance_to_pending_turn = message.distance_to_pending_turn;
                this.pending_street = message.pending_street;
                this.eta = message.eta;
                this.speed = message.speed;
                this.navigationState = message.navigationState;
                this.etaUtcTime = message.etaUtcTime;
            }
        }

        public com.navdy.service.library.events.navigation.NavigationManeuverEvent.Builder currentRoad(java.lang.String currentRoad2) {
            this.currentRoad = currentRoad2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationManeuverEvent.Builder pending_turn(com.navdy.service.library.events.navigation.NavigationTurn pending_turn2) {
            this.pending_turn = pending_turn2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationManeuverEvent.Builder distance_to_pending_turn(java.lang.String distance_to_pending_turn2) {
            this.distance_to_pending_turn = distance_to_pending_turn2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationManeuverEvent.Builder pending_street(java.lang.String pending_street2) {
            this.pending_street = pending_street2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationManeuverEvent.Builder eta(java.lang.String eta2) {
            this.eta = eta2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationManeuverEvent.Builder speed(java.lang.String speed2) {
            this.speed = speed2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationManeuverEvent.Builder navigationState(com.navdy.service.library.events.navigation.NavigationSessionState navigationState2) {
            this.navigationState = navigationState2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationManeuverEvent.Builder etaUtcTime(java.lang.Long etaUtcTime2) {
            this.etaUtcTime = etaUtcTime2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationManeuverEvent build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.navigation.NavigationManeuverEvent(this);
        }
    }

    public NavigationManeuverEvent(java.lang.String currentRoad2, com.navdy.service.library.events.navigation.NavigationTurn pending_turn2, java.lang.String distance_to_pending_turn2, java.lang.String pending_street2, java.lang.String eta2, java.lang.String speed2, com.navdy.service.library.events.navigation.NavigationSessionState navigationState2, java.lang.Long etaUtcTime2) {
        this.currentRoad = currentRoad2;
        this.pending_turn = pending_turn2;
        this.distance_to_pending_turn = distance_to_pending_turn2;
        this.pending_street = pending_street2;
        this.eta = eta2;
        this.speed = speed2;
        this.navigationState = navigationState2;
        this.etaUtcTime = etaUtcTime2;
    }

    private NavigationManeuverEvent(com.navdy.service.library.events.navigation.NavigationManeuverEvent.Builder builder) {
        this(builder.currentRoad, builder.pending_turn, builder.distance_to_pending_turn, builder.pending_street, builder.eta, builder.speed, builder.navigationState, builder.etaUtcTime);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.navigation.NavigationManeuverEvent)) {
            return false;
        }
        com.navdy.service.library.events.navigation.NavigationManeuverEvent o = (com.navdy.service.library.events.navigation.NavigationManeuverEvent) other;
        if (!equals((java.lang.Object) this.currentRoad, (java.lang.Object) o.currentRoad) || !equals((java.lang.Object) this.pending_turn, (java.lang.Object) o.pending_turn) || !equals((java.lang.Object) this.distance_to_pending_turn, (java.lang.Object) o.distance_to_pending_turn) || !equals((java.lang.Object) this.pending_street, (java.lang.Object) o.pending_street) || !equals((java.lang.Object) this.eta, (java.lang.Object) o.eta) || !equals((java.lang.Object) this.speed, (java.lang.Object) o.speed) || !equals((java.lang.Object) this.navigationState, (java.lang.Object) o.navigationState) || !equals((java.lang.Object) this.etaUtcTime, (java.lang.Object) o.etaUtcTime)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.currentRoad != null ? this.currentRoad.hashCode() : 0) * 37;
        if (this.pending_turn != null) {
            i = this.pending_turn.hashCode();
        } else {
            i = 0;
        }
        int i8 = (hashCode + i) * 37;
        if (this.distance_to_pending_turn != null) {
            i2 = this.distance_to_pending_turn.hashCode();
        } else {
            i2 = 0;
        }
        int i9 = (i8 + i2) * 37;
        if (this.pending_street != null) {
            i3 = this.pending_street.hashCode();
        } else {
            i3 = 0;
        }
        int i10 = (i9 + i3) * 37;
        if (this.eta != null) {
            i4 = this.eta.hashCode();
        } else {
            i4 = 0;
        }
        int i11 = (i10 + i4) * 37;
        if (this.speed != null) {
            i5 = this.speed.hashCode();
        } else {
            i5 = 0;
        }
        int i12 = (i11 + i5) * 37;
        if (this.navigationState != null) {
            i6 = this.navigationState.hashCode();
        } else {
            i6 = 0;
        }
        int i13 = (i12 + i6) * 37;
        if (this.etaUtcTime != null) {
            i7 = this.etaUtcTime.hashCode();
        }
        int result2 = i13 + i7;
        this.hashCode = result2;
        return result2;
    }
}
