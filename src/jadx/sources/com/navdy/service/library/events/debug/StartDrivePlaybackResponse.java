package com.navdy.service.library.events.debug;

public final class StartDrivePlaybackResponse extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.debug.StartDrivePlaybackResponse> {
        public com.navdy.service.library.events.RequestStatus status;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.debug.StartDrivePlaybackResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
            }
        }

        public com.navdy.service.library.events.debug.StartDrivePlaybackResponse.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.debug.StartDrivePlaybackResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.debug.StartDrivePlaybackResponse(this);
        }
    }

    public StartDrivePlaybackResponse(com.navdy.service.library.events.RequestStatus status2) {
        this.status = status2;
    }

    private StartDrivePlaybackResponse(com.navdy.service.library.events.debug.StartDrivePlaybackResponse.Builder builder) {
        this(builder.status);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.debug.StartDrivePlaybackResponse)) {
            return false;
        }
        return equals((java.lang.Object) this.status, (java.lang.Object) ((com.navdy.service.library.events.debug.StartDrivePlaybackResponse) other).status);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.status != null ? this.status.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
