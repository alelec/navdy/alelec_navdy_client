package com.navdy.service.library.events.ui;

public final class ShowScreen extends com.squareup.wire.Message {
    public static final java.util.List<com.navdy.service.library.events.glances.KeyValue> DEFAULT_ARGUMENTS = java.util.Collections.emptyList();
    public static final com.navdy.service.library.events.ui.Screen DEFAULT_SCREEN = com.navdy.service.library.events.ui.Screen.SCREEN_DASHBOARD;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.glances.KeyValue.class, tag = 2)
    public final java.util.List<com.navdy.service.library.events.glances.KeyValue> arguments;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.ui.Screen screen;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.ui.ShowScreen> {
        public java.util.List<com.navdy.service.library.events.glances.KeyValue> arguments;
        public com.navdy.service.library.events.ui.Screen screen;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.ui.ShowScreen message) {
            super(message);
            if (message != null) {
                this.screen = message.screen;
                this.arguments = com.navdy.service.library.events.ui.ShowScreen.copyOf(message.arguments);
            }
        }

        public com.navdy.service.library.events.ui.ShowScreen.Builder screen(com.navdy.service.library.events.ui.Screen screen2) {
            this.screen = screen2;
            return this;
        }

        public com.navdy.service.library.events.ui.ShowScreen.Builder arguments(java.util.List<com.navdy.service.library.events.glances.KeyValue> arguments2) {
            this.arguments = checkForNulls(arguments2);
            return this;
        }

        public com.navdy.service.library.events.ui.ShowScreen build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.ui.ShowScreen(this);
        }
    }

    public ShowScreen(com.navdy.service.library.events.ui.Screen screen2, java.util.List<com.navdy.service.library.events.glances.KeyValue> arguments2) {
        this.screen = screen2;
        this.arguments = immutableCopyOf(arguments2);
    }

    private ShowScreen(com.navdy.service.library.events.ui.ShowScreen.Builder builder) {
        this(builder.screen, builder.arguments);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.ui.ShowScreen)) {
            return false;
        }
        com.navdy.service.library.events.ui.ShowScreen o = (com.navdy.service.library.events.ui.ShowScreen) other;
        if (!equals((java.lang.Object) this.screen, (java.lang.Object) o.screen) || !equals(this.arguments, o.arguments)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int result2 = ((this.screen != null ? this.screen.hashCode() : 0) * 37) + (this.arguments != null ? this.arguments.hashCode() : 1);
        this.hashCode = result2;
        return result2;
    }
}
