package com.navdy.service.library.events.audio;

public final class MusicArtworkResponse extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_ALBUM = "";
    public static final java.lang.String DEFAULT_AUTHOR = "";
    public static final java.lang.String DEFAULT_COLLECTIONID = "";
    public static final com.navdy.service.library.events.audio.MusicCollectionSource DEFAULT_COLLECTIONSOURCE = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    public static final com.navdy.service.library.events.audio.MusicCollectionType DEFAULT_COLLECTIONTYPE = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    public static final java.lang.String DEFAULT_NAME = "";
    public static final okio.ByteString DEFAULT_PHOTO = okio.ByteString.EMPTY;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String album;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String author;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String collectionId;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String name;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.BYTES)
    public final okio.ByteString photo;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.MusicArtworkResponse> {
        public java.lang.String album;
        public java.lang.String author;
        public java.lang.String collectionId;
        public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
        public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
        public java.lang.String name;
        public okio.ByteString photo;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.MusicArtworkResponse message) {
            super(message);
            if (message != null) {
                this.collectionSource = message.collectionSource;
                this.name = message.name;
                this.album = message.album;
                this.author = message.author;
                this.photo = message.photo;
                this.collectionType = message.collectionType;
                this.collectionId = message.collectionId;
            }
        }

        public com.navdy.service.library.events.audio.MusicArtworkResponse.Builder collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource collectionSource2) {
            this.collectionSource = collectionSource2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicArtworkResponse.Builder name(java.lang.String name2) {
            this.name = name2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicArtworkResponse.Builder album(java.lang.String album2) {
            this.album = album2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicArtworkResponse.Builder author(java.lang.String author2) {
            this.author = author2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicArtworkResponse.Builder photo(okio.ByteString photo2) {
            this.photo = photo2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicArtworkResponse.Builder collectionType(com.navdy.service.library.events.audio.MusicCollectionType collectionType2) {
            this.collectionType = collectionType2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicArtworkResponse.Builder collectionId(java.lang.String collectionId2) {
            this.collectionId = collectionId2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicArtworkResponse build() {
            return new com.navdy.service.library.events.audio.MusicArtworkResponse(this);
        }
    }

    public MusicArtworkResponse(com.navdy.service.library.events.audio.MusicCollectionSource collectionSource2, java.lang.String name2, java.lang.String album2, java.lang.String author2, okio.ByteString photo2, com.navdy.service.library.events.audio.MusicCollectionType collectionType2, java.lang.String collectionId2) {
        this.collectionSource = collectionSource2;
        this.name = name2;
        this.album = album2;
        this.author = author2;
        this.photo = photo2;
        this.collectionType = collectionType2;
        this.collectionId = collectionId2;
    }

    private MusicArtworkResponse(com.navdy.service.library.events.audio.MusicArtworkResponse.Builder builder) {
        this(builder.collectionSource, builder.name, builder.album, builder.author, builder.photo, builder.collectionType, builder.collectionId);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.MusicArtworkResponse)) {
            return false;
        }
        com.navdy.service.library.events.audio.MusicArtworkResponse o = (com.navdy.service.library.events.audio.MusicArtworkResponse) other;
        if (!equals((java.lang.Object) this.collectionSource, (java.lang.Object) o.collectionSource) || !equals((java.lang.Object) this.name, (java.lang.Object) o.name) || !equals((java.lang.Object) this.album, (java.lang.Object) o.album) || !equals((java.lang.Object) this.author, (java.lang.Object) o.author) || !equals((java.lang.Object) this.photo, (java.lang.Object) o.photo) || !equals((java.lang.Object) this.collectionType, (java.lang.Object) o.collectionType) || !equals((java.lang.Object) this.collectionId, (java.lang.Object) o.collectionId)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.collectionSource != null ? this.collectionSource.hashCode() : 0) * 37;
        if (this.name != null) {
            i = this.name.hashCode();
        } else {
            i = 0;
        }
        int i7 = (hashCode + i) * 37;
        if (this.album != null) {
            i2 = this.album.hashCode();
        } else {
            i2 = 0;
        }
        int i8 = (i7 + i2) * 37;
        if (this.author != null) {
            i3 = this.author.hashCode();
        } else {
            i3 = 0;
        }
        int i9 = (i8 + i3) * 37;
        if (this.photo != null) {
            i4 = this.photo.hashCode();
        } else {
            i4 = 0;
        }
        int i10 = (i9 + i4) * 37;
        if (this.collectionType != null) {
            i5 = this.collectionType.hashCode();
        } else {
            i5 = 0;
        }
        int i11 = (i10 + i5) * 37;
        if (this.collectionId != null) {
            i6 = this.collectionId.hashCode();
        }
        int result2 = i11 + i6;
        this.hashCode = result2;
        return result2;
    }
}
