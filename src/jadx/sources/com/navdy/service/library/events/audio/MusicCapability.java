package com.navdy.service.library.events.audio;

public final class MusicCapability extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.audio.MusicCapability.MusicAuthorizationStatus DEFAULT_AUTHORIZATIONSTATUS = com.navdy.service.library.events.audio.MusicCapability.MusicAuthorizationStatus.MUSIC_AUTHORIZATION_NOT_DETERMINED;
    public static final com.navdy.service.library.events.audio.MusicCollectionSource DEFAULT_COLLECTIONSOURCE = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    public static final java.util.List<com.navdy.service.library.events.audio.MusicCollectionType> DEFAULT_COLLECTIONTYPES = java.util.Collections.emptyList();
    public static final java.lang.Long DEFAULT_SERIAL_NUMBER = java.lang.Long.valueOf(0);
    public static final java.util.List<com.navdy.service.library.events.audio.MusicRepeatMode> DEFAULT_SUPPORTEDREPEATMODES = java.util.Collections.emptyList();
    public static final java.util.List<com.navdy.service.library.events.audio.MusicShuffleMode> DEFAULT_SUPPORTEDSHUFFLEMODES = java.util.Collections.emptyList();
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicCapability.MusicAuthorizationStatus authorizationStatus;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    @com.squareup.wire.ProtoField(enumType = com.navdy.service.library.events.audio.MusicCollectionType.class, label = com.squareup.wire.Message.Label.REPEATED, tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final java.util.List<com.navdy.service.library.events.audio.MusicCollectionType> collectionTypes;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long serial_number;
    @com.squareup.wire.ProtoField(enumType = com.navdy.service.library.events.audio.MusicRepeatMode.class, label = com.squareup.wire.Message.Label.REPEATED, tag = 5, type = com.squareup.wire.Message.Datatype.ENUM)
    public final java.util.List<com.navdy.service.library.events.audio.MusicRepeatMode> supportedRepeatModes;
    @com.squareup.wire.ProtoField(enumType = com.navdy.service.library.events.audio.MusicShuffleMode.class, label = com.squareup.wire.Message.Label.REPEATED, tag = 4, type = com.squareup.wire.Message.Datatype.ENUM)
    public final java.util.List<com.navdy.service.library.events.audio.MusicShuffleMode> supportedShuffleModes;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.MusicCapability> {
        public com.navdy.service.library.events.audio.MusicCapability.MusicAuthorizationStatus authorizationStatus;
        public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
        public java.util.List<com.navdy.service.library.events.audio.MusicCollectionType> collectionTypes;
        public java.lang.Long serial_number;
        public java.util.List<com.navdy.service.library.events.audio.MusicRepeatMode> supportedRepeatModes;
        public java.util.List<com.navdy.service.library.events.audio.MusicShuffleMode> supportedShuffleModes;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.MusicCapability message) {
            super(message);
            if (message != null) {
                this.collectionSource = message.collectionSource;
                this.collectionTypes = com.navdy.service.library.events.audio.MusicCapability.copyOf(message.collectionTypes);
                this.authorizationStatus = message.authorizationStatus;
                this.supportedShuffleModes = com.navdy.service.library.events.audio.MusicCapability.copyOf(message.supportedShuffleModes);
                this.supportedRepeatModes = com.navdy.service.library.events.audio.MusicCapability.copyOf(message.supportedRepeatModes);
                this.serial_number = message.serial_number;
            }
        }

        public com.navdy.service.library.events.audio.MusicCapability.Builder collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource collectionSource2) {
            this.collectionSource = collectionSource2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCapability.Builder collectionTypes(java.util.List<com.navdy.service.library.events.audio.MusicCollectionType> collectionTypes2) {
            this.collectionTypes = checkForNulls(collectionTypes2);
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCapability.Builder authorizationStatus(com.navdy.service.library.events.audio.MusicCapability.MusicAuthorizationStatus authorizationStatus2) {
            this.authorizationStatus = authorizationStatus2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCapability.Builder supportedShuffleModes(java.util.List<com.navdy.service.library.events.audio.MusicShuffleMode> supportedShuffleModes2) {
            this.supportedShuffleModes = checkForNulls(supportedShuffleModes2);
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCapability.Builder supportedRepeatModes(java.util.List<com.navdy.service.library.events.audio.MusicRepeatMode> supportedRepeatModes2) {
            this.supportedRepeatModes = checkForNulls(supportedRepeatModes2);
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCapability.Builder serial_number(java.lang.Long serial_number2) {
            this.serial_number = serial_number2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCapability build() {
            return new com.navdy.service.library.events.audio.MusicCapability(this);
        }
    }

    public enum MusicAuthorizationStatus implements com.squareup.wire.ProtoEnum {
        MUSIC_AUTHORIZATION_NOT_DETERMINED(1),
        MUSIC_AUTHORIZATION_AUTHORIZED(2),
        MUSIC_AUTHORIZATION_DENIED(3);
        
        private final int value;

        private MusicAuthorizationStatus(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public MusicCapability(com.navdy.service.library.events.audio.MusicCollectionSource collectionSource2, java.util.List<com.navdy.service.library.events.audio.MusicCollectionType> collectionTypes2, com.navdy.service.library.events.audio.MusicCapability.MusicAuthorizationStatus authorizationStatus2, java.util.List<com.navdy.service.library.events.audio.MusicShuffleMode> supportedShuffleModes2, java.util.List<com.navdy.service.library.events.audio.MusicRepeatMode> supportedRepeatModes2, java.lang.Long serial_number2) {
        this.collectionSource = collectionSource2;
        this.collectionTypes = immutableCopyOf(collectionTypes2);
        this.authorizationStatus = authorizationStatus2;
        this.supportedShuffleModes = immutableCopyOf(supportedShuffleModes2);
        this.supportedRepeatModes = immutableCopyOf(supportedRepeatModes2);
        this.serial_number = serial_number2;
    }

    private MusicCapability(com.navdy.service.library.events.audio.MusicCapability.Builder builder) {
        this(builder.collectionSource, builder.collectionTypes, builder.authorizationStatus, builder.supportedShuffleModes, builder.supportedRepeatModes, builder.serial_number);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.MusicCapability)) {
            return false;
        }
        com.navdy.service.library.events.audio.MusicCapability o = (com.navdy.service.library.events.audio.MusicCapability) other;
        if (!equals((java.lang.Object) this.collectionSource, (java.lang.Object) o.collectionSource) || !equals(this.collectionTypes, o.collectionTypes) || !equals((java.lang.Object) this.authorizationStatus, (java.lang.Object) o.authorizationStatus) || !equals(this.supportedShuffleModes, o.supportedShuffleModes) || !equals(this.supportedRepeatModes, o.supportedRepeatModes) || !equals((java.lang.Object) this.serial_number, (java.lang.Object) o.serial_number)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4 = 1;
        int i5 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.collectionSource != null ? this.collectionSource.hashCode() : 0) * 37;
        if (this.collectionTypes != null) {
            i = this.collectionTypes.hashCode();
        } else {
            i = 1;
        }
        int i6 = (hashCode + i) * 37;
        if (this.authorizationStatus != null) {
            i2 = this.authorizationStatus.hashCode();
        } else {
            i2 = 0;
        }
        int i7 = (i6 + i2) * 37;
        if (this.supportedShuffleModes != null) {
            i3 = this.supportedShuffleModes.hashCode();
        } else {
            i3 = 1;
        }
        int i8 = (i7 + i3) * 37;
        if (this.supportedRepeatModes != null) {
            i4 = this.supportedRepeatModes.hashCode();
        }
        int i9 = (i8 + i4) * 37;
        if (this.serial_number != null) {
            i5 = this.serial_number.hashCode();
        }
        int result2 = i9 + i5;
        this.hashCode = result2;
        return result2;
    }
}
