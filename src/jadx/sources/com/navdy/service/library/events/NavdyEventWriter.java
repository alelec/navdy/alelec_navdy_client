package com.navdy.service.library.events;

public class NavdyEventWriter {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.events.NavdyEventWriter.class);
    protected java.io.OutputStream mOutputStream;

    public NavdyEventWriter(java.io.OutputStream output) {
        if (output == null) {
            throw new java.lang.IllegalArgumentException();
        }
        this.mOutputStream = output;
    }

    public void write(byte[] eventData) throws java.io.IOException {
        this.mOutputStream.write(new com.navdy.service.library.events.Frame(java.lang.Integer.valueOf(eventData.length)).toByteArray());
        this.mOutputStream.write(eventData);
        this.mOutputStream.flush();
        if (sLogger.isLoggable(2)) {
            sLogger.v("[Outgoing-Event] size:" + eventData.length);
        }
    }
}
