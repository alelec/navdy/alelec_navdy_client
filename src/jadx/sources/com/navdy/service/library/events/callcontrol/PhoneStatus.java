package com.navdy.service.library.events.callcontrol;

public enum PhoneStatus implements com.squareup.wire.ProtoEnum {
    PHONE_IDLE(1),
    PHONE_RINGING(2),
    PHONE_OFFHOOK(3),
    PHONE_DIALING(4),
    PHONE_HELD(5),
    PHONE_CONNECTING(6),
    PHONE_DISCONNECTING(7);
    
    private final int value;

    private PhoneStatus(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
