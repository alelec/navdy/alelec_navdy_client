package com.navdy.service.library.events.preferences;

public final class DisplaySpeakerPreferences extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_FEEDBACK_SOUND = java.lang.Boolean.valueOf(true);
    public static final java.lang.Boolean DEFAULT_MASTER_SOUND = java.lang.Boolean.valueOf(true);
    public static final java.lang.Boolean DEFAULT_NOTIFICATION_SOUND = java.lang.Boolean.valueOf(true);
    public static final java.lang.Long DEFAULT_SERIAL_NUMBER = java.lang.Long.valueOf(0);
    public static final java.lang.Integer DEFAULT_VOLUME_LEVEL = java.lang.Integer.valueOf(50);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean feedback_sound;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean master_sound;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean notification_sound;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long serial_number;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer volume_level;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.preferences.DisplaySpeakerPreferences> {
        public java.lang.Boolean feedback_sound;
        public java.lang.Boolean master_sound;
        public java.lang.Boolean notification_sound;
        public java.lang.Long serial_number;
        public java.lang.Integer volume_level;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.preferences.DisplaySpeakerPreferences message) {
            super(message);
            if (message != null) {
                this.serial_number = message.serial_number;
                this.master_sound = message.master_sound;
                this.volume_level = message.volume_level;
                this.feedback_sound = message.feedback_sound;
                this.notification_sound = message.notification_sound;
            }
        }

        public com.navdy.service.library.events.preferences.DisplaySpeakerPreferences.Builder serial_number(java.lang.Long serial_number2) {
            this.serial_number = serial_number2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DisplaySpeakerPreferences.Builder master_sound(java.lang.Boolean master_sound2) {
            this.master_sound = master_sound2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DisplaySpeakerPreferences.Builder volume_level(java.lang.Integer volume_level2) {
            this.volume_level = volume_level2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DisplaySpeakerPreferences.Builder feedback_sound(java.lang.Boolean feedback_sound2) {
            this.feedback_sound = feedback_sound2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DisplaySpeakerPreferences.Builder notification_sound(java.lang.Boolean notification_sound2) {
            this.notification_sound = notification_sound2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DisplaySpeakerPreferences build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.preferences.DisplaySpeakerPreferences(this);
        }
    }

    public DisplaySpeakerPreferences(java.lang.Long serial_number2, java.lang.Boolean master_sound2, java.lang.Integer volume_level2, java.lang.Boolean feedback_sound2, java.lang.Boolean notification_sound2) {
        this.serial_number = serial_number2;
        this.master_sound = master_sound2;
        this.volume_level = volume_level2;
        this.feedback_sound = feedback_sound2;
        this.notification_sound = notification_sound2;
    }

    private DisplaySpeakerPreferences(com.navdy.service.library.events.preferences.DisplaySpeakerPreferences.Builder builder) {
        this(builder.serial_number, builder.master_sound, builder.volume_level, builder.feedback_sound, builder.notification_sound);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.preferences.DisplaySpeakerPreferences)) {
            return false;
        }
        com.navdy.service.library.events.preferences.DisplaySpeakerPreferences o = (com.navdy.service.library.events.preferences.DisplaySpeakerPreferences) other;
        if (!equals((java.lang.Object) this.serial_number, (java.lang.Object) o.serial_number) || !equals((java.lang.Object) this.master_sound, (java.lang.Object) o.master_sound) || !equals((java.lang.Object) this.volume_level, (java.lang.Object) o.volume_level) || !equals((java.lang.Object) this.feedback_sound, (java.lang.Object) o.feedback_sound) || !equals((java.lang.Object) this.notification_sound, (java.lang.Object) o.notification_sound)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.serial_number != null ? this.serial_number.hashCode() : 0) * 37;
        if (this.master_sound != null) {
            i = this.master_sound.hashCode();
        } else {
            i = 0;
        }
        int i5 = (hashCode + i) * 37;
        if (this.volume_level != null) {
            i2 = this.volume_level.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 37;
        if (this.feedback_sound != null) {
            i3 = this.feedback_sound.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (i6 + i3) * 37;
        if (this.notification_sound != null) {
            i4 = this.notification_sound.hashCode();
        }
        int result2 = i7 + i4;
        this.hashCode = result2;
        return result2;
    }
}
