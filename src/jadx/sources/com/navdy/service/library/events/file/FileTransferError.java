package com.navdy.service.library.events.file;

public enum FileTransferError implements com.squareup.wire.ProtoEnum {
    FILE_TRANSFER_NO_ERROR(1),
    FILE_TRANSFER_INSUFFICIENT_SPACE(2),
    FILE_TRANSFER_ABORTED(3),
    FILE_TRANSFER_IO_ERROR(4),
    FILE_TRANSFER_ILLEGAL_CHUNK(5),
    FILE_TRANSFER_NOT_INITIATED(6),
    FILE_TRANSFER_PERMISSION_DENIED(7),
    FILE_TRANSFER_CHECKSUM_ERROR(8),
    FILE_TRANSFER_HOST_BUSY(9);
    
    private final int value;

    private FileTransferError(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
