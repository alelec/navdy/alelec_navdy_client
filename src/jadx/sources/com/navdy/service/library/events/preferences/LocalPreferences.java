package com.navdy.service.library.events.preferences;

public final class LocalPreferences extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.settings.DateTimeConfiguration.Clock DEFAULT_CLOCKFORMAT = com.navdy.service.library.events.settings.DateTimeConfiguration.Clock.CLOCK_24_HOUR;
    public static final java.lang.Boolean DEFAULT_MANUALZOOM = java.lang.Boolean.valueOf(false);
    public static final java.lang.Float DEFAULT_MANUALZOOMLEVEL = java.lang.Float.valueOf(0.0f);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.settings.DateTimeConfiguration.Clock clockFormat;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean manualZoom;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.FLOAT)
    public final java.lang.Float manualZoomLevel;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.preferences.LocalPreferences> {
        public com.navdy.service.library.events.settings.DateTimeConfiguration.Clock clockFormat;
        public java.lang.Boolean manualZoom;
        public java.lang.Float manualZoomLevel;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.preferences.LocalPreferences message) {
            super(message);
            if (message != null) {
                this.manualZoom = message.manualZoom;
                this.manualZoomLevel = message.manualZoomLevel;
                this.clockFormat = message.clockFormat;
            }
        }

        public com.navdy.service.library.events.preferences.LocalPreferences.Builder manualZoom(java.lang.Boolean manualZoom2) {
            this.manualZoom = manualZoom2;
            return this;
        }

        public com.navdy.service.library.events.preferences.LocalPreferences.Builder manualZoomLevel(java.lang.Float manualZoomLevel2) {
            this.manualZoomLevel = manualZoomLevel2;
            return this;
        }

        public com.navdy.service.library.events.preferences.LocalPreferences.Builder clockFormat(com.navdy.service.library.events.settings.DateTimeConfiguration.Clock clockFormat2) {
            this.clockFormat = clockFormat2;
            return this;
        }

        public com.navdy.service.library.events.preferences.LocalPreferences build() {
            return new com.navdy.service.library.events.preferences.LocalPreferences(this);
        }
    }

    public LocalPreferences(java.lang.Boolean manualZoom2, java.lang.Float manualZoomLevel2, com.navdy.service.library.events.settings.DateTimeConfiguration.Clock clockFormat2) {
        this.manualZoom = manualZoom2;
        this.manualZoomLevel = manualZoomLevel2;
        this.clockFormat = clockFormat2;
    }

    private LocalPreferences(com.navdy.service.library.events.preferences.LocalPreferences.Builder builder) {
        this(builder.manualZoom, builder.manualZoomLevel, builder.clockFormat);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.preferences.LocalPreferences)) {
            return false;
        }
        com.navdy.service.library.events.preferences.LocalPreferences o = (com.navdy.service.library.events.preferences.LocalPreferences) other;
        if (!equals((java.lang.Object) this.manualZoom, (java.lang.Object) o.manualZoom) || !equals((java.lang.Object) this.manualZoomLevel, (java.lang.Object) o.manualZoomLevel) || !equals((java.lang.Object) this.clockFormat, (java.lang.Object) o.clockFormat)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.manualZoom != null ? this.manualZoom.hashCode() : 0) * 37;
        if (this.manualZoomLevel != null) {
            i = this.manualZoomLevel.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.clockFormat != null) {
            i2 = this.clockFormat.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
