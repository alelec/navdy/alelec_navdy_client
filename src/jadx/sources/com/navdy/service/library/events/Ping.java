package com.navdy.service.library.events;

public final class Ping extends com.squareup.wire.Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.Ping> {
        public Builder() {
        }

        public Builder(com.navdy.service.library.events.Ping message) {
            super(message);
        }

        public com.navdy.service.library.events.Ping build() {
            return new com.navdy.service.library.events.Ping(this);
        }
    }

    public Ping() {
    }

    private Ping(com.navdy.service.library.events.Ping.Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        return other instanceof com.navdy.service.library.events.Ping;
    }

    public int hashCode() {
        return 0;
    }
}
