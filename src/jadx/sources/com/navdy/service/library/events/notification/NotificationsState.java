package com.navdy.service.library.events.notification;

public enum NotificationsState implements com.squareup.wire.ProtoEnum {
    NOTIFICATIONS_ENABLED(1),
    NOTIFICATIONS_STOPPED(2),
    NOTIFICATIONS_CONNECTING(3),
    NOTIFICATIONS_PAIRING_FAILED(4),
    NOTIFICATIONS_SERVICE_UNAVAILABLE(5);
    
    private final int value;

    private NotificationsState(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
