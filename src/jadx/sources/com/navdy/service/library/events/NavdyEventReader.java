package com.navdy.service.library.events;

public class NavdyEventReader {
    private static final int FRAME_LENGTH = 5;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.events.NavdyEventReader.class);
    protected java.io.DataInputStream mInputStream;
    protected com.squareup.wire.Wire mWire = new com.squareup.wire.Wire((java.lang.Class<?>[]) new java.lang.Class[]{com.navdy.service.library.events.Ext_NavdyEvent.class});

    public NavdyEventReader(java.io.InputStream input) {
        this.mInputStream = new java.io.DataInputStream(input);
    }

    public byte[] readBytes() throws java.io.IOException {
        int size = readSize();
        if (size <= 0) {
            return null;
        }
        return readBytes(size);
    }

    private int readSize() throws java.io.IOException {
        try {
            com.navdy.service.library.events.Frame frame = null;
            try {
                frame = (com.navdy.service.library.events.Frame) this.mWire.parseFrom(readBytes(5), com.navdy.service.library.events.Frame.class);
            } catch (Throwable t) {
                sLogger.e("Failed to parse frame", t);
            }
            if (frame == null) {
                return 0;
            }
            return frame.size.intValue();
        } catch (java.lang.Exception e) {
            return 0;
        }
    }

    private byte[] readBytes(int size) throws java.io.IOException {
        byte[] buf = new byte[size];
        this.mInputStream.readFully(buf);
        return buf;
    }
}
