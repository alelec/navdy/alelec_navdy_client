package com.navdy.service.library.events;

public final class TripUpdate extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_ARRIVED_AT_DESTINATION_ID = "";
    public static final java.lang.Float DEFAULT_BEARING = java.lang.Float.valueOf(0.0f);
    public static final java.lang.String DEFAULT_CHOSEN_DESTINATION_ID = "";
    public static final java.lang.Integer DEFAULT_DISTANCE_TO_DESTINATION = java.lang.Integer.valueOf(0);
    public static final java.lang.Integer DEFAULT_DISTANCE_TRAVELED = java.lang.Integer.valueOf(0);
    public static final java.lang.Double DEFAULT_ELEVATION = java.lang.Double.valueOf(0.0d);
    public static final java.lang.Float DEFAULT_ELEVATION_ACCURACY = java.lang.Float.valueOf(0.0f);
    public static final java.lang.Integer DEFAULT_ESTIMATED_TIME_REMAINING = java.lang.Integer.valueOf(0);
    public static final java.lang.Double DEFAULT_EXCESSIVE_SPEEDING_RATIO = java.lang.Double.valueOf(0.0d);
    public static final java.lang.Float DEFAULT_GPS_SPEED = java.lang.Float.valueOf(0.0f);
    public static final java.lang.Integer DEFAULT_HARD_ACCELERATION_COUNT = java.lang.Integer.valueOf(0);
    public static final java.lang.Integer DEFAULT_HARD_BREAKING_COUNT = java.lang.Integer.valueOf(0);
    public static final java.lang.Integer DEFAULT_HIGH_G_COUNT = java.lang.Integer.valueOf(0);
    public static final java.lang.Float DEFAULT_HORIZONTAL_ACCURACY = java.lang.Float.valueOf(0.0f);
    public static final java.lang.Integer DEFAULT_METERS_TRAVELED_SINCE_BOOT = java.lang.Integer.valueOf(0);
    public static final java.lang.Integer DEFAULT_OBD_SPEED = java.lang.Integer.valueOf(0);
    public static final java.lang.String DEFAULT_ROAD_ELEMENT = "";
    public static final java.lang.Integer DEFAULT_SEQUENCE_NUMBER = java.lang.Integer.valueOf(0);
    public static final java.lang.Double DEFAULT_SPEEDING_RATIO = java.lang.Double.valueOf(0.0d);
    public static final java.lang.Long DEFAULT_TIMESTAMP = java.lang.Long.valueOf(0);
    public static final java.lang.Long DEFAULT_TRIP_NUMBER = java.lang.Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 12, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String arrived_at_destination_id;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.FLOAT)
    public final java.lang.Float bearing;
    @com.squareup.wire.ProtoField(tag = 11, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String chosen_destination_id;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 5)
    public final com.navdy.service.library.events.location.LatLong current_position;
    @com.squareup.wire.ProtoField(tag = 14, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer distance_to_destination;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 4, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer distance_traveled;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.DOUBLE)
    public final java.lang.Double elevation;
    @com.squareup.wire.ProtoField(tag = 22, type = com.squareup.wire.Message.Datatype.FLOAT)
    public final java.lang.Float elevation_accuracy;
    @com.squareup.wire.ProtoField(tag = 13, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer estimated_time_remaining;
    @com.squareup.wire.ProtoField(tag = 19, type = com.squareup.wire.Message.Datatype.DOUBLE)
    public final java.lang.Double excessive_speeding_ratio;
    @com.squareup.wire.ProtoField(tag = 8, type = com.squareup.wire.Message.Datatype.FLOAT)
    public final java.lang.Float gps_speed;
    @com.squareup.wire.ProtoField(tag = 17, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer hard_acceleration_count;
    @com.squareup.wire.ProtoField(tag = 16, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer hard_breaking_count;
    @com.squareup.wire.ProtoField(tag = 15, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer high_g_count;
    @com.squareup.wire.ProtoField(tag = 21, type = com.squareup.wire.Message.Datatype.FLOAT)
    public final java.lang.Float horizontal_accuracy;
    @com.squareup.wire.ProtoField(tag = 23)
    public final com.navdy.service.library.events.location.Coordinate last_raw_coordinate;
    @com.squareup.wire.ProtoField(tag = 20, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer meters_traveled_since_boot;
    @com.squareup.wire.ProtoField(tag = 9, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer obd_speed;
    @com.squareup.wire.ProtoField(tag = 10, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String road_element;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer sequence_number;
    @com.squareup.wire.ProtoField(tag = 18, type = com.squareup.wire.Message.Datatype.DOUBLE)
    public final java.lang.Double speeding_ratio;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 3, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long timestamp;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long trip_number;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.TripUpdate> {
        public java.lang.String arrived_at_destination_id;
        public java.lang.Float bearing;
        public java.lang.String chosen_destination_id;
        public com.navdy.service.library.events.location.LatLong current_position;
        public java.lang.Integer distance_to_destination;
        public java.lang.Integer distance_traveled;
        public java.lang.Double elevation;
        public java.lang.Float elevation_accuracy;
        public java.lang.Integer estimated_time_remaining;
        public java.lang.Double excessive_speeding_ratio;
        public java.lang.Float gps_speed;
        public java.lang.Integer hard_acceleration_count;
        public java.lang.Integer hard_breaking_count;
        public java.lang.Integer high_g_count;
        public java.lang.Float horizontal_accuracy;
        public com.navdy.service.library.events.location.Coordinate last_raw_coordinate;
        public java.lang.Integer meters_traveled_since_boot;
        public java.lang.Integer obd_speed;
        public java.lang.String road_element;
        public java.lang.Integer sequence_number;
        public java.lang.Double speeding_ratio;
        public java.lang.Long timestamp;
        public java.lang.Long trip_number;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.TripUpdate message) {
            super(message);
            if (message != null) {
                this.trip_number = message.trip_number;
                this.sequence_number = message.sequence_number;
                this.timestamp = message.timestamp;
                this.distance_traveled = message.distance_traveled;
                this.current_position = message.current_position;
                this.elevation = message.elevation;
                this.bearing = message.bearing;
                this.gps_speed = message.gps_speed;
                this.obd_speed = message.obd_speed;
                this.road_element = message.road_element;
                this.chosen_destination_id = message.chosen_destination_id;
                this.arrived_at_destination_id = message.arrived_at_destination_id;
                this.estimated_time_remaining = message.estimated_time_remaining;
                this.distance_to_destination = message.distance_to_destination;
                this.high_g_count = message.high_g_count;
                this.hard_breaking_count = message.hard_breaking_count;
                this.hard_acceleration_count = message.hard_acceleration_count;
                this.speeding_ratio = message.speeding_ratio;
                this.excessive_speeding_ratio = message.excessive_speeding_ratio;
                this.meters_traveled_since_boot = message.meters_traveled_since_boot;
                this.horizontal_accuracy = message.horizontal_accuracy;
                this.elevation_accuracy = message.elevation_accuracy;
                this.last_raw_coordinate = message.last_raw_coordinate;
            }
        }

        public com.navdy.service.library.events.TripUpdate.Builder trip_number(java.lang.Long trip_number2) {
            this.trip_number = trip_number2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder sequence_number(java.lang.Integer sequence_number2) {
            this.sequence_number = sequence_number2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder timestamp(java.lang.Long timestamp2) {
            this.timestamp = timestamp2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder distance_traveled(java.lang.Integer distance_traveled2) {
            this.distance_traveled = distance_traveled2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder current_position(com.navdy.service.library.events.location.LatLong current_position2) {
            this.current_position = current_position2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder elevation(java.lang.Double elevation2) {
            this.elevation = elevation2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder bearing(java.lang.Float bearing2) {
            this.bearing = bearing2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder gps_speed(java.lang.Float gps_speed2) {
            this.gps_speed = gps_speed2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder obd_speed(java.lang.Integer obd_speed2) {
            this.obd_speed = obd_speed2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder road_element(java.lang.String road_element2) {
            this.road_element = road_element2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder chosen_destination_id(java.lang.String chosen_destination_id2) {
            this.chosen_destination_id = chosen_destination_id2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder arrived_at_destination_id(java.lang.String arrived_at_destination_id2) {
            this.arrived_at_destination_id = arrived_at_destination_id2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder estimated_time_remaining(java.lang.Integer estimated_time_remaining2) {
            this.estimated_time_remaining = estimated_time_remaining2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder distance_to_destination(java.lang.Integer distance_to_destination2) {
            this.distance_to_destination = distance_to_destination2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder high_g_count(java.lang.Integer high_g_count2) {
            this.high_g_count = high_g_count2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder hard_breaking_count(java.lang.Integer hard_breaking_count2) {
            this.hard_breaking_count = hard_breaking_count2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder hard_acceleration_count(java.lang.Integer hard_acceleration_count2) {
            this.hard_acceleration_count = hard_acceleration_count2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder speeding_ratio(java.lang.Double speeding_ratio2) {
            this.speeding_ratio = speeding_ratio2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder excessive_speeding_ratio(java.lang.Double excessive_speeding_ratio2) {
            this.excessive_speeding_ratio = excessive_speeding_ratio2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder meters_traveled_since_boot(java.lang.Integer meters_traveled_since_boot2) {
            this.meters_traveled_since_boot = meters_traveled_since_boot2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder horizontal_accuracy(java.lang.Float horizontal_accuracy2) {
            this.horizontal_accuracy = horizontal_accuracy2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder elevation_accuracy(java.lang.Float elevation_accuracy2) {
            this.elevation_accuracy = elevation_accuracy2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate.Builder last_raw_coordinate(com.navdy.service.library.events.location.Coordinate last_raw_coordinate2) {
            this.last_raw_coordinate = last_raw_coordinate2;
            return this;
        }

        public com.navdy.service.library.events.TripUpdate build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.TripUpdate(this);
        }
    }

    public TripUpdate(java.lang.Long trip_number2, java.lang.Integer sequence_number2, java.lang.Long timestamp2, java.lang.Integer distance_traveled2, com.navdy.service.library.events.location.LatLong current_position2, java.lang.Double elevation2, java.lang.Float bearing2, java.lang.Float gps_speed2, java.lang.Integer obd_speed2, java.lang.String road_element2, java.lang.String chosen_destination_id2, java.lang.String arrived_at_destination_id2, java.lang.Integer estimated_time_remaining2, java.lang.Integer distance_to_destination2, java.lang.Integer high_g_count2, java.lang.Integer hard_breaking_count2, java.lang.Integer hard_acceleration_count2, java.lang.Double speeding_ratio2, java.lang.Double excessive_speeding_ratio2, java.lang.Integer meters_traveled_since_boot2, java.lang.Float horizontal_accuracy2, java.lang.Float elevation_accuracy2, com.navdy.service.library.events.location.Coordinate last_raw_coordinate2) {
        this.trip_number = trip_number2;
        this.sequence_number = sequence_number2;
        this.timestamp = timestamp2;
        this.distance_traveled = distance_traveled2;
        this.current_position = current_position2;
        this.elevation = elevation2;
        this.bearing = bearing2;
        this.gps_speed = gps_speed2;
        this.obd_speed = obd_speed2;
        this.road_element = road_element2;
        this.chosen_destination_id = chosen_destination_id2;
        this.arrived_at_destination_id = arrived_at_destination_id2;
        this.estimated_time_remaining = estimated_time_remaining2;
        this.distance_to_destination = distance_to_destination2;
        this.high_g_count = high_g_count2;
        this.hard_breaking_count = hard_breaking_count2;
        this.hard_acceleration_count = hard_acceleration_count2;
        this.speeding_ratio = speeding_ratio2;
        this.excessive_speeding_ratio = excessive_speeding_ratio2;
        this.meters_traveled_since_boot = meters_traveled_since_boot2;
        this.horizontal_accuracy = horizontal_accuracy2;
        this.elevation_accuracy = elevation_accuracy2;
        this.last_raw_coordinate = last_raw_coordinate2;
    }

    private TripUpdate(com.navdy.service.library.events.TripUpdate.Builder builder) {
        this(builder.trip_number, builder.sequence_number, builder.timestamp, builder.distance_traveled, builder.current_position, builder.elevation, builder.bearing, builder.gps_speed, builder.obd_speed, builder.road_element, builder.chosen_destination_id, builder.arrived_at_destination_id, builder.estimated_time_remaining, builder.distance_to_destination, builder.high_g_count, builder.hard_breaking_count, builder.hard_acceleration_count, builder.speeding_ratio, builder.excessive_speeding_ratio, builder.meters_traveled_since_boot, builder.horizontal_accuracy, builder.elevation_accuracy, builder.last_raw_coordinate);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.TripUpdate)) {
            return false;
        }
        com.navdy.service.library.events.TripUpdate o = (com.navdy.service.library.events.TripUpdate) other;
        if (!equals((java.lang.Object) this.trip_number, (java.lang.Object) o.trip_number) || !equals((java.lang.Object) this.sequence_number, (java.lang.Object) o.sequence_number) || !equals((java.lang.Object) this.timestamp, (java.lang.Object) o.timestamp) || !equals((java.lang.Object) this.distance_traveled, (java.lang.Object) o.distance_traveled) || !equals((java.lang.Object) this.current_position, (java.lang.Object) o.current_position) || !equals((java.lang.Object) this.elevation, (java.lang.Object) o.elevation) || !equals((java.lang.Object) this.bearing, (java.lang.Object) o.bearing) || !equals((java.lang.Object) this.gps_speed, (java.lang.Object) o.gps_speed) || !equals((java.lang.Object) this.obd_speed, (java.lang.Object) o.obd_speed) || !equals((java.lang.Object) this.road_element, (java.lang.Object) o.road_element) || !equals((java.lang.Object) this.chosen_destination_id, (java.lang.Object) o.chosen_destination_id) || !equals((java.lang.Object) this.arrived_at_destination_id, (java.lang.Object) o.arrived_at_destination_id) || !equals((java.lang.Object) this.estimated_time_remaining, (java.lang.Object) o.estimated_time_remaining) || !equals((java.lang.Object) this.distance_to_destination, (java.lang.Object) o.distance_to_destination) || !equals((java.lang.Object) this.high_g_count, (java.lang.Object) o.high_g_count) || !equals((java.lang.Object) this.hard_breaking_count, (java.lang.Object) o.hard_breaking_count) || !equals((java.lang.Object) this.hard_acceleration_count, (java.lang.Object) o.hard_acceleration_count) || !equals((java.lang.Object) this.speeding_ratio, (java.lang.Object) o.speeding_ratio) || !equals((java.lang.Object) this.excessive_speeding_ratio, (java.lang.Object) o.excessive_speeding_ratio) || !equals((java.lang.Object) this.meters_traveled_since_boot, (java.lang.Object) o.meters_traveled_since_boot) || !equals((java.lang.Object) this.horizontal_accuracy, (java.lang.Object) o.horizontal_accuracy) || !equals((java.lang.Object) this.elevation_accuracy, (java.lang.Object) o.elevation_accuracy) || !equals((java.lang.Object) this.last_raw_coordinate, (java.lang.Object) o.last_raw_coordinate)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17;
        int i18;
        int i19;
        int i20;
        int i21;
        int i22 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.trip_number != null ? this.trip_number.hashCode() : 0) * 37;
        if (this.sequence_number != null) {
            i = this.sequence_number.hashCode();
        } else {
            i = 0;
        }
        int i23 = (hashCode + i) * 37;
        if (this.timestamp != null) {
            i2 = this.timestamp.hashCode();
        } else {
            i2 = 0;
        }
        int i24 = (i23 + i2) * 37;
        if (this.distance_traveled != null) {
            i3 = this.distance_traveled.hashCode();
        } else {
            i3 = 0;
        }
        int i25 = (i24 + i3) * 37;
        if (this.current_position != null) {
            i4 = this.current_position.hashCode();
        } else {
            i4 = 0;
        }
        int i26 = (i25 + i4) * 37;
        if (this.elevation != null) {
            i5 = this.elevation.hashCode();
        } else {
            i5 = 0;
        }
        int i27 = (i26 + i5) * 37;
        if (this.bearing != null) {
            i6 = this.bearing.hashCode();
        } else {
            i6 = 0;
        }
        int i28 = (i27 + i6) * 37;
        if (this.gps_speed != null) {
            i7 = this.gps_speed.hashCode();
        } else {
            i7 = 0;
        }
        int i29 = (i28 + i7) * 37;
        if (this.obd_speed != null) {
            i8 = this.obd_speed.hashCode();
        } else {
            i8 = 0;
        }
        int i30 = (i29 + i8) * 37;
        if (this.road_element != null) {
            i9 = this.road_element.hashCode();
        } else {
            i9 = 0;
        }
        int i31 = (i30 + i9) * 37;
        if (this.chosen_destination_id != null) {
            i10 = this.chosen_destination_id.hashCode();
        } else {
            i10 = 0;
        }
        int i32 = (i31 + i10) * 37;
        if (this.arrived_at_destination_id != null) {
            i11 = this.arrived_at_destination_id.hashCode();
        } else {
            i11 = 0;
        }
        int i33 = (i32 + i11) * 37;
        if (this.estimated_time_remaining != null) {
            i12 = this.estimated_time_remaining.hashCode();
        } else {
            i12 = 0;
        }
        int i34 = (i33 + i12) * 37;
        if (this.distance_to_destination != null) {
            i13 = this.distance_to_destination.hashCode();
        } else {
            i13 = 0;
        }
        int i35 = (i34 + i13) * 37;
        if (this.high_g_count != null) {
            i14 = this.high_g_count.hashCode();
        } else {
            i14 = 0;
        }
        int i36 = (i35 + i14) * 37;
        if (this.hard_breaking_count != null) {
            i15 = this.hard_breaking_count.hashCode();
        } else {
            i15 = 0;
        }
        int i37 = (i36 + i15) * 37;
        if (this.hard_acceleration_count != null) {
            i16 = this.hard_acceleration_count.hashCode();
        } else {
            i16 = 0;
        }
        int i38 = (i37 + i16) * 37;
        if (this.speeding_ratio != null) {
            i17 = this.speeding_ratio.hashCode();
        } else {
            i17 = 0;
        }
        int i39 = (i38 + i17) * 37;
        if (this.excessive_speeding_ratio != null) {
            i18 = this.excessive_speeding_ratio.hashCode();
        } else {
            i18 = 0;
        }
        int i40 = (i39 + i18) * 37;
        if (this.meters_traveled_since_boot != null) {
            i19 = this.meters_traveled_since_boot.hashCode();
        } else {
            i19 = 0;
        }
        int i41 = (i40 + i19) * 37;
        if (this.horizontal_accuracy != null) {
            i20 = this.horizontal_accuracy.hashCode();
        } else {
            i20 = 0;
        }
        int i42 = (i41 + i20) * 37;
        if (this.elevation_accuracy != null) {
            i21 = this.elevation_accuracy.hashCode();
        } else {
            i21 = 0;
        }
        int i43 = (i42 + i21) * 37;
        if (this.last_raw_coordinate != null) {
            i22 = this.last_raw_coordinate.hashCode();
        }
        int result2 = i43 + i22;
        this.hashCode = result2;
        return result2;
    }
}
