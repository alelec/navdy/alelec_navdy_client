package com.navdy.service.library.events.preferences;

public final class NotificationPreferences extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_ENABLED = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_READALOUD = java.lang.Boolean.valueOf(true);
    public static final java.lang.Long DEFAULT_SERIAL_NUMBER = java.lang.Long.valueOf(0);
    public static final java.util.List<com.navdy.service.library.events.notification.NotificationSetting> DEFAULT_SETTINGS = java.util.Collections.emptyList();
    public static final java.lang.Boolean DEFAULT_SHOWCONTENT = java.lang.Boolean.valueOf(true);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean enabled;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean readAloud;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long serial_number;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.notification.NotificationSetting.class, tag = 2)
    public final java.util.List<com.navdy.service.library.events.notification.NotificationSetting> settings;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean showContent;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.preferences.NotificationPreferences> {
        public java.lang.Boolean enabled;
        public java.lang.Boolean readAloud;
        public java.lang.Long serial_number;
        public java.util.List<com.navdy.service.library.events.notification.NotificationSetting> settings;
        public java.lang.Boolean showContent;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.preferences.NotificationPreferences message) {
            super(message);
            if (message != null) {
                this.serial_number = message.serial_number;
                this.settings = com.navdy.service.library.events.preferences.NotificationPreferences.copyOf(message.settings);
                this.enabled = message.enabled;
                this.readAloud = message.readAloud;
                this.showContent = message.showContent;
            }
        }

        public com.navdy.service.library.events.preferences.NotificationPreferences.Builder serial_number(java.lang.Long serial_number2) {
            this.serial_number = serial_number2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NotificationPreferences.Builder settings(java.util.List<com.navdy.service.library.events.notification.NotificationSetting> settings2) {
            this.settings = checkForNulls(settings2);
            return this;
        }

        public com.navdy.service.library.events.preferences.NotificationPreferences.Builder enabled(java.lang.Boolean enabled2) {
            this.enabled = enabled2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NotificationPreferences.Builder readAloud(java.lang.Boolean readAloud2) {
            this.readAloud = readAloud2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NotificationPreferences.Builder showContent(java.lang.Boolean showContent2) {
            this.showContent = showContent2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NotificationPreferences build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.preferences.NotificationPreferences(this);
        }
    }

    public NotificationPreferences(java.lang.Long serial_number2, java.util.List<com.navdy.service.library.events.notification.NotificationSetting> settings2, java.lang.Boolean enabled2, java.lang.Boolean readAloud2, java.lang.Boolean showContent2) {
        this.serial_number = serial_number2;
        this.settings = immutableCopyOf(settings2);
        this.enabled = enabled2;
        this.readAloud = readAloud2;
        this.showContent = showContent2;
    }

    private NotificationPreferences(com.navdy.service.library.events.preferences.NotificationPreferences.Builder builder) {
        this(builder.serial_number, builder.settings, builder.enabled, builder.readAloud, builder.showContent);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.preferences.NotificationPreferences)) {
            return false;
        }
        com.navdy.service.library.events.preferences.NotificationPreferences o = (com.navdy.service.library.events.preferences.NotificationPreferences) other;
        if (!equals((java.lang.Object) this.serial_number, (java.lang.Object) o.serial_number) || !equals(this.settings, o.settings) || !equals((java.lang.Object) this.enabled, (java.lang.Object) o.enabled) || !equals((java.lang.Object) this.readAloud, (java.lang.Object) o.readAloud) || !equals((java.lang.Object) this.showContent, (java.lang.Object) o.showContent)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (((this.serial_number != null ? this.serial_number.hashCode() : 0) * 37) + (this.settings != null ? this.settings.hashCode() : 1)) * 37;
        if (this.enabled != null) {
            i = this.enabled.hashCode();
        } else {
            i = 0;
        }
        int i4 = (hashCode + i) * 37;
        if (this.readAloud != null) {
            i2 = this.readAloud.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 37;
        if (this.showContent != null) {
            i3 = this.showContent.hashCode();
        }
        int result2 = i5 + i3;
        this.hashCode = result2;
        return result2;
    }
}
