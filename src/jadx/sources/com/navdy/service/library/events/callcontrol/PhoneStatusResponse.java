package com.navdy.service.library.events.callcontrol;

public final class PhoneStatusResponse extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 2)
    public final com.navdy.service.library.events.callcontrol.PhoneEvent callStatus;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.callcontrol.PhoneStatusResponse> {
        public com.navdy.service.library.events.callcontrol.PhoneEvent callStatus;
        public com.navdy.service.library.events.RequestStatus status;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.callcontrol.PhoneStatusResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.callStatus = message.callStatus;
            }
        }

        public com.navdy.service.library.events.callcontrol.PhoneStatusResponse.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.PhoneStatusResponse.Builder callStatus(com.navdy.service.library.events.callcontrol.PhoneEvent callStatus2) {
            this.callStatus = callStatus2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.PhoneStatusResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.callcontrol.PhoneStatusResponse(this);
        }
    }

    public PhoneStatusResponse(com.navdy.service.library.events.RequestStatus status2, com.navdy.service.library.events.callcontrol.PhoneEvent callStatus2) {
        this.status = status2;
        this.callStatus = callStatus2;
    }

    private PhoneStatusResponse(com.navdy.service.library.events.callcontrol.PhoneStatusResponse.Builder builder) {
        this(builder.status, builder.callStatus);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.callcontrol.PhoneStatusResponse)) {
            return false;
        }
        com.navdy.service.library.events.callcontrol.PhoneStatusResponse o = (com.navdy.service.library.events.callcontrol.PhoneStatusResponse) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.callStatus, (java.lang.Object) o.callStatus)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.status != null) {
            result = this.status.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.callStatus != null) {
            i = this.callStatus.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
