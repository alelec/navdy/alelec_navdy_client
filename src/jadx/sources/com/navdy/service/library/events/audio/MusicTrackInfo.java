package com.navdy.service.library.events.audio;

public final class MusicTrackInfo extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_ALBUM = "";
    public static final java.lang.String DEFAULT_AUTHOR = "";
    public static final java.lang.String DEFAULT_COLLECTIONID = "";
    public static final com.navdy.service.library.events.audio.MusicCollectionSource DEFAULT_COLLECTIONSOURCE = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    public static final com.navdy.service.library.events.audio.MusicCollectionType DEFAULT_COLLECTIONTYPE = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    public static final java.lang.Integer DEFAULT_CURRENTPOSITION = java.lang.Integer.valueOf(0);
    public static final com.navdy.service.library.events.audio.MusicDataSource DEFAULT_DATASOURCE = com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_NONE;
    public static final java.lang.Integer DEFAULT_DURATION = java.lang.Integer.valueOf(0);
    public static final java.lang.Long DEFAULT_INDEX = java.lang.Long.valueOf(0);
    public static final java.lang.Boolean DEFAULT_ISNEXTALLOWED = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_ISONLINESTREAM = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_ISPREVIOUSALLOWED = java.lang.Boolean.valueOf(false);
    public static final java.lang.String DEFAULT_NAME = "";
    public static final com.navdy.service.library.events.audio.MusicPlaybackState DEFAULT_PLAYBACKSTATE = com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE;
    public static final java.lang.Integer DEFAULT_PLAYCOUNT = java.lang.Integer.valueOf(0);
    public static final com.navdy.service.library.events.audio.MusicRepeatMode DEFAULT_REPEATMODE = com.navdy.service.library.events.audio.MusicRepeatMode.MUSIC_REPEAT_MODE_UNKNOWN;
    public static final com.navdy.service.library.events.audio.MusicShuffleMode DEFAULT_SHUFFLEMODE = com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN;
    public static final java.lang.String DEFAULT_TRACKID = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String album;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String author;
    @com.squareup.wire.ProtoField(tag = 14, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String collectionId;
    @com.squareup.wire.ProtoField(tag = 12, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    @com.squareup.wire.ProtoField(tag = 13, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer currentPosition;
    @com.squareup.wire.ProtoField(tag = 10, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicDataSource dataSource;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer duration;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long index;
    @com.squareup.wire.ProtoField(tag = 9, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean isNextAllowed;
    @com.squareup.wire.ProtoField(tag = 11, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean isOnlineStream;
    @com.squareup.wire.ProtoField(tag = 8, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean isPreviousAllowed;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String name;
    @com.squareup.wire.ProtoField(tag = 16, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer playCount;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicPlaybackState playbackState;
    @com.squareup.wire.ProtoField(tag = 18, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicRepeatMode repeatMode;
    @com.squareup.wire.ProtoField(tag = 17, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicShuffleMode shuffleMode;
    @com.squareup.wire.ProtoField(tag = 15, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String trackId;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.MusicTrackInfo> {
        public java.lang.String album;
        public java.lang.String author;
        public java.lang.String collectionId;
        public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
        public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
        public java.lang.Integer currentPosition;
        public com.navdy.service.library.events.audio.MusicDataSource dataSource;
        public java.lang.Integer duration;
        public java.lang.Long index;
        public java.lang.Boolean isNextAllowed;
        public java.lang.Boolean isOnlineStream;
        public java.lang.Boolean isPreviousAllowed;
        public java.lang.String name;
        public java.lang.Integer playCount;
        public com.navdy.service.library.events.audio.MusicPlaybackState playbackState;
        public com.navdy.service.library.events.audio.MusicRepeatMode repeatMode;
        public com.navdy.service.library.events.audio.MusicShuffleMode shuffleMode;
        public java.lang.String trackId;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.MusicTrackInfo message) {
            super(message);
            if (message != null) {
                this.playbackState = message.playbackState;
                this.index = message.index;
                this.name = message.name;
                this.author = message.author;
                this.album = message.album;
                this.duration = message.duration;
                this.currentPosition = message.currentPosition;
                this.isPreviousAllowed = message.isPreviousAllowed;
                this.isNextAllowed = message.isNextAllowed;
                this.dataSource = message.dataSource;
                this.isOnlineStream = message.isOnlineStream;
                this.collectionSource = message.collectionSource;
                this.collectionType = message.collectionType;
                this.collectionId = message.collectionId;
                this.trackId = message.trackId;
                this.playCount = message.playCount;
                this.shuffleMode = message.shuffleMode;
                this.repeatMode = message.repeatMode;
            }
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder playbackState(com.navdy.service.library.events.audio.MusicPlaybackState playbackState2) {
            this.playbackState = playbackState2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder index(java.lang.Long index2) {
            this.index = index2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder name(java.lang.String name2) {
            this.name = name2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder author(java.lang.String author2) {
            this.author = author2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder album(java.lang.String album2) {
            this.album = album2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder duration(java.lang.Integer duration2) {
            this.duration = duration2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder currentPosition(java.lang.Integer currentPosition2) {
            this.currentPosition = currentPosition2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder isPreviousAllowed(java.lang.Boolean isPreviousAllowed2) {
            this.isPreviousAllowed = isPreviousAllowed2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder isNextAllowed(java.lang.Boolean isNextAllowed2) {
            this.isNextAllowed = isNextAllowed2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder dataSource(com.navdy.service.library.events.audio.MusicDataSource dataSource2) {
            this.dataSource = dataSource2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder isOnlineStream(java.lang.Boolean isOnlineStream2) {
            this.isOnlineStream = isOnlineStream2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource collectionSource2) {
            this.collectionSource = collectionSource2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder collectionType(com.navdy.service.library.events.audio.MusicCollectionType collectionType2) {
            this.collectionType = collectionType2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder collectionId(java.lang.String collectionId2) {
            this.collectionId = collectionId2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder trackId(java.lang.String trackId2) {
            this.trackId = trackId2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder playCount(java.lang.Integer playCount2) {
            this.playCount = playCount2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder shuffleMode(com.navdy.service.library.events.audio.MusicShuffleMode shuffleMode2) {
            this.shuffleMode = shuffleMode2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo.Builder repeatMode(com.navdy.service.library.events.audio.MusicRepeatMode repeatMode2) {
            this.repeatMode = repeatMode2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicTrackInfo build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.audio.MusicTrackInfo(this);
        }
    }

    public MusicTrackInfo(com.navdy.service.library.events.audio.MusicPlaybackState playbackState2, java.lang.Long index2, java.lang.String name2, java.lang.String author2, java.lang.String album2, java.lang.Integer duration2, java.lang.Integer currentPosition2, java.lang.Boolean isPreviousAllowed2, java.lang.Boolean isNextAllowed2, com.navdy.service.library.events.audio.MusicDataSource dataSource2, java.lang.Boolean isOnlineStream2, com.navdy.service.library.events.audio.MusicCollectionSource collectionSource2, com.navdy.service.library.events.audio.MusicCollectionType collectionType2, java.lang.String collectionId2, java.lang.String trackId2, java.lang.Integer playCount2, com.navdy.service.library.events.audio.MusicShuffleMode shuffleMode2, com.navdy.service.library.events.audio.MusicRepeatMode repeatMode2) {
        this.playbackState = playbackState2;
        this.index = index2;
        this.name = name2;
        this.author = author2;
        this.album = album2;
        this.duration = duration2;
        this.currentPosition = currentPosition2;
        this.isPreviousAllowed = isPreviousAllowed2;
        this.isNextAllowed = isNextAllowed2;
        this.dataSource = dataSource2;
        this.isOnlineStream = isOnlineStream2;
        this.collectionSource = collectionSource2;
        this.collectionType = collectionType2;
        this.collectionId = collectionId2;
        this.trackId = trackId2;
        this.playCount = playCount2;
        this.shuffleMode = shuffleMode2;
        this.repeatMode = repeatMode2;
    }

    private MusicTrackInfo(com.navdy.service.library.events.audio.MusicTrackInfo.Builder builder) {
        this(builder.playbackState, builder.index, builder.name, builder.author, builder.album, builder.duration, builder.currentPosition, builder.isPreviousAllowed, builder.isNextAllowed, builder.dataSource, builder.isOnlineStream, builder.collectionSource, builder.collectionType, builder.collectionId, builder.trackId, builder.playCount, builder.shuffleMode, builder.repeatMode);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.MusicTrackInfo)) {
            return false;
        }
        com.navdy.service.library.events.audio.MusicTrackInfo o = (com.navdy.service.library.events.audio.MusicTrackInfo) other;
        if (!equals((java.lang.Object) this.playbackState, (java.lang.Object) o.playbackState) || !equals((java.lang.Object) this.index, (java.lang.Object) o.index) || !equals((java.lang.Object) this.name, (java.lang.Object) o.name) || !equals((java.lang.Object) this.author, (java.lang.Object) o.author) || !equals((java.lang.Object) this.album, (java.lang.Object) o.album) || !equals((java.lang.Object) this.duration, (java.lang.Object) o.duration) || !equals((java.lang.Object) this.currentPosition, (java.lang.Object) o.currentPosition) || !equals((java.lang.Object) this.isPreviousAllowed, (java.lang.Object) o.isPreviousAllowed) || !equals((java.lang.Object) this.isNextAllowed, (java.lang.Object) o.isNextAllowed) || !equals((java.lang.Object) this.dataSource, (java.lang.Object) o.dataSource) || !equals((java.lang.Object) this.isOnlineStream, (java.lang.Object) o.isOnlineStream) || !equals((java.lang.Object) this.collectionSource, (java.lang.Object) o.collectionSource) || !equals((java.lang.Object) this.collectionType, (java.lang.Object) o.collectionType) || !equals((java.lang.Object) this.collectionId, (java.lang.Object) o.collectionId) || !equals((java.lang.Object) this.trackId, (java.lang.Object) o.trackId) || !equals((java.lang.Object) this.playCount, (java.lang.Object) o.playCount) || !equals((java.lang.Object) this.shuffleMode, (java.lang.Object) o.shuffleMode) || !equals((java.lang.Object) this.repeatMode, (java.lang.Object) o.repeatMode)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.playbackState != null ? this.playbackState.hashCode() : 0) * 37;
        if (this.index != null) {
            i = this.index.hashCode();
        } else {
            i = 0;
        }
        int i18 = (hashCode + i) * 37;
        if (this.name != null) {
            i2 = this.name.hashCode();
        } else {
            i2 = 0;
        }
        int i19 = (i18 + i2) * 37;
        if (this.author != null) {
            i3 = this.author.hashCode();
        } else {
            i3 = 0;
        }
        int i20 = (i19 + i3) * 37;
        if (this.album != null) {
            i4 = this.album.hashCode();
        } else {
            i4 = 0;
        }
        int i21 = (i20 + i4) * 37;
        if (this.duration != null) {
            i5 = this.duration.hashCode();
        } else {
            i5 = 0;
        }
        int i22 = (i21 + i5) * 37;
        if (this.currentPosition != null) {
            i6 = this.currentPosition.hashCode();
        } else {
            i6 = 0;
        }
        int i23 = (i22 + i6) * 37;
        if (this.isPreviousAllowed != null) {
            i7 = this.isPreviousAllowed.hashCode();
        } else {
            i7 = 0;
        }
        int i24 = (i23 + i7) * 37;
        if (this.isNextAllowed != null) {
            i8 = this.isNextAllowed.hashCode();
        } else {
            i8 = 0;
        }
        int i25 = (i24 + i8) * 37;
        if (this.dataSource != null) {
            i9 = this.dataSource.hashCode();
        } else {
            i9 = 0;
        }
        int i26 = (i25 + i9) * 37;
        if (this.isOnlineStream != null) {
            i10 = this.isOnlineStream.hashCode();
        } else {
            i10 = 0;
        }
        int i27 = (i26 + i10) * 37;
        if (this.collectionSource != null) {
            i11 = this.collectionSource.hashCode();
        } else {
            i11 = 0;
        }
        int i28 = (i27 + i11) * 37;
        if (this.collectionType != null) {
            i12 = this.collectionType.hashCode();
        } else {
            i12 = 0;
        }
        int i29 = (i28 + i12) * 37;
        if (this.collectionId != null) {
            i13 = this.collectionId.hashCode();
        } else {
            i13 = 0;
        }
        int i30 = (i29 + i13) * 37;
        if (this.trackId != null) {
            i14 = this.trackId.hashCode();
        } else {
            i14 = 0;
        }
        int i31 = (i30 + i14) * 37;
        if (this.playCount != null) {
            i15 = this.playCount.hashCode();
        } else {
            i15 = 0;
        }
        int i32 = (i31 + i15) * 37;
        if (this.shuffleMode != null) {
            i16 = this.shuffleMode.hashCode();
        } else {
            i16 = 0;
        }
        int i33 = (i32 + i16) * 37;
        if (this.repeatMode != null) {
            i17 = this.repeatMode.hashCode();
        }
        int result2 = i33 + i17;
        this.hashCode = result2;
        return result2;
    }
}
