package com.navdy.service.library.events.ui;

public final class DismissScreen extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.ui.Screen DEFAULT_SCREEN = com.navdy.service.library.events.ui.Screen.SCREEN_DASHBOARD;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.ui.Screen screen;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.ui.DismissScreen> {
        public com.navdy.service.library.events.ui.Screen screen;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.ui.DismissScreen message) {
            super(message);
            if (message != null) {
                this.screen = message.screen;
            }
        }

        public com.navdy.service.library.events.ui.DismissScreen.Builder screen(com.navdy.service.library.events.ui.Screen screen2) {
            this.screen = screen2;
            return this;
        }

        public com.navdy.service.library.events.ui.DismissScreen build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.ui.DismissScreen(this);
        }
    }

    public DismissScreen(com.navdy.service.library.events.ui.Screen screen2) {
        this.screen = screen2;
    }

    private DismissScreen(com.navdy.service.library.events.ui.DismissScreen.Builder builder) {
        this(builder.screen);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.ui.DismissScreen)) {
            return false;
        }
        return equals((java.lang.Object) this.screen, (java.lang.Object) ((com.navdy.service.library.events.ui.DismissScreen) other).screen);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.screen != null ? this.screen.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
