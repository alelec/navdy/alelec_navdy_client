package com.navdy.service.library.events.connection;

public final class ConnectionStatus extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_REMOTEDEVICEID = "";
    public static final com.navdy.service.library.events.connection.ConnectionStatus.Status DEFAULT_STATUS = com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_PAIRING;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String remoteDeviceId;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.connection.ConnectionStatus.Status status;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.connection.ConnectionStatus> {
        public java.lang.String remoteDeviceId;
        public com.navdy.service.library.events.connection.ConnectionStatus.Status status;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.connection.ConnectionStatus message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.remoteDeviceId = message.remoteDeviceId;
            }
        }

        public com.navdy.service.library.events.connection.ConnectionStatus.Builder status(com.navdy.service.library.events.connection.ConnectionStatus.Status status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.connection.ConnectionStatus.Builder remoteDeviceId(java.lang.String remoteDeviceId2) {
            this.remoteDeviceId = remoteDeviceId2;
            return this;
        }

        public com.navdy.service.library.events.connection.ConnectionStatus build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.connection.ConnectionStatus(this);
        }
    }

    public enum Status implements com.squareup.wire.ProtoEnum {
        CONNECTION_PAIRING(1),
        CONNECTION_SEARCH_STARTED(2),
        CONNECTION_SEARCH_FINISHED(3),
        CONNECTION_FOUND(4),
        CONNECTION_LOST(5),
        CONNECTION_PAIRED_DEVICES_CHANGED(6);
        
        private final int value;

        private Status(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Status status2, java.lang.String remoteDeviceId2) {
        this.status = status2;
        this.remoteDeviceId = remoteDeviceId2;
    }

    private ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Builder builder) {
        this(builder.status, builder.remoteDeviceId);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.connection.ConnectionStatus)) {
            return false;
        }
        com.navdy.service.library.events.connection.ConnectionStatus o = (com.navdy.service.library.events.connection.ConnectionStatus) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.remoteDeviceId, (java.lang.Object) o.remoteDeviceId)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.status != null) {
            result = this.status.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.remoteDeviceId != null) {
            i = this.remoteDeviceId.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
