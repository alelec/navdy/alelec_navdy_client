package com.navdy.service.library.events.file;

public final class FileTransferData extends com.squareup.wire.Message {
    public static final java.lang.Integer DEFAULT_CHUNKINDEX = java.lang.Integer.valueOf(0);
    public static final okio.ByteString DEFAULT_DATABYTES = okio.ByteString.EMPTY;
    public static final java.lang.String DEFAULT_FILECHECKSUM = "";
    public static final java.lang.Boolean DEFAULT_LASTCHUNK = java.lang.Boolean.valueOf(false);
    public static final java.lang.Integer DEFAULT_TRANSFERID = java.lang.Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer chunkIndex;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 3, type = com.squareup.wire.Message.Datatype.BYTES)
    public final okio.ByteString dataBytes;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String fileCheckSum;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 4, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean lastChunk;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer transferId;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.file.FileTransferData> {
        public java.lang.Integer chunkIndex;
        public okio.ByteString dataBytes;
        public java.lang.String fileCheckSum;
        public java.lang.Boolean lastChunk;
        public java.lang.Integer transferId;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.file.FileTransferData message) {
            super(message);
            if (message != null) {
                this.transferId = message.transferId;
                this.chunkIndex = message.chunkIndex;
                this.dataBytes = message.dataBytes;
                this.lastChunk = message.lastChunk;
                this.fileCheckSum = message.fileCheckSum;
            }
        }

        public com.navdy.service.library.events.file.FileTransferData.Builder transferId(java.lang.Integer transferId2) {
            this.transferId = transferId2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferData.Builder chunkIndex(java.lang.Integer chunkIndex2) {
            this.chunkIndex = chunkIndex2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferData.Builder dataBytes(okio.ByteString dataBytes2) {
            this.dataBytes = dataBytes2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferData.Builder lastChunk(java.lang.Boolean lastChunk2) {
            this.lastChunk = lastChunk2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferData.Builder fileCheckSum(java.lang.String fileCheckSum2) {
            this.fileCheckSum = fileCheckSum2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferData build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.file.FileTransferData(this);
        }
    }

    public FileTransferData(java.lang.Integer transferId2, java.lang.Integer chunkIndex2, okio.ByteString dataBytes2, java.lang.Boolean lastChunk2, java.lang.String fileCheckSum2) {
        this.transferId = transferId2;
        this.chunkIndex = chunkIndex2;
        this.dataBytes = dataBytes2;
        this.lastChunk = lastChunk2;
        this.fileCheckSum = fileCheckSum2;
    }

    private FileTransferData(com.navdy.service.library.events.file.FileTransferData.Builder builder) {
        this(builder.transferId, builder.chunkIndex, builder.dataBytes, builder.lastChunk, builder.fileCheckSum);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.file.FileTransferData)) {
            return false;
        }
        com.navdy.service.library.events.file.FileTransferData o = (com.navdy.service.library.events.file.FileTransferData) other;
        if (!equals((java.lang.Object) this.transferId, (java.lang.Object) o.transferId) || !equals((java.lang.Object) this.chunkIndex, (java.lang.Object) o.chunkIndex) || !equals((java.lang.Object) this.dataBytes, (java.lang.Object) o.dataBytes) || !equals((java.lang.Object) this.lastChunk, (java.lang.Object) o.lastChunk) || !equals((java.lang.Object) this.fileCheckSum, (java.lang.Object) o.fileCheckSum)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.transferId != null ? this.transferId.hashCode() : 0) * 37;
        if (this.chunkIndex != null) {
            i = this.chunkIndex.hashCode();
        } else {
            i = 0;
        }
        int i5 = (hashCode + i) * 37;
        if (this.dataBytes != null) {
            i2 = this.dataBytes.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 37;
        if (this.lastChunk != null) {
            i3 = this.lastChunk.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (i6 + i3) * 37;
        if (this.fileCheckSum != null) {
            i4 = this.fileCheckSum.hashCode();
        }
        int result2 = i7 + i4;
        this.hashCode = result2;
        return result2;
    }
}
