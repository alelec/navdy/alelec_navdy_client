package com.navdy.service.library.events.hudcontrol;

public final class AccelerateShutdown extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.hudcontrol.AccelerateShutdown.AccelerateReason DEFAULT_REASON = com.navdy.service.library.events.hudcontrol.AccelerateShutdown.AccelerateReason.ACCELERATE_REASON_UNKNOWN;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.hudcontrol.AccelerateShutdown.AccelerateReason reason;

    public enum AccelerateReason implements com.squareup.wire.ProtoEnum {
        ACCELERATE_REASON_UNKNOWN(1),
        ACCELERATE_REASON_HFP_DISCONNECT(2),
        ACCELERATE_REASON_WALKING(3);
        
        private final int value;

        private AccelerateReason(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.hudcontrol.AccelerateShutdown> {
        public com.navdy.service.library.events.hudcontrol.AccelerateShutdown.AccelerateReason reason;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.hudcontrol.AccelerateShutdown message) {
            super(message);
            if (message != null) {
                this.reason = message.reason;
            }
        }

        public com.navdy.service.library.events.hudcontrol.AccelerateShutdown.Builder reason(com.navdy.service.library.events.hudcontrol.AccelerateShutdown.AccelerateReason reason2) {
            this.reason = reason2;
            return this;
        }

        public com.navdy.service.library.events.hudcontrol.AccelerateShutdown build() {
            return new com.navdy.service.library.events.hudcontrol.AccelerateShutdown(this);
        }
    }

    public AccelerateShutdown(com.navdy.service.library.events.hudcontrol.AccelerateShutdown.AccelerateReason reason2) {
        this.reason = reason2;
    }

    private AccelerateShutdown(com.navdy.service.library.events.hudcontrol.AccelerateShutdown.Builder builder) {
        this(builder.reason);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.hudcontrol.AccelerateShutdown)) {
            return false;
        }
        return equals((java.lang.Object) this.reason, (java.lang.Object) ((com.navdy.service.library.events.hudcontrol.AccelerateShutdown) other).reason);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.reason != null ? this.reason.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
