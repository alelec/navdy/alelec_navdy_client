package com.navdy.service.library.events;

public final class DeviceInfo extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_BUILDTYPE = "user";
    public static final java.lang.String DEFAULT_CLIENTVERSION = "";
    public static final java.lang.String DEFAULT_DEVICEID = "";
    public static final java.lang.String DEFAULT_DEVICEMAKE = "unknown";
    public static final java.lang.String DEFAULT_DEVICENAME = "";
    public static final java.lang.String DEFAULT_DEVICEUUID = "";
    public static final java.lang.Boolean DEFAULT_FORCEFULLUPDATE = java.lang.Boolean.valueOf(false);
    public static final java.lang.String DEFAULT_KERNELVERSION = "";
    public static final java.util.List<com.navdy.service.library.events.LegacyCapability> DEFAULT_LEGACYCAPABILITIES = java.util.Collections.emptyList();
    public static final java.lang.String DEFAULT_MODEL = "";
    public static final java.util.List<java.lang.String> DEFAULT_MUSICPLAYERS_OBSOLETE = java.util.Collections.emptyList();
    public static final com.navdy.service.library.events.DeviceInfo.Platform DEFAULT_PLATFORM = com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS;
    public static final java.lang.String DEFAULT_PROTOCOLVERSION = "";
    public static final java.lang.Integer DEFAULT_SYSTEMAPILEVEL = java.lang.Integer.valueOf(0);
    public static final java.lang.String DEFAULT_SYSTEMVERSION = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 11, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String buildType;
    @com.squareup.wire.ProtoField(tag = 16)
    public final com.navdy.service.library.events.Capabilities capabilities;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String clientVersion;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String deviceId;
    @com.squareup.wire.ProtoField(tag = 12, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String deviceMake;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String deviceName;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 7, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String deviceUuid;
    @com.squareup.wire.ProtoField(tag = 13, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean forceFullUpdate;
    @com.squareup.wire.ProtoField(tag = 9, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String kernelVersion;
    @com.squareup.wire.ProtoField(deprecated = true, enumType = com.navdy.service.library.events.LegacyCapability.class, label = com.squareup.wire.Message.Label.REPEATED, tag = 14, type = com.squareup.wire.Message.Datatype.ENUM)
    @java.lang.Deprecated
    public final java.util.List<com.navdy.service.library.events.LegacyCapability> legacyCapabilities;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 6, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String model;
    @com.squareup.wire.ProtoField(deprecated = true, label = com.squareup.wire.Message.Label.REPEATED, tag = 15, type = com.squareup.wire.Message.Datatype.STRING)
    @java.lang.Deprecated
    public final java.util.List<java.lang.String> musicPlayers_OBSOLETE;
    @com.squareup.wire.ProtoField(tag = 10, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.DeviceInfo.Platform platform;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String protocolVersion;
    @com.squareup.wire.ProtoField(tag = 8, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer systemApiLevel;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 5, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String systemVersion;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.DeviceInfo> {
        public java.lang.String buildType;
        public com.navdy.service.library.events.Capabilities capabilities;
        public java.lang.String clientVersion;
        public java.lang.String deviceId;
        public java.lang.String deviceMake;
        public java.lang.String deviceName;
        public java.lang.String deviceUuid;
        public java.lang.Boolean forceFullUpdate;
        public java.lang.String kernelVersion;
        public java.util.List<com.navdy.service.library.events.LegacyCapability> legacyCapabilities;
        public java.lang.String model;
        public java.util.List<java.lang.String> musicPlayers_OBSOLETE;
        public com.navdy.service.library.events.DeviceInfo.Platform platform;
        public java.lang.String protocolVersion;
        public java.lang.Integer systemApiLevel;
        public java.lang.String systemVersion;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.DeviceInfo message) {
            super(message);
            if (message != null) {
                this.deviceId = message.deviceId;
                this.clientVersion = message.clientVersion;
                this.protocolVersion = message.protocolVersion;
                this.deviceName = message.deviceName;
                this.systemVersion = message.systemVersion;
                this.model = message.model;
                this.deviceUuid = message.deviceUuid;
                this.systemApiLevel = message.systemApiLevel;
                this.kernelVersion = message.kernelVersion;
                this.platform = message.platform;
                this.buildType = message.buildType;
                this.deviceMake = message.deviceMake;
                this.forceFullUpdate = message.forceFullUpdate;
                this.legacyCapabilities = com.navdy.service.library.events.DeviceInfo.copyOf(message.legacyCapabilities);
                this.musicPlayers_OBSOLETE = com.navdy.service.library.events.DeviceInfo.copyOf(message.musicPlayers_OBSOLETE);
                this.capabilities = message.capabilities;
            }
        }

        public com.navdy.service.library.events.DeviceInfo.Builder deviceId(java.lang.String deviceId2) {
            this.deviceId = deviceId2;
            return this;
        }

        public com.navdy.service.library.events.DeviceInfo.Builder clientVersion(java.lang.String clientVersion2) {
            this.clientVersion = clientVersion2;
            return this;
        }

        public com.navdy.service.library.events.DeviceInfo.Builder protocolVersion(java.lang.String protocolVersion2) {
            this.protocolVersion = protocolVersion2;
            return this;
        }

        public com.navdy.service.library.events.DeviceInfo.Builder deviceName(java.lang.String deviceName2) {
            this.deviceName = deviceName2;
            return this;
        }

        public com.navdy.service.library.events.DeviceInfo.Builder systemVersion(java.lang.String systemVersion2) {
            this.systemVersion = systemVersion2;
            return this;
        }

        public com.navdy.service.library.events.DeviceInfo.Builder model(java.lang.String model2) {
            this.model = model2;
            return this;
        }

        public com.navdy.service.library.events.DeviceInfo.Builder deviceUuid(java.lang.String deviceUuid2) {
            this.deviceUuid = deviceUuid2;
            return this;
        }

        public com.navdy.service.library.events.DeviceInfo.Builder systemApiLevel(java.lang.Integer systemApiLevel2) {
            this.systemApiLevel = systemApiLevel2;
            return this;
        }

        public com.navdy.service.library.events.DeviceInfo.Builder kernelVersion(java.lang.String kernelVersion2) {
            this.kernelVersion = kernelVersion2;
            return this;
        }

        public com.navdy.service.library.events.DeviceInfo.Builder platform(com.navdy.service.library.events.DeviceInfo.Platform platform2) {
            this.platform = platform2;
            return this;
        }

        public com.navdy.service.library.events.DeviceInfo.Builder buildType(java.lang.String buildType2) {
            this.buildType = buildType2;
            return this;
        }

        public com.navdy.service.library.events.DeviceInfo.Builder deviceMake(java.lang.String deviceMake2) {
            this.deviceMake = deviceMake2;
            return this;
        }

        public com.navdy.service.library.events.DeviceInfo.Builder forceFullUpdate(java.lang.Boolean forceFullUpdate2) {
            this.forceFullUpdate = forceFullUpdate2;
            return this;
        }

        @java.lang.Deprecated
        public com.navdy.service.library.events.DeviceInfo.Builder legacyCapabilities(java.util.List<com.navdy.service.library.events.LegacyCapability> legacyCapabilities2) {
            this.legacyCapabilities = checkForNulls(legacyCapabilities2);
            return this;
        }

        @java.lang.Deprecated
        public com.navdy.service.library.events.DeviceInfo.Builder musicPlayers_OBSOLETE(java.util.List<java.lang.String> musicPlayers_OBSOLETE2) {
            this.musicPlayers_OBSOLETE = checkForNulls(musicPlayers_OBSOLETE2);
            return this;
        }

        public com.navdy.service.library.events.DeviceInfo.Builder capabilities(com.navdy.service.library.events.Capabilities capabilities2) {
            this.capabilities = capabilities2;
            return this;
        }

        public com.navdy.service.library.events.DeviceInfo build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.DeviceInfo(this);
        }
    }

    public enum Platform implements com.squareup.wire.ProtoEnum {
        PLATFORM_iOS(1),
        PLATFORM_Android(2);
        
        private final int value;

        private Platform(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public DeviceInfo(java.lang.String deviceId2, java.lang.String clientVersion2, java.lang.String protocolVersion2, java.lang.String deviceName2, java.lang.String systemVersion2, java.lang.String model2, java.lang.String deviceUuid2, java.lang.Integer systemApiLevel2, java.lang.String kernelVersion2, com.navdy.service.library.events.DeviceInfo.Platform platform2, java.lang.String buildType2, java.lang.String deviceMake2, java.lang.Boolean forceFullUpdate2, java.util.List<com.navdy.service.library.events.LegacyCapability> legacyCapabilities2, java.util.List<java.lang.String> musicPlayers_OBSOLETE2, com.navdy.service.library.events.Capabilities capabilities2) {
        this.deviceId = deviceId2;
        this.clientVersion = clientVersion2;
        this.protocolVersion = protocolVersion2;
        this.deviceName = deviceName2;
        this.systemVersion = systemVersion2;
        this.model = model2;
        this.deviceUuid = deviceUuid2;
        this.systemApiLevel = systemApiLevel2;
        this.kernelVersion = kernelVersion2;
        this.platform = platform2;
        this.buildType = buildType2;
        this.deviceMake = deviceMake2;
        this.forceFullUpdate = forceFullUpdate2;
        this.legacyCapabilities = immutableCopyOf(legacyCapabilities2);
        this.musicPlayers_OBSOLETE = immutableCopyOf(musicPlayers_OBSOLETE2);
        this.capabilities = capabilities2;
    }

    private DeviceInfo(com.navdy.service.library.events.DeviceInfo.Builder builder) {
        this(builder.deviceId, builder.clientVersion, builder.protocolVersion, builder.deviceName, builder.systemVersion, builder.model, builder.deviceUuid, builder.systemApiLevel, builder.kernelVersion, builder.platform, builder.buildType, builder.deviceMake, builder.forceFullUpdate, builder.legacyCapabilities, builder.musicPlayers_OBSOLETE, builder.capabilities);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.DeviceInfo)) {
            return false;
        }
        com.navdy.service.library.events.DeviceInfo o = (com.navdy.service.library.events.DeviceInfo) other;
        if (!equals((java.lang.Object) this.deviceId, (java.lang.Object) o.deviceId) || !equals((java.lang.Object) this.clientVersion, (java.lang.Object) o.clientVersion) || !equals((java.lang.Object) this.protocolVersion, (java.lang.Object) o.protocolVersion) || !equals((java.lang.Object) this.deviceName, (java.lang.Object) o.deviceName) || !equals((java.lang.Object) this.systemVersion, (java.lang.Object) o.systemVersion) || !equals((java.lang.Object) this.model, (java.lang.Object) o.model) || !equals((java.lang.Object) this.deviceUuid, (java.lang.Object) o.deviceUuid) || !equals((java.lang.Object) this.systemApiLevel, (java.lang.Object) o.systemApiLevel) || !equals((java.lang.Object) this.kernelVersion, (java.lang.Object) o.kernelVersion) || !equals((java.lang.Object) this.platform, (java.lang.Object) o.platform) || !equals((java.lang.Object) this.buildType, (java.lang.Object) o.buildType) || !equals((java.lang.Object) this.deviceMake, (java.lang.Object) o.deviceMake) || !equals((java.lang.Object) this.forceFullUpdate, (java.lang.Object) o.forceFullUpdate) || !equals(this.legacyCapabilities, o.legacyCapabilities) || !equals(this.musicPlayers_OBSOLETE, o.musicPlayers_OBSOLETE) || !equals((java.lang.Object) this.capabilities, (java.lang.Object) o.capabilities)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14 = 1;
        int i15 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.deviceId != null ? this.deviceId.hashCode() : 0) * 37;
        if (this.clientVersion != null) {
            i = this.clientVersion.hashCode();
        } else {
            i = 0;
        }
        int i16 = (hashCode + i) * 37;
        if (this.protocolVersion != null) {
            i2 = this.protocolVersion.hashCode();
        } else {
            i2 = 0;
        }
        int i17 = (i16 + i2) * 37;
        if (this.deviceName != null) {
            i3 = this.deviceName.hashCode();
        } else {
            i3 = 0;
        }
        int i18 = (i17 + i3) * 37;
        if (this.systemVersion != null) {
            i4 = this.systemVersion.hashCode();
        } else {
            i4 = 0;
        }
        int i19 = (i18 + i4) * 37;
        if (this.model != null) {
            i5 = this.model.hashCode();
        } else {
            i5 = 0;
        }
        int i20 = (i19 + i5) * 37;
        if (this.deviceUuid != null) {
            i6 = this.deviceUuid.hashCode();
        } else {
            i6 = 0;
        }
        int i21 = (i20 + i6) * 37;
        if (this.systemApiLevel != null) {
            i7 = this.systemApiLevel.hashCode();
        } else {
            i7 = 0;
        }
        int i22 = (i21 + i7) * 37;
        if (this.kernelVersion != null) {
            i8 = this.kernelVersion.hashCode();
        } else {
            i8 = 0;
        }
        int i23 = (i22 + i8) * 37;
        if (this.platform != null) {
            i9 = this.platform.hashCode();
        } else {
            i9 = 0;
        }
        int i24 = (i23 + i9) * 37;
        if (this.buildType != null) {
            i10 = this.buildType.hashCode();
        } else {
            i10 = 0;
        }
        int i25 = (i24 + i10) * 37;
        if (this.deviceMake != null) {
            i11 = this.deviceMake.hashCode();
        } else {
            i11 = 0;
        }
        int i26 = (i25 + i11) * 37;
        if (this.forceFullUpdate != null) {
            i12 = this.forceFullUpdate.hashCode();
        } else {
            i12 = 0;
        }
        int i27 = (i26 + i12) * 37;
        if (this.legacyCapabilities != null) {
            i13 = this.legacyCapabilities.hashCode();
        } else {
            i13 = 1;
        }
        int i28 = (i27 + i13) * 37;
        if (this.musicPlayers_OBSOLETE != null) {
            i14 = this.musicPlayers_OBSOLETE.hashCode();
        }
        int i29 = (i28 + i14) * 37;
        if (this.capabilities != null) {
            i15 = this.capabilities.hashCode();
        }
        int result2 = i29 + i15;
        this.hashCode = result2;
        return result2;
    }
}
