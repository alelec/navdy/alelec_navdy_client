package com.navdy.service.library.events.debug;

public final class StopDriveRecordingEvent extends com.squareup.wire.Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.debug.StopDriveRecordingEvent> {
        public Builder() {
        }

        public Builder(com.navdy.service.library.events.debug.StopDriveRecordingEvent message) {
            super(message);
        }

        public com.navdy.service.library.events.debug.StopDriveRecordingEvent build() {
            return new com.navdy.service.library.events.debug.StopDriveRecordingEvent(this);
        }
    }

    public StopDriveRecordingEvent() {
    }

    private StopDriveRecordingEvent(com.navdy.service.library.events.debug.StopDriveRecordingEvent.Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        return other instanceof com.navdy.service.library.events.debug.StopDriveRecordingEvent;
    }

    public int hashCode() {
        return 0;
    }
}
