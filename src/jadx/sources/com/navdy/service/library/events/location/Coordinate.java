package com.navdy.service.library.events.location;

public final class Coordinate extends com.squareup.wire.Message {
    public static final java.lang.Float DEFAULT_ACCURACY = java.lang.Float.valueOf(0.0f);
    public static final java.lang.Double DEFAULT_ALTITUDE = java.lang.Double.valueOf(0.0d);
    public static final java.lang.Float DEFAULT_BEARING = java.lang.Float.valueOf(0.0f);
    public static final java.lang.Double DEFAULT_LATITUDE = java.lang.Double.valueOf(0.0d);
    public static final java.lang.Double DEFAULT_LONGITUDE = java.lang.Double.valueOf(0.0d);
    public static final java.lang.String DEFAULT_PROVIDER = "";
    public static final java.lang.Float DEFAULT_SPEED = java.lang.Float.valueOf(0.0f);
    public static final java.lang.Long DEFAULT_TIMESTAMP = java.lang.Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.FLOAT)
    public final java.lang.Float accuracy;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.DOUBLE)
    public final java.lang.Double altitude;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.FLOAT)
    public final java.lang.Float bearing;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.DOUBLE)
    public final java.lang.Double latitude;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.DOUBLE)
    public final java.lang.Double longitude;
    @com.squareup.wire.ProtoField(tag = 8, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String provider;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.FLOAT)
    public final java.lang.Float speed;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long timestamp;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.location.Coordinate> {
        public java.lang.Float accuracy;
        public java.lang.Double altitude;
        public java.lang.Float bearing;
        public java.lang.Double latitude;
        public java.lang.Double longitude;
        public java.lang.String provider;
        public java.lang.Float speed;
        public java.lang.Long timestamp;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.location.Coordinate message) {
            super(message);
            if (message != null) {
                this.latitude = message.latitude;
                this.longitude = message.longitude;
                this.accuracy = message.accuracy;
                this.altitude = message.altitude;
                this.bearing = message.bearing;
                this.speed = message.speed;
                this.timestamp = message.timestamp;
                this.provider = message.provider;
            }
        }

        public com.navdy.service.library.events.location.Coordinate.Builder latitude(java.lang.Double latitude2) {
            this.latitude = latitude2;
            return this;
        }

        public com.navdy.service.library.events.location.Coordinate.Builder longitude(java.lang.Double longitude2) {
            this.longitude = longitude2;
            return this;
        }

        public com.navdy.service.library.events.location.Coordinate.Builder accuracy(java.lang.Float accuracy2) {
            this.accuracy = accuracy2;
            return this;
        }

        public com.navdy.service.library.events.location.Coordinate.Builder altitude(java.lang.Double altitude2) {
            this.altitude = altitude2;
            return this;
        }

        public com.navdy.service.library.events.location.Coordinate.Builder bearing(java.lang.Float bearing2) {
            this.bearing = bearing2;
            return this;
        }

        public com.navdy.service.library.events.location.Coordinate.Builder speed(java.lang.Float speed2) {
            this.speed = speed2;
            return this;
        }

        public com.navdy.service.library.events.location.Coordinate.Builder timestamp(java.lang.Long timestamp2) {
            this.timestamp = timestamp2;
            return this;
        }

        public com.navdy.service.library.events.location.Coordinate.Builder provider(java.lang.String provider2) {
            this.provider = provider2;
            return this;
        }

        public com.navdy.service.library.events.location.Coordinate build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.location.Coordinate(this);
        }
    }

    public Coordinate(java.lang.Double latitude2, java.lang.Double longitude2, java.lang.Float accuracy2, java.lang.Double altitude2, java.lang.Float bearing2, java.lang.Float speed2, java.lang.Long timestamp2, java.lang.String provider2) {
        this.latitude = latitude2;
        this.longitude = longitude2;
        this.accuracy = accuracy2;
        this.altitude = altitude2;
        this.bearing = bearing2;
        this.speed = speed2;
        this.timestamp = timestamp2;
        this.provider = provider2;
    }

    private Coordinate(com.navdy.service.library.events.location.Coordinate.Builder builder) {
        this(builder.latitude, builder.longitude, builder.accuracy, builder.altitude, builder.bearing, builder.speed, builder.timestamp, builder.provider);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.location.Coordinate)) {
            return false;
        }
        com.navdy.service.library.events.location.Coordinate o = (com.navdy.service.library.events.location.Coordinate) other;
        if (!equals((java.lang.Object) this.latitude, (java.lang.Object) o.latitude) || !equals((java.lang.Object) this.longitude, (java.lang.Object) o.longitude) || !equals((java.lang.Object) this.accuracy, (java.lang.Object) o.accuracy) || !equals((java.lang.Object) this.altitude, (java.lang.Object) o.altitude) || !equals((java.lang.Object) this.bearing, (java.lang.Object) o.bearing) || !equals((java.lang.Object) this.speed, (java.lang.Object) o.speed) || !equals((java.lang.Object) this.timestamp, (java.lang.Object) o.timestamp) || !equals((java.lang.Object) this.provider, (java.lang.Object) o.provider)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.latitude != null ? this.latitude.hashCode() : 0) * 37;
        if (this.longitude != null) {
            i = this.longitude.hashCode();
        } else {
            i = 0;
        }
        int i8 = (hashCode + i) * 37;
        if (this.accuracy != null) {
            i2 = this.accuracy.hashCode();
        } else {
            i2 = 0;
        }
        int i9 = (i8 + i2) * 37;
        if (this.altitude != null) {
            i3 = this.altitude.hashCode();
        } else {
            i3 = 0;
        }
        int i10 = (i9 + i3) * 37;
        if (this.bearing != null) {
            i4 = this.bearing.hashCode();
        } else {
            i4 = 0;
        }
        int i11 = (i10 + i4) * 37;
        if (this.speed != null) {
            i5 = this.speed.hashCode();
        } else {
            i5 = 0;
        }
        int i12 = (i11 + i5) * 37;
        if (this.timestamp != null) {
            i6 = this.timestamp.hashCode();
        } else {
            i6 = 0;
        }
        int i13 = (i12 + i6) * 37;
        if (this.provider != null) {
            i7 = this.provider.hashCode();
        }
        int result2 = i13 + i7;
        this.hashCode = result2;
        return result2;
    }
}
