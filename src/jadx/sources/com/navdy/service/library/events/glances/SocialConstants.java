package com.navdy.service.library.events.glances;

public enum SocialConstants implements com.squareup.wire.ProtoEnum {
    SOCIAL_FROM(0),
    SOCIAL_TO(1),
    SOCIAL_MESSAGE(2);
    
    private final int value;

    private SocialConstants(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
