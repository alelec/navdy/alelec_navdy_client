package com.navdy.service.library.events.glances;

public enum EmailConstants implements com.squareup.wire.ProtoEnum {
    EMAIL_FROM_EMAIL(0),
    EMAIL_FROM_NAME(1),
    EMAIL_TO_EMAIL(2),
    EMAIL_TO_NAME(3),
    EMAIL_SUBJECT(4),
    EMAIL_BODY(5);
    
    private final int value;

    private EmailConstants(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
