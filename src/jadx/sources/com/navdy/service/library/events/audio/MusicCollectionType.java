package com.navdy.service.library.events.audio;

public enum MusicCollectionType implements com.squareup.wire.ProtoEnum {
    COLLECTION_TYPE_UNKNOWN(0),
    COLLECTION_TYPE_PLAYLISTS(1),
    COLLECTION_TYPE_ARTISTS(2),
    COLLECTION_TYPE_ALBUMS(3),
    COLLECTION_TYPE_PODCASTS(4),
    COLLECTION_TYPE_AUDIOBOOKS(5);
    
    private final int value;

    private MusicCollectionType(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
