package com.navdy.service.library.events.audio;

public final class MusicCollectionInfo extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_CANSHUFFLE = java.lang.Boolean.valueOf(false);
    public static final java.lang.String DEFAULT_COLLECTIONID = "";
    public static final com.navdy.service.library.events.audio.MusicCollectionSource DEFAULT_COLLECTIONSOURCE = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    public static final com.navdy.service.library.events.audio.MusicCollectionType DEFAULT_COLLECTIONTYPE = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    public static final java.lang.Integer DEFAULT_INDEX = java.lang.Integer.valueOf(0);
    public static final java.lang.String DEFAULT_NAME = "";
    public static final java.lang.Integer DEFAULT_SIZE = java.lang.Integer.valueOf(0);
    public static final java.lang.String DEFAULT_SUBTITLE = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 8, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean canShuffle;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String collectionId;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer index;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String name;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer size;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String subtitle;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.MusicCollectionInfo> {
        public java.lang.Boolean canShuffle;
        public java.lang.String collectionId;
        public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
        public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
        public java.lang.Integer index;
        public java.lang.String name;
        public java.lang.Integer size;
        public java.lang.String subtitle;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.MusicCollectionInfo message) {
            super(message);
            if (message != null) {
                this.collectionSource = message.collectionSource;
                this.collectionType = message.collectionType;
                this.collectionId = message.collectionId;
                this.name = message.name;
                this.size = message.size;
                this.index = message.index;
                this.subtitle = message.subtitle;
                this.canShuffle = message.canShuffle;
            }
        }

        public com.navdy.service.library.events.audio.MusicCollectionInfo.Builder collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource collectionSource2) {
            this.collectionSource = collectionSource2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionInfo.Builder collectionType(com.navdy.service.library.events.audio.MusicCollectionType collectionType2) {
            this.collectionType = collectionType2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionInfo.Builder collectionId(java.lang.String collectionId2) {
            this.collectionId = collectionId2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionInfo.Builder name(java.lang.String name2) {
            this.name = name2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionInfo.Builder size(java.lang.Integer size2) {
            this.size = size2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionInfo.Builder index(java.lang.Integer index2) {
            this.index = index2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionInfo.Builder subtitle(java.lang.String subtitle2) {
            this.subtitle = subtitle2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionInfo.Builder canShuffle(java.lang.Boolean canShuffle2) {
            this.canShuffle = canShuffle2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionInfo build() {
            return new com.navdy.service.library.events.audio.MusicCollectionInfo(this);
        }
    }

    public MusicCollectionInfo(com.navdy.service.library.events.audio.MusicCollectionSource collectionSource2, com.navdy.service.library.events.audio.MusicCollectionType collectionType2, java.lang.String collectionId2, java.lang.String name2, java.lang.Integer size2, java.lang.Integer index2, java.lang.String subtitle2, java.lang.Boolean canShuffle2) {
        this.collectionSource = collectionSource2;
        this.collectionType = collectionType2;
        this.collectionId = collectionId2;
        this.name = name2;
        this.size = size2;
        this.index = index2;
        this.subtitle = subtitle2;
        this.canShuffle = canShuffle2;
    }

    private MusicCollectionInfo(com.navdy.service.library.events.audio.MusicCollectionInfo.Builder builder) {
        this(builder.collectionSource, builder.collectionType, builder.collectionId, builder.name, builder.size, builder.index, builder.subtitle, builder.canShuffle);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.MusicCollectionInfo)) {
            return false;
        }
        com.navdy.service.library.events.audio.MusicCollectionInfo o = (com.navdy.service.library.events.audio.MusicCollectionInfo) other;
        if (!equals((java.lang.Object) this.collectionSource, (java.lang.Object) o.collectionSource) || !equals((java.lang.Object) this.collectionType, (java.lang.Object) o.collectionType) || !equals((java.lang.Object) this.collectionId, (java.lang.Object) o.collectionId) || !equals((java.lang.Object) this.name, (java.lang.Object) o.name) || !equals((java.lang.Object) this.size, (java.lang.Object) o.size) || !equals((java.lang.Object) this.index, (java.lang.Object) o.index) || !equals((java.lang.Object) this.subtitle, (java.lang.Object) o.subtitle) || !equals((java.lang.Object) this.canShuffle, (java.lang.Object) o.canShuffle)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.collectionSource != null ? this.collectionSource.hashCode() : 0) * 37;
        if (this.collectionType != null) {
            i = this.collectionType.hashCode();
        } else {
            i = 0;
        }
        int i8 = (hashCode + i) * 37;
        if (this.collectionId != null) {
            i2 = this.collectionId.hashCode();
        } else {
            i2 = 0;
        }
        int i9 = (i8 + i2) * 37;
        if (this.name != null) {
            i3 = this.name.hashCode();
        } else {
            i3 = 0;
        }
        int i10 = (i9 + i3) * 37;
        if (this.size != null) {
            i4 = this.size.hashCode();
        } else {
            i4 = 0;
        }
        int i11 = (i10 + i4) * 37;
        if (this.index != null) {
            i5 = this.index.hashCode();
        } else {
            i5 = 0;
        }
        int i12 = (i11 + i5) * 37;
        if (this.subtitle != null) {
            i6 = this.subtitle.hashCode();
        } else {
            i6 = 0;
        }
        int i13 = (i12 + i6) * 37;
        if (this.canShuffle != null) {
            i7 = this.canShuffle.hashCode();
        }
        int result2 = i13 + i7;
        this.hashCode = result2;
        return result2;
    }
}
