package com.navdy.service.library.events.input;

public final class MediaRemoteKeyEvent extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.input.KeyEvent DEFAULT_ACTION = com.navdy.service.library.events.input.KeyEvent.KEY_DOWN;
    public static final com.navdy.service.library.events.input.MediaRemoteKey DEFAULT_KEY = com.navdy.service.library.events.input.MediaRemoteKey.MEDIA_REMOTE_KEY_SIRI;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.input.KeyEvent action;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.input.MediaRemoteKey key;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.input.MediaRemoteKeyEvent> {
        public com.navdy.service.library.events.input.KeyEvent action;
        public com.navdy.service.library.events.input.MediaRemoteKey key;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.input.MediaRemoteKeyEvent message) {
            super(message);
            if (message != null) {
                this.key = message.key;
                this.action = message.action;
            }
        }

        public com.navdy.service.library.events.input.MediaRemoteKeyEvent.Builder key(com.navdy.service.library.events.input.MediaRemoteKey key2) {
            this.key = key2;
            return this;
        }

        public com.navdy.service.library.events.input.MediaRemoteKeyEvent.Builder action(com.navdy.service.library.events.input.KeyEvent action2) {
            this.action = action2;
            return this;
        }

        public com.navdy.service.library.events.input.MediaRemoteKeyEvent build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.input.MediaRemoteKeyEvent(this);
        }
    }

    public MediaRemoteKeyEvent(com.navdy.service.library.events.input.MediaRemoteKey key2, com.navdy.service.library.events.input.KeyEvent action2) {
        this.key = key2;
        this.action = action2;
    }

    private MediaRemoteKeyEvent(com.navdy.service.library.events.input.MediaRemoteKeyEvent.Builder builder) {
        this(builder.key, builder.action);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.input.MediaRemoteKeyEvent)) {
            return false;
        }
        com.navdy.service.library.events.input.MediaRemoteKeyEvent o = (com.navdy.service.library.events.input.MediaRemoteKeyEvent) other;
        if (!equals((java.lang.Object) this.key, (java.lang.Object) o.key) || !equals((java.lang.Object) this.action, (java.lang.Object) o.action)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.key != null) {
            result = this.key.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.action != null) {
            i = this.action.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
