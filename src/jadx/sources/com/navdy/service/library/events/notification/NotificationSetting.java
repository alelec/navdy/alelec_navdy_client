package com.navdy.service.library.events.notification;

public final class NotificationSetting extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_APP = "";
    public static final java.lang.Boolean DEFAULT_ENABLED = java.lang.Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String app;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean enabled;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.notification.NotificationSetting> {
        public java.lang.String app;
        public java.lang.Boolean enabled;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.notification.NotificationSetting message) {
            super(message);
            if (message != null) {
                this.app = message.app;
                this.enabled = message.enabled;
            }
        }

        public com.navdy.service.library.events.notification.NotificationSetting.Builder app(java.lang.String app2) {
            this.app = app2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationSetting.Builder enabled(java.lang.Boolean enabled2) {
            this.enabled = enabled2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationSetting build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.notification.NotificationSetting(this);
        }
    }

    public NotificationSetting(java.lang.String app2, java.lang.Boolean enabled2) {
        this.app = app2;
        this.enabled = enabled2;
    }

    private NotificationSetting(com.navdy.service.library.events.notification.NotificationSetting.Builder builder) {
        this(builder.app, builder.enabled);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.notification.NotificationSetting)) {
            return false;
        }
        com.navdy.service.library.events.notification.NotificationSetting o = (com.navdy.service.library.events.notification.NotificationSetting) other;
        if (!equals((java.lang.Object) this.app, (java.lang.Object) o.app) || !equals((java.lang.Object) this.enabled, (java.lang.Object) o.enabled)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.app != null) {
            result = this.app.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.enabled != null) {
            i = this.enabled.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
