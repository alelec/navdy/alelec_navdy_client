package com.navdy.service.library.events.callcontrol;

public final class TelephonyResponse extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.callcontrol.CallAction DEFAULT_ACTION = com.navdy.service.library.events.callcontrol.CallAction.CALL_ACCEPT;
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    public static final java.lang.String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.callcontrol.CallAction action;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.callcontrol.TelephonyResponse> {
        public com.navdy.service.library.events.callcontrol.CallAction action;
        public com.navdy.service.library.events.RequestStatus status;
        public java.lang.String statusDetail;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.callcontrol.TelephonyResponse message) {
            super(message);
            if (message != null) {
                this.action = message.action;
                this.status = message.status;
                this.statusDetail = message.statusDetail;
            }
        }

        public com.navdy.service.library.events.callcontrol.TelephonyResponse.Builder action(com.navdy.service.library.events.callcontrol.CallAction action2) {
            this.action = action2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.TelephonyResponse.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.TelephonyResponse.Builder statusDetail(java.lang.String statusDetail2) {
            this.statusDetail = statusDetail2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.TelephonyResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.callcontrol.TelephonyResponse(this);
        }
    }

    public TelephonyResponse(com.navdy.service.library.events.callcontrol.CallAction action2, com.navdy.service.library.events.RequestStatus status2, java.lang.String statusDetail2) {
        this.action = action2;
        this.status = status2;
        this.statusDetail = statusDetail2;
    }

    private TelephonyResponse(com.navdy.service.library.events.callcontrol.TelephonyResponse.Builder builder) {
        this(builder.action, builder.status, builder.statusDetail);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.callcontrol.TelephonyResponse)) {
            return false;
        }
        com.navdy.service.library.events.callcontrol.TelephonyResponse o = (com.navdy.service.library.events.callcontrol.TelephonyResponse) other;
        if (!equals((java.lang.Object) this.action, (java.lang.Object) o.action) || !equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.statusDetail, (java.lang.Object) o.statusDetail)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.action != null ? this.action.hashCode() : 0) * 37;
        if (this.status != null) {
            i = this.status.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.statusDetail != null) {
            i2 = this.statusDetail.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
