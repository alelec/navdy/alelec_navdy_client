package com.navdy.service.library.events.location;

public final class TransmitLocation extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_SENDLOCATION = java.lang.Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean sendLocation;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.location.TransmitLocation> {
        public java.lang.Boolean sendLocation;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.location.TransmitLocation message) {
            super(message);
            if (message != null) {
                this.sendLocation = message.sendLocation;
            }
        }

        public com.navdy.service.library.events.location.TransmitLocation.Builder sendLocation(java.lang.Boolean sendLocation2) {
            this.sendLocation = sendLocation2;
            return this;
        }

        public com.navdy.service.library.events.location.TransmitLocation build() {
            return new com.navdy.service.library.events.location.TransmitLocation(this);
        }
    }

    public TransmitLocation(java.lang.Boolean sendLocation2) {
        this.sendLocation = sendLocation2;
    }

    private TransmitLocation(com.navdy.service.library.events.location.TransmitLocation.Builder builder) {
        this(builder.sendLocation);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.location.TransmitLocation)) {
            return false;
        }
        return equals((java.lang.Object) this.sendLocation, (java.lang.Object) ((com.navdy.service.library.events.location.TransmitLocation) other).sendLocation);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.sendLocation != null ? this.sendLocation.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
