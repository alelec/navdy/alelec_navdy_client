package com.navdy.service.library.events.navigation;

public final class NavigationSessionRouteChange extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_OLDROUTEID = "";
    public static final com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason DEFAULT_REASON = com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason.NAV_SESSION_OFF_REROUTE;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 2)
    public final com.navdy.service.library.events.navigation.NavigationRouteResult newRoute;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String oldRouteId;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason reason;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.navigation.NavigationSessionRouteChange> {
        public com.navdy.service.library.events.navigation.NavigationRouteResult newRoute;
        public java.lang.String oldRouteId;
        public com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason reason;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.navigation.NavigationSessionRouteChange message) {
            super(message);
            if (message != null) {
                this.oldRouteId = message.oldRouteId;
                this.newRoute = message.newRoute;
                this.reason = message.reason;
            }
        }

        public com.navdy.service.library.events.navigation.NavigationSessionRouteChange.Builder oldRouteId(java.lang.String oldRouteId2) {
            this.oldRouteId = oldRouteId2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionRouteChange.Builder newRoute(com.navdy.service.library.events.navigation.NavigationRouteResult newRoute2) {
            this.newRoute = newRoute2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionRouteChange.Builder reason(com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason reason2) {
            this.reason = reason2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionRouteChange build() {
            return new com.navdy.service.library.events.navigation.NavigationSessionRouteChange(this);
        }
    }

    public enum RerouteReason implements com.squareup.wire.ProtoEnum {
        NAV_SESSION_OFF_REROUTE(1),
        NAV_SESSION_TRAFFIC_REROUTE(2),
        NAV_SESSION_ARRIVAL_REROUTE(3),
        NAV_SESSION_FUEL_REROUTE(4),
        NAV_SESSION_ROUTE_PICKER(5);
        
        private final int value;

        private RerouteReason(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public NavigationSessionRouteChange(java.lang.String oldRouteId2, com.navdy.service.library.events.navigation.NavigationRouteResult newRoute2, com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason reason2) {
        this.oldRouteId = oldRouteId2;
        this.newRoute = newRoute2;
        this.reason = reason2;
    }

    private NavigationSessionRouteChange(com.navdy.service.library.events.navigation.NavigationSessionRouteChange.Builder builder) {
        this(builder.oldRouteId, builder.newRoute, builder.reason);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.navigation.NavigationSessionRouteChange)) {
            return false;
        }
        com.navdy.service.library.events.navigation.NavigationSessionRouteChange o = (com.navdy.service.library.events.navigation.NavigationSessionRouteChange) other;
        if (!equals((java.lang.Object) this.oldRouteId, (java.lang.Object) o.oldRouteId) || !equals((java.lang.Object) this.newRoute, (java.lang.Object) o.newRoute) || !equals((java.lang.Object) this.reason, (java.lang.Object) o.reason)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.oldRouteId != null ? this.oldRouteId.hashCode() : 0) * 37;
        if (this.newRoute != null) {
            i = this.newRoute.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.reason != null) {
            i2 = this.reason.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
