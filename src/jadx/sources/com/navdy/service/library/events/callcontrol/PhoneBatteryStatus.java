package com.navdy.service.library.events.callcontrol;

public final class PhoneBatteryStatus extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_CHARGING = java.lang.Boolean.valueOf(false);
    public static final java.lang.Integer DEFAULT_LEVEL = java.lang.Integer.valueOf(0);
    public static final com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus DEFAULT_STATUS = com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus.BATTERY_OK;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean charging;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer level;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus status;

    public enum BatteryStatus implements com.squareup.wire.ProtoEnum {
        BATTERY_OK(1),
        BATTERY_LOW(2),
        BATTERY_EXTREMELY_LOW(3);
        
        private final int value;

        private BatteryStatus(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.callcontrol.PhoneBatteryStatus> {
        public java.lang.Boolean charging;
        public java.lang.Integer level;
        public com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus status;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.level = message.level;
                this.charging = message.charging;
            }
        }

        public com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.Builder status(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.Builder level(java.lang.Integer level2) {
            this.level = level2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.Builder charging(java.lang.Boolean charging2) {
            this.charging = charging2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.PhoneBatteryStatus build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.callcontrol.PhoneBatteryStatus(this);
        }
    }

    public PhoneBatteryStatus(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus status2, java.lang.Integer level2, java.lang.Boolean charging2) {
        this.status = status2;
        this.level = level2;
        this.charging = charging2;
    }

    private PhoneBatteryStatus(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.Builder builder) {
        this(builder.status, builder.level, builder.charging);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.callcontrol.PhoneBatteryStatus)) {
            return false;
        }
        com.navdy.service.library.events.callcontrol.PhoneBatteryStatus o = (com.navdy.service.library.events.callcontrol.PhoneBatteryStatus) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.level, (java.lang.Object) o.level) || !equals((java.lang.Object) this.charging, (java.lang.Object) o.charging)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.level != null) {
            i = this.level.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.charging != null) {
            i2 = this.charging.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
