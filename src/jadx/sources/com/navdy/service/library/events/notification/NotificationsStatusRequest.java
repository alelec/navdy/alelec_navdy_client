package com.navdy.service.library.events.notification;

public final class NotificationsStatusRequest extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.notification.NotificationsState DEFAULT_NEWSTATE = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_ENABLED;
    public static final com.navdy.service.library.events.notification.ServiceType DEFAULT_SERVICE = com.navdy.service.library.events.notification.ServiceType.SERVICE_ANCS;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.notification.NotificationsState newState;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.notification.ServiceType service;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.notification.NotificationsStatusRequest> {
        public com.navdy.service.library.events.notification.NotificationsState newState;
        public com.navdy.service.library.events.notification.ServiceType service;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.notification.NotificationsStatusRequest message) {
            super(message);
            if (message != null) {
                this.newState = message.newState;
                this.service = message.service;
            }
        }

        public com.navdy.service.library.events.notification.NotificationsStatusRequest.Builder newState(com.navdy.service.library.events.notification.NotificationsState newState2) {
            this.newState = newState2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationsStatusRequest.Builder service(com.navdy.service.library.events.notification.ServiceType service2) {
            this.service = service2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationsStatusRequest build() {
            return new com.navdy.service.library.events.notification.NotificationsStatusRequest(this);
        }
    }

    public NotificationsStatusRequest(com.navdy.service.library.events.notification.NotificationsState newState2, com.navdy.service.library.events.notification.ServiceType service2) {
        this.newState = newState2;
        this.service = service2;
    }

    private NotificationsStatusRequest(com.navdy.service.library.events.notification.NotificationsStatusRequest.Builder builder) {
        this(builder.newState, builder.service);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.notification.NotificationsStatusRequest)) {
            return false;
        }
        com.navdy.service.library.events.notification.NotificationsStatusRequest o = (com.navdy.service.library.events.notification.NotificationsStatusRequest) other;
        if (!equals((java.lang.Object) this.newState, (java.lang.Object) o.newState) || !equals((java.lang.Object) this.service, (java.lang.Object) o.service)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.newState != null) {
            result = this.newState.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.service != null) {
            i = this.service.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
