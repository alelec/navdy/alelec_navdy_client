package com.navdy.service.library.events.glances;

public enum GenericConstants implements com.squareup.wire.ProtoEnum {
    GENERIC_TITLE(0),
    GENERIC_MESSAGE(1),
    GENERIC_MAIN_ICON(2),
    GENERIC_SIDE_ICON(3);
    
    private final int value;

    private GenericConstants(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
