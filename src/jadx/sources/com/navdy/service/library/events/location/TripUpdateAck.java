package com.navdy.service.library.events.location;

public final class TripUpdateAck extends com.squareup.wire.Message {
    public static final java.lang.Integer DEFAULT_SEQUENCE_NUMBER = java.lang.Integer.valueOf(0);
    public static final java.lang.Long DEFAULT_TRIP_NUMBER = java.lang.Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer sequence_number;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long trip_number;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.location.TripUpdateAck> {
        public java.lang.Integer sequence_number;
        public java.lang.Long trip_number;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.location.TripUpdateAck message) {
            super(message);
            if (message != null) {
                this.trip_number = message.trip_number;
                this.sequence_number = message.sequence_number;
            }
        }

        public com.navdy.service.library.events.location.TripUpdateAck.Builder trip_number(java.lang.Long trip_number2) {
            this.trip_number = trip_number2;
            return this;
        }

        public com.navdy.service.library.events.location.TripUpdateAck.Builder sequence_number(java.lang.Integer sequence_number2) {
            this.sequence_number = sequence_number2;
            return this;
        }

        public com.navdy.service.library.events.location.TripUpdateAck build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.location.TripUpdateAck(this);
        }
    }

    public TripUpdateAck(java.lang.Long trip_number2, java.lang.Integer sequence_number2) {
        this.trip_number = trip_number2;
        this.sequence_number = sequence_number2;
    }

    private TripUpdateAck(com.navdy.service.library.events.location.TripUpdateAck.Builder builder) {
        this(builder.trip_number, builder.sequence_number);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.location.TripUpdateAck)) {
            return false;
        }
        com.navdy.service.library.events.location.TripUpdateAck o = (com.navdy.service.library.events.location.TripUpdateAck) other;
        if (!equals((java.lang.Object) this.trip_number, (java.lang.Object) o.trip_number) || !equals((java.lang.Object) this.sequence_number, (java.lang.Object) o.sequence_number)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.trip_number != null) {
            result = this.trip_number.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.sequence_number != null) {
            i = this.sequence_number.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
