package com.navdy.service.library.events.audio;

public final class VoiceAssistResponse extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.audio.VoiceAssistResponse.VoiceAssistState DEFAULT_STATE = com.navdy.service.library.events.audio.VoiceAssistResponse.VoiceAssistState.VOICE_ASSIST_AVAILABLE;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.VoiceAssistResponse.VoiceAssistState state;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.VoiceAssistResponse> {
        public com.navdy.service.library.events.audio.VoiceAssistResponse.VoiceAssistState state;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.VoiceAssistResponse message) {
            super(message);
            if (message != null) {
                this.state = message.state;
            }
        }

        public com.navdy.service.library.events.audio.VoiceAssistResponse.Builder state(com.navdy.service.library.events.audio.VoiceAssistResponse.VoiceAssistState state2) {
            this.state = state2;
            return this;
        }

        public com.navdy.service.library.events.audio.VoiceAssistResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.audio.VoiceAssistResponse(this);
        }
    }

    public enum VoiceAssistState implements com.squareup.wire.ProtoEnum {
        VOICE_ASSIST_AVAILABLE(0),
        VOICE_ASSIST_NOT_AVAILABLE(1),
        VOICE_ASSIST_LISTENING(2),
        VOICE_ASSIST_STOPPED(3);
        
        private final int value;

        private VoiceAssistState(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public VoiceAssistResponse(com.navdy.service.library.events.audio.VoiceAssistResponse.VoiceAssistState state2) {
        this.state = state2;
    }

    private VoiceAssistResponse(com.navdy.service.library.events.audio.VoiceAssistResponse.Builder builder) {
        this(builder.state);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.VoiceAssistResponse)) {
            return false;
        }
        return equals((java.lang.Object) this.state, (java.lang.Object) ((com.navdy.service.library.events.audio.VoiceAssistResponse) other).state);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.state != null ? this.state.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
