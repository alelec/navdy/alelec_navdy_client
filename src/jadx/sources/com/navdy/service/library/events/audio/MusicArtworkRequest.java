package com.navdy.service.library.events.audio;

public final class MusicArtworkRequest extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_ALBUM = "";
    public static final java.lang.String DEFAULT_AUTHOR = "";
    public static final java.lang.String DEFAULT_COLLECTIONID = "";
    public static final com.navdy.service.library.events.audio.MusicCollectionSource DEFAULT_COLLECTIONSOURCE = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    public static final com.navdy.service.library.events.audio.MusicCollectionType DEFAULT_COLLECTIONTYPE = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    public static final java.lang.String DEFAULT_NAME = "";
    public static final java.lang.Integer DEFAULT_SIZE = java.lang.Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String album;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String author;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String collectionId;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String name;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.UINT32)
    public final java.lang.Integer size;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.MusicArtworkRequest> {
        public java.lang.String album;
        public java.lang.String author;
        public java.lang.String collectionId;
        public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
        public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
        public java.lang.String name;
        public java.lang.Integer size;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.MusicArtworkRequest message) {
            super(message);
            if (message != null) {
                this.collectionSource = message.collectionSource;
                this.name = message.name;
                this.album = message.album;
                this.author = message.author;
                this.size = message.size;
                this.collectionType = message.collectionType;
                this.collectionId = message.collectionId;
            }
        }

        public com.navdy.service.library.events.audio.MusicArtworkRequest.Builder collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource collectionSource2) {
            this.collectionSource = collectionSource2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicArtworkRequest.Builder name(java.lang.String name2) {
            this.name = name2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicArtworkRequest.Builder album(java.lang.String album2) {
            this.album = album2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicArtworkRequest.Builder author(java.lang.String author2) {
            this.author = author2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicArtworkRequest.Builder size(java.lang.Integer size2) {
            this.size = size2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicArtworkRequest.Builder collectionType(com.navdy.service.library.events.audio.MusicCollectionType collectionType2) {
            this.collectionType = collectionType2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicArtworkRequest.Builder collectionId(java.lang.String collectionId2) {
            this.collectionId = collectionId2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicArtworkRequest build() {
            return new com.navdy.service.library.events.audio.MusicArtworkRequest(this);
        }
    }

    public MusicArtworkRequest(com.navdy.service.library.events.audio.MusicCollectionSource collectionSource2, java.lang.String name2, java.lang.String album2, java.lang.String author2, java.lang.Integer size2, com.navdy.service.library.events.audio.MusicCollectionType collectionType2, java.lang.String collectionId2) {
        this.collectionSource = collectionSource2;
        this.name = name2;
        this.album = album2;
        this.author = author2;
        this.size = size2;
        this.collectionType = collectionType2;
        this.collectionId = collectionId2;
    }

    private MusicArtworkRequest(com.navdy.service.library.events.audio.MusicArtworkRequest.Builder builder) {
        this(builder.collectionSource, builder.name, builder.album, builder.author, builder.size, builder.collectionType, builder.collectionId);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.MusicArtworkRequest)) {
            return false;
        }
        com.navdy.service.library.events.audio.MusicArtworkRequest o = (com.navdy.service.library.events.audio.MusicArtworkRequest) other;
        if (!equals((java.lang.Object) this.collectionSource, (java.lang.Object) o.collectionSource) || !equals((java.lang.Object) this.name, (java.lang.Object) o.name) || !equals((java.lang.Object) this.album, (java.lang.Object) o.album) || !equals((java.lang.Object) this.author, (java.lang.Object) o.author) || !equals((java.lang.Object) this.size, (java.lang.Object) o.size) || !equals((java.lang.Object) this.collectionType, (java.lang.Object) o.collectionType) || !equals((java.lang.Object) this.collectionId, (java.lang.Object) o.collectionId)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.collectionSource != null ? this.collectionSource.hashCode() : 0) * 37;
        if (this.name != null) {
            i = this.name.hashCode();
        } else {
            i = 0;
        }
        int i7 = (hashCode + i) * 37;
        if (this.album != null) {
            i2 = this.album.hashCode();
        } else {
            i2 = 0;
        }
        int i8 = (i7 + i2) * 37;
        if (this.author != null) {
            i3 = this.author.hashCode();
        } else {
            i3 = 0;
        }
        int i9 = (i8 + i3) * 37;
        if (this.size != null) {
            i4 = this.size.hashCode();
        } else {
            i4 = 0;
        }
        int i10 = (i9 + i4) * 37;
        if (this.collectionType != null) {
            i5 = this.collectionType.hashCode();
        } else {
            i5 = 0;
        }
        int i11 = (i10 + i5) * 37;
        if (this.collectionId != null) {
            i6 = this.collectionId.hashCode();
        }
        int result2 = i11 + i6;
        this.hashCode = result2;
        return result2;
    }
}
