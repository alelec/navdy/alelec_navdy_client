package com.navdy.service.library.events.contacts;

public final class ContactRequest extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_IDENTIFIER = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String identifier;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.contacts.ContactRequest> {
        public java.lang.String identifier;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.contacts.ContactRequest message) {
            super(message);
            if (message != null) {
                this.identifier = message.identifier;
            }
        }

        public com.navdy.service.library.events.contacts.ContactRequest.Builder identifier(java.lang.String identifier2) {
            this.identifier = identifier2;
            return this;
        }

        public com.navdy.service.library.events.contacts.ContactRequest build() {
            return new com.navdy.service.library.events.contacts.ContactRequest(this);
        }
    }

    public ContactRequest(java.lang.String identifier2) {
        this.identifier = identifier2;
    }

    private ContactRequest(com.navdy.service.library.events.contacts.ContactRequest.Builder builder) {
        this(builder.identifier);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.contacts.ContactRequest)) {
            return false;
        }
        return equals((java.lang.Object) this.identifier, (java.lang.Object) ((com.navdy.service.library.events.contacts.ContactRequest) other).identifier);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.identifier != null ? this.identifier.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
