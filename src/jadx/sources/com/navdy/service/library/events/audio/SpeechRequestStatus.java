package com.navdy.service.library.events.audio;

public final class SpeechRequestStatus extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_ID = "";
    public static final com.navdy.service.library.events.audio.SpeechRequestStatus.SpeechRequestStatusType DEFAULT_STATUS = com.navdy.service.library.events.audio.SpeechRequestStatus.SpeechRequestStatusType.SPEECH_REQUEST_STARTING;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String id;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.SpeechRequestStatus.SpeechRequestStatusType status;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.SpeechRequestStatus> {
        public java.lang.String id;
        public com.navdy.service.library.events.audio.SpeechRequestStatus.SpeechRequestStatusType status;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.SpeechRequestStatus message) {
            super(message);
            if (message != null) {
                this.id = message.id;
                this.status = message.status;
            }
        }

        public com.navdy.service.library.events.audio.SpeechRequestStatus.Builder id(java.lang.String id2) {
            this.id = id2;
            return this;
        }

        public com.navdy.service.library.events.audio.SpeechRequestStatus.Builder status(com.navdy.service.library.events.audio.SpeechRequestStatus.SpeechRequestStatusType status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.audio.SpeechRequestStatus build() {
            return new com.navdy.service.library.events.audio.SpeechRequestStatus(this);
        }
    }

    public enum SpeechRequestStatusType implements com.squareup.wire.ProtoEnum {
        SPEECH_REQUEST_STARTING(1),
        SPEECH_REQUEST_STOPPED(2);
        
        private final int value;

        private SpeechRequestStatusType(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public SpeechRequestStatus(java.lang.String id2, com.navdy.service.library.events.audio.SpeechRequestStatus.SpeechRequestStatusType status2) {
        this.id = id2;
        this.status = status2;
    }

    private SpeechRequestStatus(com.navdy.service.library.events.audio.SpeechRequestStatus.Builder builder) {
        this(builder.id, builder.status);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.SpeechRequestStatus)) {
            return false;
        }
        com.navdy.service.library.events.audio.SpeechRequestStatus o = (com.navdy.service.library.events.audio.SpeechRequestStatus) other;
        if (!equals((java.lang.Object) this.id, (java.lang.Object) o.id) || !equals((java.lang.Object) this.status, (java.lang.Object) o.status)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.id != null) {
            result = this.id.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.status != null) {
            i = this.status.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
