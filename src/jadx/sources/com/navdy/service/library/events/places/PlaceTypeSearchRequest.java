package com.navdy.service.library.events.places;

public final class PlaceTypeSearchRequest extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.places.PlaceType DEFAULT_PLACE_TYPE = com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_UNKNOWN;
    public static final java.lang.String DEFAULT_REQUEST_ID = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.places.PlaceType place_type;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String request_id;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.places.PlaceTypeSearchRequest> {
        public com.navdy.service.library.events.places.PlaceType place_type;
        public java.lang.String request_id;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.places.PlaceTypeSearchRequest message) {
            super(message);
            if (message != null) {
                this.request_id = message.request_id;
                this.place_type = message.place_type;
            }
        }

        public com.navdy.service.library.events.places.PlaceTypeSearchRequest.Builder request_id(java.lang.String request_id2) {
            this.request_id = request_id2;
            return this;
        }

        public com.navdy.service.library.events.places.PlaceTypeSearchRequest.Builder place_type(com.navdy.service.library.events.places.PlaceType place_type2) {
            this.place_type = place_type2;
            return this;
        }

        public com.navdy.service.library.events.places.PlaceTypeSearchRequest build() {
            return new com.navdy.service.library.events.places.PlaceTypeSearchRequest(this);
        }
    }

    public PlaceTypeSearchRequest(java.lang.String request_id2, com.navdy.service.library.events.places.PlaceType place_type2) {
        this.request_id = request_id2;
        this.place_type = place_type2;
    }

    private PlaceTypeSearchRequest(com.navdy.service.library.events.places.PlaceTypeSearchRequest.Builder builder) {
        this(builder.request_id, builder.place_type);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.places.PlaceTypeSearchRequest)) {
            return false;
        }
        com.navdy.service.library.events.places.PlaceTypeSearchRequest o = (com.navdy.service.library.events.places.PlaceTypeSearchRequest) other;
        if (!equals((java.lang.Object) this.request_id, (java.lang.Object) o.request_id) || !equals((java.lang.Object) this.place_type, (java.lang.Object) o.place_type)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.request_id != null) {
            result = this.request_id.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.place_type != null) {
            i = this.place_type.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
