package com.navdy.service.library.events;

public final class Notification extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_MESSAGE_DATA = "";
    public static final java.lang.String DEFAULT_ORIGIN = "";
    public static final java.lang.String DEFAULT_SENDER = "";
    public static final java.lang.Integer DEFAULT_SMS_CLASS = java.lang.Integer.valueOf(0);
    public static final java.lang.String DEFAULT_TARGET = "";
    public static final java.lang.String DEFAULT_TYPE = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String message_data;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String origin;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String sender;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer sms_class;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String target;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String type;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.Notification> {
        public java.lang.String message_data;
        public java.lang.String origin;
        public java.lang.String sender;
        public java.lang.Integer sms_class;
        public java.lang.String target;
        public java.lang.String type;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.Notification message) {
            super(message);
            if (message != null) {
                this.sms_class = message.sms_class;
                this.origin = message.origin;
                this.sender = message.sender;
                this.target = message.target;
                this.message_data = message.message_data;
                this.type = message.type;
            }
        }

        public com.navdy.service.library.events.Notification.Builder sms_class(java.lang.Integer sms_class2) {
            this.sms_class = sms_class2;
            return this;
        }

        public com.navdy.service.library.events.Notification.Builder origin(java.lang.String origin2) {
            this.origin = origin2;
            return this;
        }

        public com.navdy.service.library.events.Notification.Builder sender(java.lang.String sender2) {
            this.sender = sender2;
            return this;
        }

        public com.navdy.service.library.events.Notification.Builder target(java.lang.String target2) {
            this.target = target2;
            return this;
        }

        public com.navdy.service.library.events.Notification.Builder message_data(java.lang.String message_data2) {
            this.message_data = message_data2;
            return this;
        }

        public com.navdy.service.library.events.Notification.Builder type(java.lang.String type2) {
            this.type = type2;
            return this;
        }

        public com.navdy.service.library.events.Notification build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.Notification(this);
        }
    }

    public Notification(java.lang.Integer sms_class2, java.lang.String origin2, java.lang.String sender2, java.lang.String target2, java.lang.String message_data2, java.lang.String type2) {
        this.sms_class = sms_class2;
        this.origin = origin2;
        this.sender = sender2;
        this.target = target2;
        this.message_data = message_data2;
        this.type = type2;
    }

    private Notification(com.navdy.service.library.events.Notification.Builder builder) {
        this(builder.sms_class, builder.origin, builder.sender, builder.target, builder.message_data, builder.type);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.Notification)) {
            return false;
        }
        com.navdy.service.library.events.Notification o = (com.navdy.service.library.events.Notification) other;
        if (!equals((java.lang.Object) this.sms_class, (java.lang.Object) o.sms_class) || !equals((java.lang.Object) this.origin, (java.lang.Object) o.origin) || !equals((java.lang.Object) this.sender, (java.lang.Object) o.sender) || !equals((java.lang.Object) this.target, (java.lang.Object) o.target) || !equals((java.lang.Object) this.message_data, (java.lang.Object) o.message_data) || !equals((java.lang.Object) this.type, (java.lang.Object) o.type)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.sms_class != null ? this.sms_class.hashCode() : 0) * 37;
        if (this.origin != null) {
            i = this.origin.hashCode();
        } else {
            i = 0;
        }
        int i6 = (hashCode + i) * 37;
        if (this.sender != null) {
            i2 = this.sender.hashCode();
        } else {
            i2 = 0;
        }
        int i7 = (i6 + i2) * 37;
        if (this.target != null) {
            i3 = this.target.hashCode();
        } else {
            i3 = 0;
        }
        int i8 = (i7 + i3) * 37;
        if (this.message_data != null) {
            i4 = this.message_data.hashCode();
        } else {
            i4 = 0;
        }
        int i9 = (i8 + i4) * 37;
        if (this.type != null) {
            i5 = this.type.hashCode();
        }
        int result2 = i9 + i5;
        this.hashCode = result2;
        return result2;
    }
}
