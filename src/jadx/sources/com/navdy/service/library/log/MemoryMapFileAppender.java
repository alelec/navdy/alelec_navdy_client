package com.navdy.service.library.log;

public class MemoryMapFileAppender implements com.navdy.service.library.log.LogAppender {
    private static final java.lang.String COLON = ":";
    private static final java.lang.String CURRENT_POINTER_PREF = "pointer";
    private static final java.lang.String CURRENT_POINTER_PREF_FILE_SUFFIX = "_current_log_pointer";
    private static final long MIN_FILE_SIZE = 16384;
    private static final java.lang.String NEWLINE = "\r\n";
    private static final byte[] ROLLOVER_MARKER = "\r\n<<<<rolling>>>>\r\n".getBytes();
    private static final int ROLLOVER_META_LEN = ROLLOVER_MARKER.length;
    private static final java.lang.String SLASH = "/";
    private static final java.lang.String SPACE = " ";
    private static final java.lang.String TAG = "MemoryMapFileAppender";
    private java.lang.StringBuilder builder;
    private android.content.Context context;
    private android.content.SharedPreferences.Editor currentPointerPrefEditor;
    private java.lang.String currentPointerPrefFileName;
    private java.text.DateFormat dateFormat;
    private java.lang.String fileName;
    private long fileSize;
    private int maxFiles;
    private java.nio.MappedByteBuffer memoryMap;

    public MemoryMapFileAppender(android.content.Context context2, java.lang.String path, java.lang.String fileName2, long fileSize2, int maxFiles2) {
        this(context2, path, fileName2, fileSize2, maxFiles2, true);
    }

    public MemoryMapFileAppender(android.content.Context context2, java.lang.String path, java.lang.String fileName2, long fileSize2, int maxFiles2, boolean useProcessName) {
        this.maxFiles = 0;
        this.builder = new java.lang.StringBuilder(20480);
        this.dateFormat = new java.text.SimpleDateFormat("MM-dd HH:mm:ss.SSS", java.util.Locale.US);
        if (context2 == null || android.text.TextUtils.isEmpty(fileName2)) {
            throw new java.lang.IllegalArgumentException();
        }
        this.maxFiles = maxFiles2;
        if (fileSize2 < 16384) {
            fileSize2 = 16384;
        }
        java.lang.String processName = "";
        if (useProcessName) {
            processName = "_" + com.navdy.service.library.util.SystemUtils.getProcessName(context2, android.os.Process.myPid());
        }
        this.context = context2;
        this.fileName = path + java.io.File.separator + fileName2 + processName;
        this.fileSize = fileSize2;
        this.currentPointerPrefFileName = processName + CURRENT_POINTER_PREF_FILE_SUFFIX;
        try {
            android.util.Log.d(TAG, "MemoryMapFileAppender::ctor::start");
            com.navdy.service.library.util.IOUtils.createDirectory(path);
            android.content.SharedPreferences prefs = context2.getSharedPreferences(this.currentPointerPrefFileName, 0);
            int currentPointer = prefs.getInt(CURRENT_POINTER_PREF, 0);
            if (currentPointer > 0 && ((long) currentPointer) < fileSize2) {
                prefs.edit().remove(CURRENT_POINTER_PREF).apply();
                android.util.Log.i(TAG, "MemoryMapFileAppender, pos set = " + currentPointer + " for " + this.fileName);
            }
            setMemoryMap(this.fileName + ".txt", currentPointer, fileSize2);
            android.util.Log.d(TAG, "MemoryMapFileAppender::ctor::end");
        } catch (Throwable t) {
            android.util.Log.e(TAG, "MemoryMapFileAppender.ctor()::", t);
        }
    }

    protected void append(java.lang.String type, java.lang.String tag, java.lang.String msg, java.lang.Throwable t) {
        try {
            synchronized (this) {
                this.builder.setLength(0);
                this.builder.append(this.dateFormat.format(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())));
                this.builder.append(SPACE);
                this.builder.append(type);
                this.builder.append("/");
                this.builder.append(tag);
                this.builder.append(COLON);
                this.builder.append(SPACE);
                if (msg != null) {
                    this.builder.append(msg);
                }
                if (t != null) {
                    this.builder.append("\r\n");
                    this.builder.append(getStackTraceString(t));
                }
                this.builder.append("\r\n");
                append(this.builder.toString());
            }
        } catch (java.lang.OutOfMemoryError e) {
            e.printStackTrace();
        }
    }

    protected void append(java.lang.String str) {
        try {
            if (this.memoryMap != null && str != null) {
                byte[] data = str.getBytes();
                int data_len = data.length;
                if (((long) (ROLLOVER_META_LEN + data_len)) < this.fileSize) {
                    if (((long) (this.memoryMap.position() + data_len)) >= this.fileSize) {
                        rollOver();
                    }
                    this.memoryMap.put(data);
                }
            }
        } catch (Throwable t) {
            android.util.Log.e(TAG, "MemoryMapFileAppender.append()::", t);
        }
    }

    private void setMemoryMap(java.lang.String outputPath, int position, long size) {
        java.io.RandomAccessFile dataFileHandle = null;
        java.nio.channels.FileChannel dataFileChannel = null;
        try {
            java.io.File file = new java.io.File(outputPath);
            if (position == 0 && file.exists()) {
                rollFiles();
            }
            java.io.RandomAccessFile dataFileHandle2 = new java.io.RandomAccessFile(outputPath, "rw");
            try {
                dataFileChannel = dataFileHandle2.getChannel();
                this.memoryMap = dataFileChannel.map(java.nio.channels.FileChannel.MapMode.READ_WRITE, 0, 1 + size);
                if (position > 0) {
                    this.memoryMap.position(position);
                }
                com.navdy.service.library.util.IOUtils.closeStream(dataFileHandle2);
                dataFileHandle = null;
                com.navdy.service.library.util.IOUtils.closeStream(dataFileChannel);
            } catch (Throwable th) {
                t = th;
                dataFileHandle = dataFileHandle2;
                com.navdy.service.library.util.IOUtils.closeStream(dataFileHandle);
                com.navdy.service.library.util.IOUtils.closeStream(dataFileChannel);
                this.memoryMap = null;
                android.util.Log.e(TAG, "setMemoryMap::", t);
            }
        } catch (Throwable th2) {
            t = th2;
            com.navdy.service.library.util.IOUtils.closeStream(dataFileHandle);
            com.navdy.service.library.util.IOUtils.closeStream(dataFileChannel);
            this.memoryMap = null;
            android.util.Log.e(TAG, "setMemoryMap::", t);
        }
    }

    private void rollFiles() {
        java.io.File src;
        if (this.maxFiles > 0) {
            com.navdy.service.library.util.IOUtils.deleteFile(this.context, this.fileName + "." + getFormatedNumber(0) + ".txt");
        }
        for (int i = 0; i < this.maxFiles; i++) {
            if (i == this.maxFiles - 1) {
                src = new java.io.File(this.fileName + ".txt");
            } else {
                src = new java.io.File(this.fileName + "." + getFormatedNumber(i + 1) + ".txt");
            }
            if (src.exists() && !src.renameTo(new java.io.File(this.fileName + "." + getFormatedNumber(i) + ".txt"))) {
                android.util.Log.w(TAG, "Unable to rename " + this.fileName + ".txt");
            }
        }
    }

    private java.lang.String getFormatedNumber(int i) {
        return java.lang.String.format("%0" + ((int) (java.lang.Math.log10((double) this.maxFiles) + 1.0d)) + "d", new java.lang.Object[]{java.lang.Integer.valueOf(i)});
    }

    private synchronized void rollOver() {
        if (this.maxFiles == 0) {
            this.memoryMap.position(0);
            this.memoryMap.put(ROLLOVER_MARKER);
        } else {
            this.memoryMap.force();
            rollFiles();
            setMemoryMap(this.fileName + ".txt", 0, this.fileSize);
        }
    }

    public void v(java.lang.String tag, java.lang.String msg) {
        append("V", tag, msg, null);
    }

    public void v(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        append("V", tag, msg, tr);
    }

    public void d(java.lang.String tag, java.lang.String msg) {
        append("D", tag, msg, null);
    }

    public void d(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        append("D", tag, msg, tr);
    }

    public void i(java.lang.String tag, java.lang.String msg) {
        append("I", tag, msg, null);
    }

    public void i(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        append("I", tag, msg, tr);
    }

    public void w(java.lang.String tag, java.lang.String msg) {
        append("W", tag, msg, null);
    }

    public void w(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        append("W", tag, msg, tr);
    }

    public void e(java.lang.String tag, java.lang.String msg) {
        append("W", tag, msg, null);
    }

    public void e(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        append("W", tag, msg, tr);
    }

    public void close() {
        synchronized (this) {
            android.util.Log.d(TAG, "MemoryMapFileAppender:closing");
            flush();
            this.memoryMap = null;
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:11:0x0020=Splitter:B:11:0x0020, B:7:0x0018=Splitter:B:7:0x0018} */
    private void truncateFileAtCurrentPosition(java.lang.String fileName2) {
        long position = (long) this.memoryMap.position();
        java.io.RandomAccessFile raf = null;
        try {
            java.io.RandomAccessFile raf2 = new java.io.RandomAccessFile(fileName2, "rws");
            try {
                raf2.setLength(position);
                com.navdy.service.library.util.IOUtils.closeStream(raf2);
                java.io.RandomAccessFile randomAccessFile = raf2;
            } catch (java.io.FileNotFoundException e) {
                e = e;
                raf = raf2;
                try {
                    e.printStackTrace();
                    com.navdy.service.library.util.IOUtils.closeStream(raf);
                } catch (Throwable th) {
                    th = th;
                    com.navdy.service.library.util.IOUtils.closeStream(raf);
                    throw th;
                }
            } catch (java.io.IOException e2) {
                e = e2;
                raf = raf2;
                e.printStackTrace();
                com.navdy.service.library.util.IOUtils.closeStream(raf);
            } catch (Throwable th2) {
                th = th2;
                raf = raf2;
                com.navdy.service.library.util.IOUtils.closeStream(raf);
                throw th;
            }
        } catch (java.io.FileNotFoundException e3) {
            e = e3;
            e.printStackTrace();
            com.navdy.service.library.util.IOUtils.closeStream(raf);
        } catch (java.io.IOException e4) {
            e = e4;
            e.printStackTrace();
            com.navdy.service.library.util.IOUtils.closeStream(raf);
        }
    }

    public void flush() {
        synchronized (this) {
            try {
                if (this.memoryMap != null) {
                    int curPos = this.memoryMap.position();
                    this.memoryMap.force();
                    if (curPos > 0) {
                        if (this.currentPointerPrefEditor == null) {
                            this.currentPointerPrefEditor = this.context.getSharedPreferences(this.currentPointerPrefFileName, 0).edit();
                        }
                        this.currentPointerPrefEditor.putInt(CURRENT_POINTER_PREF, curPos).commit();
                        android.util.Log.d(TAG, "MemoryMapFileAppender:stored pref pos:" + curPos);
                    }
                }
            } catch (Throwable t) {
                android.util.Log.d(TAG, "MemoryMapFileAppender:flush", t);
            }
        }
    }

    public static java.lang.String getStackTraceString(java.lang.Throwable tr) {
        if (tr == null) {
            return "";
        }
        for (java.lang.Throwable t = tr; t != null; t = t.getCause()) {
            if (t instanceof java.net.UnknownHostException) {
                return "";
            }
        }
        java.io.StringWriter sw = new java.io.StringWriter();
        java.io.PrintWriter pw = new com.navdy.service.library.log.FastPrintWriter((java.io.Writer) sw, false, 256);
        tr.printStackTrace(pw);
        pw.flush();
        return sw.toString();
    }

    @android.support.annotation.NonNull
    public java.util.ArrayList<java.io.File> getLogFiles() {
        java.util.ArrayList<java.io.File> files = new java.util.ArrayList<>(this.maxFiles);
        for (int i = 0; i < this.maxFiles; i++) {
            java.lang.String currentFile = this.fileName + "." + getFormatedNumber(i) + ".txt";
            if (i == this.maxFiles - 1) {
                try {
                    com.navdy.service.library.util.IOUtils.copyFile(this.fileName + ".txt", currentFile);
                    truncateFileAtCurrentPosition(currentFile);
                } catch (java.io.IOException e) {
                    e.printStackTrace();
                }
            }
            java.io.File file = new java.io.File(currentFile);
            if (file.exists() && file.length() > 0) {
                files.add(file);
            }
        }
        return files;
    }
}
