package com.navdy.service.library.log;

public final class Logger {
    public static final java.lang.String ACTION_RELOAD = "com.navdy.service.library.log.action.RELOAD";
    public static final boolean DEBUG = false;
    public static final java.lang.String DEFAULT_TAG = "Navdy";
    private static volatile long logLevelChange = -1;
    private static com.navdy.service.library.log.LogAppender[] sAppenders = new com.navdy.service.library.log.LogAppender[0];
    private volatile boolean[] loggable;
    private long startTime;
    private final java.lang.String tagName;
    private volatile long timestamp = -2;

    public static void reloadLogLevels() {
        logLevelChange = java.lang.System.currentTimeMillis();
    }

    public static void init(com.navdy.service.library.log.LogAppender[] appenders) {
        if (appenders == null || appenders.length == 0) {
            throw new java.lang.IllegalArgumentException();
        }
        sAppenders = (com.navdy.service.library.log.LogAppender[]) appenders.clone();
    }

    public static synchronized void addAppender(com.navdy.service.library.log.LogAppender appender) {
        synchronized (com.navdy.service.library.log.Logger.class) {
            if (appender == null) {
                throw new java.lang.IllegalArgumentException();
            }
            com.navdy.service.library.log.LogAppender[] appenders = new com.navdy.service.library.log.LogAppender[(sAppenders.length + 1)];
            for (int i = 0; i < appenders.length; i++) {
                if (i < appenders.length - 1) {
                    appenders[i] = sAppenders[i];
                } else {
                    appenders[i] = appender;
                }
            }
            sAppenders = appenders;
        }
    }

    public static void flush() {
        for (com.navdy.service.library.log.LogAppender flush : sAppenders) {
            flush.flush();
        }
    }

    public static void close() {
        for (com.navdy.service.library.log.LogAppender close : sAppenders) {
            close.close();
        }
    }

    public Logger(java.lang.String tagName2) {
        if (tagName2 != null) {
            this.tagName = tagName2.substring(0, java.lang.Math.min(tagName2.length(), 23));
        } else {
            this.tagName = "Navdy";
        }
    }

    public Logger(java.lang.Class clazz) {
        if (clazz != null) {
            java.lang.String name = clazz.getSimpleName();
            this.tagName = name.substring(0, java.lang.Math.min(name.length(), 23));
            return;
        }
        this.tagName = "Navdy";
    }

    public boolean isLoggable(int level) {
        return false;
    }

    public void v(java.lang.String msg) {
    }

    public void v(java.lang.String msg, java.lang.Throwable tr) {
    }

    public void d(java.lang.String msg) {
    }

    public void d(java.lang.String msg, java.lang.Throwable tr) {
    }

    public void i(java.lang.String msg) {
    }

    public void i(java.lang.String msg, java.lang.Throwable tr) {
    }

    public void w(java.lang.String msg) {
        for (com.navdy.service.library.log.LogAppender w : sAppenders) {
            w.w(this.tagName, msg);
        }
    }

    public void w(java.lang.String msg, java.lang.Throwable tr) {
        for (com.navdy.service.library.log.LogAppender w : sAppenders) {
            w.w(this.tagName, msg, tr);
        }
    }

    public void e(java.lang.String msg) {
        for (com.navdy.service.library.log.LogAppender e : sAppenders) {
            e.e(this.tagName, msg);
        }
    }

    public void e(java.lang.String msg, java.lang.Throwable tr) {
        for (com.navdy.service.library.log.LogAppender e : sAppenders) {
            e.e(this.tagName, msg, tr);
        }
    }

    public void e(java.lang.Throwable tr) {
        e("", tr);
    }

    public void w(java.lang.Throwable tr) {
        w("", tr);
    }

    public void d(java.lang.Throwable tr) {
        d("", tr);
    }

    public void v(java.lang.Throwable tr) {
        v("", tr);
    }

    public void i(java.lang.Throwable tr) {
        i("", tr);
    }

    public void recordStartTime() {
        this.startTime = java.lang.System.currentTimeMillis();
    }

    public void logTimeTaken(java.lang.String message) {
        d(message + " " + (java.lang.System.currentTimeMillis() - this.startTime) + " ms");
    }
}
