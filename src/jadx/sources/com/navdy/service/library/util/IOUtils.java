package com.navdy.service.library.util;

public final class IOUtils {
    private static final int BUFFER_SIZE = 16384;
    private static final int BUFFER_SIZE_FOR_DOWNLOADS = 1024;
    private static final java.lang.String CHECKSUM_FILE_NAME = ".checksum";
    private static final boolean CHECKSUM_UPDATE_MODE = false;
    private static final int DIGEST_BUFFER_SIZE = 1048576;
    private static final int END_OF_STREAM = -1;
    private static final int INPUT_BUFFER_SIZE = 16384;
    private static final java.lang.String TAG = com.navdy.service.library.util.IOUtils.class.getName();
    private static final java.lang.String TRASH_DIR_NAME = ".trash";
    public static final java.lang.String UTF_8 = "UTF-8";
    private static final java.util.concurrent.atomic.AtomicLong sCounter = new java.util.concurrent.atomic.AtomicLong(1);
    private static java.io.File sExternalTrashDir;
    private static final java.lang.Object sLock = new java.lang.Object();
    private static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.util.IOUtils.class);
    private static volatile java.io.File sTrashDir;

    static class Anon1 implements com.navdy.service.library.util.IOUtils.OnFileTraversal {
        final /* synthetic */ byte[] val$buffer;
        final /* synthetic */ java.security.MessageDigest val$mDigest;

        Anon1(byte[] bArr, java.security.MessageDigest messageDigest) {
            this.val$buffer = bArr;
            this.val$mDigest = messageDigest;
        }

        public void onFileTraversal(java.io.File file) {
            int bytesRead;
            java.io.FileInputStream fileInputStream = null;
            try {
                long length = file.length();
                java.io.FileInputStream fileInputStream2 = new java.io.FileInputStream(file);
                long totalRead = 0;
                do {
                    try {
                        bytesRead = fileInputStream2.read(this.val$buffer, 0, (int) java.lang.Math.min(length - totalRead, org.apache.commons.io.FileUtils.ONE_MB));
                        this.val$mDigest.update(this.val$buffer, 0, bytesRead);
                        totalRead += (long) bytesRead;
                        if (totalRead >= length) {
                            break;
                        }
                    } catch (Throwable th) {
                        th = th;
                        fileInputStream = fileInputStream2;
                        com.navdy.service.library.util.IOUtils.closeStream(fileInputStream);
                        throw th;
                    }
                } while (bytesRead != -1);
                com.navdy.service.library.util.IOUtils.closeStream(fileInputStream2);
                java.io.FileInputStream fileInputStream3 = fileInputStream2;
            } catch (Throwable th2) {
                t = th2;
                com.navdy.service.library.util.IOUtils.sLogger.e("Exception while calculating md5 checksum for a path ", t);
                com.navdy.service.library.util.IOUtils.closeStream(fileInputStream);
            }
        }
    }

    private interface OnFileTraversal {
        void onFileTraversal(java.io.File file);
    }

    public static void closeObject(java.io.Closeable obj) {
        if (obj != null) {
            try {
                obj.close();
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public static void closeStream(java.io.Closeable stream) {
        closeObject(stream);
    }

    public static void fileSync(java.io.FileOutputStream fileOutputStream) {
        if (fileOutputStream != null) {
            try {
                fileOutputStream.getFD().sync();
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public static boolean deleteFile(android.content.Context context, java.lang.String file) {
        java.io.File newFile;
        if (file == null) {
            return false;
        }
        java.io.File deleteFile = new java.io.File(file);
        if (!deleteFile.exists()) {
            return false;
        }
        if (isAppsInternalFile(context, file)) {
            newFile = new java.io.File(getNewTrashEntryPath(context));
        } else if (isAppsExternalFile(context, file)) {
            newFile = new java.io.File(getNewExternalTrashEntryPath(context));
        } else {
            newFile = new java.io.File(getTrashEntryPathInSameFolder(deleteFile));
        }
        sLogger.d("Deleted file: " + file + ":" + deleteFile.renameTo(newFile));
        return newFile.delete();
    }

    public static void deleteDirectory(android.content.Context context, java.io.File oldDir) {
        java.io.File newDir;
        if (oldDir != null && oldDir.exists()) {
            if (isAppsInternalFile(context, oldDir.getAbsolutePath())) {
                newDir = new java.io.File(getNewTrashEntryPath(context));
            } else if (isAppsExternalFile(context, oldDir.getAbsolutePath())) {
                newDir = new java.io.File(getNewExternalTrashEntryPath(context));
            } else {
                newDir = new java.io.File(getTrashEntryPathInSameFolder(oldDir));
            }
            boolean worked = oldDir.renameTo(newDir);
            if (!worked) {
                sLogger.e("Unable to rename dir " + oldDir + " to: " + newDir);
            }
            if (!worked) {
                newDir = oldDir;
            }
            try {
                deleteDirectoryInternal(context, newDir);
            } catch (java.io.IOException iex) {
                sLogger.e((java.lang.Throwable) iex);
            }
        }
    }

    public static boolean createDirectory(java.lang.String path) {
        return createDirectory(new java.io.File(path));
    }

    public static boolean createDirectory(java.io.File dir) {
        boolean worked = false;
        if (dir != null && !dir.exists()) {
            worked = dir.mkdirs();
        } else if (dir != null && dir.isDirectory()) {
            worked = true;
            sLogger.v("Directory already exists: " + dir.getPath());
        }
        if (!worked) {
            sLogger.e("Unable to create directory: " + (dir != null ? dir.getPath() : com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID));
        } else {
            sLogger.v("Successfully created directory: " + dir.getPath());
        }
        return worked;
    }

    private static boolean isAppsInternalFile(android.content.Context context, java.lang.String path) {
        return path.startsWith(context.getFilesDir().getAbsolutePath());
    }

    private static boolean isAppsExternalFile(android.content.Context context, java.lang.String path) {
        return path.startsWith(context.getExternalFilesDir(null).getAbsolutePath());
    }

    private static void deleteDirectoryInternal(android.content.Context context, java.io.File directory) throws java.io.IOException {
        if (directory.exists()) {
            cleanDirectory(context, directory);
            if (!directory.delete()) {
                throw new java.io.IOException("Unable to delete directory " + directory);
            }
        }
    }

    private static void cleanDirectory(android.content.Context context, java.io.File directory) throws java.io.IOException {
        if (directory.exists() && directory.isDirectory()) {
            java.io.File[] files = directory.listFiles();
            if (files != null) {
                java.io.IOException exception = null;
                for (java.io.File file : files) {
                    try {
                        forceDelete(context, file);
                    } catch (java.io.IOException ioe) {
                        exception = ioe;
                    }
                }
                if (exception != null) {
                    throw exception;
                }
            }
        }
    }

    private static void forceDelete(android.content.Context context, java.io.File file) throws java.io.IOException {
        if (file.isDirectory()) {
            deleteDirectory(context, file);
        } else if (!file.delete()) {
            sLogger.e("Unable to delete kernel crash file: " + file);
        }
    }

    private static java.lang.String getNewTrashEntryPath(android.content.Context context) {
        if (sTrashDir == null) {
            synchronized (sLock) {
                if (sTrashDir == null) {
                    sTrashDir = new java.io.File(context.getFilesDir().getAbsolutePath() + java.io.File.separator + TRASH_DIR_NAME);
                    createDirectory(sTrashDir);
                }
            }
        }
        return sTrashDir.getAbsolutePath() + java.io.File.separator + java.lang.System.currentTimeMillis() + "_" + sCounter.getAndIncrement();
    }

    public static java.lang.String getNewExternalTrashEntryPath(android.content.Context context) {
        java.lang.String str = null;
        if (context != null) {
            synchronized (sLock) {
                if (sExternalTrashDir == null) {
                    java.io.File externalFilesDir = context.getExternalFilesDir(null);
                    if (externalFilesDir != null) {
                        sExternalTrashDir = new java.io.File(externalFilesDir.getAbsolutePath() + java.io.File.separator + TRASH_DIR_NAME);
                        createDirectory(sExternalTrashDir);
                    }
                }
                str = sExternalTrashDir.getAbsolutePath() + java.io.File.separator + java.lang.System.currentTimeMillis() + "_" + sCounter.getAndIncrement();
            }
        }
        return str;
    }

    public static java.lang.String getTrashEntryPathInSameFolder(java.io.File file) {
        if (file == null) {
            return null;
        }
        return file.getParent() + java.io.File.separator + java.lang.System.currentTimeMillis() + "_" + sCounter.getAndIncrement();
    }

    public static java.lang.String convertInputStreamToString(java.io.InputStream inputStream, java.lang.String charSet) throws java.io.IOException {
        java.io.ByteArrayOutputStream byteArrayOutputStream = new java.io.ByteArrayOutputStream();
        byte[] buffer = new byte[16384];
        while (true) {
            int n = inputStream.read(buffer);
            if (n == -1) {
                return byteArrayOutputStream.toString(charSet);
            }
            byteArrayOutputStream.write(buffer, 0, n);
        }
    }

    public static java.lang.String convertFileToString(java.lang.String path) throws java.io.IOException {
        java.io.FileInputStream fileInputStream = new java.io.FileInputStream(path);
        try {
            return convertInputStreamToString(fileInputStream, "UTF-8");
        } finally {
            closeStream(fileInputStream);
        }
    }

    public static byte[] readInputStreamToByteArray(java.io.InputStream inputStream) throws java.io.IOException {
        java.io.ByteArrayOutputStream byteArrayOutputStream = new java.io.ByteArrayOutputStream();
        byte[] buffer = new byte[16384];
        while (true) {
            int n = inputStream.read(buffer);
            if (n == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(buffer, 0, n);
        }
    }

    public static byte[] readBinaryFile(java.lang.String path) throws java.io.IOException {
        java.io.File file = new java.io.File(path);
        byte[] buffer = new byte[((int) file.length())];
        java.io.FileInputStream ios = new java.io.FileInputStream(file);
        try {
            ios.read(buffer);
            return buffer;
        } finally {
            ios.close();
        }
    }

    public static void closeFD(int fd) {
        if (fd != -1) {
            try {
                android.os.ParcelFileDescriptor.adoptFd(fd).close();
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public static int getSocketFD(android.bluetooth.BluetoothSocket bluetoothSocket) {
        if (bluetoothSocket != null) {
            try {
                java.lang.reflect.Field field = android.bluetooth.BluetoothSocket.class.getDeclaredField("mSocket");
                field.setAccessible(true);
                android.net.LocalSocket localSocket = (android.net.LocalSocket) field.get(bluetoothSocket);
                if (localSocket != null) {
                    java.lang.reflect.Field field2 = android.net.LocalSocket.class.getDeclaredField("impl");
                    field2.setAccessible(true);
                    java.lang.Object localSocketImpl = field2.get(localSocket);
                    if (localSocketImpl != null) {
                        java.lang.reflect.Field field3 = localSocketImpl.getClass().getDeclaredField("fd");
                        field3.setAccessible(true);
                        java.io.FileDescriptor fileDescriptor = (java.io.FileDescriptor) field3.get(localSocketImpl);
                        if (fileDescriptor != null) {
                            java.lang.reflect.Field field4 = java.io.FileDescriptor.class.getDeclaredField("descriptor");
                            field4.setAccessible(true);
                            return ((java.lang.Integer) field4.get(fileDescriptor)).intValue();
                        }
                    }
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
        return -1;
    }

    public static boolean copyFile(java.lang.String srcPath, java.lang.String targetPath) throws java.io.IOException {
        java.io.FileInputStream fileInputStream = null;
        try {
            java.io.File file = new java.io.File(srcPath);
            if (file.exists()) {
                java.io.FileInputStream fileInputStream2 = new java.io.FileInputStream(file);
                try {
                    copyFile(targetPath, (java.io.InputStream) fileInputStream2);
                    closeStream(null);
                    return true;
                } catch (Throwable th) {
                    th = th;
                    fileInputStream = fileInputStream2;
                    closeStream(fileInputStream);
                    throw th;
                }
            } else {
                closeStream(null);
                return false;
            }
        } catch (Throwable th2) {
            th = th2;
            closeStream(fileInputStream);
            throw th;
        }
    }

    public static int copyFile(java.lang.String targetPath, java.io.InputStream srcStream) throws java.io.IOException {
        java.io.FileOutputStream targetFile = null;
        int bytesCopied = 0;
        try {
            java.io.FileOutputStream targetFile2 = new java.io.FileOutputStream(targetPath);
            try {
                byte[] data = new byte[16384];
                while (true) {
                    int count = srcStream.read(data);
                    if (count > 0) {
                        targetFile2.write(data, 0, count);
                        bytesCopied += count;
                    } else {
                        closeStream(srcStream);
                        fileSync(targetFile2);
                        closeStream(targetFile2);
                        return bytesCopied;
                    }
                }
            } catch (Throwable th) {
                th = th;
                targetFile = targetFile2;
                closeStream(srcStream);
                fileSync(targetFile);
                closeStream(targetFile);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            closeStream(srcStream);
            fileSync(targetFile);
            closeStream(targetFile);
            throw th;
        }
    }

    public static int copyFile(java.lang.String targetPath, byte[] data) throws java.io.IOException {
        return copyFile(targetPath, data, true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x001a  */
    public static int copyFile(java.lang.String targetPath, byte[] data, boolean sync) throws java.io.IOException {
        java.io.FileOutputStream targetFile = null;
        try {
            java.io.FileOutputStream targetFile2 = new java.io.FileOutputStream(targetPath);
            try {
                targetFile2.write(data, 0, data.length);
                int bytesCopied = 0 + data.length;
                if (sync) {
                    fileSync(targetFile2);
                }
                closeStream(targetFile2);
                return bytesCopied;
            } catch (Throwable th) {
                th = th;
                targetFile = targetFile2;
                if (sync) {
                    fileSync(targetFile);
                }
                closeStream(targetFile);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            if (sync) {
            }
            closeStream(targetFile);
            throw th;
        }
    }

    public static java.lang.String hashForKey(java.lang.String key) {
        if (key == null) {
            return null;
        }
        try {
            return hashForBytes(key.getBytes());
        } catch (java.security.NoSuchAlgorithmException e) {
            return java.lang.String.valueOf(key.hashCode());
        }
    }

    public static java.lang.String hashForBitmap(android.graphics.Bitmap bitmap) {
        java.lang.String str = null;
        if (bitmap == null) {
            return str;
        }
        try {
            return hashForBytes(bitmap2ByteBuffer(bitmap));
        } catch (java.security.NoSuchAlgorithmException e) {
            return str;
        }
    }

    public static java.lang.String hashForBytes(byte[] bytes) throws java.security.NoSuchAlgorithmException {
        if (bytes == null || bytes.length <= 0) {
            return null;
        }
        java.security.MessageDigest mDigest = java.security.MessageDigest.getInstance("MD5");
        if (mDigest == null) {
            return null;
        }
        mDigest.update(bytes);
        return bytesToHexString(mDigest.digest());
    }

    public static java.lang.String hashForFile(java.io.File file) {
        return hashForFile(file, file.length());
    }

    public static java.lang.String hashForFile(java.io.File file, long offset) {
        int bytesRead;
        sLogger.recordStartTime();
        java.io.FileInputStream fis = null;
        if (file == null || !file.exists() || !file.canRead() || offset > file.length()) {
            throw new java.lang.IllegalArgumentException();
        }
        try {
            java.security.MessageDigest mDigest = java.security.MessageDigest.getInstance("MD5");
            long totalRead = 0;
            byte[] buffer = new byte[1048576];
            java.io.FileInputStream fis2 = new java.io.FileInputStream(file);
            do {
                try {
                    bytesRead = fis2.read(buffer, 0, (int) java.lang.Math.min(offset - totalRead, (long) 1048576));
                    mDigest.update(buffer, 0, bytesRead);
                    totalRead += (long) bytesRead;
                    if (totalRead >= offset) {
                        break;
                    }
                } catch (Throwable th) {
                    th = th;
                    fis = fis2;
                    closeStream(fis);
                    throw th;
                }
            } while (bytesRead != -1);
            java.lang.String digest = bytesToHexString(mDigest.digest());
            sLogger.logTimeTaken("Time taken to calculate hash of the file of size :" + file.length());
            closeStream(fis2);
            java.io.FileInputStream fileInputStream = fis2;
            return digest;
        } catch (Throwable th2) {
            t = th2;
            sLogger.e("Exception while calculating md5 checksum for the file ", t);
            closeStream(fis);
            return null;
        }
    }

    public static java.lang.String hashForPath(java.lang.String path, boolean recursive, java.lang.String... excludeFileNames) {
        sLogger.recordStartTime();
        try {
            java.security.MessageDigest mDigest = java.security.MessageDigest.getInstance("MD5");
            traverseFiles(path, recursive, java.util.Arrays.asList(excludeFileNames), new com.navdy.service.library.util.IOUtils.Anon1(new byte[1048576], mDigest));
            java.lang.String digest = bytesToHexString(mDigest.digest());
            sLogger.logTimeTaken("Time taken to calculate hash of all files on path:" + path);
            return digest;
        } catch (Throwable t) {
            sLogger.e("Exception while calculating md5 checksum for a path ", t);
            return null;
        }
    }

    private static void traverseFiles(java.lang.String path, boolean recursive, java.util.List<java.lang.String> excludeFileNames, com.navdy.service.library.util.IOUtils.OnFileTraversal onFileTraversal) throws java.io.FileNotFoundException {
        java.io.File containingDirectory = new java.io.File(path);
        if (containingDirectory.isDirectory()) {
            java.io.File[] files = containingDirectory.listFiles();
            if (files != null) {
                java.util.Arrays.sort(files);
                for (java.io.File file : files) {
                    if (!file.isDirectory() && !excludeFileNames.contains(file.getName())) {
                        onFileTraversal.onFileTraversal(file);
                    } else if (recursive) {
                        traverseFiles(file.getPath(), true, excludeFileNames, onFileTraversal);
                    }
                }
            }
        }
    }

    public static void checkIntegrity(android.content.Context context, java.lang.String path, int md5ReferenceRes) {
        sLogger.i("integrity check for " + path + " starting");
        long l1 = android.os.SystemClock.elapsedRealtime();
        java.lang.String buildChecksum = null;
        java.lang.String cachedChecksum = null;
        java.io.InputStream buildChecksumInputStream = null;
        try {
            buildChecksumInputStream = context.getResources().openRawResource(md5ReferenceRes);
            buildChecksum = convertInputStreamToString(buildChecksumInputStream, "UTF-8");
            cachedChecksum = convertFileToString(path + java.io.File.separator + CHECKSUM_FILE_NAME);
        } catch (Throwable e) {
            sLogger.i("error while retrieving integrity checksum from filesystem, might not be present", e);
        } finally {
            closeStream(buildChecksumInputStream);
        }
        if ((buildChecksum == null || cachedChecksum == null || !buildChecksum.equals(cachedChecksum)) ? false : true) {
            sLogger.v("checksum for " + path + " is fine, no-op");
        } else {
            java.lang.String actualChecksum = hashForPath(path, true, CHECKSUM_FILE_NAME);
            if (!((actualChecksum == null || buildChecksum == null || !actualChecksum.equals(buildChecksum)) ? false : true)) {
                sLogger.w("files on " + path + " are out of date or corrupted, redoing");
                java.io.File file = new java.io.File(path);
                deleteDirectory(context, file);
                createDirectory(path);
            } else {
                sLogger.v("checksum for " + path + " is fine, writing integrity checksum on filesystem");
                java.io.PrintWriter printWriter = null;
                try {
                    java.io.PrintWriter printWriter2 = new java.io.PrintWriter(path + java.io.File.separator + CHECKSUM_FILE_NAME);
                    try {
                        printWriter2.print(actualChecksum);
                        closeStream(printWriter2);
                        java.io.PrintWriter printWriter3 = printWriter2;
                    } catch (Throwable th) {
                        th = th;
                        printWriter = printWriter2;
                        closeStream(printWriter);
                        throw th;
                    }
                } catch (Throwable th2) {
                    e = th2;
                    sLogger.e("could not write integrity checksum on filesystem", e);
                    closeStream(printWriter);
                    sLogger.i("integrity check for " + path + " took " + (android.os.SystemClock.elapsedRealtime() - l1) + " ms");
                }
            }
        }
        sLogger.i("integrity check for " + path + " took " + (android.os.SystemClock.elapsedRealtime() - l1) + " ms");
    }

    public static byte[] bitmap2ByteBuffer(android.graphics.Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        java.io.ByteArrayOutputStream stream = new java.io.ByteArrayOutputStream();
        bitmap.compress(android.graphics.Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    public static java.lang.String bytesToHexString(byte[] bytes, int offset, int length) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        for (int i = offset; i < length; i++) {
            java.lang.String hex = java.lang.Integer.toHexString(bytes[i] & com.here.posclient.WifiMeasurement.WLAN_BAND_UNKNOWN);
            if (hex.length() == 1) {
                sb.append(com.vividsolutions.jts.geom.Dimension.SYM_P);
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public static java.lang.String bytesToHexString(byte[] bytes) {
        return bytesToHexString(bytes, 0, bytes.length);
    }

    public static long getFreeSpace(java.lang.String directory) {
        java.io.File file = new java.io.File(directory);
        if (!file.exists() || !file.isDirectory()) {
            return -1;
        }
        android.os.StatFs statFs = new android.os.StatFs(directory);
        return ((long) statFs.getFreeBlocks()) * ((long) statFs.getBlockSize());
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0048  */
    public static org.json.JSONObject getJSONFromURL(java.lang.String url) {
        org.json.JSONObject jSONObject;
        java.net.HttpURLConnection urlConnection = null;
        java.io.InputStreamReader input = null;
        try {
            urlConnection = (java.net.HttpURLConnection) new java.net.URL(url).openConnection();
            java.io.InputStreamReader input2 = new java.io.InputStreamReader(urlConnection.getInputStream(), "UTF-8");
            try {
                jSONObject = new org.json.JSONObject(convertInputStreamToString(urlConnection.getInputStream(), "UTF-8"));
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                closeStream(input2);
                java.io.Closeable closeable = input2;
            } catch (Throwable th) {
                th = th;
                input = input2;
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                closeStream(input);
                throw th;
            }
        } catch (Throwable th2) {
            e = th2;
            sLogger.e("Exception while downloading/parsing json", e);
            jSONObject = null;
            if (urlConnection != null) {
            }
            closeStream(input);
            return jSONObject;
        }
        return jSONObject;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0059  */
    public static byte[] downloadImage(java.lang.String url) {
        byte[] bArr;
        java.io.ByteArrayOutputStream output;
        java.net.HttpURLConnection urlConnection = null;
        java.io.BufferedInputStream input = null;
        java.io.ByteArrayOutputStream output2 = null;
        try {
            urlConnection = (java.net.HttpURLConnection) new java.net.URL(url).openConnection();
            java.io.BufferedInputStream input2 = new java.io.BufferedInputStream(urlConnection.getInputStream());
            try {
                output = new java.io.ByteArrayOutputStream();
            } catch (Throwable th) {
                th = th;
                input = input2;
                if (urlConnection != null) {
                }
                closeStream(input);
                closeStream(output2);
                throw th;
            }
            try {
                byte[] buf = new byte[1024];
                while (true) {
                    int n = input2.read(buf);
                    if (n < 0) {
                        break;
                    }
                    output.write(buf, 0, n);
                }
                bArr = output.toByteArray();
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                closeStream(input2);
                closeStream(output);
                java.io.ByteArrayOutputStream byteArrayOutputStream = output;
                java.io.BufferedInputStream bufferedInputStream = input2;
            } catch (Throwable th2) {
                th = th2;
                output2 = output;
                input = input2;
                if (urlConnection != null) {
                }
                closeStream(input);
                closeStream(output2);
                throw th;
            }
        } catch (Throwable th3) {
            e = th3;
            sLogger.e("Exception while downloading binary resource", e);
            bArr = null;
            if (urlConnection != null) {
            }
            closeStream(input);
            closeStream(output2);
            return bArr;
        }
        return bArr;
    }

    public static void compressFilesToZip(android.content.Context context, java.io.File[] zipEntryFiles, java.lang.String zipFilePath) {
        java.util.zip.ZipOutputStream zos;
        java.io.FileInputStream fis;
        java.io.File file = new java.io.File(zipFilePath);
        if (file.exists()) {
            deleteFile(context, zipFilePath);
        }
        try {
            if (file.createNewFile() && zipEntryFiles != null) {
                java.io.FileOutputStream fos = null;
                java.io.FileInputStream fis2 = null;
                java.util.zip.ZipOutputStream zos2 = null;
                try {
                    byte[] buffer = new byte[16384];
                    java.io.FileOutputStream fos2 = new java.io.FileOutputStream(zipFilePath);
                    try {
                        zos = new java.util.zip.ZipOutputStream(fos2);
                    } catch (Throwable th) {
                        th = th;
                        fos = fos2;
                        closeStream(zos2);
                        closeStream(fis2);
                        closeStream(fos);
                        throw th;
                    }
                    try {
                        int length = zipEntryFiles.length;
                        int i = 0;
                        java.io.FileInputStream fileInputStream = null;
                        while (i < length) {
                            try {
                                java.io.File entryFile = zipEntryFiles[i];
                                if (!entryFile.exists()) {
                                    fis = fileInputStream;
                                } else if (!entryFile.canRead()) {
                                    fis = fileInputStream;
                                } else {
                                    fis2 = new java.io.FileInputStream(entryFile);
                                    zos.putNextEntry(new java.util.zip.ZipEntry(entryFile.getName()));
                                    while (true) {
                                        int length2 = fis2.read(buffer);
                                        if (length2 <= 0) {
                                            break;
                                        }
                                        zos.write(buffer, 0, length2);
                                    }
                                    zos.closeEntry();
                                    closeStream(fis2);
                                    fis = null;
                                }
                                i++;
                                fileInputStream = fis;
                            } catch (Throwable th2) {
                                th = th2;
                                zos2 = zos;
                                fis2 = fileInputStream;
                                fos = fos2;
                                closeStream(zos2);
                                closeStream(fis2);
                                closeStream(fos);
                                throw th;
                            }
                        }
                        closeStream(zos);
                        closeStream(fileInputStream);
                        closeStream(fos2);
                    } catch (Throwable th3) {
                        th = th3;
                        zos2 = zos;
                        fos = fos2;
                        closeStream(zos2);
                        closeStream(fis2);
                        closeStream(fos);
                        throw th;
                    }
                } catch (Throwable th4) {
                    t = th4;
                    sLogger.e("Error while compressing files ", t);
                    closeStream(zos2);
                    closeStream(fis2);
                    closeStream(fos);
                }
            }
        } catch (java.io.IOException e) {
            sLogger.e("IO Exception while creating new file");
        }
    }

    public static void deCompressZipToDirectory(android.content.Context context, java.io.File zipFile, java.io.File outputDir) throws java.io.IOException {
        byte[] buffer = new byte[1024];
        java.util.zip.ZipInputStream zis = new java.util.zip.ZipInputStream(new java.io.FileInputStream(zipFile));
        for (java.util.zip.ZipEntry ze = zis.getNextEntry(); ze != null; ze = zis.getNextEntry()) {
            java.io.File newFile = new java.io.File(outputDir, ze.getName());
            boolean mkdirs = new java.io.File(newFile.getParent()).mkdirs();
            java.io.FileOutputStream fos = new java.io.FileOutputStream(newFile);
            while (true) {
                int len = zis.read(buffer);
                if (len <= 0) {
                    break;
                }
                fos.write(buffer, 0, len);
            }
            fos.close();
        }
        zis.closeEntry();
        zis.close();
    }

    public static boolean isExternalStorageReadable() {
        return "mounted".equals(android.os.Environment.getExternalStorageState());
    }

    public static java.io.File getTempFile(@android.support.annotation.Nullable com.navdy.service.library.log.Logger logger, java.lang.String fileName) {
        java.io.File file;
        if (android.os.Environment.getExternalStorageState().equals("mounted")) {
            if (android.text.TextUtils.isEmpty(fileName)) {
                file = new java.io.File(android.os.Environment.getExternalStorageDirectory(), getTempFilename());
            } else {
                file = new java.io.File(android.os.Environment.getExternalStorageDirectory(), fileName);
            }
            try {
                if (!file.createNewFile()) {
                    return file;
                }
                if (logger != null) {
                    logger.v("Created " + file.getAbsolutePath());
                    return file;
                }
                android.util.Log.v(TAG, "Created " + file.getAbsolutePath());
                return file;
            } catch (java.io.IOException e) {
                if (logger != null) {
                    logger.e("FileUtils:: Unable to create file. ", e);
                    return file;
                }
                android.util.Log.e(TAG, "FileUtils:: Unable to create file. ", e);
                return file;
            }
        } else {
            if (logger != null) {
                logger.e("FileUtils:: External Storage not mounted!");
            } else {
                android.util.Log.e(TAG, "FileUtils:: External Storage not mounted!");
            }
            return null;
        }
    }

    public static java.lang.String getExternalStorageFolderPath() {
        if (android.os.Environment.getExternalStorageState().equals("mounted")) {
            java.io.File externalStorageDirectory = android.os.Environment.getExternalStorageDirectory();
            if (externalStorageDirectory != null && externalStorageDirectory.exists()) {
                return externalStorageDirectory.getPath();
            }
        }
        return null;
    }

    public static java.lang.String getTempFilename() {
        return new java.text.SimpleDateFormat(com.navdy.client.app.framework.util.SupportTicketService.NEW_FOLDER_FORMAT, java.util.Locale.US).format(new java.util.Date(java.lang.System.currentTimeMillis())) + ".jpg";
    }
}
