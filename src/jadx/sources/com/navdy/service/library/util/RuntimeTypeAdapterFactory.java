package com.navdy.service.library.util;

public final class RuntimeTypeAdapterFactory<T> implements com.google.gson.TypeAdapterFactory {
    private final java.util.Map<java.lang.String, java.lang.Class<?>> aliasToSubtype = new java.util.LinkedHashMap();
    private final java.lang.Class<?> baseType;
    private final java.util.Map<java.lang.String, java.lang.Class<?>> labelToSubtype = new java.util.LinkedHashMap();
    private final java.util.Map<java.lang.Class<?>, java.lang.String> subtypeToLabel = new java.util.LinkedHashMap();
    private final java.lang.String typeFieldName;

    class Anon1 extends com.google.gson.TypeAdapter<R> {
        final /* synthetic */ java.util.Map val$labelToDelegate;
        final /* synthetic */ java.util.Map val$subtypeToDelegate;

        Anon1(java.util.Map map, java.util.Map map2) {
            this.val$labelToDelegate = map;
            this.val$subtypeToDelegate = map2;
        }

        public R read(com.google.gson.stream.JsonReader in) throws java.io.IOException {
            com.google.gson.JsonElement jsonElement = com.google.gson.internal.Streams.parse(in);
            com.google.gson.JsonElement labelJsonElement = jsonElement.getAsJsonObject().remove(com.navdy.service.library.util.RuntimeTypeAdapterFactory.this.typeFieldName);
            if (labelJsonElement == null) {
                throw new com.google.gson.JsonParseException("cannot deserialize " + com.navdy.service.library.util.RuntimeTypeAdapterFactory.this.baseType + " because it does not define a field named " + com.navdy.service.library.util.RuntimeTypeAdapterFactory.this.typeFieldName);
            }
            java.lang.String label = labelJsonElement.getAsString();
            com.google.gson.TypeAdapter<R> delegate = (com.google.gson.TypeAdapter) this.val$labelToDelegate.get(label);
            if (delegate != null) {
                return delegate.fromJsonTree(jsonElement);
            }
            throw new com.google.gson.JsonParseException("cannot deserialize " + com.navdy.service.library.util.RuntimeTypeAdapterFactory.this.baseType + " subtype named " + label + "; did you forget to register a subtype?");
        }

        public void write(com.google.gson.stream.JsonWriter out, R value) throws java.io.IOException {
            java.lang.Class<?> srcType = value.getClass();
            java.lang.String label = (java.lang.String) com.navdy.service.library.util.RuntimeTypeAdapterFactory.this.subtypeToLabel.get(srcType);
            com.google.gson.TypeAdapter<R> delegate = (com.google.gson.TypeAdapter) this.val$subtypeToDelegate.get(srcType);
            if (delegate == null) {
                throw new com.google.gson.JsonParseException("cannot serialize " + srcType.getName() + "; did you forget to register a subtype?");
            }
            com.google.gson.JsonObject jsonObject = delegate.toJsonTree(value).getAsJsonObject();
            if (jsonObject.has(com.navdy.service.library.util.RuntimeTypeAdapterFactory.this.typeFieldName)) {
                throw new com.google.gson.JsonParseException("cannot serialize " + srcType.getName() + " because it already defines a field named " + com.navdy.service.library.util.RuntimeTypeAdapterFactory.this.typeFieldName);
            }
            com.google.gson.JsonObject clone = new com.google.gson.JsonObject();
            clone.add(com.navdy.service.library.util.RuntimeTypeAdapterFactory.this.typeFieldName, new com.google.gson.JsonPrimitive(label));
            for (java.util.Map.Entry<java.lang.String, com.google.gson.JsonElement> e : jsonObject.entrySet()) {
                clone.add((java.lang.String) e.getKey(), (com.google.gson.JsonElement) e.getValue());
            }
            com.google.gson.internal.Streams.write(clone, out);
        }
    }

    private RuntimeTypeAdapterFactory(java.lang.Class<?> baseType2, java.lang.String typeFieldName2) {
        if (typeFieldName2 == null || baseType2 == null) {
            throw new java.lang.NullPointerException();
        }
        this.baseType = baseType2;
        this.typeFieldName = typeFieldName2;
    }

    public static <T> com.navdy.service.library.util.RuntimeTypeAdapterFactory<T> of(java.lang.Class<T> baseType2, java.lang.String typeFieldName2) {
        return new com.navdy.service.library.util.RuntimeTypeAdapterFactory<>(baseType2, typeFieldName2);
    }

    public static <T> com.navdy.service.library.util.RuntimeTypeAdapterFactory<T> of(java.lang.Class<T> baseType2) {
        return new com.navdy.service.library.util.RuntimeTypeAdapterFactory<>(baseType2, "type");
    }

    public com.navdy.service.library.util.RuntimeTypeAdapterFactory<T> registerSubtype(java.lang.Class<? extends T> type, java.lang.String label) {
        if (type == null || label == null) {
            throw new java.lang.NullPointerException();
        } else if (this.subtypeToLabel.containsKey(type) || this.labelToSubtype.containsKey(label)) {
            throw new java.lang.IllegalArgumentException("types and labels must be unique");
        } else if (this.aliasToSubtype.containsKey(label)) {
            throw new java.lang.IllegalArgumentException("alias already exists for label");
        } else {
            this.labelToSubtype.put(label, type);
            this.subtypeToLabel.put(type, label);
            return this;
        }
    }

    public com.navdy.service.library.util.RuntimeTypeAdapterFactory<T> registerSubtypeAlias(java.lang.Class<? extends T> type, java.lang.String label) {
        if (type == null || label == null) {
            throw new java.lang.NullPointerException();
        } else if (this.labelToSubtype.containsKey(label) || this.aliasToSubtype.containsKey(label)) {
            throw new java.lang.IllegalArgumentException("labels must be unique");
        } else if (!this.subtypeToLabel.containsKey(type)) {
            throw new java.lang.IllegalArgumentException("subtype must already have a mapping to add an alias");
        } else {
            this.aliasToSubtype.put(label, type);
            return this;
        }
    }

    public com.navdy.service.library.util.RuntimeTypeAdapterFactory<T> registerSubtype(java.lang.Class<? extends T> type) {
        return registerSubtype(type, type.getSimpleName());
    }

    public <R> com.google.gson.TypeAdapter<R> create(com.google.gson.Gson gson, com.google.gson.reflect.TypeToken<R> type) {
        if (type.getRawType() != this.baseType) {
            return null;
        }
        java.util.Map<java.lang.String, com.google.gson.TypeAdapter<?>> labelToDelegate = new java.util.LinkedHashMap<>();
        java.util.Map<java.lang.Class<?>, com.google.gson.TypeAdapter<?>> subtypeToDelegate = new java.util.LinkedHashMap<>();
        for (java.util.Map.Entry<java.lang.String, java.lang.Class<?>> entry : this.labelToSubtype.entrySet()) {
            com.google.gson.TypeAdapter<?> delegate = gson.getDelegateAdapter(this, com.google.gson.reflect.TypeToken.get((java.lang.Class) entry.getValue()));
            labelToDelegate.put(entry.getKey(), delegate);
            subtypeToDelegate.put(entry.getValue(), delegate);
        }
        for (java.util.Map.Entry<java.lang.String, java.lang.Class<?>> entry2 : this.aliasToSubtype.entrySet()) {
            labelToDelegate.put(entry2.getKey(), gson.getDelegateAdapter(this, com.google.gson.reflect.TypeToken.get((java.lang.Class) entry2.getValue())));
        }
        return new com.navdy.service.library.util.RuntimeTypeAdapterFactory.Anon1(labelToDelegate, subtypeToDelegate);
    }
}
