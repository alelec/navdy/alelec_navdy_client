package com.navdy.service.library.util;

public class SystemUtils {
    private static final java.lang.String COMMA = ",";
    private static final java.lang.String PERCENTAGE = "%";
    private static final java.lang.String SPACE = " ";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.util.SystemUtils.class);

    public static class CpuInfo {
        private java.util.ArrayList<com.navdy.service.library.util.SystemUtils.ProcessCpuInfo> plist;
        private int system;
        private int usr;

        public CpuInfo(int usr2, int system2, java.util.ArrayList<com.navdy.service.library.util.SystemUtils.ProcessCpuInfo> plist2) {
            this.usr = usr2;
            this.system = system2;
            this.plist = plist2;
        }

        public int getCpuUser() {
            return this.usr;
        }

        public int getCpuSystem() {
            return this.system;
        }

        public java.util.ArrayList<com.navdy.service.library.util.SystemUtils.ProcessCpuInfo> getList() {
            return this.plist;
        }
    }

    public static class ProcessCpuInfo {
        private int cpu;
        private java.lang.String name;
        private int pid;
        private java.lang.String thread;
        private int tid;

        public ProcessCpuInfo(int pid2, int tid2, java.lang.String name2, java.lang.String thread2, int cpu2) {
            this.pid = pid2;
            this.tid = tid2;
            this.name = name2;
            this.thread = thread2;
            this.cpu = cpu2;
        }

        public int getPid() {
            return this.pid;
        }

        public int getTid() {
            return this.tid;
        }

        public java.lang.String getProcessName() {
            return this.name;
        }

        public java.lang.String getThreadName() {
            return this.thread;
        }

        public int getCpu() {
            return this.cpu;
        }
    }

    public static java.lang.String getProcessName(android.content.Context context, int pid) {
        java.util.List<android.app.ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((android.app.ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (android.app.ActivityManager.RunningAppProcessInfo processInfo : runningAppProcesses) {
                if (processInfo != null && processInfo.pid == pid) {
                    return processInfo.processName;
                }
            }
        }
        return "";
    }

    public static int getProcessId(android.content.Context context, java.lang.String processName) {
        for (android.app.ActivityManager.RunningAppProcessInfo processInfo : ((android.app.ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
            if (android.text.TextUtils.equals(processInfo.processName, processName)) {
                return processInfo.pid;
            }
        }
        return -1;
    }

    public static int getNativeProcessId(java.lang.String processName) {
        java.lang.String line;
        java.io.InputStream inputStream = null;
        try {
            java.lang.Process process = java.lang.Runtime.getRuntime().exec("ps");
            process.waitFor();
            inputStream = process.getInputStream();
            java.io.BufferedReader reader = new java.io.BufferedReader(new java.io.StringReader(com.navdy.service.library.util.IOUtils.convertInputStreamToString(inputStream, "UTF-8")));
            do {
                line = reader.readLine();
                if (line == null) {
                    com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                    return -1;
                }
            } while (!line.contains(processName));
            java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(line);
            tokenizer.nextElement();
            return java.lang.Integer.parseInt(((java.lang.String) tokenizer.nextElement()).trim());
        } catch (Throwable th) {
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(inputStream);
        }
    }

    public static boolean isConnectedToNetwork(android.content.Context context) {
        android.net.NetworkInfo activeNetwork = ((android.net.ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static boolean isConnectedToWifi(android.content.Context context) {
        android.net.NetworkInfo wifiNetworkInfo = ((android.net.ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1);
        if (wifiNetworkInfo == null || !wifiNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public static boolean isConnectedToCellNetwork(android.content.Context context) {
        android.net.NetworkInfo wifiNetworkInfo = ((android.net.ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0);
        if (wifiNetworkInfo == null || !wifiNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00dd, code lost:
        com.navdy.service.library.util.IOUtils.closeStream(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00fc, code lost:
        r24 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00fd, code lost:
        com.navdy.service.library.util.IOUtils.closeStream(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0100, code lost:
        throw r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00fc A[ExcHandler: all (r24v0 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r13 
  PHI: (r13v1 'inputStream' java.io.InputStream) = (r13v0 'inputStream' java.io.InputStream), (r13v3 'inputStream' java.io.InputStream), (r13v3 'inputStream' java.io.InputStream), (r13v3 'inputStream' java.io.InputStream), (r13v3 'inputStream' java.io.InputStream), (r13v3 'inputStream' java.io.InputStream), (r13v3 'inputStream' java.io.InputStream), (r13v3 'inputStream' java.io.InputStream), (r13v3 'inputStream' java.io.InputStream) binds: [B:1:0x0001, B:37:0x00e3, B:38:?, B:33:0x00cd, B:34:?, B:28:0x00c2, B:29:?, B:26:0x00bd, B:27:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:1:0x0001] */
    public static com.navdy.service.library.util.SystemUtils.CpuInfo getCpuUsage() {
        java.io.InputStream inputStream = null;
        try {
            java.lang.Process process = java.lang.Runtime.getRuntime().exec("top -m 5 -t -n 1");
            process.waitFor();
            inputStream = process.getInputStream();
            java.io.StringReader stringReader = new java.io.StringReader(com.navdy.service.library.util.IOUtils.convertInputStreamToString(inputStream, "UTF-8"));
            java.io.BufferedReader bufferedReader = new java.io.BufferedReader(stringReader);
            int lineCount = 1;
            java.lang.String usr = null;
            java.lang.String system = null;
            java.util.ArrayList<com.navdy.service.library.util.SystemUtils.ProcessCpuInfo> plist = new java.util.ArrayList<>();
            while (true) {
                java.lang.String line = bufferedReader.readLine();
                if (line == null) {
                    com.navdy.service.library.util.SystemUtils.CpuInfo cpuInfo = new com.navdy.service.library.util.SystemUtils.CpuInfo(extractNumberFromPercent(usr), extractNumberFromPercent(system), plist);
                    com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                    return cpuInfo;
                } else if (line.trim().length() != 0) {
                    if (lineCount == 1) {
                        java.util.StringTokenizer stringTokenizer = new java.util.StringTokenizer(line, ",");
                        while (true) {
                            if (stringTokenizer.hasMoreElements()) {
                                java.lang.String element = ((java.lang.String) stringTokenizer.nextElement()).trim();
                                int index = element.indexOf(SPACE);
                                if (index >= 0) {
                                    if (usr == null) {
                                        usr = element.substring(index + 1).trim();
                                    } else {
                                        system = element.substring(index + 1).trim();
                                    }
                                }
                            }
                        }
                    } else if (lineCount >= 4) {
                        java.lang.String cpu = null;
                        java.lang.String thread = null;
                        java.lang.String pname = null;
                        java.util.StringTokenizer stringTokenizer2 = new java.util.StringTokenizer(line, SPACE);
                        int item = 1;
                        int pid = 0;
                        int tid = 0;
                        while (stringTokenizer2.hasMoreElements()) {
                            java.lang.String element2 = (java.lang.String) stringTokenizer2.nextElement();
                            switch (item) {
                                case 1:
                                    pid = java.lang.Integer.parseInt(element2);
                                    break;
                                case 2:
                                    tid = java.lang.Integer.parseInt(element2);
                                    break;
                                case 4:
                                    cpu = element2;
                                    break;
                                case 10:
                                    thread = element2;
                                    break;
                                case 11:
                                    pname = element2;
                                    break;
                            }
                            item++;
                        }
                        plist.add(new com.navdy.service.library.util.SystemUtils.ProcessCpuInfo(pid, tid, pname, thread, extractNumberFromPercent(cpu)));
                    }
                    lineCount++;
                }
            }
        } catch (Throwable th) {
        }
    }

    private static int extractNumberFromPercent(java.lang.String s) {
        if (s == null) {
            return 0;
        }
        try {
            java.lang.String s2 = s.trim();
            int index = s2.indexOf(PERCENTAGE);
            if (index != -1) {
                s2 = s2.substring(0, index).trim();
            }
            return java.lang.Integer.parseInt(s2);
        } catch (Throwable t) {
            sLogger.e(t);
            return 0;
        }
    }
}
