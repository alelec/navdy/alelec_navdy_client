package com.navdy.service.library.device.link;

public class EventRequest {
    public final com.navdy.service.library.device.RemoteDevice.PostEventHandler eventCompleteHandler;
    public final byte[] eventData;
    private final android.os.Handler handler;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.device.RemoteDevice.PostEventStatus val$status;

        Anon1(com.navdy.service.library.device.RemoteDevice.PostEventStatus postEventStatus) {
            this.val$status = postEventStatus;
        }

        public void run() {
            if (com.navdy.service.library.device.link.EventRequest.this.eventCompleteHandler != null) {
                com.navdy.service.library.device.link.EventRequest.this.eventCompleteHandler.onComplete(this.val$status);
            }
        }
    }

    public EventRequest(android.os.Handler handler2, byte[] eventData2, com.navdy.service.library.device.RemoteDevice.PostEventHandler eventCompleteHandler2) {
        this.eventData = (byte[]) eventData2.clone();
        this.eventCompleteHandler = eventCompleteHandler2;
        this.handler = handler2;
    }

    public void callCompletionHandlers(com.navdy.service.library.device.RemoteDevice.PostEventStatus status) {
        this.handler.post(new com.navdy.service.library.device.link.EventRequest.Anon1(status));
    }
}
