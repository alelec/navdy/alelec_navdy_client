package com.navdy.service.library.device.link;

public class LinkManager {
    private static com.navdy.service.library.device.link.LinkManager.LinkFactory sDefaultFactory = new com.navdy.service.library.device.link.LinkManager.Anon1();
    private static java.util.HashMap<com.navdy.service.library.device.connection.ConnectionType, com.navdy.service.library.device.link.LinkManager.LinkFactory> sFactoryMap = new java.util.HashMap<>();

    static class Anon1 implements com.navdy.service.library.device.link.LinkManager.LinkFactory {
        Anon1() {
        }

        public com.navdy.service.library.device.link.Link build(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
            switch (com.navdy.service.library.device.link.LinkManager.Anon2.$SwitchMap$com$navdy$service$library$device$connection$ConnectionType[connectionInfo.getType().ordinal()]) {
                case 1:
                case 2:
                    return new com.navdy.service.library.device.link.ProtobufLink(connectionInfo);
                default:
                    return null;
            }
        }
    }

    static /* synthetic */ class Anon2 {
        static final /* synthetic */ int[] $SwitchMap$com$navdy$service$library$device$connection$ConnectionType = new int[com.navdy.service.library.device.connection.ConnectionType.values().length];

        static {
            try {
                $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
        }
    }

    public interface LinkFactory {
        com.navdy.service.library.device.link.Link build(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo);
    }

    static {
        registerFactory(com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF, sDefaultFactory);
        registerFactory(com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF, sDefaultFactory);
    }

    public static void registerFactory(com.navdy.service.library.device.connection.ConnectionType type, com.navdy.service.library.device.link.LinkManager.LinkFactory factory) {
        if (factory != null) {
            sFactoryMap.put(type, factory);
        }
    }

    public static com.navdy.service.library.device.link.Link build(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
        com.navdy.service.library.device.link.LinkManager.LinkFactory factory = (com.navdy.service.library.device.link.LinkManager.LinkFactory) sFactoryMap.get(connectionInfo.getType());
        if (factory != null) {
            return factory.build(connectionInfo);
        }
        return null;
    }
}
