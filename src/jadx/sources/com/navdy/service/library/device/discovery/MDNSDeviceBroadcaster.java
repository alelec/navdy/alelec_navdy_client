package com.navdy.service.library.device.discovery;

public abstract class MDNSDeviceBroadcaster implements com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster {
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.class);
    protected android.net.nsd.NsdManager mNsdManager;
    protected android.net.nsd.NsdManager.RegistrationListener mRegistrationListener;
    protected android.net.nsd.NsdServiceInfo mServiceInfo;
    protected boolean registered;

    class Anon1 implements android.net.nsd.NsdManager.RegistrationListener {
        Anon1() {
        }

        public void onServiceRegistered(android.net.nsd.NsdServiceInfo nsdServiceInfo) {
            com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.sLogger.d("SERVICE REGISTERED: " + com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.this.mServiceInfo.getServiceType());
        }

        public void onRegistrationFailed(android.net.nsd.NsdServiceInfo nsdServiceInfo, int errorCode) {
            com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.sLogger.d("SERVICE REGISTRATION FAILED" + com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.this.mServiceInfo.getServiceType());
        }

        public void onServiceUnregistered(android.net.nsd.NsdServiceInfo nsdServiceInfo) {
            com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.sLogger.d("SERVICE UNREGISTERED" + com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.this.mServiceInfo.getServiceType());
        }

        public void onUnregistrationFailed(android.net.nsd.NsdServiceInfo nsdServiceInfo, int errorCode) {
            com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.sLogger.d("SERVICE UNREGISTRATION FAILED: " + com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.this.mServiceInfo.getServiceType());
        }
    }

    class Anon2 extends java.util.TimerTask {
        final /* synthetic */ android.net.nsd.NsdManager.RegistrationListener val$registrationListener;
        final /* synthetic */ android.net.nsd.NsdServiceInfo val$serviceInfo;

        Anon2(android.net.nsd.NsdServiceInfo nsdServiceInfo, android.net.nsd.NsdManager.RegistrationListener registrationListener) {
            this.val$serviceInfo = nsdServiceInfo;
            this.val$registrationListener = registrationListener;
        }

        public void run() {
            com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.sLogger.e("Register timer fired: " + this.val$serviceInfo.getServiceType());
            com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.this.mNsdManager.registerService(this.val$serviceInfo, 1, this.val$registrationListener);
            com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.this.registered = true;
            com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.sLogger.e("Registered: " + this.val$serviceInfo.getServiceType());
        }
    }

    protected MDNSDeviceBroadcaster() {
    }

    public boolean start() {
        if (this.mRegistrationListener != null) {
            sLogger.e("Already started: " + this.mServiceInfo.getServiceType());
            return false;
        }
        this.mRegistrationListener = getRegistrationListener();
        return registerRecord(this.mServiceInfo, this.mRegistrationListener);
    }

    public boolean stop() {
        if (this.mRegistrationListener == null) {
            sLogger.e("Already stopped: " + this.mServiceInfo.getServiceType());
            return false;
        }
        sLogger.e("Unregistering: " + this.mServiceInfo.getServiceType());
        return unregisterRecord(this.mRegistrationListener);
    }

    public MDNSDeviceBroadcaster(android.content.Context context, android.net.nsd.NsdServiceInfo serviceInfo) {
        this.mNsdManager = (android.net.nsd.NsdManager) context.getSystemService("servicediscovery");
        if (serviceInfo == null) {
            throw new java.lang.IllegalArgumentException("Service info required.");
        }
        this.mServiceInfo = serviceInfo;
    }

    protected android.net.nsd.NsdManager.RegistrationListener getRegistrationListener() {
        return new com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.Anon1();
    }

    protected boolean registerRecord(android.net.nsd.NsdServiceInfo serviceInfo, android.net.nsd.NsdManager.RegistrationListener registrationListener) {
        sLogger.e("Registering: " + serviceInfo.getServiceType());
        new java.util.Timer().schedule(new com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.Anon2(serviceInfo, registrationListener), 2000);
        return true;
    }

    protected boolean unregisterRecord(android.net.nsd.NsdManager.RegistrationListener registrationListener) {
        if (this.registered) {
            this.registered = false;
            this.mNsdManager.unregisterService(registrationListener);
        }
        return true;
    }
}
