package com.navdy.service.library.device.discovery;

public class TCPRemoteDeviceScanner extends com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner {
    public static final java.lang.String TAG = "TCPRemoteDeviceScanner";

    public TCPRemoteDeviceScanner(android.content.Context context) {
        super(context, com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF.getServiceType());
    }
}
