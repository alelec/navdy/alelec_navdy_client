package com.navdy.service.library.device.discovery;

public abstract class RemoteDeviceScanner extends com.navdy.service.library.util.Listenable<com.navdy.service.library.device.discovery.RemoteDeviceScanner.Listener> {
    protected android.content.Context mContext;

    class Anon1 implements com.navdy.service.library.device.discovery.RemoteDeviceScanner.ScanEventDispatcher {
        Anon1() {
        }

        public void dispatchEvent(com.navdy.service.library.device.discovery.RemoteDeviceScanner source, com.navdy.service.library.device.discovery.RemoteDeviceScanner.Listener listener) {
            listener.onScanStopped(source);
        }
    }

    class Anon2 implements com.navdy.service.library.device.discovery.RemoteDeviceScanner.ScanEventDispatcher {
        Anon2() {
        }

        public void dispatchEvent(com.navdy.service.library.device.discovery.RemoteDeviceScanner source, com.navdy.service.library.device.discovery.RemoteDeviceScanner.Listener listener) {
            listener.onScanStarted(source);
        }
    }

    class Anon3 implements com.navdy.service.library.device.discovery.RemoteDeviceScanner.ScanEventDispatcher {
        final /* synthetic */ java.util.List val$devices;

        Anon3(java.util.List list) {
            this.val$devices = list;
        }

        public void dispatchEvent(com.navdy.service.library.device.discovery.RemoteDeviceScanner source, com.navdy.service.library.device.discovery.RemoteDeviceScanner.Listener listener) {
            listener.onDiscovered(source, this.val$devices);
        }
    }

    class Anon4 implements com.navdy.service.library.device.discovery.RemoteDeviceScanner.ScanEventDispatcher {
        final /* synthetic */ java.util.List val$devices;

        Anon4(java.util.List list) {
            this.val$devices = list;
        }

        public void dispatchEvent(com.navdy.service.library.device.discovery.RemoteDeviceScanner source, com.navdy.service.library.device.discovery.RemoteDeviceScanner.Listener listener) {
            listener.onLost(source, this.val$devices);
        }
    }

    public interface Listener extends com.navdy.service.library.util.Listenable.Listener {
        void onDiscovered(com.navdy.service.library.device.discovery.RemoteDeviceScanner remoteDeviceScanner, java.util.List<com.navdy.service.library.device.connection.ConnectionInfo> list);

        void onLost(com.navdy.service.library.device.discovery.RemoteDeviceScanner remoteDeviceScanner, java.util.List<com.navdy.service.library.device.connection.ConnectionInfo> list);

        void onScanStarted(com.navdy.service.library.device.discovery.RemoteDeviceScanner remoteDeviceScanner);

        void onScanStopped(com.navdy.service.library.device.discovery.RemoteDeviceScanner remoteDeviceScanner);
    }

    protected interface ScanEventDispatcher extends com.navdy.service.library.util.Listenable.EventDispatcher<com.navdy.service.library.device.discovery.RemoteDeviceScanner, com.navdy.service.library.device.discovery.RemoteDeviceScanner.Listener> {
    }

    public abstract boolean startScan();

    public abstract boolean stopScan();

    RemoteDeviceScanner(android.content.Context context) {
        this.mContext = context;
    }

    protected void dispatchOnScanStopped() {
        dispatchToListeners(new com.navdy.service.library.device.discovery.RemoteDeviceScanner.Anon1());
    }

    protected void dispatchOnScanStarted() {
        dispatchToListeners(new com.navdy.service.library.device.discovery.RemoteDeviceScanner.Anon2());
    }

    protected void dispatchOnDiscovered(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
        dispatchOnDiscovered(java.util.Arrays.asList(new com.navdy.service.library.device.connection.ConnectionInfo[]{connectionInfo}));
    }

    protected void dispatchOnDiscovered(java.util.List<com.navdy.service.library.device.connection.ConnectionInfo> devices) {
        dispatchToListeners(new com.navdy.service.library.device.discovery.RemoteDeviceScanner.Anon3(devices));
    }

    protected void dispatchOnLost(java.util.List<com.navdy.service.library.device.connection.ConnectionInfo> devices) {
        dispatchToListeners(new com.navdy.service.library.device.discovery.RemoteDeviceScanner.Anon4(devices));
    }
}
