package com.navdy.service.library.device.link;

public interface Link extends java.io.Closeable {
    public static final int BANDWIDTH_LEVEL_LOW = 0;
    public static final int BANDWIDTH_LEVEL_NORMAL = 1;

    void close();

    int getBandWidthLevel();

    void setBandwidthLevel(int i);

    boolean start(com.navdy.service.library.network.SocketAdapter socketAdapter, java.util.concurrent.LinkedBlockingDeque<com.navdy.service.library.device.link.EventRequest> linkedBlockingDeque, com.navdy.service.library.device.link.LinkListener linkListener);
}
