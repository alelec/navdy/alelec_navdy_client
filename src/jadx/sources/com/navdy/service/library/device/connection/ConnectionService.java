package com.navdy.service.library.device.connection;

public abstract class ConnectionService extends android.app.Service implements com.navdy.service.library.device.connection.ConnectionListener.Listener, com.navdy.service.library.device.RemoteDevice.Listener {
    public static final java.util.UUID ACCESSORY_IAP2 = java.util.UUID.fromString("00000000-deca-fade-deca-deafdecacaff");
    public static final java.lang.String ACTION_LINK_BANDWIDTH_LEVEL_CHANGED = "LINK_BANDWIDTH_LEVEL_CHANGED";
    public static final java.lang.String CATEGORY_NAVDY_LINK = "NAVDY_LINK";
    private static final int CONNECT_TIMEOUT = 10000;
    private static final int DEAD_CONNECTION_TIME = 60000;
    private static final com.navdy.service.library.events.connection.ConnectionStatus DEVICES_CHANGED_EVENT = new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_PAIRED_DEVICES_CHANGED, (java.lang.String) null);
    public static final java.util.UUID DEVICE_IAP2 = java.util.UUID.fromString("00000000-deca-fade-deca-deafdecacafe");
    private static final int DISCONNECT_TIMEOUT = 2000;
    private static final int EVENT_DISCONNECT = 3;
    protected static final int EVENT_HEARTBEAT = 2;
    private static final int EVENT_RESTART_LISTENERS = 4;
    private static final int EVENT_STATE_CHANGE = 1;
    public static final java.lang.String EXTRA_BANDWIDTH_LEVEL = "EXTRA_BANDWIDTH_MODE";
    private static final int HEARTBEAT_INTERVAL = 4000;
    private static final int IDLE_TIMEOUT = 1000;
    public static final java.lang.String ILLEGAL_ARGUMENT = "ILLEGAL_ARGUMENT";
    public static final java.lang.String NAVDY_IAP_NAME = "Navdy iAP";
    public static final java.lang.String NAVDY_PROTO_SERVICE_NAME = "Navdy";
    public static final java.util.UUID NAVDY_PROTO_SERVICE_UUID = java.util.UUID.fromString("1992B7D7-C9B9-4F4F-AA7F-9FE33D95851E");
    public static final java.util.UUID NAVDY_PROXY_TUNNEL_UUID = java.util.UUID.fromString("D72BC85F-F015-4F24-A72F-35924E10888F");
    private static final int PAIRING_TIMEOUT = 30000;
    public static final java.lang.String REASON_DEAD_CONNECTION = "DEAD_CONNECTION";
    private static final int RECONNECT_DELAY = 2000;
    private static final int RECONNECT_TIMEOUT = 10000;
    protected static final int RETRY_CONNECTING_AFTER_FAILURE_TIMEOUT = 30000;
    private static final int SLEEP_TIMEOUT = 60000;
    private android.content.BroadcastReceiver bluetoothReceiver = new com.navdy.service.library.device.connection.ConnectionService.Anon3();
    private boolean broadcasting = false;
    protected final java.lang.Object connectionLock = new java.lang.Object();
    protected com.navdy.service.library.device.RemoteDeviceRegistry deviceRegistry;
    private boolean forceReconnect = false;
    protected boolean inProcess = true;
    private long lastMessageReceivedTime;
    protected final java.lang.Object listenerLock = new java.lang.Object();
    private com.navdy.hud.app.IEventListener[] listenersArray = new com.navdy.hud.app.IEventListener[0];
    private boolean listening = false;
    protected final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());
    protected com.navdy.service.library.device.connection.ConnectionListener[] mConnectionListeners;
    private com.navdy.hud.app.IEventSource.Stub mEventSource = new com.navdy.service.library.device.connection.ConnectionService.Anon1();
    private final java.util.List<com.navdy.hud.app.IEventListener> mListeners = new java.util.ArrayList();
    protected com.navdy.service.library.device.RemoteDevice mRemoteDevice;
    protected com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster[] mRemoteDeviceBroadcasters;
    protected com.squareup.wire.Wire mWire;
    private com.navdy.service.library.device.connection.ConnectionService.PendingConnectHandler pendingConnectHandler;
    private com.navdy.service.library.device.connection.ProxyService proxyService;
    private volatile boolean quitting;
    protected java.lang.Runnable reconnectRunnable = new com.navdy.service.library.device.connection.ConnectionService.Anon2();
    protected boolean serverMode = false;
    protected volatile com.navdy.service.library.device.connection.ConnectionService.ServiceHandler serviceHandler;
    private volatile android.os.Looper serviceLooper;
    protected com.navdy.service.library.device.connection.ConnectionService.State state = com.navdy.service.library.device.connection.ConnectionService.State.START;
    private long stateAge = 0;

    class Anon1 extends com.navdy.hud.app.IEventSource.Stub {
        Anon1() {
        }

        public void addEventListener(com.navdy.hud.app.IEventListener listener) throws android.os.RemoteException {
            synchronized (com.navdy.service.library.device.connection.ConnectionService.this.listenerLock) {
                com.navdy.service.library.device.connection.ConnectionService.this.mListeners.add(listener);
                com.navdy.service.library.device.connection.ConnectionService.this.createListenerList();
            }
            com.navdy.service.library.device.connection.ConnectionService.this.logger.v("listener added: pid:" + com.navdy.service.library.util.SystemUtils.getProcessName(com.navdy.service.library.device.connection.ConnectionService.this.getApplicationContext(), getCallingPid()));
            if (com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice == null || !com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice.isConnected()) {
                com.navdy.service.library.device.connection.ConnectionService.this.forwardEventLocally((com.squareup.wire.Message) new com.navdy.service.library.events.connection.ConnectionStateChange("", com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED));
                com.navdy.service.library.device.connection.ConnectionService.this.forwardEventLocally((com.squareup.wire.Message) new com.navdy.service.library.events.connection.ConnectionStateChange("", com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_LINK_LOST));
            } else {
                com.navdy.service.library.device.connection.ConnectionService.this.forwardEventLocally((com.squareup.wire.Message) new com.navdy.service.library.events.connection.ConnectionStateChange(com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice.getDeviceId().toString(), com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_LINK_ESTABLISHED));
                com.navdy.service.library.device.connection.ConnectionService.this.forwardEventLocally((com.squareup.wire.Message) new com.navdy.service.library.events.connection.ConnectionStateChange(com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice.getDeviceId().toString(), com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_CONNECTED));
                com.navdy.service.library.events.DeviceInfo deviceInfo = com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice.getDeviceInfo();
                if (deviceInfo != null) {
                    com.navdy.service.library.device.connection.ConnectionService.this.logger.v("send device info");
                    com.navdy.service.library.device.connection.ConnectionService.this.forwardEventLocally((com.squareup.wire.Message) deviceInfo);
                }
            }
            com.navdy.service.library.device.connection.ConnectionService.this.sendEventsOnLocalConnect();
        }

        public void removeEventListener(com.navdy.hud.app.IEventListener listener) throws android.os.RemoteException {
            synchronized (com.navdy.service.library.device.connection.ConnectionService.this.listenerLock) {
                com.navdy.service.library.device.connection.ConnectionService.this.mListeners.remove(listener);
                com.navdy.service.library.device.connection.ConnectionService.this.createListenerList();
            }
            com.navdy.service.library.device.connection.ConnectionService.this.logger.v("listener removed: pid:" + com.navdy.service.library.util.SystemUtils.getProcessName(com.navdy.service.library.device.connection.ConnectionService.this.getApplicationContext(), getCallingPid()));
        }

        public void postEvent(byte[] bytes) throws android.os.RemoteException {
            boolean sentEvent = false;
            boolean createList = false;
            synchronized (com.navdy.service.library.device.connection.ConnectionService.this.listenerLock) {
                for (int i = 0; i < com.navdy.service.library.device.connection.ConnectionService.this.listenersArray.length; i++) {
                    try {
                        com.navdy.service.library.device.connection.ConnectionService.this.listenersArray[i].onEvent(bytes);
                        sentEvent = true;
                    } catch (android.os.DeadObjectException exception) {
                        com.navdy.service.library.device.connection.ConnectionService.this.logger.w("Reaping dead listener", exception);
                        com.navdy.service.library.device.connection.ConnectionService.this.mListeners.remove(com.navdy.service.library.device.connection.ConnectionService.this.listenersArray[i]);
                        createList = true;
                    } catch (android.os.TransactionTooLargeException tooLargeException) {
                        com.navdy.service.library.device.connection.ConnectionService.this.logger.w("Communication Pipe is full:", tooLargeException);
                    } catch (Throwable throwable) {
                        com.navdy.service.library.device.connection.ConnectionService.this.logger.w("Exception throws by remote:", throwable);
                    }
                }
                if (com.navdy.service.library.device.connection.ConnectionService.this.logger.isLoggable(2) && !sentEvent) {
                    com.navdy.service.library.device.connection.ConnectionService.this.logger.d("No one listening for event - byte length " + bytes.length);
                }
                if (createList) {
                    com.navdy.service.library.device.connection.ConnectionService.this.createListenerList();
                }
            }
        }

        public void postRemoteEvent(java.lang.String remoteDeviceId, byte[] bytes) throws android.os.RemoteException {
            if (remoteDeviceId == null || bytes == null) {
                com.navdy.service.library.device.connection.ConnectionService.this.logger.e("illegal argument");
                throw new android.os.RemoteException(com.navdy.service.library.device.connection.ConnectionService.ILLEGAL_ARGUMENT);
            }
            com.navdy.service.library.device.NavdyDeviceId deviceId = new com.navdy.service.library.device.NavdyDeviceId(remoteDeviceId);
            if (deviceId.equals(com.navdy.service.library.device.NavdyDeviceId.getThisDevice(com.navdy.service.library.device.connection.ConnectionService.this))) {
                com.navdy.service.library.events.NavdyEvent.MessageType messageType = com.navdy.service.library.events.WireUtil.getEventType(bytes);
                if (messageType == null || !com.navdy.service.library.device.connection.ConnectionService.this.processLocalEvent(bytes, messageType)) {
                    com.navdy.service.library.device.connection.ConnectionService.this.logger.w("Connection service ignored message:" + messageType);
                }
            } else if (com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice == null || !com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice.isConnected()) {
                if (com.navdy.service.library.device.connection.ConnectionService.this.logger.isLoggable(2)) {
                    com.navdy.service.library.device.connection.ConnectionService.this.logger.e("app not connected");
                }
            } else if (com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice.getDeviceId().equals(deviceId)) {
                com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice.postEvent(bytes);
            } else {
                com.navdy.service.library.device.connection.ConnectionService.this.logger.i("Device id mismatch: deviceId=" + deviceId + " remote:" + com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice.getDeviceId());
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            if (com.navdy.service.library.device.connection.ConnectionService.this.state == com.navdy.service.library.device.connection.ConnectionService.State.RECONNECTING && com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice != null && !com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice.isConnecting() && !com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice.isConnected()) {
                com.navdy.service.library.device.connection.ConnectionService.this.logger.i("Retrying to connect to " + com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice.getDeviceId());
                com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice.connect();
            }
        }
    }

    class Anon3 extends android.content.BroadcastReceiver {
        Anon3() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            if (intent.getAction().equals("android.bluetooth.adapter.action.STATE_CHANGED") && intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE) == 12) {
                if (com.navdy.service.library.device.connection.ConnectionService.this.state == com.navdy.service.library.device.connection.ConnectionService.State.START) {
                    com.navdy.service.library.device.connection.ConnectionService.this.logger.i("bluetooth turned on - exiting START state");
                    com.navdy.service.library.device.connection.ConnectionService.this.setState(com.navdy.service.library.device.connection.ConnectionService.State.IDLE);
                    return;
                }
                com.navdy.service.library.device.connection.ConnectionService.this.logger.i("bluetooth turned on - restarting listeners");
                com.navdy.service.library.device.connection.ConnectionService.this.serviceHandler.sendEmptyMessage(4);
            }
        }
    }

    private class PendingConnectHandler implements java.lang.Runnable {
        private com.navdy.service.library.device.connection.Connection connection;
        private com.navdy.service.library.device.RemoteDevice device;

        public PendingConnectHandler(com.navdy.service.library.device.RemoteDevice device2, com.navdy.service.library.device.connection.Connection connection2) {
            this.device = device2;
            this.connection = connection2;
        }

        public void run() {
            boolean newDevice = com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice != this.device;
            com.navdy.service.library.device.connection.ConnectionService.this.logger.d("Trying to " + (newDevice ? "connect" : "reconnect") + " to " + this.device.getDeviceId().getDisplayName());
            com.navdy.service.library.device.connection.ConnectionService.this.setRemoteDevice(this.device);
            if (this.connection != null) {
                com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice.setActiveConnection(this.connection);
            } else if (com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice.isConnected()) {
                com.navdy.service.library.device.connection.ConnectionService.this.onDeviceConnected(com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice);
            } else {
                com.navdy.service.library.device.connection.ConnectionService.this.setState(newDevice ? com.navdy.service.library.device.connection.ConnectionService.State.CONNECTING : com.navdy.service.library.device.connection.ConnectionService.State.RECONNECTING);
                com.navdy.service.library.device.connection.ConnectionService.this.mRemoteDevice.connect();
            }
            com.navdy.service.library.device.connection.ConnectionService.this.pendingConnectHandler = null;
        }
    }

    protected final class ServiceHandler extends android.os.Handler {
        public ServiceHandler(android.os.Looper looper) {
            super(looper);
        }

        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 1:
                    com.navdy.service.library.device.connection.ConnectionService.State newState = (com.navdy.service.library.device.connection.ConnectionService.State) msg.obj;
                    if (newState != com.navdy.service.library.device.connection.ConnectionService.this.state) {
                        com.navdy.service.library.device.connection.ConnectionService.this.exitState(com.navdy.service.library.device.connection.ConnectionService.this.state);
                        com.navdy.service.library.device.connection.ConnectionService.this.state = newState;
                        com.navdy.service.library.device.connection.ConnectionService.this.enterState(com.navdy.service.library.device.connection.ConnectionService.this.state);
                        return;
                    }
                    return;
                case 2:
                    com.navdy.service.library.device.connection.ConnectionService.this.heartBeat();
                    return;
                case 3:
                    com.navdy.service.library.device.RemoteDevice device = (com.navdy.service.library.device.RemoteDevice) msg.obj;
                    com.navdy.service.library.device.connection.ConnectionService.this.handleDeviceDisconnect(device, com.navdy.service.library.device.connection.Connection.DisconnectCause.values()[msg.arg1]);
                    return;
                case 4:
                    if (com.navdy.service.library.device.connection.ConnectionService.this.listening) {
                        com.navdy.service.library.device.connection.ConnectionService.this.logger.i("stopping/starting listeners");
                        com.navdy.service.library.device.connection.ConnectionService.this.stopListeners();
                        com.navdy.service.library.device.connection.ConnectionService.this.startListeners();
                    }
                    if (com.navdy.service.library.device.connection.ConnectionService.this.broadcasting) {
                        com.navdy.service.library.device.connection.ConnectionService.this.logger.i("restarting broadcasters");
                        com.navdy.service.library.device.connection.ConnectionService.this.stopBroadcasters();
                        com.navdy.service.library.device.connection.ConnectionService.this.startBroadcasters();
                    }
                    com.navdy.service.library.device.connection.ConnectionService.this.stopProxyService();
                    com.navdy.service.library.device.connection.ConnectionService.this.startProxyService();
                    return;
                default:
                    com.navdy.service.library.device.connection.ConnectionService.this.logger.e("Unknown message: " + msg);
                    return;
            }
        }
    }

    public enum State {
        START,
        IDLE,
        SEARCHING,
        CONNECTING,
        RECONNECTING,
        CONNECTED,
        DISCONNECTING,
        DISCONNECTED,
        PAIRING,
        LISTENING,
        DESTROYED
    }

    protected abstract com.navdy.service.library.device.connection.ProxyService createProxyService() throws java.io.IOException;

    protected abstract com.navdy.service.library.device.connection.ConnectionListener[] getConnectionListeners(android.content.Context context);

    protected abstract com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster[] getRemoteDeviceBroadcasters();

    private void sendPingIfNeeded() {
        try {
            if (this.mRemoteDevice != null && this.mRemoteDevice.isConnected() && android.os.SystemClock.elapsedRealtime() - this.mRemoteDevice.lastMessageSentTime > 4000) {
                this.mRemoteDevice.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.Ping());
                if (this.serverMode && this.logger.isLoggable(2)) {
                    this.logger.v("NAVDY-PACKET [H2P-Outgoing-Event] Ping");
                }
            }
        } catch (Throwable t) {
            this.logger.e(t);
        }
    }

    private void checkConnection() {
        if (this.mRemoteDevice == null || !this.mRemoteDevice.isConnected()) {
            this.logger.v("dead connection remotedevice=" + (this.mRemoteDevice == null ? com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID : "not null") + " isConnected:" + (this.mRemoteDevice == null ? "false" : java.lang.Boolean.valueOf(this.mRemoteDevice.isConnected())));
            setState(com.navdy.service.library.device.connection.ConnectionService.State.DISCONNECTING);
        } else if (this.mRemoteDevice.getLinkStatus() == com.navdy.service.library.device.RemoteDevice.LinkStatus.CONNECTED) {
            long timeElapsed = android.os.SystemClock.elapsedRealtime() - this.lastMessageReceivedTime;
            if (timeElapsed > com.here.odnp.util.OdnpConstants.ONE_MINUTE_IN_MS) {
                this.logger.v("dead connection timed out:" + timeElapsed);
                if (reconnectAfterDeadConnection()) {
                    this.logger.d("Reconncting after dead connection timed out disconnect");
                    reconnect(REASON_DEAD_CONNECTION);
                    return;
                }
                setState(com.navdy.service.library.device.connection.ConnectionService.State.DISCONNECTING);
            }
        }
    }

    public boolean reconnectAfterDeadConnection() {
        return false;
    }

    public android.os.IBinder onBind(android.content.Intent intent) {
        if (com.navdy.hud.app.IEventSource.class.getName().equals(intent.getAction())) {
            this.logger.d("returning IEventSource API endpoint");
            return this.mEventSource;
        }
        this.logger.w("invalid action:" + intent.getAction());
        return null;
    }

    public void onCreate() {
        this.logger.e("ConnectionService created: mainthread:" + android.os.Looper.getMainLooper().getThread().getId());
        this.mWire = new com.squareup.wire.Wire((java.lang.Class<?>[]) new java.lang.Class[]{com.navdy.service.library.events.Ext_NavdyEvent.class});
        android.os.HandlerThread thread = new android.os.HandlerThread("ConnectionService");
        thread.start();
        this.serviceLooper = thread.getLooper();
        this.serviceHandler = new com.navdy.service.library.device.connection.ConnectionService.ServiceHandler(this.serviceLooper);
        registerReceiver(this.bluetoothReceiver, new android.content.IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
        this.deviceRegistry = com.navdy.service.library.device.RemoteDeviceRegistry.getInstance(this);
    }

    public int onStartCommand(android.content.Intent intent, int flags, int startId) {
        if (intent != null && android.bluetooth.BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            if (this.state == com.navdy.service.library.device.connection.ConnectionService.State.CONNECTED) {
                this.logger.i("BT enabled, disconnecting since app likely crashed");
                setState(com.navdy.service.library.device.connection.ConnectionService.State.DISCONNECTING);
            } else {
                this.logger.i("BT enabled, switching to IDLE state");
                setState(com.navdy.service.library.device.connection.ConnectionService.State.IDLE);
            }
        }
        return 1;
    }

    public void onDestroy() {
        this.logger.v("ConnectionService destroyed.  mwhahahaha.");
        try {
            setState(com.navdy.service.library.device.connection.ConnectionService.State.DESTROYED);
            unregisterReceiver(this.bluetoothReceiver);
            this.serviceLooper.quitSafely();
            stopProxyService();
        } catch (Throwable t) {
            this.logger.e(t);
        }
    }

    protected void forwardEventLocally(com.squareup.wire.Message message) {
        forwardEventLocally(com.navdy.service.library.events.NavdyEventUtil.eventFromMessage(message).toByteArray());
    }

    protected void forwardEventLocally(byte[] eventData) {
        try {
            this.mEventSource.postEvent(eventData);
        } catch (Throwable t) {
            this.logger.e(t);
        }
    }

    protected void startBroadcasters() {
        synchronized (this.connectionLock) {
            this.broadcasting = true;
            if (this.mRemoteDeviceBroadcasters == null) {
                this.mRemoteDeviceBroadcasters = getRemoteDeviceBroadcasters();
            }
            for (int i = 0; i < this.mRemoteDeviceBroadcasters.length; i++) {
                this.logger.v("starting connection broadcaster:" + this.mRemoteDeviceBroadcasters[i].getClass().getName());
                try {
                    this.mRemoteDeviceBroadcasters[i].start();
                } catch (Throwable t) {
                    this.logger.e(t);
                }
            }
        }
    }

    protected void stopBroadcasters() {
        this.logger.v("stopping all broadcasters");
        synchronized (this.connectionLock) {
            this.broadcasting = false;
            if (this.mRemoteDeviceBroadcasters != null) {
                for (int i = 0; i < this.mRemoteDeviceBroadcasters.length; i++) {
                    this.logger.v("stopping connection broadcaster:" + this.mRemoteDeviceBroadcasters[i].getClass().getName());
                    try {
                        this.mRemoteDeviceBroadcasters[i].stop();
                    } catch (Throwable t) {
                        this.logger.e(t);
                    }
                }
            }
        }
    }

    protected void startListeners() {
        synchronized (this.connectionLock) {
            this.listening = true;
            if (this.mConnectionListeners == null) {
                this.logger.v("initializing connector listener");
                this.mConnectionListeners = getConnectionListeners(getApplicationContext());
                for (com.navdy.service.library.device.connection.ConnectionListener addListener : this.mConnectionListeners) {
                    addListener.addListener(this);
                }
            }
            for (int i = 0; i < this.mConnectionListeners.length; i++) {
                this.logger.v("starting connection listener:" + this.mConnectionListeners[i]);
                try {
                    this.mConnectionListeners[i].start();
                } catch (Throwable t) {
                    this.logger.e(t);
                }
            }
        }
    }

    protected void stopListeners() {
        synchronized (this.connectionLock) {
            this.listening = false;
            if (this.mConnectionListeners != null) {
                for (int i = 0; i < this.mConnectionListeners.length; i++) {
                    this.logger.v("stopping:" + this.mConnectionListeners[i]);
                    try {
                        this.mConnectionListeners[i].stop();
                    } catch (Throwable t) {
                        this.logger.e(t);
                    }
                }
            }
        }
    }

    public void onStarted(com.navdy.service.library.device.connection.ConnectionListener connectionListener) {
        this.logger.v("started listening");
    }

    public void onStartFailure(com.navdy.service.library.device.connection.ConnectionListener connectionListener) {
        this.logger.e("failed to start listening:" + connectionListener);
    }

    public void onStopped(com.navdy.service.library.device.connection.ConnectionListener connectionListener) {
        this.logger.v("stopped listening:" + connectionListener);
    }

    public void onConnected(com.navdy.service.library.device.connection.ConnectionListener connectionListener, com.navdy.service.library.device.connection.Connection connection) {
        this.logger.v("listener connected");
        setActiveDevice(new com.navdy.service.library.device.RemoteDevice(getApplicationContext(), connection.getConnectionInfo(), this.inProcess), connection);
    }

    public void onConnectionFailed(com.navdy.service.library.device.connection.ConnectionListener connectionListener) {
        this.logger.e("onConnectionFailed:restart listeners");
        setState(com.navdy.service.library.device.connection.ConnectionService.State.IDLE);
    }

    protected synchronized void setRemoteDevice(com.navdy.service.library.device.RemoteDevice remoteDevice) {
        if (remoteDevice != this.mRemoteDevice) {
            if (this.mRemoteDevice != null) {
                this.mRemoteDevice.removeListener(this);
            }
            this.mRemoteDevice = remoteDevice;
            if (this.mRemoteDevice != null) {
                this.mRemoteDevice.addListener(this);
            }
        }
    }

    public void connect(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
        setActiveDevice(new com.navdy.service.library.device.RemoteDevice((android.content.Context) this, connectionInfo, this.inProcess));
    }

    public boolean setActiveDevice(com.navdy.service.library.device.RemoteDevice remoteDevice) {
        return setActiveDevice(remoteDevice, null);
    }

    public synchronized void reconnect(java.lang.String reconnectReason) {
        if (this.mRemoteDevice != null) {
            this.forceReconnect = true;
            setState(com.navdy.service.library.device.connection.ConnectionService.State.DISCONNECTING);
        }
    }

    public synchronized boolean setActiveDevice(com.navdy.service.library.device.RemoteDevice remoteDevice, com.navdy.service.library.device.connection.Connection connection) {
        if (this.mRemoteDevice != null) {
            if (this.mRemoteDevice.isConnected() || this.mRemoteDevice.isConnecting()) {
                setState(com.navdy.service.library.device.connection.ConnectionService.State.DISCONNECTING);
            } else {
                setRemoteDevice(null);
            }
        }
        if (remoteDevice != null) {
            com.navdy.service.library.device.connection.ConnectionService.PendingConnectHandler connectHandler = new com.navdy.service.library.device.connection.ConnectionService.PendingConnectHandler(remoteDevice, connection);
            if (this.mRemoteDevice == null) {
                connectHandler.run();
            } else {
                this.pendingConnectHandler = connectHandler;
            }
        }
        return true;
    }

    private boolean hasPaired() {
        return this.deviceRegistry.hasPaired();
    }

    public void setState(com.navdy.service.library.device.connection.ConnectionService.State newState) {
        if (!this.quitting && this.state != newState) {
            if (newState == com.navdy.service.library.device.connection.ConnectionService.State.DESTROYED) {
                this.quitting = true;
            }
            this.serviceHandler.sendMessage(this.serviceHandler.obtainMessage(1, newState));
        }
    }

    protected void enterState(com.navdy.service.library.device.connection.ConnectionService.State state2) {
        this.logger.d("Entering state:" + state2);
        if (state2 == com.navdy.service.library.device.connection.ConnectionService.State.DESTROYED) {
            if (this.listening) {
                stopListeners();
            }
            if (this.broadcasting) {
                stopBroadcasters();
            }
            this.serviceHandler.removeCallbacksAndMessages(null);
            return;
        }
        this.stateAge = 0;
        int timeout = 1000;
        switch (state2) {
            case IDLE:
                timeout = 1000;
                break;
            case PAIRING:
                timeout = com.google.firebase.appindexing.Indexable.MAX_BYTE_SIZE;
                startBroadcasters();
                startListeners();
                forwardEventLocally((com.squareup.wire.Message) new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_PAIRING, ""));
                break;
            case LISTENING:
                if (isPromiscuous()) {
                    startBroadcasters();
                }
                startListeners();
                if (this.serverMode) {
                    timeout = com.google.firebase.appindexing.Indexable.MAX_BYTE_SIZE;
                    break;
                }
                break;
            case CONNECTING:
                timeout = 10000;
                break;
            case RECONNECTING:
                timeout = 10000;
                break;
            case DISCONNECTING:
                if (this.mRemoteDevice == null) {
                    setState(com.navdy.service.library.device.connection.ConnectionService.State.IDLE);
                    break;
                } else {
                    timeout = com.navdy.client.app.ui.settings.AudioDialogActivity.DELAY_MILLIS;
                    this.mRemoteDevice.disconnect();
                    break;
                }
            case CONNECTED:
                stopListeners();
                stopBroadcasters();
                handleDeviceConnect(this.mRemoteDevice);
                break;
        }
        this.serviceHandler.removeMessages(2);
        this.serviceHandler.sendEmptyMessageDelayed(2, (long) timeout);
    }

    protected void exitState(com.navdy.service.library.device.connection.ConnectionService.State state2) {
        this.logger.d("Exiting state:" + state2);
        switch (state2) {
            case PAIRING:
                if (!isPromiscuous()) {
                    stopBroadcasters();
                    return;
                }
                return;
            case RECONNECTING:
                this.serviceHandler.removeCallbacks(this.reconnectRunnable);
                return;
            case START:
                startProxyService();
                return;
            default:
                return;
        }
    }

    protected void heartBeat() {
        if (this.stateAge % 10 == 0) {
            this.logger.d("Heartbeat: in state:" + this.state);
        }
        this.stateAge++;
        int timeout = 1000;
        switch (this.state) {
            case IDLE:
                if (this.pendingConnectHandler == null) {
                    if (hasPaired() || !this.serverMode) {
                        if (!hasPaired()) {
                            timeout = 60000;
                            break;
                        } else {
                            setState(com.navdy.service.library.device.connection.ConnectionService.State.LISTENING);
                            break;
                        }
                    } else {
                        setState(com.navdy.service.library.device.connection.ConnectionService.State.PAIRING);
                        break;
                    }
                } else {
                    this.pendingConnectHandler.run();
                    break;
                }
                break;
            case PAIRING:
                timeout = com.google.firebase.appindexing.Indexable.MAX_BYTE_SIZE;
                break;
            case LISTENING:
                if (this.serverMode) {
                    if (this.mRemoteDevice == null) {
                        if (!needAutoSearch()) {
                            timeout = 60000;
                            break;
                        } else {
                            setState(com.navdy.service.library.device.connection.ConnectionService.State.SEARCHING);
                            break;
                        }
                    } else {
                        setState(com.navdy.service.library.device.connection.ConnectionService.State.RECONNECTING);
                        this.serviceHandler.post(this.reconnectRunnable);
                        break;
                    }
                } else {
                    timeout = 60000;
                    break;
                }
            case CONNECTING:
            case RECONNECTING:
            case DISCONNECTING:
                setState(com.navdy.service.library.device.connection.ConnectionService.State.IDLE);
                break;
            case CONNECTED:
                timeout = HEARTBEAT_INTERVAL;
                sendPingIfNeeded();
                checkConnection();
                break;
        }
        this.serviceHandler.removeMessages(2);
        this.serviceHandler.sendEmptyMessageDelayed(2, (long) timeout);
    }

    public boolean needAutoSearch() {
        return true;
    }

    public boolean isPromiscuous() {
        return false;
    }

    public void onDeviceConnecting(com.navdy.service.library.device.RemoteDevice device) {
    }

    public void onDeviceConnected(com.navdy.service.library.device.RemoteDevice device) {
        setState(com.navdy.service.library.device.connection.ConnectionService.State.CONNECTED);
    }

    public void onDeviceConnectFailure(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.device.connection.Connection.ConnectionFailureCause cause) {
        if (this.state == com.navdy.service.library.device.connection.ConnectionService.State.RECONNECTING) {
            this.logger.i("onDeviceConnectFailure - retrying");
            this.serviceHandler.postDelayed(this.reconnectRunnable, 2000);
            return;
        }
        this.logger.i("onDeviceConnectFailure - switching to idle state");
        if (device != this.mRemoteDevice || this.serverMode) {
            this.logger.d("Not clearing the remote device, to attempt reconnect");
        } else {
            setRemoteDevice(null);
        }
        setState(com.navdy.service.library.device.connection.ConnectionService.State.IDLE);
    }

    public void onDeviceDisconnected(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.device.connection.Connection.DisconnectCause cause) {
        this.serviceHandler.sendMessage(this.serviceHandler.obtainMessage(3, cause.ordinal(), -1, device));
    }

    protected void rememberPairedDevice(com.navdy.service.library.device.RemoteDevice device) {
        this.deviceRegistry.addPairedConnection(device.getActiveConnectionInfo());
        forwardEventLocally((com.squareup.wire.Message) DEVICES_CHANGED_EVENT);
    }

    protected void forgetPairedDevice(com.navdy.service.library.device.RemoteDevice device) {
        this.deviceRegistry.removePairedConnection(device.getActiveConnectionInfo());
        forwardEventLocally((com.squareup.wire.Message) DEVICES_CHANGED_EVENT);
    }

    protected void forgetPairedDevice(android.bluetooth.BluetoothDevice device) {
        this.deviceRegistry.removePairedConnection(device);
        forwardEventLocally((com.squareup.wire.Message) DEVICES_CHANGED_EVENT);
    }

    private void handleDeviceConnect(com.navdy.service.library.device.RemoteDevice device) {
        this.logger.v("onDeviceConnected:remembering device");
        rememberPairedDevice(device);
        this.logger.v("onDeviceConnected:heartBeat started");
        this.lastMessageReceivedTime = android.os.SystemClock.elapsedRealtime();
        this.logger.d("Connecting with app context: " + getApplicationContext());
        if (!this.mRemoteDevice.startLink()) {
            setState(com.navdy.service.library.device.connection.ConnectionService.State.DISCONNECTING);
        }
    }

    protected void handleDeviceDisconnect(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.device.connection.Connection.DisconnectCause cause) {
        this.logger.v("device disconnect:" + device + " cause:" + cause);
        if (device != null) {
            device.stopLink();
        }
        if (this.mRemoteDevice != device) {
            return;
        }
        if ((cause == com.navdy.service.library.device.connection.Connection.DisconnectCause.ABORTED || this.forceReconnect) && this.serverMode) {
            this.forceReconnect = false;
            new com.navdy.service.library.device.connection.ConnectionService.PendingConnectHandler(this.mRemoteDevice, null).run();
            return;
        }
        setRemoteDevice(null);
        setState(com.navdy.service.library.device.connection.ConnectionService.State.IDLE);
    }

    public void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice device, byte[] eventData) {
        this.lastMessageReceivedTime = android.os.SystemClock.elapsedRealtime();
        com.navdy.service.library.events.NavdyEvent.MessageType messageType = com.navdy.service.library.events.WireUtil.getEventType(eventData);
        if (this.logger.isLoggable(2)) {
            this.logger.v((this.serverMode ? "NAVDY-PACKET [P2H" : "NAVDY-PACKET [H2P") + "-Event] " + (messageType != null ? messageType.name() : com.navdy.client.app.ui.settings.SettingsConstants.LAST_HUD_UUID_DEFAULT) + " size:" + eventData.length);
        }
        if (messageType != null && messageType != com.navdy.service.library.events.NavdyEvent.MessageType.Ping) {
            if (messageType == com.navdy.service.library.events.NavdyEvent.MessageType.DisconnectRequest) {
                try {
                    handleRemoteDisconnect((com.navdy.service.library.events.connection.DisconnectRequest) ((com.navdy.service.library.events.NavdyEvent) this.mWire.parseFrom(eventData, com.navdy.service.library.events.NavdyEvent.class)).getExtension(com.navdy.service.library.events.Ext_NavdyEvent.disconnectRequest));
                } catch (Throwable t) {
                    this.logger.e("Failed to parse event", t);
                }
            } else if (processEvent(eventData, messageType)) {
            } else {
                if (messageType == com.navdy.service.library.events.NavdyEvent.MessageType.DeviceInfo) {
                    try {
                        if (this.mRemoteDevice != null && this.mRemoteDevice.isConnected()) {
                            com.navdy.service.library.events.DeviceInfo patchedDeviceInfo = new com.navdy.service.library.events.DeviceInfo.Builder((com.navdy.service.library.events.DeviceInfo) com.navdy.service.library.events.NavdyEventUtil.messageFromEvent((com.navdy.service.library.events.NavdyEvent) this.mWire.parseFrom(eventData, com.navdy.service.library.events.NavdyEvent.class))).deviceId(this.mRemoteDevice.getDeviceId().toString()).build();
                            this.mRemoteDevice.setDeviceInfo(patchedDeviceInfo);
                            this.logger.v("set remote device info");
                            forwardEventLocally(com.navdy.service.library.events.NavdyEventUtil.eventFromMessage(patchedDeviceInfo).toByteArray());
                        }
                    } catch (Throwable t2) {
                        this.logger.e("Failed to parse deviceinfo", t2);
                    }
                } else {
                    forwardEventLocally(eventData);
                }
            }
        }
    }

    public void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.events.NavdyEvent event) {
        this.lastMessageReceivedTime = android.os.SystemClock.elapsedRealtime();
        if (event.type == com.navdy.service.library.events.NavdyEvent.MessageType.DisconnectRequest) {
            handleRemoteDisconnect((com.navdy.service.library.events.connection.DisconnectRequest) event.getExtension(com.navdy.service.library.events.Ext_NavdyEvent.disconnectRequest));
        }
    }

    protected boolean processEvent(byte[] eventData, com.navdy.service.library.events.NavdyEvent.MessageType messageType) {
        return false;
    }

    protected boolean processLocalEvent(byte[] eventData, com.navdy.service.library.events.NavdyEvent.MessageType messageType) {
        return false;
    }

    private void createListenerList() {
        this.listenersArray = new com.navdy.hud.app.IEventListener[this.mListeners.size()];
        this.mListeners.toArray(this.listenersArray);
    }

    private void handleRemoteDisconnect(com.navdy.service.library.events.connection.DisconnectRequest disconnectRequest) {
        if (this.mRemoteDevice != null) {
            if (disconnectRequest.forget != null && disconnectRequest.forget.booleanValue()) {
                forgetPairedDevice(this.mRemoteDevice);
            }
            setState(com.navdy.service.library.device.connection.ConnectionService.State.DISCONNECTING);
        }
    }

    protected void startProxyService() {
        if (this.proxyService == null || !this.proxyService.isAlive()) {
            this.logger.v(com.navdy.service.library.util.SystemUtils.getProcessName(getApplicationContext(), android.os.Process.myPid()) + ": start service for proxy");
            try {
                this.proxyService = createProxyService();
                this.proxyService.start();
            } catch (java.lang.Exception e) {
                this.logger.e("Failed to start proxy service", e);
            }
        }
    }

    protected void stopProxyService() {
        if (this.proxyService != null) {
            this.proxyService.cancel();
            this.proxyService = null;
        }
    }

    public void sendMessage(com.squareup.wire.Message event) {
        if (isConnected()) {
            this.mRemoteDevice.postEvent(event);
        }
    }

    public boolean isConnected() {
        return this.mRemoteDevice != null && this.mRemoteDevice.isConnected();
    }

    protected void sendEventsOnLocalConnect() {
    }

    protected void setBandwidthLevel(int level) {
        if (this.state == com.navdy.service.library.device.connection.ConnectionService.State.CONNECTED && this.mRemoteDevice != null) {
            if (level == 0 || level == 1) {
                this.mRemoteDevice.setLinkBandwidthLevel(level);
                com.navdy.service.library.events.connection.LinkPropertiesChanged.Builder builder = new com.navdy.service.library.events.connection.LinkPropertiesChanged.Builder();
                builder.bandwidthLevel(java.lang.Integer.valueOf(level));
                forwardEventLocally((com.squareup.wire.Message) builder.build());
            }
        }
    }
}
