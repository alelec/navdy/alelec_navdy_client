package com.navdy.service.library.device.discovery;

public class TCPRemoteDeviceBroadcaster extends com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster {
    public TCPRemoteDeviceBroadcaster(android.content.Context context) {
        super(context, com.navdy.service.library.device.connection.TCPConnectionInfo.nsdServiceWithDeviceId(com.navdy.service.library.device.NavdyDeviceId.getThisDevice(context)));
    }
}
