package com.navdy.service.library.device.connection;

public abstract class ConnectionListener extends com.navdy.service.library.util.Listenable {
    protected final com.navdy.service.library.log.Logger logger;
    com.navdy.service.library.device.connection.ConnectionListener.AcceptThread mAcceptThread;
    protected final android.content.Context mContext;

    protected abstract class AcceptThread extends java.lang.Thread {
        abstract void cancel();

        protected AcceptThread() {
        }
    }

    class Anon1 implements com.navdy.service.library.device.connection.ConnectionListener.EventDispatcher {
        Anon1() {
        }

        public void dispatchEvent(com.navdy.service.library.device.connection.ConnectionListener source, com.navdy.service.library.device.connection.ConnectionListener.Listener listener) {
            listener.onStarted(source);
        }
    }

    class Anon2 implements com.navdy.service.library.device.connection.ConnectionListener.EventDispatcher {
        Anon2() {
        }

        public void dispatchEvent(com.navdy.service.library.device.connection.ConnectionListener source, com.navdy.service.library.device.connection.ConnectionListener.Listener listener) {
            listener.onStartFailure(source);
        }
    }

    class Anon3 implements com.navdy.service.library.device.connection.ConnectionListener.EventDispatcher {
        Anon3() {
        }

        public void dispatchEvent(com.navdy.service.library.device.connection.ConnectionListener source, com.navdy.service.library.device.connection.ConnectionListener.Listener listener) {
            listener.onStopped(source);
        }
    }

    class Anon4 implements com.navdy.service.library.device.connection.ConnectionListener.EventDispatcher {
        final /* synthetic */ com.navdy.service.library.device.connection.Connection val$connection;

        Anon4(com.navdy.service.library.device.connection.Connection connection) {
            this.val$connection = connection;
        }

        public void dispatchEvent(com.navdy.service.library.device.connection.ConnectionListener source, com.navdy.service.library.device.connection.ConnectionListener.Listener listener) {
            listener.onConnected(source, this.val$connection);
        }
    }

    class Anon5 implements com.navdy.service.library.device.connection.ConnectionListener.EventDispatcher {
        Anon5() {
        }

        public void dispatchEvent(com.navdy.service.library.device.connection.ConnectionListener source, com.navdy.service.library.device.connection.ConnectionListener.Listener listener) {
            listener.onConnectionFailed(source);
        }
    }

    protected interface EventDispatcher extends com.navdy.service.library.util.Listenable.EventDispatcher<com.navdy.service.library.device.connection.ConnectionListener, com.navdy.service.library.device.connection.ConnectionListener.Listener> {
    }

    public interface Listener extends com.navdy.service.library.util.Listenable.Listener {
        void onConnected(com.navdy.service.library.device.connection.ConnectionListener connectionListener, com.navdy.service.library.device.connection.Connection connection);

        void onConnectionFailed(com.navdy.service.library.device.connection.ConnectionListener connectionListener);

        void onStartFailure(com.navdy.service.library.device.connection.ConnectionListener connectionListener);

        void onStarted(com.navdy.service.library.device.connection.ConnectionListener connectionListener);

        void onStopped(com.navdy.service.library.device.connection.ConnectionListener connectionListener);
    }

    protected abstract com.navdy.service.library.device.connection.ConnectionListener.AcceptThread getNewAcceptThread() throws java.io.IOException;

    public abstract com.navdy.service.library.device.connection.ConnectionType getType();

    public ConnectionListener(android.content.Context context, java.lang.String tag) {
        this.mContext = context;
        this.logger = new com.navdy.service.library.log.Logger(tag);
    }

    public synchronized boolean start() {
        boolean z = false;
        synchronized (this) {
            if (this.mAcceptThread != null) {
                if (this.mAcceptThread.isAlive()) {
                    this.logger.e("Already running");
                } else {
                    this.logger.e("clearing existing accept thread");
                    this.mAcceptThread.cancel();
                    this.mAcceptThread = null;
                }
            }
            try {
                this.mAcceptThread = getNewAcceptThread();
                if (this.mAcceptThread != null) {
                    this.mAcceptThread.start();
                }
                z = true;
            } catch (Throwable e) {
                this.logger.e("Unable to start accept thread: ", e);
                dispatchStartFailure();
            }
        }
        return z;
    }

    public synchronized boolean stop() {
        boolean z;
        if (this.mAcceptThread == null) {
            this.logger.e("Already stopped.");
            z = false;
        } else {
            this.mAcceptThread.cancel();
            this.mAcceptThread = null;
            z = true;
        }
        return z;
    }

    public void dispatchStarted() {
        dispatchToListeners(new com.navdy.service.library.device.connection.ConnectionListener.Anon1());
    }

    public void dispatchStartFailure() {
        dispatchToListeners(new com.navdy.service.library.device.connection.ConnectionListener.Anon2());
    }

    public void dispatchStopped() {
        dispatchToListeners(new com.navdy.service.library.device.connection.ConnectionListener.Anon3());
    }

    public void dispatchConnected(com.navdy.service.library.device.connection.Connection connection) {
        dispatchToListeners(new com.navdy.service.library.device.connection.ConnectionListener.Anon4(connection));
    }

    public void dispatchConnectionFailed() {
        dispatchToListeners(new com.navdy.service.library.device.connection.ConnectionListener.Anon5());
    }
}
