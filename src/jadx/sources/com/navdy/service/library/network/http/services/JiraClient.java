package com.navdy.service.library.network.http.services;

public class JiraClient {
    public static final java.lang.String ASSIGNEE = "assignee";
    public static final java.lang.String DESCRIPTION = "description";
    public static final java.lang.String EMAIL_ADDRESS = "emailAddress";
    public static final java.lang.String ENVIRONMENT = "environment";
    public static final java.lang.String FIELDS = "fields";
    public static final java.lang.String ISSUE_TYPE = "issuetype";
    public static final java.lang.String ISSUE_TYPE_BUG = "Bug";
    public static final java.lang.String JIRA_API_SEARCH_ASSIGNABLE_USER = "https://navdyhud.atlassian.net/rest/api/2/user/assignable/search";
    private static final java.lang.String JIRA_ATTACHMENTS = "/attachments/";
    private static final java.lang.String JIRA_ISSUE_API_URL = "https://navdyhud.atlassian.net/rest/api/2/issue/";
    private static final java.lang.String JIRA_ISSUE_EDIT_URL = "https://navdyhud.atlassian.net/rest/api/2/issue/";
    public static final java.lang.String KEY = "key";
    public static final java.lang.String NAME = "name";
    public static final java.lang.String PROJECT = "project";
    public static final java.lang.String SUMMARY = "summary";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.network.http.services.JiraClient.class);
    private java.lang.String encodedCredentials;
    private okhttp3.OkHttpClient mClient;

    class Anon1 implements okhttp3.Callback {
        final /* synthetic */ com.navdy.service.library.network.http.services.JiraClient.ResultCallback val$callback;
        final /* synthetic */ java.lang.String val$emailAddress;
        final /* synthetic */ java.lang.String val$ticketId;

        Anon1(com.navdy.service.library.network.http.services.JiraClient.ResultCallback resultCallback, java.lang.String str, java.lang.String str2) {
            this.val$callback = resultCallback;
            this.val$emailAddress = str;
            this.val$ticketId = str2;
        }

        public void onFailure(okhttp3.Call call, java.io.IOException e) {
            com.navdy.service.library.network.http.services.JiraClient.sLogger.e("onFailure ", e);
            if (this.val$callback != null) {
                this.val$callback.onError(e);
            }
        }

        public void onResponse(okhttp3.Call call, okhttp3.Response response) throws java.io.IOException {
            try {
                int responseCode = response.code();
                if (responseCode == 200) {
                    org.json.JSONArray resultsArray = new org.json.JSONArray(response.body().string());
                    if (resultsArray.length() > 0) {
                        java.lang.String userName = "";
                        int i = 0;
                        while (true) {
                            if (i >= resultsArray.length()) {
                                break;
                            }
                            org.json.JSONObject userObject = (org.json.JSONObject) resultsArray.get(i);
                            if (i == 0) {
                                userName = userObject.getString("name");
                            }
                            if (userObject.has(com.navdy.service.library.network.http.services.JiraClient.EMAIL_ADDRESS)) {
                                java.lang.String emailId = userObject.getString(com.navdy.service.library.network.http.services.JiraClient.EMAIL_ADDRESS);
                                com.navdy.service.library.network.http.services.JiraClient.sLogger.d("Email Id " + emailId);
                                if (android.text.TextUtils.equals(emailId, this.val$emailAddress) && userObject.has("name")) {
                                    userName = userObject.getString("name");
                                    com.navdy.service.library.network.http.services.JiraClient.sLogger.d("User name " + userName);
                                    break;
                                }
                            }
                            i++;
                        }
                        com.navdy.service.library.network.http.services.JiraClient.sLogger.d("Assigning the ticket to User " + userName);
                        com.navdy.service.library.network.http.services.JiraClient.this.assignTicketToUser(this.val$ticketId, userName, this.val$callback);
                    }
                } else if (this.val$callback != null) {
                    this.val$callback.onError(new java.io.IOException("Jira request failed with " + responseCode));
                }
            } catch (Throwable t) {
                if (this.val$callback != null) {
                    this.val$callback.onError(t);
                }
            }
        }
    }

    class Anon2 implements okhttp3.Callback {
        final /* synthetic */ com.navdy.service.library.network.http.services.JiraClient.ResultCallback val$callback;
        final /* synthetic */ java.lang.String val$user;

        Anon2(com.navdy.service.library.network.http.services.JiraClient.ResultCallback resultCallback, java.lang.String str) {
            this.val$callback = resultCallback;
            this.val$user = str;
        }

        public void onFailure(okhttp3.Call call, java.io.IOException e) {
            com.navdy.service.library.network.http.services.JiraClient.sLogger.e("onFailure ", e);
            if (this.val$callback != null) {
                this.val$callback.onError(e);
            }
        }

        public void onResponse(okhttp3.Call call, okhttp3.Response response) throws java.io.IOException {
            try {
                int responseCode = response.code();
                if (responseCode != 200 && responseCode != 204) {
                    com.navdy.service.library.network.http.services.JiraClient.sLogger.d("Response data " + response.body().string());
                    if (this.val$callback != null) {
                        this.val$callback.onError(new java.io.IOException("Jira request failed with " + responseCode));
                    }
                } else if (this.val$callback != null) {
                    this.val$callback.onSuccess(this.val$user);
                }
            } catch (Throwable t) {
                com.navdy.service.library.network.http.services.JiraClient.sLogger.e("Exception paring response ", t);
                if (this.val$callback != null) {
                    this.val$callback.onError(t);
                }
            }
        }
    }

    class Anon3 implements okhttp3.Callback {
        final /* synthetic */ com.navdy.service.library.network.http.services.JiraClient.ResultCallback val$callback;

        Anon3(com.navdy.service.library.network.http.services.JiraClient.ResultCallback resultCallback) {
            this.val$callback = resultCallback;
        }

        public void onFailure(okhttp3.Call call, java.io.IOException e) {
            if (this.val$callback != null) {
                this.val$callback.onError(e);
            }
        }

        public void onResponse(okhttp3.Call call, okhttp3.Response response) throws java.io.IOException {
            try {
                int responseCode = response.code();
                if (responseCode == 200 || responseCode == 201) {
                    java.lang.String key = new org.json.JSONObject(response.body().string()).getString("key");
                    if (this.val$callback != null) {
                        this.val$callback.onSuccess(key);
                    }
                } else if (this.val$callback != null) {
                    this.val$callback.onError(new java.io.IOException("Jira request failed with " + responseCode));
                }
            } catch (Throwable t) {
                if (this.val$callback != null) {
                    this.val$callback.onError(t);
                }
            }
        }
    }

    class Anon4 implements okhttp3.Callback {
        final /* synthetic */ com.navdy.service.library.network.http.services.JiraClient.ResultCallback val$callback;
        final /* synthetic */ java.lang.String val$ticketId;

        Anon4(com.navdy.service.library.network.http.services.JiraClient.ResultCallback resultCallback, java.lang.String str) {
            this.val$callback = resultCallback;
            this.val$ticketId = str;
        }

        public void onFailure(okhttp3.Call call, java.io.IOException e) {
            if (this.val$callback != null) {
                this.val$callback.onError(e);
            }
        }

        public void onResponse(okhttp3.Call call, okhttp3.Response response) throws java.io.IOException {
            try {
                int responseCode = response.code();
                if ((responseCode == 200 || responseCode == 201) && this.val$callback != null) {
                    this.val$callback.onSuccess(this.val$ticketId);
                }
            } catch (Throwable t) {
                if (this.val$callback != null) {
                    this.val$callback.onError(t);
                }
            }
        }
    }

    public static class Attachment {
        public java.lang.String filePath = "";
        public java.lang.String mimeType = "";
    }

    public interface ResultCallback {
        void onError(java.lang.Throwable th);

        void onSuccess(java.lang.Object obj);
    }

    public JiraClient(okhttp3.OkHttpClient httpClient) {
        this.mClient = httpClient;
    }

    public void assignTicketForName(java.lang.String ticketId, java.lang.String name, java.lang.String emailAddress, com.navdy.service.library.network.http.services.JiraClient.ResultCallback callback) {
        okhttp3.Request.Builder requestBuilder = new okhttp3.Request.Builder().url("https://navdyhud.atlassian.net/rest/api/2/user/assignable/search?issueKey=" + ticketId + "&username=" + name).get();
        if (!android.text.TextUtils.isEmpty(this.encodedCredentials)) {
            requestBuilder.addHeader("Authorization", "Basic " + this.encodedCredentials);
        }
        this.mClient.newCall(requestBuilder.build()).enqueue(new com.navdy.service.library.network.http.services.JiraClient.Anon1(callback, emailAddress, ticketId));
    }

    public void assignTicketToUser(java.lang.String ticketId, java.lang.String user, com.navdy.service.library.network.http.services.JiraClient.ResultCallback callback) {
        try {
            org.json.JSONObject data = new org.json.JSONObject();
            org.json.JSONObject fields = new org.json.JSONObject();
            org.json.JSONObject assignee = new org.json.JSONObject();
            assignee.put("name", user);
            fields.put(ASSIGNEE, assignee);
            data.put("fields", fields);
            okhttp3.Request.Builder requestBuilder = new okhttp3.Request.Builder().url("https://navdyhud.atlassian.net/rest/api/2/issue//" + ticketId).put(com.navdy.service.library.network.http.HttpUtils.getJsonRequestBody(data.toString()));
            if (!android.text.TextUtils.isEmpty(this.encodedCredentials)) {
                requestBuilder.addHeader("Authorization", "Basic " + this.encodedCredentials);
            }
            this.mClient.newCall(requestBuilder.build()).enqueue(new com.navdy.service.library.network.http.services.JiraClient.Anon2(callback, user));
        } catch (Throwable t) {
            callback.onError(t);
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(null);
        }
    }

    public void submitTicket(java.lang.String projectName, java.lang.String issueTypeName, java.lang.String summary, java.lang.String description, com.navdy.service.library.network.http.services.JiraClient.ResultCallback callback) throws java.io.IOException {
        submitTicket(projectName, issueTypeName, summary, description, null, callback);
    }

    public void submitTicket(java.lang.String projectName, java.lang.String issueTypeName, java.lang.String summary, java.lang.String description, java.lang.String environment, com.navdy.service.library.network.http.services.JiraClient.ResultCallback callback) throws java.io.IOException {
        try {
            org.json.JSONObject data = new org.json.JSONObject();
            org.json.JSONObject fields = new org.json.JSONObject();
            org.json.JSONObject project = new org.json.JSONObject();
            org.json.JSONObject issueType = new org.json.JSONObject();
            project.put("key", projectName);
            fields.put("project", project);
            issueType.put("name", issueTypeName);
            fields.put("issuetype", issueType);
            fields.put("summary", summary);
            if (!android.text.TextUtils.isEmpty(environment)) {
                fields.put(ENVIRONMENT, environment);
            }
            fields.put("description", description);
            data.put("fields", fields);
            okhttp3.Request.Builder requestBuilder = new okhttp3.Request.Builder().url("https://navdyhud.atlassian.net/rest/api/2/issue/").post(com.navdy.service.library.network.http.HttpUtils.getJsonRequestBody(data.toString()));
            if (!android.text.TextUtils.isEmpty(this.encodedCredentials)) {
                requestBuilder.addHeader("Authorization", "Basic " + this.encodedCredentials);
            }
            this.mClient.newCall(requestBuilder.build()).enqueue(new com.navdy.service.library.network.http.services.JiraClient.Anon3(callback));
        } catch (Throwable t) {
            callback.onError(t);
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(null);
        }
    }

    public void setEncodedCredentials(java.lang.String encodedCredentials2) {
        this.encodedCredentials = encodedCredentials2;
    }

    public void attachFilesToTicket(java.lang.String ticketId, java.util.List<com.navdy.service.library.network.http.services.JiraClient.Attachment> attachments, com.navdy.service.library.network.http.services.JiraClient.ResultCallback callback) {
        okhttp3.MultipartBody.Builder builder = new okhttp3.MultipartBody.Builder().setType(okhttp3.MultipartBody.FORM);
        for (com.navdy.service.library.network.http.services.JiraClient.Attachment attachment : attachments) {
            java.io.File file = new java.io.File(attachment.filePath);
            if (!file.exists()) {
                throw new java.lang.IllegalArgumentException("Attachment " + file.getName() + "File does not exists");
            } else if (!file.canRead()) {
                throw new java.lang.IllegalArgumentException("Attachment " + file.getName() + "File cannot be read");
            } else {
                builder.addFormDataPart(com.amazonaws.mobileconnectors.s3.transferutility.TransferTable.COLUMN_FILE, file.getName(), okhttp3.RequestBody.create(okhttp3.MediaType.parse(attachment.mimeType), file));
            }
        }
        okhttp3.Request.Builder requestBuilder = new okhttp3.Request.Builder().url("https://navdyhud.atlassian.net/rest/api/2/issue/" + ticketId + JIRA_ATTACHMENTS).addHeader("X-Atlassian-Token", "nocheck").post(builder.build());
        if (!android.text.TextUtils.isEmpty(this.encodedCredentials)) {
            requestBuilder.addHeader("Authorization", "Basic " + this.encodedCredentials);
        }
        this.mClient.newCall(requestBuilder.build()).enqueue(new com.navdy.service.library.network.http.services.JiraClient.Anon4(callback, ticketId));
    }
}
