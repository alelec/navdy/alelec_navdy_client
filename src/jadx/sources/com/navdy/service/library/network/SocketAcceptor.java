package com.navdy.service.library.network;

public interface SocketAcceptor extends java.io.Closeable {
    com.navdy.service.library.network.SocketAdapter accept() throws java.io.IOException;

    void close() throws java.io.IOException;

    com.navdy.service.library.device.connection.ConnectionInfo getRemoteConnectionInfo(com.navdy.service.library.network.SocketAdapter socketAdapter, com.navdy.service.library.device.connection.ConnectionType connectionType);
}
