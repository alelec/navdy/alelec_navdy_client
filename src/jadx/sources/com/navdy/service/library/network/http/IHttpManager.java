package com.navdy.service.library.network.http;

public interface IHttpManager {
    okhttp3.OkHttpClient getClient();

    okhttp3.OkHttpClient.Builder getClientCopy();
}
