package com.navdy.service.library.network;

public class BTSocketAcceptor implements com.navdy.service.library.network.SocketAcceptor {
    private static final boolean needsPatch = (android.os.Build.VERSION.SDK_INT >= 17 && android.os.Build.VERSION.SDK_INT <= 20);
    private static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.network.BTSocketAcceptor.class);
    android.bluetooth.BluetoothServerSocket btServerSocket;
    private com.navdy.service.library.network.BTSocketAdapter internalSocket;
    private final java.lang.String sdpName;
    private final boolean secure;
    private final java.util.UUID serviceUUID;

    public BTSocketAcceptor(java.lang.String sdpName2, java.util.UUID serviceUUID2, boolean secure2) {
        this.sdpName = sdpName2;
        this.serviceUUID = serviceUUID2;
        this.secure = secure2;
    }

    public BTSocketAcceptor(java.lang.String sdpName2, java.util.UUID serviceUUID2) {
        this(sdpName2, serviceUUID2, true);
    }

    public com.navdy.service.library.network.SocketAdapter accept() throws java.io.IOException {
        if (this.btServerSocket == null) {
            android.bluetooth.BluetoothAdapter adapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
            if (adapter == null) {
                throw new java.io.IOException("Bluetooth unavailable");
            }
            if (this.secure) {
                this.btServerSocket = adapter.listenUsingRfcommWithServiceRecord(this.sdpName, this.serviceUUID);
            } else {
                this.btServerSocket = adapter.listenUsingInsecureRfcommWithServiceRecord(this.sdpName, this.serviceUUID);
            }
            if (needsPatch) {
                storeSocket();
            }
        }
        return new com.navdy.service.library.network.BTSocketAdapter(this.btServerSocket.accept());
    }

    public void close() throws java.io.IOException {
        if (this.btServerSocket != null) {
            this.btServerSocket.close();
            if (this.internalSocket != null) {
                this.internalSocket.close();
                this.internalSocket = null;
            }
            this.btServerSocket = null;
        }
    }

    public com.navdy.service.library.device.connection.ConnectionInfo getRemoteConnectionInfo(com.navdy.service.library.network.SocketAdapter socket, com.navdy.service.library.device.connection.ConnectionType connectionType) {
        com.navdy.service.library.device.NavdyDeviceId deviceId = socket.getRemoteDevice();
        if (deviceId != null) {
            return new com.navdy.service.library.device.connection.BTConnectionInfo(deviceId, this.serviceUUID, connectionType);
        }
        return null;
    }

    private void storeSocket() {
        try {
            java.lang.reflect.Field mSocketField = this.btServerSocket.getClass().getDeclaredField("mSocket");
            mSocketField.setAccessible(true);
            this.internalSocket = new com.navdy.service.library.network.BTSocketAdapter((android.bluetooth.BluetoothSocket) mSocketField.get(this.btServerSocket));
        } catch (java.lang.Exception e) {
            sLogger.w("Failed to close server socket", e);
        }
    }
}
