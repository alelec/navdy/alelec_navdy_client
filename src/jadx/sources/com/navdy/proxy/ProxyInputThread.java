package com.navdy.proxy;

public class ProxyInputThread extends java.lang.Thread {
    private static final boolean VERBOSE_DBG = false;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.proxy.ProxyInputThread.class);
    java.io.InputStream mInputStream;
    com.navdy.proxy.ProxyThread mProxyThread;

    public ProxyInputThread(com.navdy.proxy.ProxyThread proxyThread, java.io.InputStream inputStream) {
        this.mProxyThread = proxyThread;
        this.mInputStream = inputStream;
    }

    public void run() {
        super.run();
        byte[] buffer = new byte[1024];
        while (true) {
            try {
                int bytesRead = this.mInputStream.read(buffer);
                if (bytesRead < 0) {
                    break;
                } else if (bytesRead > 0) {
                    this.mProxyThread.receiveBytes(buffer, bytesRead);
                }
            } catch (java.io.IOException e) {
                sLogger.i("mInputStream closed: " + e);
            }
        }
        this.mProxyThread.inputThreadWillFinish();
    }
}
