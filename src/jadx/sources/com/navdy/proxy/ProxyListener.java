package com.navdy.proxy;

public class ProxyListener extends com.navdy.service.library.device.connection.ProxyService {
    public static final int MAX_RETRY_INTERVAL = 16000;
    public static final int MIN_RETRY_INTERVAL = 1000;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.proxy.ProxyListener.class);
    private volatile boolean closing = false;
    private com.navdy.service.library.network.SocketAcceptor mSocketAcceptor;
    private int retryInterval = 1000;

    public ProxyListener(com.navdy.service.library.network.SocketAcceptor acceptor) {
        setName(com.navdy.proxy.ProxyListener.class.getSimpleName());
        this.mSocketAcceptor = acceptor;
    }

    public void run() {
        sLogger.d("start: " + this.mSocketAcceptor);
        while (true) {
            if (this.closing) {
                break;
            }
            try {
                com.navdy.service.library.network.SocketAdapter fromSocket = this.mSocketAcceptor.accept();
                this.retryInterval = 1000;
                sLogger.v("accepted connection (" + fromSocket + ")");
                try {
                    new com.navdy.proxy.ProxyThread(fromSocket).start();
                } catch (java.lang.Exception e) {
                    sLogger.w("Exception while starting proxy", e);
                    com.navdy.service.library.util.IOUtils.closeStream(fromSocket);
                }
            } catch (java.lang.Exception e2) {
                if (!this.closing) {
                    sLogger.e(e2.toString());
                    try {
                        java.lang.Thread.sleep((long) this.retryInterval);
                    } catch (java.lang.InterruptedException e3) {
                    }
                    this.retryInterval *= 2;
                    if (this.retryInterval > 16000) {
                        sLogger.e("Giving up on proxy - assuming that we'll be restarted");
                        break;
                    }
                } else {
                    continue;
                }
            }
        }
        sLogger.d("end: " + this.mSocketAcceptor);
    }

    public void cancel() {
        try {
            this.closing = true;
            this.mSocketAcceptor.close();
        } catch (java.io.IOException e) {
            sLogger.e("error closing socket acceptor: " + e);
        }
    }
}
