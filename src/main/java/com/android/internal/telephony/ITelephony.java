package com.android.internal.telephony;
// https://stackoverflow.com/a/18065415

public interface ITelephony {

    boolean endCall();

    void answerRingingCall();

    void silenceRinger();

}