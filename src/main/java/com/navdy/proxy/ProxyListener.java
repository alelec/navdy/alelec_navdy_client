package com.navdy.proxy;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class ProxyListener extends com.navdy.service.library.device.connection.ProxyService {
    @DexIgnore
    public static /* final */ int MAX_RETRY_INTERVAL; // = 16000;
    @DexIgnore
    public static /* final */ int MIN_RETRY_INTERVAL; // = 1000;
    @DexIgnore
    private static /* final */ com.navdy.service.library.log.Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.proxy.ProxyListener.class);
    @DexIgnore
    private volatile boolean closing; // = false;
    @DexIgnore
    private com.navdy.service.library.network.SocketAcceptor mSocketAcceptor;
    @DexIgnore
    private int retryInterval; // = 1000;

    @DexIgnore
    public ProxyListener(com.navdy.service.library.network.SocketAcceptor acceptor) {
        setName(com.navdy.proxy.ProxyListener.class.getSimpleName());
        this.mSocketAcceptor = acceptor;
    }

    @DexReplace
    public void run() {
        sLogger.d("start: " + this.mSocketAcceptor);
        while (true) {
            if (this.closing) {
                break;
            }
            try {
                com.navdy.service.library.network.SocketAdapter fromSocket = this.mSocketAcceptor.accept();
                this.retryInterval = 1000;
                sLogger.v("accepted connection (" + fromSocket + ")");
                try {
                    new com.navdy.proxy.ProxyThread(fromSocket).start();
                } catch (java.lang.Exception | UnsatisfiedLinkError | NoClassDefFoundError e) {
                    sLogger.w("Exception while starting proxy: " + e.toString());
                    com.navdy.service.library.util.IOUtils.closeStream(fromSocket);
                }
            } catch (java.lang.Exception e2) {
                if (!this.closing) {
                    sLogger.e(e2.toString());
                    try {
                        java.lang.Thread.sleep((long) this.retryInterval);
                    } catch (java.lang.InterruptedException e3) {
                    }
                    this.retryInterval *= 2;
                    if (this.retryInterval > 16000) {
                        sLogger.e("Giving up on proxy - assuming that we'll be restarted");
                        break;
                    }
                } else {
                    continue;
                }
            }
        }
        sLogger.d("end: " + this.mSocketAcceptor);
    }

    @DexIgnore
    public void cancel() {
        try {
            this.closing = true;
            this.mSocketAcceptor.close();
        } catch (java.io.IOException e) {
            sLogger.e("error closing socket acceptor: " + e);
        }
    }
}
