package com.navdy.client.app.ui.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition.Builder;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.navdy.client.R.drawable;
import com.navdy.client.R.layout;
import com.navdy.client.R.string;
import com.navdy.client.R.styleable;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.CredentialsUtils;
import com.navdy.client.app.framework.util.GmsUtils;
import com.navdy.client.app.framework.util.PackageManagerWrapper;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.ui.settings.GeneralSettingsActivity.LimitCellDataEvent;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;

import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexWrap;

@DexEdit(defaultAction = DexAction.IGNORE)
public class BaseGoogleMapFragment extends MapFragment {
    @DexIgnore
    private static /* final */ boolean AUTO_CENTER_DEFAULT; // = false;
    @DexIgnore
    private static /* final */ boolean CAR_LOCATION_ENABLED_DEFAULT; // = true;
    @DexIgnore
    private static /* final */ boolean CENTER_MAP_ON_LAST_KNOWN_LOCATION_ENABLED_DEFAULT; // = true;
    @DexIgnore
    private static /* final */ float CENTER_MAP_ZOOM; // = 16.0f;
    @DexIgnore
    public static /* final */ float KEEP_ZOOM; // = -1.0f;
    @DexIgnore
    private static /* final */ boolean MAP_TOOLBAR_ENABLED_DEFAULT; // = false;
    @DexIgnore
    private static /* final */ double MIN_DISTANCE_CENTER_MAP_BOTH; // = 50.0d;
    @DexIgnore
    private static /* final */ boolean MY_LOCATION_BUTTON_ENABLED_DEFAULT; // = false;
    @DexIgnore
    private static /* final */ boolean MY_LOCATION_ENABLED_DEFAULT; // = true;
    @DexIgnore
    private static /* final */ boolean SCROLL_GESTURES_ENABLED_DEFAULT; // = true;
    @DexIgnore
    private static /* final */ boolean TRAFFIC_ENABLED_DEFAULT; // = true;
    @DexIgnore
    public static /* final */ float ZOOM_DEFAULT; // = 14.0f;
    @DexIgnore
    private static /* final */ Logger logger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.base.BaseGoogleMapFragment.class);
    @DexIgnore
    private /* final */ Queue<BaseGoogleMapFragment.OnGoogleMapFragmentAttached> attachCompleteListeners; // = new java.util.LinkedList();
    @DexIgnore
    private boolean autoCenter;
    @DexIgnore
    private /* final */ BusProvider bus; // = com.navdy.client.app.framework.util.BusProvider.getInstance();
    @DexIgnore
    private boolean carLocationEnabled;
    @DexIgnore
    private Marker carMarker;
    @DexIgnore
    private boolean centerMapOnLastKnownLocation;
    @DexIgnore
    AtomicBoolean isAttached; // = new java.util.concurrent.atomic.AtomicBoolean(false);
    @DexIgnore
    private int mapPaddingBottom;
    @DexIgnore
    private int mapPaddingLeft;
    @DexIgnore
    private int mapPaddingRight;
    @DexIgnore
    private int mapPaddingTop;
    @DexIgnore
    private boolean mapToolbarEnabled;
    @DexIgnore
    private boolean myLocationButtonEnabled;
    @DexIgnore
    private boolean myLocationEnabled;
    @DexIgnore
    private /* final */ OnNavdyLocationChangedListener navdyLocationListener; // = new com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon1();
    @DexIgnore
    private /* final */ NavdyLocationManager navdyLocationManager; // = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance();
    @DexIgnore
    private /* final */ Queue<BaseGoogleMapFragment.OnGoogleMapFragmentReady> readyCompleteListeners; // = new java.util.LinkedList();
    @DexIgnore
    private boolean scrollGesturesEnabled;
    @DexIgnore
    private boolean trafficEnabled;

    @DexIgnore
    class Anon1 implements OnNavdyLocationChangedListener {

        /* renamed from: com.navdy.client.app.ui.base.BaseGoogleMapFragment$Anon1$Anon1 reason: collision with other inner class name */
        // class C0053Anon1 implements OnMapReadyCallback {
        //     C0053Anon1() {
        //     }
        //
        //     public void onMapReady(GoogleMap googleMap) {
        //         try {
        //             if (BaseGoogleMapFragment.this.autoCenter) {
        //                 BaseGoogleMapFragment.this.centerOnLastKnownLocation(true);
        //             }
        //         } catch (Exception e) {
        //             BaseGoogleMapFragment.logger.e((Throwable) e);
        //         }
        //     }
        // }

        @DexIgnore
        Anon1() {
        }

        @DexIgnore
        public void onPhoneLocationChanged(@NonNull Coordinate phoneLocation) {
            // BaseGoogleMapFragment.this.getMapAsync(new Anon1.C0053Anon1());
        }

        @DexIgnore
        public void onCarLocationChanged(@NonNull Coordinate carLocation) {
            BaseGoogleMapFragment.this.updateCarMarker(carLocation);
        }
    }

    @DexIgnore
    class Anon10 implements OnMapReadyCallback {
        @DexIgnore
        Anon10() {
        }

        @DexIgnore
        /* final */ /* synthetic */ Coordinate val$carLocation;

        @DexIgnore
        Anon10(Coordinate coordinate) {
            this.val$carLocation = coordinate;
        }

        @DexIgnore
        public void onMapReady(GoogleMap googleMap) {
            LatLng carLatLng = new LatLng(this.val$carLocation.latitude, this.val$carLocation.longitude);
            if (BaseGoogleMapFragment.this.carMarker != null) {
                BaseGoogleMapFragment.this.carMarker.remove();
            }
            BaseGoogleMapFragment.this.carMarker = googleMap.addMarker(new MarkerOptions().position(carLatLng).title(NavdyApplication.getAppContext().getString(string.car_location)).icon(BitmapDescriptorFactory.fromResource(drawable.icon_pin_car)));
            if (BaseGoogleMapFragment.this.autoCenter) {
                BaseGoogleMapFragment.this.centerOnLastKnownLocation(true);
            }
        }
    }

    @DexIgnore
    class Anon2 implements BaseGoogleMapFragment.OnGoogleMapFragmentReady {
        @DexIgnore
        Anon2() {
        }

        @DexIgnore
        /* final */ /* synthetic */ OnShowMapListener val$onShowMapListener;

        @DexIgnore
        class Anon1 implements OnMapReadyCallback {
            @DexIgnore
            Anon1() {
            }

            @DexIgnore
            public void onMapReady(GoogleMap googleMap) {
                if (BaseGoogleMapFragment.this.centerMapOnLastKnownLocation) {
                    BaseGoogleMapFragment.this.centerOnLastKnownLocation(false);
                    BaseGoogleMapFragment.this.fadeIn();
                    if (Anon2.this.val$onShowMapListener != null) {
                        Anon2.this.val$onShowMapListener.onShow();
                        return;
                    }
                    return;
                }
                BaseGoogleMapFragment.logger.v("do not centerOnLastKnownLocation");
                BaseGoogleMapFragment.this.fadeIn();
                if (Anon2.this.val$onShowMapListener != null) {
                    Anon2.this.val$onShowMapListener.onShow();
                }
            }
        }

        @DexIgnore
        Anon2(OnShowMapListener onShowMapListener) {
            this.val$onShowMapListener = onShowMapListener;
        }

        @DexIgnore
        public void onReady() {
            BaseGoogleMapFragment.this.getMapAsync(new Anon2.Anon1());
        }
    }

    @DexIgnore
    class Anon3 implements OnMapReadyCallback {
        @DexIgnore
        Anon3() {
        }

        @DexIgnore
        /* final */ /* synthetic */ boolean val$animate;
        @DexIgnore
        /* final */ /* synthetic */ LatLngBounds val$latLngBounds;

        @DexIgnore
        class Anon1 implements OnGlobalLayoutListener {
            @DexIgnore
            /* final */ /* synthetic */ View val$container;

            @DexIgnore
            Anon1(View view) {
                this.val$container = view;
            }

            @DexIgnore
            public void onGlobalLayout() {
                int height = this.val$container.getHeight();
                int width = this.val$container.getWidth();
                if (height == 0 || width == 0) {
                    BaseGoogleMapFragment.logger.e("zoomTo, googleMap has no dimensions");
                } else {
                    int padding = 0;
                    int additionalTopDownPadding = (int) (0.15d * ((double) height));
                    boolean additionalTopDownPaddingFitsSides = additionalTopDownPadding < ((width - BaseGoogleMapFragment.this.mapPaddingRight) - BaseGoogleMapFragment.this.mapPaddingLeft) * 2;
                    if (!(additionalTopDownPadding < ((height - BaseGoogleMapFragment.this.mapPaddingTop) - BaseGoogleMapFragment.this.mapPaddingBottom) * 2) || !additionalTopDownPaddingFitsSides) {
                        int additionalSidePadding = (int) (0.1d * ((double) width));
                        boolean additionalSidePaddingFitsSides = additionalSidePadding < ((width - BaseGoogleMapFragment.this.mapPaddingRight) - BaseGoogleMapFragment.this.mapPaddingLeft) * 2;
                        if ((additionalSidePadding < ((height - BaseGoogleMapFragment.this.mapPaddingTop) - BaseGoogleMapFragment.this.mapPaddingBottom) * 2) && additionalSidePaddingFitsSides) {
                            padding = additionalSidePadding;
                        }
                    } else {
                        padding = additionalTopDownPadding;
                    }
                    BaseGoogleMapFragment.this.moveMapInternal(CameraUpdateFactory.newLatLngBounds(Anon3.this.val$latLngBounds, padding), Anon3.this.val$animate);
                }
                this.val$container.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        }

        @DexIgnore
        Anon3(LatLngBounds latLngBounds, boolean z) {
            this.val$latLngBounds = latLngBounds;
            this.val$animate = z;
        }

        @DexIgnore
        public void onMapReady(GoogleMap googleMap) {
            BaseGoogleMapFragment.logger.v("zoomTo, creating new CameraUpdate, map ready? " + (googleMap != null));
            View container = BaseGoogleMapFragment.this.getView();
            if (container == null || !container.getViewTreeObserver().isAlive()) {
                BaseGoogleMapFragment.logger.e("zoomTo, view is null or its viewtreeobserver not alive");
            } else {
                container.getViewTreeObserver().addOnGlobalLayoutListener(new Anon3.Anon1(container));
            }
        }
    }

    @DexIgnore
    class Anon4 implements BaseGoogleMapFragment.OnGoogleMapFragmentAttached {
        @DexIgnore
        Anon4() {
        }

        @DexIgnore
        public void onAttached() {
            if (!BaseGoogleMapFragment.this.isVisible()) {
                BaseGoogleMapFragment.logger.v("hide, already hidden, no-op");
                return;
            }
            BaseGoogleMapFragment.this.getFragmentManager().beginTransaction().hide(BaseGoogleMapFragment.this).commitAllowingStateLoss();
            BaseGoogleMapFragment.this.getFragmentManager().executePendingTransactions();
        }
    }

    @DexIgnore
    class Anon5 implements OnMapReadyCallback {
        @DexIgnore
        Anon5() {
        }

        @DexIgnore
        /* final */ /* synthetic */ LimitCellDataEvent val$event;

        @DexIgnore
        Anon5(LimitCellDataEvent limitCellDataEvent) {
            this.val$event = limitCellDataEvent;
        }

        @DexIgnore
        public void onMapReady(GoogleMap googleMap) {
            googleMap.setTrafficEnabled(!this.val$event.hasLimitedCellData);
        }
    }

    @DexIgnore
    class Anon6 implements OnMapReadyCallback {

        @DexIgnore
        class Anon1 implements Runnable {
            @DexIgnore
            /* final */ /* synthetic */ GoogleMap val$googleMap;

            Anon1(GoogleMap googleMap) {
                this.val$googleMap = googleMap;
            }

            @SuppressLint("MissingPermission")
            @DexIgnore
            public void run() {
                this.val$googleMap.setMyLocationEnabled(true);
            }
        }

        @DexIgnore
        Anon6() {
        }

        @DexIgnore
        public void onMapReady(GoogleMap googleMap) {
            try {
                MapsInitializer.initialize(BaseGoogleMapFragment.this.getContext());
                UiSettings uiSettings = googleMap.getUiSettings();
                uiSettings.setScrollGesturesEnabled(BaseGoogleMapFragment.this.scrollGesturesEnabled);
                uiSettings.setMyLocationButtonEnabled(BaseGoogleMapFragment.this.myLocationButtonEnabled);
                uiSettings.setMapToolbarEnabled(BaseGoogleMapFragment.this.mapToolbarEnabled);
                googleMap.setTrafficEnabled(BaseGoogleMapFragment.this.trafficEnabled && !SettingsUtils.isLimitingCellularData());
                if (BaseGoogleMapFragment.this.myLocationEnabled) {
                    BaseActivity.requestLocationPermission(new Anon6.Anon1(googleMap), null, BaseGoogleMapFragment.this.getActivity());
                }
            } catch (Exception e) {
                BaseGoogleMapFragment.logger.e(e);
            }
        }
    }

    @DexIgnore
    class Anon7 implements OnGlobalLayoutListener {
        @DexIgnore
        Anon7() {
        }

        @DexIgnore
        /* final */ /* synthetic */ View val$finalView;

        @DexIgnore
        class Anon1 implements OnMapReadyCallback {
            @DexIgnore
            Anon1() {
            }

            @DexIgnore
            public void onMapReady(GoogleMap googleMap) {
                googleMap.setPadding(BaseGoogleMapFragment.this.mapPaddingLeft, BaseGoogleMapFragment.this.mapPaddingTop, BaseGoogleMapFragment.this.mapPaddingRight, BaseGoogleMapFragment.this.mapPaddingBottom);
            }
        }

        @DexIgnore
        Anon7(View view) {
            this.val$finalView = view;
        }

        @DexIgnore
        public void onGlobalLayout() {
            boolean paddingFitsSides;
            boolean paddingFitsTopDown;
            int width = this.val$finalView.getWidth();
            int height = this.val$finalView.getHeight();
            paddingFitsSides = ((double) width) > ((double) (BaseGoogleMapFragment.this.mapPaddingLeft + BaseGoogleMapFragment.this.mapPaddingRight)) * 1.5d;
            paddingFitsTopDown = ((double) height) > ((double) (BaseGoogleMapFragment.this.mapPaddingTop + BaseGoogleMapFragment.this.mapPaddingBottom)) * 1.5d;
            if (paddingFitsSides && !paddingFitsTopDown) {
                BaseGoogleMapFragment.this.mapPaddingTop = 0;
                BaseGoogleMapFragment.this.mapPaddingBottom = 0;
            } else if (paddingFitsTopDown && !paddingFitsSides) {
                BaseGoogleMapFragment.this.mapPaddingRight = 0;
                BaseGoogleMapFragment.this.mapPaddingLeft = 0;
            }
            BaseGoogleMapFragment.this.getMapAsync(new Anon7.Anon1());
            this.val$finalView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
    }

    @DexIgnore
    class Anon8 implements BaseGoogleMapFragment.OnGoogleMapFragmentAttached {
        @DexIgnore
        Anon8() {
        }

        @SuppressLint("ResourceType")
        @DexIgnore
        public void onAttached() {
            BaseGoogleMapFragment.this.getFragmentManager().beginTransaction().setCustomAnimations(17498112, 17498113).show(BaseGoogleMapFragment.this).commitAllowingStateLoss();
        }
    }

    @DexIgnore
    class Anon9 implements OnMapReadyCallback {
        @DexIgnore
        Anon9() {
        }

        @DexIgnore
        /* final */ /* synthetic */ boolean val$animate;
        @DexIgnore
        /* final */ /* synthetic */ CameraUpdate val$cameraUpdate;

        @DexIgnore
        Anon9(boolean z, CameraUpdate cameraUpdate) {
            this.val$animate = z;
            this.val$cameraUpdate = cameraUpdate;
        }

        @DexIgnore
        public void onMapReady(GoogleMap googleMap) {
            try {
                if (this.val$animate) {
                    googleMap.animateCamera(this.val$cameraUpdate);
                } else {
                    googleMap.moveCamera(this.val$cameraUpdate);
                }
            } catch (Exception e) {
                BaseGoogleMapFragment.logger.e("moveMapInternal: Unable to move the map", e);
            }
        }
    }

    @DexIgnore
    private interface OnGoogleMapFragmentAttached {
        void onAttached();
    }

    @DexIgnore
    private interface OnGoogleMapFragmentReady {
        void onReady();
    }

    @DexIgnore
    public interface OnShowMapListener {
        void onShow();
    }

    /* @DexIgnore
    static {
        com.google.android.gms.maps.MapsInitializer.initialize(com.navdy.client.app.NavdyApplication.getAppContext());
    } */

    @DexIgnore
    public void show(@Nullable OnShowMapListener onShowMapListener) {
        logger.v("show");
        whenReady(new Anon2(onShowMapListener));
    }

    @MainThread
    @DexIgnore
    public void centerOnLastKnownLocation(boolean animate) {
        SystemUtils.ensureOnMainThread();
        centerMap(animate, this.navdyLocationManager.getPhoneCoordinates(), this.navdyLocationManager.getCarCoordinates());
    }

    @MainThread
    @DexIgnore
    public void moveMap(Location location, float zoom, boolean animate) {
        SystemUtils.ensureOnMainThread();
        moveMap(new LatLng(location.getLatitude(), location.getLongitude()), zoom, animate);
    }

    @MainThread
    @lanchon.dexpatcher.annotation.DexReplace
    public void moveMap(LatLng latLng, float zoom, boolean animate) {
        SystemUtils.ensureOnMainThread();
        try {
            moveMapInternal(buildCameraUpdate(latLng, zoom), animate);
        } catch (NullPointerException ex) {
            logger.e("Couldn't move map", ex);
        }
    }

    @MainThread
    @DexIgnore
    public void zoomTo(LatLngBounds latLngBounds, boolean animate) {
        SystemUtils.ensureOnMainThread();
        getMapAsync(new Anon3(latLngBounds, animate));
    }

    @DexIgnore
    public void hide() {
        whenAttached(new Anon4());
    }

    @Subscribe
    @DexIgnore
    public void onLimitCellDataEvent(LimitCellDataEvent event) {
        getMapAsync(new Anon5(event));
    }

    @DexIgnore
    public void onAttach(Context context) {
        super.onAttach(context);
        this.bus.register(this);
        this.isAttached.set(true);
        while (!this.attachCompleteListeners.isEmpty()) {
            this.attachCompleteListeners.poll().onAttached();
        }
    }

    // @DexIgnore
    // public void onInflate(Activity activity, AttributeSet attrs, Bundle savedInstanceState) {
    //     logger.v("onInflate");
    //     if (GmsUtils.finishIfGmsIsNotUpToDate(activity)) {
    //         logger.e("GMS is out of date or missing, preventing inflation");
    //         return;
    //     }
    //     super.onInflate(activity, attrs, savedInstanceState);
    //     TypedArray viewStyles = activity.obtainStyledAttributes(attrs, styleable.GoogleMapFragment);
    //     this.myLocationEnabled = viewStyles.getBoolean(4, true);
    //     this.myLocationButtonEnabled = viewStyles.getBoolean(5, false);
    //     this.carLocationEnabled = viewStyles.getBoolean(6, true);
    //     this.scrollGesturesEnabled = viewStyles.getBoolean(7, true);
    //     this.centerMapOnLastKnownLocation = viewStyles.getBoolean(8, true);
    //     this.trafficEnabled = viewStyles.getBoolean(9, true);
    //     this.mapToolbarEnabled = viewStyles.getBoolean(10, false);
    //     this.autoCenter = viewStyles.getBoolean(11, false);
    //     this.mapPaddingTop = viewStyles.getDimensionPixelSize(0, 0);
    //     this.mapPaddingBottom = viewStyles.getDimensionPixelSize(1, 0);
    //     this.mapPaddingLeft = viewStyles.getDimensionPixelSize(2, 0);
    //     this.mapPaddingRight = viewStyles.getDimensionPixelSize(3, 0);
    //     viewStyles.recycle();
    // }

    @SuppressLint("MissingSuperCall")
    @DexWrap
    public void onInflate(android.app.Activity activity, android.util.AttributeSet attrs, android.os.Bundle savedInstanceState) {
        try {
            PackageManagerWrapper.API_KEY = CredentialsUtils.getCredentials("com.google.android.geo.GOOGLE_API_KEY");
            NavdyApplication.inject_api_key_for = Thread.currentThread();

            onInflate(activity, attrs, savedInstanceState);
        } finally {
            NavdyApplication.inject_api_key_for = null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        logger.v("onCreateView");
        getMapAsync(new Anon6());
        try {
            View view = super.onCreateView(inflater, container, savedInstanceState);
            if (view == null || !view.getViewTreeObserver().isAlive()) {
                logger.e("container view is null or its viewtreeobserver is not alive");
                return view;
            }
            view.getViewTreeObserver().addOnGlobalLayoutListener(new Anon7(view));
            return view;
        } catch (Throwable t) {
            logger.e("could not inflate Google Map Fragment", t);
            return inflater.inflate(layout.error_loading_google_maps, container);
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        while (!this.readyCompleteListeners.isEmpty()) {
            this.readyCompleteListeners.poll().onReady();
        }
        if (this.centerMapOnLastKnownLocation) {
            centerOnLastKnownLocation(false);
        }
        if (this.carLocationEnabled && this.navdyLocationManager.getCarCoordinates() != null) {
            updateCarMarker(this.navdyLocationManager.getCarCoordinates());
        }
        if (this.myLocationEnabled || this.carLocationEnabled) {
            this.navdyLocationManager.addListener(this.navdyLocationListener);
        }
    }

    @DexIgnore
    public void onPause() {
        if (this.myLocationEnabled || this.carLocationEnabled) {
            this.navdyLocationManager.removeListener(this.navdyLocationListener);
        }
        super.onPause();
    }

    @DexIgnore
    public void onDetach() {
        if (this.isAttached.get()) {
            this.bus.unregister(this);
            this.isAttached.set(false);
        }
        super.onDetach();
    }

    @DexIgnore
    public Context getContext() {
        return NavdyApplication.getAppContext();
    }

    @MainThread
    @DexIgnore
    private void whenAttached(BaseGoogleMapFragment.OnGoogleMapFragmentAttached attachListener) {
        if (attachListener == null) {
            logger.w("tried to add a null attachListener");
            return;
        }
        SystemUtils.ensureOnMainThread();
        if (isAdded()) {
            attachListener.onAttached();
        } else if (isRemoving()) {
            logger.v("whenAttached, isRemoving, no-op");
        } else {
            this.attachCompleteListeners.add(attachListener);
        }
    }

    @MainThread
    @DexIgnore
    private void whenReady(BaseGoogleMapFragment.OnGoogleMapFragmentReady readyListener) {
        if (readyListener == null) {
            logger.w("tried to add a null readyListener");
            return;
        }
        SystemUtils.ensureOnMainThread();
        if (isResumed()) {
            readyListener.onReady();
        } else if (isRemoving()) {
            logger.v("whenAttached, isRemoving, no-op");
        } else {
            this.readyCompleteListeners.add(readyListener);
        }
    }

    @DexIgnore
    private void fadeIn() {
        whenAttached(new Anon8());
    }

    @DexIgnore
    private void centerMap(boolean animate, @Nullable Coordinate phoneLocation, @Nullable Coordinate carLocation) {
        if (!MapUtils.isValidSetOfCoordinates(phoneLocation) || !MapUtils.isValidSetOfCoordinates(carLocation)) {
            if (MapUtils.isValidSetOfCoordinates(phoneLocation)) {
                moveMap(new LatLng(phoneLocation.latitude, phoneLocation.longitude), 16.0f, animate);
            } else if (MapUtils.isValidSetOfCoordinates(carLocation)) {
                moveMap(new LatLng(carLocation.latitude, carLocation.longitude), 16.0f, animate);
            }
        } else if (MapUtils.distanceBetween(phoneLocation, carLocation) > MIN_DISTANCE_CENTER_MAP_BOTH) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(new LatLng(phoneLocation.latitude, phoneLocation.longitude));
            builder.include(new LatLng(carLocation.latitude, carLocation.longitude));
            zoomTo(builder.build(), animate);
        } else {
            moveMap(new LatLng(phoneLocation.latitude, phoneLocation.longitude), 16.0f, animate);
        }
    }

    @DexIgnore
    private void moveMapInternal(@NonNull CameraUpdate cameraUpdate, boolean animate) {
        getMapAsync(new Anon9(animate, cameraUpdate));
    }

    @DexIgnore
    private LatLng getTargetSafely(GoogleMap googleMap) {
        try {
            return googleMap.getCameraPosition().target;
        } catch (RuntimeException e) {
            logger.e("Unable to get camera target because: " + e.getCause());
            return null;
        }
    }

    @DexIgnore
    private LatLngBounds getLatLongBoundsSafely(GoogleMap googleMap) {
        try {
            return googleMap.getProjection().getVisibleRegion().latLngBounds;
        } catch (RuntimeException e) {
            logger.e("Unable to get lat long bounds because: " + e.getCause());
            return null;
        }
    }

    @DexIgnore
    private CameraUpdate buildCameraUpdate(@NonNull LatLng latLng, float zoom) {
        Builder builder = new Builder().target(latLng);
        if (zoom != -1.0f) {
            builder.zoom(zoom);
        }
        return CameraUpdateFactory.newCameraPosition(builder.build());
    }

    @DexIgnore
    private void updateCarMarker(@NonNull Coordinate carLocation) {
        if (this.carLocationEnabled) {
            getMapAsync(new Anon10(carLocation));
        }
    }
}
