package com.navdy.client.app.ui.settings;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.navdy.client.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.ImageUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants;
import com.navdy.client.app.ui.base.BaseEditActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.homescreen.HomescreenActivity;
import com.navdy.client.debug.util.S3Constants;
import com.navdy.client.ota.OTAUpdateListener;
import com.navdy.client.ota.OTAUpdateService;
import com.navdy.client.ota.OTAUpdateServiceInterface;
import com.navdy.client.ota.OTAUpdateUIClient;
import com.navdy.client.ota.model.UpdateInfo;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import com.navdy.service.library.util.SystemUtils;

import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class OtaSettingsActivity extends BaseEditActivity implements ServiceConnection, OTAUpdateUIClient {
    @DexIgnore
    public static /* final */ String ARGUMENT_CONFIRMATION_MESSAGE; // = "message_confirmation";
    @DexIgnore
    public static /* final */ int DIALOG_DOWNLOAD_UNSTABLE_CONFIRMATION; // = 2;
    @DexIgnore
    public static /* final */ int DIALOG_DOWNLOAD_USING_MOBILE_DATA; // = 1;
    @DexIgnore
    public static /* final */ int DIALOG_ERROR; // = 3;
    @DexIgnore
    private static /* final */ int MAX_LINES_FOR_HIDDEN_DESCRIPTION; // = 2;
    @DexIgnore
    private static /* final */ double SCREEN_PERCENTAGE_FOR_IMAGE; // = 0.35d;
    @DexIgnore
    private static /* final */ int START_INDEX_OF_HEX_COLOR; // = 3;
    @DexIgnore
    private TextView appVersion;
    @DexIgnore
    private S3Constants.BuildType buildType;
    @DexIgnore
    private Switch buildTypeUnstableSwitch;
    @DexIgnore
    private Button button;
    @DexIgnore
    private TextView description;
    @DexIgnore
    private TextView downloadVersion;
    @DexIgnore
    private TextView hardwareSupportTitle;
    @DexIgnore
    private TextView hudVersion;
    @DexIgnore
    private ImageView image;
    @DexIgnore
    private AtomicBoolean isBound; // = new java.util.concurrent.atomic.AtomicBoolean(false);
    @DexIgnore
    private TextView moreOrLess;
    @DexIgnore
    private OTAUpdateServiceInterface otaUpdateService;
    @DexIgnore
    private ProgressBar progressBar;
    @DexIgnore
    private ProgressBar progressBar2;
    @DexIgnore
    private RelativeLayout progressBarContainer;
    @DexIgnore
    private TextView progressData;
    @DexIgnore
    private TextView progressPercentage;
    @DexIgnore
    private SharedPreferences sharedPrefs;
    @DexIgnore
    private boolean showingMoreInfo; // = false;
    @DexIgnore
    private TextView subtitle;
    @DexIgnore
    private TextView subtitle2;
    @DexIgnore
    private TextView title;
    @DexIgnore
    private boolean viewsInitialized;

    // @DexIgnore
    // class Anon1 implements Runnable {
    //     final /* synthetic */ OTAUpdateUIClient.Error val$error;
    //
    //     @DexIgnore
    //     Anon1(OTAUpdateUIClient.Error error) {
    //         this.val$error = error;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         if (OtaSettingsActivity.this.isInForeground()) {
    //             OtaSettingsActivity.this.setUIState(false, OtaSettingsActivity.this.getState(), OtaSettingsActivity.this.getUpdateInfo());
    //             String message = null;
    //             String title = OtaSettingsActivity.this.getString(R.string.error);
    //             switch (OtaSettingsActivity.Anon9.$SwitchMap$com$navdy$client$ota$OTAUpdateUIClient$Error[this.val$error.ordinal()]) {
    //                 case 1:
    //                     if (!com.navdy.client.app.framework.util.SystemUtils.isAirplaneModeOn(NavdyApplication.getAppContext())) {
    //                         message = OtaSettingsActivity.this.getString(R.string.no_connectivity);
    //                         break;
    //                     } else {
    //                         message = OtaSettingsActivity.this.getString(R.string.air_plane_mode);
    //                         break;
    //                     }
    //                 case 2:
    //                     message = OtaSettingsActivity.this.getString(R.string.server_error);
    //                     break;
    //             }
    //             OtaSettingsActivity.this.showSimpleDialog(3, title, message);
    //         }
    //     }
    // }

    // @DexIgnore
    // class Anon2 implements Runnable {
    //     final /* synthetic */ OTAUpdateService.State val$state;
    //     final /* synthetic */ UpdateInfo val$updateInfo;
    //
    //     @DexIgnore
    //     Anon2(OTAUpdateService.State state, UpdateInfo updateInfo) {
    //         this.val$state = state;
    //         this.val$updateInfo = updateInfo;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         if (OtaSettingsActivity.this.isInForeground()) {
    //             OtaSettingsActivity.this.setUIState(false, this.val$state, this.val$updateInfo);
    //         }
    //     }
    // }

    // @DexIgnore
    // class Anon3 implements Runnable {
    //     final /* synthetic */ byte val$percent;
    //     final /* synthetic */ OTAUpdateListener.DownloadUpdateStatus val$progress;
    //     final /* synthetic */ long val$totalDownloaded;
    //
    //     @DexIgnore
    //     Anon3(byte b, OTAUpdateListener.DownloadUpdateStatus downloadUpdateStatus, long j) {
    //         this.val$percent = b;
    //         this.val$progress = downloadUpdateStatus;
    //         this.val$totalDownloaded = j;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         if (OtaSettingsActivity.this.isInForeground()) {
    //             String title = OtaSettingsActivity.this.getString(R.string.error);
    //             switch (OtaSettingsActivity.Anon9.$SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[this.val$progress.ordinal()]) {
    //                 case 1:
    //                     OtaSettingsActivity.this.showSimpleDialog(3, title, OtaSettingsActivity.this.getString(R.string.download_failed));
    //                     return;
    //                 case 2:
    //                     if (com.navdy.client.app.framework.util.SystemUtils.isAirplaneModeOn(NavdyApplication.getAppContext())) {
    //                         OtaSettingsActivity.this.showSimpleDialog(3, title, OtaSettingsActivity.this.getString(R.string.air_plane_mode));
    //                         return;
    //                     }
    //                     OtaSettingsActivity.this.showSimpleDialog(3, title, OtaSettingsActivity.this.getString(R.string.no_connectivity));
    //                     return;
    //                 case 3:
    //                     OtaSettingsActivity.this.showSimpleDialog(3, title, OtaSettingsActivity.this.getString(R.string.not_enough_space));
    //                     return;
    //                 case 4:
    //                     OtaSettingsActivity.this.setDownloadProgressPercentage(this.val$percent, this.val$totalDownloaded);
    //                     return;
    //                 case 5:
    //                     OtaSettingsActivity.this.setDownloadProgressPercentage(100, this.val$totalDownloaded);
    //                     return;
    //                 default:
    //                     OtaSettingsActivity.this.logger.e("Unsupported progress: " + this.val$progress);
    //                     return;
    //             }
    //         }
    //     }
    // }

    // @DexIgnore
    // class Anon4 implements Runnable {
    //     final /* synthetic */ byte val$percent;
    //     final /* synthetic */ OTAUpdateListener.UploadToHUDStatus val$progress;
    //     final /* synthetic */ long val$totalUploaded;
    //
    //     @DexIgnore
    //     Anon4(OTAUpdateListener.UploadToHUDStatus uploadToHUDStatus, byte b, long j) {
    //         this.val$progress = uploadToHUDStatus;
    //         this.val$percent = b;
    //         this.val$totalUploaded = j;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         if (OtaSettingsActivity.this.isInForeground()) {
    //             String title = OtaSettingsActivity.this.getString(R.string.error);
    //             switch (OtaSettingsActivity.Anon9.$SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus[this.val$progress.ordinal()]) {
    //                 case 1:
    //                     OtaSettingsActivity.this.showSimpleDialog(3, title, OtaSettingsActivity.this.getString(R.string.upload_to_device_failed));
    //                     return;
    //                 case 2:
    //                     OtaSettingsActivity.this.showSimpleDialog(3, title, OtaSettingsActivity.this.getString(R.string.not_connected_to_device));
    //                     return;
    //                 case 3:
    //                     OtaSettingsActivity.this.setUploadProgressPercentage(this.val$percent, this.val$totalUploaded);
    //                     return;
    //                 case 4:
    //                     OtaSettingsActivity.this.setUploadProgressPercentage(100, this.val$totalUploaded);
    //                     return;
    //                 default:
    //                     return;
    //             }
    //         }
    //     }
    // }

    // @DexIgnore
    // class Anon5 implements Runnable {
    //     @DexIgnore
    //     Anon5() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         OtaSettingsActivity.this.setupView();
    //     }
    // }
    //
    // @DexIgnore
    // class Anon6 implements DialogInterface.OnClickListener {
    //     @DexIgnore
    //     Anon6() {
    //     }
    //
    //     @DexIgnore
    //     public void onClick(DialogInterface dialog, int which) {
    //         OtaSettingsActivity.this.logger.d("Download on LTE approved");
    //         OtaSettingsActivity.this.otaUpdateService.setNetworkDownloadApproval(true);
    //         OtaSettingsActivity.this.otaUpdateService.downloadOTAUpdate();
    //     }
    // }
    //
    // @DexIgnore
    // class Anon7 implements DialogInterface.OnClickListener {
    //     @DexIgnore
    //     Anon7() {
    //     }
    //
    //     @DexIgnore
    //     public void onClick(DialogInterface dialog, int which) {
    //         OtaSettingsActivity.this.logger.d("Download on LTE cancelled");
    //     }
    // }

    // @DexIgnore
    // class Anon8 implements CompoundButton.OnCheckedChangeListener {
    //
    //     class Anon1 implements DialogInterface.OnClickListener {
    //         final /* synthetic */ OTAUpdateService.State val$state;
    //
    //         Anon1(OTAUpdateService.State state) {
    //             this.val$state = state;
    //         }
    //
    //         public void onClick(DialogInterface dialogInterface, int isPositive) {
    //             if (isPositive == -1) {
    //                 OtaSettingsActivity.this.setUnstableSource(true);
    //                 SettingsUtils.setFeatureMode(DriverProfilePreferences.FeatureMode.FEATURE_MODE_BETA);
    //                 OtaSettingsActivity.this.cancelAndRecheckForUpdate(this.val$state);
    //                 return;
    //             }
    //             OtaSettingsActivity.this.setUnstableSource(false);
    //         }
    //     }
    //
    //     @DexIgnore
    //     Anon8() {
    //     }
    //
    //     @DexIgnore
    //     public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
    //         OtaSettingsActivity.this.somethingChanged = true;
    //         OTAUpdateService.State state = OtaSettingsActivity.this.getState();
    //         OtaSettingsActivity.sLogger.d("State of the update process :" + state.name());
    //         if (!isChecked) {
    //             OtaSettingsActivity.this.setUnstableSource(false);
    //             SettingsUtils.setFeatureMode(DriverProfilePreferences.FeatureMode.FEATURE_MODE_RELEASE);
    //             OtaSettingsActivity.this.cancelAndRecheckForUpdate(state);
    //             return;
    //         }
    //         int confirmationMessage = R.string.beta_disclaimer;
    //         switch (OtaSettingsActivity.Anon9.$SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType[OtaSettingsActivity.this.buildType.ordinal()]) {
    //             case 1:
    //                 confirmationMessage = R.string.beta_disclaimer;
    //                 break;
    //             case 2:
    //                 confirmationMessage = R.string.unstable_disclaimer;
    //                 break;
    //         }
    //         OtaSettingsActivity.this.showQuestionDialog(R.string.warning, confirmationMessage, R.string.ok, R.string.cancel, new OtaSettingsActivity.Anon8.Anon1(state), null);
    //     }
    // }

    // @DexIgnore
    // static /* synthetic */ class Anon9 {
    //     static final /* synthetic */ int[] $SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType = new int[S3Constants.BuildType.values().length];
    //     static final /* synthetic */ int[] $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus = new int[OTAUpdateListener.DownloadUpdateStatus.values().length];
    //     static final /* synthetic */ int[] $SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus = new int[OTAUpdateListener.UploadToHUDStatus.values().length];
    //     static final /* synthetic */ int[] $SwitchMap$com$navdy$client$ota$OTAUpdateUIClient$Error = new int[OTAUpdateUIClient.Error.values().length];
    //
    //     static {
    //         try {
    //             $SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType[S3Constants.BuildType.user.ordinal()] = 1;
    //         } catch (NoSuchFieldError e) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType[S3Constants.BuildType.eng.ordinal()] = 2;
    //         } catch (NoSuchFieldError e2) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus[OTAUpdateListener.UploadToHUDStatus.UPLOAD_FAILED.ordinal()] = 1;
    //         } catch (NoSuchFieldError e3) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus[OTAUpdateListener.UploadToHUDStatus.DEVICE_NOT_CONNECTED.ordinal()] = 2;
    //         } catch (NoSuchFieldError e4) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus[OTAUpdateListener.UploadToHUDStatus.UPLOADING.ordinal()] = 3;
    //         } catch (NoSuchFieldError e5) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus[OTAUpdateListener.UploadToHUDStatus.COMPLETED.ordinal()] = 4;
    //         } catch (NoSuchFieldError e6) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[OTAUpdateListener.DownloadUpdateStatus.DOWNLOAD_FAILED.ordinal()] = 1;
    //         } catch (NoSuchFieldError e7) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[OTAUpdateListener.DownloadUpdateStatus.NO_CONNECTIVITY.ordinal()] = 2;
    //         } catch (NoSuchFieldError e8) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[OTAUpdateListener.DownloadUpdateStatus.NOT_ENOUGH_SPACE.ordinal()] = 3;
    //         } catch (NoSuchFieldError e9) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[OTAUpdateListener.DownloadUpdateStatus.DOWNLOADING.ordinal()] = 4;
    //         } catch (NoSuchFieldError e10) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[OTAUpdateListener.DownloadUpdateStatus.COMPLETED.ordinal()] = 5;
    //         } catch (NoSuchFieldError e11) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateUIClient$Error[OTAUpdateUIClient.Error.NO_CONNECTIVITY.ordinal()] = 1;
    //         } catch (NoSuchFieldError e12) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateUIClient$Error[OTAUpdateUIClient.Error.SERVER_ERROR.ordinal()] = 2;
    //         } catch (NoSuchFieldError e13) {
    //         }
    //         $SwitchMap$com$navdy$client$ota$OTAUpdateService$State = new int[OTAUpdateService.State.values().length];
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[OTAUpdateService.State.UP_TO_DATE.ordinal()] = 1;
    //         } catch (NoSuchFieldError e14) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[OTAUpdateService.State.UPDATE_AVAILABLE.ordinal()] = 2;
    //         } catch (NoSuchFieldError e15) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[OTAUpdateService.State.DOWNLOADING_UPDATE.ordinal()] = 3;
    //         } catch (NoSuchFieldError e16) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[OTAUpdateService.State.READY_TO_UPLOAD.ordinal()] = 4;
    //         } catch (NoSuchFieldError e17) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[OTAUpdateService.State.UPLOADING.ordinal()] = 5;
    //         } catch (NoSuchFieldError e18) {
    //         }
    //         try {
    //             $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[OTAUpdateService.State.READY_TO_INSTALL.ordinal()] = 6;
    //         } catch (NoSuchFieldError e19) {
    //         }
    //     }
    // }

    @DexIgnore
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_ota);
        new BaseToolbarActivity.ToolbarBuilder().title(R.string.menu_navdy_display).build();
        this.title = (TextView) findViewById(R.id.title);
        this.subtitle = (TextView) findViewById(R.id.subtitle);
        this.subtitle2 = (TextView) findViewById(R.id.subtitle2);
        this.image = (ImageView) findViewById(R.id.image);
        this.progressBarContainer = (RelativeLayout) findViewById(R.id.progress_container);
        this.progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        this.progressBar2 = (ProgressBar) findViewById(R.id.progress_bar2);
        this.progressPercentage = (TextView) findViewById(R.id.progress_percentage);
        this.progressData = (TextView) findViewById(R.id.progress_data);
        this.button = (Button) findViewById(R.id.button);
        this.description = (TextView) findViewById(R.id.description);
        this.moreOrLess = (TextView) findViewById(R.id.more_or_less);
        this.hudVersion = (TextView) findViewById(R.id.hud_version);
        this.appVersion = (TextView) findViewById(R.id.app_version);
        this.buildTypeUnstableSwitch = (Switch) findViewById(R.id.build_type);
        this.hardwareSupportTitle = (TextView) findViewById(R.id.release_notes_title);
        this.downloadVersion = (TextView) findViewById(R.id.download_version);
        if (this.title == null || this.subtitle == null || this.subtitle2 == null || this.image == null || this.downloadVersion == null || this.progressBarContainer == null || this.progressBar == null || this.progressBar2 == null || this.progressPercentage == null || this.progressData == null || this.button == null || this.description == null || this.moreOrLess == null || this.hudVersion == null || this.appVersion == null || this.buildTypeUnstableSwitch == null) {
            throw new Resources.NotFoundException("Layout element");
        }
        this.sharedPrefs = SettingsUtils.getSharedPreferences();
        setBuildSources();
        this.appVersion.setText(HomescreenActivity.getAppVersionString());
        getIntent().putExtra(OTAUpdateService.EXTRA_OTA_UPDATE_UI, false);
        this.viewsInitialized = true;
        if (this.otaUpdateService != null) {
            setupView();
        }
    }

    @DexIgnore
    protected void onResume() {
        super.onResume();
        connectToService();
        Tracker.tagScreen(TrackerConstants.Screen.Settings.UPDATE);
        adjustImageContainerHeight();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        if (this.otaUpdateService != null) {
            this.otaUpdateService.unregisterUIClient();
            disconnectService();
        }
    }

    @DexReplace
    public void onButtonClick(View view) {
        if (this.otaUpdateService == null) {
            this.logger.e("OTA Update Service not available");
            return;
        }
        switch (getState()) {
            case UP_TO_DATE:
                this.otaUpdateService.checkForUpdate();
                return;
            case UPDATE_AVAILABLE:
                startDownload();
                return;
            case DOWNLOADING_UPDATE:
                this.otaUpdateService.cancelDownload();
                this.otaUpdateService.toggleAutoDownload(false);
                this.otaUpdateService.checkForUpdate();
                return;
            case READY_TO_UPLOAD:
            case UPLOADING:
                this.otaUpdateService.cancelUpload();
                this.otaUpdateService.toggleAutoDownload(false);
                this.otaUpdateService.resetUpdate();
                this.otaUpdateService.checkForUpdate();
                return;
            case READY_TO_INSTALL:
                this.otaUpdateService.resetUpdate();
                this.otaUpdateService.checkForUpdate();
                return;
            default:
                this.logger.e("Unsupported state for a button click: " + getState());
        }
    }

    @DexIgnore
    public void onMoreInfoClick(View view) {
        this.showingMoreInfo = !this.showingMoreInfo;
        showReleaseNotes(this.showingMoreInfo);
    }

    @DexIgnore
    private void connectToService() {
        Context context = getApplicationContext();
        Intent serviceIntent = OTAUpdateService.getServiceIntent(context);
        if (context != null) {
            this.isBound.set(context.bindService(serviceIntent, this, Context.BIND_AUTO_CREATE));
        }
    }

    @DexIgnore
    private void disconnectService() {
        Context context = getApplicationContext();
        if (context != null && this.isBound.get()) {
            try {
                context.unbindService(this);
            } catch (Exception e) {
                this.logger.e("Unable to unbind from OTA Service.", e);
            }
        }
    }

    @DexIgnore
    public void onErrorCheckingForUpdate(OTAUpdateUIClient.Error error) {
        // this.handler.post(new OtaSettingsActivity.Anon1(error));
    }

    @DexIgnore
    public void onStateChanged(OTAUpdateService.State state, UpdateInfo updateInfo) {
        // this.handler.post(new OtaSettingsActivity.Anon2(state, updateInfo));
    }

    @DexIgnore
    public void onDownloadProgress(OTAUpdateListener.DownloadUpdateStatus progress, long totalDownloaded, byte percent) {
        // this.handler.post(new OtaSettingsActivity.Anon3(percent, progress, totalDownloaded));
    }

    @DexIgnore
    public void onUploadProgress(OTAUpdateListener.UploadToHUDStatus progress, long totalUploaded, byte percent) {
        // this.handler.post(new OtaSettingsActivity.Anon4(progress, percent, totalUploaded));
    }

    @DexIgnore
    private void setDownloadProgressPercentage(int percentage, long totalDownloaded) {
        setThisProgressPercentage(this.progressBar, percentage, totalDownloaded);
        this.progressBar2.setProgress(1);
    }

    @DexIgnore
    private void setUploadProgressPercentage(int percentage, long totalUploaded) {
        if (percentage < 1) {
            percentage = 1;
        }
        setThisProgressPercentage(this.progressBar2, percentage, totalUploaded);
        this.progressBar.setProgress(100);
    }

    @DexIgnore
    private void setThisProgressPercentage(ProgressBar progressBar3, int percentage, long doneSoFar) {
        progressBar3.setProgress(percentage);
        this.progressPercentage.setText(String.format(Locale.getDefault(), "%1$d%%", percentage));
        UpdateInfo updateInfo = getUpdateInfo();
        if (updateInfo != null) {
            Context applicationContext = getApplicationContext();
            this.progressData.setText(String.format("%1$s / %2$s", Formatter.formatShortFileSize(applicationContext, doneSoFar), Formatter.formatShortFileSize(applicationContext, updateInfo.size)));
        }
    }

    @DexIgnore
    public void onServiceConnected(ComponentName name, IBinder service) {
        // if (service != null) {
        //     this.otaUpdateService = (OTAUpdateServiceInterface) service;
        //     this.otaUpdateService.registerUIClient(this);
        //     if (!this.otaUpdateService.isCheckingForUpdate()) {
        //         OTAUpdateService.State state = this.otaUpdateService.getOTAUpdateState();
        //         if (!(state == OTAUpdateService.State.UPLOADING || state == OTAUpdateService.State.DOWNLOADING_UPDATE)) {
        //             this.otaUpdateService.checkForUpdate();
        //         }
        //     }
        //     if (this.viewsInitialized) {
        //         this.handler.post(new OtaSettingsActivity.Anon5());
        //     }
        // }
    }

    @DexIgnore
    public void onServiceDisconnected(ComponentName name) {
        this.otaUpdateService = null;
    }

    @DexIgnore
    protected void saveChanges() {
        showShortToast(R.string.settings_ota_succeeded);
        Tracker.tagEvent(TrackerConstants.Event.Settings.OTA_SETTINGS_CHANGED);
    }

    @DexIgnore
    private void setupView() {
        setUIState(isCheckingForUpdate(), getState(), getUpdateInfo(), getLastKnownUploadSize());
    }

    @DexIgnore
    private void setUIState(boolean checkingForUpdate, OTAUpdateService.State state, UpdateInfo updateInfo) {
        setUIState(checkingForUpdate, state, updateInfo, 0);
    }

    @DexIgnore
    private void setUIState(boolean checkingForUpdate, OTAUpdateService.State state, UpdateInfo updateInfo, long lastKnownUpdateSize) {
        sLogger.v("setUIState; checkingForUpdate=" + checkingForUpdate + "; state=" + state + "; updateInfo.versionName=" + (updateInfo != null ? updateInfo.versionName : "updateInfo is null!!!") + "; lastKnownUpdateSize=" + lastKnownUpdateSize);
        Context context = getApplicationContext();
        if (context != null) {
            this.hudVersion.setText(StringUtils.fromHtml(getHUDVersionText()));
            this.subtitle2.setVisibility(View.GONE);
            this.progressBarContainer.setVisibility(View.GONE);
            this.downloadVersion.setVisibility(View.GONE);
            this.hardwareSupportTitle.setVisibility(View.GONE);
            this.description.setVisibility(View.GONE);
            this.button.setEnabled(true);
            this.button.setVisibility(View.VISIBLE);
            setBuildSources();
            showReleaseNotes(this.showingMoreInfo);
            if (checkingForUpdate) {
                this.title.setText(R.string.checking_for_updates);
                this.subtitle.setText(R.string.contacting_navdy_servers);
                ImageUtils.loadImage(this.image, R.drawable.image_navdy_checking, null);
                this.button.setText(R.string.checking_for_update);
                this.button.setEnabled(false);
                return;
            }
            switch (state) {
                case UP_TO_DATE:
                    this.title.setText(R.string.up_to_date);
                    this.subtitle.setText(R.string.navdy_display_up_to_date);
                    this.subtitle2.setVisibility(View.VISIBLE);
                    this.subtitle2.setText(getCurrentHUDVersionText());
                    ImageUtils.loadImage(this.image, R.drawable.image_navdy_up_to_date, null);
                    this.button.setText(R.string.check_for_update);
                    return;
                case UPDATE_AVAILABLE:
                    this.title.setText(R.string.update_available);
                    if (updateInfo != null) {
                        this.subtitle.setText(String.format(getText(R.string.update_version_text).toString(), UpdateInfo.getFormattedVersionName(updateInfo), Formatter.formatShortFileSize(context, updateInfo.size)));
                    } else {
                        this.subtitle.setText(R.string.no_update_info);
                    }
                    this.subtitle2.setVisibility(View.VISIBLE);
                    this.subtitle2.setText(getCurrentHUDVersionText());
                    ImageUtils.loadImage(this.image, R.drawable.image_navdy_update_available, null);
                    this.button.setText(R.string.download_update);
                    return;
                case DOWNLOADING_UPDATE:
                    this.title.setText(R.string.downloading_update);
                    this.subtitle.setText(R.string.we_are_downloading_update);
                    this.subtitle2.setVisibility(View.GONE);
                    ImageUtils.loadImage(this.image, R.drawable.image_phone_downloading, null);
                    this.button.setText(R.string.cancel_download);
                    makeProgressBarVisibleAndSetInfoVersion(updateInfo);
                    this.downloadVersion.setVisibility(View.VISIBLE);
                    setDownloadProgressPercentage(1, 0);
                    return;
                case READY_TO_UPLOAD:
                    this.title.setText(R.string.ready_to_transfer_update);
                    this.subtitle.setText(R.string.ready_to_transfer_update_desc);
                    this.subtitle2.setVisibility(View.GONE);
                    ImageUtils.loadImage(this.image, R.drawable.image_ready_to_transfer, null);
                    makeProgressBarVisibleAndSetInfoVersion(updateInfo);
                    setUploadProgressPercentage(0, 0);
                    this.progressData.setText(R.string.waiting_for_navdy);
                    this.progressPercentage.setText("");
                    this.button.setText(R.string.cancel_download);
                    return;
                case UPLOADING:
                    long total = updateInfo != null ? updateInfo.size : 0;
                    setUploadProgressPercentage(total > 0 ? (int) ((((float) lastKnownUpdateSize) / ((float) total)) * 100.0f) : 0, lastKnownUpdateSize);
                    this.title.setText(R.string.transferring_update);
                    this.subtitle.setText(R.string.transferring_update_desc);
                    this.subtitle2.setVisibility(View.GONE);
                    ImageUtils.loadImage(this.image, R.drawable.image_transferring, null);
                    makeProgressBarVisibleAndSetInfoVersion(updateInfo);
                    setUploadProgressPercentage(0, 0);
                    this.button.setText(R.string.cancel_download);
                    return;
                case READY_TO_INSTALL:
                    this.title.setText(R.string.ready_to_install);
                    this.subtitle.setText(R.string.ready_to_install_desc);
                    this.subtitle2.setVisibility(View.GONE);
                    ImageUtils.loadImage(this.image, R.drawable.image_ready, null);
                    if (updateInfo != null) {
                        setUploadProgressPercentage(100, updateInfo.size);
                        makeProgressBarVisibleAndSetInfoVersion(updateInfo);
                    }
                    this.button.setText(R.string.cancel_download);
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public void makeProgressBarVisibleAndSetInfoVersion(UpdateInfo updateInfo) {
        this.logger.d("makeProgressBarVisibleAndSetInfoVersion: " + updateInfo);
        if (updateInfo != null) {
            this.progressBarContainer.setVisibility(View.VISIBLE);
            if (Build.VERSION.SDK_INT <= 21) {
                LayerDrawable progressBar1LayerDrawable = (LayerDrawable) this.progressBar.getProgressDrawable();
                Drawable background = progressBar1LayerDrawable.findDrawableByLayerId(R.id.progress_bar_background_1);
                Drawable progress = progressBar1LayerDrawable.findDrawableByLayerId(R.id.progress_bar_progress_1);
                LayerDrawable progressBar2LayerDrawable = (LayerDrawable) this.progressBar2.getProgressDrawable();
                Drawable background2 = progressBar2LayerDrawable.findDrawableByLayerId(R.id.progress_bar_background_2);
                Drawable progress2 = progressBar2LayerDrawable.findDrawableByLayerId(R.id.progress_bar_progress_2);
                background.setColorFilter(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.grey_2), PorterDuff.Mode.SRC_IN);
                progress.setColorFilter(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.purple_warm), PorterDuff.Mode.SRC_IN);
                background2.setColorFilter(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.grey_2), PorterDuff.Mode.SRC_IN);
                progress2.setColorFilter(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.blue), PorterDuff.Mode.SRC_IN);
            }
            this.downloadVersion.setVisibility(View.VISIBLE);
            this.downloadVersion.setText(String.format("v%1$s", UpdateInfo.getFormattedVersionName(updateInfo)));
        }
    }

    @DexIgnore
    private OTAUpdateService.State getState() {
        if (this.otaUpdateService != null) {
            return this.otaUpdateService.getOTAUpdateState();
        }
        return OTAUpdateService.State.UP_TO_DATE;
    }

    /* access modifiers changed from: private */
    // @android.support.annotation.Nullable
    @DexIgnore
    public UpdateInfo getUpdateInfo() {
        UpdateInfo updateInfo = null;
        if (this.otaUpdateService != null) {
            updateInfo = this.otaUpdateService.getUpdateInfo();
        }
        if (updateInfo == null) {
            updateInfo = OTAUpdateService.bReadUpdateInfo();
        }
        this.logger.v("updateInfo status: " + updateInfo);
        return updateInfo;
    }

    @DexIgnore
    private long getLastKnownUploadSize() {
        if (this.otaUpdateService == null || getState() != OTAUpdateService.State.UPLOADING) {
            return 0;
        }
        return this.otaUpdateService.lastKnownUploadSize();
    }

    @DexIgnore
    private boolean isCheckingForUpdate() {
        return this.otaUpdateService != null && this.otaUpdateService.isCheckingForUpdate();
    }

    @DexIgnore
    private String getCurrentHUDVersionText() {
        if (this.otaUpdateService == null) {
            return "";
        }
        String resourceText = getText(R.string.current_hud_version_is).toString();
        String hudBuildVersionText = this.otaUpdateService.getHUDBuildVersionText();
        if (StringUtils.isEmptyAfterTrim(hudBuildVersionText)) {
            return getString(R.string.current_hud_version_unknown);
        }
        return String.format(resourceText, hudBuildVersionText);
    }

    @DexIgnore
    private String getHUDVersionText() {
        if (this.otaUpdateService == null) {
            return null;
        }
        String resourceText = getText(R.string.current_hud_version).toString();
        if (StringUtils.isEmptyAfterTrim(this.otaUpdateService.getHUDBuildVersionText())) {
            return getString(R.string.unknown);
        }
        return String.format(resourceText, this.otaUpdateService.getHUDBuildVersionText());
    }

    @DexIgnore
    private void startDownload() {
        Context appContext = getApplicationContext();
        if (this.otaUpdateService == null) {
            return;
        }
        if (com.navdy.client.app.framework.util.SystemUtils.isAirplaneModeOn(appContext)) {
            showSimpleDialog(3, getString(R.string.error), getString(R.string.air_plane_mode));
        } else if (!SystemUtils.isConnectedToNetwork(appContext)) {
            showSimpleDialog(3, getString(R.string.error), getString(R.string.no_connectivity));
        } else if (!SystemUtils.isConnectedToWifi(appContext)) {
            showSimpleDialog(1, getString(R.string.large_file_over_network), getString(R.string.large_file_over_network_desc));
        } else {
            setUIState(true, null, null);
            this.otaUpdateService.downloadOTAUpdate();
        }
    }

    @DexIgnore
    public Dialog createDialog(int id, Bundle arguments) {
        throw null;
        // Dialog dialog = super.createDialog(id, arguments);
        // AlertDialog alert = (AlertDialog) dialog;
        // if (dialog != null) {
        //     switch (id) {
        //         case 1:
        //             alert.setButton(-1, getString(R.string.ok), new OtaSettingsActivity.Anon6());
        //             alert.setButton(-2, getString(R.string.cancel), new OtaSettingsActivity.Anon7());
        //             return alert;
        //     }
        // }
        // return dialog;
    }

    @DexIgnore
    private void setBuildSources() {
        // boolean z = false;
        // if (this.buildType == null || this.buildType != OTAUpdateService.getBuildType()) {
        //     sLogger.d("Setting build source view");
        //     S3Constants.BuildSource buildSource = OTAUpdateService.getBuildSource();
        //     sLogger.d("Build source :" + buildSource.name());
        //     S3Constants.BuildSource[] sources = OTAUpdateService.getBuildSources();
        //     this.buildType = OTAUpdateService.getBuildType();
        //     sLogger.d("Build Type :" + this.buildType.name());
        //     int i = 0;
        //     int index = 0;
        //     for (S3Constants.BuildSource source : sources) {
        //         if (buildSource == source) {
        //             index = i;
        //         }
        //         i++;
        //     }
        //     Switch switchR = this.buildTypeUnstableSwitch;
        //     if (index == 0) {
        //         z = true;
        //     }
        //     switchR.setChecked(z);
        //     this.buildTypeUnstableSwitch.setOnCheckedChangeListener(new OtaSettingsActivity.Anon8());
        // }
    }

    /* access modifiers changed from: private */
    // @android.annotation.SuppressLint({"CommitPrefEdits"})
    @DexIgnore
    public void setUnstableSource(boolean unstable) {
        char c;
        S3Constants.BuildSource[] sources = OTAUpdateService.getBuildSources();
        if (unstable) {
            c = 0;
        } else {
            c = 1;
        }
        S3Constants.BuildSource source = sources[c];
        OTAUpdateService.persistUserPreferredBuildSource(source);
        sLogger.d("Set to unstable source ? :" + unstable);
        sLogger.d("Setting to the source  :" + source.name());
        this.sharedPrefs.edit().putString(SettingsConstants.OTA_STATUS, SettingsConstants.OTA_STATUS_DEFAULT).putInt(SettingsConstants.OTA_VERSION, -1).putString(SettingsConstants.OTA_URL, "").putString(SettingsConstants.OTA_DESCRIPTION, "").putLong(SettingsConstants.OTA_SIZE, 0).putBoolean(SettingsConstants.OTA_IS_INCREMENTAL, false).putInt(SettingsConstants.OTA_FROM_VERSION, 0).commit();
        setUIState(true, OTAUpdateService.State.UP_TO_DATE, null);
        setupView();
    }

    @DexIgnore
    private void cancelAndRecheckForUpdate(OTAUpdateService.State state) {
        switch (state) {
            case DOWNLOADING_UPDATE:
                this.otaUpdateService.cancelDownload();
                this.otaUpdateService.checkForUpdate();
                return;
            case UPLOADING:
                this.otaUpdateService.cancelUpload();
                this.otaUpdateService.resetUpdate();
                this.otaUpdateService.checkForUpdate();
                return;
            default:
                return;
        }
    }

    @DexIgnore
    private void showReleaseNotes(boolean more) {
        sLogger.d("Show release notes, more : " + more);
        try {
            String releaseNotesText = UpdateInfo.extractReleaseNotes(getUpdateInfo());
            if (!StringUtils.isEmptyAfterTrim(releaseNotesText)) {
                this.description.setText(releaseNotesText);
                this.moreOrLess.setVisibility(View.VISIBLE);
                this.hardwareSupportTitle.setVisibility(View.VISIBLE);
                this.description.setVisibility(View.VISIBLE);
                if (more) {
                    expandAnimationForTextView(this.description);
                    this.description.setMaxLines(Integer.MAX_VALUE);
                    this.moreOrLess.setText(R.string.less_info);
                    return;
                }
                collapseAnimationForTextView(this.description);
                this.description.setMaxLines(2);
                this.description.setEllipsize(TextUtils.TruncateAt.END);
                this.moreOrLess.setText(R.string.more_info);
            }
        } catch (Exception e) {
            this.logger.e("exception found");
            sLogger.d("No release notes found");
            this.moreOrLess.setVisibility(View.GONE);
            this.description.setVisibility(View.GONE);
        }
    }

    @DexIgnore
    public void expandAnimationForTextView(TextView textView) {
        textView.setMaxLines(Integer.MAX_VALUE);
    }

    @DexIgnore
    public void collapseAnimationForTextView(TextView textView) {
        textView.setMaxLines(2);
    }

    @DexIgnore
    public void adjustImageContainerHeight() {
        try {
            DisplayMetrics displayMetrics = NavdyApplication.getAppContext().getResources().getDisplayMetrics();
            if (this.image != null) {
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.image.getLayoutParams();
                layoutParams.height = Double.valueOf(((double) displayMetrics.heightPixels) * SCREEN_PERCENTAGE_FOR_IMAGE).intValue();
                this.image.setLayoutParams(layoutParams);
            }
        } catch (Exception e) {
            this.logger.e("There was a problem changing the layout height of the ota container: " + e);
        }
    }

    @DexIgnore
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    @DexIgnore
    public void onUnstableVersionClick(View view) {
        if (this.buildTypeUnstableSwitch != null && this.buildTypeUnstableSwitch.isEnabled()) {
            this.buildTypeUnstableSwitch.performClick();
        }
    }
}
