package com.navdy.client.app.ui.settings;

import android.net.Uri;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class SupportActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    @DexIgnore
    public static int SHOW_SUCCESS_DIALOG_FOR_SUPPORT_TICKET; // = 1;

    // @DexIgnore
    // class Anon1 implements java.lang.Runnable {
    //     Anon1() {
    //     }
    //
    //     public void run() {
    //         android.net.Uri zendeskUri = com.navdy.client.app.framework.util.ZendeskJWT.getZendeskUri();
    //         if (zendeskUri != null) {
    //             android.content.Intent browserIntent = new android.content.Intent(com.navdy.client.app.ui.settings.SupportActivity.this.getApplicationContext(), com.navdy.client.app.ui.WebViewActivity.class);
    //             browserIntent.putExtra("type", com.navdy.client.app.ui.WebViewActivity.ZENDESK);
    //             browserIntent.putExtra(com.navdy.client.app.ui.WebViewActivity.EXTRA_HTML, zendeskUri.toString());
    //             com.navdy.client.app.ui.settings.SupportActivity.this.startActivity(browserIntent);
    //         }
    //     }
    // }

    @DexIgnore
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.settings_support);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.menu_support).build();
    }

    @DexIgnore
    protected void onResume() {
        super.onResume();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.Settings.SUPPORT);
    }

    @DexReplace
    public void onHelpCenterClick(android.view.View view) {
        openBrowserFor(Uri.parse("http://navdy.alelec.net"));
        // com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.settings.SupportActivity.Anon1(), 1);
    }

    @DexIgnore
    public void onInstallationVideoClick(android.view.View view) {
        android.content.Intent i = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.InstallCompleteActivity.class);
        i.putExtra(com.navdy.client.app.ui.firstlaunch.InstallCompleteActivity.EXTRA_COMING_FROM_SETTINGS, true);
        startActivity(i);
    }

    @DexIgnore
    public void onGuidedTourClick(android.view.View view) {
        startActivity(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.settings.FeatureVideosActivity.class));
    }

    @DexIgnore
    public void onContactUsClick(android.view.View view) {
        startActivityForResult(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.settings.ContactUsActivity.class), 1);
    }
}
