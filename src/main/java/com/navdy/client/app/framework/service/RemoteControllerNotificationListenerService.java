package com.navdy.client.app.framework.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.RemoteController;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.support.annotation.Nullable;

import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.glances.GlanceConstants;
import com.navdy.client.app.framework.music.RemoteControllerSeekHelper;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.service.library.events.audio.MusicCollectionSource;
import com.navdy.service.library.events.audio.MusicCollectionType;
import com.navdy.service.library.events.audio.MusicDataSource;
import com.navdy.service.library.events.audio.MusicPlaybackState;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.log.Logger;

import org.droidparts.contract.SQL;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public class RemoteControllerNotificationListenerService extends NavdyCustomNotificationListenerService implements RemoteController.OnClientUpdateListener {
    @DexIgnore
    private static /* final */ int MINIMUM_UPDATE_WAIT; // = 1000;
    @DexIgnore
    private static /* final */ int REMOTE_CONTROL_PLAYSTATE_NONE; // = 0;
    @DexIgnore
    private static /* final */ boolean VERBOSE; // = false;
    @DexIgnore
    public static /* final */ Logger logger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService.class);
    @DexIgnore
    private Bitmap albumArt; // = null;
    @DexIgnore
    private AudioManager audioManager;
    @DexIgnore
    private boolean ignoreMetadata;
    @DexIgnore
    private double lastUpdateTime;
    @DexIgnore
    private boolean lastUpdateWasClientMetadata;
    // @android.support.annotation.NonNull
    @DexIgnore
    private MusicTrackInfo musicTrackInfo; // = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.EMPTY_TRACK_INFO;
    @DexIgnore
    private int playbackState; // = 0;
    @DexIgnore
    private RemoteController remoteController;

    @DexIgnore
    class Anon1 implements Runnable {
        @DexIgnore
        Anon1() {
        }

        @DexIgnore
        public void run() {
            RemoteControllerNotificationListenerService.this.remoteController = new RemoteController(RemoteControllerNotificationListenerService.this, RemoteControllerNotificationListenerService.this);
            if (!RemoteControllerNotificationListenerService.this.remoteController.setArtworkConfiguration(200, 200)) {
                RemoteControllerNotificationListenerService.logger.e("Couldn't set artwork configuration");
            }
            RemoteControllerNotificationListenerService.this.audioManager = (AudioManager) NavdyApplication.getAppContext().getSystemService(Context.AUDIO_SERVICE);
            try {
                RemoteControllerNotificationListenerService.this.audioManager.registerRemoteController(RemoteControllerNotificationListenerService.this.remoteController);
                MusicServiceHandler.getInstance().setMusicSeekHelper(new RemoteControllerSeekHelper(RemoteControllerNotificationListenerService.this.remoteController));
                RemoteControllerNotificationListenerService.logger.i("Remote controller and audio manager initialized");
            } catch (Throwable e) {
                RemoteControllerNotificationListenerService.this.remoteController = null;
                RemoteControllerNotificationListenerService.logger.e("Remote controller could not be initialized: " + e);
            }
        }
    }

    @DexAdd
    public static void rebind() {
        ComponentName componentName = new ComponentName(NavdyApplication.getAppContext(), RemoteControllerNotificationListenerService.class);
        PackageManager packageManager = NavdyApplication.getAppContext().getPackageManager();
        packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
        packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
        if (Build.VERSION.SDK_INT >= 24) {
            // FLogger.INSTANCE.getLocal().d(a(), "tryReconnectNotificationService() - requestRebind");
            NotificationListenerService.requestRebind(componentName);
        }
    }

    @DexIgnore
    public void onCreate() {
        logger.i("onCreate");
        super.onCreate();
    }

    @DexIgnore
    private void initializeRemoteController() {
        logger.i("initializeRemoteController");
        if (this.remoteController != null) {
            logger.i("RemoteController already initialized, returning");
        } else {
            new Handler(getMainLooper()).post(new RemoteControllerNotificationListenerService.Anon1());
        }
    }

    @DexReplace
    public IBinder onBind(Intent intent) {
        logger.d("onBind");
        // if (Build.VERSION.SDK_INT < 21) {
            initializeRemoteController();
        // }
        return super.onBind(intent);
    }

    @DexIgnore
    public void onListenerConnected() {
        logger.d("onListenerConnected");
        initializeRemoteController();
        super.onListenerConnected();
    }

    @DexIgnore
    public boolean onUnbind(Intent intent) {
        logger.d("onUnbind");
        return super.onUnbind(intent);
    }

    @DexIgnore
    public void onDestroy() {
        logger.i("onDestroy");
        if (this.audioManager != null) {
            this.audioManager.unregisterRemoteController(this.remoteController);
        }
        MusicServiceHandler.getInstance().setMusicSeekHelper(null);
        super.onDestroy();
    }

    @DexIgnore
    public void onClientChange(boolean clearing) {
        logger.v("onClientChange: " + clearing);
        if (clearing) {
            setMusicTrackInfo(null);
            this.albumArt = null;
            this.playbackState = 0;
        }
    }

    @DexIgnore
    public void onClientPlaybackStateUpdate(int state) {
        logger.v("onClientPlaybackStateUpdate: " + state);
        checkForSpuriousStateUpdate(state);
        if (state != this.playbackState && state != 0) {
            this.playbackState = state;
            setMusicTrackInfo(new MusicTrackInfo.Builder(this.musicTrackInfo).playbackState(musicPlaybackState(this.playbackState)).collectionSource(MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN).collectionType(MusicCollectionType.COLLECTION_TYPE_UNKNOWN).collectionId(null).trackId(null).build());
            sendAlbumArt();
        }
    }

    @DexIgnore
    public void onClientPlaybackStateUpdate(int state, long stateChangeTimeMs, long currentPosMs, float speed) {
        logger.v("onClientPlaybackStateUpdate: " + state + SQL.DDL.SEPARATOR + stateChangeTimeMs + SQL.DDL.SEPARATOR + currentPosMs + SQL.DDL.SEPARATOR + speed);
        checkForSpuriousStateUpdate(state);
        long currentTime = System.currentTimeMillis();
        if (state == 0) {
            return;
        }
        if (this.playbackState != state || ((double) currentTime) - this.lastUpdateTime >= 1000.0d || this.lastUpdateWasClientMetadata) {
            this.lastUpdateTime = (double) currentTime;
            this.lastUpdateWasClientMetadata = false;
            setMusicTrackInfo(new MusicTrackInfo.Builder(this.musicTrackInfo).playbackState(musicPlaybackState(state)).currentPosition((int) currentPosMs).collectionSource(MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN).collectionType(MusicCollectionType.COLLECTION_TYPE_UNKNOWN).collectionId(null).trackId(null).build());
            if (this.playbackState != state) {
                sendAlbumArt();
            }
            this.playbackState = state;
        }
    }

    @DexIgnore
    private void checkForSpuriousStateUpdate(int state) {
        if (state >= 3 || this.playbackState >= 3) {
            this.ignoreMetadata = false;
            return;
        }
        logger.i("Spurious none/stop/pause detected, will ignore metadata updates until we leave none/stopped/paused states");
        this.ignoreMetadata = true;
    }

    @DexIgnore
    public void onClientTransportControlUpdate(int transportControlFlags) {
        boolean z = true;
        logger.v("onClientTransportControlUpdate: " + Integer.toBinaryString(transportControlFlags));
        MusicServiceHandler musicServiceHandler = MusicServiceHandler.getInstance();
        MusicTrackInfo.Builder isPreviousAllowed = new MusicTrackInfo.Builder(this.musicTrackInfo).isPreviousAllowed((transportControlFlags & 1) != 0);
        if ((isAdvertisement() || !GlanceConstants.PANDORA.equals(musicServiceHandler.getLastMusicApp())) && (transportControlFlags & 128) == 0) {
            z = false;
        }
        setMusicTrackInfo(isPreviousAllowed.isNextAllowed(z).collectionSource(MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN).collectionType(MusicCollectionType.COLLECTION_TYPE_UNKNOWN).collectionId(null).trackId(null).build());
        sendAlbumArt();
    }

    @DexIgnore
    public void onClientMetadataUpdate(RemoteController.MetadataEditor metadataEditor) {
        logger.v("onClientMetadataUpdate");
        this.lastUpdateWasClientMetadata = true;
        if (this.ignoreMetadata) {
            logger.i("ignoring metadata due to prior spurious update");
            return;
        }
        setMusicTrackInfo(new MusicTrackInfo.Builder(this.musicTrackInfo).dataSource(MusicDataSource.MUSIC_SOURCE_OS_NOTIFICATION).album(metadataEditor.getString(1, "")).author(metadataEditor.getString(2, "")).name(metadataEditor.getString(7, "")).duration((int) metadataEditor.getLong(9, 0)).playbackState(musicPlaybackState(this.playbackState)).collectionSource(MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN).collectionType(MusicCollectionType.COLLECTION_TYPE_UNKNOWN).collectionId(null).trackId(null).build());
        this.albumArt = null;
        Bitmap bitmap = metadataEditor.getBitmap(100, null);
        if (bitmap != null) {
            this.albumArt = bitmap;
            sendAlbumArt();
        }
    }

    @DexIgnore
    public void onClientSessionEvent(String s, Bundle b) {
        logger.v("onClientSessionEvent");
    }

    @DexIgnore
    private void setMusicTrackInfo(@Nullable MusicTrackInfo trackInfo) {
        if (trackInfo == null) {
            this.musicTrackInfo = MusicServiceHandler.EMPTY_TRACK_INFO;
        } else {
            this.musicTrackInfo = trackInfo;
        }
        MusicServiceHandler.getInstance().setCurrentMediaTrackInfo(trackInfo);
    }

    @DexIgnore
    private void sendAlbumArt() {
        MusicServiceHandler.getInstance().checkAndSetCurrentMediaArtwork(this.musicTrackInfo, this.albumArt);
    }

    @DexIgnore
    private boolean isAdvertisement() {
        MusicTrackInfo currentTrackInfo = MusicServiceHandler.getInstance().getCurrentMediaTrackInfo();
        String name = currentTrackInfo.name;
        return name != null && name.toLowerCase().contains("advertisement") && StringUtils.isEmptyAfterTrim(currentTrackInfo.author) && StringUtils.isEmptyAfterTrim(currentTrackInfo.album);
    }

    @DexIgnore
    private MusicPlaybackState musicPlaybackState(int remoteControlClientPlaybackState) {
        switch (remoteControlClientPlaybackState) {
            case 1:
                return MusicPlaybackState.PLAYBACK_STOPPED;
            case 2:
                return MusicPlaybackState.PLAYBACK_PAUSED;
            case 3:
                return MusicPlaybackState.PLAYBACK_PLAYING;
            case 4:
                return MusicPlaybackState.PLAYBACK_FAST_FORWARDING;
            case 5:
                return MusicPlaybackState.PLAYBACK_REWINDING;
            case 6:
                return MusicPlaybackState.PLAYBACK_SKIPPING_TO_NEXT;
            case 7:
                return MusicPlaybackState.PLAYBACK_SKIPPING_TO_PREVIOUS;
            case 8:
                return MusicPlaybackState.PLAYBACK_BUFFERING;
            case 9:
                return MusicPlaybackState.PLAYBACK_ERROR;
            default:
                return MusicPlaybackState.PLAYBACK_NONE;
        }
    }

    @DexIgnore
    private void logMetadata(RemoteController.MetadataEditor metadataEditor) {
        logger.v("- METADATA_KEY_ALBUM: " + metadataEditor.getString(1, ""));
        logger.v("- METADATA_KEY_ARTIST: " + metadataEditor.getString(2, ""));
        logger.v("- METADATA_KEY_TITLE: " + metadataEditor.getString(7, ""));
        logger.v("- METADATA_KEY_DURATION: " + metadataEditor.getLong(9, 0));
        logger.v("- BITMAP_KEY_ARTWORK: " + metadataEditor.getBitmap(100, null));
    }
}
