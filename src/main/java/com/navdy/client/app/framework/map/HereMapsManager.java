package com.navdy.client.app.framework.map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.ContextCompat;

import com.here.android.mpa.common.ApplicationContext;
import com.here.android.mpa.common.MapEngine;
import com.here.android.mpa.common.MapSettings;
import com.here.android.mpa.common.OnEngineInitListener;
import com.navdy.client.BuildConfig;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.CredentialsUtils;

import java.io.File;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

//import static android.provider.Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION;

@DexEdit
public class HereMapsManager {
    @DexIgnore
    private static java.lang.String HERE_MAPS_INTERNAL_FOLDER;  // = "/.here-maps"
    @DexIgnore
    private static java.lang.String MAP_ENGINE_PROCESS_NAME;  // = "Navdy.Mapservice"
    @DexIgnore
    private static java.lang.String MAP_SERVICE_INTENT;  // = "com.navdy.mapservice.MapService"
    @DexIgnore
    private static java.lang.Object initListenersLock;  // = new java.lang.Object()
    @DexIgnore
    private static com.navdy.client.app.framework.map.HereMapsManager instance;  // = new com.navdy.client.app.framework.map.HereMapsManager()
    @DexIgnore
    private static com.navdy.service.library.log.Logger logger;  // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.map.HereMapsManager.class)
    @DexIgnore
    private com.here.android.mpa.common.OnEngineInitListener.Error initError;
    @DexIgnore
    private java.util.Queue<com.here.android.mpa.common.OnEngineInitListener> initListeners;  // = new java.util.LinkedList()
    @DexIgnore
    private android.os.Handler mainThread;  // = new android.os.Handler(android.os.Looper.getMainLooper())
    @DexIgnore
    private MapEngine mapEngine;  // = com.here.android.mpa.common.MapEngine.getInstance()
    @DexIgnore
    private com.here.android.mpa.common.OnEngineInitListener onEngineInitListener;  // = new com.navdy.client.app.framework.map.HereMapsManager.Anon1()
    @DexIgnore
    private volatile com.navdy.client.app.framework.map.HereMapsManager.State state; // = com.navdy.client.app.framework.map.HereMapsManager.State.IDLE;

//    class Anon1 implements com.here.android.mpa.common.OnEngineInitListener {
//
//        /* renamed from: com.navdy.client.app.framework.map.HereMapsManager$Anon1$Anon1 reason: collision with other inner class name */
//        class C0041Anon1 implements java.lang.Runnable {
//            final /* synthetic */ com.here.android.mpa.common.OnEngineInitListener.Error val$error;
//
//            /* renamed from: com.navdy.client.app.framework.map.HereMapsManager$Anon1$Anon1$Anon1 reason: collision with other inner class name */
//            class C0042Anon1 implements java.lang.Runnable {
//                C0042Anon1() {
//                }
//
//                public void run() {
//                    synchronized (com.navdy.client.app.framework.map.HereMapsManager.initListenersLock) {
//                        while (!com.navdy.client.app.framework.map.HereMapsManager.this.initListeners.isEmpty()) {
//                            ((com.here.android.mpa.common.OnEngineInitListener) com.navdy.client.app.framework.map.HereMapsManager.this.initListeners.poll()).onEngineInitializationCompleted(com.navdy.client.app.framework.map.HereMapsManager.Anon1.C0041Anon1.this.val$error);
//                        }
//                    }
//                }
//            }
//
//            C0041Anon1(com.here.android.mpa.common.OnEngineInitListener.Error error) {
//                this.val$error = error;
//            }
//
//            public void run() {
//                com.navdy.client.app.framework.map.HereMapsManager.this.initError = this.val$error;
//                if (this.val$error == com.here.android.mpa.common.OnEngineInitListener.Error.NONE) {
//                    com.navdy.client.app.framework.map.HereMapsManager.logger.i("MapEngine has initialized correctly");
//                    com.navdy.client.app.framework.map.HereMapsManager.logger.i("HERE SDK version " + com.here.android.mpa.common.Version.getSdkVersion());
//                    com.navdy.client.app.framework.map.HereMapsConfigurator.getInstance().updateMapsConfig();
//                    com.navdy.client.app.framework.map.HereMapsManager.this.state = com.navdy.client.app.framework.map.HereMapsManager.State.INITIALIZED;
//                    com.navdy.client.app.framework.map.HereMapsManager.this.mainThread.post(new com.navdy.client.app.framework.map.HereMapsManager.Anon1.C0041Anon1.C0042Anon1());
//                    return;
//                }
//                com.navdy.client.app.framework.map.HereMapsManager.this.state = com.navdy.client.app.framework.map.HereMapsManager.State.FAILED;
//                com.navdy.client.app.framework.map.HereMapsManager.logger.e("Map failed to initialize: " + this.val$error);
//                com.navdy.client.app.framework.map.HereMapsManager.logger.e("ERROR ON MAP ENGINE INIT, RESTARTING");
//                com.navdy.client.app.framework.map.HereMapsManager.this.killSelfAndMapEngine();
//            }
//        }
//
//        Anon1() {
//        }
//
//        public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
//            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.map.HereMapsManager.Anon1.C0041Anon1(error), 11);
//        }
//    }
//
//    class Anon2 implements java.lang.Runnable {
//        final /* synthetic */ com.here.android.mpa.common.OnEngineInitListener val$listener;
//
//        Anon2(com.here.android.mpa.common.OnEngineInitListener onEngineInitListener) {
//            this.val$listener = onEngineInitListener;
//        }
//
//        public void run() {
//            this.val$listener.onEngineInitializationCompleted(com.navdy.client.app.framework.map.HereMapsManager.this.initError);
//        }
//    }
//
//    class Anon3 implements java.lang.Runnable {
//        final /* synthetic */ com.here.android.mpa.common.OnEngineInitListener val$listener;
//
//        Anon3(com.here.android.mpa.common.OnEngineInitListener onEngineInitListener) {
//            this.val$listener = onEngineInitListener;
//        }
//
//        public void run() {
//            synchronized (com.navdy.client.app.framework.map.HereMapsManager.initListenersLock) {
//                com.navdy.client.app.framework.map.HereMapsManager.this.initListeners.add(this.val$listener);
//            }
//            if (com.navdy.client.app.framework.map.HereMapsManager.this.state == com.navdy.client.app.framework.map.HereMapsManager.State.IDLE) {
//                com.navdy.client.app.framework.map.HereMapsManager.this.initializeIfPermitted();
//            }
//        }
//    }
//
    @DexIgnore
    private enum State {
        IDLE,
        INITIALIZING,
        INITIALIZED,
        FAILED
    }

    @DexIgnore
    public static com.navdy.client.app.framework.map.HereMapsManager getInstance() {
        return instance;
    }

    @DexIgnore
    private HereMapsManager() {
    }

    @DexIgnore
    public void addOnInitializedListener(com.here.android.mpa.common.OnEngineInitListener listener) {
        // if (this.state != com.navdy.client.app.framework.map.HereMapsManager.State.FAILED) {
        //     if (this.state == com.navdy.client.app.framework.map.HereMapsManager.State.INITIALIZED) {
        //         this.mainThread.post(new com.navdy.client.app.framework.map.HereMapsManager.Anon2(listener));
        //     } else {
        //         com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.map.HereMapsManager.Anon3(listener), 11);
        //     }
        // }
    }

    @DexIgnore
    public boolean isInitialized() {
        return this.state == com.navdy.client.app.framework.map.HereMapsManager.State.INITIALIZED;
    }

    @DexIgnore
    public void killSelfAndMapEngine() {
        @SuppressLint("WrongConstant") java.util.Iterator it = ((android.app.ActivityManager) com.navdy.client.app.NavdyApplication.getAppContext().getSystemService("activity")).getRunningAppProcesses().iterator();
        while (true) {
           if (!it.hasNext()) {
               break;
           }
           android.app.ActivityManager.RunningAppProcessInfo processInfo = (android.app.ActivityManager.RunningAppProcessInfo) it.next();
           if (processInfo.processName.equals(MAP_ENGINE_PROCESS_NAME)) {
               android.os.Process.killProcess(processInfo.pid);
               break;
           }
        }
        com.navdy.client.app.NavdyApplication.kill();
    }

//    private void initializeIfPermitted() {
//        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
//        boolean readExternalStoragePermission = com.navdy.client.app.ui.base.BaseActivity.weHaveThisPermission(context, "android.permission.READ_EXTERNAL_STORAGE");
//        boolean writeExternalStoragePermission = com.navdy.client.app.ui.base.BaseActivity.weHaveThisPermission(context, "android.permission.WRITE_EXTERNAL_STORAGE");
//        boolean accessFineLocationPermission = com.navdy.client.app.ui.base.BaseActivity.weHaveThisPermission(context, "android.permission.ACCESS_FINE_LOCATION");
//        if (!readExternalStoragePermission || !writeExternalStoragePermission || !accessFineLocationPermission) {
//            logger.d("initialization is not permitted yet, permission granted for READ_EXTERNAL_STORAGE=" + readExternalStoragePermission + "; writeExternalStoragePermission=" + writeExternalStoragePermission + "; accessFineLocationPermission=" + accessFineLocationPermission);
//            return;
//        }
//        logger.v("initialization is permitted, initializing...");
//        initialize();
//    }

    @DexAdd
    public boolean set_maps_path(Activity activity) {

        if (false && Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File navdy_dir = new File(Environment.getExternalStorageDirectory(), "navdy");
            File maps_dir = new File(navdy_dir, HERE_MAPS_INTERNAL_FOLDER);
            if (navdy_dir.exists()) {
                if (maps_dir.exists()) {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
//                        //Android is 11 (R) or above
//                        if (!Environment.isExternalStorageManager()) {
//                            if (activity != null) {
//                                Intent intent = new Intent(ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION, Uri.parse("package:" + BuildConfig.APPLICATION_ID));
//                                final int APP_STORAGE_ACCESS_REQUEST_CODE = 501; // Any value
//                                activity.startActivityForResult(intent, APP_STORAGE_ACCESS_REQUEST_CODE);
//                            }
//                            return false;
//                        }
//                    }
                    if (activity == null) {
                        com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(maps_dir.toString(), MAP_SERVICE_INTENT);
                    }
                    return true;
                }
            } else {
                navdy_dir.mkdir();
            }
        }
        return false;
    }

    @DexReplace
    private void initialize() {
        logger.v("initializing here map engine");
        this.state = com.navdy.client.app.framework.map.HereMapsManager.State.INITIALIZING;
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();

        boolean path_set = set_maps_path(null);

        if (!path_set) {
            com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(context.getFilesDir() + HERE_MAPS_INTERNAL_FOLDER, MAP_SERVICE_INTENT);
        }
        mapEngineInit(this.mapEngine, context, this.onEngineInitListener);
    }

    @DexAdd
    public static void mapEngineInit(MapEngine mapEngine, android.content.Context context, OnEngineInitListener onEngineInitListener) {
        ApplicationContext appContext = new ApplicationContext(context);

        appContext.setAppIdCode(
                CredentialsUtils.getCredentials("NAVIGATION_HERE_APPID"),
                CredentialsUtils.getCredentials("NAVIGATION_HERE_APPCODE")
        );
        appContext.setLicenseKey(
                CredentialsUtils.getCredentials("NAVIGATION_HERE_LICENCE")
        );

        if (CredentialsUtils.fixed) {
            NavdyApplication.inject_name_for = Thread.currentThread();
            NavdyApplication.inject_name_skip = 1;
        }
        mapEngine.init(appContext, onEngineInitListener);
    }
}
