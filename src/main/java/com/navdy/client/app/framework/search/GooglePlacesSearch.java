package com.navdy.client.app.framework.search;

import android.net.Uri;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.navdy.client.R.string;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult;
import com.navdy.client.app.framework.util.CredentialsUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.SystemUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class GooglePlacesSearch {
    @DexIgnore
    public static /* final */ int DEFAULT_RADIUS_VALUE; // = 1609;
    @DexIgnore
    private static /* final */ String DESTINATION_PLACE_ID_SYNTAX; // = "destination=place_id:";
    @DexIgnore
    private static /* final */ String DESTINATION_SYNTAX; // = "destination=";
    @DexIgnore
    private static /* final */ String INPUT_SYNTAX; // = "input=";
    @DexIgnore
    private static /* final */ String KEYWORD_SYNTAX; // = "keyword=";
    @DexIgnore
    private static /* final */ String KEY_SYNTAX; // = "key=";
    @DexIgnore
    private static /* final */ String LOCATION_SYNTAX; // = "location=";
    @DexIgnore
    private static /* final */ String OPENNOW_SYNTAX; // = "opennow=";
    @DexIgnore
    private static /* final */ String OPENNOW_VALUE; // = "true";
    @DexIgnore
    private static /* final */ String ORIGIN_SYNTAX; // = "origin=";
    @DexIgnore
    private static /* final */ String PLACE_ID_SYNTAX; // = "placeid=";
    @DexIgnore
    private static /* final */ String QUERY_SYNTAX; // = "query=";
    @DexIgnore
    private static /* final */ String RADIUS_SYNTAX; // = "radius=";
    @DexIgnore
    private static /* final */ String RANKBY_SYNTAX; // = "rankby=";
    @DexIgnore
    private static /* final */ int SERVICE_RADIUS_VALUE; // = 8046;
    @DexIgnore
    private static /* final */ String TYPE_SYNTAX; // = "type=";
    @DexIgnore
    private static /* final */ boolean VERBOSE; // = false;
    @DexIgnore
    public static /* final */ Logger logger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.search.GooglePlacesSearch.class);
    @DexIgnore
    private String apiKEY; // = "";
    @DexIgnore
    private Query currentQueryType; // = com.navdy.client.app.framework.search.GooglePlacesSearch.Query.NEARBY;
    @DexIgnore
    private Destination destination;
    @DexIgnore
    private GoogleSearchListener googleSearchListener;
    @DexIgnore
    private Handler handler;
    @DexIgnore
    private String location_value;
    @DexIgnore
    private Runnable onSuccess; // = new com.navdy.client.app.framework.search.GooglePlacesSearch.Anon1();
    @DexIgnore
    private String placeId;
    @DexIgnore
    private List<GoogleTextSearchDestinationResult> results;

    @DexIgnore
    class Anon1 implements Runnable {
        @DexIgnore
        Anon1() {
        }

        @DexIgnore
        public void run() {
            GooglePlacesSearch.this.sendDestinationsToListener(GooglePlacesSearch.this.results, Error.NONE);
        }
    }

    @DexIgnore
    class Anon2 implements Runnable {
        @DexIgnore
        Anon2() {
        }

        @DexIgnore
        /* final */ /* synthetic */ int val$searchRadius;
        @DexIgnore
        /* final */ /* synthetic */ String val$searchText;
        @DexIgnore
        /* final */ /* synthetic */ ServiceTypes val$type;

        @DexIgnore
        Anon2(String str, ServiceTypes serviceTypes, int i) {
            this.val$searchText = str;
            this.val$type = serviceTypes;
            this.val$searchRadius = i;
        }

        @DexIgnore
        public void run() {
            if (GooglePlacesSearch.this.currentQueryType == Query.DETAILS && !StringUtils.isEmptyAfterTrim(GooglePlacesSearch.this.placeId)) {
                String placeDetailJson = NavdyContentProvider.getNonStalePlaceDetailInfoFor(GooglePlacesSearch.this.placeId);
                if (!StringUtils.isEmptyAfterTrim(placeDetailJson)) {
                    GooglePlacesSearch.logger.v("Place detail data is not stale so using it.");
                    GoogleTextSearchDestinationResult destinationResult = GoogleTextSearchDestinationResult.createDestinationObject(placeDetailJson);
                    GooglePlacesSearch.this.results = new ArrayList();
                    GooglePlacesSearch.this.results.add(destinationResult);
                    if (GooglePlacesSearch.this.handler != null) {
                        GooglePlacesSearch.this.handler.post(GooglePlacesSearch.this.onSuccess);
                        return;
                    } else {
                        GooglePlacesSearch.this.onSuccess.run();
                        return;
                    }
                }
            }
            GooglePlacesSearch.this.runSearchWebApi(this.val$searchText, this.val$type, this.val$searchRadius);
        }
    }

    @DexIgnore
    public enum Error {
        NONE,
        SERVICE_ERROR
    }

    @DexIgnore
    public interface GoogleSearchListener {
        void onGoogleSearchResult(List<GoogleTextSearchDestinationResult> list, Query query, Error error);
    }

    @DexIgnore
    public enum Query {
        QUERY_AUTOCOMPLETE,
        TEXTSEARCH,
        DETAILS,
        NEARBY,
        DIRECTIONS;

        @DexIgnore
        public static String getBaseUrl(Query query) {
            switch (query) {
                case QUERY_AUTOCOMPLETE:
                    return "https://maps.googleapis.com/maps/api/place/queryautocomplete/json?";
                case TEXTSEARCH:
                    return "https://maps.googleapis.com/maps/api/place/textsearch/json?";
                case DETAILS:
                    return "https://maps.googleapis.com/maps/api/place/details/json?";
                case DIRECTIONS:
                    return "https://maps.googleapis.com/maps/api/directions/json?";
                default:
                    return "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
            }
        }
    }

    @DexIgnore
    private enum RankByTypes {
        PROMINENCE,
        DISTANCE;

        @DexIgnore
        public static String getBaseUrl(GooglePlacesSearch.RankByTypes type) {
            switch (type) {
                case PROMINENCE:
                    return "prominence";
                default:
                    return "distance";
            }
        }
    }

    @DexReplace
    public enum ServiceTypes {
        GAS("gas_station", "Gas or petrol"),
        PARKING("parking", "Parking"),
        FOOD("restaurant", "Food"),
        COFFEE("cafe", "Coffee"),
        ATM("atm", "ATMs"),
        HOSPITAL("hospital", "Hospitals"),
        STORE("convenience_store", "Grocery Stores");
        
        String baseUrl;
        String hudServiceType;

        @DexReplace
        private ServiceTypes(String baseUrl2, String hudServiceType2) {
            this.baseUrl = baseUrl2;
            this.hudServiceType = hudServiceType2;
        }

        @DexReplace
        public String getBaseUrl() {
            return this.baseUrl;
        }

        @DexReplace
        public String getHudServiceType() {
            return this.hudServiceType;
        }
    }

    @DexIgnore
    public GooglePlacesSearch(GoogleSearchListener listener, Handler handler2) {
        this.handler = handler2;
        init(listener);
    }

    @DexIgnore
    public GooglePlacesSearch(GoogleSearchListener listener) {
        init(listener);
    }

    @DexIgnore
    private void init(GoogleSearchListener listener) {
        this.apiKEY = KEY_SYNTAX + CredentialsUtils.getCredentials(NavdyApplication.getAppContext().getString(string.metadata_google_web_service));
        this.googleSearchListener = listener;
        Coordinate location = NavdyLocationManager.getInstance().getSmartStartCoordinates();
        if (location != null) {
            this.location_value = location.latitude + "," + location.longitude;
        }
    }

    @DexIgnore
    public void runQueryAutoCompleteWebApi(String query) {
        logger.d("executing Query Auto Complete");
        this.currentQueryType = Query.QUERY_AUTOCOMPLETE;
        runSearchWebApiInTaskManager(query);
    }

    @DexIgnore
    public void runTextSearchWebApi(String query) {
        logger.d("executing Text Search");
        this.currentQueryType = Query.TEXTSEARCH;
        runSearchWebApiInTaskManager(query);
    }

    @DexIgnore
    public void runServiceSearchWebApi(ServiceTypes type) {
        runServiceSearchWebApi(type, SERVICE_RADIUS_VALUE);
    }

    @DexIgnore
    public void runServiceSearchWebApi(ServiceTypes type, int searchRadius) {
        if (type == null) {
            logger.e("Trying to call runServiceSearchWebApi with a null service type. You crazy?");
            return;
        }
        logger.d("executing Service Search");
        if (!StringUtils.isEmptyAfterTrim(this.location_value)) {
            this.currentQueryType = Query.NEARBY;
        } else {
            this.currentQueryType = Query.TEXTSEARCH;
        }
        runSearchWebApiInTaskManager(null, type, searchRadius);
    }

    @DexIgnore
    public void runNearbySearchWebApi(String query) {
        logger.d("executing Nearby Search");
        this.currentQueryType = Query.NEARBY;
        runSearchWebApiInTaskManager(query);
    }

    @DexIgnore
    public void runDetailsSearchWebApi(String placeId2) {
        this.placeId = placeId2;
        this.currentQueryType = Query.DETAILS;
        runSearchWebApiInTaskManager(null);
    }

    @DexIgnore
    public void runDirectionsSearchWebApi(Destination d) {
        this.destination = d;
        this.placeId = d.placeId;
        this.currentQueryType = Query.DIRECTIONS;
        runSearchWebApiInTaskManager(null);
    }

    @DexIgnore
    private void runSearchWebApiInTaskManager(String searchText) {
        runSearchWebApiInTaskManager(searchText, null, DEFAULT_RADIUS_VALUE);
    }

    @DexIgnore
    private void runSearchWebApiInTaskManager(@Nullable String searchText, @Nullable ServiceTypes type, int searchRadius) {
        if (!SystemUtils.isConnectedToNetwork(NavdyApplication.getAppContext())) {
            sendDestinationsToListener(null, Error.SERVICE_ERROR);
        }
        TaskManager.getInstance().execute(new Anon2(searchText, type, searchRadius), 3);
    }

    @DexIgnore
    private void runSearchWebApi(String searchText, @Nullable ServiceTypes type, int searchRadius) {
        String typeString = "";
        if (type != null) {
            typeString = type.getBaseUrl();
        }
        logger.v("Running search with following parameters: " + searchText + " type: " + type);
        String encodedSearchText = Uri.encode(searchText);
        StringBuilder url = new StringBuilder();
        url.append(Query.getBaseUrl(this.currentQueryType));
        url.append(this.apiKEY);
        switch (this.currentQueryType) {
            case QUERY_AUTOCOMPLETE:
                if (!StringUtils.isEmptyAfterTrim(this.location_value)) {
                    url.append("&");
                    url.append(LOCATION_SYNTAX);
                    url.append(this.location_value);
                }
                url.append("&");
                url.append(RADIUS_SYNTAX);
                url.append(DEFAULT_RADIUS_VALUE);
                if (!StringUtils.isEmptyAfterTrim(encodedSearchText)) {
                    url.append("&");
                    url.append(INPUT_SYNTAX);
                    url.append(encodedSearchText);
                    break;
                }
                break;
            case TEXTSEARCH:
                if (MapUtils.parseLatLng(searchText) == null) {
                    if (!StringUtils.isEmptyAfterTrim(this.location_value)) {
                        url.append("&");
                        url.append(LOCATION_SYNTAX);
                        url.append(this.location_value);
                    }
                    url.append("&");
                    url.append(RADIUS_SYNTAX);
                    url.append(DEFAULT_RADIUS_VALUE);
                }
                if (!StringUtils.isEmptyAfterTrim(encodedSearchText)) {
                    url.append("&");
                    url.append(QUERY_SYNTAX);
                    url.append(encodedSearchText);
                    break;
                }
                break;
            case DETAILS:
                url.append("&");
                url.append(PLACE_ID_SYNTAX);
                url.append(this.placeId);
                break;
            case DIRECTIONS:
                if (!StringUtils.isEmptyAfterTrim(this.location_value)) {
                    url.append("&");
                    url.append(ORIGIN_SYNTAX);
                    url.append(this.location_value);
                }
                url.append("&");
                if (StringUtils.isEmptyAfterTrim(this.placeId)) {
                    if (this.destination != null && !StringUtils.isEmptyAfterTrim(this.destination.rawAddressNotForDisplay)) {
                        url.append(DESTINATION_SYNTAX);
                        url.append(Uri.encode(this.destination.rawAddressNotForDisplay));
                        break;
                    } else {
                        logger.e("Trying to call Google Directions API with a no placeId or address.");
                        break;
                    }
                } else {
                    url.append(DESTINATION_PLACE_ID_SYNTAX);
                    url.append(this.placeId);
                    break;
                }
                // break;
            case NEARBY:
                if (!StringUtils.isEmptyAfterTrim(this.location_value)) {
                    url.append("&");
                    url.append(LOCATION_SYNTAX);
                    url.append(this.location_value);
                }
                url.append("&");
                url.append(RANKBY_SYNTAX);
                if (type != null) {
                    switch (type) {
                        case GAS:
                        case PARKING:
                            url.append(GooglePlacesSearch.RankByTypes.getBaseUrl(GooglePlacesSearch.RankByTypes.DISTANCE));
                            break;
                        default:
                            url.append(GooglePlacesSearch.RankByTypes.getBaseUrl(GooglePlacesSearch.RankByTypes.PROMINENCE));
                            url.append("&");
                            url.append(RADIUS_SYNTAX);
                            if (searchRadius <= 0) {
                                url.append(SERVICE_RADIUS_VALUE);
                                break;
                            } else {
                                url.append(searchRadius);
                                break;
                            }
                    }
                } else {
                    url.append(GooglePlacesSearch.RankByTypes.getBaseUrl(GooglePlacesSearch.RankByTypes.DISTANCE));
                    url.append("&");
                    url.append(KEYWORD_SYNTAX);
                    url.append(encodedSearchText);
                }
                url.append("&");
                url.append(OPENNOW_SYNTAX);
                url.append(OPENNOW_VALUE);
                break;
        }
        if (!StringUtils.isEmptyAfterTrim(typeString)) {
            url.append("&");
            url.append(TYPE_SYNTAX);
            url.append(typeString);
        }
        String urlString = url.toString();
        logger.d("Calling Google with URL: " + urlString.replaceAll(this.apiKEY, "key=API_KEY"));
        this.results = getSearchResults(urlString);
        if (this.handler != null) {
            this.handler.post(this.onSuccess);
        } else {
            this.onSuccess.run();
        }
    }

    @DexIgnore
    private void sendDestinationsToListener(List<GoogleTextSearchDestinationResult> destinations, Error error) {
        this.googleSearchListener.onGoogleSearchResult(destinations, this.currentQueryType, error);
    }

    @DexIgnore
    private List<GoogleTextSearchDestinationResult> getSearchResults(String url) {
        InputStream inputStream = null;
        ArrayList<GoogleTextSearchDestinationResult> resultDestinations = new ArrayList<>();
        try {
            URL url2 = new URL(url);
            inputStream = ((HttpURLConnection) url2.openConnection()).getInputStream();
            JSONObject jsonObject = new JSONObject(IOUtils.convertInputStreamToString(inputStream, "UTF-8"));
            logger.v("jsonObject received: " + jsonObject.toString());
            if (this.currentQueryType == Query.DIRECTIONS) {
                GoogleTextSearchDestinationResult destination2 = GoogleTextSearchDestinationResult.createDestinationObject(jsonObject);
                if (destination2 != null) {
                    resultDestinations.add(destination2);
                }
            } else {
                if (this.currentQueryType == Query.QUERY_AUTOCOMPLETE) {
                    JSONArray predictions = (JSONArray) jsonObject.get("predictions");
                    int predictionsLength = predictions.length();
                    for (int i = 0; i < predictionsLength; i++) {
                        GoogleTextSearchDestinationResult destination3 = GoogleTextSearchDestinationResult.createDestinationObject(predictions.getJSONObject(i));
                        if (destination3 != null) {
                            resultDestinations.add(destination3);
                        }
                    }
                } else {
                    if (this.currentQueryType != Query.DETAILS) {
                        JSONArray results2 = (JSONArray) jsonObject.get("results");
                        int resultsLength = results2.length();
                        for (int i2 = 0; i2 < resultsLength; i2++) {
                            GoogleTextSearchDestinationResult destination4 = GoogleTextSearchDestinationResult.createDestinationObject(results2.getJSONObject(i2));
                            if (destination4 != null) {
                                resultDestinations.add(destination4);
                            }
                        }
                    } else {
                        GoogleTextSearchDestinationResult destination5 = GoogleTextSearchDestinationResult.createDestinationObject((JSONObject) jsonObject.get("result"));
                        if (destination5 != null) {
                            resultDestinations.add(destination5);
                        }
                    }
                }
            }
            logger.v("resultDestinations size after parsing response: " + resultDestinations.size());
            return resultDestinations;
        } catch (Throwable e) {
            logger.v("throwable e: " + e);
            return null;
        } finally {
            IOUtils.closeStream(inputStream);
        }
    }

    @DexIgnore
    public void setListener(GoogleSearchListener listener) {
        this.googleSearchListener = listener;
    }
}
