package com.navdy.client.app.framework.glances;

import android.app.Notification;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;

import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.glances.GlanceUtils;
import com.navdy.client.debug.util.NotificationBuilder;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.events.glances.MessageConstants;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class MessagingNotificationHandler {
    @DexIgnore
    private static Logger sLogger; // = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.sLogger;

    @DexIgnore
    public static void handleMessageNotification(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        if (GlanceUtils.isThisGlanceEnabledAsWellAsGlobal(packageName)) {
            char c = 65535;
            switch (packageName.hashCode()) {
                case -1547699361:
                    if (packageName.equals(GlanceConstants.WHATS_APP)) {
                        c = 2;
                        break;
                    }
                    break;
                case 908140028:
                    if (packageName.equals(GlanceConstants.FACEBOOK_MESSENGER)) {
                        c = 3;
                        break;
                    }
                    break;
                case 979613891:
                    if (packageName.equals(GlanceConstants.SLACK)) {
                        c = 1;
                        break;
                    }
                    break;
                case 1515426419:
                    if (packageName.equals(GlanceConstants.GOOGLE_HANGOUTS)) {
                        c = 0;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    handleGoogleTalkNotification(sbn);
                    return;
                case 1:
                    handleSlackNotification(sbn);
                    return;
                case 2:
                    handleWhatsAppNotification(sbn);
                    return;
                case 3:
                    handleFacebookMessengerNotification(sbn);
                    return;
                default:
                    sLogger.w("message notification not handled [" + packageName + "]");
            }
        }
    }

    @DexIgnore
    private static void handleGoogleTalkNotification(StatusBarNotification sbn) {
        String body;
        String packageName = sbn.getPackageName();
        Notification notification = sbn.getNotification();
        String tag = sbn.getTag();
        if (!StringUtils.isEmptyAfterTrim(tag)) {
            String tag2 = tag.toLowerCase();
            if (tag2.contains(":sms:")) {
                sLogger.v("navdyinfo-hangout is sms[" + tag2 + "]");
                return;
            } else if (!tag2.contains(":")) {
                sLogger.v("navdyinfo-hangout is sms[" + tag2 + "]");
                return;
            }
        }
        if (notification.tickerText != null) {
            String contactName = "";
            Matcher matcher = NavdyCustomNotificationListenerService.splitTickerTextOnColon.matcher(notification.tickerText.toString());
            if (!matcher.matches() || matcher.groupCount() < 2) {
                body = notification.tickerText.toString();
            } else {
                contactName = matcher.group(1);
                body = matcher.group(2);
            }
            sLogger.d("[navdyinfo-hangout] hangout message received sender[" + contactName + "] message[" + body + "]");
            RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
            if (remoteDevice == null) {
                sLogger.v("[navdyinfo-hangout]  lost connection");
            } else if (!GlancesHelper.isHudVersionCompatible(remoteDevice.getDeviceInfo().protocolVersion)) {
                remoteDevice.postEvent((Message) NotificationBuilder.buildSmsNotification(contactName, contactName, body, true));
            } else {
                String id = GlancesHelper.getId();
                List<KeyValue> data = new ArrayList<>();
                data.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), contactName));
                data.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), body));
                GlancesHelper.sendEvent(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider(packageName).id(id).postTime(notification.when).glanceData(data).build());
            }
        }
    }

    @DexReplace
    private static void handleSlackNotification(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        Notification notification = sbn.getNotification();
        Bundle extras = NotificationCompat.getExtras(notification);
        String from = extras.getString("android.title");
        String message = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.text");
        String domain = extras.getString("android.summaryText");
        sLogger.v("[navdyinfo-slack] from[" + from + "] domain[" + domain + "] message[" + message + "]");
        String id = GlancesHelper.getId();
        List<KeyValue> data = new ArrayList<>();
        data.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), from));
        data.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), message));
        data.add(new KeyValue(MessageConstants.MESSAGE_DOMAIN.name(), domain));
        GlancesHelper.sendEvent(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider(packageName).id(id).postTime(notification.when).glanceData(data).build());
    }

    @DexReplace
    private static void handleWhatsAppNotification(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        Notification notification = sbn.getNotification();
        Bundle extras = NotificationCompat.getExtras(notification);
        String from = extras.getString("android.title");
        String message = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.text");
        String firstParticipantInCarConversation = NavdyCustomNotificationListenerService.getFirstParticipantInCarConversation(extras);
        if (!StringUtils.isEmptyAfterTrim(firstParticipantInCarConversation)) {
            from = firstParticipantInCarConversation;
        }
        String lastLine = NavdyCustomNotificationListenerService.getLastTextLine(extras);
        if (!StringUtils.isEmptyAfterTrim(lastLine)) {
            message = lastLine;
        } else {
            String lastMessageInCarConversation = NavdyCustomNotificationListenerService.getLastMessageInCarConversation(extras);
            if (!StringUtils.isEmptyAfterTrim(lastMessageInCarConversation)) {
                message = lastMessageInCarConversation;
            }
        }
        sLogger.v("[navdyinfo-whatsapp] from[" + from + "] message[" + message + "]");
        String id = GlancesHelper.getId();
        List<KeyValue> data = new ArrayList<>();
        data.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), from));
        data.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), message));
        GlancesHelper.sendEvent(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider(packageName).id(id).postTime(notification.when).glanceData(data).build());
    }

    @DexReplace
    private static void handleFacebookMessengerNotification(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        Notification notification = sbn.getNotification();
        Bundle extras = NotificationCompat.getExtras(notification);
        String from = extras.getString("android.title");
        String message = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.text");
        sLogger.v("[navdyinfo-fbmessenger] from[" + from + "] message[" + message + "]");
        String id = GlancesHelper.getId();
        List<KeyValue> data = new ArrayList<>();
        data.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), from));
        data.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), message));
        GlancesHelper.sendEvent(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider(packageName).id(id).postTime(notification.when).glanceData(data).build());
    }
}
