package com.navdy.client.app.framework.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.navdy.client.app.framework.glances.GlanceConstants;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.service.library.log.Logger;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public class MediaPlayerStateUpdateReceiver extends BroadcastReceiver {
    @DexIgnore
    private static /* final */ String INTENT_PARAM_PLAYING; // = "playing";
    @DexIgnore
    public static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.receiver.MediaPlayerStateUpdateReceiver.class);

    @DexReplace
    public void onReceive(Context context, Intent intent) {
        // Spotify music notifications are now disabled by default, so stop parsing for them.
        // They are now handled the same way as other music apps, via the notification
        // in com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.isMusicNotification
        // https://developer.spotify.com/documentation/android/guides/android-media-notifications/#enabling-media-notifications

        // sLogger.v("onReceive, intent: " + intent);
        // String action = intent.getAction();
        // if (!StringUtils.isEmptyAfterTrim(action)) {
        //     String packageName = action.substring(0, action.lastIndexOf(46));
        //     if (GlanceConstants.SPOTIFY.equals(packageName) && intent.getBooleanExtra(INTENT_PARAM_PLAYING, false)) {
        //         MusicServiceHandler.getInstance().setLastMusicApp(packageName);
        //     }
        // }
    }
}
