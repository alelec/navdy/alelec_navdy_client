package com.navdy.client.app.framework;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.navdy.client.BuildConfig;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.service.ClientConnectionService;
import com.navdy.client.app.framework.service.TaskRemovalService;
import com.navdy.client.app.framework.servicehandler.BatteryStatusManager;
import com.navdy.client.app.framework.servicehandler.CalendarHandler;
import com.navdy.client.app.framework.servicehandler.LocationTransmitter;
import com.navdy.client.app.framework.servicehandler.NetworkStatusManager;
import com.navdy.client.app.framework.suggestion.DestinationSuggestionService;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.SupportTicketService;
import com.navdy.client.app.service.ActivityRecognizedCallbackService;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants;
import com.navdy.client.app.ui.homescreen.CalendarUtils;
import com.navdy.client.app.ui.homescreen.HomescreenActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.ota.OTAUpdateService;
import com.navdy.service.library.Version;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.device.RemoteDeviceRegistry;
import com.navdy.service.library.device.connection.Connection;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.discovery.BTRemoteDeviceScanner;
import com.navdy.service.library.events.Capabilities;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.location.TransmitLocation;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.service.library.events.navigation.NavigationSessionStatusEvent;
import com.navdy.service.library.events.settings.DateTimeConfiguration;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.Listenable;
import com.squareup.wire.Message;

import java.util.Calendar;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD)
public class AppInstance extends Listenable<AppInstance.Listener> implements ClientConnectionService.DeviceChangeBroadcaster.Listener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    @DexIgnore
    private static long ACTIVITY_RECOGNITION_UPDATE_INTERVAL; // = java.util.concurrent.TimeUnit.SECONDS.toMillis(10);
    @DexIgnore
    private static IntentFilter DATETIME_CHANGE_INTENT_FILTER; // = new android.content.IntentFilter("android.intent.action.DATE_CHANGED");
    @DexIgnore
    private static String EMPTY; // = "";
    @DexIgnore
    private static int REQUEST_CODE; // = 34567;
    @DexIgnore
    private static long SUGGESTION_KICK_OFF_DELAY_AFTER_CONNECTED; // = 15000;
    @DexIgnore
    private static long TRIP_DB_CHECK_KICK_OFF_DELAY_AFTER_CONNECTED; // = 60000;
    // @android.annotation.SuppressLint({"StaticFieldLeak"})
    @DexIgnore
    private static volatile AppInstance sInstance;
    @DexIgnore
    private static Object sInstanceLock; // = new java.lang.Object();
    @DexIgnore
    public static Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.AppInstance.class);
    @DexIgnore
    private boolean busIsRegistered; // = false;
    @DexIgnore
    private CalendarHandler calendarHandler; // = null;
    @DexIgnore
    private BroadcastReceiver datetimeUpdateReceiver; // = new com.navdy.client.app.framework.AppInstance.Anon6();
    @DexIgnore
    private volatile boolean deviceConnected; // = false;
    @DexIgnore
    private boolean isHudMapEngineReady;
    @DexIgnore
    private GoogleApiClient mApiClient;
    @DexIgnore
    private boolean mAppInitialized; // = false;
    @DexIgnore
    private BatteryStatusManager mBatteryStatus;
    @DexIgnore
    private ClientConnectionService mConnectionService;
    @DexIgnore
    private Context mContext; // = com.navdy.client.app.NavdyApplication.getAppContext();
    @DexIgnore
    public LocationTransmitter mLocationTransmitter;
    @DexIgnore
    private Handler mMainHandler; // = new android.os.Handler(this.mContext.getMainLooper());
    @DexIgnore
    private NavigationSessionState mNavigationSessionState; // = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
    // @javax.inject.Inject
    @DexIgnore
    NetworkStatusManager mNetworkStatus;
    @DexIgnore
    private RemoteDeviceRegistry mRemoteDeviceRegistry;
    @DexIgnore
    private ServiceConnection serviceConnection; // = new com.navdy.client.app.framework.AppInstance.Anon1();

    @DexAdd
    public HomescreenActivity mHomescreenActivity;

    // @DexIgnore
    // class Anon1 implements ServiceConnection {
    //     Anon1() {
    //     }
    //
    //     public void onServiceConnected(ComponentName name, IBinder service) {
    //         if (!(service instanceof ClientConnectionService.LocalBinder)) {
    //             AppInstance.sLogger.v("service is not a LocalBinder, no-op");
    //             return;
    //         }
    //         AppInstance.sLogger.i("Connected to ClientConnectionService");
    //         AppInstance.this.mConnectionService = ((ClientConnectionService.LocalBinder) service).getService();
    //         AppInstance.this.mConnectionService.addListener(AppInstance.this);
    //         AppInstance.this.initializeDevice();
    //     }
    //
    //     public void onServiceDisconnected(ComponentName name) {
    //         AppInstance.sLogger.i("Disconnected from ClientConnectionService service");
    //         AppInstance.this.mConnectionService = null;
    //     }
    // }
    //
    // @DexIgnore
    // class Anon2 implements Runnable {
    //     final /* synthetic */ boolean val$big;
    //     final /* synthetic */ int val$toastDuration;
    //     final /* synthetic */ String val$toastText;
    //
    //     Anon2(String str, int i, boolean z) {
    //         this.val$toastText = str;
    //         this.val$toastDuration = i;
    //         this.val$big = z;
    //     }
    //
    //     public void run() {
    //         Toast toast = Toast.makeText(AppInstance.this.mContext, this.val$toastText, this.val$toastDuration);
    //         View view = toast.getView();
    //         if (this.val$big) {
    //             if (view instanceof LinearLayout) {
    //                 TextView messageTextView = (TextView) ((LinearLayout) view).getChildAt(0);
    //                 if (messageTextView != null) {
    //                     messageTextView.setTextSize(48.0f);
    //                 }
    //             }
    //             toast.setGravity(17, 0, 0);
    //         }
    //         toast.show();
    //     }
    // }

    @DexIgnore
    class Anon3 implements Runnable {
        @DexIgnore
        Anon3() {
        }

        @DexIgnore
        public void run() {
            DestinationSuggestionService.suggestDestination(AppInstance.this.mContext, false);
        }
    }

    // @DexIgnore
    // class Anon5 implements AppInstance.EventDispatcher {
    //     final /* synthetic */ RemoteDevice val$newDevice;
    //
    //     Anon5(RemoteDevice remoteDevice) {
    //         this.val$newDevice = remoteDevice;
    //     }
    //
    //     public void dispatchEvent(AppInstance source, AppInstance.Listener listener) {
    //         listener.onDeviceChanged(this.val$newDevice);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon6 extends BroadcastReceiver {
    //     Anon6() {
    //     }
    //
    //     public void onReceive(Context context, Intent intent) {
    //         RemoteDevice remoteDevice = AppInstance.this.getRemoteDevice();
    //         if (remoteDevice != null) {
    //             AppInstance.this.sendCurrentTime(remoteDevice, DateFormat.is24HourFormat(context));
    //         }
    //     }
    // }

    @DexIgnore
    interface EventDispatcher extends Listenable.EventDispatcher<AppInstance, AppInstance.Listener> {
    }

    @DexIgnore
    public interface Listener extends Listenable.Listener {
        void onDeviceChanged(RemoteDevice remoteDevice);
    }

    // static {
    //     DATETIME_CHANGE_INTENT_FILTER.addAction("android.intent.action.TIME_SET");
    //     DATETIME_CHANGE_INTENT_FILTER.addAction("android.intent.action.TIMEZONE_CHANGED");
    // }

    @DexIgnore
    private AppInstance() {
        Injector.inject(NavdyApplication.getAppContext(), this);
    }

    @DexIgnore
    public static AppInstance getInstance() {
        if (sInstance == null) {
            synchronized (sInstanceLock) {
                if (sInstance == null) {
                    sInstance = new AppInstance();
                }
            }
        }
        return sInstance;
    }

    @DexIgnore
    public synchronized void initializeApp() {
        if (!this.mAppInitialized) {
            this.mAppInitialized = true;
            initializeRegistry();
            initializeConnectionService();
            TaskRemovalService.startService(this.mContext);
            this.mApiClient = new GoogleApiClient.Builder(NavdyApplication.getAppContext()).addApi(ActivityRecognition.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
            this.mApiClient.connect();
        }
    }

    @DexIgnore
    public void initializeDevice() {
        ConnectionInfo connectionInfo = this.mRemoteDeviceRegistry.getDefaultConnectionInfo();
        if (connectionInfo != null && this.mConnectionService != null) {
            this.mConnectionService.connect(connectionInfo);
        }
    }

    @DexIgnore
    private void initializeRegistry() {
        this.mRemoteDeviceRegistry = RemoteDeviceRegistry.getInstance(this.mContext);
        this.mRemoteDeviceRegistry.addRemoteDeviceScanner(new BTRemoteDeviceScanner(this.mContext, 12));
    }

    @DexIgnore
    private void initializeConnectionService() {
        sLogger.v("init DeviceConnection");
        DeviceConnection.getInstance();
        sLogger.d("Binding to client connection service");
        Intent intent = new Intent(this.mContext, ClientConnectionService.class);
        intent.setAction(ClientConnectionService.class.getName());
        try {
            this.mContext.bindService(intent, this.serviceConnection, Context.BIND_AUTO_CREATE);
        } catch (Exception e) {
            e.printStackTrace();
            this.mAppInitialized = false;
        }
    }

    @DexReplace
    public void setLocationTransmitter(LocationTransmitter transmitter) {
        if (this.mLocationTransmitter != null) {
            this.mLocationTransmitter.stop();
        }
        this.mLocationTransmitter = transmitter;
        RemoteDevice device = this.mConnectionService.getRemoteDevice();
        if (device != null && device.isConnected() && this.mLocationTransmitter != null) {
            this.mLocationTransmitter.setHomescreenActivity(mHomescreenActivity);
            this.mLocationTransmitter.start();
        }
    }

    // @android.support.annotation.Nullable
    @DexIgnore
    public RemoteDevice getRemoteDevice() {
        if (this.mConnectionService != null) {
            return this.mConnectionService.getRemoteDevice();
        }
        return null;
    }

    @DexAdd
    public ClientConnectionService getClientConnectionService() {
        return this.mConnectionService;
    }

    @DexIgnore
    public NavigationSessionState getNavigationSessionState() {
        return this.mNavigationSessionState;
    }

    @DexIgnore
    public boolean isHudMapEngineReady() {
        return this.isHudMapEngineReady;
    }

    // @java.lang.Deprecated
    @DexIgnore
    public void showToast(String toastText, boolean big) {
        showToast(toastText, big, 1);
    }

    // @java.lang.Deprecated
    @DexIgnore
    public void showToast(String toastText, boolean big, int toastDuration) {
        // this.mMainHandler.post(new AppInstance.Anon2(toastText, toastDuration, big));
    }

    @DexReplace
    void onDeviceConnectedFirstResponder(RemoteDevice device) {
        this.deviceConnected = true;
        if (!this.busIsRegistered) {
            this.busIsRegistered = true;
            BusProvider.getInstance().register(this);
        }
        BusProvider.getInstance().post(new DeviceConnection.DeviceConnectedEvent(device));
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            if (ContextCompat.checkSelfPermission(mHomescreenActivity, Manifest.permission.ACTIVITY_RECOGNITION)
//                    != PackageManager.PERMISSION_GRANTED) {
////            PermissionCompat.requestPermission(mHomescreenActivity, "com.google.android.gms.permission.ACTIVITY_RECOGNITION", requestCode);
////        }
//
//            if (ContextCompat.checkSelfPermission(mHomescreenActivity, Manifest.permission.ACTIVITY_RECOGNITION)
//                    != PackageManager.PERMISSION_GRANTED) {
//                // Permission is not granted
//                ActivityCompat.requestPermissions(mHomescreenActivity,
//                        arrayOf(Manifest.permission.ACTIVITY_RECOGNITION),
//                        MY_PERMISSIONS_REQUEST_ACTIVITY_RECOGNITION);
//
//            }
//        }
//
        requestActivityRecognitionUpdates();
        if (device != null) {
            sendPhoneInfo(device);
            sendCurrentTime(device, DateFormat.is24HourFormat(NavdyApplication.getAppContext()));
        }
        if (this.mLocationTransmitter == null) {
            this.mLocationTransmitter = new LocationTransmitter(this.mContext, device);
            this.mLocationTransmitter.setHomescreenActivity(mHomescreenActivity);

        }
        if (this.mBatteryStatus == null) {
            this.mBatteryStatus = new BatteryStatusManager(device);
        }
        this.mNetworkStatus.onRemoteDeviceConnected(device);
        this.mLocationTransmitter.start();

        this.mContext.registerReceiver(this.datetimeUpdateReceiver, DATETIME_CHANGE_INTENT_FILTER);
        this.mBatteryStatus.register();
        Tracker.tagEvent(TrackerConstants.Event.HUD_CONNECTION_ESTABLISHED);
        LocalyticsManager.setProfileAttribute(TrackerConstants.Attributes.MOBILE_APP_VERSION, BuildConfig.VERSION_NAME);
        if (SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.RECOMMENDATIONS_ENABLED, true)) {
            this.mMainHandler.postDelayed(new AppInstance.Anon3(), 15000);
        }
        if (this.calendarHandler == null) {
            this.calendarHandler = new CalendarHandler(this.mContext);
        }
        this.calendarHandler.registerObserver();
        CalendarUtils.sendCalendarsToHud(device);
        SupportTicketService.submitTicketsIfConditionsAreMet();
        // ConnectedNotification.startService(mContext);
    }

    @DexReplace
    void onDeviceDisconnectedFirstResponder(RemoteDevice device, Connection.DisconnectCause cause) {
        // ConnectedNotification.stopService(mContext);
        String showName;
        this.mNavigationSessionState = NavigationSessionState.NAV_SESSION_STOPPED;
        this.deviceConnected = false;
        removeActivityRecognitionUpdates();
        this.isHudMapEngineReady = false;
        BusProvider.getInstance().post(new DeviceConnection.DeviceDisconnectedEvent(device, cause));
        if (this.mBatteryStatus != null) {
            this.mBatteryStatus.unregister();
            this.mBatteryStatus = null;
        }
        this.mNetworkStatus.onRemoteDeviceDisconnected();
        if (this.mLocationTransmitter != null) {
            this.mLocationTransmitter.stop();
            this.mLocationTransmitter = null;
        }
        try {
            showName = device.getDeviceId().getDisplayName();
        } catch (Exception e) {
            showName = "?";
        }
        showToast("Disconnected: " + showName, false);
        try {
            sLogger.v("unregistering datetimeUpdateReceiver");
            this.mContext.unregisterReceiver(this.datetimeUpdateReceiver);
        } catch (IllegalArgumentException e2) {
            sLogger.e("Cannot unregister datetime update receiver", e2);
        }
        Tracker.tagEvent(TrackerConstants.Event.HUD_CONNECTION_LOST);
        if (this.calendarHandler != null) {
            this.calendarHandler.unregisterObserver();
        }
        if (this.busIsRegistered) {
            this.busIsRegistered = false;
            BusProvider.getInstance().unregister(this);
        }
    }

    @DexReplace
    private synchronized void requestActivityRecognitionUpdates() {
        sLogger.d("GoogleApiClient : requesting activity updates");
        if (this.mApiClient.isConnected()) {
            sLogger.d("GoogleApiClient is already connected, requesting the updates");
            @SuppressLint("WrongConstant") PendingIntent pendingIntent = PendingIntent.getService(NavdyApplication.getAppContext(), REQUEST_CODE, new Intent(NavdyApplication.getAppContext(), ActivityRecognizedCallbackService.class), 134217728);
            if (ActivityRecognition.ActivityRecognitionApi != null) {
                try {
                    ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(this.mApiClient, ACTIVITY_RECOGNITION_UPDATE_INTERVAL, pendingIntent);
                } catch (SecurityException unused) {
                    sLogger.w("Can't start ActivityRecognition (walk vs drive) without ACTIVITY_RECOGNITION permission.");
                    // ignore
                }
            }
        } else {
            sLogger.d("The GoogleApiClient is not connected yet.Waiting for it to be connected");
        }
    }

    @DexReplace
    private synchronized void removeActivityRecognitionUpdates() {
        sLogger.d("GoogleApiClient : removing activity updates");
        if (this.mApiClient != null && this.mApiClient.isConnected()) {
            @SuppressLint("WrongConstant") PendingIntent pendingIntent = PendingIntent.getService(NavdyApplication.getAppContext(), REQUEST_CODE, new Intent(NavdyApplication.getAppContext(), ActivityRecognizedCallbackService.class), 134217728);
            if (!(ActivityRecognition.ActivityRecognitionApi == null || pendingIntent == null)) {
                try {
                    ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(this.mApiClient, pendingIntent);
                } catch (NullPointerException | SecurityException e) {
                    sLogger.e("Something went wrong while trying to remove activity recognition updates");
                }
            }
        }
        return;
    }

    @DexIgnore
    public boolean isDeviceConnected() {
        return this.deviceConnected;
    }

    @DexIgnore
    public boolean canReachInternet() {
        return this.mNetworkStatus.canReachInternet();
    }

    @DexIgnore
    public void checkForNetwork() {
        this.mNetworkStatus.checkForNetwork();
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onNavigationSessionStatusEvent(NavigationSessionStatusEvent status) {
        this.mNavigationSessionState = status.sessionState;
        if (status.sessionState == NavigationSessionState.NAV_SESSION_ENGINE_READY) {
            this.isHudMapEngineReady = true;
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDeviceInfo(DeviceInfo deviceInfo) {
        RemoteDevice device = getRemoteDevice();
        if (device != null) {
            device.setDeviceInfo(deviceInfo);
        } else {
            sLogger.e("Unable to get remote device in order to update device info!");
        }
        sLogger.i("Display DeviceInfo: " + deviceInfo);
        if (deviceInfo.capabilities != null) {
            SettingsUtils.checkCapabilities(deviceInfo.capabilities);
        } else {
            SettingsUtils.checkLegacyCapabilityList(deviceInfo.legacyCapabilities);
        }
        LocalyticsManager.setProfileAttribute(TrackerConstants.Attributes.HUD_SERIAL_NUMBER, deviceInfo.deviceUuid);
        LocalyticsManager.setProfileAttribute(TrackerConstants.Attributes.HUD_VERSION_NUMBER, deviceInfo.systemVersion);
        OTAUpdateService.bPersistDeviceInfo(deviceInfo);
        OTAUpdateService.startService(this.mContext);
        BusProvider.getInstance().post(new DeviceConnection.DeviceInfoEvent(deviceInfo));
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onTransmitLocation(TransmitLocation transmitLocation) {
        if (transmitLocation == null) {
            sLogger.e("location transmission event with a null transmit location extension !!! WTF !!!");
        } else if (Boolean.TRUE.equals(transmitLocation.sendLocation)) {
            sLogger.v("starting location transmission");
            this.mLocationTransmitter.start();
        } else {
            sLogger.v("stopping location transmission");
            this.mLocationTransmitter.stop();
        }
    }

    @DexIgnore
    public void onDeviceChanged(RemoteDevice newDevice) {
        // sLogger.d("DeviceChanged: " + newDevice);
        // dispatchToListeners(new AppInstance.Anon5(newDevice));
    }

    @DexIgnore
    private void sendPhoneInfo(RemoteDevice device) {
        if (device == null) {
            sLogger.w("called setPhoneInfo with a null RemoteDevice!");
            return;
        }
        try {
            Capabilities.Builder capabilitiesBuilder = new Capabilities.Builder();
            capabilitiesBuilder.placeTypeSearch(Boolean.valueOf(true));
            capabilitiesBuilder.voiceSearch(Boolean.valueOf(true));
            capabilitiesBuilder.navCoordsLookup(Boolean.valueOf(true));
            capabilitiesBuilder.cannedResponseToSms(Boolean.valueOf(true));
            capabilitiesBuilder.supportsPlayAudioRequest(Boolean.valueOf(true));
            Capabilities capabilities = capabilitiesBuilder.build();
            NavdyDeviceId deviceId = NavdyDeviceId.getThisDevice(this.mContext);
            String deviceName = deviceId.getDeviceName();
            if (deviceName == null) {
                deviceName = "";
            }
            DeviceInfo phoneInfo = new DeviceInfo.Builder().deviceId(deviceId.toString()).clientVersion(BuildConfig.VERSION_NAME).protocolVersion(Version.PROTOCOL_VERSION.toString()).deviceName(deviceName).systemVersion(getSystemVersion()).model(Build.MODEL).deviceUuid("UUID").systemApiLevel(Integer.valueOf(Build.VERSION.SDK_INT)).kernelVersion(System.getProperty("os.version")).platform(DeviceInfo.Platform.PLATFORM_Android).buildType(Build.TYPE).deviceMake(Build.MANUFACTURER).capabilities(capabilities).build();
            sLogger.v("sending device info:" + phoneInfo);
            device.postEvent((Message) phoneInfo);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    @DexIgnore
    private String getSystemVersion() {
        if (!String.valueOf(1).equals(Build.VERSION.INCREMENTAL)) {
            return BuildConfig.VERSION_NAME;
        }
        return Build.VERSION.INCREMENTAL;
    }

    @DexIgnore
    private void sendCurrentTime(RemoteDevice remoteDevice, boolean is24HourFormat) {
        Calendar calendar = Calendar.getInstance();
        remoteDevice.postEvent((Message) new DateTimeConfiguration(Long.valueOf(calendar.getTimeInMillis()), calendar.getTimeZone().getID(), is24HourFormat ? DateTimeConfiguration.Clock.CLOCK_24_HOUR : DateTimeConfiguration.Clock.CLOCK_12_HOUR));
    }

    @DexIgnore
    public synchronized void onConnected(@Nullable Bundle bundle) {
        sLogger.d("GoogleApiClient : connected ");
        if (isDeviceConnected()) {
            sLogger.d("Remote device is already connected");
            requestActivityRecognitionUpdates();
        }
    }

    @DexIgnore
    public void onConnectionSuspended(int i) {
        sLogger.d("GoogleApiClient : onConnectionSuspended , Connection :" + i);
    }

    @DexIgnore
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        sLogger.d("GoogleApiClient : onConnectionFailed , Connection result : " + connectionResult);
    }
}
