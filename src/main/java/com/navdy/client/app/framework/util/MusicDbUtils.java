package com.navdy.client.app.framework.util;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;

@DexEdit
public class MusicDbUtils {
    @DexIgnore
    private static /* final */ java.lang.String CHARACTER_MAP_SELECTION; // = "1=1) GROUP BY (letter";
    @DexIgnore
    public static /* final */ java.lang.String CHARACTER_MAP_SORT; // = "CASE\n  WHEN letter GLOB '[A-Z]' THEN 0\n  WHEN letter GLOB '[0-9]' THEN 1\n  ELSE 2\nEND ASC";
    @DexIgnore
    private static /* final */ java.lang.String COUNT; // = "count(*) AS count";
    @DexIgnore
    public static /* final */ java.lang.String COUNT_COLUMN; // = "count";
    @DexIgnore
    private static /* final */ java.lang.String FIRST_LETTER_FORMAT; // = "SUBSTR(UPPER(%s), 1, 1) AS letter";
    @DexIgnore
    private static /* final */ java.lang.String GPM_PLAYLISTS_BASE_URI; // = "content://com.google.android.music.MusicContent/playlists";
    @DexIgnore
    public static /* final */ java.lang.String GPM_PLAYLISTS_COLUMN_PLAYLIST_NAME; // = "playlist_name";
    @DexIgnore
    private static /* final */ java.lang.String GPM_PLAYLISTS_MEMBERS_PATH; // = "members";
    @DexIgnore
    public static /* final */ java.lang.String GPM_PLAYLIST_MEMBERS_COLUMN_ALBUM; // = "album";
    @DexIgnore
    public static /* final */ java.lang.String GPM_PLAYLIST_MEMBERS_COLUMN_ARTIST; // = "artist";
    @DexIgnore
    public static /* final */ java.lang.String GPM_PLAYLIST_MEMBERS_COLUMN_SOURCE_ID; // = "SourceId";
    @DexIgnore
    public static /* final */ java.lang.String GPM_PLAYLIST_MEMBERS_COLUMN_TITLE; // = "title";
    @DexIgnore
    public static /* final */ java.lang.String LETTER_COLUMN; // = "letter";
    @DexIgnore
    public static /* final */ java.lang.String ORDER_FORMAT; // = "CASE%n  WHEN UPPER(%1$s) GLOB '[A-Z]*' THEN 0%n  WHEN UPPER(%1$s) GLOB '[0-9]*' THEN 1%n  ELSE 2%nEND ASC, UPPER(%1$s);";
    @DexIgnore
    private static com.navdy.service.library.log.Logger logger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.MusicDbUtils.class);

    @DexIgnore
    public static android.database.Cursor getPlaylistsCursor() {
        return com.navdy.client.app.providers.NavdyContentProvider.getPlaylistsCursor();
    }

    @DexIgnore
    public static android.database.Cursor getPlaylistMembersCursor(java.lang.String playlistIdStr) {
        try {
            return getPlaylistMembersCursor(java.lang.Integer.parseInt(playlistIdStr));
        } catch (java.lang.NumberFormatException e) {
            logger.e("Invalid collection ID: " + playlistIdStr);
            return null;
        }
    }

    @DexIgnore
    public static android.database.Cursor getPlaylistMembersCursor(int playlistId) {
        return com.navdy.client.app.providers.NavdyContentProvider.getPlaylistMembersCursor(playlistId);
    }

    @DexIgnore
    public static android.database.Cursor getAlbumsCursor() {
        return com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver().query(android.provider.MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, new java.lang.String[]{"_id", "album", "numsongs", "artist"}, null, null, getOrderString("album"));
    }

    @DexIgnore
    public static android.database.Cursor getAlbumMembersCursor(java.lang.String albumId) {
        return com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver().query(android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new java.lang.String[]{"title", "artist", "album", "_id"}, "album_id = ?", new java.lang.String[]{albumId}, null);
    }

    @DexIgnore
    public static android.database.Cursor getArtistsCursor() {
        return com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver().query(android.provider.MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI, new java.lang.String[]{"_id", "artist"}, null, null, getOrderString("artist"));
    }

    @DexIgnore
    public static android.database.Cursor getArtistMembersCursor(java.lang.String artistId) {
        return com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver().query(android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new java.lang.String[]{"title", "artist", "album", "_id"}, "artist_id = ?", new java.lang.String[]{artistId}, null);
    }

    @DexIgnore
    public static int getArtistSongsCount(java.lang.String artistId) {
        android.database.Cursor cursor = null;
        int count = -1;
        try {
            cursor = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver().query(android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new java.lang.String[]{COUNT}, "artist_id = ?", new java.lang.String[]{artistId}, null);
            if (cursor != null && cursor.moveToFirst()) {
                count = cursor.getInt(cursor.getColumnIndex(COUNT_COLUMN));
            }
            return count;
        } finally {
            com.navdy.service.library.util.IOUtils.closeObject(cursor);
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x004b  */
    @DexIgnore
    public static android.database.Cursor getArtistsAlbums(java.lang.String artistId) {
        throw null;
        // android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        // android.net.Uri membersUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        // java.lang.String[] membersColumns = {"DISTINCT album_id"};
        // java.lang.String query = "artist_id = ?";
        // java.lang.String[] args = {artistId};
        // android.database.Cursor tracksCursor = null;
        // java.util.Set<java.lang.Integer> albumIds = new java.util.HashSet<>();
        // try {
        //     android.database.Cursor tracksCursor2 = contentResolver.query(membersUri, membersColumns, query, args, null);
        //     if (tracksCursor2 == null || !tracksCursor2.moveToFirst()) {
        //         if (tracksCursor2 != null) {
        //             tracksCursor2.close();
        //         }
        //         java.lang.StringBuilder queryBuilder = new java.lang.StringBuilder();
        //         queryBuilder.append("_id IN (");
        //         int count = albumIds.size();
        //         int i = 0;
        //         java.lang.String[] args2 = new java.lang.String[count];
        //         for (java.lang.Integer num : albumIds) {
        //             int i2 = i + 1;
        //             args2[i] = num.toString();
        //             if (i2 <= count - 1) {
        //                 queryBuilder.append("?,");
        //                 i = i2;
        //             } else if (i2 == count) {
        //                 queryBuilder.append("?)");
        //                 i = i2;
        //             } else {
        //                 i = i2;
        //             }
        //         }
        //         java.lang.String query2 = queryBuilder.toString();
        //         return contentResolver.query(android.provider.MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, new java.lang.String[]{"_id", "album", "numsongs", "artist"}, query2, args2, getOrderString("album"));
        //     }
        //     do {
        //         albumIds.add(java.lang.Integer.valueOf(tracksCursor2.getInt(tracksCursor2.getColumnIndex("album_id"))));
        //     } while (tracksCursor2.moveToNext());
        //     if (tracksCursor2 != null) {
        //     }
        //     java.lang.StringBuilder queryBuilder2 = new java.lang.StringBuilder();
        //     queryBuilder2.append("_id IN (");
        //     int count2 = albumIds.size();
        //     int i3 = 0;
        //     java.lang.String[] args22 = new java.lang.String[count2];
        //     while (albumIdIterator.hasNext()) {
        //     }
        //     java.lang.String query22 = queryBuilder2.toString();
        //     return contentResolver.query(android.provider.MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, new java.lang.String[]{"_id", "album", "numsongs", "artist"}, query22, args22, getOrderString("album"));
        // } catch (Throwable th) {
        //     if (tracksCursor != null) {
        //         tracksCursor.close();
        //     }
        //     throw th;
        // }
    }

    @DexIgnore
    public static android.database.Cursor getPlaylistsCharacterMapCursor() {
        return com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver().query(com.navdy.client.app.providers.NavdyContentProviderConstants.PLAYLISTS_CONTENT_URI, getCharacterMapProjection("playlist_name"), CHARACTER_MAP_SELECTION, null, CHARACTER_MAP_SORT);
    }

    @DexIgnore
    public static android.database.Cursor getAlbumsCharacterMapCursor() {
        return com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver().query(android.provider.MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, getCharacterMapProjection("album"), CHARACTER_MAP_SELECTION, null, CHARACTER_MAP_SORT);
    }

    @DexIgnore
    public static android.database.Cursor getArtistsCharacterMapCursor() {
        return com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver().query(android.provider.MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI, getCharacterMapProjection("artist"), CHARACTER_MAP_SELECTION, null, CHARACTER_MAP_SORT);
    }

    @DexIgnore
    public static android.net.Uri getGpmPlaylistsUri() {
        return android.net.Uri.parse(GPM_PLAYLISTS_BASE_URI);
    }

    @DexIgnore
    private static android.net.Uri getGpmPlaylistUri(int playlistId) {
        return android.net.Uri.parse("content://com.google.android.music.MusicContent/playlists/" + playlistId);
    }

    @DexIgnore
    private static android.net.Uri getGpmPlaylistMembersUri(int playlistId) {
        return android.net.Uri.parse("content://com.google.android.music.MusicContent/playlists/" + playlistId + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + GPM_PLAYLISTS_MEMBERS_PATH);
    }

    @DexIgnore
    public static android.database.Cursor getGpmPlaylistsCursor() {
        android.database.Cursor cursor = null;
        try {
            return com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver().query(getGpmPlaylistsUri(), new java.lang.String[]{"_id", "playlist_name"}, null, null, "playlist_name");
        } catch (Throwable t) {
            logger.e("Couldn't get GPM playlists cursor: " + t);
            return cursor;
        }
    }

    @DexIgnore
    public static android.database.Cursor getGpmPlaylistCursor(int playlistId) {
        android.database.Cursor cursor = null;
        try {
            return com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver().query(getGpmPlaylistUri(playlistId), new java.lang.String[]{"playlist_name"}, null, null, null);
        } catch (Throwable t) {
            logger.e("Couldn't get GPM playlist cursor for playlist " + playlistId + com.navdy.client.app.framework.util.CrashlyticsAppender.SEPARATOR + t);
            return cursor;
        }
    }

    @DexIgnore
    public static android.database.Cursor getGpmPlaylistMembersCursor(int playlistId) {
        android.database.Cursor cursor = null;
        try {
            return com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver().query(getGpmPlaylistMembersUri(playlistId), new java.lang.String[]{"title", "artist", "album", "SourceId"}, null, null, null);
        } catch (SecurityException t) {
            logger.e("Couldn't get GPM playlist members cursor for playlist " + playlistId + com.navdy.client.app.framework.util.CrashlyticsAppender.SEPARATOR + t + "\nTry opening Google Play Music and accepting any/all permission requests");
            return cursor;
        } catch (Throwable t) {
            logger.e("Couldn't get GPM playlist members cursor for playlist " + playlistId + com.navdy.client.app.framework.util.CrashlyticsAppender.SEPARATOR + t);
            return cursor;
        }
    }

    @DexIgnore
    public static java.lang.String getOrderString(java.lang.String nameColumn) {
        return java.lang.String.format(ORDER_FORMAT, nameColumn);
    }

    @DexIgnore
    public static java.lang.String[] getCharacterMapProjection(java.lang.String nameColumn) {
        return new java.lang.String[]{COUNT, java.lang.String.format(FIRST_LETTER_FORMAT, nameColumn)};
    }
}
