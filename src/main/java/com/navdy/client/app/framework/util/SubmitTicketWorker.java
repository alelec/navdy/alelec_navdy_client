package com.navdy.client.app.framework.util;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;

import com.alelec.navdyclient.R;
import com.alelec.navdyclient.BuildConfig;
import com.navdy.client.R.string;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.util.SupportTicketService.OnLogCollectedInterface;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.app.ui.settings.ZendeskConstants;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.zendesk.sdk.model.request.CreateRequest;
import com.zendesk.sdk.model.request.CustomField;
import com.zendesk.sdk.model.request.UploadResponse;
import com.zendesk.sdk.network.RequestProvider;
import com.zendesk.sdk.network.impl.ProviderStore;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.service.ErrorResponse;
import com.zendesk.service.ZendeskCallback;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class SubmitTicketWorker {
    @DexIgnore
    private static String ANDROID_BACKGROUND_SUBMISSION; // = "android_background_submission";
    @DexIgnore
    private static int INTERNET_ERROR_CODE; // = -1;
    @DexIgnore
    private static int MALFORMED_TICKET; // = 422;
    @DexIgnore
    private static long TEN_DAYS_IN_MILLIS; // = java.util.concurrent.TimeUnit.DAYS.toMillis(10);
    @DexIgnore
    private static boolean VERBOSE; // = true;
    @DexIgnore
    private static String ZIP_MIME_TYPE; // = "application/zip";
    @DexIgnore
    private static Logger logger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.SubmitTicketWorker.class);
    @DexIgnore
    private OnFinishSubmittingTickets callback;
    @DexIgnore
    private File displayLog;
    @DexIgnore
    private int failCount; // = 0;
    @DexIgnore
    private AtomicBoolean isRunning; // = new java.util.concurrent.atomic.AtomicBoolean(false);
    @DexIgnore
    private ProviderStore providerStore; // = com.zendesk.sdk.network.impl.ZendeskConfig.INSTANCE.provider();
    @DexIgnore
    private boolean showSuccessNotification;
    @DexIgnore
    private int successCount; // = 0;
    @DexIgnore
    private int ticketCount; // = 0;

    // @DexIgnore
    // class Anon1 implements Runnable {
    //     final /* synthetic */ File val$ticketFolder;
    //
    //     Anon1(File file) {
    //         this.val$ticketFolder = file;
    //     }
    //
    //     public void run() {
    //         if (!this.val$ticketFolder.exists()) {
    //             SubmitTicketWorker.this.incrementFailureCount();
    //             return;
    //         }
    //         SubmitTicketWorker.logger.i("Now processing: " + this.val$ticketFolder.getName());
    //         try {
    //             if (System.currentTimeMillis() - SubmitTicketWorker.TEN_DAYS_IN_MILLIS >= this.val$ticketFolder.lastModified()) {
    //                 SubmitTicketWorker.logger.v("deleting old folder: " + this.val$ticketFolder.getAbsolutePath());
    //                 IOUtils.deleteDirectory(NavdyApplication.getAppContext(), this.val$ticketFolder);
    //                 SubmitTicketWorker.this.incrementSuccessCount();
    //                 return;
    //             }
    //             File attachments = new File(this.val$ticketFolder.getAbsolutePath() + File.separator + "attachments.zip");
    //             SubmitTicketWorker.logger.v("attachment filepath: " + this.val$ticketFolder.getAbsolutePath() + File.separator + "attachments.zip");
    //             File content = new File(this.val$ticketFolder.getAbsolutePath() + File.separator + ZendeskConstants.TICKET_JSON_FILENAME);
    //             SubmitTicketWorker.logger.v("content filepath: " + this.val$ticketFolder.getAbsolutePath() + File.separator + ZendeskConstants.TICKET_JSON_FILENAME);
    //             JSONObject jsonObject = SubmitTicketWorker.getJSONObjectFromFile(content);
    //             if (jsonObject == null) {
    //                 SubmitTicketWorker.logger.d("No JSON Object found in the folder " + this.val$ticketFolder.getName());
    //                 SubmitTicketWorker.this.incrementFailureCount();
    //             } else if (!jsonObject.optString(ZendeskConstants.TICKET_ACTION, "").equals(ZendeskConstants.ACTION_FETCH_HUD_LOG_UPLOAD_DELETE)) {
    //                 SubmitTicketWorker.this.submitTicket(jsonObject, attachments, this.val$ticketFolder);
    //             } else if (SubmitTicketWorker.this.displayLog == null || !SubmitTicketWorker.this.displayLog.exists()) {
    //                 SubmitTicketWorker.this.incrementSuccessCount();
    //             } else {
    //                 SubmitTicketWorker.this.attachLogsAndUpdateTicketAction(jsonObject, new File[]{SubmitTicketWorker.this.displayLog}, attachments);
    //                 SubmitTicketWorker.this.submitTicket(jsonObject, attachments, this.val$ticketFolder);
    //             }
    //         } catch (Exception e) {
    //             SubmitTicketWorker.logger.e("Exception was found: " + e);
    //             SubmitTicketWorker.this.incrementFailureCount();
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon2 extends ZendeskCallback<UploadResponse> {
    //     final /* synthetic */ File val$folder;
    //     final /* synthetic */ JSONObject val$ticketContents;
    //
    //     Anon2(File file, JSONObject jSONObject) {
    //         this.val$folder = file;
    //         this.val$ticketContents = jSONObject;
    //     }
    //
    //     public void onSuccess(UploadResponse uploadResponse) {
    //         SubmitTicketWorker.logger.v("file successfully uploaded");
    //         SubmitTicketWorker.this.sendToZenddesk(this.val$folder, this.val$ticketContents, uploadResponse.getToken());
    //     }
    //
    //     public void onError(ErrorResponse errorResponse) {
    //         SubmitTicketWorker.logger.v("file failed to upload: " + errorResponse.getReason());
    //         SubmitTicketWorker.this.incrementFailureCount();
    //     }
    // }
    //
    // @DexIgnore
    // class Anon3 extends ZendeskCallback<CreateRequest> {
    //     final /* synthetic */ Context val$appContext;
    //     final /* synthetic */ File val$folder;
    //     final /* synthetic */ JSONObject val$ticketContents;
    //
    //     Anon3(JSONObject jSONObject, File file, Context context) {
    //         this.val$ticketContents = jSONObject;
    //         this.val$folder = file;
    //         this.val$appContext = context;
    //     }
    //
    //     public void onSuccess(CreateRequest createRequest) {
    //         SubmitTicketWorker.logger.d("ticket request successful");
    //         SubmitTicketWorker.this.showSuccessNotification = true;
    //         BaseActivity.showShortToast(string.zendesk_submit_succeeded);
    //         Tracker.tagEvent(Event.SUPPORT_TICKET_CREATED);
    //         try {
    //             if (this.val$ticketContents.optString(ZendeskConstants.TICKET_ACTION, "").equals(ZendeskConstants.ACTION_UPLOAD_AWAIT_HUD_LOG)) {
    //                 this.val$ticketContents.put(ZendeskConstants.TICKET_ACTION, ZendeskConstants.ACTION_FETCH_HUD_LOG_UPLOAD_DELETE);
    //                 SupportTicketService.writeJsonObjectToFile(this.val$ticketContents, this.val$folder.getAbsolutePath(), SubmitTicketWorker.logger);
    //                 SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.TICKETS_AWAITING_HUD_LOG_EXIST, true).apply();
    //             } else {
    //                 SubmitTicketWorker.logger.d("deleting submitted ticket folder: " + this.val$folder.getAbsolutePath());
    //                 IOUtils.deleteDirectory(this.val$appContext, this.val$folder);
    //             }
    //         } catch (Exception e) {
    //             SubmitTicketWorker.logger.e("exception found trying to read JSON object: " + e);
    //         }
    //         SubmitTicketWorker.this.incrementSuccessCount();
    //         SupportTicketService.resetRetryCount();
    //     }
    //
    //     public void onError(ErrorResponse errorResponse) {
    //         SubmitTicketWorker.logger.e("ticket request failed: " + errorResponse.getReason());
    //         switch (errorResponse.getStatus()) {
    //             case -1:
    //                 SubmitTicketWorker.logger.e("Internet error");
    //                 break;
    //             case SubmitTicketWorker.MALFORMED_TICKET /*422*/:
    //                 SubmitTicketWorker.logger.e("Ticket is malformed");
    //                 break;
    //             default:
    //                 SubmitTicketWorker.logger.e("Ticket request failed: " + errorResponse.getReason());
    //                 break;
    //         }
    //         SubmitTicketWorker.this.incrementFailureCount();
    //     }
    // }
    //
    // @DexIgnore
    // class Anon4 implements OnLogCollectedInterface {
    //     final /* synthetic */ File[] val$folders;
    //
    //     Anon4(File[] fileArr) {
    //         this.val$folders = fileArr;
    //     }
    //
    //     public void onLogCollectionSucceeded(ArrayList<File> logAttachments) {
    //         SubmitTicketWorker.logger.d("fetching hud log succeeded");
    //         if (!logAttachments.isEmpty()) {
    //             Iterator it = logAttachments.iterator();
    //             while (true) {
    //                 if (!it.hasNext()) {
    //                     break;
    //                 }
    //                 File log = (File) it.next();
    //                 if (log.getName().contains("display_log")) {
    //                     SubmitTicketWorker.this.displayLog = log;
    //                     break;
    //                 }
    //             }
    //         }
    //         SubmitTicketWorker.this.processAllTickets(this.val$folders);
    //     }
    //
    //     public void onLogCollectionFailed() {
    //         SubmitTicketWorker.logger.d("fetching hud log failed.");
    //         SubmitTicketWorker.this.processAllTickets(this.val$folders);
    //     }
    // }

    @DexIgnore
    public interface OnFinishSubmittingTickets {
        void onFinish(int i, boolean z);
    }

    SubmitTicketWorker() {
    }

    @DexIgnore
    public synchronized void start(OnFinishSubmittingTickets onFinishSubmittingTickets, File[] folders) {
        this.ticketCount = folders.length;
        this.callback = onFinishSubmittingTickets;
        if (folders.length > 0) {
            this.isRunning.set(true);
            if (!SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.TICKETS_AWAITING_HUD_LOG_EXIST, false) || !AppInstance.getInstance().isDeviceConnected()) {
                processAllTickets(folders);
            } else {
                fetchHudLogAndProcessTickets(folders);
            }
        } else if (this.callback != null) {
            this.callback.onFinish(this.failCount, this.showSuccessNotification);
        }
    }

    @DexIgnore
    private synchronized void processNextTicketFolder(File ticketFolder) {
        // TaskManager.getInstance().execute(new Anon1(ticketFolder), 9);
    }

    @DexIgnore
    static JSONObject getJSONObjectFromFile(File file) {
        try {
            JSONObject jsonObject = new JSONObject(getStringFromFile(file));
            logger.v("jsonObject status: " + jsonObject);
            return jsonObject;
        } catch (Exception e) {
            logger.e("getting the JSON Object from file failed: " + e);
            return null;
        }
    }

    @DexIgnore
    private static String getStringFromFile(File file) throws Exception {
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            String fileString = convertStreamToString(fileInputStream);
            fileInputStream.close();
            if (!StringUtils.isEmptyAfterTrim(fileString)) {
                return fileString;
            }
            logger.e("getStringFromFile failed");
            return null;
        } catch (Exception e) {
            logger.e("Exception found: " + e);
            return null;
        }
    }

    @DexIgnore
    private static String convertStreamToString(InputStream inputStream) throws Exception {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            while (true) {
                String line = bufferedReader.readLine();
                if (line == null) {
                    break;
                }
                stringBuilder.append(line).append("\n");
            }
            bufferedReader.close();
            if (!StringUtils.isEmptyAfterTrim(stringBuilder)) {
                return stringBuilder.toString();
            }
            logger.e("convertStreamToString failed");
            return null;
        } catch (Exception e) {
            logger.e("Exception found: " + e);
            return null;
        }
    }

    @DexReplace
    private synchronized void submitTicket(JSONObject ticketContents, File ticketAttachment, File folder) {
        logger.v("submitTicket content status: " + ticketContents + ", attachment status : " + ticketAttachment);
        if (ticketContents == null) {
            logger.e("Ticket contents are null. Deleting folder for invalid support request form:" + folder.getAbsolutePath());
            IOUtils.deleteDirectory(NavdyApplication.getAppContext(), folder);
            incrementFailureCount();
        } else {
            // if (ticketAttachment != null) {
            //     if (ticketAttachment.length() > 0) {
            //         logger.v("ticket attachment size: " + ticketAttachment.length());
            //         this.providerStore.uploadProvider().uploadAttachment("attachments.zip", ticketAttachment, "application/zip", new Anon2(folder, ticketContents));
            //     }
            // }
            sendToZenddesk(folder, ticketContents, null);
        }
    }

    @DexReplace
    private synchronized void sendToZenddesk(File folder, JSONObject ticketContents, String attachmentToken) {
        logger.v("sendToGitlab");

        String ticketType = ticketContents.optString(ZendeskConstants.TICKET_TYPE, "");
        String ticketDescription = ticketContents.optString(ZendeskConstants.TICKET_DESCRIPTION, "");
        String vin = ticketContents.optString(ZendeskConstants.TICKET_VIN, "");
        String email = ticketContents.optString(ZendeskConstants.TICKET_EMAIL, "");
        logger.v("ticketDescription: " + ticketDescription + ", ticketType: " + ticketType);
        if (StringUtils.isEmptyAfterTrim(ticketType) || StringUtils.isEmptyAfterTrim(ticketDescription)) {
            logger.e("ticket contents were null.");
            incrementFailureCount();
        } else {

            final Context appContext = NavdyApplication.getAppContext();

            File attachments = new File(folder.getAbsolutePath() + File.separator + "attachments.zip");
            SubmitTicketWorker.logger.v("attachment filepath: " + folder.getAbsolutePath() + File.separator + "attachments.zip");
//            File content = new File(folder.getAbsolutePath() + File.separator + ZendeskConstants.TICKET_JSON_FILENAME);
            SubmitTicketWorker.logger.v("content filepath: " + folder.getAbsolutePath() + File.separator + ZendeskConstants.TICKET_JSON_FILENAME);

            String details =
                    "<h2>" + ticketType.replace('_', ' ') + "</h2>" +
                            ticketDescription + "<p/>" +
                            "<br>VEHICLE_MAKE: " + SettingsUtils.getCustomerPreferences().getString(ProfilePreferences.CAR_MAKE, "unknown") +
                            "<br>VEHICLE_MODEL: " + SettingsUtils.getCustomerPreferences().getString(ProfilePreferences.CAR_MODEL, "unknown") +
                            "<br>VEHICLE_YEAR: " + SettingsUtils.getCustomerPreferences().getString(ProfilePreferences.CAR_YEAR, "unknown") +
                            "<br>MOBILE_APP_LANGUAGE: " + Locale.getDefault().toString() +
                            "<br>MOBILE_APP_VERSION: " + BuildConfig.VERSION_NAME +
                            "<br>MOBILE_APP_NAME: " + appContext.getString(R.string.app_name) +
                            "<br>MOBILE_DEVICE_MODEL: " + Build.MODEL +
                            "<br>MOBILE_DEVICE_MAKE: " + Build.MANUFACTURER +
                            "<br>MOBILE_OS_VERSION: " + VERSION.RELEASE;

            RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
            DeviceInfo deviceInfo = null;
            if (remoteDevice != null) {
                deviceInfo = remoteDevice.getDeviceInfo();
            }

            Boolean sent = GitlabSender.uploadReport(
                    Build.VERSION.INCREMENTAL,
                    details,
                    email,
                    deviceInfo,
                    attachments
            );
            if (sent) {
                SubmitTicketWorker.logger.d("ticket request successful");
                SubmitTicketWorker.this.showSuccessNotification = true;
                BaseActivity.showShortToast(R.string.zendesk_submit_succeeded);
                Tracker.tagEvent(Event.SUPPORT_TICKET_CREATED);
                SubmitTicketWorker.logger.d("deleting submitted ticket folder: " + folder.getAbsolutePath());
                IOUtils.deleteDirectory(appContext, folder);
                SubmitTicketWorker.this.incrementSuccessCount();
                SupportTicketService.resetRetryCount();
            } else {
                SubmitTicketWorker.logger.e("ticket request failed");
                SubmitTicketWorker.this.incrementFailureCount();
            }
        }

//
//            ArrayList<CustomField> customFields = new ArrayList();
//            CreateRequest request = new CreateRequest();
//            request.setDescription(ticketDescription);
//            try {
//                if (ticketContents.getString(ZendeskConstants.TICKET_ACTION).equals(ZendeskConstants.ACTION_FETCH_HUD_LOG_UPLOAD_DELETE)) {
//                    logger.d("adding tag for android background submission");
//                    request.setTags(Collections.singletonList(ANDROID_BACKGROUND_SUBMISSION));
//                }
//            } catch (Exception e) {
//                logger.e("JSON Exception: " + e);
//            }
//            if (!StringUtils.isEmptyAfterTrim(attachmentToken)) {
//                ArrayList<String> attachmentTokenList = new ArrayList();
//                attachmentTokenList.add(attachmentToken);
//                request.setAttachments(attachmentTokenList);
//            }
//            addCustomField(customFields, ZendeskConstants.PROBLEM_TYPES.longValue(), ticketType);
//            if (!StringUtils.isEmptyAfterTrim(email)) {
//                request.setEmail(email);
//            }
//            if (!StringUtils.isEmptyAfterTrim(vin)) {
//                addCustomField(customFields, ZendeskConstants.VIN.longValue(), vin);
//            }
//
//            addCustomFieldFromSharedPref(customFields, ZendeskConstants.VEHICLE_MAKE.longValue(), ProfilePreferences.CAR_MAKE);
//            addCustomFieldFromSharedPref(customFields, ZendeskConstants.VEHICLE_MODEL.longValue(), ProfilePreferences.CAR_MODEL);
//            addCustomFieldFromSharedPref(customFields, ZendeskConstants.VEHICLE_YEAR.longValue(), ProfilePreferences.CAR_YEAR);
//            addCustomField(customFields,                ZendeskConstants.MOBILE_APP_VERSION.longValue(), BuildConfig.VERSION_NAME);
//            addCustomField(customFields,                ZendeskConstants.MOBILE_APP_NAME.longValue(), appContext.getString(R.string.app_name));
//            addCustomField(customFields,                ZendeskConstants.MOBILE_DEVICE_MODEL.longValue(), Build.MODEL);
//            addCustomField(customFields,                ZendeskConstants.MOBILE_DEVICE_MAKE.longValue(), Build.MANUFACTURER);
//            addCustomField(customFields,                ZendeskConstants.MOBILE_OS_VERSION.longValue(), String.valueOf(VERSION.RELEASE));
////            RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
////            if (remoteDevice != null) {
////                DeviceInfo info = remoteDevice.getDeviceInfo();
////                if (info != null) {
////                    addCustomField(customFields, ZendeskConstants.DISPLAY_SOFTWARE_VERSION.longValue(), info.systemVersion);
////                    addCustomField(customFields, ZendeskConstants.DISPLAY_SERIAL_NUMBER.longValue(), info.deviceUuid);
////                }
////            }
//            logger.i("setting custom fields: " + customFields);
//            request.setCustomFields(customFields);
//            ZendeskConfig.INSTANCE.setCustomFields(customFields);
//            final JSONObject jSONObject = ticketContents;
//            final File file = folder;
//            this.providerStore.requestProvider().createRequest(request, new ZendeskCallback<CreateRequest>() {
//                public void onSuccess(CreateRequest createRequest) {
//                    SubmitTicketWorker.logger.d("ticket request successful");
//                    SubmitTicketWorker.this.showSuccessNotification = true;
//                    BaseActivity.showShortToast(R.string.zendesk_submit_succeeded, new Object[0]);
//                    Tracker.tagEvent(Event.SUPPORT_TICKET_CREATED);
//                    try {
//                        if (jSONObject.optString(ZendeskConstants.TICKET_ACTION, "").equals(ZendeskConstants.ACTION_UPLOAD_AWAIT_HUD_LOG)) {
//                            jSONObject.put(ZendeskConstants.TICKET_ACTION, ZendeskConstants.ACTION_FETCH_HUD_LOG_UPLOAD_DELETE);
//                            SupportTicketService.writeJsonObjectToFile(jSONObject, file.getAbsolutePath(), SubmitTicketWorker.logger);
//                            SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.TICKETS_AWAITING_HUD_LOG_EXIST, true).apply();
//                        } else {
//                            SubmitTicketWorker.logger.d("deleting submitted ticket folder: " + file.getAbsolutePath());
//                            IOUtils.deleteDirectory(appContext, file);
//                        }
//                    } catch (Exception e) {
//                        SubmitTicketWorker.logger.e("exception found trying to read JSON object: " + e);
//                    }
//                    SubmitTicketWorker.this.incrementSuccessCount();
//                    SupportTicketService.resetRetryCount();
//                }
//
//                public void onError(ErrorResponse errorResponse) {
//                    SubmitTicketWorker.logger.e("ticket request failed: " + errorResponse.getReason());
//                    switch (errorResponse.getStatus()) {
//                        case -1:
//                            SubmitTicketWorker.logger.e("Internet error");
//                            break;
//                        case SubmitTicketWorker.MALFORMED_TICKET /*422*/:
//                            SubmitTicketWorker.logger.e("Ticket is malformed");
//                            break;
//                        default:
//                            SubmitTicketWorker.logger.e("Ticket request failed: " + errorResponse.getReason());
//                            break;
//                    }
//                    SubmitTicketWorker.this.incrementFailureCount();
//                }
//            });
//        }
//        return;
    }

    @DexIgnore
    private void addCustomField(ArrayList<CustomField> customFields, long field, String value) {
        if (!StringUtils.isEmptyAfterTrim(value)) {
            customFields.add(new CustomField(Long.valueOf(field), value));
        }
    }

    @DexIgnore
    private void addCustomFieldFromSharedPref(ArrayList<CustomField> customFields, long field, String sharedPrefKey) {
        String value = SettingsUtils.getCustomerPreferences().getString(sharedPrefKey, null);
        if (!StringUtils.isEmptyAfterTrim(value)) {
            customFields.add(new CustomField(Long.valueOf(field), value));
        }
    }

    @DexIgnore
    private synchronized void fetchHudLogAndProcessTickets(File[] folders) {
        // logger.d("fetchHudLogAndProcessTickets");
        // if (this.displayLog == null) {
        //     SupportTicketService.collectHudLog(new Anon4(folders));
        // } else {
        //     logger.d("hud log exists: " + this.displayLog);
        //     processAllTickets(folders);
        // }
    }

    @DexIgnore
    private void processAllTickets(File[] folders) {
        logger.d("processAllTickets");
        for (File folder : folders) {
            processNextTicketFolder(folder);
        }
    }

    @DexIgnore
    private void attachLogsAndUpdateTicketAction(JSONObject ticketContents, File[] logs, File attachmentsZipFile) {
        logger.d("attachLogsAndUpdateTicketAction");
        IOUtils.compressFilesToZip(NavdyApplication.getAppContext(), logs, attachmentsZipFile.getAbsolutePath());
        try {
            ticketContents.put(ZendeskConstants.TICKET_HAS_DISPLAY_LOG, true);
            if (ticketContents.optString(ZendeskConstants.TICKET_ACTION, "").equals(ZendeskConstants.ACTION_UPLOAD_AWAIT_HUD_LOG)) {
                ticketContents.put(ZendeskConstants.TICKET_ACTION, ZendeskConstants.ACTION_UPLOAD_DELETE);
            }
        } catch (Exception e) {
            logger.e("JSON Exception found: " + e);
        }
    }

    @DexIgnore
    public synchronized boolean isRunning() {
        return this.isRunning.get();
    }

    @DexIgnore
    private synchronized void incrementSuccessCount() {
        this.successCount++;
        logger.i("incrementSuccessCount: " + this.successCount);
        finishIfAllTicketsAttemptedSubmission();
    }

    @DexIgnore
    private synchronized void incrementFailureCount() {
        this.failCount++;
        logger.i("incrementFailureCount: " + this.failCount);
        finishIfAllTicketsAttemptedSubmission();
    }

    @DexIgnore
    private synchronized void finishIfAllTicketsAttemptedSubmission() {
        if (this.successCount + this.failCount == this.ticketCount) {
            this.isRunning.set(false);
            this.callback.onFinish(this.failCount, this.showSuccessNotification);
        }
    }
}
