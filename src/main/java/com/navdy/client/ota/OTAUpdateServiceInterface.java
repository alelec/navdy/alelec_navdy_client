package com.navdy.client.ota;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexEdit(defaultAction = DexAction.IGNORE)
public interface OTAUpdateServiceInterface extends android.os.IBinder {
    @DexIgnore
    void cancelDownload();

    @DexIgnore
    void cancelUpload();

    @DexIgnore
    boolean checkForUpdate();

    @DexIgnore
    void downloadOTAUpdate();

    @DexIgnore
    java.lang.String getHUDBuildVersionText();

    @DexIgnore
    com.navdy.client.ota.OTAUpdateService.State getOTAUpdateState();

    @DexIgnore
    com.navdy.client.ota.model.UpdateInfo getUpdateInfo();

    @DexIgnore
    boolean isCheckingForUpdate();

    @DexIgnore
    long lastKnownUploadSize();

    @DexIgnore
    void registerUIClient(com.navdy.client.ota.OTAUpdateUIClient oTAUpdateUIClient);

    @DexIgnore
    void resetUpdate();

    @DexIgnore
    void setNetworkDownloadApproval(boolean z);

    @DexIgnore
    void toggleAutoDownload(boolean z);

    @DexIgnore
    void unregisterUIClient();
}
