package com.navdy.service.library.device.connection;

import android.annotation.SuppressLint;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.TransactionTooLargeException;

import com.amazonaws.services.s3.internal.Constants;
import com.google.firebase.appindexing.Indexable;
import com.here.odnp.util.OdnpConstants;
import com.navdy.client.app.ui.settings.AudioDialogActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.hud.app.IEventListener;
import com.navdy.hud.app.IEventSource;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.device.RemoteDeviceRegistry;
import com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.Ext_NavdyEvent;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.service.library.events.Ping;
import com.navdy.service.library.events.WireUtil;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStatus;
import com.navdy.service.library.events.connection.DisconnectRequest;
import com.navdy.service.library.events.connection.LinkPropertiesChanged;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.SystemUtils;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexIgnore
public abstract class ConnectionService extends Service implements ConnectionListener.Listener, RemoteDevice.Listener {
    @DexIgnore
    public static /* final */ UUID ACCESSORY_IAP2; // = java.util.UUID.fromString("00000000-deca-fade-deca-deafdecacaff");
    @DexIgnore
    public static /* final */ String ACTION_LINK_BANDWIDTH_LEVEL_CHANGED; // = "LINK_BANDWIDTH_LEVEL_CHANGED";
    @DexIgnore
    public static /* final */ String CATEGORY_NAVDY_LINK; // = "NAVDY_LINK";
    @DexIgnore
    private static /* final */ int CONNECT_TIMEOUT; // = 10000;
    @DexIgnore
    private static /* final */ int DEAD_CONNECTION_TIME; // = 60000;
    @DexIgnore
    private static /* final */ ConnectionStatus DEVICES_CHANGED_EVENT; // = new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_PAIRED_DEVICES_CHANGED, (java.lang.String) null);
    @DexIgnore
    public static /* final */ UUID DEVICE_IAP2; // = java.util.UUID.fromString("00000000-deca-fade-deca-deafdecacafe");
    @DexIgnore
    private static /* final */ int DISCONNECT_TIMEOUT; // = 2000;
    @DexIgnore
    private static /* final */ int EVENT_DISCONNECT; // = 3;
    @DexIgnore
    protected static /* final */ int EVENT_HEARTBEAT; // = 2;
    @DexIgnore
    private static /* final */ int EVENT_RESTART_LISTENERS; // = 4;
    @DexIgnore
    private static /* final */ int EVENT_STATE_CHANGE; // = 1;
    @DexIgnore
    public static /* final */ String EXTRA_BANDWIDTH_LEVEL; // = "EXTRA_BANDWIDTH_MODE";
    @DexIgnore
    private static /* final */ int HEARTBEAT_INTERVAL; // = 4000;
    @DexIgnore
    private static /* final */ int IDLE_TIMEOUT; // = 1000;
    @DexIgnore
    public static /* final */ String ILLEGAL_ARGUMENT; // = "ILLEGAL_ARGUMENT";
    @DexIgnore
    public static /* final */ String NAVDY_IAP_NAME; // = "Navdy iAP";
    @DexIgnore
    public static /* final */ String NAVDY_PROTO_SERVICE_NAME; // = "Navdy";
    @DexIgnore
    public static /* final */ UUID NAVDY_PROTO_SERVICE_UUID; // = java.util.UUID.fromString("1992B7D7-C9B9-4F4F-AA7F-9FE33D95851E");
    @DexIgnore
    public static /* final */ UUID NAVDY_PROXY_TUNNEL_UUID; // = java.util.UUID.fromString("D72BC85F-F015-4F24-A72F-35924E10888F");
    @DexIgnore
    private static /* final */ int PAIRING_TIMEOUT; // = 30000;
    @DexIgnore
    public static /* final */ String REASON_DEAD_CONNECTION; // = "DEAD_CONNECTION";
    @DexIgnore
    private static /* final */ int RECONNECT_DELAY; // = 2000;
    @DexIgnore
    private static /* final */ int RECONNECT_TIMEOUT; // = 10000;
    @DexIgnore
    protected static /* final */ int RETRY_CONNECTING_AFTER_FAILURE_TIMEOUT; // = 30000;
    @DexIgnore
    private static /* final */ int SLEEP_TIMEOUT; // = 60000;
    @DexIgnore
    private BroadcastReceiver bluetoothReceiver; // = new com.navdy.service.library.device.connection.ConnectionService.Anon3();
    @DexIgnore
    private boolean broadcasting; // = false;
    @DexIgnore
    protected /* final */ Object connectionLock; // = new java.lang.Object();
    @DexIgnore
    protected RemoteDeviceRegistry deviceRegistry;
    @DexIgnore
    private boolean forceReconnect; // = false;
    @DexIgnore
    protected boolean inProcess; // = true;
    @DexIgnore
    private long lastMessageReceivedTime;
    @DexIgnore
    protected /* final */ Object listenerLock; // = new java.lang.Object();
    @DexIgnore
    private IEventListener[] listenersArray; // = new com.navdy.hud.app.IEventListener[0];
    @DexIgnore
    private boolean listening; // = false;
    @DexIgnore
    protected /* final */ Logger logger; // = new com.navdy.service.library.log.Logger(getClass());
    @DexIgnore
    protected ConnectionListener[] mConnectionListeners;
    @DexIgnore
    private IEventSource.Stub mEventSource; // = new com.navdy.service.library.device.connection.ConnectionService.Anon1();
    @DexIgnore
    private /* final */ List<IEventListener> mListeners; // = new java.util.ArrayList();
    @DexIgnore
    protected RemoteDevice mRemoteDevice;
    @DexIgnore
    protected RemoteDeviceBroadcaster[] mRemoteDeviceBroadcasters;
    @DexIgnore
    protected Wire mWire;
    @DexIgnore
    private ConnectionService.PendingConnectHandler pendingConnectHandler;
    @DexIgnore
    private ProxyService proxyService;
    @DexIgnore
    private volatile boolean quitting;
    @DexIgnore
    protected Runnable reconnectRunnable; // = new com.navdy.service.library.device.connection.ConnectionService.Anon2();
    @DexIgnore
    protected boolean serverMode; // = false;
    @DexIgnore
    protected volatile ConnectionService.ServiceHandler serviceHandler;
    @DexIgnore
    private volatile Looper serviceLooper;
    @DexIgnore
    protected ConnectionService.State state; // = com.navdy.service.library.device.connection.ConnectionService.State.START;
    @DexIgnore
    private long stateAge; // = 0;

    @DexIgnore
    class Anon1 extends IEventSource.Stub {
        @DexIgnore
        Anon1() {
        }

        @DexIgnore
        public void addEventListener(IEventListener listener) throws RemoteException {
            synchronized (ConnectionService.this.listenerLock) {
                ConnectionService.this.mListeners.add(listener);
                ConnectionService.this.createListenerList();
            }
            ConnectionService.this.logger.v("listener added: pid:" + SystemUtils.getProcessName(ConnectionService.this.getApplicationContext(), getCallingPid()));
            if (ConnectionService.this.mRemoteDevice == null || !ConnectionService.this.mRemoteDevice.isConnected()) {
                ConnectionService.this.forwardEventLocally(new ConnectionStateChange("", ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED));
                ConnectionService.this.forwardEventLocally(new ConnectionStateChange("", ConnectionStateChange.ConnectionState.CONNECTION_LINK_LOST));
            } else {
                ConnectionService.this.forwardEventLocally(new ConnectionStateChange(ConnectionService.this.mRemoteDevice.getDeviceId().toString(), ConnectionStateChange.ConnectionState.CONNECTION_LINK_ESTABLISHED));
                ConnectionService.this.forwardEventLocally(new ConnectionStateChange(ConnectionService.this.mRemoteDevice.getDeviceId().toString(), ConnectionStateChange.ConnectionState.CONNECTION_CONNECTED));
                DeviceInfo deviceInfo = ConnectionService.this.mRemoteDevice.getDeviceInfo();
                if (deviceInfo != null) {
                    ConnectionService.this.logger.v("send device info");
                    ConnectionService.this.forwardEventLocally(deviceInfo);
                }
            }
            ConnectionService.this.sendEventsOnLocalConnect();
        }

        @DexIgnore
        public void removeEventListener(IEventListener listener) throws RemoteException {
            synchronized (ConnectionService.this.listenerLock) {
                ConnectionService.this.mListeners.remove(listener);
                ConnectionService.this.createListenerList();
            }
            ConnectionService.this.logger.v("listener removed: pid:" + SystemUtils.getProcessName(ConnectionService.this.getApplicationContext(), getCallingPid()));
        }

        @DexIgnore
        public void postEvent(byte[] bytes) throws RemoteException {
            boolean sentEvent = false;
            boolean createList = false;
            synchronized (ConnectionService.this.listenerLock) {
                for (int i = 0; i < ConnectionService.this.listenersArray.length; i++) {
                    try {
                        ConnectionService.this.listenersArray[i].onEvent(bytes);
                        sentEvent = true;
                    } catch (DeadObjectException exception) {
                        ConnectionService.this.logger.w("Reaping dead listener", exception);
                        ConnectionService.this.mListeners.remove(ConnectionService.this.listenersArray[i]);
                        createList = true;
                    } catch (TransactionTooLargeException tooLargeException) {
                        ConnectionService.this.logger.w("Communication Pipe is full:", tooLargeException);
                    } catch (Throwable throwable) {
                        ConnectionService.this.logger.w("Exception throws by remote:", throwable);
                    }
                }
                if (ConnectionService.this.logger.isLoggable(2) && !sentEvent) {
                    ConnectionService.this.logger.d("No one listening for event - byte length " + bytes.length);
                }
                if (createList) {
                    ConnectionService.this.createListenerList();
                }
            }
        }

        @DexIgnore
        public void postRemoteEvent(String remoteDeviceId, byte[] bytes) throws RemoteException {
            if (remoteDeviceId == null || bytes == null) {
                ConnectionService.this.logger.e("illegal argument");
                throw new RemoteException(ConnectionService.ILLEGAL_ARGUMENT);
            }
            NavdyDeviceId deviceId = new NavdyDeviceId(remoteDeviceId);
            if (deviceId.equals(NavdyDeviceId.getThisDevice(ConnectionService.this))) {
                NavdyEvent.MessageType messageType = WireUtil.getEventType(bytes);
                if (messageType == null || !ConnectionService.this.processLocalEvent(bytes, messageType)) {
                    ConnectionService.this.logger.w("Connection service ignored message:" + messageType);
                }
            } else if (ConnectionService.this.mRemoteDevice == null || !ConnectionService.this.mRemoteDevice.isConnected()) {
                if (ConnectionService.this.logger.isLoggable(2)) {
                    ConnectionService.this.logger.e("app not connected");
                }
            } else if (ConnectionService.this.mRemoteDevice.getDeviceId().equals(deviceId)) {
                ConnectionService.this.mRemoteDevice.postEvent(bytes);
            } else {
                ConnectionService.this.logger.i("Device id mismatch: deviceId=" + deviceId + " remote:" + ConnectionService.this.mRemoteDevice.getDeviceId());
            }
        }
    }

    @DexIgnore
    class Anon2 implements Runnable {
        @DexIgnore
        Anon2() {
        }

        @DexIgnore
        public void run() {
            if (ConnectionService.this.state == ConnectionService.State.RECONNECTING && ConnectionService.this.mRemoteDevice != null && !ConnectionService.this.mRemoteDevice.isConnecting() && !ConnectionService.this.mRemoteDevice.isConnected()) {
                ConnectionService.this.logger.i("Retrying to connect to " + ConnectionService.this.mRemoteDevice.getDeviceId());
                ConnectionService.this.mRemoteDevice.connect();
            }
        }
    }

    @DexIgnore
    class Anon3 extends BroadcastReceiver {
        @DexIgnore
        Anon3() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.bluetooth.adapter.action.STATE_CHANGED") && intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE) == 12) {
                if (ConnectionService.this.state == ConnectionService.State.START) {
                    ConnectionService.this.logger.i("bluetooth turned on - exiting START state");
                    ConnectionService.this.setState(ConnectionService.State.IDLE);
                    return;
                }
                ConnectionService.this.logger.i("bluetooth turned on - restarting listeners");
                ConnectionService.this.serviceHandler.sendEmptyMessage(4);
            }
        }
    }

    @DexIgnore
    private class PendingConnectHandler implements Runnable {
        @DexIgnore
        private Connection connection;
        @DexIgnore
        private RemoteDevice device;

        @DexIgnore
        public PendingConnectHandler(RemoteDevice device2, Connection connection2) {
            this.device = device2;
            this.connection = connection2;
        }

        @DexIgnore
        public void run() {
            boolean newDevice = ConnectionService.this.mRemoteDevice != this.device;
            ConnectionService.this.logger.d("Trying to " + (newDevice ? "connect" : "reconnect") + " to " + this.device.getDeviceId().getDisplayName());
            ConnectionService.this.setRemoteDevice(this.device);
            if (this.connection != null) {
                ConnectionService.this.mRemoteDevice.setActiveConnection(this.connection);
            } else if (ConnectionService.this.mRemoteDevice.isConnected()) {
                ConnectionService.this.onDeviceConnected(ConnectionService.this.mRemoteDevice);
            } else {
                ConnectionService.this.setState(newDevice ? ConnectionService.State.CONNECTING : ConnectionService.State.RECONNECTING);
                ConnectionService.this.mRemoteDevice.connect();
            }
            ConnectionService.this.pendingConnectHandler = null;
        }
    }

    @DexIgnore
    protected final class ServiceHandler extends Handler {
        @DexIgnore
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 1:
                    ConnectionService.State newState = (ConnectionService.State) msg.obj;
                    if (newState != ConnectionService.this.state) {
                        ConnectionService.this.exitState(ConnectionService.this.state);
                        ConnectionService.this.state = newState;
                        ConnectionService.this.enterState(ConnectionService.this.state);
                        return;
                    }
                    return;
                case 2:
                    ConnectionService.this.heartBeat();
                    return;
                case 3:
                    RemoteDevice device = (RemoteDevice) msg.obj;
                    ConnectionService.this.handleDeviceDisconnect(device, Connection.DisconnectCause.values()[msg.arg1]);
                    return;
                case 4:
                    if (ConnectionService.this.listening) {
                        ConnectionService.this.logger.i("stopping/starting listeners");
                        ConnectionService.this.stopListeners();
                        ConnectionService.this.startListeners();
                    }
                    if (ConnectionService.this.broadcasting) {
                        ConnectionService.this.logger.i("restarting broadcasters");
                        ConnectionService.this.stopBroadcasters();
                        ConnectionService.this.startBroadcasters();
                    }
                    ConnectionService.this.stopProxyService();
                    ConnectionService.this.startProxyService();
                    return;
                default:
                    ConnectionService.this.logger.e("Unknown message: " + msg);
                    return;
            }
        }
    }

    @DexIgnore
    public enum State {
        START,
        IDLE,
        SEARCHING,
        CONNECTING,
        RECONNECTING,
        CONNECTED,
        DISCONNECTING,
        DISCONNECTED,
        PAIRING,
        LISTENING,
        DESTROYED
    }

    @DexIgnore
    protected abstract ProxyService createProxyService() throws IOException;

    @DexIgnore
    protected abstract ConnectionListener[] getConnectionListeners(Context context);

    @DexIgnore
    protected abstract RemoteDeviceBroadcaster[] getRemoteDeviceBroadcasters();

    @DexIgnore
    private void sendPingIfNeeded() {
        try {
            if (this.mRemoteDevice != null && this.mRemoteDevice.isConnected() && SystemClock.elapsedRealtime() - this.mRemoteDevice.lastMessageSentTime > 4000) {
                this.mRemoteDevice.postEvent(new Ping());
                if (this.serverMode && this.logger.isLoggable(2)) {
                    this.logger.v("NAVDY-PACKET [H2P-Outgoing-Event] Ping");
                }
            }
        } catch (Throwable t) {
            this.logger.e(t);
        }
    }

    @DexIgnore
    private void checkConnection() {
        if (this.mRemoteDevice == null || !this.mRemoteDevice.isConnected()) {
            this.logger.v("dead connection remotedevice=" + (this.mRemoteDevice == null ? Constants.NULL_VERSION_ID : "not null") + " isConnected:" + (this.mRemoteDevice == null ? "false" : Boolean.valueOf(this.mRemoteDevice.isConnected())));
            setState(ConnectionService.State.DISCONNECTING);
        } else if (this.mRemoteDevice.getLinkStatus() == RemoteDevice.LinkStatus.CONNECTED) {
            long timeElapsed = SystemClock.elapsedRealtime() - this.lastMessageReceivedTime;
            if (timeElapsed > OdnpConstants.ONE_MINUTE_IN_MS) {
                this.logger.v("dead connection timed out:" + timeElapsed);
                if (reconnectAfterDeadConnection()) {
                    this.logger.d("Reconncting after dead connection timed out disconnect");
                    reconnect(REASON_DEAD_CONNECTION);
                    return;
                }
                setState(ConnectionService.State.DISCONNECTING);
            }
        }
    }

    @DexIgnore
    public boolean reconnectAfterDeadConnection() {
        return false;
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        if (IEventSource.class.getName().equals(intent.getAction())) {
            this.logger.d("returning IEventSource API endpoint");
            return this.mEventSource;
        }
        this.logger.w("invalid action:" + intent.getAction());
        return null;
    }

    @DexIgnore
    public void onCreate() {
        this.logger.e("ConnectionService created: mainthread:" + Looper.getMainLooper().getThread().getId());
        this.mWire = new Wire((Class<?>[]) new Class[]{Ext_NavdyEvent.class});
        HandlerThread thread = new HandlerThread("ConnectionService");
        thread.start();
        this.serviceLooper = thread.getLooper();
        this.serviceHandler = new ConnectionService.ServiceHandler(this.serviceLooper);
        registerReceiver(this.bluetoothReceiver, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
        this.deviceRegistry = RemoteDeviceRegistry.getInstance(this);
    }

    @SuppressLint("WrongConstant")
    @DexIgnore
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            if (this.state == ConnectionService.State.CONNECTED) {
                this.logger.i("BT enabled, disconnecting since app likely crashed");
                setState(ConnectionService.State.DISCONNECTING);
            } else {
                this.logger.i("BT enabled, switching to IDLE state");
                setState(ConnectionService.State.IDLE);
            }
        }
        return 1;
    }

    @DexIgnore
    public void onDestroy() {
        this.logger.v("ConnectionService destroyed.  mwhahahaha.");
        try {
            setState(ConnectionService.State.DESTROYED);
            unregisterReceiver(this.bluetoothReceiver);
            this.serviceLooper.quitSafely();
            stopProxyService();
        } catch (Throwable t) {
            this.logger.e(t);
        }
    }

    @DexIgnore
    protected void forwardEventLocally(Message message) {
        forwardEventLocally(NavdyEventUtil.eventFromMessage(message).toByteArray());
    }

    @DexIgnore
    protected void forwardEventLocally(byte[] eventData) {
        try {
            this.mEventSource.postEvent(eventData);
        } catch (Throwable t) {
            this.logger.e(t);
        }
    }

    @DexIgnore
    protected void startBroadcasters() {
        synchronized (this.connectionLock) {
            this.broadcasting = true;
            if (this.mRemoteDeviceBroadcasters == null) {
                this.mRemoteDeviceBroadcasters = getRemoteDeviceBroadcasters();
            }
            for (int i = 0; i < this.mRemoteDeviceBroadcasters.length; i++) {
                this.logger.v("starting connection broadcaster:" + this.mRemoteDeviceBroadcasters[i].getClass().getName());
                try {
                    this.mRemoteDeviceBroadcasters[i].start();
                } catch (Throwable t) {
                    this.logger.e(t);
                }
            }
        }
    }

    @DexIgnore
    protected void stopBroadcasters() {
        this.logger.v("stopping all broadcasters");
        synchronized (this.connectionLock) {
            this.broadcasting = false;
            if (this.mRemoteDeviceBroadcasters != null) {
                for (RemoteDeviceBroadcaster mRemoteDeviceBroadcaster : this.mRemoteDeviceBroadcasters) {
                    this.logger.v("stopping connection broadcaster:" + mRemoteDeviceBroadcaster.getClass().getName());
                    try {
                        mRemoteDeviceBroadcaster.stop();
                    } catch (Throwable t) {
                        this.logger.e(t);
                    }
                }
            }
        }
    }

    @DexIgnore
    protected void startListeners() {
        synchronized (this.connectionLock) {
            this.listening = true;
            if (this.mConnectionListeners == null) {
                this.logger.v("initializing connector listener");
                this.mConnectionListeners = getConnectionListeners(getApplicationContext());
                for (ConnectionListener addListener : this.mConnectionListeners) {
                    addListener.addListener(this);
                }
            }
            for (ConnectionListener mConnectionListener : this.mConnectionListeners) {
                this.logger.v("starting connection listener:" + mConnectionListener);
                try {
                    mConnectionListener.start();
                } catch (Throwable t) {
                    this.logger.e(t);
                }
            }
        }
    }

    @DexIgnore
    protected void stopListeners() {
        synchronized (this.connectionLock) {
            this.listening = false;
            if (this.mConnectionListeners != null) {
                for (ConnectionListener mConnectionListener : this.mConnectionListeners) {
                    this.logger.v("stopping:" + mConnectionListener);
                    try {
                        mConnectionListener.stop();
                    } catch (Throwable t) {
                        this.logger.e(t);
                    }
                }
            }
        }
    }

    @DexIgnore
    public void onStarted(ConnectionListener connectionListener) {
        this.logger.v("started listening");
    }

    @DexIgnore
    public void onStartFailure(ConnectionListener connectionListener) {
        this.logger.e("failed to start listening:" + connectionListener);
    }

    @DexIgnore
    public void onStopped(ConnectionListener connectionListener) {
        this.logger.v("stopped listening:" + connectionListener);
    }

    @DexIgnore
    public void onConnected(ConnectionListener connectionListener, Connection connection) {
        this.logger.v("listener connected");
        setActiveDevice(new RemoteDevice(getApplicationContext(), connection.getConnectionInfo(), this.inProcess), connection);
    }

    @DexIgnore
    public void onConnectionFailed(ConnectionListener connectionListener) {
        this.logger.e("onConnectionFailed:restart listeners");
        setState(ConnectionService.State.IDLE);
    }

    @DexIgnore
    protected synchronized void setRemoteDevice(RemoteDevice remoteDevice) {
        if (remoteDevice != this.mRemoteDevice) {
            if (this.mRemoteDevice != null) {
                this.mRemoteDevice.removeListener(this);
            }
            this.mRemoteDevice = remoteDevice;
            if (this.mRemoteDevice != null) {
                this.mRemoteDevice.addListener(this);
            }
        }
    }

    @DexIgnore
    public void connect(ConnectionInfo connectionInfo) {
        setActiveDevice(new RemoteDevice(this, connectionInfo, this.inProcess));
    }

    @DexIgnore
    public boolean setActiveDevice(RemoteDevice remoteDevice) {
        return setActiveDevice(remoteDevice, null);
    }

    @DexIgnore
    public synchronized void reconnect(String reconnectReason) {
        if (this.mRemoteDevice != null) {
            this.forceReconnect = true;
            setState(ConnectionService.State.DISCONNECTING);
        }
    }

    @DexIgnore
    public synchronized boolean setActiveDevice(RemoteDevice remoteDevice, Connection connection) {
        if (this.mRemoteDevice != null) {
            if (this.mRemoteDevice.isConnected() || this.mRemoteDevice.isConnecting()) {
                setState(ConnectionService.State.DISCONNECTING);
            } else {
                setRemoteDevice(null);
            }
        }
        if (remoteDevice != null) {
            ConnectionService.PendingConnectHandler connectHandler = new ConnectionService.PendingConnectHandler(remoteDevice, connection);
            if (this.mRemoteDevice == null) {
                connectHandler.run();
            } else {
                this.pendingConnectHandler = connectHandler;
            }
        }
        return true;
    }

    @DexIgnore
    private boolean hasPaired() {
        return this.deviceRegistry.hasPaired();
    }

    @DexIgnore
    public void setState(ConnectionService.State newState) {
        if (!this.quitting && this.state != newState) {
            if (newState == ConnectionService.State.DESTROYED) {
                this.quitting = true;
            }
            this.serviceHandler.sendMessage(this.serviceHandler.obtainMessage(1, newState));
        }
    }

    @DexIgnore
    protected void enterState(ConnectionService.State state2) {
        this.logger.d("Entering state:" + state2);
        if (state2 == ConnectionService.State.DESTROYED) {
            if (this.listening) {
                stopListeners();
            }
            if (this.broadcasting) {
                stopBroadcasters();
            }
            this.serviceHandler.removeCallbacksAndMessages(null);
            return;
        }
        this.stateAge = 0;
        int timeout = 1000;
        switch (state2) {
            case IDLE:
                timeout = 1000;
                break;
            case PAIRING:
                timeout = Indexable.MAX_BYTE_SIZE;
                startBroadcasters();
                startListeners();
                forwardEventLocally(new ConnectionStatus(ConnectionStatus.Status.CONNECTION_PAIRING, ""));
                break;
            case LISTENING:
                if (isPromiscuous()) {
                    startBroadcasters();
                }
                startListeners();
                if (this.serverMode) {
                    timeout = Indexable.MAX_BYTE_SIZE;
                    break;
                }
                break;
            case CONNECTING:
                timeout = 10000;
                break;
            case RECONNECTING:
                timeout = 10000;
                break;
            case DISCONNECTING:
                if (this.mRemoteDevice == null) {
                    setState(ConnectionService.State.IDLE);
                    break;
                } else {
                    timeout = AudioDialogActivity.DELAY_MILLIS;
                    this.mRemoteDevice.disconnect();
                    break;
                }
            case CONNECTED:
                stopListeners();
                stopBroadcasters();
                handleDeviceConnect(this.mRemoteDevice);
                break;
        }
        this.serviceHandler.removeMessages(2);
        this.serviceHandler.sendEmptyMessageDelayed(2, (long) timeout);
    }

    @DexIgnore
    protected void exitState(ConnectionService.State state2) {
        this.logger.d("Exiting state:" + state2);
        switch (state2) {
            case PAIRING:
                if (!isPromiscuous()) {
                    stopBroadcasters();
                    return;
                }
                return;
            case RECONNECTING:
                this.serviceHandler.removeCallbacks(this.reconnectRunnable);
                return;
            case START:
                startProxyService();
                return;
            default:
                return;
        }
    }

    @DexIgnore
    protected void heartBeat() {
        if (this.stateAge % 10 == 0) {
            this.logger.d("Heartbeat: in state:" + this.state);
        }
        this.stateAge++;
        int timeout = 1000;
        switch (this.state) {
            case IDLE:
                if (this.pendingConnectHandler == null) {
                    if (hasPaired() || !this.serverMode) {
                        if (!hasPaired()) {
                            timeout = 60000;
                            break;
                        } else {
                            setState(ConnectionService.State.LISTENING);
                            break;
                        }
                    } else {
                        setState(ConnectionService.State.PAIRING);
                        break;
                    }
                } else {
                    this.pendingConnectHandler.run();
                    break;
                }
                // break;
            case PAIRING:
                timeout = Indexable.MAX_BYTE_SIZE;
                break;
            case LISTENING:
                if (this.serverMode) {
                    if (this.mRemoteDevice == null) {
                        if (!needAutoSearch()) {
                            timeout = 60000;
                            break;
                        } else {
                            setState(ConnectionService.State.SEARCHING);
                            break;
                        }
                    } else {
                        setState(ConnectionService.State.RECONNECTING);
                        this.serviceHandler.post(this.reconnectRunnable);
                        break;
                    }
                } else {
                    timeout = 60000;
                    break;
                }
            case CONNECTING:
            case RECONNECTING:
            case DISCONNECTING:
                setState(ConnectionService.State.IDLE);
                break;
            case CONNECTED:
                timeout = HEARTBEAT_INTERVAL;
                sendPingIfNeeded();
                checkConnection();
                break;
        }
        this.serviceHandler.removeMessages(2);
        this.serviceHandler.sendEmptyMessageDelayed(2, (long) timeout);
    }

    @DexIgnore
    public boolean needAutoSearch() {
        return true;
    }

    @DexIgnore
    public boolean isPromiscuous() {
        return false;
    }

    @DexIgnore
    public void onDeviceConnecting(RemoteDevice device) {
    }

    @DexIgnore
    public void onDeviceConnected(RemoteDevice device) {
        setState(ConnectionService.State.CONNECTED);
    }

    @DexIgnore
    public void onDeviceConnectFailure(RemoteDevice device, Connection.ConnectionFailureCause cause) {
        if (this.state == ConnectionService.State.RECONNECTING) {
            this.logger.i("onDeviceConnectFailure - retrying");
            this.serviceHandler.postDelayed(this.reconnectRunnable, 2000);
            return;
        }
        this.logger.i("onDeviceConnectFailure - switching to idle state");
        if (device != this.mRemoteDevice || this.serverMode) {
            this.logger.d("Not clearing the remote device, to attempt reconnect");
        } else {
            setRemoteDevice(null);
        }
        setState(ConnectionService.State.IDLE);
    }

    @DexIgnore
    public void onDeviceDisconnected(RemoteDevice device, Connection.DisconnectCause cause) {
        this.serviceHandler.sendMessage(this.serviceHandler.obtainMessage(3, cause.ordinal(), -1, device));
    }

    @DexIgnore
    protected void rememberPairedDevice(RemoteDevice device) {
        this.deviceRegistry.addPairedConnection(device.getActiveConnectionInfo());
        forwardEventLocally(DEVICES_CHANGED_EVENT);
    }

    @DexIgnore
    protected void forgetPairedDevice(RemoteDevice device) {
        this.deviceRegistry.removePairedConnection(device.getActiveConnectionInfo());
        forwardEventLocally(DEVICES_CHANGED_EVENT);
    }

    @DexIgnore
    protected void forgetPairedDevice(BluetoothDevice device) {
        this.deviceRegistry.removePairedConnection(device);
        forwardEventLocally(DEVICES_CHANGED_EVENT);
    }

    @DexIgnore
    private void handleDeviceConnect(RemoteDevice device) {
        this.logger.v("onDeviceConnected:remembering device");
        rememberPairedDevice(device);
        this.logger.v("onDeviceConnected:heartBeat started");
        this.lastMessageReceivedTime = SystemClock.elapsedRealtime();
        this.logger.d("Connecting with app context: " + getApplicationContext());
        if (!this.mRemoteDevice.startLink()) {
            setState(ConnectionService.State.DISCONNECTING);
        }
    }

    @DexIgnore
    protected void handleDeviceDisconnect(RemoteDevice device, Connection.DisconnectCause cause) {
        this.logger.v("device disconnect:" + device + " cause:" + cause);
        if (device != null) {
            device.stopLink();
        }
        if (this.mRemoteDevice != device) {
            return;
        }
        if ((cause == Connection.DisconnectCause.ABORTED || this.forceReconnect) && this.serverMode) {
            this.forceReconnect = false;
            new ConnectionService.PendingConnectHandler(this.mRemoteDevice, null).run();
            return;
        }
        setRemoteDevice(null);
        setState(ConnectionService.State.IDLE);
    }

    @DexIgnore
    public void onNavdyEventReceived(RemoteDevice device, byte[] eventData) {
        this.lastMessageReceivedTime = SystemClock.elapsedRealtime();
        NavdyEvent.MessageType messageType = WireUtil.getEventType(eventData);
        if (this.logger.isLoggable(2)) {
            this.logger.v((this.serverMode ? "NAVDY-PACKET [P2H" : "NAVDY-PACKET [H2P") + "-Event] " + (messageType != null ? messageType.name() : SettingsConstants.LAST_HUD_UUID_DEFAULT) + " size:" + eventData.length);
        }
        if (messageType != null && messageType != NavdyEvent.MessageType.Ping) {
            if (messageType == NavdyEvent.MessageType.DisconnectRequest) {
                try {
                    handleRemoteDisconnect(this.mWire.parseFrom(eventData, NavdyEvent.class).getExtension(Ext_NavdyEvent.disconnectRequest));
                } catch (Throwable t) {
                    this.logger.e("Failed to parse event", t);
                }
            } else if (processEvent(eventData, messageType)) {
            } else {
                if (messageType == NavdyEvent.MessageType.DeviceInfo) {
                    try {
                        if (this.mRemoteDevice != null && this.mRemoteDevice.isConnected()) {
                            DeviceInfo patchedDeviceInfo = new DeviceInfo.Builder((DeviceInfo) NavdyEventUtil.messageFromEvent(this.mWire.parseFrom(eventData, NavdyEvent.class))).deviceId(this.mRemoteDevice.getDeviceId().toString()).build();
                            this.mRemoteDevice.setDeviceInfo(patchedDeviceInfo);
                            this.logger.v("set remote device info");
                            forwardEventLocally(NavdyEventUtil.eventFromMessage(patchedDeviceInfo).toByteArray());
                        }
                    } catch (Throwable t2) {
                        this.logger.e("Failed to parse deviceinfo", t2);
                    }
                } else {
                    forwardEventLocally(eventData);
                }
            }
        }
    }

    @DexIgnore
    public void onNavdyEventReceived(RemoteDevice device, NavdyEvent event) {
        this.lastMessageReceivedTime = SystemClock.elapsedRealtime();
        if (event.type == NavdyEvent.MessageType.DisconnectRequest) {
            handleRemoteDisconnect(event.getExtension(Ext_NavdyEvent.disconnectRequest));
        }
    }

    @DexIgnore
    protected boolean processEvent(byte[] eventData, NavdyEvent.MessageType messageType) {
        return false;
    }

    @DexIgnore
    protected boolean processLocalEvent(byte[] eventData, NavdyEvent.MessageType messageType) {
        return false;
    }

    @DexIgnore
    private void createListenerList() {
        this.listenersArray = new IEventListener[this.mListeners.size()];
        this.mListeners.toArray(this.listenersArray);
    }

    @DexIgnore
    private void handleRemoteDisconnect(DisconnectRequest disconnectRequest) {
        if (this.mRemoteDevice != null) {
            if (disconnectRequest.forget != null && disconnectRequest.forget.booleanValue()) {
                forgetPairedDevice(this.mRemoteDevice);
            }
            setState(ConnectionService.State.DISCONNECTING);
        }
    }

    @DexReplace
    protected void startProxyService() {
        // No more proxy, no x64 library
//        if (this.proxyService == null || !this.proxyService.isAlive()) {
//            this.logger.v(SystemUtils.getProcessName(getApplicationContext(), android.os.Process.myPid()) + ": start service for proxy");
//            try {
//                this.proxyService = createProxyService();
//                this.proxyService.start();
//            } catch (Exception e) {
//                this.logger.e("Failed to start proxy service", e);
//            }
//        }
    }

    @DexIgnore
    protected void stopProxyService() {
        if (this.proxyService != null) {
            this.proxyService.cancel();
            this.proxyService = null;
        }
    }

    @DexIgnore
    public void sendMessage(Message event) {
        if (isConnected()) {
            this.mRemoteDevice.postEvent(event);
        }
    }

    @DexIgnore
    public boolean isConnected() {
        return this.mRemoteDevice != null && this.mRemoteDevice.isConnected();
    }

    @DexIgnore
    protected void sendEventsOnLocalConnect() {
    }

    @DexIgnore
    protected void setBandwidthLevel(int level) {
        if (this.state == ConnectionService.State.CONNECTED && this.mRemoteDevice != null) {
            if (level == 0 || level == 1) {
                this.mRemoteDevice.setLinkBandwidthLevel(level);
                LinkPropertiesChanged.Builder builder = new LinkPropertiesChanged.Builder();
                builder.bandwidthLevel(level);
                forwardEventLocally(builder.build());
            }
        }
    }
}
