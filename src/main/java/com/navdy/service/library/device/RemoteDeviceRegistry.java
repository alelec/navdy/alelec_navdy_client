package com.navdy.service.library.device;

import android.annotation.SuppressLint;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexEdit(defaultAction = DexAction.IGNORE)
public class RemoteDeviceRegistry extends com.navdy.service.library.util.Listenable<com.navdy.service.library.device.RemoteDeviceRegistry.DeviceListUpdatedListener> implements com.navdy.service.library.device.discovery.RemoteDeviceScanner.Listener {
    @DexIgnore
    public static /* final */ int MAX_PAIRED_DEVICES; // = 5;
    @DexIgnore
    public static /* final */ java.lang.String PREFS_FILE_DEVICE_REGISTRY; // = "DeviceRegistry";
    @DexIgnore
    public static /* final */ java.lang.String PREFS_KEY_DEFAULT_CONNECTION_INFO; // = "DefaultConnectionInfo";
    @DexIgnore
    public static /* final */ java.lang.String PREFS_KEY_PAIRED_CONNECTION_INFOS; // = "PairedConnectionInfos";
    @DexIgnore
    public static /* final */ com.navdy.service.library.log.Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.RemoteDeviceRegistry.class);
    @DexIgnore
    private static com.navdy.service.library.device.RemoteDeviceRegistry sRemoteDeviceRegistry;
    @DexIgnore
    protected android.content.Context mContext;
    @DexIgnore
    protected com.google.gson.Gson mGson; // = new com.google.gson.GsonBuilder().registerTypeAdapterFactory(com.navdy.service.library.device.connection.ConnectionInfo.connectionInfoAdapter).create();
    @DexIgnore
    protected java.util.Set<com.navdy.service.library.device.connection.ConnectionInfo> mKnownConnectionInfo; // = new java.util.HashSet();
    @DexIgnore
    protected java.util.HashSet<com.navdy.service.library.device.discovery.RemoteDeviceScanner> mRemoteDeviceScanners; // = new java.util.HashSet<>();
    @DexIgnore
    protected android.content.SharedPreferences mSharedPrefs;
    @DexIgnore
    private java.util.ArrayList<com.navdy.service.library.device.connection.ConnectionInfo> pairedConnections;

    @DexIgnore
    class Anon1 implements com.navdy.service.library.device.RemoteDeviceRegistry.DeviceListEventDispatcher {
        @DexIgnore
        Anon1() {
        }

        @DexIgnore
        public void dispatchEvent(com.navdy.service.library.device.RemoteDeviceRegistry source, com.navdy.service.library.device.RemoteDeviceRegistry.DeviceListUpdatedListener listener) {
            listener.onDeviceListChanged(new java.util.HashSet<>(com.navdy.service.library.device.RemoteDeviceRegistry.this.mKnownConnectionInfo));
        }
    }

    @DexIgnore
    class Anon2 extends com.google.gson.reflect.TypeToken<java.util.List<com.navdy.service.library.device.connection.ConnectionInfo>> {
        @DexIgnore
        Anon2() {
        }
    }

    @DexIgnore
    class Anon3 extends com.google.gson.reflect.TypeToken<java.util.List<com.navdy.service.library.device.connection.ConnectionInfo>> {
        @DexIgnore
        Anon3() {
        }
    }

    @DexIgnore
    protected interface DeviceListEventDispatcher extends com.navdy.service.library.util.Listenable.EventDispatcher<com.navdy.service.library.device.RemoteDeviceRegistry, com.navdy.service.library.device.RemoteDeviceRegistry.DeviceListUpdatedListener> {
    }

    @DexIgnore
    public interface DeviceListUpdatedListener extends com.navdy.service.library.util.Listenable.Listener {
        void onDeviceListChanged(java.util.Set<com.navdy.service.library.device.connection.ConnectionInfo> set);
    }

    @DexIgnore
    public static com.navdy.service.library.device.RemoteDeviceRegistry getInstance(android.content.Context c) {
        if (sRemoteDeviceRegistry == null) {
            sRemoteDeviceRegistry = new com.navdy.service.library.device.RemoteDeviceRegistry(c.getApplicationContext());
        }
        return sRemoteDeviceRegistry;
    }

    @DexIgnore
    private RemoteDeviceRegistry(android.content.Context context) {
        this.mContext = context;
        loadPairedConnections();
    }

    @DexIgnore
    public java.util.Set<com.navdy.service.library.device.connection.ConnectionInfo> getKnownConnectionInfo() {
        return this.mKnownConnectionInfo;
    }

    @DexIgnore
    public void addRemoteDeviceScanner(com.navdy.service.library.device.discovery.RemoteDeviceScanner scanner) {
        scanner.addListener(this);
        this.mRemoteDeviceScanners.add(scanner);
    }

    @DexIgnore
    public void removeRemoteDeviceScanner(com.navdy.service.library.device.discovery.RemoteDeviceScanner scanner) {
        scanner.removeListener(this);
        this.mRemoteDeviceScanners.remove(scanner);
    }

    @DexIgnore
    public void startScanning() {
        this.mKnownConnectionInfo.clear();
        java.util.Iterator it = this.mRemoteDeviceScanners.iterator();
        while (it.hasNext()) {
            ((com.navdy.service.library.device.discovery.RemoteDeviceScanner) it.next()).startScan();
        }
    }

    @DexIgnore
    public void stopScanning() {
        java.util.Iterator it = this.mRemoteDeviceScanners.iterator();
        while (it.hasNext()) {
            ((com.navdy.service.library.device.discovery.RemoteDeviceScanner) it.next()).stopScan();
        }
    }

    @DexIgnore
    public void refresh() {
        sLogger.i("refreshing paired connections info");
        loadPairedConnections();
    }

    @DexIgnore
    public void addDiscoveredConnectionInfo(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
        if (this.mKnownConnectionInfo.contains(connectionInfo)) {
            sLogger.d("Already contains: " + connectionInfo);
            return;
        }
        this.mKnownConnectionInfo.add(connectionInfo);
        sendDeviceListUpdate();
    }

    @DexIgnore
    protected void sendDeviceListUpdate() {
        dispatchToListeners(new com.navdy.service.library.device.RemoteDeviceRegistry.Anon1());
    }

    @DexIgnore
    public void setDefaultConnectionInfo(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
        try {
            this.mSharedPrefs.edit().putString(PREFS_KEY_DEFAULT_CONNECTION_INFO, this.mGson.toJson((java.lang.Object) connectionInfo, (java.lang.reflect.Type) com.navdy.service.library.device.connection.ConnectionInfo.class)).apply();
        } catch (java.lang.Exception e) {
            sLogger.e("Exception " + e);
        }
    }

    @DexIgnore
    public com.navdy.service.library.device.connection.ConnectionInfo getDefaultConnectionInfo() {
        com.navdy.service.library.device.connection.ConnectionInfo connectionInfo = null;
        try {
            java.lang.String connectionInfoText = this.mSharedPrefs.getString(PREFS_KEY_DEFAULT_CONNECTION_INFO, null);
            if (connectionInfoText == null) {
                return null;
            }
            connectionInfo = (com.navdy.service.library.device.connection.ConnectionInfo) this.mGson.fromJson(connectionInfoText, com.navdy.service.library.device.connection.ConnectionInfo.class);
            return connectionInfo;
        } catch (com.google.gson.JsonParseException e) {
            sLogger.e("Unable to read connection info: ", e);
        }
        return null;
    }

    @DexIgnore
    public void addPairedConnection(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
        this.pairedConnections.remove(connectionInfo);
        if (this.pairedConnections.size() >= 5) {
            this.pairedConnections.remove(4);
        }
        this.pairedConnections.add(0, connectionInfo);
        savePairedConnections();
    }

    @DexIgnore
    public void removePairedConnection(android.bluetooth.BluetoothDevice device) {
        java.util.Iterator<com.navdy.service.library.device.connection.ConnectionInfo> listIterator = this.pairedConnections.listIterator();
        while (listIterator.hasNext()) {
            com.navdy.service.library.device.connection.ConnectionInfo info = (com.navdy.service.library.device.connection.ConnectionInfo) listIterator.next();
            if ((info instanceof com.navdy.service.library.device.connection.BTConnectionInfo) && info.getAddress().getAddress().equals(device.getAddress())) {
                listIterator.remove();
            }
        }
        savePairedConnections();
    }

    @DexIgnore
    public com.navdy.service.library.device.connection.ConnectionInfo findDevice(com.navdy.service.library.device.NavdyDeviceId deviceId) {
        java.util.Iterator<com.navdy.service.library.device.connection.ConnectionInfo> listIterator = this.pairedConnections.listIterator();
        while (listIterator.hasNext()) {
            com.navdy.service.library.device.connection.ConnectionInfo info = (com.navdy.service.library.device.connection.ConnectionInfo) listIterator.next();
            if (info.getDeviceId().equals(deviceId)) {
                return info;
            }
        }
        return null;
    }

    @DexIgnore
    public void removePairedConnection(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
        this.pairedConnections.remove(connectionInfo);
        savePairedConnections();
    }

    @DexIgnore
    public java.util.List<com.navdy.service.library.device.connection.ConnectionInfo> getPairedConnections() {
        return this.pairedConnections;
    }

    @DexIgnore
    public boolean hasPaired() {
        return this.pairedConnections.size() > 0;
    }

    @DexIgnore
    public void savePairedConnections() {
        try {
            this.mSharedPrefs.edit().putString(PREFS_KEY_PAIRED_CONNECTION_INFOS, this.mGson.toJson((java.lang.Object) this.pairedConnections, new com.navdy.service.library.device.RemoteDeviceRegistry.Anon2().getType())).commit();
        } catch (java.lang.Exception e) {
            sLogger.e("Exception saving paired connections", e);
        }
    }

    @SuppressLint("WrongConstant")
    @lanchon.dexpatcher.annotation.DexReplace
    private void loadPairedConnections() {
        java.util.ArrayList<com.navdy.service.library.device.connection.ConnectionInfo> list = new java.util.ArrayList<>();
        try {
            this.mSharedPrefs = this.mContext.getSharedPreferences(PREFS_FILE_DEVICE_REGISTRY, 4);
            try {
                java.lang.String connectionInfoText = this.mSharedPrefs.getString(PREFS_KEY_PAIRED_CONNECTION_INFOS, null);
                java.lang.reflect.Type listType = new com.navdy.service.library.device.RemoteDeviceRegistry.Anon3().getType();
                if (connectionInfoText != null) {
                    list = (java.util.ArrayList) this.mGson.fromJson(connectionInfoText, listType);
                }
            } catch (com.google.gson.JsonParseException e) {
                sLogger.e("Unable to read connection infos: ", e);
            }
            sLogger.v("Read pairing list of:" + java.util.Arrays.toString(list.toArray()));
        } catch (RuntimeException e) {
            sLogger.e("Unable to read connection infos: ", e);
        }
        this.pairedConnections = list;
    }

    @DexIgnore
    public void onScanStarted(com.navdy.service.library.device.discovery.RemoteDeviceScanner scanner) {
        sLogger.e("Scan started: " + scanner.toString());
    }

    @DexIgnore
    public void onScanStopped(com.navdy.service.library.device.discovery.RemoteDeviceScanner scanner) {
        sLogger.e("Scan stopped: " + scanner.toString());
    }

    @DexIgnore
    public void onDiscovered(com.navdy.service.library.device.discovery.RemoteDeviceScanner scanner, java.util.List<com.navdy.service.library.device.connection.ConnectionInfo> devices) {
        for (com.navdy.service.library.device.connection.ConnectionInfo connectionInfo : devices) {
            if (connectionInfo == null) {
                sLogger.e("missing connection info.");
            } else {
                addDiscoveredConnectionInfo(connectionInfo);
                if (!(connectionInfo instanceof com.navdy.service.library.device.connection.BTConnectionInfo) && connectionInfo.getDeviceId().getBluetoothAddress() != null) {
                    addDiscoveredConnectionInfo(new com.navdy.service.library.device.connection.BTConnectionInfo(connectionInfo.getDeviceId(), new com.navdy.service.library.device.connection.ServiceAddress(connectionInfo.getDeviceId().getBluetoothAddress(), com.navdy.service.library.device.connection.ConnectionService.NAVDY_PROTO_SERVICE_UUID.toString()), com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF));
                }
            }
        }
    }

    @DexIgnore
    public void onLost(com.navdy.service.library.device.discovery.RemoteDeviceScanner scanner, java.util.List<com.navdy.service.library.device.connection.ConnectionInfo> list) {
    }

    @DexIgnore
    public com.navdy.service.library.device.connection.ConnectionInfo getLastPairedDevice() {
        try {
            if (this.pairedConnections.size() > 0) {
                return (com.navdy.service.library.device.connection.ConnectionInfo) this.pairedConnections.get(0);
            }
            return null;
        } catch (Throwable th) {
            return null;
        }
    }
}
