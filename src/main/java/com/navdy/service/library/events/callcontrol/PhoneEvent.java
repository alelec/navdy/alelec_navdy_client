package com.navdy.service.library.events.callcontrol;

import com.squareup.wire.Message;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;

@DexEdit(defaultAction = DexAction.ADD)
public final class PhoneEvent extends Message {
    @DexIgnore
    public static /* final */ String DEFAULT_CALLUUID; // = "";
    @DexIgnore
    public static /* final */ String DEFAULT_CONTACT_NAME; // = "";
    @DexIgnore
    public static /* final */ String DEFAULT_LABEL; // = "";
    @DexIgnore
    public static /* final */ String DEFAULT_NUMBER; // = "";
    @DexIgnore
    public static /* final */ PhoneStatus DEFAULT_STATUS; // = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE;
    @DexIgnore
    private static /* final */ long serialVersionUID; // = 0;
    // @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.STRING)
    @DexIgnore
    public /* final */ String callUUID;
    // @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    @DexIgnore
    public /* final */ String contact_name;
    // @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    @DexIgnore
    public /* final */ String label;
    // @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    @DexIgnore
    public /* final */ String number;
    // @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    @DexIgnore
    public /* final */ PhoneStatus status;

    @DexIgnore
    PhoneEvent() {}

    @DexIgnore
    public static final class Builder extends Message.Builder<PhoneEvent> {
        @DexIgnore
        public String callUUID;
        @DexIgnore
        public String contact_name;
        @DexIgnore
        public String label;
        @DexIgnore
        public String number;
        @DexIgnore
        public PhoneStatus status;

        @DexIgnore
        public Builder() {
        }

        @DexIgnore
        public Builder(PhoneEvent message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.number = message.number;
                this.contact_name = message.contact_name;
                this.label = message.label;
                this.callUUID = message.callUUID;
            }
        }

        @DexIgnore
        public PhoneEvent.Builder status(PhoneStatus status2) {
            this.status = status2;
            return this;
        }

        @DexIgnore
        public PhoneEvent.Builder number(String number2) {
            this.number = number2;
            return this;
        }

        @DexIgnore
        public PhoneEvent.Builder contact_name(String contact_name2) {
            this.contact_name = contact_name2;
            return this;
        }

        @DexIgnore
        public PhoneEvent.Builder label(String label2) {
            this.label = label2;
            return this;
        }

        @DexIgnore
        public PhoneEvent.Builder callUUID(String callUUID2) {
            this.callUUID = callUUID2;
            return this;
        }

        @DexIgnore
        public PhoneEvent build() {
            checkRequiredFields();
            return new PhoneEvent(this);
        }
    }

    @DexIgnore
    public PhoneEvent(PhoneStatus status2, String number2, String contact_name2, String label2, String callUUID2) {
        this.status = status2;
        this.number = number2;
        this.contact_name = contact_name2;
        this.label = label2;
        this.callUUID = callUUID2;
    }

    @DexIgnore
    private PhoneEvent(PhoneEvent.Builder builder) {
        this(builder.status, builder.number, builder.contact_name, builder.label, builder.callUUID);
        setBuilder(builder);
    }

    @DexIgnore
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PhoneEvent)) {
            return false;
        }
        PhoneEvent o = (PhoneEvent) other;
        return equals(this.status, o.status) && equals(this.number, o.number) && equals(this.contact_name, o.contact_name) && equals(this.label, o.label) && equals(this.callUUID, o.callUUID);
    }

    @DexIgnore
    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.number != null) {
            i = this.number.hashCode();
        } else {
            i = 0;
        }
        int i5 = (hashCode + i) * 37;
        if (this.contact_name != null) {
            i2 = this.contact_name.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 37;
        if (this.label != null) {
            i3 = this.label.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (i6 + i3) * 37;
        if (this.callUUID != null) {
            i4 = this.callUUID.hashCode();
        }
        int result2 = i7 + i4;
        this.hashCode = result2;
        return result2;
    }
}
