package com.navdy.service.library.network;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.ParcelFileDescriptor;

import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexEdit(defaultAction = DexAction.IGNORE)
public class BTSocketAdapter implements SocketAdapter {
    @DexIgnore
    private static /* final */ boolean needsPatch; // = (android.os.Build.VERSION.SDK_INT >= 17 && android.os.Build.VERSION.SDK_INT <= 20);
    @DexIgnore
    private static Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.service.library.network.BTSocketAdapter.class);
    @DexIgnore
    private boolean closed; // = false;
    @DexIgnore
    private int mFd; // = -1;
    @DexIgnore
    private ParcelFileDescriptor mPfd;
    @DexIgnore
    /* final */ BluetoothSocket socket;

    @DexIgnore
    public BTSocketAdapter(BluetoothSocket socket2) {
        if (socket2 == null) {
            throw new IllegalArgumentException("Socket must not be null");
        }
        this.socket = socket2;
        if (needsPatch) {
            storeFd();
        }
    }

    @DexIgnore
    public void connect() throws IOException {
        try {
            this.socket.connect();
            if (needsPatch) {
                storeFd();
            }
        } catch (IOException e) {
            if (needsPatch) {
                storeFd();
            }
            throw e;
        }
    }

    @DexIgnore
    public void close() throws IOException {
        synchronized (this.socket) {
            if (!this.closed) {
                if (needsPatch) {
                    storeFd();
                }
                this.socket.close();
                if (needsPatch) {
                    cleanupFd();
                }
                this.closed = true;
            }
        }
    }

    @DexIgnore
    public InputStream getInputStream() throws IOException {
        return this.socket.getInputStream();
    }

    @DexIgnore
    public OutputStream getOutputStream() throws IOException {
        return this.socket.getOutputStream();
    }

    @DexIgnore
    public boolean isConnected() {
        return this.socket.isConnected();
    }

    @DexIgnore
    public NavdyDeviceId getRemoteDevice() {
        BluetoothDevice device = this.socket.getRemoteDevice();
        if (device != null) {
            return new NavdyDeviceId(device);
        }
        return null;
    }

    @DexIgnore
    private synchronized void storeFd() {
        if (this.mPfd == null || this.mFd == -1) {
            try {
                Field field = this.socket.getClass().getDeclaredField("mPfd");
                field.setAccessible(true);
                ParcelFileDescriptor pfd = (ParcelFileDescriptor) field.get(this.socket);
                if (pfd != null) {
                    this.mPfd = pfd;
                }
                int fd = IOUtils.getSocketFD(this.socket);
                if (fd != -1) {
                    this.mFd = fd;
                }
                sLogger.d("Stored " + this.mPfd + " fd:" + this.mFd);
            } catch (Exception e) {
                sLogger.w("Exception storing socket internals", e);
            }
        }
        return;
    }

    @DexIgnore
    private synchronized void cleanupFd() throws IOException {
        if (this.mPfd != null) {
            sLogger.d("Closing mPfd");
            this.mPfd.close();
            this.mPfd = null;
        }
        if (this.mFd != -1) {
            sLogger.d("Closing mFd:" + this.mFd);
            IOUtils.closeFD(this.mFd);
            this.mFd = -1;
        }
    }
}
