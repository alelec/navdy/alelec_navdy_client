import re
import os
import sys
import shutil
from pathlib import Path

cd = Path(__file__).parent

jadx = cd / 'src' / 'jadx' / 'sources'
main = cd / 'src' / 'main' / 'java'

target = sys.argv[1]


if 'app-classes.jar!' in target:
    # idea decompiler, don't think it writes to temp file at all :-(
    tail = target.split('app-classes.jar!\\')[1]
    tail = tail.replace('.class', '.java')
    target = jadx.joinpath(tail)

else:
    target = Path(target)

newpath = main / os.path.relpath(target, jadx)
print(os.path.abspath(newpath))

newpath.parent.mkdir(exist_ok=True, parents=True)
shutil.copy(target, newpath)

text = Path(newpath).read_text()
print(len(text))

text = re.sub(r"/\* access modifiers changed from: (p.*) \*/\n +p.*? ", r"\1 ", text, flags=re.M)

text = re.sub(r"^((    )+)((public|private|protected) .*;)", r"\1@DexIgnore\n\1\3", text, flags=re.M)

text = re.sub(r"^    @", r"    // @", text, flags=re.M)
#text = re.sub(r"^(    [a-z].* )final (.* = .*;)", r"\1/* final */ \2", text, flags=re.M)
text = re.sub(r"^(    [a-z].* )final (.*;)", r"\1/* final */ \2", text, flags=re.M)
text = re.sub(r"^    final (.* = .*;)", r"    /* final */ \1", text, flags=re.M)
text = re.sub(r"^(    [a-z].*)( = .*;)", r"\1; //\2", text, flags=re.M)
text = re.sub(r"^(public|class)", r"import lanchon.dexpatcher.annotation.DexEdit;\nimport lanchon.dexpatcher.annotation.DexIgnore;\nimport lanchon.dexpatcher.annotation.DexAction;\n\n@DexEdit(defaultAction = DexAction.IGNORE)\n\1", text, flags=re.M)
text = re.sub(r"^(    [a-z])", r"    @DexIgnore\n\1", text, flags=re.M)
text = re.sub(r"^( +// @DexIgnore\n)", r"", text, flags=re.M)
text = re.sub(r"^(        [pcA-Z].*\(.* {)", r"        @DexIgnore\n\1", text, flags=re.M)

text = re.sub(r"^( +)final( .*;)", r"\1@DexIgnore\n\1/* final */\2", text, flags=re.M)

for m in re.findall(r'class (Anon\d+) ', text):
    if (' %s() {' % m) not in text:
        text = re.sub(r"^( +)(.*class )(" + str(m) + r")( .*{)", r"\1\2\3\4\n\1    @DexIgnore\n\1    \3() {\n\1    }\n", text, flags=re.M)

#text = re.sub(r"\n(    class Anon.*\n)((.*\n)+?)(    })", r"\n\1/*\2*/\4", text)

text = re.sub(r"(@DexIgnore\n    static \{\n)((.*\n)+?)(    })", r"/* \1\2\4 */", text)

print(len(text))
Path(newpath).write_text(text)

os.system("git add %s" % newpath)
# os.system("update_hud_jar.exe")
